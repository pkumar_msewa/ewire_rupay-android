#-optimizationpasses 5
#-verbose
#-useuniqueclassmembernames
#-keepattributes SourceFile,LineNumberTable
#-allowaccessmodification
#-dontwarn **
#-ignorewarnings
#
#
##Keep classes that are referenced on the AndroidManifest
#-keep public class * extends android.support.v7.app.AppCompatActivity
#-keep public class * extends android.app.Application
#-keep public class * extends com.orm.SugarRecord
#-keep class in.msscard.msspay.** { *; }
#-dontusemixedcaseclassnames
#-dontskipnonpubliclibraryclasses
#-verbose
#-adaptresourcefilenames
#-keep class in.msscard.model.** { *; }
#-keep class in.msscard.adapter.** { *; }
#-keep class in.msscard.Animations.** { *; }
#-keep class in.msscard.fragment.** { *; }
#-keep class in.msscard.shoppingold.** { *; }
#-keep class in.msscard.util.** { *; }
#-keep class in.msscard.msspay.** {*;}
##-keep class in.msspay.paulpay.** {*;}
#-keep public class * extends android.content.BroadcastReceiver
#-keep public class * extends Request<String>
#
#-keep interface android.support.v4.app.** { *; }
#-keep class android.support.v4.app.** { *; }
#-keep class org.apache.http.** { *; }
#-keep class okhttp3.** { *; }
#
#-dontwarn org.apache.commons.**
#-dontwarn org.apache.http.**
#-dontwarn com.google.**
#-dontwarn com.github.**
#-dontwarn com.android.volley.**
#-dontwarn com.androidquery.**
#-dontwarn okio.**
#
#-dontnote android.net.http.*
#-dontnote org.apache.commons.codec.**
#-dontnote org.apache.http.**
#-dontnote okhttp3.**
#
#-keeppackagenames org.jsoup.nodes
#-keep public enum * {
#    public static **[] values();
#    public static ** valueOf(java.lang.String);
#}
#
##-dontwarn okhttp3.**
##-dontwarn com.squareup.**
#
#
### Keep Picasso
##-keep class com.squareup.picasso.** { *; }
##-keepclasseswithmembers class * {
##    @com.squareup.picasso.** *;
##}
##-keepclassmembers class * {
##    @com.squareup.picasso.** *;
##}
#
#
#
### Keep OKHttp
## OkHttp
#-keepattributes Signature
#-keepattributes *Annotation*
#-keep class okhttp3.** { *; }
#-keep interface okhttp3.** { *; }
#-dontwarn okhttp3.**
#-keep class com.google.android.gms.iid.zzd { *; }
#-keep class android.support.v4.content.ContextCompat { *; }
### Keep AQuery
#-keep class com.androidquery.** { *; }
#-keepclasseswithmembers class * {
#    @com.androidquery.** *;
#}
#-keepclassmembers class * {
#    @com.androidquery.** *;
#}
#-keepclassmembers class com.dom925.xxxx {
#   public *;
#}
##To remove debug logs:
#-assumenosideeffects class android.util.Log {
#public static boolean isLoggable(java.lang.String, int);
#    public static *** d(...);
#    public static *** i(...);
#    public static *** e(...);
#    public static *** w(...);
#    public static *** wtf(...);
#    public static *** v(...);
#}
#-keepclassmembers class io.appsfly.**{
#   *;
#}
#
#
##Maintain java native methods
#-keepclasseswithmembernames class * {
#    native <methods>;
#}
#
##To maintain custom components names that are used on layouts XML.
##Uncomment if having any problem with the approach below
##-keep public class custom.components.package.and.name.**
#
##To maintain custom components names that are used on layouts XML:
#-keep public class * extends android.view.View {
#    public <init>(android.content.Context);
#    public <init>(android.content.Context, android.util.AttributeSet);
#    public <init>(android.content.Context, android.util.AttributeSet, int);
#    public void set*(...);
#}
#
#-keepclasseswithmembers class * {
#    public <init>(android.content.Context, android.util.AttributeSet);
#}
#
#-keepclasseswithmembers class * {
#    public <init>(android.content.Context, android.util.AttributeSet, int);
#}
#
#
##To keep parcelable classes (to serialize - deserialize objects to sent through Intents)
#-keep class * implements android.os.Parcelable {
#  public static final android.os.Parcelable$Creator *;
#}
#
##Keep the R
#-keepclassmembers class **.R$* {
#    public static <fields>;
#}
#
#-keep public class com.google.android.gms.common.internal.safeparcel.SafeParcelable {
#    public static final *** NULL;
#}
#
#-keepnames class * implements android.os.Parcelable
#-keepclassmembers class * implements android.os.Parcelable {
#  public static final *** CREATOR;
#}
#
#-keep @interface android.support.annotation.Keep
#-keep @android.support.annotation.Keep class *
#-keepclasseswithmembers class * {
#  @android.support.annotation.Keep <fields>;
#}
#-keepclasseswithmembers class * {
#  @android.support.annotation.Keep <methods>;
#}
#
#-keep @interface com.google.android.gms.common.annotation.KeepName
#-keepnames @com.google.android.gms.common.annotation.KeepName class *
#-keepclassmembernames class * {
#  @com.google.android.gms.common.annotation.KeepName *;
#}
#
#-keep @interface com.google.android.gms.common.util.DynamiteApi
#-keep public @com.google.android.gms.common.util.DynamiteApi class * {
#  public <fields>;
#  public <methods>;
#}
#
#-dontwarn android.security.NetworkSecurityPolicy
#


-optimizationpasses 5
-verbose
-useuniqueclassmembernames
-keepattributes SourceFile,LineNumberTable
-allowaccessmodification
-dontwarn **
-ignorewarnings


#Keep classes that are referenced on the AndroidManifest
-keep public class * extends android.support.v7.app.AppCompatActivity
-keep public class * extends android.app.Application
-keep public class * extends com.orm.SugarRecord
-keep class in.MadMoveGlobal.EwireRuPay.** { *; }
-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-verbose
-adaptresourcefilenames
-keep class in.MadMoveGlobal.model.** { *; }
-keep class in.MadMoveGlobal.adapter.** { *; }
-keep class in.MadMoveGlobal.Animations.** { *; }
-keep class in.MadMoveGlobal.fragment.** { *; }
-keep class in.MadMoveGlobal.shoppingold.** { *; }
-keep class in.MadMoveGlobal.util.** { *; }
-keep class in.MadMoveGlobal.EwireRuPay.** {*;}
-keep class android.support.v7.widget.** { *; }
#-keep class in.msspay.paulpay.** {*;}
-keep public class * extends android.content.BroadcastReceiver

-keep interface android.support.v4.app.** { *; }
-keep class android.support.v4.app.** { *; }
-keep class org.apache.http.** { *; }
-keep class okhttp3.** { *; }

-dontwarn org.apache.commons.**
-dontwarn org.apache.http.**
-dontwarn com.google.**
-dontwarn com.github.**
-dontwarn com.android.volley.**
-dontwarn com.androidquery.**
-dontwarn okio.**

-dontnote android.net.http.*
-dontnote org.apache.commons.codec.**
-dontnote org.apache.http.**
-dontnote okhttp3.**
-dontwarn com.zoho.salesiq.**

-keeppackagenames org.jsoup.nodes
-keep public enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

#-dontwarn okhttp3.**
#-dontwarn com.squareup.**


## Keep Picasso
#-keep class com.squareup.picasso.** { *; }
#-keepclasseswithmembers class * {
#    @com.squareup.picasso.** *;
#}
#-keepclassmembers class * {
#    @com.squareup.picasso.** *;
#}



## Keep OKHttp
# OkHttp
-keepattributes Signature
-keepattributes *Annotation*
-keep class okhttp3.** { *; }
-keep interface okhttp3.** { *; }
-dontwarn okhttp3.**
-keep class com.google.android.gms.iid.zzd { *; }
-keep class android.support.v4.content.ContextCompat { *; }
## Keep AQuery
-keep class com.androidquery.** { *; }
-keepclasseswithmembers class * {
    @com.androidquery.** *;
}
-keepclassmembers class * {
    @com.androidquery.** *;
}
-keepclassmembers class com.dom925.xxxx {
   public *;
}
#To remove debug logs:
-assumenosideeffects class android.util.Log {
public static boolean isLoggable(java.lang.String, int);
    public static *** d(...);
    public static *** i(...);
    public static *** e(...);
    public static *** w(...);
    public static *** wtf(...);
    public static *** v(...);
}

-keepclassmembers class * {
    @android.webkit.JavascriptInterface <methods>;
}

-keepattributes JavascriptInterface
-keepattributes *Annotation*

-dontwarn com.razorpay.**
-keep class com.razorpay.** {*;}

-optimizations !method/inlining/*

-keepclasseswithmembers class * {
  public void onPayment*(...);
}

-optimizations !method/inlining/*

-keepclassmembers class io.appsfly.**{
   *;
}


#Maintain java native methods
-keepclasseswithmembernames class * {
    native <methods>;
}

#To maintain custom components names that are used on layouts XML.
#Uncomment if having any problem with the approach below
#-keep public class custom.components.package.and.name.**

#To maintain custom components names that are used on layouts XML:
-keep public class * extends android.view.View {
    public <init>(android.content.Context);
    public <init>(android.content.Context, android.util.AttributeSet);
    public <init>(android.content.Context, android.util.AttributeSet, int);
    public void set*(...);
}

-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet);
}

-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet, int);
}


#To keep parcelable classes (to serialize - deserialize objects to sent through Intents)
-keep class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator *;
}

#Keep the R
-keepclassmembers class **.R$* {
    public static <fields>;
}

-keep public class com.google.android.gms.common.internal.safeparcel.SafeParcelable {
    public static final *** NULL;
}

-keepnames class * implements android.os.Parcelable
-keepclassmembers class * implements android.os.Parcelable {
  public static final *** CREATOR;
}

-keep @interface android.support.annotation.Keep
-keep @android.support.annotation.Keep class *
-keepclasseswithmembers class * {
  @android.support.annotation.Keep <fields>;
}
-keepclasseswithmembers class * {
  @android.support.annotation.Keep <methods>;
}

-keep @interface com.google.android.gms.common.annotation.KeepName
-keepnames @com.google.android.gms.common.annotation.KeepName class *
-keepclassmembernames class * {
  @com.google.android.gms.common.annotation.KeepName *;
}

-keep @interface com.google.android.gms.common.util.DynamiteApi
-keep public @com.google.android.gms.common.util.DynamiteApi class * {
  public <fields>;
  public <methods>;
}

-dontwarn android.security.NetworkSecurityPolicy
