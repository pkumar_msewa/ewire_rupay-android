package in.MadMoveGlobal.util;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyLog;

import org.apache.http.HttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map;

import in.MadMoveGlobal.model.UserModel;

public class MultipartRequest extends Request<String> {

    MultipartEntityBuilder entity = MultipartEntityBuilder.create();
    HttpEntity httpentity;
    private String FILE_PART_NAME = "profilePicture";
    private String parmimage2;

    private UserModel userModel = UserModel.getInstance();

    private final Response.Listener<String> mListener;
    private final File mFilePart, image2;
    private Map<String, String> headerParams;

    public MultipartRequest(String url, Response.ErrorListener errorListener,
                            Response.Listener<String> listener, File file,
                            final Map<String, String> headerParams, String partName, File image2, String parmter2) {
        super(Method.POST, url, errorListener);
        this.mListener = listener;
        this.mFilePart = file;
        this.image2 = image2;
        this.headerParams = headerParams;
        this.FILE_PART_NAME = partName;
        this.parmimage2 = parmter2;
        entity.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        buildMultipartEntity();
        httpentity = entity.build();
    }


    private void buildMultipartEntity() {
        if (mFilePart.exists()) {

        }

        ContentType contentType = ContentType.create("image/jpeg");

        entity.addPart(FILE_PART_NAME, new FileBody(mFilePart, contentType, mFilePart.getName()));
        entity.addPart(parmimage2, new FileBody(image2, contentType, image2.getName()));
        entity.addTextBody("sessionId", userModel.getUserSessionId());
        entity.addTextBody("idNumber", headerParams.get("idNumber"));
        entity.addTextBody("idType", headerParams.get("idType"));
        entity.addTextBody("address1", headerParams.get("address1"));
        entity.addTextBody("addres2", headerParams.get("addres2"));
        entity.addTextBody("city", headerParams.get("city"));
        entity.addTextBody("state", headerParams.get("state"));
        entity.addTextBody("pincode", headerParams.get("pincode"));
        entity.addTextBody("country", "India");


        Log.i("NEWHEADER", entity.toString());
    }


//    @Override
//    public Map<String, String> getHeaders() throws AuthFailureError {
//        return (headerParams != null) ? headerParams : super.getHeaders();
//    }

    @Override
    public String getBodyContentType() {
        return httpentity.getContentType().getValue();
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            httpentity.writeTo(bos);
        } catch (IOException e) {
            VolleyLog.e("IOException writing to ByteArrayOutputStream");
        }
//        String s = createPostBody(headerParams);
//        return s.getBytes();
        return bos.toByteArray();
    }

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {

        try {
            return Response.success(new String(response.data, "UTF-8"),
                    getCacheEntry());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return Response.success(new String(response.data), getCacheEntry());
        }
    }

    @Override
    protected void deliverResponse(String response) {
        mListener.onResponse(response);
    }

    private String createPostBody(Map<String, String> params) {
        StringBuilder sbPost = new StringBuilder();
        for (String key : params.keySet()) {
            if (params.get(key) != null) {
                sbPost.append("\r\n" + "--" + "BOUNDARY" + "\r\n");
                sbPost.append("Content-Disposition: form-data; name=\"" + key + "\"" + "\r\n\r\n");
                sbPost.append(params.get(key));
            }
        }
        return sbPost.toString();
    }

}
