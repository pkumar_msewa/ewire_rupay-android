package in.MadMoveGlobal.util;

import android.content.Context;
import android.content.Intent;

import in.MadMoveGlobal.EwireRuPay.WebViewActivity;

/**
 * Created by Dushant on 06-Nov-17.
 */

public class LoadCheckMoney {

    private Context context;
    private String amount;

    public LoadCheckMoney(Context context, String amount) {
        this.context = context;
        this.amount = amount;
    }

    public void startLoadMoney(String amt){
        Intent ccAvenueIntent = new Intent(context, WebViewActivity.class);
        ccAvenueIntent.putExtra("amountToLoad",amt);
    }


}
