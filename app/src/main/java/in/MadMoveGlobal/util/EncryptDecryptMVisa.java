package in.MadMoveGlobal.util;

import android.util.Base64;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by Kashif-PC on 1/13/2017.
 */
public class EncryptDecryptMVisa {

    private static final String ALGO = "AES";
    private static final String WORKING_KEY = "VIJAYABANKM2PBNK";

//    public static String encrypt(String Data) throws Exception {
//        Key key = generateKey(WORKING_KEY);
//        Cipher c = Cipher.getInstance(ALGO);
//        c.init(Cipher.ENCRYPT_MODE, key);
//        byte[] encVal = c.doFinal(Data.getBytes());
//        String encryptedValue = Base64.encodeToString(encVal, Base64.DEFAULT);
//        return encryptedValue;
//    }

    private static Key generateKey(String workingKey) throws Exception {
        byte[] keyValue = workingKey.getBytes();
        Key key = new SecretKeySpec(keyValue, ALGO);
        return key;
    }

    public static String decrypt(String encryptedData) throws Exception {
        String d = URLDecoder.decode(encryptedData, "UTF-8");
        Key key = generateKey(WORKING_KEY);
        Cipher c = Cipher.getInstance(ALGO);
        c.init(Cipher.DECRYPT_MODE, key);
        byte[] data = Base64.decode(d, Base64.DEFAULT);
        byte[] decValue = c.doFinal(data);
        String decryptedValue = new String(decValue);
        return decryptedValue;
    }


    public static String encrypt(String Data) throws Exception {
        Key key = generateKey(WORKING_KEY);
        Cipher c = Cipher.getInstance(ALGO);
        c.init(Cipher.ENCRYPT_MODE, key);
        byte[] encVal = c.doFinal(Data.getBytes());
//        String encryptedValue = new BASE64Encoder().encode(encVal);
//        String encryptedValue = new BASE64Encoder().encode(encVal);
        String base64 = Base64.encodeToString(encVal, Base64.DEFAULT);
        String ss = URLEncoder.encode(base64, "UTF-8");
        return ss;
    }
}
