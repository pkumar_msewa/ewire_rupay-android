package in.MadMoveGlobal.util;


import java.util.ArrayList;

import in.MadMoveGlobal.model.MobilePlansModel;


public class MobilePlansCheck {
	
	private volatile static MobilePlansCheck uniqueInstance;
	
	private ArrayList<MobilePlansModel> talkTimePlans = new ArrayList<>();
	private ArrayList<MobilePlansModel> topUpPlans = new ArrayList<>();
	private ArrayList<MobilePlansModel> threeGPlans = new ArrayList<>();
	private ArrayList<MobilePlansModel> twoGPlans = new ArrayList<>();
	private ArrayList<MobilePlansModel> fourGPlans = new ArrayList<>();
	private ArrayList<MobilePlansModel> localPlans = new ArrayList<>();
	private ArrayList<MobilePlansModel> isdPlans = new ArrayList<>();
	private ArrayList<MobilePlansModel> stdPlans = new ArrayList<>();
	private ArrayList<MobilePlansModel> smsPlans = new ArrayList<>();
	private ArrayList<MobilePlansModel> planPlans = new ArrayList<>();
	private ArrayList<MobilePlansModel> otherPlans = new ArrayList<>();
	
	/*
	 * Singleton
	 */
	private MobilePlansCheck() {}
	
	/*
	 * Singleton getInstance()
	 */
	public static MobilePlansCheck getInstance() {
		if (uniqueInstance == null) {
			synchronized (MobilePlansCheck.class) {
				if (uniqueInstance == null) {
					uniqueInstance = new MobilePlansCheck();
				}
			}
		}
		return uniqueInstance;
	}

	public ArrayList<MobilePlansModel> getTalkTimePlans() {
		return talkTimePlans;
	}

	public void setTalkTimePlans(ArrayList<MobilePlansModel> talkTimePlans) {
		this.talkTimePlans = talkTimePlans;
	}

	public ArrayList<MobilePlansModel> getTopUpPlans() {
		return topUpPlans;
	}

	public void setTopUpPlans(ArrayList<MobilePlansModel> topUpPlans) {
		this.topUpPlans = topUpPlans;
	}

	public ArrayList<MobilePlansModel> getThreeGPlans() {
		return threeGPlans;
	}

	public void setThreeGPlans(ArrayList<MobilePlansModel> threeGPlans) {
		this.threeGPlans = threeGPlans;
	}

	public ArrayList<MobilePlansModel> getTwoGPlans() {
		return twoGPlans;
	}

	public void setTwoGPlans(ArrayList<MobilePlansModel> twoGPlans) {
		this.twoGPlans = twoGPlans;
	}

	public ArrayList<MobilePlansModel> getFourGPlans() {
		return fourGPlans;
	}

	public void setFourGPlans(ArrayList<MobilePlansModel> fourGPlans) {
		this.fourGPlans = fourGPlans;
	}

	public ArrayList<MobilePlansModel> getOtherPlans() {
		return otherPlans;
	}

	public void setOtherPlans(ArrayList<MobilePlansModel> otherPlans) {
		this.otherPlans = otherPlans;
	}

	public ArrayList<MobilePlansModel> getLocalPlans() {
		return localPlans;
	}

	public void setLocalPlans(ArrayList<MobilePlansModel> localPlans) {
		this.localPlans = localPlans;
	}

	public ArrayList<MobilePlansModel> getSMSPlans() {
		return smsPlans;
	}

	public void setSMSPlans(ArrayList<MobilePlansModel> smsPlans) {
		this.smsPlans = smsPlans;
	}
	public ArrayList<MobilePlansModel> getISDPlans() {
		return isdPlans;
	}

	public void setISDPlans(ArrayList<MobilePlansModel> isdPlans) {
		this.isdPlans = isdPlans;
	}
	public ArrayList<MobilePlansModel> getSTDPlans() {
		return stdPlans;
	}

	public void setSTDPlans(ArrayList<MobilePlansModel> stdPlans) {
		this.stdPlans = stdPlans;
	}

	public ArrayList<MobilePlansModel> getPlanPlans() {
		return planPlans;
	}

	public void setPlanPlans(ArrayList<MobilePlansModel> planPlans) {
		this.planPlans = planPlans;
	}


	
}
