package in.MadMoveGlobal.util;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Ksf on 5/7/2016.
 */
public class CreatePostValue {



    public static String createLogin(String userName, String password){
        String response = "";
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("username", userName);
            jsonObject.put("password", password);
            response = jsonObject.toString();
            return  response;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return response;
    }

    public static String createRegister(String firstName, String lastName, String password, String rePassword, String contactNo, String email,String dob, String gender){
        String response = "";
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("firstName", firstName);
            jsonObject.put("lastName", lastName);
            jsonObject.put("password", password);
            jsonObject.put("confirmPassword", rePassword);
            jsonObject.put("contactNo", contactNo);
            jsonObject.put("email", email);
            jsonObject.put("dob", dob);
            jsonObject.put("gender", gender);

            response = jsonObject.toString();
            return  response;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return response;
    }


    public static String createDTH(String serviceProvider, String dthNo, String amount, String sessionId){
        String response = "";
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("serviceProvider", serviceProvider);
            jsonObject.put("dthNo", dthNo);
            jsonObject.put("amount", amount);
            jsonObject.put("sessionId", sessionId);
            response = jsonObject.toString();
            Log.i("DTH CREATOR",response);
            return  response;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return response;
    }

    public static String createElectricity(String serviceProvider, String electNo, String cycleNo, String amount, String sessionId){
        String response = "";
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("serviceProvider", serviceProvider);
            jsonObject.put("accountNumber", electNo);
            jsonObject.put("cycleNumber", cycleNo);
            jsonObject.put("amount", amount);
            jsonObject.put("sessionId", sessionId);

            response = jsonObject.toString();
            Log.i("Electricity CREATOR",response);
            return  response;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return response;
    }

    public static String createElectricityNoCycle(String serviceProvider, String electNo, String amount, String sessionId){
        String response = "";
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("serviceProvider", serviceProvider);
            jsonObject.put("accountNumber", electNo);
            jsonObject.put("amount", amount);
            jsonObject.put("sessionId", sessionId);

            response = jsonObject.toString();
            Log.i("Electricity CREATOR",response);
            return  response;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return response;
    }

    


}
