package in.MadMoveGlobal.util;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;

/**
 * Created by Ksf on 6/10/2016.
 */
public class EmailCouponsUtil {

    private static UserModel session = UserModel.getInstance();


    public static void emailForCoupons(String tag_json_obj) {
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, ApiUrl.URL_EMAIL_COUPONS+session.getUserEmail(), (String)null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i("Email Successful","Yeh!! Email Successful");
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Error Response","Email not Successful");
            }
        }) {


        };
        PayQwikApplication.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);

    }


}
