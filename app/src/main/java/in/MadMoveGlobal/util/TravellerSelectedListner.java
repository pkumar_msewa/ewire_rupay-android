package in.MadMoveGlobal.util;

/**
 * Created by Ksf on 9/26/2016.
 */
public interface TravellerSelectedListner {
    public void onSelected(int adultCount, int childCount, int infantCount);
}
