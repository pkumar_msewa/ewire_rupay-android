package in.MadMoveGlobal.util;


import in.MadMoveGlobal.model.OperatorsModel;

/**
 * Created by Ksf on 9/30/2016.
 */
public interface ServicesSelectedListener {

    public void serviceselect(String type, long servicecode, String serviceName);
    public void serviceFilter(OperatorsModel type);
}
