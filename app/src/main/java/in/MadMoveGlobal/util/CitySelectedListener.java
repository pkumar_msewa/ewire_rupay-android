package in.MadMoveGlobal.util;


import in.MadMoveGlobal.model.BusCityModel;

/**
 * Created by Ksf on 9/30/2016.
 */
public interface CitySelectedListener {

    public void citySelected(String type, long cityCode, String cityName);
    public void cityFilter(BusCityModel type);
}
