package in.MadMoveGlobal.util;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyLog;

import org.apache.http.HttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map;

import in.MadMoveGlobal.model.UserModel;

public class MultipartInsuranceRequest extends Request<String> {
    MultipartEntityBuilder entity = MultipartEntityBuilder.create();
    HttpEntity httpEntity;
    private String FILE_PART_NAME;
    private String paramImage;
    private UserModel userModel = UserModel.getInstance();

    private final Response.Listener<String> mListener;
    private final File mFilePart, image2;
    private Map<String, String> headerParams;

    public MultipartInsuranceRequest(String url, Response.ErrorListener errorListener,
                            Response.Listener<String> listener, File file,
                            final Map<String, String> headerParams, String partName, File image2, String parmter2) {
        super(Method.POST, url, errorListener);
        this.mListener = listener;
        this.mFilePart = file;
        this.image2 = image2;
        this.headerParams = headerParams;
        this.FILE_PART_NAME = partName;
        this.paramImage = paramImage;
        entity.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        buildMultipartEntity();
        httpEntity = entity.build();
    }

    private void buildMultipartEntity() {

        ContentType contentType = ContentType.create("image/jpeg");
        entity.addPart(FILE_PART_NAME, new FileBody(mFilePart, contentType, mFilePart.getName()));
        entity.addPart(paramImage, new FileBody(image2, contentType, image2.getName()));
        entity.addTextBody("sessionId", userModel.getUserSessionId());
        entity.addTextBody("email", headerParams.get("email"));
        entity.addTextBody("mobile", headerParams.get("mobile"));
    }

    @Override
    public String getBodyContentType() {
        return httpEntity.getContentType().getValue();
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            httpEntity.writeTo(bos);
        } catch (IOException e) {
            VolleyLog.e("IOException writing to ByteArrayOutputStream");
        }
        return bos.toByteArray();
    }

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {
        try {
            return Response.success(new String(response.data, "UTF-8"),
                    getCacheEntry());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return Response.success(new String(response.data), getCacheEntry());
        }
    }

    @Override
    protected void deliverResponse(String response) {
        mListener.onResponse(response);
    }
}
