package in.MadMoveGlobal.util;


import in.MadMoveGlobal.model.CircleModel;
import in.MadMoveGlobal.model.OperatorsModel;

/**
 * Created by Ksf on 9/30/2016.
 */
public interface AreasSelectedListener {

    public void areaselect(String type, long areacode, String areaName);
    public void areaFilter(CircleModel type);
}
