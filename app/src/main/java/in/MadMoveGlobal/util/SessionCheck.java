package in.MadMoveGlobal.util;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.RequestQueue;

import org.json.JSONObject;

import in.MadMoveGlobal.model.UserModel;


/**
 * Created by Kashif-PC on 4/25/2017.
 */
public class SessionCheck extends Service {
    private RequestQueue rq;
    private JSONObject jsonRequest;
    private UserModel session = UserModel.getInstance();
    private boolean status = true;
    private Context context;
    Handler h;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Let it continue running until it is stopped.
        Toast.makeText(this, "Service Started : "+Thread.currentThread().getId(), Toast.LENGTH_LONG).show();
//        final Handler handler = new Handler();
//        Timer    timer = new Timer();
//        TimerTask doAsynchronousTask = new TimerTask() {
//            @Override
//            public void run() {
//                handler.post(new Runnable() {
//                    @SuppressWarnings("unchecked")
//                    public void run() {
//                        try {
//                            CustomToast.showMessage(context,"checking started");
//                            Log.i("SERVICESTART","Srvice thread started : "+Thread.currentThread().getId());
//                           if(!checkSession()){
//                               stopSelf();
//                           }
//                        }
//                        catch (Exception e) {
//                            // TODO Auto-generated catch block
//                        }
//                    }
//                });
//            }
//        };
//        timer.schedule(doAsynchronousTask, 0, 30000);

        h = new Handler();
            new Thread(new Runnable() {
                public void run(){
                    while(true){
                        try{
                            h.post(new Runnable(){
                                       public void run(){
//                                           CustomToast.showMessage(context,"checking started");
                                           Log.i("SERVICESTART","Srvice thread started : "+Thread.currentThread().getId());

                                       }
                                   });
//                                    TimeUnit.MINUTES.sleep(5);
                        }
                        catch(Exception ex){
                        }
                    }
                }
            }).start();

        return START_STICKY;

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "Service Destroyed : "+Thread.currentThread().getId(), Toast.LENGTH_LONG).show();
        Log.i("ONDESTROY","Srvice thread destroy : "+Thread.currentThread().getId());
        stopSelf();
    }

}
