package in.MadMoveGlobal.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.provider.Settings;
import android.telephony.TelephonyManager;

/**
 * Created by Ksf on 5/7/2016.
 */
public class SecurityUtil {


    public static String getSecurityKey(Context context){
        // TODO Encryption Algorithm
        String android_id = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        TelephonyManager mTelephonyMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String imsi = mTelephonyMgr.getSubscriberId();
        String imei = mTelephonyMgr.getDeviceId();
        String simno = mTelephonyMgr.getSimSerialNumber();
        String key = "ANDROID ID:"+android_id + "\nIMSI"+ imsi+"\nIMEI"+imei+"\nSimNo"+simno;
        return key;
    }

    public static String getAndroidId(Context context){
        // TODO Encryption Algorithm
        @SuppressLint("HardwareIds") String android_id = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        return android_id;
    }

    public static String getIMEI(Context context){
        // TODO Encryption Algorithm
        TelephonyManager mTelephonyMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        @SuppressLint("HardwareIds") String imei = mTelephonyMgr.getDeviceId();
        return imei;
    }


    public static String getSecurityKey(String message){
        // TODO Encryption Algorithm
        return "123";



    }

}
