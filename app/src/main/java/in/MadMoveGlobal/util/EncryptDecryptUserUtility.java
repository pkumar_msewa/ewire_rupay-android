package in.MadMoveGlobal.util;


import android.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by Ksf on 5/29/2016.
 */
public class EncryptDecryptUserUtility {
    private static final byte[] keyValueUser =  new byte[] {'P','a','y','Q','w','i','K','A','l','g','O','u','s','e','r','s'};

    public static String encrypt(String plainText) throws Exception
    {
        Cipher cipher = getCipherUser(Cipher.ENCRYPT_MODE);
        byte[] encryptedBytes = cipher.doFinal(plainText.getBytes());
        return android.util.Base64.encodeToString(encryptedBytes, Base64.URL_SAFE);

    }

    public static String encryptAuth(String data){
        return "Basic " + new String(Base64.encode(data.getBytes(), Base64.NO_WRAP));
    }

    public static String decrypt(String encrypted) throws Exception
    {
        Cipher cipher = getCipherUser(Cipher.DECRYPT_MODE);
        byte[] plainBytes = cipher.doFinal(android.util.Base64.decode(encrypted, Base64.URL_SAFE));
        return new String(plainBytes);
    }


    private static Cipher getCipherUser(int cipherMode) throws Exception
    {
        String encryptionAlgorithm = "AES";
        SecretKeySpec keySpecification = new SecretKeySpec(keyValueUser, encryptionAlgorithm);
        Cipher cipher = Cipher.getInstance(encryptionAlgorithm);
        cipher.init(cipherMode, keySpecification);

        return cipher;
    }


}
