package in.MadMoveGlobal.util;



import in.MadMoveGlobal.model.IFSCModel;


/**
 * Created by Ksf on 9/30/2016.
 */
public interface IFSCSelectedListener {

    public void serviceselect(String type, long ifscAccount, long ifscNo, String Name,long ids, String bankName);
    public void serviceFilter(IFSCModel type);
}
