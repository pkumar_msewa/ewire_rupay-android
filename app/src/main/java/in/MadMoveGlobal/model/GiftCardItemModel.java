package in.MadMoveGlobal.model;

/**
 * Created by kashifimam on 28/02/17.
 */

public class GiftCardItemModel {

    private long id;
    private int name;
    private String skuId;
    private String type;
    private String valueType;

    public GiftCardItemModel(long id, int name, String skuId, String type, String valueType) {
        this.id = id;
        this.name = name;
        this.skuId = skuId;
        this.type = type;
        this.valueType = valueType;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getName() {
        return name;
    }

    public void setName(int name) {
        this.name = name;
    }

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValueType() {
        return valueType;
    }

    public void setValueType(String valueType) {
        this.valueType = valueType;
    }
}
