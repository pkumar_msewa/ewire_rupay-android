package in.MadMoveGlobal.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by acer on 04-07-2017.
 */

public class ImagicaPackageModel implements Parcelable {

    private String PimageURL,Pdescription,Ptitle,Pcurrency,passengerType,bdaddOneName,cdaddOnName,tdname;
    private int Pquantity,tdquantity,bdquantity,cdquantity;
    private double PcostPer,PcostTotal,tdpricePerQty;
    private long PproductID,cdaddOnId,bdaddOnId;
    private  boolean carExists,busExists;
    private ArrayList<TaxDetailList> taxDetailListList;
    private TaxDetailList taxDetailListList1;

    public ImagicaPackageModel(String pimageURL, String pdescription, String ptitle, String pcurrency, String passengerType, String bdaddOneName, String cdaddOnName,  int pquantity, int tdquantity, int bdquantity, int cdquantity, double pcostPer, double pcostTotal, long pproductID, long cdaddOnId, long bdaddOnId, boolean carExists, boolean busExists,ArrayList<TaxDetailList> taxDetailListList) {
        PimageURL = pimageURL;
        Pdescription = pdescription;
        Ptitle = ptitle;
        Pcurrency = pcurrency;
        this.passengerType = passengerType;
        this.bdaddOneName = bdaddOneName;
        this.cdaddOnName = cdaddOnName;
        Pquantity = pquantity;
        this.tdquantity = tdquantity;
        this.bdquantity = bdquantity;
        this.cdquantity = cdquantity;
        PcostPer = pcostPer;
        PcostTotal = pcostTotal;
        PproductID = pproductID;
        this.cdaddOnId = cdaddOnId;
        this.bdaddOnId = bdaddOnId;
        this.carExists = carExists;
        this.busExists = busExists;
        this.taxDetailListList=taxDetailListList;
    }


    public ImagicaPackageModel(TaxDetailList taxDetailListList) {
        this.taxDetailListList1 = taxDetailListList;
    }

    protected ImagicaPackageModel(Parcel in) {
        PimageURL = in.readString();
        Pdescription = in.readString();
        Ptitle = in.readString();
        Pcurrency = in.readString();
        passengerType = in.readString();
        bdaddOneName = in.readString();
        cdaddOnName = in.readString();
        tdname = in.readString();
        Pquantity = in.readInt();
        tdquantity = in.readInt();
        bdquantity = in.readInt();
        cdquantity = in.readInt();
        PcostPer = in.readDouble();
        PcostTotal = in.readDouble();
        tdpricePerQty = in.readDouble();
        PproductID = in.readLong();
        cdaddOnId = in.readLong();
        bdaddOnId = in.readLong();
        carExists = in.readByte() != 0;
        busExists = in.readByte() != 0;
    }

    public static final Creator<ImagicaPackageModel> CREATOR = new Creator<ImagicaPackageModel>() {
        @Override
        public ImagicaPackageModel createFromParcel(Parcel in) {
            return new ImagicaPackageModel(in);
        }

        @Override
        public ImagicaPackageModel[] newArray(int size) {
            return new ImagicaPackageModel[size];
        }
    };

    public String getPimageURL() {
        return PimageURL;
    }

    public void setPimageURL(String pimageURL) {
        PimageURL = pimageURL;
    }

    public String getPdescription() {
        return Pdescription;
    }

    public void setPdescription(String pdescription) {
        Pdescription = pdescription;
    }

    public String getPtitle() {
        return Ptitle;
    }

    public void setPtitle(String ptitle) {
        Ptitle = ptitle;
    }

    public String getPcurrency() {
        return Pcurrency;
    }

    public void setPcurrency(String pcurrency) {
        Pcurrency = pcurrency;
    }

    public String getPassengerType() {
        return passengerType;
    }

    public void setPassengerType(String passengerType) {
        this.passengerType = passengerType;
    }

    public String getBdaddOneName() {
        return bdaddOneName;
    }

    public void setBdaddOneName(String bdaddOneName) {
        this.bdaddOneName = bdaddOneName;
    }

    public String getCdaddOnName() {
        return cdaddOnName;
    }

    public void setCdaddOnName(String cdaddOnName) {
        this.cdaddOnName = cdaddOnName;
    }

    public String getTdname() {
        return tdname;
    }

    public void setTdname(String tdname) {
        this.tdname = tdname;
    }

    public int getPquantity() {
        return Pquantity;
    }

    public void setPquantity(int pquantity) {
        Pquantity = pquantity;
    }

    public int getTdquantity() {
        return tdquantity;
    }

    public void setTdquantity(int tdquantity) {
        this.tdquantity = tdquantity;
    }

    public int getBdquantity() {
        return bdquantity;
    }

    public void setBdquantity(int bdquantity) {
        this.bdquantity = bdquantity;
    }

    public int getCdquantity() {
        return cdquantity;
    }

    public void setCdquantity(int cdquantity) {
        this.cdquantity = cdquantity;
    }

    public double getPcostPer() {
        return PcostPer;
    }

    public void setPcostPer(double pcostPer) {
        PcostPer = pcostPer;
    }

    public double getPcostTotal() {
        return PcostTotal;
    }

    public void setPcostTotal(double pcostTotal) {
        PcostTotal = pcostTotal;
    }

    public double getTdpricePerQty() {
        return tdpricePerQty;
    }

    public void setTdpricePerQty(double tdpricePerQty) {
        this.tdpricePerQty = tdpricePerQty;
    }

    public long getPproductID() {
        return PproductID;
    }

    public void setPproductID(long pproductID) {
        PproductID = pproductID;
    }

    public long getCdaddOnId() {
        return cdaddOnId;
    }

    public void setCdaddOnId(long cdaddOnId) {
        this.cdaddOnId = cdaddOnId;
    }

    public long getBdaddOnId() {
        return bdaddOnId;
    }

    public void setBdaddOnId(long bdaddOnId) {
        this.bdaddOnId = bdaddOnId;
    }

    public boolean isCarExists() {
        return carExists;
    }

    public void setCarExists(boolean carExists) {
        this.carExists = carExists;
    }

    public boolean isBusExists() {
        return busExists;
    }

    public void setBusExists(boolean busExists) {
        this.busExists = busExists;
    }


    public List<TaxDetailList> taxDetailList() {
        return taxDetailListList;
    }

    public void setTaxDetailList(ArrayList  <TaxDetailList> taxDetailList) {
        this.taxDetailListList = taxDetailList;

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(PimageURL);
        dest.writeString(Pdescription);
        dest.writeString(Ptitle);
        dest.writeString(Pcurrency);
        dest.writeString(passengerType);
        dest.writeString(bdaddOneName);
        dest.writeString(cdaddOnName);
        dest.writeString(tdname);
        dest.writeInt(Pquantity);
        dest.writeInt(tdquantity);
        dest.writeInt(bdquantity);
        dest.writeInt(cdquantity);
        dest.writeDouble(PcostPer);
        dest.writeDouble(PcostTotal);
        dest.writeDouble(tdpricePerQty);
        dest.writeLong(PproductID);
        dest.writeLong(cdaddOnId);
        dest.writeLong(bdaddOnId);
        dest.writeByte((byte) (carExists ? 1 : 0));
        dest.writeByte((byte) (busExists ? 1 : 0));
    }

    public static class TaxDetailList{
        private String name;
        private double pricePerQty;

        public TaxDetailList(){

        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public double getPricePerQty() {
            return pricePerQty;
        }

        public void setPricePerQty(double pricePerQty) {
            this.pricePerQty = pricePerQty;
        }
    }
}
