package in.MadMoveGlobal.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Ksf on 5/8/2016.
 */
public class FingoolHistoryModel implements Parcelable {
    public String name;
    public String amountPaid;
    public String coi;
    public String transactionNo;
    public String date;
    public String mobile;


    public FingoolHistoryModel(String name, String amountPaid, String coi, String transactionNo, String date, String mobile) {
        this.name = name;
        this.amountPaid = amountPaid;
        this.coi = coi;
        this.transactionNo = transactionNo;
        this.date = date;
        this.mobile = mobile;

    }

    protected FingoolHistoryModel(Parcel in) {
        name = in.readString();
        amountPaid = in.readString();
        coi = in.readString();
        transactionNo = in.readString();
        date = in.readString();
        mobile = in.readString();

    }

    public static final Creator<FingoolHistoryModel> CREATOR = new Creator<FingoolHistoryModel>() {
        @Override
        public FingoolHistoryModel createFromParcel(Parcel in) {
            return new FingoolHistoryModel(in);
        }

        @Override
        public FingoolHistoryModel[] newArray(int size) {
            return new FingoolHistoryModel[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(String amountPaid) {
        this.amountPaid = amountPaid;
    }

    public String getCoi() {
        return coi;
    }

    public void setCoi(String coi) {
        this.coi = coi;
    }

    public String getTransactionNo() {
        return transactionNo;
    }

    public void setTransactionNo(String transactionNo) {
        this.transactionNo = transactionNo;
    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeString(amountPaid);
        parcel.writeString(coi);
        parcel.writeString(transactionNo);
        parcel.writeString(date);
        parcel.writeString(mobile);

    }
}
