package in.MadMoveGlobal.model;

import com.orm.SugarRecord;

/**
 * Created by Ksf on 3/27/2016.
 */
public class MerchantModel extends SugarRecord {

    private long merchantId;
    private String merchantName;
    private String merchantContact;
    private String merchantEmail;
    private String merchantImage;


    public MerchantModel() {

    }

    public MerchantModel(long merchantId, String merchantName, String merchantContact, String merchantEmail, String merchantImage) {
        this.merchantId = merchantId;
        this.merchantName = merchantName;
        this.merchantContact = merchantContact;
        this.merchantEmail = merchantEmail;
        this.merchantImage = merchantImage;

    }

    public long getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(long merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantContact() {
        return merchantContact;
    }

    public void setMerchantContact(String merchantContact) {
        this.merchantContact = merchantContact;
    }

    public String getMerchantEmail() {
        return merchantEmail;
    }

    public void setMerchantEmail(String merchantEmail) {
        this.merchantEmail = merchantEmail;
    }

    public String getMerchantImage() {
        return merchantImage;
    }

    public void setMerchantImage(String merchantImage) {
        this.merchantImage = merchantImage;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }


}
