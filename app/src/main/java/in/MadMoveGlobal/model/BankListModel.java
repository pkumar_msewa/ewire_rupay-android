package in.MadMoveGlobal.model;

import com.orm.SugarRecord;

/**
 * Created by Ksf on 9/8/2016.
 */
public class BankListModel extends SugarRecord{
    private String bankCode;
    private String bankName;

    public BankListModel(){

    }

    public BankListModel(String bankCode, String bankName){
        this.bankCode = bankCode;
        this.bankName = bankName;

    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }
}
