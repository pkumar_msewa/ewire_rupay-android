package in.MadMoveGlobal.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.orm.SugarRecord;

/**
 * Created by Msewa-Admin on 6/5/2017.
 */

public class PQCartModel extends SugarRecord implements Parcelable {
//    private long price;
//    private String status;
//    private long totalprice;
//    private long userId;
//    private String fileName;
//    private long cartId;
//    private long quantity;
//    private String productName;
//    private long productId;
//
//    public PQCartModel(long price, String status, long totalprice, long userId, String fileName, long cartId, long quantity, String productName, long productId) {
//        this.price = price;
//        this.status = status;
//        this.totalprice = totalprice;
//        this.userId = userId;
//        this.fileName = fileName;
//        this.cartId = cartId;
//        this.quantity = quantity;
//        this.productName = productName;
//        this.productId = productId;
//    }
//
//    protected PQCartModel(Parcel in) {
//        price = in.readLong();
//        status = in.readString();
//        totalprice = in.readLong();
//        userId = in.readLong();
//        fileName = in.readString();
//        cartId = in.readLong();
//        quantity = in.readLong();
//        productName = in.readString();
//        productId = in.readLong();
//    }
//
//    public static final Creator<PQCartModel> CREATOR = new Creator<PQCartModel>() {
//        @Override
//        public PQCartModel createFromParcel(Parcel in) {
//            return new PQCartModel(in);
//        }
//
//        @Override
//        public PQCartModel[] newArray(int size) {
//            return new PQCartModel[size];
//        }
//    };
//
//    public long getPrice() {
//        return price;
//    }
//
//    public void setPrice(long price) {
//        this.price = price;
//    }
//
//    public String getStatus() {
//        return status;
//    }
//
//    public void setStatus(String status) {
//        this.status = status;
//    }
//
//    public long getTotalprice() {
//        return totalprice;
//    }
//
//    public void setTotalprice(long totalprice) {
//        this.totalprice = totalprice;
//    }
//
//    public long getUserId() {
//        return userId;
//    }
//
//    public void setUserId(long userId) {
//        this.userId = userId;
//    }
//
//    public String getFileName() {
//        return fileName;
//    }
//
//    public void setFileName(String fileName) {
//        this.fileName = fileName;
//    }
//
//    public long getCartId() {
//        return cartId;
//    }
//
//    public void setCartId(long cartId) {
//        this.cartId = cartId;
//    }
//
//    public long getQuantity() {
//        return quantity;
//    }
//
//    public void setQuantity(long quantity) {
//        this.quantity = quantity;
//    }
//
//    public String getProductName() {
//        return productName;
//    }
//
//    public void setProductName(String productName) {
//        this.productName = productName;
//    }
//
//    public long getProductId() {
//        return productId;
//    }
//
//    public void setProductId(long productId) {
//        this.productId = productId;
//    }
//
//    @Override
//    public int describeContents() {
//        return 0;
//    }
//
//    @Override
//    public void writeToParcel(Parcel dest, int flags) {
//        dest.writeLong(price);
//        dest.writeString(status);
//        dest.writeLong(totalprice);
//        dest.writeLong(userId);
//        dest.writeString(fileName);
//        dest.writeLong(cartId);
//        dest.writeLong(quantity);
//        dest.writeString(productName);
//        dest.writeLong(productId);
//    }
//}


    private long pid;
    private String pName;
    private String pDesc;
    private long pPrice;
    private String pImage;
    private String pStatus;
    private long pWeight;
    private String pCat;
    private String pSubCat;
    private String pBrand;

    public PQCartModel(long pid, String pName, String pDesc, long pPrice, String pImage, String pStatus, long pWeight, String pCat, String pSubCat, String pBrand) {
        this.pid = pid;
        this.pName = pName;
        this.pDesc = pDesc;
        this.pPrice = pPrice;
        this.pImage = pImage;
        this.pStatus = pStatus;
        this.pWeight = pWeight;
        this.pCat = pCat;
        this.pSubCat = pSubCat;
        this.pBrand = pBrand;
    }

    public long getPid() {
        return pid;
    }

    public void setPid(long pid) {
        this.pid = pid;
    }

    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }

    public String getpDesc() {
        return pDesc;
    }

    public void setpDesc(String pDesc) {
        this.pDesc = pDesc;
    }

    public long getpPrice() {
        return pPrice;
    }

    public void setpPrice(long pPrice) {
        this.pPrice = pPrice;
    }

    public String getpImage() {
        return pImage;
    }

    public void setpImage(String pImage) {
        this.pImage = pImage;
    }


    public String getpStatus() {
        return pStatus;
    }

    public void setpStatus(String pStatus) {
        this.pStatus = pStatus;
    }

    public long getpWeight() {
        return pWeight;
    }

    public void setpWeight(long pWeight) {
        this.pWeight = pWeight;
    }

    public String getpCat() {
        return pCat;
    }

    public void setpCat(String pCat) {
        this.pCat = pCat;
    }

    public String getpSubCat() {
        return pSubCat;
    }

    public void setpSubCat(String pSubCat) {
        this.pSubCat = pSubCat;
    }

    public String getpBrand() {
        return pBrand;
    }

    public void setpBrand(String pBrand) {
        this.pBrand = pBrand;
    }

    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.pid);
        dest.writeString(this.pName);
        dest.writeString(this.pDesc);
        dest.writeLong(this.pPrice);
        dest.writeString(this.pImage);
        dest.writeString(this.pStatus);
        dest.writeLong(this.pWeight);
        dest.writeString(this.pCat);
        dest.writeString(this.pSubCat);
        dest.writeString(this.pBrand);

    }

    protected PQCartModel(Parcel in) {
        this.pid = in.readLong();
        this.pName = in.readString();
        this.pDesc = in.readString();
        this.pPrice = in.readLong();
        this.pImage = in.readString();
        this.pStatus = in.readString();
        this.pWeight = in.readLong();
        this.pCat = in.readString();
        this.pSubCat = in.readString();
        this.pBrand = in.readString();

    }

    public static final Creator<PQCartModel> CREATOR = new Creator<PQCartModel>() {
        @Override
        public PQCartModel createFromParcel(Parcel source) {
            return new PQCartModel(source);
        }

        @Override
        public PQCartModel[] newArray(int size) {
            return new PQCartModel[size];
        }
    };
}
