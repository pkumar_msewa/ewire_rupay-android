package in.MadMoveGlobal.model;

/**
 * Created by Ksf on 8/3/2015.
 */
public class NavDrawerHeader {
    private String userName;
    private String userImage;
    private String userPoints;


    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPoints() {
        return userPoints;
    }

    public void setUserPoints(String userPoints) {
        this.userPoints = userPoints;
    }



    public NavDrawerHeader() {
    }


}
