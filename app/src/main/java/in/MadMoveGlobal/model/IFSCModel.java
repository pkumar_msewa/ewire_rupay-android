package in.MadMoveGlobal.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.orm.SugarRecord;

public class IFSCModel extends SugarRecord implements Parcelable {

	public String accountNo;
	public String name;
	public String ifscNo;


   public IFSCModel(){

   }

	public IFSCModel(String accountNo, String name, String ifscNo) {
		this.accountNo = accountNo;
		this.name = name;
		this.ifscNo = ifscNo;


    }

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIfscNo() {
		return ifscNo;
	}

	public void setIfscNo(String ifscNo) {
		this.ifscNo = ifscNo;
	}






    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.accountNo);
        dest.writeString(this.name);
        dest.writeString(this.ifscNo);

    }

    protected IFSCModel(Parcel in) {
        this.accountNo = in.readString();
        this.name = in.readString();
        this.ifscNo = in.readString();

    }

    public static final Creator<IFSCModel> CREATOR = new Creator<IFSCModel>() {
        @Override
        public IFSCModel createFromParcel(Parcel source) {
            return new IFSCModel(source);
        }

        @Override
        public IFSCModel[] newArray(int size) {
            return new IFSCModel[size];
        }
    };
}
