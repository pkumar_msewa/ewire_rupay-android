package in.MadMoveGlobal.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Azhar on 8/22/2017.
 */

public class BusBookedTicketModel implements Parcelable {

    private String userMobile;
    private String userEmail;
    private String ticketPnr;
    private String operatorPnr;
    private String emtTxnId;
    private String busId;
    private double totalFare;
    private String journeyDate;
    private String source;
    private String destination;
    private String boardingId;
    private String boardingAddress;
    private String busOperator;
    private String arrTime;
    private String deptTime;
    private ArrayList<BusPassengerModel> busPassengerList;
    private String oneway, roundway;

    public BusBookedTicketModel(String userMobile, String userEmail, String ticketPnr, String operatorPnr, String emtTxnId, String busId, double totalFare, String journeyDate, String source, String destination, String boardingId, String boardingAddress, String busOperator, String arrTime, String deptTime, ArrayList<BusPassengerModel> busPassengerList, String oneway, String roundway) {
        this.userMobile = userMobile;
        this.userEmail = userEmail;
        this.ticketPnr = ticketPnr;
        this.operatorPnr = operatorPnr;
        this.emtTxnId = emtTxnId;
        this.busId = busId;
        this.totalFare = totalFare;
        this.journeyDate = journeyDate;
        this.source = source;
        this.destination = destination;
        this.boardingId = boardingId;
        this.boardingAddress = boardingAddress;
        this.busOperator = busOperator;
        this.arrTime = arrTime;
        this.deptTime = deptTime;
        this.busPassengerList = busPassengerList;
        this.oneway = oneway;
        this.roundway = roundway;
    }

    protected BusBookedTicketModel(Parcel in) {
        userMobile = in.readString();
        userEmail = in.readString();
        ticketPnr = in.readString();
        operatorPnr = in.readString();
        emtTxnId = in.readString();
        busId = in.readString();
        totalFare = in.readDouble();
        journeyDate = in.readString();
        source = in.readString();
        destination = in.readString();
        boardingId = in.readString();
        boardingAddress = in.readString();
        busOperator = in.readString();
        arrTime = in.readString();
        deptTime = in.readString();
        busPassengerList = in.createTypedArrayList(BusPassengerModel.CREATOR);
        oneway = in.readString();
        roundway = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userMobile);
        dest.writeString(userEmail);
        dest.writeString(ticketPnr);
        dest.writeString(operatorPnr);
        dest.writeString(emtTxnId);
        dest.writeString(busId);
        dest.writeDouble(totalFare);
        dest.writeString(journeyDate);
        dest.writeString(source);
        dest.writeString(destination);
        dest.writeString(boardingId);
        dest.writeString(boardingAddress);
        dest.writeString(busOperator);
        dest.writeString(arrTime);
        dest.writeString(deptTime);
        dest.writeTypedList(busPassengerList);
        dest.writeString(oneway);
        dest.writeString(roundway);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BusBookedTicketModel> CREATOR = new Creator<BusBookedTicketModel>() {
        @Override
        public BusBookedTicketModel createFromParcel(Parcel in) {
            return new BusBookedTicketModel(in);
        }

        @Override
        public BusBookedTicketModel[] newArray(int size) {
            return new BusBookedTicketModel[size];
        }
    };

    public String getUserMobile() {
        return userMobile;
    }

    public void setUserMobile(String userMobile) {
        this.userMobile = userMobile;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getTicketPnr() {
        return ticketPnr;
    }

    public void setTicketPnr(String ticketPnr) {
        this.ticketPnr = ticketPnr;
    }

    public String getOperatorPnr() {
        return operatorPnr;
    }

    public void setOperatorPnr(String operatorPnr) {
        this.operatorPnr = operatorPnr;
    }

    public String getEmtTxnId() {
        return emtTxnId;
    }

    public void setEmtTxnId(String emtTxnId) {
        this.emtTxnId = emtTxnId;
    }

    public String getBusId() {
        return busId;
    }

    public void setBusId(String busId) {
        this.busId = busId;
    }

    public double getTotalFare() {
        return totalFare;
    }

    public void setTotalFare(double totalFare) {
        this.totalFare = totalFare;
    }

    public String getJourneyDate() {
        return journeyDate;
    }

    public void setJourneyDate(String journeyDate) {
        this.journeyDate = journeyDate;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getBoardingId() {
        return boardingId;
    }

    public void setBoardingId(String boardingId) {
        this.boardingId = boardingId;
    }

    public String getBoardingAddress() {
        return boardingAddress;
    }

    public void setBoardingAddress(String boardingAddress) {
        this.boardingAddress = boardingAddress;
    }

    public String getBusOperator() {
        return busOperator;
    }

    public void setBusOperator(String busOperator) {
        this.busOperator = busOperator;
    }

    public String getArrTime() {
        return arrTime;
    }

    public void setArrTime(String arrTime) {
        this.arrTime = arrTime;
    }

    public String getDeptTime() {
        return deptTime;
    }

    public void setDeptTime(String deptTime) {
        this.deptTime = deptTime;
    }

    public ArrayList<BusPassengerModel> getBusPassengerList() {
        return busPassengerList;
    }

    public void setBusPassengerList(ArrayList<BusPassengerModel> busPassengerList) {
        this.busPassengerList = busPassengerList;
    }

    public String getOneway() {
        return oneway;
    }

    public void setOneway(String oneway) {
        this.oneway = oneway;
    }

    public String getRoundway() {
        return roundway;
    }

    public void setRoundway(String roundway) {
        this.roundway = roundway;
    }
}
