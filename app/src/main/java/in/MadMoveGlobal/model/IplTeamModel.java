package in.MadMoveGlobal.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Kashif-PC on 3/31/2017.
 */
public class IplTeamModel implements Parcelable {

    private int matchId;
    private String matchName;
    private String matchDate;
    private Venue venue;
    private Team team1;
    private Team team2;


    public IplTeamModel(int matchId, String matchName, String matchDate, Venue venue, Team team1, Team team2) {
        this.matchId = matchId;
        this.matchName = matchName;
        this.matchDate = matchDate;
        this.venue = venue;
        this.team1 = team1;
        this.team2 = team2;

    }

    private IplTeamModel(Parcel in) {
        matchDate = in.readString();
        matchId = in.readInt();
        matchName = in.readString();
    }

    public static final Creator<IplTeamModel> CREATOR = new Creator<IplTeamModel>() {
        @Override
        public IplTeamModel createFromParcel(Parcel in) {
            return new IplTeamModel(in);
        }

        @Override
        public IplTeamModel[] newArray(int size) {
            return new IplTeamModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(matchDate);
        parcel.writeInt(matchId);
        parcel.writeString(matchName);

    }

    public int getMatchId() {
        return matchId;
    }

    public void setMatchId(int matchId) {
        this.matchId = matchId;
    }

    public String getMatchName() {
        return matchName;
    }

    public void setMatchName(String matchName) {
        this.matchName = matchName;
    }

    public String getMatchDate() {
        return matchDate;
    }

    public void setMatchDate(String matchDate) {
        this.matchDate = matchDate;
    }

    public Venue getVenue() {
        return venue;
    }

    public void setVenue(Venue venue) {
        this.venue = venue;
    }

    public Team getTeam1() {
        return team1;
    }

    public void setTeam1(Team team1) {
        this.team1 = team1;
    }

    public Team getTeam2() {
        return team2;
    }

    public void setTeam2(Team team2) {
        this.team2 = team2;
    }




}
