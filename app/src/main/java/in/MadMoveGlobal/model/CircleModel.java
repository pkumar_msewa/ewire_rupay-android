package in.MadMoveGlobal.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.orm.SugarRecord;

public class CircleModel extends SugarRecord implements Parcelable {

	public String code;
	public String name;


	public CircleModel(){

    }

    public CircleModel(String code, String name) {
		this.code = code;
		this.name = name;
	}

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.code);
        dest.writeString(this.name);
    }

    protected CircleModel(Parcel in) {
        this.code = in.readString();
        this.name = in.readString();
    }

    public static final Creator<CircleModel> CREATOR = new Creator<CircleModel>() {
        @Override
        public CircleModel createFromParcel(Parcel source) {
            return new CircleModel(source);
        }

        @Override
        public CircleModel[] newArray(int size) {
            return new CircleModel[size];
        }
    };
}
