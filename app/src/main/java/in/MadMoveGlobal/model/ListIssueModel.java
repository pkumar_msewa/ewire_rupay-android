package in.MadMoveGlobal.model;

import com.orm.SugarRecord;

/**
 * Created by Ksf on 3/16/2016.
 */
public class ListIssueModel extends SugarRecord {
    private long Id;
    private String ticketNo;
    private String description;
    private String status;
    private boolean New;
    private String component;
    private long date;

    public ListIssueModel(long id, String ticketNo, String description, String status, boolean New, String component, long date) {
        Id = id;
        this.ticketNo = ticketNo;
        this.description = description;
        this.status = status;
        this.New = New;
        this.component=component;
        this.date=date;
    }

    @Override
    public Long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public String getTicketNo() {
        return ticketNo;
    }

    public void setTicketNo(String ticketNo) {
        this.ticketNo = ticketNo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isNew() {
        return New;
    }

    public void setNew(boolean aNew) {
        New = New;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String status) {
        this.component = component;
    }

    public long getDate() {
        return date;
    }

    public void setdate(String status) {
        this.date = date;
    }
}



