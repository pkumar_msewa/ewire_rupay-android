package in.MadMoveGlobal.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kashifimam on 21/02/17.
 */

public class GiftCardCatModel implements Parcelable {
  private String product_id;
  private String brand_id;
  private String brand_name;
  private String images;
  private String min_custom_price;
  private String max_custom_price;
  private String custom_denominations;
  private String sku;
  private String product_type;
  private String paywithamazon_disable;
  private String tnc_mobile;
  private String order_handling_charge;
  private String themes;

  public GiftCardCatModel(String product_id, String brand_id, String brand_name, String images, String min_custom_price, String max_custom_price, String custom_denominations, String sku, String product_type, String paywithamazon_disable, String tnc_mobile, String order_handling_charge, String themes) {
    this.product_id = product_id;
    this.brand_id = brand_id;
    this.brand_name = brand_name;
    this.images = images;
    this.min_custom_price = min_custom_price;
    this.max_custom_price = max_custom_price;
    this.custom_denominations = custom_denominations;
    this.sku = sku;
    this.product_type = product_type;
    this.paywithamazon_disable = paywithamazon_disable;
    this.tnc_mobile = tnc_mobile;
    this.order_handling_charge = order_handling_charge;
    this.themes = themes;
  }

  protected GiftCardCatModel(Parcel in) {
    product_id = in.readString();
    brand_id = in.readString();
    brand_name = in.readString();
    images = in.readString();
    min_custom_price = in.readString();
    max_custom_price = in.readString();
    custom_denominations = in.readString();
    sku = in.readString();
    product_type = in.readString();
    paywithamazon_disable = in.readString();
    tnc_mobile = in.readString();
    order_handling_charge = in.readString();
    themes = in.readString();
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(product_id);
    dest.writeString(brand_id);
    dest.writeString(brand_name);
    dest.writeString(images);
    dest.writeString(min_custom_price);
    dest.writeString(max_custom_price);
    dest.writeString(custom_denominations);
    dest.writeString(sku);
    dest.writeString(product_type);
    dest.writeString(paywithamazon_disable);
    dest.writeString(tnc_mobile);
    dest.writeString(order_handling_charge);
    dest.writeString(themes);
  }

  @Override
  public int describeContents() {
    return 0;
  }

  public static final Creator<GiftCardCatModel> CREATOR = new Creator<GiftCardCatModel>() {
    @Override
    public GiftCardCatModel createFromParcel(Parcel in) {
      return new GiftCardCatModel(in);
    }

    @Override
    public GiftCardCatModel[] newArray(int size) {
      return new GiftCardCatModel[size];
    }
  };

  public String getProduct_id() {
    return product_id;
  }

  public void setProduct_id(String product_id) {
    this.product_id = product_id;
  }

  public String getBrand_id() {
    return brand_id;
  }

  public void setBrand_id(String brand_id) {
    this.brand_id = brand_id;
  }

  public String getBrand_name() {
    return brand_name;
  }

  public void setBrand_name(String brand_name) {
    this.brand_name = brand_name;
  }

  public String getImages() {
    return images;
  }

  public void setImages(String images) {
    this.images = images;
  }

  public String getMin_custom_price() {
    return min_custom_price;
  }

  public void setMin_custom_price(String min_custom_price) {
    this.min_custom_price = min_custom_price;
  }

  public String getMax_custom_price() {
    return max_custom_price;
  }

  public void setMax_custom_price(String max_custom_price) {
    this.max_custom_price = max_custom_price;
  }

  public String getCustom_denominations() {
    return custom_denominations;
  }

  public void setCustom_denominations(String custom_denominations) {
    this.custom_denominations = custom_denominations;
  }

  public String getSku() {
    return sku;
  }

  public void setSku(String sku) {
    this.sku = sku;
  }

  public String getProduct_type() {
    return product_type;
  }

  public void setProduct_type(String product_type) {
    this.product_type = product_type;
  }

  public String getPaywithamazon_disable() {
    return paywithamazon_disable;
  }

  public void setPaywithamazon_disable(String paywithamazon_disable) {
    this.paywithamazon_disable = paywithamazon_disable;
  }

  public String getTnc_mobile() {
    return tnc_mobile;
  }

  public void setTnc_mobile(String tnc_mobile) {
    this.tnc_mobile = tnc_mobile;
  }

  public String getOrder_handling_charge() {
    return order_handling_charge;
  }

  public void setOrder_handling_charge(String order_handling_charge) {
    this.order_handling_charge = order_handling_charge;
  }

  public String getThemes() {
    return themes;
  }

  public void setThemes(String themes) {
    this.themes = themes;
  }
}
