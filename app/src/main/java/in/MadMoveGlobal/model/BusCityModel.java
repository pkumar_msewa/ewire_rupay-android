package in.MadMoveGlobal.model;

import com.orm.SugarRecord;

/**
 * Created by Ksf on 9/29/2016.
 */
public class BusCityModel extends SugarRecord {
    private long cityId;
    private String cityname;

    public BusCityModel() {

    }

    public BusCityModel(long cityId, String cityname) {

        this.cityId = cityId;
        this.cityname = cityname;

    }

    public long getCityId() {
        return cityId;
    }

    public void setCityId(long cityId) {
        this.cityId = cityId;
    }

    public String getCityname() {
        return cityname;
    }

    public void setCityname(String cityname) {
        this.cityname = cityname;
    }
}
