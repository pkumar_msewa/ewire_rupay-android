package in.MadMoveGlobal.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Ksf on 5/8/2016.
 */
public class OrderModel implements Parcelable{
    public String transactionNumber;
    public String amountPaid;
    public String serviceStatus;
    public String serviceName;
    public String image;
    public String date;
    public String description;


    public OrderModel(String transactionNumber, String amountPaid, String serviceStatus, String serviceName, String image,String date,String description) {
        this.transactionNumber = transactionNumber;
        this.amountPaid = amountPaid;
        this.serviceStatus = serviceStatus;
        this.serviceName = serviceName;
        this.image = image;
        this.date = date;
        this.description = description;

    }

    protected OrderModel(Parcel in) {
        transactionNumber = in.readString();
        amountPaid = in.readString();
        serviceStatus = in.readString();
        serviceName = in.readString();
        image = in.readString();
        date = in.readString();
        description = in.readString();

    }

    public static final Creator<OrderModel> CREATOR = new Creator<OrderModel>() {
        @Override
        public OrderModel createFromParcel(Parcel in) {
            return new OrderModel(in);
        }

        @Override
        public OrderModel[] newArray(int size) {
            return new OrderModel[size];
        }
    };

    public String getTransactionNumber() {
        return transactionNumber;
    }

    public void setTransactionNumber(String transactionNumber) {
        this.transactionNumber = transactionNumber;
    }

    public String getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(String amountPaid) {
        this.amountPaid = amountPaid;
    }

    public String getServiceStatus() {
        return serviceStatus;
    }

    public void setServiceStatus(String serviceStatus) {
        this.serviceStatus = serviceStatus;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(transactionNumber);
        parcel.writeString(amountPaid);
        parcel.writeString(serviceStatus);
        parcel.writeString(serviceName);
        parcel.writeString(image);
        parcel.writeString(date);
        parcel.writeString(description);

    }
}
