package in.MadMoveGlobal.model;


import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.util.ServiceUtility;

public class PQTelebuyPaymentHolder {

	/*
	 * Session Details
	 */
	private volatile static PQTelebuyPaymentHolder uniqueInstance;
	public static final String ACCESS_CODE = "AVKQ03BK16BA20QKAB";
	public static final String MERCHANT_ID = "167";
	public static final String CURRENCY = "INR";
	// URL_BASE

	public static final String REDIRECT_URL = ApiUrl.URL_IMAGE_MAIN + "payments/ccavResponseHandler.aspx";
	public static final String CANCEL_URL = ApiUrl.URL_IMAGE_MAIN+ "payments/TranCancelled.aspx";
	public static final String RSA_URL = ApiUrl.URL_IMAGE_MAIN + "payments/GetRSA.aspx";
	
	private String amount;
	private String orderId;
	private String contactAddressId;
	private String name;
	private String address;
	private String countryId;
	private String stateId;
	private String state;
	private String cityId;
	private String city;
	private String zip;
	private String telephone;
	private String emailId;
	
	private String paymentOption;
	private String cardType;
	
	private String orderRef;
	
	/*
	 * Singleton
	 */
	private PQTelebuyPaymentHolder() {}
 
	/*
	 * Singleton getInstance()
	 */
	public static PQTelebuyPaymentHolder getInstance() {
		if (uniqueInstance == null) {
			synchronized (PQTelebuyPaymentHolder.class) {
				if (uniqueInstance == null) {
					uniqueInstance = new PQTelebuyPaymentHolder();
				}
			}
		}
		return uniqueInstance;
	}
	
	public void createOrderId() {
		Integer randomNum = ServiceUtility.randInt(0, 9999999);
		setOrderId(randomNum.toString());
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getOrderId() {
		return orderId;
	}

	private void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPaymentOption() {
		return paymentOption;
	}

	public void setPaymentOption(String paymentOption) {
		this.paymentOption = paymentOption;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getStateId() {
		return stateId;
	}

	public void setStateId(String stateId) {
		this.stateId = stateId;
	}

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public String getContactAddressId() {
		return contactAddressId;
	}

	public void setContactAddressId(String contactAddressId) {
		this.contactAddressId = contactAddressId;
	}

	public String getCountryId() {
		return countryId;
	}

	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}

	public String getOrderRef() {
		return orderRef;
	}

	public void setOrderRef(String orderRef) {
		this.orderRef = orderRef;
	}
	
}
