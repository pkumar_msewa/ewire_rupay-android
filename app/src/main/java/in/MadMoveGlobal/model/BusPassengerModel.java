package in.MadMoveGlobal.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Azhar on 8/22/2017.
 */

public class BusPassengerModel implements Parcelable {

    private String fName;
    private String lName;
    private String age;
    private String gender;
    private String seatNo;
    private String seatType;
    private String fare;

    public BusPassengerModel(String fName, String lName, String age, String gender, String seatNo, String seatType, String fare) {
        this.fName = fName;
        this.lName = lName;
        this.age = age;
        this.gender = gender;
        this.seatNo = seatNo;
        this.seatType = seatType;
        this.fare = fare;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getSeatNo() {
        return seatNo;
    }

    public void setSeatNo(String seatNo) {
        this.seatNo = seatNo;
    }

    public String getSeatType() {
        return seatType;
    }

    public void setSeatType(String seatType) {
        this.seatType = seatType;
    }

    public String getFare() {
        return fare;
    }

    public void setFare(String fare) {
        this.fare = fare;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.fName);
        dest.writeString(this.lName);
        dest.writeString(this.age);
        dest.writeString(this.gender);
        dest.writeString(this.seatNo);
        dest.writeString(this.seatType);
        dest.writeString(this.fare);
    }

    protected BusPassengerModel(Parcel in) {
        fName = in.readString();
        lName = in.readString();
        age = in.readString();
        gender = in.readString();
        seatNo = in.readString();
        seatType = in.readString();
        fare = in.readString();
    }

    public static final Creator<BusPassengerModel> CREATOR = new Creator<BusPassengerModel>() {
        @Override
        public BusPassengerModel createFromParcel(Parcel in) {
            return new BusPassengerModel(in);
        }

        @Override
        public BusPassengerModel[] newArray(int size) {
            return new BusPassengerModel[size];
        }
    };
}
