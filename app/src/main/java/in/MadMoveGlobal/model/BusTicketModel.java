package in.MadMoveGlobal.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Kashif-PC on 11/24/2016.
 */
public class BusTicketModel implements Parcelable {
    private String bookingDate;
    private String bookingContact;
    private String bookingEmail;
    private String bookingReferenceNo;
    private String bookingPNRNo;
    private String bookingTicketNo;
    private String boardingDetails;
    private String dateOfJourney;
    private String busFrom;
    private String busTo;
    private String busName;
    private String busDepartureTime;
    private String busFare;

    private String passengerTitle;
    private String passengerName;
    private String passengerGender;
    private String passengerAge;
    private String passengerSeats;
    private String passengerFare;

    public BusTicketModel(String bookingDate,String bookingContact,String bookingEmail,String bookingReferenceNo,String bookingPNRNo,String bookingTicketNo,String boardingDetails,String dateOfJourney,String busFrom,String busTo,String busName,String busDepartureTime,String busFare,String passengerTitle,String passengerName,String passengerGender,String passengerAge,String passengerSeats,String passengerFare){
        this.bookingDate = bookingDate;
        this.bookingContact = bookingContact;
        this.bookingEmail = bookingEmail;
        this.bookingReferenceNo = bookingReferenceNo;
        this.bookingPNRNo = bookingPNRNo;
        this.bookingTicketNo = bookingTicketNo;
        this.boardingDetails = boardingDetails;
        this.dateOfJourney = dateOfJourney;
        this.busFrom = busFrom;
        this.busTo = busTo;
        this.busName = busName;
        this.busDepartureTime = busDepartureTime;
        this.busFare = busFare;
        this.passengerTitle = passengerTitle;
        this.passengerName = passengerName;
        this.passengerGender = passengerGender;
        this.passengerAge = passengerAge;
        this.passengerSeats = passengerSeats;
        this.passengerFare = passengerFare;
    }

    public String getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    public String getBookingContact() {
        return bookingContact;
    }

    public void setBookingContact(String bookingContact) {
        this.bookingContact = bookingContact;
    }

    public String getBookingEmail() {
        return bookingEmail;
    }

    public void setBookingEmail(String bookingEmail) {
        this.bookingEmail = bookingEmail;
    }

    public String getBookingReferenceNo() {
        return bookingReferenceNo;
    }

    public void setBookingReferenceNo(String bookingReferenceNo) {
        this.bookingReferenceNo = bookingReferenceNo;
    }

    public String getBookingPNRNo() {
        return bookingPNRNo;
    }

    public void setBookingPNRNo(String bookingPNRNo) {
        this.bookingPNRNo = bookingPNRNo;
    }

    public String getBookingTicketNo() {
        return bookingTicketNo;
    }

    public void setBookingTicketNo(String bookingTicketNo) {
        this.bookingTicketNo = bookingTicketNo;
    }

    public String getBoardingDetails() {
        return boardingDetails;
    }

    public void setBoardingDetails(String boardingDetails) {
        this.boardingDetails = boardingDetails;
    }

    public String getDateOfJourney() {
        return dateOfJourney;
    }

    public void setDateOfJourney(String dateOfJourney) {
        this.dateOfJourney = dateOfJourney;
    }

    public String getBusFrom() {
        return busFrom;
    }

    public void setBusFrom(String busFrom) {
        this.busFrom = busFrom;
    }

    public String getBusTo() {
        return busTo;
    }

    public void setBusTo(String busTo) {
        this.busTo = busTo;
    }

    public String getBusName() {
        return busName;
    }

    public void setBusName(String busName) {
        this.busName = busName;
    }

    public String getBusDepartureTime() {
        return busDepartureTime;
    }

    public void setBusDepartureTime(String busDepartureTime) {
        this.busDepartureTime = busDepartureTime;
    }

    public String getBusFare() {
        return busFare;
    }

    public void setBusFare(String busFare) {
        this.busFare = busFare;
    }

    public String getPassengerTitle() {
        return passengerTitle;
    }

    public void setPassengerTitle(String passengerTitle) {
        this.passengerTitle = passengerTitle;
    }

    public String getPassengerName() {
        return passengerName;
    }

    public void setPassengerName(String passengerName) {
        this.passengerName = passengerName;
    }

    public String getPassengerGender() {
        return passengerGender;
    }

    public void setPassengerGender(String passengerGender) {
        this.passengerGender = passengerGender;
    }

    public String getPassengerAge() {
        return passengerAge;
    }

    public void setPassengerAge(String passengerAge) {
        this.passengerAge = passengerAge;
    }

    public String getPassengerSeats() {
        return passengerSeats;
    }

    public void setPassengerSeats(String passengerSeats) {
        this.passengerSeats = passengerSeats;
    }

    public String getPassengerFare() {
        return passengerFare;
    }

    public void setPassengerFare(String passengerFare) {
        this.passengerFare = passengerFare;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.bookingDate);
        dest.writeString(this.bookingContact);
        dest.writeString(this.bookingEmail);
        dest.writeString(this.bookingReferenceNo);
        dest.writeString(this.bookingPNRNo);
        dest.writeString(this.bookingTicketNo);
        dest.writeString(this.boardingDetails);
        dest.writeString(this.dateOfJourney);
        dest.writeString(this.busFrom);
        dest.writeString(this.busTo);
        dest.writeString(this.busName);
        dest.writeString(this.busDepartureTime);
        dest.writeString(this.busFare);
        dest.writeString(this.passengerTitle);
        dest.writeString(this.passengerName);
        dest.writeString(this.passengerGender);
        dest.writeString(this.passengerAge);
        dest.writeString(this.passengerSeats);
        dest.writeString(this.passengerFare);
    }

    protected BusTicketModel(Parcel in) {
        this.bookingDate = in.readString();
        this.bookingContact = in.readString();
        this.bookingEmail = in.readString();
        this.bookingReferenceNo = in.readString();
        this.bookingPNRNo = in.readString();
        this.bookingTicketNo = in.readString();
        this.boardingDetails = in.readString();
        this.dateOfJourney = in.readString();
        this.busFrom = in.readString();
        this.busTo = in.readString();
        this.busName = in.readString();
        this.busDepartureTime = in.readString();
        this.busFare = in.readString();
        this.passengerTitle = in.readString();
        this.passengerName = in.readString();
        this.passengerGender = in.readString();
        this.passengerAge = in.readString();
        this.passengerSeats = in.readString();
        this.passengerFare = in.readString();
    }

    public static final Creator<BusTicketModel> CREATOR = new Creator<BusTicketModel>() {
        @Override
        public BusTicketModel createFromParcel(Parcel source) {
            return new BusTicketModel(source);
        }

        @Override
        public BusTicketModel[] newArray(int size) {
            return new BusTicketModel[size];
        }
    };
}
