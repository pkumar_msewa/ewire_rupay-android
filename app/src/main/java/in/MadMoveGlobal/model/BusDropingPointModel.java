package in.MadMoveGlobal.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Kashif-PC on 11/21/2016.
 */
public class BusDropingPointModel implements Parcelable {

    //    {"dpId":"48702","dpName":"Sriperumpudur Toll gate","locatoin":"Toll Gate",
// "prime":"null","dpTime":"04:00","contactNumber":" 9444073636"}
    private String dpPoint,dpName,dpId,dpTime,contactNumber,prime;
    private String locatoin;

    public String getDpPoint() {
        return dpPoint;
    }

    public void setDpPoint(String dpPoint) {
        this.dpPoint = dpPoint;
    }

    public String getDpName() {
        return dpName;
    }

    public void setDpName(String dpName) {
        this.dpName = dpName;
    }

    public String getDpId() {
        return dpId;
    }

    public void setDpId(String dpId) {
        this.dpId = dpId;
    }

    public String getDpTime() {
        return dpTime;
    }

    public void setDpTime(String dpTime) {
        this.dpTime = dpTime;
    }

    public String getLocatoin() {
        return locatoin;
    }

    public void setLocatoin(String locatoin) {
        this.locatoin = locatoin;
    }

    public String getPrime() {
        return prime;
    }

    public void setPrime(String prime) {
        this.prime = prime;
    }

    public String getBdPoint() {
        return dpPoint;
    }

    public void setBdPoint(String bdPoint) {
        this.dpPoint = bdPoint;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public static Creator<BusBoardingPointModel> getCREATOR() {
        return CREATOR;
    }

    protected BusDropingPointModel(Parcel in) {
        dpId = in.readString();
//        dplocation = in.readString();
        dpName = in.readString();
        dpPoint = in.readString();
        prime = in.readString();
        contactNumber = in.readString();
        dpTime = in.readString();
        locatoin = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(locatoin);
        dest.writeString(dpId);
        dest.writeString(dpName);
        dest.writeString(dpPoint);
        dest.writeString(contactNumber);
        dest.writeString(prime);
        dest.writeString(dpTime);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BusBoardingPointModel> CREATOR = new Creator<BusBoardingPointModel>() {
        @Override
        public BusBoardingPointModel createFromParcel(Parcel in) {
            return new BusBoardingPointModel(in);
        }

        @Override
        public BusBoardingPointModel[] newArray(int size) {
            return new BusBoardingPointModel[size];
        }
    };


}
