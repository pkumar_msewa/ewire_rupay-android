package in.MadMoveGlobal.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Kashif-PC on 1/3/2017.
 */
public class EventsModel implements Parcelable {
    private long eventId;
    private long ownerId;
    private String eventTitle;
    private String bannerImage;
    private String thumbImage;
    private String eventStartDate;
    private String eventEndDate;
    private String eventCity;
    private String eventVenue;
    private String categoryName;

    public EventsModel(long eventId,long ownerId, String eventTitle, String bannerImage, String thumbImage, String eventStartDate, String eventEndDate,String eventCity,String eventVenue, String categoryName){
        this.eventId = eventId;
        this.ownerId = ownerId;
        this.eventTitle = eventTitle;
        this.bannerImage = bannerImage;
        this.thumbImage = thumbImage;
        this.eventStartDate = eventStartDate;
        this.eventEndDate = eventEndDate;
        this.eventCity = eventCity;
        this.eventVenue = eventVenue;
        this.categoryName = categoryName;
    }



    public long getEventId() {
        return eventId;
    }

    public void setEventId(long eventId) {
        this.eventId = eventId;
    }

    public long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(long ownerId) {
        this.ownerId = ownerId;
    }

    public String getEventTitle() {
        return eventTitle;
    }

    public void setEventTitle(String eventTitle) {
        this.eventTitle = eventTitle;
    }

    public String getBannerImage() {
        return bannerImage;
    }

    public void setBannerImage(String bannerImage) {
        this.bannerImage = bannerImage;
    }

    public String getThumbImage() {
        return thumbImage;
    }

    public void setThumbImage(String thumbImage) {
        this.thumbImage = thumbImage;
    }

    public String getEventStartDate() {
        return eventStartDate;
    }

    public void setEventStartDate(String eventStartDate) {
        this.eventStartDate = eventStartDate;
    }

    public String getEventEndDate() {
        return eventEndDate;
    }

    public void setEventEndDate(String eventEndDate) {
        this.eventEndDate = eventEndDate;
    }

    public String getEventCity() {
        return eventCity;
    }

    public void setEventCity(String eventCity) {
        this.eventCity = eventCity;
    }

    public String getEventVenue() {
        return eventVenue;
    }

    public void setEventVenue(String eventVenue) {
        this.eventVenue = eventVenue;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.eventId);
        dest.writeLong(this.ownerId);
        dest.writeString(this.eventTitle);
        dest.writeString(this.bannerImage);
        dest.writeString(this.thumbImage);
        dest.writeString(this.eventStartDate);
        dest.writeString(this.eventEndDate);
        dest.writeString(this.eventCity);
        dest.writeString(this.eventVenue);
        dest.writeString(this.categoryName);
    }

    protected EventsModel(Parcel in) {
        this.eventId = in.readLong();
        this.ownerId = in.readLong();
        this.eventTitle = in.readString();
        this.bannerImage = in.readString();
        this.thumbImage = in.readString();
        this.eventStartDate = in.readString();
        this.eventEndDate = in.readString();
        this.eventCity = in.readString();
        this.eventVenue = in.readString();
        this.categoryName = in.readString();
    }

    public static final Creator<EventsModel> CREATOR = new Creator<EventsModel>() {
        @Override
        public EventsModel createFromParcel(Parcel source) {
            return new EventsModel(source);
        }

        @Override
        public EventsModel[] newArray(int size) {
            return new EventsModel[size];
        }
    };
}
