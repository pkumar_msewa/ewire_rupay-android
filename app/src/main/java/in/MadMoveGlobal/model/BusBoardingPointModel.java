package in.MadMoveGlobal.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Kashif-PC on 11/21/2016.
 */
public class BusBoardingPointModel implements Parcelable {

    //{"bdPoint":"#6,Ananth Arcade,24,A.V.Road,Kalasipalyam or http:\/\/zip.pr\/IN\/SPST5453, Kalasipalyam","bdLongName":"#6,Ananth Arcade,24,A.V.Road,Kalasipalyam or http:\/\/zip.pr\/IN\/SPST5453","
    // bdid":"14231","bdlocation":"null","landmark":"Near National Travels or http:\/\/zip.pr\/IN\/SPST5453","prime":"21:00","time":"21:00","contactNumber":"08026707081 8197313131"}

    private String bdPoint,bdLongName,bdid,bdlocation,time,contactNumber,prime;
    private String landmark;

    public String getPrime() {
        return prime;
    }

    public void setPrime(String prime) {
        this.prime = prime;
    }

    public String getBdPoint() {
        return bdPoint;
    }

    public void setBdPoint(String bdPoint) {
        this.bdPoint = bdPoint;
    }

    public String getBdLongName() {
        return bdLongName;
    }

    public void setBdLongName(String bdLongName) {
        this.bdLongName = bdLongName;
    }

    public String getBdid() {
        return bdid;
    }

    public void setBdid(String bdid) {
        this.bdid = bdid;
    }

    public String getBdlocation() {
        return bdlocation;
    }

    public void setBdlocation(String bdlocation) {
        this.bdlocation = bdlocation;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public static Creator<BusBoardingPointModel> getCREATOR() {
        return CREATOR;
    }

    protected BusBoardingPointModel(Parcel in) {
        bdid = in.readString();
        bdlocation = in.readString();
        bdLongName = in.readString();
        bdPoint = in.readString();
        prime = in.readString();
        contactNumber = in.readString();
        time = in.readString();
        landmark = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(landmark);
        dest.writeString(bdid);
        dest.writeString(bdlocation);
        dest.writeString(bdLongName);
        dest.writeString(bdPoint);
        dest.writeString(contactNumber);
        dest.writeString(prime);
        dest.writeString(time);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BusBoardingPointModel> CREATOR = new Creator<BusBoardingPointModel>() {
        @Override
        public BusBoardingPointModel createFromParcel(Parcel in) {
            return new BusBoardingPointModel(in);
        }

        @Override
        public BusBoardingPointModel[] newArray(int size) {
            return new BusBoardingPointModel[size];
        }
    };

    public String getLandmark() {
        return landmark;
    }
    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

}
