package in.MadMoveGlobal.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Ksf on 10/11/2016.
 */
public class FlightListModel implements Parcelable {

    //Fares
    private double flightActualBaseFare;
    private double flightTaxFare;
    private double serviceTax;
    private double serviceCharge;
    private double convenienceCharge;
    private double flightNetFare;
    private double totalFare;
    private String flightProviderCode;

    private String depature;

    private String flightJsonString;

    private ArrayList<FlightModel> flightListArray;
    private ArrayList<FlightModel> flightListRoundArray;
    private String contingFlight;
    private String refund;
    private String duration;


    public FlightListModel(double flightActualBaseFare, double flightTaxFare, double serviceTax, double serviceCharge, double convenienceCharge, double flightNetFare, double totalFare, String flightProviderCode, ArrayList<FlightModel> flightListArray, String flightJsonString, String contingFlight, ArrayList<FlightModel> flightListRoundArray, String refund, String depature, String duration) {
        this.flightActualBaseFare = flightActualBaseFare;
        this.flightTaxFare = flightTaxFare;
        this.serviceTax = serviceTax;
        this.serviceCharge = serviceCharge;
        this.convenienceCharge = convenienceCharge;
        this.flightNetFare = flightNetFare;
        this.totalFare = totalFare;
        this.flightProviderCode = flightProviderCode;
        this.flightListArray = flightListArray;
        this.flightJsonString = flightJsonString;
        this.contingFlight = contingFlight;
        this.flightListRoundArray = flightListRoundArray;
        this.refund = refund;
        this.depature = depature;
        this.duration = duration;
    }

    protected FlightListModel(Parcel in) {
        flightActualBaseFare = in.readDouble();
        flightTaxFare = in.readDouble();
        serviceTax = in.readDouble();
        serviceCharge = in.readDouble();
        convenienceCharge = in.readDouble();
        flightNetFare = in.readDouble();
        totalFare = in.readDouble();
        flightProviderCode = in.readString();
        depature = in.readString();
        flightJsonString = in.readString();
        flightListArray = in.createTypedArrayList(FlightModel.CREATOR);
        flightListRoundArray = in.createTypedArrayList(FlightModel.CREATOR);
        contingFlight = in.readString();
        refund = in.readString();
        duration = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(flightActualBaseFare);
        dest.writeDouble(flightTaxFare);
        dest.writeDouble(serviceTax);
        dest.writeDouble(serviceCharge);
        dest.writeDouble(convenienceCharge);
        dest.writeDouble(flightNetFare);
        dest.writeDouble(totalFare);
        dest.writeString(flightProviderCode);
        dest.writeString(depature);
        dest.writeString(flightJsonString);
        dest.writeTypedList(flightListArray);
        dest.writeTypedList(flightListRoundArray);
        dest.writeString(contingFlight);
        dest.writeString(refund);
        dest.writeString(duration);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FlightListModel> CREATOR = new Creator<FlightListModel>() {
        @Override
        public FlightListModel createFromParcel(Parcel in) {
            return new FlightListModel(in);
        }

        @Override
        public FlightListModel[] newArray(int size) {
            return new FlightListModel[size];
        }
    };

    public double getFlightActualBaseFare() {
        return flightActualBaseFare;
    }

    public void setFlightActualBaseFare(double flightActualBaseFare) {
        this.flightActualBaseFare = flightActualBaseFare;
    }

    public double getFlightTaxFare() {
        return flightTaxFare;
    }

    public void setFlightTaxFare(double flightTaxFare) {
        this.flightTaxFare = flightTaxFare;
    }

    public double getServiceTax() {
        return serviceTax;
    }

    public void setServiceTax(double serviceTax) {
        this.serviceTax = serviceTax;
    }

    public double getServiceCharge() {
        return serviceCharge;
    }

    public void setServiceCharge(double serviceCharge) {
        this.serviceCharge = serviceCharge;
    }

    public double getConvenienceCharge() {
        return convenienceCharge;
    }

    public void setConvenienceCharge(double convenienceCharge) {
        this.convenienceCharge = convenienceCharge;
    }

    public double getFlightNetFare() {
        return flightNetFare;
    }

    public void setFlightNetFare(double flightNetFare) {
        this.flightNetFare = flightNetFare;
    }

    public double getTotalFare() {
        return totalFare;
    }

    public void setTotalFare(double totalFare) {
        this.totalFare = totalFare;
    }

    public String getFlightProviderCode() {
        return flightProviderCode;
    }

    public void setFlightProviderCode(String flightProviderCode) {
        this.flightProviderCode = flightProviderCode;
    }

    public String getDepature() {
        return depature;
    }

    public void setDepature(String depature) {
        this.depature = depature;
    }

    public String getFlightJsonString() {
        return flightJsonString;
    }

    public void setFlightJsonString(String flightJsonString) {
        this.flightJsonString = flightJsonString;
    }

    public ArrayList<FlightModel> getFlightListArray() {
        return flightListArray;
    }

    public void setFlightListArray(ArrayList<FlightModel> flightListArray) {
        this.flightListArray = flightListArray;
    }

    public ArrayList<FlightModel> getFlightListRoundArray() {
        return flightListRoundArray;
    }

    public void setFlightListRoundArray(ArrayList<FlightModel> flightListRoundArray) {
        this.flightListRoundArray = flightListRoundArray;
    }

    public String getContingFlight() {
        return contingFlight;
    }

    public void setContingFlight(String contingFlight) {
        this.contingFlight = contingFlight;
    }

    public String getRefund() {
        return refund;
    }

    public void setRefund(String refund) {
        this.refund = refund;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
}
