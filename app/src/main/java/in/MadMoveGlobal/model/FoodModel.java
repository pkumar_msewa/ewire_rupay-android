package in.MadMoveGlobal.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Kashif-PC on 12/9/2016.
 */
public class FoodModel implements Parcelable {

    private long pId;
    private long storeId;
    private String pName;
    private String pCode;
    private String pRetailPrice;
    private String pApplicablePrice;
    private String pShippingPrice;
    private String pThumb;

    private boolean isDiscount;
    private int pDiscountPrice;


    private ArrayList<String> pImageUrls;
    private String pDetails;
    private boolean isCod;


    public FoodModel() {

    }

    public FoodModel(long pId, long storeId, String pName, String pCode, String pRetailPrice, String pApplicablePrice, String pShippingPrice,String pThumb, boolean isDiscount, int pDiscountPrice,boolean isCod,String pDetails,ArrayList<String> pImageUrls) {
        this.pId = pId;
        this.storeId = storeId;
        this.pName = pName;
        this.pCode = pCode;
        this.pRetailPrice = pRetailPrice;
        this.pApplicablePrice = pApplicablePrice;
        this.pShippingPrice = pShippingPrice;
        this.pThumb = pThumb;
        this.isDiscount = isDiscount;
        this.pDiscountPrice = pDiscountPrice;

        this.pImageUrls = pImageUrls;
        this.pDetails = pDetails;
        this.isCod = isCod;

    }

    public ArrayList<String> getpImageUrls() {
        return pImageUrls;
    }

    public void setpImageUrls(ArrayList<String> pImageUrls) {
        this.pImageUrls = pImageUrls;
    }

    public String getpDetails() {
        return pDetails;
    }

    public void setpDetails(String pDetails) {
        this.pDetails = pDetails;
    }

    public boolean isCod() {
        return isCod;
    }

    public void setCod(boolean cod) {
        isCod = cod;
    }

    public int getpDiscountPrice() {
        return pDiscountPrice;
    }

    public void setpDiscountPrice(int pDiscountPrice) {
        this.pDiscountPrice = pDiscountPrice;
    }

    public boolean isDiscount() {
        return isDiscount;
    }

    public void setDiscount(boolean discount) {
        isDiscount = discount;
    }

    public String getpThumb() {
        return pThumb;
    }

    public void setpThumb(String pThumb) {
        this.pThumb = pThumb;
    }

    public long getpId() {
        return pId;
    }

    public void setpId(long pId) {
        this.pId = pId;
    }

    public long getStoreId() {
        return storeId;
    }

    public void setStoreId(long storeId) {
        this.storeId = storeId;
    }

    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }

    public String getpCode() {
        return pCode;
    }

    public void setpCode(String pCode) {
        this.pCode = pCode;
    }

    public String getpRetailPrice() {
        return pRetailPrice;
    }

    public void setpRetailPrice(String pRetailPrice) {
        this.pRetailPrice = pRetailPrice;
    }

    public String getpApplicablePrice() {
        return pApplicablePrice;
    }

    public void setpApplicablePrice(String pApplicablePrice) {
        this.pApplicablePrice = pApplicablePrice;
    }

    public String getpShippingPrice() {
        return pShippingPrice;
    }

    public void setpShippingPrice(String pShippingPrice) {
        this.pShippingPrice = pShippingPrice;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.pId);
        dest.writeLong(this.storeId);
        dest.writeString(this.pName);
        dest.writeString(this.pCode);
        dest.writeString(this.pRetailPrice);
        dest.writeString(this.pApplicablePrice);
        dest.writeString(this.pShippingPrice);
        dest.writeString(this.pThumb);
        dest.writeByte(this.isDiscount ? (byte) 1 : (byte) 0);
        dest.writeInt(this.pDiscountPrice);
        dest.writeStringList(this.pImageUrls);
        dest.writeString(this.pDetails);
        dest.writeByte(this.isCod ? (byte) 1 : (byte) 0);
    }

    protected FoodModel(Parcel in) {
        this.pId = in.readLong();
        this.storeId = in.readLong();
        this.pName = in.readString();
        this.pCode = in.readString();
        this.pRetailPrice = in.readString();
        this.pApplicablePrice = in.readString();
        this.pShippingPrice = in.readString();
        this.pThumb = in.readString();
        this.isDiscount = in.readByte() != 0;
        this.pDiscountPrice = in.readInt();
        this.pImageUrls = in.createStringArrayList();
        this.pDetails = in.readString();
        this.isCod = in.readByte() != 0;
    }

    public static final Creator<FoodModel> CREATOR = new Creator<FoodModel>() {
        @Override
        public FoodModel createFromParcel(Parcel source) {
            return new FoodModel(source);
        }

        @Override
        public FoodModel[] newArray(int size) {
            return new FoodModel[size];
        }
    };
}
