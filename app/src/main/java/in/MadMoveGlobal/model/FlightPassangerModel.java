package in.MadMoveGlobal.model;

/**
 * Created by kashifimam on 18/08/17.
 */

public class FlightPassangerModel {
    public int passengerCount;
    public String type;


    private FlightPassangerModel(int passengerCount, String type){
        this.passengerCount = passengerCount;
        this.type = type;
    }

    public int getPassengerCount() {
        return passengerCount;
    }

    public void setPassengerCount(int passengerCount) {
        this.passengerCount = passengerCount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
