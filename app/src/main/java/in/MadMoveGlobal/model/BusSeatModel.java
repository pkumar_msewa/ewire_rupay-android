package in.MadMoveGlobal.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Ksf on 9/30/2016.
 */
public class BusSeatModel implements Parcelable {
    public static final Creator<BusSeatModel> CREATOR = new Creator<BusSeatModel>() {
        @Override
        public BusSeatModel createFromParcel(Parcel source) {
            return new BusSeatModel(source);
        }

        @Override
        public BusSeatModel[] newArray(int size) {
            return new BusSeatModel[size];
        }
    };
    private String seatFare;
    private String netFare;
    private String isAvailable;
    private String isLadiesSeat;
    private String seatServiceTax;
    private String seatOptServiceCharge;
    private String seatNo;
    private int column;
    private int seatRow;
    private int seatLength;
    private int seatWidth;
    private int seatZIndex;
    private String seatCode;
    private String seatType;

    public BusSeatModel(int column, String seatFare, String netFare, String isAvailable, String isLadiesSeat, int seatLength, String seatNo, int seatRow, int seatWidth,int seatZIndex, String seatServiceTax, String seatOptServiceCharge,String seatCode, String seatType){
        this.column = column;
        this.seatFare = seatFare;
        this.netFare = netFare;
        this.isAvailable = isAvailable;
        this.isLadiesSeat = isLadiesSeat;
        this.seatLength = seatLength;
        this.seatNo = seatNo;
        this.seatRow = seatRow;

        this.seatWidth = seatWidth;
        this.seatZIndex = seatZIndex;
        this.seatServiceTax = seatServiceTax;
        this.seatOptServiceCharge = seatOptServiceCharge;
        this.seatCode = seatCode;
        this.seatType = seatType;
    }

    protected BusSeatModel(Parcel in) {
        this.seatFare = in.readString();
        this.netFare = in.readString();
        this.isAvailable = in.readString();
        this.isLadiesSeat = in.readString();
        this.seatServiceTax = in.readString();
        this.seatOptServiceCharge = in.readString();
        this.seatNo = in.readString();
        this.column = in.readInt();
        this.seatRow = in.readInt();
        this.seatLength = in.readInt();
        this.seatWidth = in.readInt();
        this.seatZIndex = in.readInt();
        this.seatCode = in.readString();
        this.seatType = in.readString();
    }

    public String getSeatCode() {
        return seatCode;
    }

    public void setSeatCode(String seatCode) {
        this.seatCode = seatCode;
    }

    public String getSeatServiceTax() {
        return seatServiceTax;
    }

    public void setSeatServiceTax(String seatServiceTax) {
        this.seatServiceTax = seatServiceTax;
    }

    public String getSeatOptServiceCharge() {
        return seatOptServiceCharge;
    }

    public void setSeatOptServiceCharge(String seatOptServiceCharge) {
        this.seatOptServiceCharge = seatOptServiceCharge;
    }

    public int getSeatWidth() {
        return seatWidth;
    }

    public void setSeatWidth(int seatWidth) {
        this.seatWidth = seatWidth;
    }

    public int getSeatZIndex() {
        return seatZIndex;
    }

    public void setSeatZIndex(int seatZIndex) {
        this.seatZIndex = seatZIndex;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public String getSeatFare() {
        return seatFare;
    }

    public void setSeatFare(String seatFare) {
        this.seatFare = seatFare;
    }

    public String getNetFare() {
        return netFare;
    }

    public void setNetFare(String netFare) {
        this.netFare = netFare;
    }

    public String getIsAvailable() {
        return isAvailable;
    }

    public void setIsAvailable(String isAvailable) {
        this.isAvailable = isAvailable;
    }

    public String getIsLadiesSeat() {
        return isLadiesSeat;
    }

    public void setIsLadiesSeat(String isLadiesSeat) {
        this.isLadiesSeat = isLadiesSeat;
    }

    public int getSeatLength() {
        return seatLength;
    }

    public void setSeatLength(int seatLength) {
        this.seatLength = seatLength;
    }

    public String getSeatNo() {
        return seatNo;
    }

    public void setSeatNo(String seatNo) {
        this.seatNo = seatNo;
    }

    public int getSeatRow() {
        return seatRow;
    }

    public void setSeatRow(int seatRow) {
        this.seatRow = seatRow;
    }

    public String getSeatType() {
        return seatType;
    }

    public void setSeatType(String seatType) {
        this.seatType = seatType;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.seatFare);
        dest.writeString(this.netFare);
        dest.writeString(this.isAvailable);
        dest.writeString(this.isLadiesSeat);
        dest.writeString(this.seatServiceTax);
        dest.writeString(this.seatOptServiceCharge);
        dest.writeString(this.seatNo);
        dest.writeInt(this.column);
        dest.writeInt(this.seatRow);
        dest.writeInt(this.seatLength);
        dest.writeInt(this.seatWidth);
        dest.writeInt(this.seatZIndex);
        dest.writeString(this.seatCode);
        dest.writeString(this.seatType);
    }
}
