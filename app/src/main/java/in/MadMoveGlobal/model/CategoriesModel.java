package in.MadMoveGlobal.model;

/**
 * Created by Ksf on 8/19/2015.
 */
public class CategoriesModel {
    private int catId;
    private String catName;
    private int catImage;

    public int getCatImage() {
        return catImage;
    }

    public void setCatImage(int catImage) {
        this.catImage = catImage;
    }

    public CategoriesModel() {

    }

    public CategoriesModel(int catId, String catName, int catImage) {
        this.catId = catId;
        this.catName = catName;
        this.catImage = catImage;

    }

    public int getCatId() {
        return catId;
    }

    public void setCatId(int catId) {
        this.catId = catId;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }
}
