package in.MadMoveGlobal.model;

import com.orm.SugarRecord;

/**
 * Created by Ksf on 9/8/2016.
 */
public class QuestionListModel extends SugarRecord{
    private String quesCode;
    private String question;

    public QuestionListModel(){

    }

    public QuestionListModel(String quesCode, String question){
        this.quesCode = quesCode;
        this.question = question;

    }

    public String getQuesCode() {
        return quesCode;
    }

    public void setQuesCode(String quesCode) {
        this.quesCode = quesCode;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }
}
