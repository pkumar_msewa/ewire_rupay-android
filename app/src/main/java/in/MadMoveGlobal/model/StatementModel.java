package in.MadMoveGlobal.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Ksf on 5/8/2016.
 */
public class StatementModel implements Parcelable{
    public String indicator;
    public String date;
    public String amount;
    public String description;
    public String status;
    public String authNo;
    public String refNo;

    public StatementModel(String indicator, String date, String amount, String description, String status, String authNo, String refNo) {
        this.indicator = indicator;
        this.date = date;
        this.amount = amount;
        this.description = description;
        this.status = status;
        this.authNo = authNo;
        this.refNo = refNo;
    }

    protected StatementModel(Parcel in) {
        indicator = in.readString();
        date = in.readString();
        amount = in.readString();
        description = in.readString();
        status = in.readString();
        authNo = in.readString();
        refNo = in.readString();
    }

    public static final Creator<StatementModel> CREATOR = new Creator<StatementModel>() {
        @Override
        public StatementModel createFromParcel(Parcel in) {
            return new StatementModel(in);
        }

        @Override
        public StatementModel[] newArray(int size) {
            return new StatementModel[size];
        }
    };

    public String getIndicator() {
        return indicator;
    }

    public void setIndicator(String indicator) {
        this.indicator = indicator;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAuthNo() {
        return authNo;
    }

    public void setAuthNo(String authNo) {
        this.authNo = authNo;
    }

    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(indicator);
        parcel.writeString(date);
        parcel.writeString(amount);
        parcel.writeString(description);
        parcel.writeString(status);
        parcel.writeString(authNo);
        parcel.writeString(refNo);
    }
}
