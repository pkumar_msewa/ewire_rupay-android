package in.MadMoveGlobal.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.orm.SugarRecord;


public class GroupModel {
    public String name;
    public String id;

    public GroupModel(String name, String id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}