package in.MadMoveGlobal.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.orm.SugarRecord;

public class TutioralModel extends SugarRecord implements Parcelable {

	public boolean code;

    public TutioralModel() {
    }

    public TutioralModel(boolean code) {
        this.code = code;
    }

    protected TutioralModel(Parcel in) {
        code = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (code ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TutioralModel> CREATOR = new Creator<TutioralModel>() {
        @Override
        public TutioralModel createFromParcel(Parcel in) {
            return new TutioralModel(in);
        }

        @Override
        public TutioralModel[] newArray(int size) {
            return new TutioralModel[size];
        }
    };

    public boolean isCode() {
        return code;
    }

    public void setCode(boolean code) {
        this.code = code;
    }
}
