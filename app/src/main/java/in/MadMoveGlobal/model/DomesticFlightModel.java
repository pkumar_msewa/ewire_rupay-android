package in.MadMoveGlobal.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Ksf on 10/6/2016.
 */
public class DomesticFlightModel implements Parcelable {
    private String airportCode;
    private String cityName;
    private String countryName;
    private String airportDesc;
    private String airportType;


    public DomesticFlightModel(){

    }

    public DomesticFlightModel(String airportCode, String cityName, String countryName, String airportDesc){
        this.airportCode = airportCode;
        this.cityName = cityName;
        this.countryName = countryName;
        this.airportDesc = airportDesc;
    }

    public String getAirportCode() {
        return airportCode;
    }

    public void setAirportCode(String airportCode) {
        this.airportCode = airportCode;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getAirportDesc() {
        return airportDesc;
    }

    public void setAirportDesc(String airportDesc) {
        this.airportDesc = airportDesc;
    }

    public String getAirportType() {
        return airportType;
    }

    public void setAirportType(String airportType) {
        this.airportType = airportType;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.airportCode);
        dest.writeString(this.cityName);
        dest.writeString(this.countryName);
        dest.writeString(this.airportDesc);
        dest.writeString(this.airportType);
    }

    protected DomesticFlightModel(Parcel in) {
        this.airportCode = in.readString();
        this.cityName = in.readString();
        this.countryName = in.readString();
        this.airportDesc = in.readString();
        this.airportType = in.readString();
    }

    public static final Creator<DomesticFlightModel> CREATOR = new Creator<DomesticFlightModel>() {
        @Override
        public DomesticFlightModel createFromParcel(Parcel source) {
            return new DomesticFlightModel(source);
        }

        @Override
        public DomesticFlightModel[] newArray(int size) {
            return new DomesticFlightModel[size];
        }
    };
}
