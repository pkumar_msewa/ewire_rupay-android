package in.MadMoveGlobal.model;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class AddressModel {

	public static final String COUNTRY_ID = "5";
	
	private HashMap<String, AddressEntityModel> cityMap;
	private HashMap<String, AddressEntityModel> stateMap;

	public static HashMap<String, HashMap<String, AddressEntityModel>> stateCityMap = new HashMap<String, HashMap<String, AddressEntityModel>>();
	public static HashMap<String, HashMap<String, AddressEntityModel>> countryStateMap = new HashMap<String, HashMap<String,AddressEntityModel>>();
	
	private ArrayList<AddressEntityModel> stateList;
	private ArrayList<AddressEntityModel> cityList;

	public AddressModel() {
	}

	/*
	 * {"cities":[{"id":"6","desc":"Ahmednagar"},{"id":"10","desc":"Akola"},{"id"
	 * :"20","desc":"Amravati"},{"id":"34","desc": "Aurangabad" },
	 * {"id":"521","desc" : "Vani(Nashik)"},{"id":"534","desc":"Wardha"
	 * },{"id":"542","desc":"Yavatmal" }]}
	 */
	public void createCityMap(String cityJSON, String stateId) {
		cityMap = new HashMap<String, AddressEntityModel>();
		cityList = new ArrayList<AddressEntityModel>();
		try {
			JSONObject json = new JSONObject(cityJSON);
			JSONArray states = json.getJSONArray("cities");
			int statesLength = states.length();
			for (int i = 0; i < statesLength; i++) {
				JSONObject state = (JSONObject) states.get(i);
				String id = state.getString("id");
				String desc = state.getString("desc");
				AddressEntityModel ae = new AddressEntityModel(i, id, desc);
				cityMap.put(id, ae);
				cityList.add(ae);
			}
			stateCityMap.put(stateId, cityMap);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/*
	 * {"state":[{"id":"3","desc":"AndamanNicobar"},{"id":"4","desc":"AndhraPradesh"
	 * },{"id":"89","desc":"Tripura"},{"id":"93","desc":"Uttaranchal"
	 * },{"id":"94","desc":"UttarPradesh"},{"id":"97","desc":"WestBengal"}]}
	 */
	public void createStateMap(String stateJSON, String countryId) {
		stateMap = new HashMap<String, AddressEntityModel>();
		stateList = new ArrayList<AddressEntityModel>();
		try {
			JSONObject json = new JSONObject(stateJSON);
			JSONArray states = json.getJSONArray("state");
			int statesLength = states.length();
			for (int i = 0; i < statesLength; i++) {
				JSONObject state = (JSONObject) states.get(i);
				String id = state.getString("id");
				String desc = state.getString("desc");
				AddressEntityModel ae = new AddressEntityModel(i, id, desc);
				stateMap.put(id, ae);
				stateList.add(ae);
			}
			countryStateMap.put(countryId, stateMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public ArrayList<AddressEntityModel> getStateList() {
		return stateList;
	}
	
	public ArrayList<AddressEntityModel> getNoStateList() {
		ArrayList<AddressEntityModel> list = new ArrayList<AddressEntityModel>();
		list.add(new AddressEntityModel(0, "0", "No State"));
		return list;
	}
	
	public ArrayList<AddressEntityModel> getCityList() {
		return cityList;
	}
	
	public ArrayList<AddressEntityModel> getNoCityList() {
		ArrayList<AddressEntityModel> list = new ArrayList<>();
		list.add(new AddressEntityModel(0, "0", "No City"));
		return list;
	}

	public HashMap<String, AddressEntityModel> getCityMap() {
		return cityMap;
	}

	public void setCityMap(HashMap<String, AddressEntityModel> cityMap) {
		this.cityMap = cityMap;
	}

	public HashMap<String, AddressEntityModel> getStateMap() {
		return stateMap;
	}

	public void setStateMap(HashMap<String, AddressEntityModel> stateMap) {
		this.stateMap = stateMap;
	}

}
