package in.MadMoveGlobal.model;

/**
 * Created by Msewa-Admin on 5/17/2017.
 */

public class ThemeParkTicketModel {
    private String type;
    private String cost_per;
    private String product_id;
    private String service_tax;
    private String swachh_tax;
    private String kkc_tax;

    public ThemeParkTicketModel(String type, String cost_per, String product_id, String service_tax, String swachh_tax, String kkc_tax) {
        this.type = type;
        this.cost_per = cost_per;
        this.product_id = product_id;
        this.service_tax = service_tax;
        this.swachh_tax = swachh_tax;
        this.kkc_tax = kkc_tax;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCost_per() {
        return cost_per;
    }

    public void setCost_per(String cost_per) {
        this.cost_per = cost_per;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getService_tax() {
        return service_tax;
    }

    public void setService_tax(String service_tax) {
        this.service_tax = service_tax;
    }

    public String getSwachh_tax() {
        return swachh_tax;
    }

    public void setSwachh_tax(String swachh_tax) {
        this.swachh_tax = swachh_tax;
    }

    public String getKkc_tax() {
        return kkc_tax;
    }

    public void setKkc_tax(String kkc_tax) {
        this.kkc_tax = kkc_tax;
    }
}
