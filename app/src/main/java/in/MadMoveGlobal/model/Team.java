package in.MadMoveGlobal.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Team implements Parcelable {

    private int id;
    private String fullName;
    private String shortName;
    private String abbreviation;
    private String primaryColor;

    public Team(int id, String fullName, String shortName, String abbreviation, String primaryColor) {
        this.id = id;
        this.fullName = fullName;
        this.shortName = shortName;
        this.abbreviation = abbreviation;
        this.primaryColor = primaryColor;
    }

    protected Team(Parcel in) {
        id = in.readInt();
        fullName = in.readString();
        shortName = in.readString();
        abbreviation = in.readString();
        primaryColor = in.readString();
    }

    public static final Creator<Team> CREATOR = new Creator<Team>() {
        @Override
        public Team createFromParcel(Parcel in) {
            return new Team(in);
        }

        @Override
        public Team[] newArray(int size) {
            return new Team[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String getPrimaryColor() {
        return primaryColor;
    }

    public void setPrimaryColor(String primaryColor) {
        this.primaryColor = primaryColor;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(fullName);
        parcel.writeString(shortName);
        parcel.writeString(abbreviation);
        parcel.writeString(primaryColor);
    }
}