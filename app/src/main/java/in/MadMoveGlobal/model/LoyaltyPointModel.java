package in.MadMoveGlobal.model;

public class LoyaltyPointModel {

    private String cardNo,pin,expiryDate;
    private boolean hasClavax;

    public LoyaltyPointModel() {
    }

    public LoyaltyPointModel(String cardNo, String pin, String expiryDate, boolean hasClavax) {
        this.cardNo = cardNo;
        this.pin = pin;
        this.expiryDate = expiryDate;
        this.hasClavax = hasClavax;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public boolean isHasClavax() {
        return hasClavax;
    }

    public void setHasClavax(boolean hasClavax) {
        this.hasClavax = hasClavax;
    }
}
