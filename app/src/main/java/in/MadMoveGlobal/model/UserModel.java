package in.MadMoveGlobal.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.orm.SugarRecord;

/**
 * Created by Ksf on 3/16/2016.
 */
public class UserModel extends SugarRecord implements Parcelable {
    public String userSessionId;
    public String userFirstName;
    public String userLastName;
    public String userMobileNo;
    public String userEmail;
    public String userAddress;
    public String emailIsActive;

    public String userAcNo;
    public String userBalance;
    public String userDailyLimit;
    public String userMonthlyLimit;
    public String userBalanceLimit;
    public String userAcName;
    public String userAcCode;
    public String userImage;
    public String userPoints;
    public String usercardNo;
    public String usercvv;
    public String userexpiryDate;
    public String usercardHolder;
    public String userPhysicalRequest;
    public String userminimumCardBalance;
    public String loadMoneyComm;
    private String usercardFees;
    private String usercardBaseFare;


    public int isValid;

    public String userDob;
    public String userGender;
    public String userPhysicalBlock;
    public String userVirtualBlock;
    public String impsCommissionAmt;
    public boolean isMPin, hasRefer, hasVcard, hasPcard, hasPhyReq, haskycRequest, isPCStatus, isVCStatus, isImpsFixed;


    private volatile static UserModel uniqueInstance;


    public UserModel() {

    }

    protected UserModel(Parcel in) {
        userSessionId = in.readString();
        userFirstName = in.readString();
        userLastName = in.readString();
        userMobileNo = in.readString();
        userEmail = in.readString();
        userAddress = in.readString();
        emailIsActive = in.readString();
        userAcNo = in.readString();
        userBalance = in.readString();
        userDailyLimit = in.readString();
        userMonthlyLimit = in.readString();
        userBalanceLimit = in.readString();
        userAcName = in.readString();
        userAcCode = in.readString();
        userImage = in.readString();
        userPoints = in.readString();
        isValid = in.readInt();
        userDob = in.readString();
        userGender = in.readString();
        usercardNo = in.readString();
        usercvv = in.readString();
        userexpiryDate = in.readString();
        usercardHolder = in.readString();
        userminimumCardBalance = in.readString();
        usercardFees = in.readString();
        usercardBaseFare = in.readString();
        loadMoneyComm = in.readString();
        isMPin = in.readByte() != 0;
        hasRefer = in.readByte() != 0;
        hasVcard = in.readByte() != 0;
        hasPcard = in.readByte() != 0;
        isPCStatus = in.readByte() != 0;
        isVCStatus = in.readByte() != 0;
        isImpsFixed = in.readByte() != 0;

        userPhysicalRequest = in.readString();
        hasPhyReq = in.readByte() != 0;
        haskycRequest = in.readByte() != 0;
        userPhysicalBlock = in.readString();
        userVirtualBlock = in.readString();
        impsCommissionAmt = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userSessionId);
        dest.writeString(userFirstName);
        dest.writeString(userLastName);
        dest.writeString(userMobileNo);
        dest.writeString(userEmail);
        dest.writeString(userAddress);
        dest.writeString(emailIsActive);
        dest.writeString(userAcNo);
        dest.writeString(userBalance);
        dest.writeString(userDailyLimit);
        dest.writeString(userMonthlyLimit);
        dest.writeString(userBalanceLimit);
        dest.writeString(userAcName);
        dest.writeString(userAcCode);
        dest.writeString(userImage);
        dest.writeString(userPoints);

        dest.writeString(usercardNo);
        dest.writeString(usercvv);
        dest.writeString(userexpiryDate);
        dest.writeString(usercardHolder);
        dest.writeString(userminimumCardBalance);
        dest.writeString(usercardFees);
        dest.writeString(usercardBaseFare);
        dest.writeString(loadMoneyComm);
        dest.writeString(userPhysicalBlock);
        dest.writeString(userVirtualBlock);

        dest.writeInt(isValid);
        dest.writeString(userDob);
        dest.writeString(userGender);
        dest.writeString(impsCommissionAmt);
        dest.writeByte((byte) (isMPin ? 1 : 0));
        dest.writeByte((byte) (hasRefer ? 1 : 0));
        dest.writeByte((byte) (hasVcard ? 1 : 0));
        dest.writeByte((byte) (hasPcard ? 1 : 0));
        dest.writeByte((byte) (isPCStatus ? 1 : 0));
        dest.writeByte((byte) (isVCStatus ? 1 : 0));
        dest.writeString(userPhysicalRequest);
        dest.writeByte((byte) (hasPhyReq ? 1 : 0));
        dest.writeByte((byte) (haskycRequest ? 1 : 0));
        dest.writeByte((byte) (isImpsFixed ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UserModel> CREATOR = new Creator<UserModel>() {
        @Override
        public UserModel createFromParcel(Parcel in) {
            return new UserModel(in);
        }

        @Override
        public UserModel[] newArray(int size) {
            return new UserModel[size];
        }
    };

    /*
     * Singleton getInstance()
     */
    public static UserModel getInstance() {
        if (uniqueInstance == null) {
            synchronized (UserModel.class) {
                if (uniqueInstance == null) {
                    uniqueInstance = new UserModel();
                }
            }
        }
        return uniqueInstance;
    }

    public UserModel(String userSessionId, String userFirstName, String userLastName, String userMobileNo, String userEmail, String emailIsActive, int isValid, String userAcNo, String userAcName, String userAcCode, String userBalance, String userBalanceLimit, String userDailyLimit, String userMonthlyLimit, String userAddress, String userImage, String userDob, String userGender, boolean isMPin, String userPoints, boolean hasRefer, boolean hasVcard, boolean hasPcard, String usercardNo, String usercvv, String userexpiryDate, String usercardHolder, boolean hasPhyReq, String userPhysicalRequest, String userminimumCardBalance, String usercardFees, boolean haskycRequest, String loadMoneyComm, String usercardBaseFare, boolean isPCStatus, boolean isVCStatus, String userVirtualBlock, String userPhysicalBlock, String impsCommissionAmt, boolean isImpsFixed) {
        this.userSessionId = userSessionId;
        this.userFirstName = userFirstName;
        this.userLastName = userLastName;
        this.userMobileNo = userMobileNo;
        this.userEmail = userEmail;
        this.emailIsActive = emailIsActive;
        this.isValid = isValid;
        this.isMPin = isMPin;
        this.userDob = userDob;
        this.userGender = userGender;
        this.userAcNo = userAcNo;
        this.userBalance = userBalance;
        this.userBalanceLimit = userBalanceLimit;
        this.userDailyLimit = userDailyLimit;
        this.userMonthlyLimit = userMonthlyLimit;
        this.userAcCode = userAcCode;
        this.userAcName = userAcName;
        this.userAddress = userAddress;
        this.userImage = userImage;

        this.usercardNo = usercardNo;
        this.usercvv = usercvv;
        this.userexpiryDate = userexpiryDate;
        this.usercardHolder = usercardHolder;
        this.userminimumCardBalance = userminimumCardBalance;
        this.usercardFees = usercardFees;
        this.usercardBaseFare = usercardBaseFare;

        this.userPoints = userPoints;
        this.hasRefer = hasRefer;
        this.hasVcard = hasVcard;
        this.hasVcard = hasPcard;
        this.isPCStatus = isPCStatus;
        this.isPCStatus = isVCStatus;
        this.userPhysicalRequest = userPhysicalRequest;
        this.loadMoneyComm = loadMoneyComm;
        this.hasPhyReq = hasPhyReq;
        this.haskycRequest = haskycRequest;
        this.userVirtualBlock = userVirtualBlock;
        this.userPhysicalBlock = userPhysicalBlock;
        this.impsCommissionAmt = impsCommissionAmt;
        this.isImpsFixed = isImpsFixed;
    }


    public String getUserSessionId() {
        return userSessionId;
    }

    public void setUserSessionId(String userSessionId) {
        this.userSessionId = userSessionId;
    }

    public String getUserFirstName() {
        return userFirstName;
    }

    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    public String getUserMobileNo() {
        return userMobileNo;
    }

    public void setUserMobileNo(String userMobileNo) {
        this.userMobileNo = userMobileNo;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public String getEmailIsActive() {
        return emailIsActive;
    }

    public void setEmailIsActive(String emailIsActive) {
        this.emailIsActive = emailIsActive;
    }

    public String getUserAcNo() {
        return userAcNo;
    }

    public void setUserAcNo(String userAcNo) {
        this.userAcNo = userAcNo;
    }

    public String getUserBalance() {
        return userBalance;
    }

    public void setUserBalance(String userBalance) {
        this.userBalance = userBalance;
    }

    public String getUserDailyLimit() {
        return userDailyLimit;
    }

    public void setUserDailyLimit(String userDailyLimit) {
        this.userDailyLimit = userDailyLimit;
    }

    public String getUserMonthlyLimit() {
        return userMonthlyLimit;
    }

    public void setUserMonthlyLimit(String userMonthlyLimit) {
        this.userMonthlyLimit = userMonthlyLimit;
    }

    public String getUserPhysicalBlock() {
        return userPhysicalBlock;
    }

    public void setUserPhysicalBlock(String userPhysicalBlock) {
        this.userPhysicalBlock = userPhysicalBlock;
    }

    public String getUserVirtualBlock() {
        return userVirtualBlock;
    }

    public void setUserVirtualBlock(String userVirtualBlock) {
        this.userVirtualBlock = userVirtualBlock;
    }

    public String getUserBalanceLimit() {
        return userBalanceLimit;
    }

    public void setUserBalanceLimit(String userBalanceLimit) {
        this.userBalanceLimit = userBalanceLimit;
    }

    public String getUserAcName() {
        return userAcName;
    }

    public void setUserAcName(String userAcName) {
        this.userAcName = userAcName;
    }

    public String getUserAcCode() {
        return userAcCode;
    }

    public void setUserAcCode(String userAcCode) {
        this.userAcCode = userAcCode;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getUserPoints() {
        return userPoints;
    }

    public void setUserPoints(String userPoints) {
        this.userPoints = userPoints;
    }

    public String getUsercardNo() {
        return usercardNo;
    }

    public void setUsercardNo(String usercardNo) {
        this.usercardNo = usercardNo;
    }

    public String getUsercvv() {
        return usercvv;
    }

    public void setUsercvv(String usercvv) {
        this.usercvv = usercvv;
    }

    public String getUserexpiryDate() {
        return userexpiryDate;
    }

    public void setUserexpiryDate(String userexpiryDate) {
        this.userexpiryDate = userexpiryDate;
    }

    public String getUsercardHolder() {
        return usercardHolder;
    }

    public void setUsercardHolder(String usercardHolder) {
        this.usercardHolder = usercardHolder;
    }

    public int getIsValid() {
        return isValid;
    }

    public void setIsValid(int isValid) {
        this.isValid = isValid;
    }

    public String getUserDob() {
        return userDob;
    }

    public void setUserDob(String userDob) {
        this.userDob = userDob;
    }

    public String getUserGender() {
        return userGender;
    }


    public String getUserminimumCardBalance() {
        return userminimumCardBalance;
    }

    public void setUserminimumCardBalance(String userminimumCardBalance) {
        this.userminimumCardBalance = userminimumCardBalance;
    }

    public String getloadMoneyComm() {
        return loadMoneyComm;
    }

    public void setloadMoneyComm(String loadMoneyComm) {
        this.loadMoneyComm = loadMoneyComm;
    }

    public String getUsercardFees() {
        return usercardFees;
    }

    public void setUsercardFees(String usercardFees) {
        this.usercardFees = usercardFees;
    }


    public String getUsercardBaseFare() {
        return usercardBaseFare;
    }

    public void setUsercardBaseFare(String usercardBaseFare) {
        this.usercardBaseFare = usercardBaseFare;
    }

    public String getimpsCommissionAmt() {
        return impsCommissionAmt;
    }

    public void setImpsCommissionAmt(String impsCommissionAmt) {
        this.impsCommissionAmt = impsCommissionAmt;
    }

    public void setUserGender(String userGender) {
        this.userGender = userGender;
    }

    public boolean isMPin() {
        return isMPin;
    }

    public void setMPin(boolean MPin) {
        isMPin = MPin;
    }


    public boolean isPCStatus() {
        return isPCStatus;
    }

    public void setPCStatus(boolean PCStatus) {
        isPCStatus = PCStatus;
    }

    public boolean isVCStatus() {
        return isVCStatus;
    }

    public void setVCStatus(boolean VCStatus) {
        isVCStatus = VCStatus;
    }


    public boolean isHasRefer() {
        return hasRefer;
    }

    public void setHasRefer(boolean hasRefer) {
        this.hasRefer = hasRefer;
    }

    public boolean isHasVcard() {
        return hasVcard;
    }

    public void setHasVcard(boolean hasVcard) {
        this.hasVcard = hasVcard;
    }

    public boolean isHasPcard() {
        return hasPcard;
    }

    public void setHasPcard(boolean hasPcard) {
        this.hasPcard = hasPcard;
    }

    public String getUserPhysicalRequest() {
        return userPhysicalRequest;
    }

    public void setPhysicalCardStatus(String userPhysicalRequest) {
        this.userPhysicalRequest = userPhysicalRequest;
    }


    public boolean isHasPhyReq() {
        return hasPhyReq;
    }

    public void setHasPhysicalRequest(boolean hasPhyReq) {
        this.hasPhyReq = hasPhyReq;
    }


    public boolean isHaskycRequest() {
        return haskycRequest;
    }

    public void setHaskycRequest(boolean haskycRequest) {
        this.haskycRequest = haskycRequest;
    }


    public boolean isImpsFixed() {
        return isImpsFixed;
    }

    public void setIsImpsFixed(boolean isImpsFixed) {
        this.isImpsFixed = isImpsFixed;
    }


    public static UserModel getUniqueInstance() {
        return uniqueInstance;
    }

    public static void setUniqueInstance(UserModel uniqueInstance) {
        UserModel.uniqueInstance = uniqueInstance;
    }
}
