package in.MadMoveGlobal.metadata;

public class ApiUrl {


    //    Test
//   private static final String URL_DOMAIN = "http://13.233.137.181/Api/";
//    public static final String URL_DOMAIN_ = "http://13.233.137.181/";

//    public static final String URL_BIL_MOBILE = "http://106.51.8.246/mdex/api/recharge/services/";
//    private static final String URL_DOMAIN = "http://13.233.137.181/Ewire-Rupay/Api/";
//    public static final String URL_DOMAIN_ = "http://13.233.137.181/Ewire-Rupay/";

// Local Host
//    private static final String URL_DOMAIN = "http://192.170.1.177:8089/Api/";
//    public static final String URL_DOMAIN_ = "http://192.170.1.177:8089/";

//    Live
    private static final String URL_DOMAIN = "http://ewirerupay.com/Api/";
    public static final String URL_DOMAIN_ = "http://ewirerupay.com/";

    public static final String URL_BIL_MOBILE = "https://mdex.msewa.com/api/recharge/services/";


    private static final String VERSION = "v1";
    private static final String ROLE = "User";
    private static final String DEVICE = "Android";
    private static final String LANGUAGE = "en";
    private static final String SEPARATOR = "/";

    public static final String URL_MAIN = URL_DOMAIN + VERSION + SEPARATOR + ROLE + SEPARATOR + DEVICE + SEPARATOR + LANGUAGE + SEPARATOR;

    public static final String URL_MAIN_ = URL_DOMAIN + ROLE + SEPARATOR + DEVICE + SEPARATOR + LANGUAGE + SEPARATOR;
    public static final String URL_LOAD_MONEY = "https://www.vpayqwik.com/" + "Api/v1/User/Android/en/LoadMoney/Process";
    public static final String URL_FLIGHT_MONEY = URL_DOMAIN + "v1/User/Android/en/Treval/Flight/Process";

    public static final String REDIRECT = URL_DOMAIN + "v1/User/Android/en/LoadMoney/ccLoad/MobileLanding";
    public static final String CANCLE = URL_DOMAIN_ + "LoadMoney/ccAvenues/Cancel";
    //    public static final String URL_LOAD_MONEY_RESPONSE = URL_DOMAIN_ + "/Api/v1/User/Android/en/LoadMoney/Response";
    public static final String URL_LOAD_MONEY_RESPONSE = URL_DOMAIN_ + "/Api/v1/User/Android/en/LoadMoney/Redirect";
    public static final String URL_LOAD_MONEY_CARD_RESPONSE = URL_DOMAIN_ + "/Api/v1/User/Android/en/LoadMoney/ConfirmCardRequest";


    //USER
    public static final String URL_LOGIN = URL_MAIN + "Login/Process";
    //    public static final String URL_LOGIN = URL_MAIN + "Login";
    public static final String URL_GROUP_LIST = URL_DOMAIN + "User/Android/en/Group" /*"http://106.51.8.246:8090/ewire/Api/User/Android/en/Group"*/;
    public static final String URL_REGISTRATION = URL_MAIN_ + "RegisterFullDetails";
    public static final String URL_VERIFIED_MOBILE_OTP = URL_MAIN_ + "CustomRegister";
    public static final String URL_FORGET_PWD = URL_MAIN_ + "ForgotPassword";
    public static final String URL_CHANGE_PWD = URL_MAIN_ + "ChangePasswordOTP";
    public static final String URL_RESEND_OTP = URL_MAIN_ + "Resend/Mobile/OTP";
    //    public static final String URL_EDIT_USER = URL_MAIN + "User/EditProfile";
    public static final String URL_EDIT_USER = URL_DOMAIN_ + "User/Upgrade/KycAccount";
    public static final String URL_VERIFY_PHONE = URL_MAIN_ + "CustomActivate/Mobile";
    public static final String URL_VERIFY_PHONEOTP = URL_MAIN_ + "ChangePasswordOTP";
    public static final String URL_LOGOUT = URL_MAIN + "Logout";
    public static final String URL_CHANGE_CURRENT_PWD = URL_MAIN_ + "ChangePassword";
    public static final String URL_FORGET_MPIN = URL_MAIN + "User/ForgotMPIN";


    public static final String URL_SEND_KYC_DATA = URL_MAIN + "User/KycRequest";
    public static final String URL_VERIFY_KYC_DATA = URL_MAIN + "User/KycRequest/OTPVerification";

    public static final String URL_GET_USER_DETAILS = URL_MAIN + "GetUserDetails";

    public static final String URL_GET_USER_BALANCE = URL_MAIN + "GetBalance";
    public static final String URL_GET_USER_RESET = URL_MAIN + "ResetPin";
    public static final String URL_GET_USER_POINTS = URL_MAIN + "User/GetDetails/Account";
    public static final String URL_GET_USER_SUB_DETAILS = URL_MAIN + "User/GetDetails/Basic";

    public static final String URL_GET_CARD_USER_BALANCE = URL_MAIN + "DebitFromCard";

    //Old
    public static final String URL_UPLOAD_IMAGE = URL_DOMAIN_ + "User/Upgrade/KycAccount";
    //    public static final String URL_IMAGE_MAIN = "http://fgmtest.firstglobalmoney.com:8035/";
    public static final String URL_IMAGE_MAIN = "https://www.vpayqwik.com";

    //TEST
    public static final String URL_LOYALTY_REQUEST = "http://ewirerupay.com/Api/User/Android/en/Clavex/Registration";

    public static final String URL_LOYALTY_OFFER = "http://ewirerupay.com/Api/v1/User/ANDROID/en/Clavex/Offers";

    //TOPUPS New
    public static final String URL_PREPAID_TOPUP = URL_MAIN + "MdexTransaction/InitiateMdex";
    public static final String URL_POSTPAID_TOPUP = URL_MAIN + "Topup/PostPaid";
    public static final String URL_DATACARD_TOPUP = URL_MAIN + "Topup/DataCard";

    //GENERATE TOP_UP NO AND PLANS new
//    public static final String URL_LOAD_ACCORDING_NO = "https://mdex.msewa.com/api/recharge/gettelco";
//    public static final String URL_BROWSE_PLAN = "https://mdex.msewa.com/api/recharge/getplans";
//    public static final String URL_CIRCLE_OPERATOR = URL_BIL_MOBILE + "PRE";
//    public static final String URL_CIRCLE_OPERATORLL = URL_BIL_MOBILE + "landline";
//    public static final String URL_CIRCLE_OPERATORPOST = URL_BIL_MOBILE + "post";
//    public static final String URL_CIRCLE_OPERATORDTH = URL_BIL_MOBILE + "dthbill";
//    public static final String URL_CIRCLE_OPERATORGAS = URL_BIL_MOBILE + "gas";
//    public static final String URL_CIRCLE_OPERATORINSURANCE = URL_BIL_MOBILE + "insurance";
//    public static final String URL_CIRCLE_OPERATORELC = URL_BIL_MOBILE + "elec";


    public static final String URL_LOAD_ACCORDING_NO = URL_MAIN + "BillPayment/GetTelcoMobile";

    public static final String URL_LOAD_ACCORDING_NOM = URL_MAIN + "BillPayment/GetMnp";
    public static final String URL_BROWSE_PLAN = URL_MAIN + "BillPayment/GetPlansPrepaidData";
    public static final String URL_CIRCLE_OPERATOR = URL_MAIN + "BillPayment/GetCircleOperator";
    //Check DEU AMOUNT
    public static final String URL_CHECK_DEU = URL_MAIN + "BillPayment/DueAmount";
    public static final String URL_BROWSE_PLANS = URL_MAIN + "BillPayment/GetBrowsePlans";


    //MERCHANT API and pay at store
    public static final String URL_LIST_STORES = URL_MAIN + "User/GetMerchants";
    public static final String URL_PAY_AT_STORE = URL_MAIN + "SendMoney/Store";

    //SEnd Money.
    public static final String URL_FUND_TRANSFER = URL_MAIN + "SendMoney";
    public static final String URL_FUND_DONATION = URL_MAIN + "Donation";

    //BillPayment New
    public static final String URL_DTH_PAYMENT = URL_MAIN + "BillPayment/DTH";
    public static final String URL_ELECTRICITY_PAYMENT = URL_MAIN + "BillPayment/Electricity";
    public static final String URL_LANDLINE_PAYMENT = URL_MAIN + "BillPayment/Landline";
    public static final String URL_GAS_PAYMENT = URL_MAIN + "BillPayment/Gas";
    public static final String URL_INSURANCE_PAYMENT = URL_MAIN + "BillPayment/Insurance";


    //TELEBUY
    public static final String URL_TELEBUY_MAIN = "http://122.183.176.98/Payqwikweb_web/PAGE_PayQwik.awp";
    public static final String URL_GET_ALL_PRODUCTS = URL_TELEBUY_MAIN + "?API=GETALLPRODUCTDETAILS";
    public static final String URL_GET_SHOPPING_VIDEO = URL_TELEBUY_MAIN + "?API=GETYOUTUBEURL&PID=";
    public static final String URL_GET_CART = URL_TELEBUY_MAIN + "?API=RetrieveOpenCartDetails&PHONE=";
    public static final String URL_GET_ADDRESS = URL_TELEBUY_MAIN + "?API=LISTCONTACTADDRESSES&PHONE=";
    public static final String URL_GET_STATE = URL_TELEBUY_MAIN + "?API=GETSTATE&CountryID=5";
    public static final String URL_GET_CITY = URL_TELEBUY_MAIN + "?API=GetCity&Stateid=";
    public static final String URL_ADD_CART = URL_TELEBUY_MAIN + "?API=UPDATEITEMQUANTITYINCART&PHONE=";
    public static final String URL_REMOVE_CART = URL_TELEBUY_MAIN + "?API=RemoveItemFromCart&PHONE=";


    //FOR LOADING MONEY old
    public static final String URL_GENERATE_RSA = URL_MAIN + "User/LoadMoney/RSAKey";
    public static final String REDIRECT_URL = URL_MAIN + "User/LoadMoney/Redirect";
    public static final String CANCEL_URL = URL_MAIN + "User/LoadMoney/Redirect";

    //MPIN NEW
    public static final String URL_GENERATE_M_PIN = URL_MAIN + "User/SetMPIN";
    public static final String URL_DELETE_M_PIN = URL_MAIN + "User/DeleteMPIN";
    public static final String URL_CHANGE_M_PIN = URL_MAIN + "User/ChangeMPIN";
    public static final String URL_VERIFY_M_PIN = URL_MAIN + "User/VerifyMPIN";

    //Other new
    public static final String URL_INVITE_FRIENDS = URL_MAIN + "User/Invite/Mobile";
    public static final String URL_RECEIPT = URL_MAIN + "GetTransactionsCustom";
    public static final String URL_MYORDER = URL_MAIN + "MyOrders";
    public static final String URL_RESEND_EMAIL = "https://www.vpayqwik.com/User/ReSendEmailOTP";
    public static final String URL_SET_PIN = URL_MAIN + "SetCustomPin";
    public static final String URL_QUICK_GET_PAY_LIST = URL_MAIN + "User/STransactions";
    public static final String URL_QUICK_POST_PAY = URL_MAIN + "OneClickPay";
    public static final String URL_UPDATE_FAV = URL_MAIN + "User/UpdateFavourite";
    public static final String URL_DONEE_LIST = URL_MAIN_ + "DonationList";

    //Coupons API
    public static final String URL_GET_ZERCH_COUPONS = "http://52.74.231.38:8280/OfferList/1.0.0";

    public static final String URL_EMAIL_COUPONS = "http://10.1.1.56:7000/GetOffers/coupons?emailID=";

    //Redeeem Coupons
    public static final String URL_SEND_REDEEM_CODE = URL_MAIN + "User/Redeem/Code";

    //GCM
    public static final String URL_SEND_GCM_ID = "http://fgmtest.firstglobalmoney.com:8089/GCM/gcm/RegistrationId";

    //SMS
    public static final String URL_SEND_FREE_SMS = "https://www.vpayqwik.com/SendSMS";

    //Bank Transfer
    public static final String URL_LIST_BANK = URL_MAIN + "listBanks";
    public static final String URL_LIST_IFSC = URL_MAIN + "getIFSC/";
    public static final String URL_BANK_TRANSFER = URL_MAIN + "IMPS/IMPSTransaction ";
    public static final String URL_IFSC_CODE = URL_MAIN + "/IMPS/BeneficiaryList";


    public static final String URL_SHARE_POINTS = URL_MAIN + "User/SharePoints";

    //OTHER
    public static final String URL_CHECK_VERSION = "https://www.vpayqwik.com/Version/Check";
    public static final String URL_VALIDATE_TRANSACTION = URL_MAIN + "validateTransactionRequest";

    //LOAD MONEY SDK
    public static final String URL_INITIATE_LOAD_MONEY = URL_MAIN + "LoadMoney/InitiateLoadMoney";

    //Redirtect API EBS
    public static final String URL_VERIFY_TRANSACTION_EBS_KIT = "http://66.207.206.54:8035/Api/v1/User/Android/en/LoadMoney/ccLoad/MobileLanding";

    //Travel
    //Bus
    public static final String URL_GET_CITY_BUS = URL_MAIN + "Travel/Bus/getSources";
    public static final String URL_GET_LIST_BUS = URL_MAIN + "Travel/Bus/getAvailableBuses";
    public static final String URL_GET_BUS_DETAILS = URL_MAIN + "Travel/Bus/getTripDetails";
    public static final String URL_POST_BOOK_BUS = URL_MAIN + "Travel/Bus/blockSeat";


    //Flight
    public static final String URL_GET_LIST_FLIGHT_ONE_WAY = URL_MAIN + "Treval/Flight/OneWay";
    public static final String URL_FLIGHT_BOOK_ONEWAY = URL_MAIN + "Treval/Flight/OneWay";

    public static final String URL_FETCH_ONE_WAY_PRICE = URL_MAIN + "Treval/Flight/ShowPriceDetail";
    public static final String URL_FETCH_ONE_WAY_CONNCTING_PRICE = URL_MAIN + "Treval/Flight/ConnectingShowPriceDetail";
    public static final String URL_FLIGHT_CHECKOUT = URL_MAIN + "Treval/Flight/CheckOut";
    public static final String URL_FLIGHT_CHECKOUT_CONTING = URL_MAIN + "Treval/Flight/Conecting/CheckOut";
    public static final String URL_FLIGHT_CHECKOUT_ROUNDCONTING = URL_MAIN + "Treval/Flight/Roundway/Connecting/CheckOut";
    public static final String URL_FLIGHT_CHECKOUT_ROUND = URL_MAIN + "Treval/Flight/RoundwayCheckOut";
    public static final String URL_SOURCE_FLIGHT = URL_MAIN + "Treval/Flight/Source";
    public static final String URL_FLIGHT_INITIATE_LOAD_MONEY = URL_MAIN + "Treval/Flight/InitiateLoadMoney";
    public static final String URL_FLIGHT_INITIATE_LOAD_MONEY_FLIGHT_BOOKING = URL_MAIN + "Treval/Flight/BookFlight";

    public static final String URL_FLIGHT_VNET = URL_MAIN + "Flight/BookFlight/Vnet";

    //new flight api url provide by subir

    public static final String URL_GET_LIST_FLIGHT_SOURCE_DETAILS = URL_MAIN + "Treval/Flight/AirLineNames";
    public static final String URL_FLIGHT_LIST_DETAILS = URL_MAIN + "Treval/Flight/AirLineList";

    public static final String URL_FETCH_RECHECK_PRICE = URL_MAIN + "Treval/Flight/AirPriceRecheck";
    public static final String URL_FLIGHT_BOOKING = URL_MAIN + "Treval/Flight/BookTicket";

    public static final String URL_ENCASH_POINTS = URL_MAIN + "RedeemPoints/Process";
    public static final String URL_FLIGHT_GET_BOOKED_TICKETS = URL_MAIN + "Treval/Flight/MyTickets";


    //ZEPO

    private static final String ZEPO_MAIN = "http://test.api.zepo.in/";
    private static final String ZEPO_STORE = "store/";
    private static final String ZEPO_ID = "63/";


    public static final String URL_ZEPO_FETCH_PRODUCTS = ZEPO_MAIN + ZEPO_STORE + ZEPO_ID;
    public static final String URL_M_VISA_PAY = URL_MAIN + "User/VisaRequest";
    public static final String URL_M_VISA_CARD_INFO = URL_MAIN + "User/GetAccountNumber";


    //Events
    public static final String URL_EVENT_AUTH = URL_MAIN + "MeraEvents/AuthCode";
    public static final String URL_EVENT_CAT_LIST = URL_MAIN + "MeraEvents/CategoryList";
    public static final String URL_EVENT_LIST = URL_MAIN + "MeraEvents/EventList";


    //GCI- Gift Card
    public static final String URL_GIFT_CARD_CAT = URL_MAIN + "GiftCard/GetBrands";
    public static final String URL_GIFT_CARD_LIST = URL_MAIN + "GiftCard/GetBrandDenominations";
    public static final String URL_GIFT_CARD_ORDER = URL_MAIN + "GiftCard/AddOrder";

    //IPL-Predict n win
    public static final String URL_TEAM_LIST = URL_MAIN + "User/IPL/GetIPLMatchList";
    public static final String URL_TEAM_PREDICT = URL_MAIN + "User/IPL/TeamPrediction";
    public static final String URL_TEAM_CHECK_PREDICTION = URL_MAIN + "User/IPL/CheckPrediction";

    //Donation
    public static final String URL_DONATION_TIRUPATI = URL_MAIN + "SendMoney/Donate";

    //API HEADER
    public static final String H_PREPAID = "prepaidMobile|";
    public static final String H_POSTPAID = "postpaidMobile|";
    public static final String H_DTH = "dthMobile|";
    public static final String H_DATACARD = "datacardBill|";
    public static final String H_LANDLINE = "landlineBill|";
    public static final String H_ELECTRICITY = "electricityBill|";
    public static final String H_NSURANCE = "insuranceBill|";
    public static final String H_GAS = "gasBill|";
    public static final String H_SENDMONEY = "sendMoneyPay|";
    public static final String H_BANKTRANSFER = "bankTransferPay|";


    //API REPOSNE CODE
    public static final String C_SUCCESS = "S00";
    public static final String C_SESSION_EXPIRE = "F03";
    public static final String C_SESSION_BLOCKED = "H01";


    public static final String URL_LIST_QUES = URL_MAIN + "Registration/GetQuestion";

    //split payment
    public static final String URL_CHECK_BALANCE_FOR_SPLIT = URL_MAIN + "Topup/checkForSplitPay";
    public static final String URL_CHECK_BALANCE_FOR_SPLIT_BILL = URL_MAIN + "BillPayment/checkForSplitPayBillPay";
    public static final String URL_PAYMENTS = URL_MAIN + "BillPayment/Process";

    //Niki
    public static final String URL_NIKKI_GET_TOKEN = URL_MAIN + "nikki/User/GetNikkiToken";

    //Imagica
    public static final String URL_IMAGICA_LOGIN = URL_MAIN + "Adlabs/AuthMobile";
    public static final java.lang.String URL_IMAGICA_THEME_PARK_LIST = URL_MAIN + "Imagica/SearchTickets";
    public static final java.lang.String URL_IMAGICA_CREATE_ORDER = URL_MAIN + "Adlabs/OrderCreateMobile";
    public static final java.lang.String URL_IMAGICA_PAYMENT = URL_MAIN + "Adlabs/PaymentMobile";
    public static final String URL_RESEND_OTP_DEVICE = URL_MAIN + "Login/ResendDeviceBindingOTP";

    public static final String URL_CHECK_BALANCE_FOR_QWIKPAY = URL_MAIN + "checkForSplitOneClickPay";
    public static final String URL_VALIDATE_TRX_TIME = URL_MAIN + "LoadMoney/InitiateRequest";
    public static final String URL_VALIDATE_CARD_TRX_TIME = URL_MAIN + "LoadMoney/InitiateCardRequest";
    public static final String URL_VALIDATE_TRX_TIMEUPI = URL_MAIN + "LoadMoney/InitiateUPI";
    public static final String URL_VALIDATE_TRX_TIME_PAYFI = URL_MAIN + "LoadMoney/InitiateRequest";
    public static final String URL_VALIDATE_CARD_TRX_TIMEUPI = URL_MAIN + "LoadMoney/RedirectUPI";

//    public static final String URL_VALIDATE_TRX_TIME = URL_MAIN + "LoadMoney/getTrxTimeDiff";


    public static final String URL_GET_ALL_SHOPPING_PRODUCTS = "http://www.qwikrpay.com/Api/v1/User/Android/en/ShowProduct";
    public static final String URL_SHOPPONG_REMOVE_ITEM_CART = "http://www.qwikrpay.com/Api/v1/User/Android/en/removeCartItem";
    public static final String URL_SHOPPONG_ADD_ITEM_CART = "http://www.qwikrpay.com/Api/v1/User/Android/en/itemAddToCart";
    public static final String URL_SHOPPONG_SHOW_CART = "http://www.qwikrpay.com/Api/v1/User/Android/en/showCartData";
    public static final String URL_SHOPPONG_ADD_DELIVERY_ADDRESS = "http://www.qwikrpay.com/Api/v1/User/Android/en/addAddress";
    public static final String URL_SHOPPONG_DELETE_DELIVERY_ADDRESS = "http://www.qwikrpay.com/Api/v1/User/Android/en/deleteAddress";
    public static final String URL_SHOPPONG_CHECKOUT_PROCESS = "http://www.qwikrpay.com/Api/v1/User/Android/en/checkOutProcess";
    public static final String URL_EXPENSE_TRACKER = URL_MAIN + "User/UpdateReceipt";
    public static final String URL_FETCH_NOTIFICATIONS = "http://66.207.206.54:8043/Api/v1/User/Android/en/User/getNotifications";
    public static final String URL_SHOPPING_CATEGORY_LIST = "http://www.qwikrpay.com/Api/v1/User/Android/en/country/categoryList";
    public static final String URL_SHOPPING_SUB_CATEGORY_LIST = "http://www.qwikrpay.com/Api/v1/User/Android/en/category/SubcategoryList";
    public static final String URL_SHOPPING_BRAND_LIST = "http://www.qwikrpay.com/Api/v1/User/Android/en/subCategory/brandList";
    public static final String URL_SHOPPONG_GO_FOR_DELIVERY = "http://www.qwikrpay.com/Api/v1/User/Android/en/gofordelivery";
    public static final String URL_YUP_TV_REGISTER = URL_MAIN + "YuppTV/Register";
    public static final String URL_GET_ALL_SHOPPING_PAYMENT = "http://www.qwikrpay.com/Api/v1/User/Android/en/Payment";
    public static final String URL_SHOPPONG_EDIT_DELIVERY_ADDRESS = "http://www.qwikrpay.com/Api/v1/User/Android/en/updateAddress";
    public static final String URL_SHOPPING_MY_ORDER = "http://www.qwikrpay.com/Api/v1/User/Android/en/myOrder";

    public static final String URL_GET_LIST_COMPONTENTS_CUSTOMRER = "http://letsmanage.in/Api/V1/MobileTicket/Component";
    public static final String URL_CREAT_TICKET = "http://letsmanage.in/Api/V1/MobileTicket/Ticket";
    //public static final String URL_GET_TICKET_LIST=""
    public static final String URL_GET_TICKET_LIST = "http://letsmanage.in/Api/V1/MobileTicket/GetTicket/Email";
    public static final String URL_VER_CHECK = URL_MAIN + "CheckVersion";
    public static final String URL_PLACEORDER = URL_MAIN + "Imagica/PlaceOrder";
    public static final String URL_PLACEPAYMENT = URL_MAIN + "Imagica/ProcessPayment";
    public static final String URL_REFER_AND_EARN = URL_MAIN + "User/getMobileList";

    //Bus new
    public static final String URL_BUS_GET_CITY = URL_MAIN + "Travel/Bus/GetCityList";
    public static final String URL_GET_LIST_BUS_NEW = URL_MAIN + "Travel/Bus/GetAllAvailableTrips";
    public static final String URL_BUS_SEAT_DETAILS = URL_MAIN + "Travel/Bus/GetSeatDetails";
    public static final String URL_BUS_SEAT_TRANXID = URL_MAIN + "Travel/Bus/GetTransactionId";
    public static final String URL_BUS_SEAT_BOOKING = URL_MAIN + "Travel/Bus/BookTicketUpdated";
    public static final String URL_BUS_CANCEL_INITIATION = URL_MAIN + "Travel/Bus/cancelInitPayment";
    public static final String URL_BUS_GET_BOOKED_TICKETS = URL_MAIN + "Travel/Bus/MyTickets";
    public static final String URL_BUS_GET_SEAT_SAVE_DETAILS = URL_MAIN + "Travel/Bus/SaveSeat";


    private static final String URL_TEST = "https://www.tripay.in/Api/";
    public static final String URL_DOMAIN_lOAD = URL_DOMAIN;

    public static final String URL_BANK_TRANSFERS = URL_MAIN + "SendMoney/Bank";
    public static final String URL_BANK_CRASH = URL_MAIN + "User/MobileCrashReport";

    public static final String URL_LIST_BANKS = URL_MAIN + "/SendMoney/GetBankDetails";


//      Woohoo Gft Card

//    public static final String URL_WOOHOO_CATEGORIES = "http://66.207.206.54:8035/Paul/Api/v1/User/Android/en/Woohoo/categories";

    public static final String URL_WOOHOO_PRODUCT_LIST = "http://66.207.206.54:8035/Paul/Api/v1/User/Android/en/Woohoo/ProductList";
    public static final String URL_WOOHOO_PRODUCT = "http://66.207.206.54:8035/Paul/Api/v1/User/Android/en/Woohoo/products/";
    public static final String URL_WOOHOO_RECHECKPRICE = "http://66.207.206.54:8035/Paul/Api/v1/User/Android/en/Woohoo/recheckPrice";
    public static final String URL_WOOHOO_PAYMENT = "http://66.207.206.54:8035/Paul/Api/v1/User/Android/en/Woohoo/processTransaction";

    public static final String URL_MATCHMOVE_CARDDETAILS = URL_MAIN + "GenerateVirtualCard";
    public static final String URL_MATCHMOVE_CARDDETAILSFETCH = URL_MAIN + "FetchVirtualCard";
    public static final String URL_MATCHMOVE_GENRATEPHYSICALCARD = URL_MAIN + "PhysicalCardRequest";

    public static final String URL_ADD_PHYSICALCARD = URL_MAIN + "AddPhysicalCard";
    public static final String URL_ACTIVATE_PHYSICAL_CARD = URL_MAIN + "ActivatePhysicalCard";
    public static final String URL_MATCHMOVE_PhysicalCardFETCH = URL_MAIN + "FetchPhysicalCard";
    public static final String URL_RESEND_ACTIVATION_CODE = URL_MAIN + "ResendActivationCode";
    public static final String URL_ACTIVATION_CARD = URL_MAIN + "CustomActivatePhysicalCardNew";
    public static final String URL_PROMO_CODE = URL_MAIN + "requestPromo";
    public static final String URL_KYC = URL_MAIN + "SubmitKYC";

    //Block Virtual Card
    public static final String URL_BLOCK_VIRTUAL_CARD = URL_MAIN + "BlockCard";

    // Reactivate Virtual Card
    public static final String URL_REACTIVATE_CARD = URL_MAIN + "ReactivateCard";

    public static final String URL_VALIDATE_MAIL = URL_MAIN_ + "ValidateEmail";


//FINGOOL

    public static final String URL_FINGOOL_SERVICES = URL_MAIN_ + "Fingoole/Services";
    public static final String URL_FINGOOL_AMOUNT = URL_MAIN_ + "Fingoole/Amount";
    public static final String URL_FINGOOL_REGISTER = URL_MAIN_ + "Fingoole/Register";
    public static final String URL_FINGOOL_PROCESS = URL_MAIN_ + "Fingoole/Process";
    public static final String URL_FINGOOL_HISTORY = URL_MAIN_ + "Fingoole/TransactionList";


}
