package in.MadMoveGlobal.metadata;

import android.content.Context;

import java.util.ArrayList;

import in.MadMoveGlobal.model.MainMenuModel;
import in.MadMoveGlobal.model.OperatorsModel;
import in.MadMoveGlobal.EwireRuPay.R;

public class MenuMetadata {

    public static ArrayList<MainMenuModel> getMenu(Context context) {
        ArrayList<MainMenuModel> catItems = new ArrayList<>();
        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu1) + "", R.drawable.ic_prepaid, R.color.menuB));
//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu2) + "", R.drawable.menu_new_pay_bills1, R.color.menuG));
        //all out payments
        catItems.add(new MainMenuModel( context.getResources().getString(R.string.topup_data) + "", R.drawable.ic_datacard_icon, R.color.menu7));
        catItems.add(new MainMenuModel(context.getResources().getString(R.string.topup_postpaid) + "", R.drawable.menu_new_mobile_topup1, R.color.menu7));
        catItems.add(new MainMenuModel(context.getResources().getString(R.string.bill_landline) + "", R.drawable.ic_landline_icon, R.color.menu7));
        catItems.add(new MainMenuModel(context.getResources().getString(R.string.bill_gas) + "", R.drawable.ic_gas_icon, R.color.menu7));
        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu3) + "", R.drawable.menu_dth_new1, R.color.menu7));

        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu4) + "", R.drawable.menu_send_money, R.color.menu5));
        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu5) + "", R.drawable.menu_new_cards, R.color.menu8));
        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu6) + "", R.drawable.menu_new_qr_donate1, R.color.menuR));
        catItems.add(new MainMenuModel(context.getResources().getString(R.string.bill_electricity) + "", R.drawable.menu_electricity, R.color.menu9));
        catItems.add(new MainMenuModel(context.getResources().getString(R.string.bill_insurance) + "", R.drawable.menu_insurance, R.color.menu2));
       // catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu15) + "", R.drawable.menu_new_bank_transfer1, R.color.menu7));
        catItems.add(new MainMenuModel(context.getResources().getString(R.string.premium_insurance) + "", R.drawable.menu_new_insurance, R.color.menu8));
        catItems.add(new MainMenuModel(context.getResources().getString(R.string.Bus) + "", R.drawable.menu_bus, R.color.menuB));
        catItems.add(new MainMenuModel(context.getResources().getString(R.string.Offers) + "", R.drawable.ic_offers, R.color.menu7));

        catItems.add(new MainMenuModel(context.getResources().getString(R.string.vehicle_insurance) + "", R.drawable.menu_insurance, R.color.menu2));

//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.Flight) + "", R.drawable.menu_flight, R.color.menu7));
        // catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu16)+"", R.drawable.menu_new_health_care,R.color.menu7));


//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.nav18) + "", R.drawable.ic_big_basket, R.color.menu5));
//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.nav19) + "", R.drawable.menu_flight, R.color.menu5));

//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu13)+"", R.drawable.menu_new_entertainment,R.color.menu3));


//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu7) + "", R.drawable.menu_new_redeem_coupon, R.color.menu7));
//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu6)+"", R.drawable.menu_new_coupns,R.color.menuG));

//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu24) + "", R.drawable.menu_new_pay_store, R.color.menu9));
//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu17)+"", R.drawable.menu_new_gift,R.color.menu7));
//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu19)+"", R.drawable.menu_split_money,R.color.menu7));
//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu18) + "", R.drawable.menu_request_money, R.color.menu7));

//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu16) + "", R.drawable.menu_new_health_care, R.color.menuR));

//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu21)+"", R.drawable.menu_share_points,R.color.menu7));
//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu26) + "", R.drawable.menu_new_invite, R.color.menu7));


//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu22)+"", R.drawable.menu_bus,R.color.menu7));
//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu11)+"", R.drawable.menu_new_food,R.color.menu8));
//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu11)+"", R.drawable.menu_new_food,R.color.menu1));
//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu17) + "", R.drawable.menu_new_gift, R.color.menu1));
//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu29) + "", R.drawable.menu_new_qr_donate, R.color.menu9));
//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu28) + "", R.drawable.menu_new_pay_bills, R.color.menu7));
//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu27) + "", R.drawable.menu_new_entertainment, R.color.menuG));
//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu28) + "", R.drawable.menu_new_adventure_new, R.color.menu7));
//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu30) + "", R.drawable.menu_new_chat_new, R.color.menu7));
//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu12) + "", R.drawable.menu_new_shopping_new, R.color.menu8));
//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu31) + "", R.drawable.menu_new_yupptv_new, R.color.menu8));
//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu22)+"", R.drawable.menu_bus,R.color.menu7));
        return catItems;

    }

    public static ArrayList<OperatorsModel> getPostOperator() {
        ArrayList<OperatorsModel> opItems = new ArrayList<>();
        opItems.add(new OperatorsModel("", "Select your operator", ""));
        opItems.add(new OperatorsModel("ACCIN", "Aircel", "ACCIN"));
        opItems.add(new OperatorsModel("ATCIN", "Airtel", "ATCIN"));
        opItems.add(new OperatorsModel("BGCIN", "BSNL", "BGCIN"));
        opItems.add(new OperatorsModel("IDCIN", "Idea", "IDCIN"));
        opItems.add(new OperatorsModel("RGCIN", "Reliance", "RGCIN"));
        opItems.add(new OperatorsModel("TDCIN", "Tata Docomo", "TDCIN"));
        opItems.add(new OperatorsModel("VFCIN", "Vodafone", "VFCIN"));
        return opItems;

    }

    public static ArrayList<OperatorsModel> getDTH() {
        ArrayList<OperatorsModel> opItems = new ArrayList<>();
        opItems.add(new OperatorsModel("", "Select DTH Provider", ""));
        opItems.add(new OperatorsModel("ATVIN", "Airtel Digital TV", "ATVIN"));
        opItems.add(new OperatorsModel("STVIN", "Sun Direct", "STVIN"));
        opItems.add(new OperatorsModel("RTVIN", "Reliance Digital TV", "RTVIN"));
        opItems.add(new OperatorsModel("DTVIN", "Dish TV", "DTVIN"));
        opItems.add(new OperatorsModel("OTVIN", "Tata Sky", "OTVIN"));
        opItems.add(new OperatorsModel("VTVIN", "Videocon D2H", "VTVIN"));
        return opItems;
    }

    public static ArrayList<OperatorsModel> getInsurance() {
        ArrayList<OperatorsModel> opItems = new ArrayList<>();
        opItems.add(new OperatorsModel("", "Select Insurance Provider", ""));
        opItems.add(new OperatorsModel("IPIIN", "ICICI Prudential Life Insurance", "IPIIN"));
        opItems.add(new OperatorsModel("TAIIN", "Tata AIA Life Insurance", "TAIIN"));
        opItems.add(new OperatorsModel("ILIIN", "IndiaFirst Life Insurance", "ILIIN"));
        opItems.add(new OperatorsModel("BAIIN", "Bharti AXA Life Insurance", "BAIIN"));

        return opItems;
    }

    public static ArrayList<OperatorsModel> getLandLine() {
        ArrayList<OperatorsModel> opItems = new ArrayList<>();
        opItems.add(new OperatorsModel("", "Select Landline Provider", ""));
        opItems.add(new OperatorsModel("ATLIN", "Airtel", "ATLIN"));
        opItems.add(new OperatorsModel("BGLIN", "BSNL", "BGLIN"));
        opItems.add(new OperatorsModel("MDLIN", "MTNL Delhi", "MDLIN"));
        opItems.add(new OperatorsModel("RGLIN", "Reliance", "RGLIN"));
        opItems.add(new OperatorsModel("TCLIN", "Tata Docomo", "TCLIN"));
        return opItems;

    }

    public static ArrayList<OperatorsModel> getGas() {
        ArrayList<OperatorsModel> opItems = new ArrayList<>();
        opItems.add(new OperatorsModel("", "Select Gas Provider", ""));
        opItems.add(new OperatorsModel("IPGIN", "Indraprastha Gas", "IPGIN"));
        opItems.add(new OperatorsModel("MMGIN", "Mahanagar Gas", "MMGIN"));
        opItems.add(new OperatorsModel("GJGIN", "Gujarat Gas", "GJGIN"));
        opItems.add(new OperatorsModel("ADGIN", "Adani Gas", "ADGIN"));
        return opItems;

    }

    public static ArrayList<OperatorsModel> getgovtId() {
        ArrayList<OperatorsModel> opItems = new ArrayList<>();
        opItems.add(new OperatorsModel("", "SELECT ID", ""));
        opItems.add(new OperatorsModel("0", "AADHAAR", "aadhaar"));
        opItems.add(new OperatorsModel("1", "DRIVING LICENSE", "drivers_id"));
        opItems.add(new OperatorsModel("2", "PASSPORT", "passport"));
        opItems.add(new OperatorsModel("3", "VOTER ID", "voters_id"));
        return opItems;


    }

    public static ArrayList<OperatorsModel> getEkycId() {
        ArrayList<OperatorsModel> opItems = new ArrayList<>();
        opItems.add(new OperatorsModel("", "SELECT ID", ""));
        opItems.add(new OperatorsModel("0", "AADHAAR", "aadhaar"));
//        opItems.add(new OperatorsModel("1", "PAN CARD", "pan"));
//        opItems.add(new OperatorsModel("2", "PASSPORT", "passport"));
        opItems.add(new OperatorsModel("1", "VOTER ID", "voters_id"));
        opItems.add(new OperatorsModel("2", "DRIVING LICENSE", "drivers_id"));
        opItems.add(new OperatorsModel("3", "PASSPORT", "passport"));
        return opItems;


    }


    public static ArrayList<OperatorsModel> getElectricity() {
        ArrayList<OperatorsModel> opItems = new ArrayList<>();
        opItems.add(new OperatorsModel("", "Select Electricity Provider", ""));
        opItems.add(new OperatorsModel("TTEIN", "TSECL - TRIPURA", "TTEIN"));
        opItems.add(new OperatorsModel("TPEIN", "Torrent Power", "TPEIN"));
        opItems.add(new OperatorsModel("NDEIN", "Tata Power - DELHI", "NDEIN"));
        opItems.add(new OperatorsModel("STEIN", "Southern Power -TELANGANA", "STEIN"));
        opItems.add(new OperatorsModel("SAEIN", "Southern Power - ANDHRA PRADESH", "SAEIN"));
        opItems.add(new OperatorsModel("REEIN", "Reliance Energy - MUMBAI", "REEIN"));
        opItems.add(new OperatorsModel("MPEIN", "Paschim Kshetra Vitaran - MADHYA PRADESH", "MPEIN"));
        opItems.add(new OperatorsModel("DOEIN", "Odisha Discoms - ODISHA", "DOEIN"));
        opItems.add(new OperatorsModel("NUEIN", "Noida Power - NOIDA", "NUEIN"));

        opItems.add(new OperatorsModel("MDEIN", "MSEDC - MAHARASHTRA", "MDEIN"));
        opItems.add(new OperatorsModel("MMEN", "Madhya Kshetra Vitaran - MADHYA PRADESH", "MMEN"));
        opItems.add(new OperatorsModel("DREIN", "Jodhpur Vidyut Vitran Nigam - RAJASTHAN", "DREIN"));

        opItems.add(new OperatorsModel("JUEIN", "Jamshedpur Utilities & Services (JUSCO)", "JUEIN"));
        opItems.add(new OperatorsModel("JREIN", "Jaipur Vidyut Vitran Nigam - RAJASTHAN", "JREIN"));
        opItems.add(new OperatorsModel("IPEIN", "India Power", "IPEIN"));
        opItems.add(new OperatorsModel("DNEIN", "DNHPDCL - DADRA & NAGAR HAVELI", "DNEIN"));
        opItems.add(new OperatorsModel("DHEIN", "DHBVN - HARYANA", "DHEIN"));

        opItems.add(new OperatorsModel("CCEIN", "CSEB - CHHATTISGARH", "CCEIN"));
        opItems.add(new OperatorsModel("CWEIN", "CESC - WEST BENGAL", "CWEIN"));
        opItems.add(new OperatorsModel("BYEIN", "BSES Yamuna - DELHI", "BYEIN"));
        opItems.add(new OperatorsModel("BREIN", "BSES Rajdhani - DELHI", "BREIN"));
        opItems.add(new OperatorsModel("BMEIN", "BEST Undertaking - MUMBAI", "BMEIN"));
        opItems.add(new OperatorsModel("BBEIN", "BESCOM - BENGALURU", "BBEIN"));
        opItems.add(new OperatorsModel("AAEIN", "APDCL - ASSAM", "AAEIN"));
        opItems.add(new OperatorsModel("AREIN", "Ajmer Vidyut Vitran Nigam - RAJASTHAN", "AREIN"));

        return opItems;
    }
}