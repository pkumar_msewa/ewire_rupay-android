package in.MadMoveGlobal.metadata;

import android.widget.TextView;

import java.util.ArrayList;

import in.MadMoveGlobal.model.BusCityModel;
import in.MadMoveGlobal.model.QwikPayModel;
import in.MadMoveGlobal.model.StatementModel;
import in.MadMoveGlobal.EwireRuPay.R;


/**
 * Created by Ksf on 3/27/2016.
 */
public class AppMetadata {


    public static String FRAGMENT_TYPE = "NAVIGATION";
    public static String YOUTUBE_DEVELOPER_KEY = "AIzaSyDgA5zCjZAOgR4Ccp9gthSJvI4XwaCwgAI";
    public static String TELEBUY_ID = "";
    public static String appKey = "1478673778";
    public static String appSecret = "H/79xKHJBWCubOl9vjkpEBkcpDlVGPNssD6FP6sVY4c=";
//    public static String appKey = "1495104428";
//    public static String appSecret = "PbytNScI5BCga4E7zcABSLjX3vp1mnkv";


    //Flight And Images

    public static int getFLightImage(String imageName, TextView tvFlightListName) {
        switch (imageName) {
            case "SG":
                tvFlightListName.setText("SpiceJet");
                return R.drawable.flight_spice_jet;
            case "9W":
                tvFlightListName.setText("JetAirways");
                return R.drawable.flight_jet_airways;
            case "6E":
                tvFlightListName.setText("Indigo");
                return R.drawable.flight_indigo;
            case "AI":
                tvFlightListName.setText("AirIndia");
                return R.drawable.flight_air_india;
            case "I5":
                tvFlightListName.setText("AirAsia");
                return R.drawable.flight_air_asia;
            case "UK":
                tvFlightListName.setText("Vistara");
                return R.drawable.flight_vistara;
            case "G8":
                tvFlightListName.setText("GoAir");
                return R.drawable.flight_go_air;
            default:
                tvFlightListName.setText("");
                return R.drawable.ic_no_flight;

        }
    }

    public static String getErrorCode(int ErrorCode) {
        switch (ErrorCode) {
            case 10002:

                return "Merchant Authentication failed";
            case 21001:

                return "order_id:Required parameter missing";
            case 21002:

                return "currency: Required paramter missing";
            case 21003:

                return "amount:Required parameter missing";
            case 21004:

                return "billing_name: Required parameter missing";
            case 21005:

                return "billing_address: Required parameter missing";
            case 21006:

                return "billing_city: Required parameter missing";

            case 21007:

                return "billing_state: Required parameter missing";
            case 21008:

                return "billing_zip: Required parameter missing";
            case 21009:

                return "billing_country: Required parameter missing";
            case 21010:

                return "billing_tel: Required parameter missing";
            case 21011:

                return "billing_email: Required parameter missing";
            case 21012:

                return "delivery_name: Required parameter missing";
            case 21013:

                return "delivery_address: Required parameter missing";
            case 21014:

                return "delivery_city: Required parameter missing";
            case 21015:

                return "delivery_state: Required parameter missing";
            case 21016:

                return "delivery_zip: Required parameter missing";
            case 21017:

                return "delivery_country: Required parameter missing";
            case 21018:

                return "delivery_tel: Required parameter missing";

            case 21020:

                return "card_name: Required parameter missing";
            case 21021:

                return "card_type: Required parameter missing";
            case 21022:

                return "payment_option: Required parameter missing";
            case 21023:

                return "card_number: Required parameter missing";
            case 21024:

                return "expiry_month: Required parameter missing";

            case 21025:

                return "expiry_year: Required parameter missing";
            case 21026:

                return "cvv_number: Required parameter missing";
            case 21027:

                return "issuing_bank: Required parameter missing";
            case 21028:

                return "emi_plan_id: Required parameter missing";
            case 21029:

                return "emi_tenure_id: Required parameter missing";


            case 21031:

                return "mm_id: Required parameter missing";
            case 21032:

                return "otp: Required parameter missing";
            case 31001:

                return "order_id:Invalid Parameter";
            case 31002:

                return "currency: Invalid Parameter";
            case 31003:

                return "amount:Invalid Parameter";
            case 31004:

                return "billing_name: Invalid Parameter";
            case 31005:

                return "billing_address: Invalid Parameter";
            case 31006:

                return "billing_city: Invalid Parameter";

            case 31007:

                return "billing_state: Invalid Parameter";
            case 31008:

                return "billing_zip: Invalid Parameter";
            case 31009:

                return "billing_country: Invalid Parameter";
            case 31010:

                return "billing_tel: Invalid Parameter";
            case 31011:

                return "billing_email: Invalid Parameter";
            case 31012:

                return "delivery_name: Invalid Parameter";
            case 31013:

                return "delivery_address: Invalid Parameter";
            case 31014:

                return "delivery_city: Invalid Parameter";
            case 31015:

                return "delivery_state: Invalid Parameter";
            case 31016:

                return "delivery_zip: Invalid Parameter";
            case 31017:

                return "delivery_country: Invalid Parameter";
            case 31018:

                return "delivery_tel: Invalid Parameter";

            case 31020:

                return "card_name: Invalid Parameter";
            case 31021:

                return "card_type: Invalid Parameter";
            case 31022:

                return "payment_option: Invalid Parameter";
            case 31023:

                return "card_number: Invalid Parameter";
            case 31024:

                return "expiry_month: Invalid Parameter";

            case 31025:

                return "expiry_year: Invalid Parameter";
            case 31026:

                return "cvv_number: Invalid Parameter";
            case 31027:

                return "issuing_bank: Invalid Parameter";
            case 31028:

                return "emi_plan_id: Invalid Parameter";
            case 31029:

                return "emi_tenure_id: Invalid Parameter";

            case 31030:

                return "order_uid: Invalid Parameter";


            case 31031:

                return "mm_id: Invalid Parameter";
            case 31032:

                return "otp: Invalid Parameter";


            case 31034:

                return "Selected EMI facility is not valid for entered card";
            default:

                return "Oh no! Some thing is wrong, Please try again";
        }
    }

    public static ArrayList<QwikPayModel> getQwikPay() {
        ArrayList<QwikPayModel> payModel = new ArrayList<>();
        payModel.add(new QwikPayModel("Fund Transfer", "1472901067000", "Fund transfer to no. 8800241972", "15", true, "Prepaid"));
        payModel.add(new QwikPayModel("Prepaid Mobile Topup", "1472901067000", "Mobile topup to no. 7204943677", "200", false, "PostPaid"));
        payModel.add(new QwikPayModel("Data Pack Topup", "1472901067000", "Data pack topup to no. 8800241872", "200", false, "Dth"));
        payModel.add(new QwikPayModel("DTH Recharge", "1472901067000", "DTH recharge to a/c no 2900023923", "500", false, "Electricity"));
        payModel.add(new QwikPayModel("Fund Transfer", "1472901067000", "Amount transfer to no. 7204943677", "3000", true, "landline"));
        payModel.add(new QwikPayModel("Postpaid Mobile Topup", "1472901067000", "Mobile topup to no. 9841555787", "2000", false, "load Money"));
        return payModel;
    }


    public static ArrayList<BusCityModel> getBusCities() {
        ArrayList<BusCityModel> cityModel = new ArrayList<>();
        cityModel.add(new BusCityModel(1059, "Delhi"));
        cityModel.add(new BusCityModel(5523, "Mumbai, Thane"));
        cityModel.add(new BusCityModel(897, "Bangalore"));
        cityModel.add(new BusCityModel(682, "Pune"));
        cityModel.add(new BusCityModel(501, "Hyderabad"));
        cityModel.add(new BusCityModel(567, "Chennai"));
        cityModel.add(new BusCityModel(1705, "Ahmedabad"));
        cityModel.add(new BusCityModel(1268, "Mumbai"));
        cityModel.add(new BusCityModel(919, "Gurgaon"));


        return cityModel;
    }


    public static ArrayList<StatementModel> getStatement() {
        ArrayList<StatementModel> statementList = new ArrayList<StatementModel>();
//        statementList.add(new StatementModel("200.00", "100.00", "05-05-2016 05:39 PM", "TopUp", "Success", "A204", "Successfully recharged on 8800241872", true));
//        statementList.add(new StatementModel("120.00", "20.00", "05-05-2016 01:39 PM", "Transfer", "Success", "A205", "Successfully transferred to 8800241872", true));
//        statementList.add(new StatementModel("1200.00", "600.32", "05-05-2016 01:39 PM", "Bill Pay", "Pending", "A207", "Electricity payment to BSES Rajdhani- Delhi", true));
//        statementList.add(new StatementModel("1200.00", "2000.00", "05-05-2016 01:39 PM", "Load Money", "Failure", "F223", "Loading INR 2000 failed", false));
//        statementList.add(new StatementModel("1200.00", "2000.00", "05-05-2016 02:39 PM", "Load Money", "Success", "A223", "Loading INR 2000 sucess", true));
        return statementList;
    }

    public static String getInvalidSession() {
        String source = "<b><font color=#000000> Session Expired.</font></b>" +
                "<br><br><b><font color=#ff0000> Please login again to use E-WIRE.</font></b><br></br>";
        return source;
    }


    public static String getSuccessMPinReset() {
        String source = "<b><font color=#000000> MPIN Reset Successfully.</font></b>" +
                "<br><br><b><font color=#ff0000> Please create new MPIN in next step.</font></b><br></br>";
        return source;
    }

    public static String getLogout() {
        String source = "<b><font color=#000000> This will clear your session.</font></b>" +
                "<br><br><b><font color=#ff0000> Are you sure you want to log out?</font></b><br></br>";
        return source;
    }


    public static String getBlockSession() {
        String source = "<b><font color=#000000> Account has been blocked.</font></b>" +
                "<br><br><b><font color=#ff0000> Suspicious behaviour detected, Please contact customer care</font></b><br></br>";
        return source;
    }


}
