package in.MadMoveGlobal.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;

import com.android.volley.RequestQueue;
//import com.ebs.android.sdk.Config.Encryption;
//import com.ebs.android.sdk.Config.Mode;
//import com.ebs.android.sdk.EBSPayment;
//import com.ebs.android.sdk.PaymentRequest;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONObject;

import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.fragment.fragmentuser.RegisterFragment;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.EwireRuPay.R;

/**
 * Created by Ksf on 4/11/2016.
 */
public class PswMpinFragment extends Fragment {

    private View rootView;
    private MaterialEditText etLoadMoneyAmount;
    private Button btnBackPswprv;
    private RadioButton rbLoadMoneyVBank, rbLoadMoneyOther;
    private View focusView = null;
    private boolean cancel;
    private String amount = null;
    AlertDialog.Builder payDialog;
    private UserModel session = UserModel.getInstance();
    private LoadingDialog loadDlg;
    private RequestQueue rq;
    private boolean isVBank = true;
    private String inValidMessage = "";
    private String tag_json_obj = "load_money";
    private JSONObject jsonRequest;
    String autoFill;
    double loadAmount;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.frgment_paswmpin, container, false);
        btnBackPswprv = (Button)rootView.findViewById(R.id.btnBackPsw);

        btnBackPswprv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RegisterFragment fragment = new RegisterFragment();
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.llLRMain, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        return rootView;
    }
}
