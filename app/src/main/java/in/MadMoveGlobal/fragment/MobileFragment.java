package in.MadMoveGlobal.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import in.MadMoveGlobal.metadata.AppMetadata;
import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.EwireRuPay.activity.MainMenuDetailActivity;

/**
 * Created by acer on 10-10-2017.
 */

public class MobileFragment extends Fragment {

    private View rootView;
    private ImageButton ibPrepaid, ibPostpaid, ibDataCard;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        onDetach();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_mobile, container, false);
        onDetach();
        ibPrepaid = (ImageButton) rootView.findViewById(R.id.ibPrepaid);
        ibPostpaid = (ImageButton) rootView.findViewById(R.id.ibPostpaid);
        ibDataCard = (ImageButton) rootView.findViewById(R.id.ibDataCard);


        ibPrepaid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), MainMenuDetailActivity.class).putExtra("postion","0").putExtra(AppMetadata.FRAGMENT_TYPE, "MobileTopUp"));


            }
        });

        ibPostpaid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(getActivity(), MainMenuDetailActivity.class).putExtra("postion","1").putExtra(AppMetadata.FRAGMENT_TYPE, "MobileTopUp"));

            }
        });


        ibDataCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(getActivity(), MainMenuDetailActivity.class).putExtra("postion","2").putExtra(AppMetadata.FRAGMENT_TYPE, "MobileTopUp"));

            }
        });


        return rootView;
    }

    @Override
    public void onDetach() {
        super.onDetach();

        //hide keyboard when any fragment of this class has been detached

    }



}
