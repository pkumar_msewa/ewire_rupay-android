package in.MadMoveGlobal.fragment.fragmentissues;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.MadMoveGlobal.adapter.AllIssuesListAdapter;
import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.model.ListIssueModel;
import in.MadMoveGlobal.model.ListcomponentModel;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;

/**
 * Created by Dushant on 08/27/2017.
 */

/*import in.payqwik.adapter.AllIssuesListAdapter;
import in.payqwik.custom.LoadingDialog;
import in.payqwik.metadata.ApiUrl;
import in.payqwik.model.ListIssueModel;
import in.payqwik.model.ListcomponentModel;
import in.payqwik.model.UserModel;
import in.payqwik.test.PayQwikApplication;
import in.payqwik.test.R;*/

public class AllissueFragment extends android.support.v4.app.Fragment {

    private ListView lvCustomer;
    private View rootView;
    private TextView ticketNo, status, description, component, date;
    private Spinner spnComponent;
    private LoadingDialog loadingDialog;
    private JSONObject jsonRequest;
    private String tag_json_obj = "json_user";
    private UserModel userModel = UserModel.getInstance();
    private ArrayList<ListcomponentModel> listcomponentModels;
    private ArrayList<ListIssueModel> listIssueModelArrayList;
    private TextView tvError;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_allissue_fragment, container, false);
        spnComponent = (Spinner) rootView.findViewById(R.id.spnComponent);
        lvCustomer = (ListView) rootView.findViewById(R.id.lvCustomer);
        loadingDialog = new LoadingDialog(getActivity());
        tvError = (TextView) rootView.findViewById(R.id.tvError);
        tvError.setVisibility(View.GONE);

        attemptRegister();
        return rootView;
    }

    public void attemptRegister() {
        loadingDialog.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("merchantCode", "LETS10031");
            jsonRequest.put("projectCode", "LETSPRO32301");
            jsonRequest.put("email", userModel.getUserEmail());

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("GET USER PARAMS", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_GET_TICKET_LIST, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.i("GET USER PARAMS", response.toString());
                    Log.i("RESPONSEURL", ApiUrl.URL_GET_TICKET_LIST);
                    try {
                        String code = response.getString("code");

                        if (code != null && code.equals("S00")) {
                            loadingDialog.dismiss();
//                            String jsonString = response.getString("response");
                            //JSONObject jsonObject = new JSONObject(jsonString);
                            //String jsonString = response.getString("details");
                            JSONArray jsonArray = response.getJSONArray("details");
                            listIssueModelArrayList = new ArrayList<>();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                long Id = jsonArray.getJSONObject(i).getLong("id");
                                String TicketNo = jsonArray.getJSONObject(i).getString("ticketNo");
                                String Description = jsonArray.getJSONObject(i).getString("description");
                                String Status = jsonArray.getJSONObject(i).getString("status");
                                String component = jsonArray.getJSONObject(i).getString("component");
                                boolean New = jsonArray.getJSONObject(i).getBoolean("new");
                                long date = jsonArray.getJSONObject(i).getLong("created");
                                ListIssueModel listIssueModel = new ListIssueModel(Id, TicketNo, Description, Status, New, component, date);
                                listIssueModelArrayList.add(listIssueModel);

                            }
                        }
                        if (listIssueModelArrayList != null && listIssueModelArrayList.size() != 0) {
                            AllIssuesListAdapter compountSpinnerAdapter = new AllIssuesListAdapter(getActivity(), listIssueModelArrayList);
                            loadingDialog.dismiss();
                            lvCustomer.setAdapter(compountSpinnerAdapter);
                        }
                    } catch (JSONException e) {
                        loadingDialog.dismiss();
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadingDialog.dismiss();
                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hash", "1234");
                    return map;
                }

            };

            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

}
