package in.MadMoveGlobal.fragment.fragmentmobilerecharge;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Html;
import android.text.Spanned;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.orm.query.Select;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.MadMoveGlobal.EwireRuPay.WebViewsActivity;
import in.MadMoveGlobal.EwireRuPay.activity.MainActivity;
import in.MadMoveGlobal.adapter.OperatorSpinnerAdapter;
import in.MadMoveGlobal.custom.CustomAlertDialog;
import in.MadMoveGlobal.custom.CustomBlockDialog;
import in.MadMoveGlobal.custom.CustomServiceDialogSearch;
import in.MadMoveGlobal.custom.CustomSuccessDialog;
import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.custom.VerifyMPinDialog;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.metadata.AppMetadata;
import in.MadMoveGlobal.model.CircleModel;
import in.MadMoveGlobal.model.OperatorsModel;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.util.CheckLog;
import in.MadMoveGlobal.util.GenerateAPIHeader;
import in.MadMoveGlobal.util.MPinVerifiedListner;
import in.MadMoveGlobal.util.NetworkErrorHandler;
import in.MadMoveGlobal.util.PayingDetailsValidation;
import in.MadMoveGlobal.util.ServicesSelectedListener;
import in.MadMoveGlobal.util.Utility;


//import static in.MadMoveGlobal.metadata.ApiUrl.URL_CIRCLE_OPERATORPOST;


/**
 * Created by Ksf on 3/16/2016.
 */
public class PostpaidFragment extends Fragment implements MPinVerifiedListner {
    private View rootView;
    private MaterialEditText edt_mobile_number_postpaid;
    private ImageButton ibtnPhoneBook;
    private Spinner spinner_mob_provider_postpaid;
    private MaterialEditText edt_mob_amount_postpaid;
    private Button btn_pay_mob_postpaid;
    private OperatorSpinnerAdapter spinnerAdapterOperator;
    Integer DueAmounts;
    private List<CircleModel> circleList;
    private List<OperatorsModel> operatorList;
    private ArrayList<OperatorsModel> operatorLists;

    private static HashMap<String, Integer> operatorIdCode;
    private static HashMap<String, Integer> circleIdCode;

    private OperatorSpinnerAdapter operatorSpinnerAdapter;
    private LoadingDialog loadDlg;

    private UserModel session = UserModel.getInstance();
    private boolean cancel;
    private View focusView = null;
    private String operatorCode, selectedFromServiceCode, selectedFromServiceName, mobileNumber;
    private LinearLayout llLayouts;

    private String amount, toMobileNumber, serviceProvider, serviceProviderName;
    private ArrayList<OperatorsModel> operator;
    private JSONObject jsonRequest;

    private static final int PICK_CONTACT = 1;
    private static final int PLUS_SIGN_POS = 0;
    private static final int MOBILE_DIGITS = 10;
    private MaterialEditText spService;
    private MyReceiver r;
    private ImageButton btnDeuPost;
    String dueAmount, duedate, billnumber, billdate, customername, referenceNumber;
    //Volley Tag
    private String tag_json_obj = "json_topup_pay";


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadDlg = new LoadingDialog(getActivity());

        circleList = Select.from(CircleModel.class).list();
        operatorList = Select.from(OperatorsModel.class).list();

        operatorIdCode = new HashMap<>();
        circleIdCode = new HashMap<>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_mobile_postpaid, container, false);
        edt_mobile_number_postpaid = (MaterialEditText) rootView.findViewById(R.id.edt_mobile_number_postpaid);
        ibtnPhoneBook = (ImageButton) rootView.findViewById(R.id.ibtnPhoneBook);
        spinner_mob_provider_postpaid = (Spinner) rootView.findViewById(R.id.spinner_mob_provider_postpaid);
        edt_mob_amount_postpaid = (MaterialEditText) rootView.findViewById(R.id.edt_mob_amount_postpaid);
        btn_pay_mob_postpaid = (Button) rootView.findViewById(R.id.btn_pay_mob_postpaid);
        spService = (MaterialEditText) rootView.findViewById(R.id.spService);
        llLayouts = (LinearLayout) rootView.findViewById(R.id.llLayouts);
        btnDeuPost = (ImageButton) rootView.findViewById(R.id.btnDeuPost);
        operatorLists = new ArrayList<>();
edt_mobile_number_postpaid = (MaterialEditText) rootView.findViewById(R.id.edt_mobile_number_postpaid);
//        operatorSpinnerAdapter = new OperatorSpinnerAdapter(getActivity(), android.R.layout.simple_spinner_item, operator);
//        spinner_mob_provider_postpaid.setAdapter(operatorSpinnerAdapter);

        //DONE CLICK ON VIRTUAL KEYPAD
        edt_mob_amount_postpaid.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    attemptPayment();
                }
                return false;
            }
        });

        btn_pay_mob_postpaid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptPayment();
            }
        });

        ibtnPhoneBook.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                startActivityForResult(intent, PICK_CONTACT);
            }
        });
        loadOperatorCircleNew();

        spService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final CustomServiceDialogSearch search = new CustomServiceDialogSearch(getActivity(), operatorLists, "from", new ServicesSelectedListener() {
                    @Override
                    public void serviceselect(String type, long servicecode, String serviceName) {

                    }

                    @Override
                    public void serviceFilter(OperatorsModel type) {

                        spService.setText(type.getName());
                        selectedFromServiceCode = type.getCode();
                        selectedFromServiceName = type.getName();

                        if (selectedFromServiceCode.equalsIgnoreCase("BGCIN")) {

                            btnDeuPost.setVisibility(View.VISIBLE);
                            edt_mob_amount_postpaid.setFocusable(false);
                            edt_mob_amount_postpaid.setFocusableInTouchMode(false);

                        } else {
                            btnDeuPost.setVisibility(View.GONE);
                            edt_mob_amount_postpaid.setFocusable(true);
                            edt_mob_amount_postpaid.setFocusableInTouchMode(true);
                        }
                        btnDeuPost.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                attemptDeu();
                            }
                        });

                    }
                });
                search.show();
            }
        });


        return rootView;
    }

    private void attemptDeu() {
        edt_mobile_number_postpaid.setError(null);
        cancel = false;

        mobileNumber = edt_mobile_number_postpaid.getText().toString();
        checkPhone(mobileNumber);


        if (spService.getText().toString().equals("")) {
            spService.setError(getResources().getString(R.string.Select_operator_to_continue));
            return;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            getDeuAmount();
        }
    }

    private void attemptPayment() {
        edt_mob_amount_postpaid.setError(null);
        edt_mobile_number_postpaid.setError(null);
        cancel = false;

        amount = edt_mob_amount_postpaid.getText().toString();
        toMobileNumber = edt_mobile_number_postpaid.getText().toString();

//        spinner_mob_provider_postpaid.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                serviceProvider = ((OperatorsModel) spinner_mob_provider_postpaid.getItemAtPosition(i)).getCode();
//                operatorCode = ((OperatorsModel) spinner_mob_provider_postpaid.getItemAtPosition(i)).getDefaultCode();
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });


        checkPhone(toMobileNumber);


        if (spService.getText().toString().equals("")) {
            spService.setError(getResources().getString(R.string.Select_operator_to_continue));
            return;
        }

        if (edt_mob_amount_postpaid.getText().toString().equalsIgnoreCase("")){
            edt_mob_amount_postpaid.setError("Mandatory Field");
            return;
        }
        if (!validateAmount()) {
            return;
        }

        checkPayAmount(amount);

        if (cancel) {
            focusView.requestFocus();
        } else {
            showCustomDialog();

        }
    }


//    private void checkPhone(String phNo) {
//        CheckLog gasCheckLog = PayingDetailsValidation.checkMobileTenDigit(phNo);
//        if (!gasCheckLog.isValid) {
//            edt_mobile_number_postpaid.setError(getString(gasCheckLog.msg));
//            focusView = edt_mobile_number_postpaid;
//            cancel = true;
//        }
//    }

    private boolean validateAmount() {
        String amt = edt_mob_amount_postpaid.getText().toString().trim();
        if (amt.isEmpty()) {
            edt_mob_amount_postpaid.setError(getResources().getString(R.string.lessAmount));
            return false;
        } else if (amt.substring(0, 1).equals("0")) {
            edt_mob_amount_postpaid.setError(getResources().getString(R.string.Amount_should_not_start_with_0));
            return false;
        }
        return true;
    }

//    private boolean checkPhone() {
//        if (edt_mobile_number_postpaid.getText().toString().trim().isEmpty() || edt_mobile_number_postpaid.getText().toString().trim().length() < 10) {
//            CustomToast.showMessage(getActivity(), "Enter valid mobile no.");
//            Snackbar.make(llLayouts, "Enter valid mobile no.", Snackbar.LENGTH_LONG).show();
//            return false;
//        }
//        return true;
//    }

    private boolean checkPhone(String toMobileNumber) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkMobileTenDigit(toMobileNumber);
        if (!gasCheckLog.isValid) {
            edt_mobile_number_postpaid.setError(getString(gasCheckLog.msg));
            focusView = edt_mobile_number_postpaid;
            cancel = true;
        }

        return true;

    }


    private void checkPayAmount(String amount) {
        Double protein = Double.valueOf(amount);
        DueAmounts = Integer.valueOf(protein.intValue());
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
        if (!gasCheckLog.isValid) {
            edt_mob_amount_postpaid.setError(getString(gasCheckLog.msg));
            focusView = edt_mob_amount_postpaid;
            cancel = true;
        } else if (DueAmounts < 10) {
            edt_mob_amount_postpaid.setError(getString(R.string.lessAmount));
            focusView = edt_mob_amount_postpaid;
            cancel = true;
        }
    }

    private void getDeuAmount() {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {

            jsonRequest.put("serviceProvider", selectedFromServiceCode);
            jsonRequest.put("accountNumber", mobileNumber);

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            Log.i("URL", ApiUrl.URL_CHECK_DEU);
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_CHECK_DEU, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.i("DUE RESPONSE", response.toString());
                        String code = response.getString("code");
                        String message = response.getString("message");
                        loadDlg.dismiss();
                        if (code != null && code.equals("S00")) {
                            loadDlg.dismiss();
                            JSONObject detailsObject = response.getJSONObject("details");
//                            JSONObject particularsObject = new JSONObject(detailsObject.getString("particulars"));
                            dueAmount = detailsObject.getString("dueAmount");
                            duedate = detailsObject.getString("dueDate");
                            billnumber = detailsObject.getString("billNumber");
                            billdate = detailsObject.getString("billDate");
                            customername = detailsObject.getString("customerName");
                            referenceNumber = detailsObject.getString("referenceNumber");
                            edt_mob_amount_postpaid.setText(dueAmount);

//                            showCustomDisclaimerDialog();
                            Snackbar.make(llLayouts, getResources().getString(R.string.Your_Due_amount_is_INR) + dueAmount + " ", Snackbar.LENGTH_LONG).show();


                        } else if (code != null && code.equals("F00")) {
                            loadDlg.dismiss();
                            CustomToast.showMessage(getActivity(), message);
                            startActivity(new Intent(getContext(), MainActivity.class));
                        } else if (code == null) {
                            loadDlg.dismiss();

                        } else {
                            if (response.has("message") && response.getString("message") != null) {
                                String messages = response.getString("message");
                                loadDlg.dismiss();
                                Snackbar.make(llLayouts, messages, Snackbar.LENGTH_LONG).show();

                            }
                        }

                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));

                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    error.printStackTrace();
                    CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));


                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");

//                    String basicAuth = "Basic " + new String(Base64.encode("user:pass".getBytes(), Base64.NO_WRAP));
//                    map.put("Authorization", basicAuth);

                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    public void checkUpBalance() {
        loadDlg.show();
        Utility.hideKeyboard(getActivity());
        jsonRequest = new JSONObject();
        try {
//            jsonRequest.put("topupType", "Postpaid");
//            jsonRequest.put("serviceProvider", serviceProvider + "IN");
//            jsonRequest.put("mobileNo", edt_mobile_number_postpaid.getText().toString());
//            jsonRequest.put("amount", edt_mob_amount_postpaid.getText().toString());
//            jsonRequest.put("sessionId", session.getUserSessionId());

            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("amount", edt_mob_amount_postpaid.getText().toString());
            jsonRequest.put("serviceCode", selectedFromServiceCode);
            jsonRequest.put("topUpType", "Postpaid");
            jsonRequest.put("referenceNumber", referenceNumber);
//            jsonRequest.put("area", "kn");
            jsonRequest.put("mobileNumber", edt_mobile_number_postpaid.getText().toString());

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("Postpaid Request", jsonRequest.toString());
            Log.i("Postpaid URL", ApiUrl.URL_PAYMENTS);
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_PAYMENTS, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.i("Postpaid Response", response.toString());
                        String code = response.getString("code");

                        if (code != null && code.equals("S00")) {
                            showSuccessDialog();
                        } else if (code != null && code.equals("F03")) {
                            showInvalidSessionDialog();
//                        } else if (code != null && code.equals("T01")) {
//                            loadDlg.dismiss();
//                            JSONObject dtoObject = response.getJSONObject("dto");
//                            String splitAmount = dtoObject.getString("splitAmount");
//                            String message = response.getString("message");
//                            Snackbar.make(llLayouts, "You have insufficient balance. Please wait while you are redirected to load money.", Snackbar.LENGTH_LONG).show();
//
//
////                            CustomToast.showMessage(getActivity(), "You have insufficient balance. Please wait while you are redirected to load money.");
//                            Intent mainMenuDetailActivity = new Intent(getActivity(), LoadMoneyFragment.class);
////                            mainMenuDetailActivity.putExtra(AppMetadata.FRAGMENT_TYPE, "LoadMoney");
//                            mainMenuDetailActivity.putExtra("AutoFill", "yes");
//                            if (!splitAmount.isEmpty()) {
//                                mainMenuDetailActivity.putExtra("TYPE", "TOPUP");
//                                mainMenuDetailActivity.putExtra("splitAmount", splitAmount);
//                                mainMenuDetailActivity.putExtra("URL", ApiUrl.URL_POSTPAID_TOPUP);
//                                mainMenuDetailActivity.putExtra("REQUESTOBJECT", jsonRequest.toString());
//                                mainMenuDetailActivity.putExtra("spiltPay", true);
//                                startActivity(mainMenuDetailActivity);
////                                getActivity().finish();
//                            } else {
////                                CustomToast.showMessage(getActivity(), "split amount can't be empty");
//                                Snackbar.make(llLayouts, "split amount can't be empty", Snackbar.LENGTH_LONG).show();
//                            }
//

                        } else {
                            loadDlg.dismiss();
                            if (response.has("message") && response.getString("message") != null) {
                                String message = response.getString("message");
//                                CustomToast.showMessage(getActivity(), message);
                                Snackbar.make(llLayouts, message, Snackbar.LENGTH_LONG).show();
                            } else {
//                                CustomToast.showMessage(getActivity(), "Error message is null");
                                Snackbar.make(llLayouts, getResources().getString(R.string.Error_message_is_null), Snackbar.LENGTH_LONG).show();
                            }
                        }

                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", GenerateAPIHeader.getHeader(ApiUrl.H_PREPAID + session.getUserSessionId()));
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }

    }

    public void promoteTopUpNew() {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("amount", edt_mob_amount_postpaid.getText().toString());
            jsonRequest.put("serviceCode", selectedFromServiceCode);
            jsonRequest.put("topUpType", "Postpaid");
//            jsonRequest.put("area", "kn");
            jsonRequest.put("referenceNumber", referenceNumber);
            jsonRequest.put("mobileNumber", edt_mobile_number_postpaid.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
            jsonRequest = null;
        }
        if (jsonRequest != null) {
            Log.i("Prepaid Request", jsonRequest.toString());
            Log.i("Prepaid URL", ApiUrl.URL_PREPAID_TOPUP);
            StringRequest postReq = new StringRequest(Request.Method.POST, ApiUrl.URL_PREPAID_TOPUP, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.i("DEU Response", response.toString());


                    loadDlg.dismiss();


                    Intent intent = new Intent(getActivity(), WebViewsActivity.class);
                    intent.putExtra("TYPE", "TOPUP");
                    intent.putExtra("amount", amount);
                    intent.putExtra("response", response);
                    intent.putExtra("REQUESTOBJECT", jsonRequest.toString());
                    intent.putExtra("URL", ApiUrl.URL_PREPAID_TOPUP);
                    startActivity(intent);


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
//                CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));
                    Snackbar.make(llLayouts, getResources().getString(R.string.server_exception), Snackbar.LENGTH_LONG).show();

                }
            }) {
//            @Override
//            protected Map<String, String> getParams() {
//                Map<String, String> params = new HashMap<>();
//
//
//                params.put("sessionId", session.getUserSessionId());
//                params.put("amount", etPrepaidAmount.getText().toString());
//                params.put("serviceCode", serviceProvider);
//                params.put("topUpType", "Prepaid");
//                params.put("area", areacode);
//                params.put("mobileNumber", etPrepaidNo.getText().toString());
//
//
////                if (etLandLineAcNo.getVisibility() == View.VISIBLE) {
////                    params.put("accountNumber", acNo);
////                }
//                Log.i("HEADER", params.toString());
//                return params;
//            }

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return jsonRequest == null ? null : jsonRequest.toString().getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", jsonRequest, "utf-8");
                        return null;
                    }
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("hash", "123");
                    params.put("Content-Type", "application/json");
                    return params;
                }
            };

            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    public void loadOperatorCircleNew() {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("topUpType", "post");
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }
//
        if (jsonRequest != null) {
            Log.i("LOADCIRCLEOP Request", jsonRequest.toString());
            Log.i("LOADCIRCLEOP URL", ApiUrl.URL_CIRCLE_OPERATOR);

//        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, URL_CIRCLE_OPERATORPOST, (String) null,
//                new Response.Listener<JSONObject>() {

            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_CIRCLE_OPERATOR, jsonRequest, new Response.Listener<JSONObject>() {


                @Override
                public void onResponse(JSONObject JsonObj) {
                    loadDlg.dismiss();
                    Log.i("LOADCIRCLEOP Response", JsonObj.toString());
                    /*try {
                        operatorList.clear();
                        circleList.clear();

                        operatorIdCode.clear();
                        circleIdCode.clear();

                        OperatorsModel.deleteAll(OperatorsModel.class);
                        CircleModel.deleteAll(CircleModel.class);
                        //New Implementation

                        String jsonString = JsonObj.getString("details");
                        Log.i("Escape str", jsonEscape(jsonString));

                        JSONObject response = new JSONObject(jsonString);

                        JSONArray operatorArray = response.getJSONArray("operators");

//                        operatorList.add(new OperatorsModel("", "Select your operator", ""));

                        for (int i = 0; i < operatorArray.length(); i++) {
                            JSONObject c = operatorArray.getJSONObject(i);
                            String op_code = c.getString("serviceCode");
                            String op_name = c.getString("name");
                            String op_default_code = c.getString("code");
                            OperatorsModel oModel = new OperatorsModel(op_code, op_name, op_default_code);
                            oModel.save();
                            operatorList.add(oModel);
                            Log.i("OperatorsModel", String.valueOf(operatorList.get(i).getCode()));
                            operatorIdCode.put(op_code, i + 1);
                        }


                        JSONArray circleArray = response.getJSONArray("circles");

                        circleList.add(new CircleModel("", "Select your circle"));
                        for (int i = 0; i < circleArray.length(); i++) {
                            JSONObject c = circleArray.getJSONObject(i);
                            String op_code = c.getString("code");
                            String op_name = c.getString("name");
                            CircleModel cModel = new CircleModel(op_code, op_name);
                            cModel.save();
                            circleList.add(cModel);
                            circleIdCode.put(op_code, i + 1);
                        }


                        if (operatorList != null && operatorList.size() != 0) {
                            try {
                                operatorList.add(0, new OperatorsModel("", "Select your operator", ""));
                                spinnerAdapterOperator = new OperatorSpinnerAdapter(getActivity(), android.R.layout.simple_spinner_item, operatorList);
                                spinnerPrepaidProvider.setAdapter(spinnerAdapterOperator);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        if (circleList != null && circleList.size() != 0) {
                            try {
                                circleList.add(0, new CircleModel("", "Select your circle"));
                                spinnerAdapterCircle = new CircleSpinnerAdapter(getActivity(), android.R.layout.simple_spinner_item, circleList);
                                spinnerPrepaidCircle.setAdapter(spinnerAdapterCircle);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }*/
                    try {
                        operatorList.clear();
                        circleList.clear();

                        operatorIdCode.clear();
                        circleIdCode.clear();

                        OperatorsModel.deleteAll(OperatorsModel.class);
                        CircleModel.deleteAll(CircleModel.class);
                        //New Implementation

//                    String jsonString = JsonObj.getString("response");
//                    Log.i("Escape str", jsonEscape(jsonString));
//
//                    JSONObject response = new JSONObject(jsonString);

                        JSONArray operatorArray = JsonObj.getJSONArray("details");

//                        operatorList.add(new OperatorsModel("", "Select your operator", ""));

                        for (int i = 0; i < operatorArray.length(); i++) {
                            JSONObject c = operatorArray.getJSONObject(i);
                            String op_code = c.getString("code");
                            String op_name = c.getString("name");
                            String op_default_code = c.getString("serviceLogo");
                            OperatorsModel oModel = new OperatorsModel(op_code, op_name, op_default_code);
                            oModel.save();
                            operatorList.add(oModel);
                            operatorLists.add(oModel);
                            Log.i("OperatorsModel", String.valueOf(operatorList.get(i).getCode()));
                            operatorIdCode.put(op_code, i + 1);
                        }
                        spService.setHint(getResources().getString(R.string.Operator));

//                    JSONArray circleArray = response.getJSONArray("circles");
//
//                        circleList.add(new CircleModel("", "Select your circle"));
//                    for (int i = 0; i < circleArray.length(); i++) {
//                        JSONObject c = circleArray.getJSONObject(i);
//                        String op_code = c.getString("code");
//                        String op_name = c.getString("name");
//                        CircleModel cModel = new CircleModel(op_code, op_name);
//                        cModel.save();
//                        circleList.add(cModel);
//                        circleIdCode.put(op_code, i + 1);
//                    }


//                            if (operatorList != null && operatorList.size() != 0) {
//                                try {
//                                    operatorList.add(0, new OperatorsModel("", "Select your operator", ""));
//                                    spinnerAdapterOperator = new OperatorSpinnerAdapter(getActivity(), android.R.layout.simple_spinner_item, operatorList);
//                                    spinner_mob_provider_postpaid.setAdapter(spinnerAdapterOperator);
//                                } catch (Exception e) {
//                                    e.printStackTrace();
//                                }
//                            }

//                    if (circleList != null && circleList.size() != 0) {
//                        try {
//                            circleList.add(0, new CircleModel("", "Select your circle"));
//                            spinnerAdapterCircle = new CircleSpinnerAdapter(getActivity(), android.R.layout.simple_spinner_item, circleList);
//                            spinnerPrepaidCircle.setAdapter(spinnerAdapterCircle);
//
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }

                    } catch (JSONException e) {
                        e.printStackTrace();
//                            Toast.makeText(getActivity(), getResources().getString(R.string.server_exception2), Toast.LENGTH_SHORT).show();
                        Snackbar.make(llLayouts, getResources().getString(R.string.server_exception2), Snackbar.LENGTH_LONG).show();

                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    error.printStackTrace();
//                Toast.makeText(getActivity(), getResources().getString(R.string.server_exception), Toast.LENGTH_SHORT).show();
                    Snackbar.make(llLayouts, getResources().getString(R.string.server_exception), Snackbar.LENGTH_LONG).show();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");


                    String basicAuth = "Basic " + new String(Base64.encode("user:pass".getBytes(), Base64.NO_WRAP));
                    map.put("Authorization", basicAuth);
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);

        }
    }


    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton(getResources().getString(R.string.agree), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton(getResources().getString(R.string.Dismiss), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }


    public void showCustomDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title, Html.fromHtml(generateMessage()));
        builder.setPositiveButton(getResources().getString(R.string.agree), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                checkUpBalance();

            }
        });
        builder.setNegativeButton(getResources().getString(R.string.Dismiss), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public String generateMessage() {
        return "<b><font color=#000000>" + getActivity().getResources().getString(R.string.Operator) + ": " + "</font></b>" + "<font color=#000000>" + selectedFromServiceName + "</font><br>" +
                "<b><font color=#000000>" + getActivity().getResources().getString(R.string.Mobile_No) + ": " + "</font></b>" + "<font>" + toMobileNumber + "</font><br>" +
                "<b><font color=#000000>" + getActivity().getResources().getString(R.string.amount) + ": " + "</font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + amount + "</font><br><br>" +
                "<b><font color=#ff0000>" + getActivity().getResources().getString(R.string.Are_you_sure_you_want_to_proceed) + "</font></b><br>";
    }

    private void sendRefresh() {
        getActivity().finish();
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "3");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String[] projection = {ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.NUMBER};
        switch (requestCode) {
            case (PICK_CONTACT):
                if (resultCode == Activity.RESULT_OK) {
                    Uri contactUri = data.getData();
                    Cursor c = getActivity().getContentResolver().query(contactUri, projection, null, null, null);
                    if (c != null) {
                        if (c.moveToFirst()) {
                            String phoneNumber = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            String finalNumber = phoneNumber.replaceAll("[^0-9\\+]", "");

                            if (finalNumber != null && !finalNumber.isEmpty()) {
                                removeCountryCode(finalNumber);
                            }
                        }
                        c.close();
                    }
                }
                break;
        }
    }

    private void removeCountryCode(String number) {
        if (hasCountryCode(number)) {
            int country_digits = number.length() - MOBILE_DIGITS;
            number = number.substring(country_digits);
            edt_mobile_number_postpaid.setText(number);
        } else if (hasZero(number)) {
            if (number.length() >= 10) {
                int country_digits = number.length() - MOBILE_DIGITS;
                number = number.substring(country_digits);
                edt_mobile_number_postpaid.setText(number);
            } else {
//                CustomToast.showMessage(getActivity(), "Please select 10 digit no");
                Snackbar.make(llLayouts, getResources().getString(R.string.Please_select_10_digit_no), Snackbar.LENGTH_LONG).show();

            }
        } else {
            edt_mobile_number_postpaid.setText(number);
        }

    }

    private boolean hasCountryCode(String number) {
        return number.charAt(PLUS_SIGN_POS) == '+';
    }

    private boolean hasZero(String number) {
        return number.charAt(PLUS_SIGN_POS) == '0';
    }


    public void showMPinDialog() {
        VerifyMPinDialog builder = new VerifyMPinDialog(getActivity(), this);
        builder.setNegativeButton(getResources().getString(R.string.Dismiss), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
    }


    public void showSuccessDialog() {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(getSuccessMessage(), Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(getSuccessMessage());
        }
        CustomSuccessDialog builder = new CustomSuccessDialog(getActivity(), getResources().getString(R.string.Recharge_Successful), result);
        builder.setPositiveButton(getResources().getString(R.string.agree), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendRefresh();
                dialog.dismiss();
                getActivity().finish();
            }
        });
        builder.show();
    }

    public String getSuccessMessage() {
        String source =
                "<b><font color=#000000>" + getActivity().getResources().getString(R.string.Service_Provider) + ": " + "</font></b>" + "<font color=#000000>" + selectedFromServiceName + "</font><br>" +
                        "<b><font color=#000000>" + getActivity().getResources().getString(R.string.Mobile_No) + ": " + "</font></b>" + "<font>" + toMobileNumber + "</font><br>" +
                        "<b><font color=#000000>" + getActivity().getResources().getString(R.string.amount) + ": " + "</font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + amount + "</font><br><br>";
        return source;
    }

    public void showBlockDialog() {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(AppMetadata.getBlockSession(), Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(AppMetadata.getBlockSession());
        }
        CustomBlockDialog builder = new CustomBlockDialog(getActivity(), getResources().getString(R.string.Please_contact_customer_care), result);
        builder.setPositiveButton(getResources().getString(R.string.agree), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
                dialog.dismiss();
                getActivity().finish();
            }
        });
        builder.show();
    }

    @Override
    public void verifiedCompleted() {
        promoteTopUpNew();
    }

    @Override
    public void sessionInvalid() {
        showInvalidSessionDialog();
    }

    public void refresh() {
        //yout code in refresh.
        edt_mobile_number_postpaid.setText("");
        edt_mob_amount_postpaid.setText("");
        spService.setText("");
        edt_mobile_number_postpaid.setError(null);
        edt_mob_amount_postpaid.setError(null);
        spService.setError(null);
//        operatorSpinnerAdapter = new OperatorSpinnerAdapter(getActivity(), android.R.layout.simple_spinner_item, operator);
//        spinner_mob_provider_postpaid.setAdapter(operatorSpinnerAdapter);
    }

    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(r);
    }

    public void onResume() {
        super.onResume();
        r = new MyReceiver();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(r, new IntentFilter("TAG_REFRESH"));
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            PostpaidFragment.this.refresh();
        }
    }

}
