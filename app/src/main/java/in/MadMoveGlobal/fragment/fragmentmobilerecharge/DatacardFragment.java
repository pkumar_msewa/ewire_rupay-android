package in.MadMoveGlobal.fragment.fragmentmobilerecharge;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.Html;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.orm.query.Select;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.MadMoveGlobal.EwireRuPay.WebViewsActivity;
import in.MadMoveGlobal.adapter.CircleSpinnerAdapter;
import in.MadMoveGlobal.adapter.OperatorSpinnerAdapter;
import in.MadMoveGlobal.custom.CustomAlertDialog;
import in.MadMoveGlobal.custom.CustomBlockDialog;
import in.MadMoveGlobal.custom.CustomCircleDialogSearch;
import in.MadMoveGlobal.custom.CustomServiceDialogSearch;
import in.MadMoveGlobal.custom.CustomSuccessDialog;
import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.custom.VerifyMPinDialog;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.metadata.AppMetadata;
import in.MadMoveGlobal.model.CircleModel;
import in.MadMoveGlobal.model.OperatorsModel;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.EwireRuPay.activity.BrowseDataPackPlanActivity;
import in.MadMoveGlobal.util.AreasSelectedListener;
import in.MadMoveGlobal.util.CheckLog;
import in.MadMoveGlobal.util.GenerateAPIHeader;
import in.MadMoveGlobal.util.MPinVerifiedListner;
import in.MadMoveGlobal.util.NetworkErrorHandler;
import in.MadMoveGlobal.util.PayingDetailsValidation;
import in.MadMoveGlobal.util.ServicesSelectedListener;
import in.MadMoveGlobal.util.Utility;


public class DatacardFragment extends Fragment implements MPinVerifiedListner {
    private static final int PICK_CONTACT = 1;
    private static final int PICK_PLAN = 2;
    private static final int PLUS_SIGN_POS = 0;
    private static final int MOBILE_DIGITS = 10;


    private Spinner spinnerPrepaidCircle;
    private Spinner spinnerPrepaidProvider;
    private MaterialEditText etPrepaidAmount;
    private MaterialEditText etPrepaidNo;
    private Button btnPrepaidPay;
    private View rootView;

    private View focusView = null;
    private String fixedAmountPlanValue,serviceName, circleName;
    private String amount, toMobileNumber, serviceProvider, serviceProviderName, area, areacode, areaname;

    private LinearLayout llLayouts;
    private ImageButton ibtnPhoneBook;
    private Button btnMobileBrowsePlanPrepaid;
    private boolean cancel;

    private static HashMap<String, Integer> operatorIdCode;
    private static HashMap<String, Integer> circleIdCode;


    private List<CircleModel> circleList;
    private List<OperatorsModel> operatorList;
    private ArrayList<OperatorsModel> operatorLists;
    private ArrayList<CircleModel> circleLists;


    private CircleSpinnerAdapter spinnerAdapterCircle;
    private OperatorSpinnerAdapter spinnerAdapterOperator;
    private UserModel session = UserModel.getInstance();

    private String operatorCode, selectedFromServiceCode, selectedFromServiceName;
    ;

    private RadioGroup rgPlansType;
    private RadioButton rbtnPlan1;
    private RadioButton rbtnPlan2;
    private LoadingDialog loadDlg;
    private JSONObject jsonRequest;
    private MaterialEditText spService, spArea;

    private MyReceiver r;

    //Volley Tag
    private String tag_json_obj = "json_topup_pay";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadDlg = new LoadingDialog(getActivity());

        circleList = Select.from(CircleModel.class).list();
        operatorList = Select.from(OperatorsModel.class).list();

        operatorIdCode = new HashMap<>();
        circleIdCode = new HashMap<>();

        /*for (int i = 1; i < operatorList.size(); i++) {
            operatorIdCode.put(operatorList.get(i).getCode(), i);
        }
        for (int i = 1; i < circleList.size(); i++) {
            circleIdCode.put(circleList.get(i).getCode(), i);
        }*/


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_mobile_prepaid, container, false);
        spinnerPrepaidProvider = (Spinner) rootView.findViewById(R.id.spinner_mob_provider_prepaid);
        spinnerPrepaidCircle = (Spinner) rootView.findViewById(R.id.spinner_mobile_circle_prepaid);
        spService = (MaterialEditText) rootView.findViewById(R.id.spService);
        spArea = (MaterialEditText) rootView.findViewById(R.id.spArea);

        rgPlansType = (RadioGroup) rootView.findViewById(R.id.rgPlansType);
        rbtnPlan1 = (RadioButton) rootView.findViewById(R.id.rbtnPlan1);
        rbtnPlan2 = (RadioButton) rootView.findViewById(R.id.rbtnPlan2);
        rgPlansType.setVisibility(View.GONE);

        declare();

        operatorLists = new ArrayList<>();
        circleLists = new ArrayList<>();


        loadOperatorCircleNew();

        /*if (circleList.size() == 0 || operatorList.size() == 0) {
            loadOperatorCircleNew();
        } else {
            if (circleList != null && circleList.size() != 0) {
//                circleList.add(0, new CircleModel("", "Select your circle"));
                spinnerAdapterCircle = new CircleSpinnerAdapter(getActivity(), android.R.layout.simple_spinner_item, circleList);
                spinnerPrepaidCircle.setAdapter(spinnerAdapterCircle);
            }

            if (operatorList != null && operatorList.size() != 0) {
//                operatorList.add(0, new OperatorsModel("", "Select your operator", ""));
                spinnerAdapterOperator = new OperatorSpinnerAdapter(getActivity(), android.R.layout.simple_spinner_item, operatorList);
                spinnerPrepaidProvider.setAdapter(spinnerAdapterOperator);
            }
        }*/

        spService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final CustomServiceDialogSearch search = new CustomServiceDialogSearch(getActivity(), operatorLists, "from", new ServicesSelectedListener() {
                    @Override
                    public void serviceselect(String type, long servicecode, String serviceName) {

                    }

                    @Override
                    public void serviceFilter(OperatorsModel type) {

                        spService.setText(type.getName());
                        serviceProvider = type.getCode();
                        selectedFromServiceName = type.getName();

                        Log.i("SELECT", serviceProvider + "   " + selectedFromServiceName);


                    }
                });
                search.show();
            }
        });

        spArea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final CustomCircleDialogSearch search = new CustomCircleDialogSearch(getActivity(), circleLists, "from", new AreasSelectedListener() {
                    @Override
                    public void areaselect(String type, long areaCode, String areaName) {

                    }

                    @Override
                    public void areaFilter(CircleModel type) {

                        spArea.setText(type.getName());
                        areacode = type.getCode();
                        areaname = type.getName();


                        Log.i("SELECT", areacode + "   " + areaname);

                    }
                });
                search.show();
            }
        });

        return rootView;
    }


    private void declare() {
        ibtnPhoneBook = (ImageButton) rootView.findViewById(R.id.ibtnPhoneBook);
        etPrepaidAmount = (MaterialEditText) rootView.findViewById(R.id.edt_mob_amount_prepaid);
        etPrepaidNo = (MaterialEditText) rootView.findViewById(R.id.edt_mobile_number_prepaid);
        llLayouts = (LinearLayout) rootView.findViewById(R.id.llLayouts);
        btnPrepaidPay = (Button) rootView.findViewById(R.id.btn_pay_mob_prepaid);
        btnMobileBrowsePlanPrepaid = (Button) rootView.findViewById(R.id.btnMobileBrowsePlanPrepaid);
        etPrepaidNo.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String number = etPrepaidNo.getText().toString();
                if (number.length() == 10) {
                    loadAccordingToNo(number);
                }
            }
        });


//        spinnerPrepaidCircle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                area = ((CircleModel) spinnerPrepaidCircle.getItemAtPosition(position)).getCode();
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> arg0) {
//
//            }
//        });

//        spinnerPrepaidProvider.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                serviceProvider = ((OperatorsModel) spinnerPrepaidProvider.getItemAtPosition(i)).getCode();
//                operatorCode = ((OperatorsModel) spinnerPrepaidProvider.getItemAtPosition(i)).getDefaultCode();
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });


        btnMobileBrowsePlanPrepaid.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (spService.getText().toString().trim().isEmpty() || spArea == null || spService.getText().toString().equals("") || spArea.getText().toString().equals("")) {
//                    CustomToast.showMessage(getActivity(), "Select opeartor and area to browse");
                    Snackbar.make(llLayouts, getResources().getString(R.string.Select_operator_and_circle_to_browse), Snackbar.LENGTH_LONG).show();

                } else {
                    Intent planIntent = new Intent(getActivity(), BrowseDataPackPlanActivity.class);
                    planIntent.putExtra("operatorCode", serviceProvider);
                    planIntent.putExtra("circleCode", areacode);
                    startActivityForResult(planIntent, PICK_PLAN);
                }
            }
        });

        //DONE CLICK ON VIRTUAL KEYPAD
        etPrepaidAmount.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    attemptPayment();
                }
                return false;
            }
        });


        btnPrepaidPay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                attemptPayment();

            }
        });

        ibtnPhoneBook.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                startActivityForResult(intent, PICK_CONTACT);
            }
        });

//        loadOperatorCircleNew();

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String[] projection = {ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.NUMBER};
        switch (requestCode) {
            case (PICK_CONTACT):
                if (resultCode == Activity.RESULT_OK) {
                    Uri contactUri = data.getData();
                    Cursor c = getActivity().getContentResolver().query(contactUri, projection, null, null, null);
                    if (c != null) {
                        if (c.moveToFirst()) {
                            String phoneNumber = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            String finalNumber = phoneNumber.replaceAll("[^0-9\\+]", "");
                            if (finalNumber != null && !finalNumber.isEmpty()) {
                                removeCountryCode(finalNumber);
                            }
                        }
                        c.close();
                    }
                }
                break;
            case (PICK_PLAN):
                if (resultCode == Activity.RESULT_OK) {
                    String newText = data.getStringExtra("amount");
                    etPrepaidAmount.setText(newText);
                }
                break;

            case (651):
                etPrepaidAmount.setText("");
                etPrepaidNo.setText("");
                break;

        }
    }

    private void removeCountryCode(String number) {
        if (hasCountryCode(number)) {
            int country_digits = number.length() - MOBILE_DIGITS;
            number = number.substring(country_digits);
            etPrepaidNo.setText(number);
        } else if (hasZero(number)) {
            if (number.length() >= 10) {
                int country_digits = number.length() - MOBILE_DIGITS;
                number = number.substring(country_digits);
                etPrepaidNo.setText(number);
            } else {
//                CustomToast.showMessage(getActivity(), "Please select 10 digit no");
                Snackbar.make(llLayouts, getResources().getString(R.string.Please_select_10_digit_no), Snackbar.LENGTH_LONG).show();
            }
        } else {
            etPrepaidNo.setText(number);
        }
    }

    private boolean hasCountryCode(String number) {
        return number.charAt(PLUS_SIGN_POS) == '+';
    }

    private boolean hasZero(String number) {
        return number.charAt(PLUS_SIGN_POS) == '0';
    }

    public void loadAccordingToNo(String number) {
        jsonRequest = new JSONObject();
        loadDlg.show();
        try {
//            jsonRequest.put("sessionId", session.getUserSessionId());
//            jsonRequest.put("mobileNumber", number);
            jsonRequest.put("accountNumber", number);
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());

            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_LOAD_ACCORDING_NOM, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    loadDlg.dismiss();
                    Log.i("AUTO RESPONSE", response.toString());

                    String op_code, circle_code;

                    try {
                        String mssg = response.getString("message");
                        String code = response.getString("code");

                        if (code != null && code.equals("S00")) {
                            String responseString = response.getString("details");
                            Log.i("escape string", jsonEscape(responseString));

                            JSONObject responseObj = new JSONObject(responseString);

//                            String serviceName = responseObj.getString("serviceName");
                            String serviceCode = responseObj.getString("operator");
//                            String circleName = responseObj.getString("circleName");
                            String circleCode = responseObj.getString("circle");
                            for (int i = 1; i < operatorList.size(); i++) {
                                if (operatorList.get(i).getCode().equals(serviceCode))
                                    serviceName = operatorList.get(i).getName();
                                Log.i("operatorIdCode", operatorList.get(i).getName());
                            }

                            for (int i = 1; i < circleList.size(); i++) {

//                                circleIdCode.put(circleList.get(i).getCode(), i);

                                if (circleList.get(i).getCode().equals(circleCode))
                                    circleName = circleList.get(i).getName();

                                Log.i("operatorIdCode", circleList.get(i).getName());
                            }

                            selectedFromServiceName = serviceName;
                            serviceProvider = serviceCode;
                            areaname = circleName;
                            areacode = circleCode;

                            spService.setText(selectedFromServiceName);
                            spArea.setText(areaname);

//                            if (operatorIdCode != null && op_code != null && !op_code.isEmpty()) {
//                                int op_selection = operatorIdCode.get(op_code);
//                                spinnerPrepaidProvider.setSelection(op_selection);
//
//                            }
//                            if (circleIdCode != null && circle_code != null && !circle_code.isEmpty()) {
//                                int circle_selection = circleIdCode.get(circle_code);
//                                spinnerPrepaidCircle.setSelection(circle_selection);
//
//                            }
                        } else if (code != null && code.equals("F00")) {
                            Snackbar.make(llLayouts, mssg, Snackbar.LENGTH_LONG).show();
                        } else if (code != null && code.equals("F03")) {
                            showInvalidSessionDialog();
                        } else {
//                            Snackbar.make(llLayouts, mssg, Snackbar.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");

                    String basicAuth = "Basic " + new String(Base64.encode("user:pass".getBytes(), Base64.NO_WRAP));
                    map.put("Authorization", basicAuth);


                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }

    }


    public void loadOperatorCircleNew() {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("topUpType", "PRE");
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }
//
        if (jsonRequest != null) {
            Log.i("LOADCIRCLEOP Request", jsonRequest.toString());
            Log.i("LOADCIRCLEOP URL", ApiUrl.URL_CIRCLE_OPERATOR);

//            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, URL_CIRCLE_OPERATOR, (String) null,
//                    new Response.Listener<JSONObject>() {
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_CIRCLE_OPERATOR, jsonRequest, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject JsonObj) {
                    loadDlg.dismiss();
                    Log.i("LOADCIRCLEOP Response", JsonObj.toString());
                    /*try {
                        operatorList.clear();
                        circleList.clear();

                        operatorIdCode.clear();
                        circleIdCode.clear();

                        OperatorsModel.deleteAll(OperatorsModel.class);
                        CircleModel.deleteAll(CircleModel.class);
                        //New Implementation

                        String jsonString = JsonObj.getString("details");
                        Log.i("Escape str", jsonEscape(jsonString));

                        JSONObject response = new JSONObject(jsonString);

                        JSONArray operatorArray = response.getJSONArray("operators");

//                        operatorList.add(new OperatorsModel("", "Select your operator", ""));

                        for (int i = 0; i < operatorArray.length(); i++) {
                            JSONObject c = operatorArray.getJSONObject(i);
                            String op_code = c.getString("serviceCode");
                            String op_name = c.getString("name");
                            String op_default_code = c.getString("code");
                            OperatorsModel oModel = new OperatorsModel(op_code, op_name, op_default_code);
                            oModel.save();
                            operatorList.add(oModel);
                            Log.i("OperatorsModel", String.valueOf(operatorList.get(i).getCode()));
                            operatorIdCode.put(op_code, i + 1);
                        }


                        JSONArray circleArray = response.getJSONArray("circles");

                        circleList.add(new CircleModel("", "Select your circle"));
                        for (int i = 0; i < circleArray.length(); i++) {
                            JSONObject c = circleArray.getJSONObject(i);
                            String op_code = c.getString("code");
                            String op_name = c.getString("name");
                            CircleModel cModel = new CircleModel(op_code, op_name);
                            cModel.save();
                            circleList.add(cModel);
                            circleIdCode.put(op_code, i + 1);
                        }


                        if (operatorList != null && operatorList.size() != 0) {
                            try {
                                operatorList.add(0, new OperatorsModel("", "Select your operator", ""));
                                spinnerAdapterOperator = new OperatorSpinnerAdapter(getActivity(), android.R.layout.simple_spinner_item, operatorList);
                                spinnerPrepaidProvider.setAdapter(spinnerAdapterOperator);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        if (circleList != null && circleList.size() != 0) {
                            try {
                                circleList.add(0, new CircleModel("", "Select your circle"));
                                spinnerAdapterCircle = new CircleSpinnerAdapter(getActivity(), android.R.layout.simple_spinner_item, circleList);
                                spinnerPrepaidCircle.setAdapter(spinnerAdapterCircle);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }*/
                    try {
                        operatorList.clear();
                        circleList.clear();

                        operatorIdCode.clear();
                        circleIdCode.clear();

                        OperatorsModel.deleteAll(OperatorsModel.class);
                        CircleModel.deleteAll(CircleModel.class);
                        //New Implementation

//                    String jsonString = JsonObj.getString("response");
//                    Log.i("Escape str", jsonEscape(jsonString));
//
//                    JSONObject response = new JSONObject(jsonString);

                        JSONArray operatorArray = JsonObj.getJSONArray("details");

//                        operatorList.add(new OperatorsModel("", "Select your operator", ""));

                        for (int i = 0; i < operatorArray.length(); i++) {
                            JSONObject c = operatorArray.getJSONObject(i);
                            String op_code = c.getString("code");
                            String op_name = c.getString("name");
                            String op_default_code = c.getString("serviceLogo");
                            OperatorsModel oModel = new OperatorsModel(op_code, op_name, op_default_code);
                            oModel.save();
                            operatorList.add(oModel);
                            operatorLists.add(oModel);
                            Log.i("OperatorsModel", String.valueOf(operatorList.get(i).getCode()));
                            operatorIdCode.put(op_code, i + 1);
                        }

                        spService.setHint(getResources().getString(R.string.Operator));
                        JSONArray circleArray = JsonObj.getJSONArray("circles");

                        circleList.add(new CircleModel("", getResources().getString(R.string.Select_your_circle)));
                        for (int i = 0; i < circleArray.length(); i++) {
                            JSONObject c = circleArray.getJSONObject(i);
                            String op_code = c.getString("code");
                            String op_name = c.getString("name");
                            CircleModel cModel = new CircleModel(op_code, op_name);
                            cModel.save();
                            circleList.add(cModel);
                            circleLists.add(cModel);
                            circleIdCode.put(op_code, i + 1);
                        }
                        spArea.setHint(getResources().getString(R.string.Circle));

//                            if (operatorList != null && operatorList.size() != 0) {
//                                try {
//                                    operatorList.add(0, new OperatorsModel("", "Select your operator", ""));
//                                    spinnerAdapterOperator = new OperatorSpinnerAdapter(getActivity(), android.R.layout.simple_spinner_item, operatorList);
//                                    spinnerPrepaidProvider.setAdapter(spinnerAdapterOperator);
//                                } catch (Exception e) {
//                                    e.printStackTrace();
//                                }
//                            }

//                    if (circleList != null && circleList.size() != 0) {
//                        try {
//                            circleList.add(0, new CircleModel("", "Select your circle"));
//                            spinnerAdapterCircle = new CircleSpinnerAdapter(getActivity(), android.R.layout.simple_spinner_item, circleList);
//                            spinnerPrepaidCircle.setAdapter(spinnerAdapterCircle);
//
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }

                    } catch (JSONException e) {
                        e.printStackTrace();
//                            Toast.makeText(getActivity(), getResources().getString(R.string.server_exception2), Toast.LENGTH_SHORT).show();
                        Snackbar.make(llLayouts, getResources().getString(R.string.server_exception2), Snackbar.LENGTH_LONG).show();

                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    error.printStackTrace();
//                Toast.makeText(getActivity(), getResources().getString(R.string.server_exception), Toast.LENGTH_SHORT).show();
                    Snackbar.make(llLayouts, getResources().getString(R.string.server_exception), Snackbar.LENGTH_LONG).show();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");


//                    String basicAuth = "Basic " + new String(Base64.encode("user:pass".getBytes(), Base64.NO_WRAP));
//                    map.put("Authorization", basicAuth);
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }

    }

    private void attemptPayment() {
        etPrepaidAmount.setError(null);
        etPrepaidNo.setError(null);
        cancel = false;

        amount = etPrepaidAmount.getText().toString();
        toMobileNumber = etPrepaidNo.getText().toString();
//        serviceProviderName = ((OperatorsModel) spinnerPrepaidProvider.getSelectedItem()).getName();
//        serviceProvider = ((OperatorsModel) spinnerPrepaidProvider.getSelectedItem()).getCode();
//        area = ((CircleModel) spinnerPrepaidCircle.getSelectedItem()).getCode();


        checkPhone(toMobileNumber);


        if (spService.getText().toString().equals("")) {

            spService.setError(getResources().getString(R.string.Select_operator_to_continue));

//            Snackbar.make(llLayouts, "Select operator to continue", Snackbar.LENGTH_LONG).show();

            return;
        }
        if (spArea.getText().toString().equals("")) {
            spArea.setError(getResources().getString(R.string.Select_area_to_continue));
//            Snackbar.make(llLayouts, "Select area to continue", Snackbar.LENGTH_LONG).show();

            return;
        }


        if (!validateAmount()) {
            return;
        }

        checkPayAmount(amount);

        if (cancel) {
            focusView.requestFocus();
        } else {
            showCustomDialog();

        }
    }


//    private void checkPhone(String phNo) {
//        CheckLog gasCheckLog = PayingDetailsValidation.checkMobileTenDigit(phNo);
//        if (!gasCheckLog.isValid) {
//            etPrepaidNo.setError(getString(gasCheckLog.msg));
//            focusView = etPrepaidNo;
//            cancel = true;
//        }
//    }

    private boolean validateAmount() {
        String amt = etPrepaidAmount.getText().toString().trim();
        if (amt.isEmpty()) {
            etPrepaidAmount.setError(getResources().getString(R.string.Please_enter_valid_amount));
            return false;
        } else if (amt.substring(0, 1).equals("0")) {
            etPrepaidAmount.setError(getResources().getString(R.string.Amount_should_not_start_with_0));
            return false;
        }
        return true;
    }


    private boolean checkPhone(String toMobileNumber) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkMobileTenDigit(toMobileNumber);
        if (!gasCheckLog.isValid) {
            etPrepaidNo.setError(getString(gasCheckLog.msg));
            focusView = etPrepaidNo;
            cancel = true;
        }

        return true;

    }


    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }


    private void checkPayAmount(String amount) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
        if (!gasCheckLog.isValid) {
            etPrepaidAmount.setError(getString(gasCheckLog.msg));
            focusView = etPrepaidAmount;
            cancel = true;
        } else if (Integer.valueOf(etPrepaidAmount.getText().toString()) < 10) {
            etPrepaidAmount.setError(getString(R.string.lessAmount));
            focusView = etPrepaidAmount;
            cancel = true;
        }
    }


    public void showCustomDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title, Html.fromHtml(generateMessage()));
        builder.setPositiveButton(getResources().getString(R.string.agree), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
//                if (session.isMPin() && Integer.valueOf(amount) >= getResources().getInteger(R.integer.transactionValue)) {
//                    showMPinDialog();
//                } else {
//                    checkUpBalance();
//                    loadDlg.show();
//                    promoteTopUpNew();
//                }
                checkUpBalance();
            }
        });
        builder.setNegativeButton(getResources().getString(R.string.Dismiss), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public String generateMessage() {
        return "<b><font color=#000000>" + getActivity().getResources().getString(R.string.Operator) + ": " + "</font></b>" + "<font color=#000000>" + selectedFromServiceName + "</font><br>" +
                "<b><font color=#000000>" + getActivity().getResources().getString(R.string.Mobile_No) + ": " + "</font></b>" + "<font>" + toMobileNumber + "</font><br>" +
                "<b><font color=#000000>" + getActivity().getResources().getString(R.string.amount) + ": " + "</font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + amount + "</font><br><br>" +
                "<b><font color=#ff0000>" + getActivity().getResources().getString(R.string.Are_you_sure_you_want_to_proceed) + "</font></b><br>";
    }

    public void checkUpBalance() {
        loadDlg.show();
        Utility.hideKeyboard(getActivity());
        jsonRequest = new JSONObject();
        try {
//            jsonRequest.put("topupType", "DataCard");
//            jsonRequest.put("serviceProvider", serviceProvider);
//            jsonRequest.put("mobileNo", etPrepaidNo.getText().toString());
//            jsonRequest.put("amount", etPrepaidAmount.getText().toString());
//            jsonRequest.put("area", areacode);
//            jsonRequest.put("sessionId", session.getUserSessionId());

            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("amount", etPrepaidAmount.getText().toString());
            jsonRequest.put("serviceCode", serviceProvider);
            jsonRequest.put("topUpType", "DataCard");
            jsonRequest.put("area", areacode);
            jsonRequest.put("mobileNumber", etPrepaidNo.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_PAYMENTS, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    loadDlg.dismiss();
                    try {
                        Log.i("Prepaid RESPONSE", response.toString());
                        String code = response.getString("code");

                        if (code != null && code.equals("S00")) {
                            showSuccessDialog();
//                        }
//                        else if (code != null && code.equals("T01")) {
//                            JSONObject dtoObject = response.getJSONObject("dto");
//                            String splitAmount = dtoObject.getString("splitAmount");
//                            String message = response.getString("message");
//                            Snackbar.make(llLayouts, "You have insufficient balance. Please wait while you are redirected to load money.", Snackbar.LENGTH_LONG).show();
//
////                            CustomToast.showMessage(getActivity(), "You have insufficient balance. Please wait while you are redirected to load money.");
//                            Intent mainMenuDetailActivity = new Intent(getActivity(), LoadMoneyFragment.class);
////                            mainMenuDetailActivity.putExtra(AppMetadata.FRAGMENT_TYPE, "LoadMoney");
//                            mainMenuDetailActivity.putExtra("AutoFill", "yes");
//                            if (!splitAmount.isEmpty()) {
//                                mainMenuDetailActivity.putExtra("TYPE", "TOPUP");
//                                mainMenuDetailActivity.putExtra("splitAmount", splitAmount);
//                                mainMenuDetailActivity.putExtra("URL", ApiUrl.URL_DATACARD_TOPUP);
//                                mainMenuDetailActivity.putExtra("REQUESTOBJECT", jsonRequest.toString());
//                                mainMenuDetailActivity.putExtra("spiltPay", true);
//                                startActivity(mainMenuDetailActivity);
//                            } else {
////                                CustomToast.showMessage(getActivity(), "split amount can't be empty");
//                                Snackbar.make(llLayouts, "split amount can't be empty", Snackbar.LENGTH_LONG).show();
//
//                            }

                        } else {
                            if (response.has("message") && response.getString("message") != null) {
                                String message = response.getString("message");
//                                CustomToast.showMessage(getActivity(), message);
                                Snackbar.make(llLayouts, message, Snackbar.LENGTH_LONG).show();
                            } else {
//                                CustomToast.showMessage(getActivity(), "Error message is null");
                                Snackbar.make(llLayouts, "Error message is null", Snackbar.LENGTH_LONG).show();

                            }
                        }

                    } catch (JSONException e) {
//                        CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
                        Snackbar.make(llLayouts, getResources().getString(R.string.server_exception2), Snackbar.LENGTH_LONG).show();

                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
//                    CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
                    Snackbar.make(llLayouts, NetworkErrorHandler.getMessage(error, getActivity()), Snackbar.LENGTH_LONG).show();

                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", GenerateAPIHeader.getHeader(ApiUrl.H_PREPAID + session.getUserSessionId()));
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }

    }

    public void promoteTopUpNew() {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("amount", etPrepaidAmount.getText().toString());
            jsonRequest.put("serviceCode", serviceProvider);
            jsonRequest.put("topUpType", "DataCard");
            jsonRequest.put("area", areacode);
            jsonRequest.put("mobileNumber", etPrepaidNo.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
            jsonRequest = null;
        }
        if (jsonRequest != null) {
            Log.i("Prepaid Request", jsonRequest.toString());
            Log.i("Prepaid URL", ApiUrl.URL_PREPAID_TOPUP);
            StringRequest postReq = new StringRequest(Request.Method.POST, ApiUrl.URL_PREPAID_TOPUP, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.i("DEU Response", response.toString());


                    loadDlg.dismiss();


                    Intent intent = new Intent(getActivity(), WebViewsActivity.class);
                    intent.putExtra("TYPE", "TOPUP");
                    intent.putExtra("amount", amount);
                    intent.putExtra("response", response);
                    intent.putExtra("REQUESTOBJECT", jsonRequest.toString());
                    intent.putExtra("URL", ApiUrl.URL_PREPAID_TOPUP);
                    startActivity(intent);


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
//                CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));
                    Snackbar.make(llLayouts, getResources().getString(R.string.server_exception), Snackbar.LENGTH_LONG).show();

                }
            }) {
//            @Override
//            protected Map<String, String> getParams() {
//                Map<String, String> params = new HashMap<>();
//
//
//                params.put("sessionId", session.getUserSessionId());
//                params.put("amount", etPrepaidAmount.getText().toString());
//                params.put("serviceCode", serviceProvider);
//                params.put("topUpType", "Prepaid");
//                params.put("area", areacode);
//                params.put("mobileNumber", etPrepaidNo.getText().toString());
//
//
////                if (etLandLineAcNo.getVisibility() == View.VISIBLE) {
////                    params.put("accountNumber", acNo);
////                }
//                Log.i("HEADER", params.toString());
//                return params;
//            }

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return jsonRequest == null ? null : jsonRequest.toString().getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", jsonRequest, "utf-8");
                        return null;
                    }
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("hash", "123");
                    params.put("Content-Type", "application/json");
                    return params;
                }
            };

            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        System.gc();
    }

    private void sendRefresh() {
        getActivity().finish();
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "3");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton(getResources().getString(R.string.Ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton(getResources().getString(R.string.Dismiss), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    public void showMPinDialog() {
        VerifyMPinDialog builder = new VerifyMPinDialog(getActivity(), this);
        builder.setNegativeButton(getResources().getString(R.string.Dismiss), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
    }


    public void showSuccessDialog() {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(getSuccessMessage(), Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(getSuccessMessage());
        }
        CustomSuccessDialog builder = new CustomSuccessDialog(getActivity(), getResources().getString(R.string.Recharge_Successful), result);
        builder.setPositiveButton(getResources().getString(R.string.agree), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                sendRefresh();

            }
        });
        builder.show();
    }

    public String getSuccessMessage() {
        String source =
                "<b><font color=#000000>" + getActivity().getResources().getString(R.string.Service_Provider) + ": " + "</font></b>" + "<font color=#000000>" + selectedFromServiceName + "</font><br>" +
                        "<b><font color=#000000>" + getActivity().getResources().getString(R.string.Mobile_No) + ": " + "</font></b>" + "<font>" + toMobileNumber + "</font><br>" +
                        "<b><font color=#000000>" + getActivity().getResources().getString(R.string.amount) + ": " + "</font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + amount + "</font><br><br>";
        return source;
    }

    public void refresh() {
        //yout code in refresh.
        Log.i("Refresh", "YES");
        etPrepaidAmount.setText("");
        etPrepaidNo.setText("");
        spService.setText("");
        spArea.setText("");

        etPrepaidNo.setError(null);
        etPrepaidAmount.setError(null);
        spService.setError(null);
        spArea.setError(null);


    }


    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(r);
    }

    public void onResume() {
        super.onResume();
        r = new MyReceiver();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(r,
                new IntentFilter("TAG_REFRESH"));
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            DatacardFragment.this.refresh();
        }
    }

    public void showBlockDialog() {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(AppMetadata.getBlockSession(), Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(AppMetadata.getBlockSession());
        }
        CustomBlockDialog builder = new CustomBlockDialog(getActivity(), getResources().getString(R.string.Please_contact_customer_care), result);
        builder.setPositiveButton(getResources().getString(R.string.agree), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
                dialog.dismiss();
                getActivity().finish();
            }
        });
        builder.show();
    }

    @Override
    public void verifiedCompleted() {
        checkUpBalance();
    }

    @Override
    public void sessionInvalid() {
        showInvalidSessionDialog();
    }

    public String jsonEscape(final String str) {
        return str.replace("\\\\n", "");
    }
}
