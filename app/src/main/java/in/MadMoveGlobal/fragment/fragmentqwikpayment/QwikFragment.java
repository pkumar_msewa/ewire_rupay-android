package in.MadMoveGlobal.fragment.fragmentqwikpayment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import in.MadMoveGlobal.adapter.QwikPaymentAdapter;
import in.MadMoveGlobal.custom.CustomAlertDialog;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.metadata.AppMetadata;
import in.MadMoveGlobal.model.QwikPayModel;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.EwireRuPay.WebViewActivity;

/**
 * Created by Ksf on 3/27/2016.
 */
public class QwikFragment extends Fragment {
    private View rootView;
    private RecyclerView rvQuickPay;
    private UserModel session = UserModel.getInstance();
    private JSONObject jsonRequest;
    private ArrayList<QwikPayModel> payList;
    private ProgressBar pbQuickPayList;
    private String amount;
    private LinearLayout llNoQuickPayList;

    //Volley Tag
    private String tag_json_obj = "json_quick_pay";
    private JsonObjectRequest postReq;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        payList = new ArrayList<>();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessageReceiver, new IntentFilter("Favtoogle"));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //Views
        rootView = inflater.inflate(R.layout.fragment_qwik_pay, container, false);
        rvQuickPay = (RecyclerView) rootView.findViewById(R.id.rvQuickPay);
        llNoQuickPayList = (LinearLayout) rootView.findViewById(R.id.llNoQuickPayList);
        pbQuickPayList = (ProgressBar) rootView.findViewById(R.id.pbQuickPayList);
        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.recycle_spacing);
        rvQuickPay.addItemDecoration(new SpacesItemDecoration(spacingInPixels));

        GridLayoutManager manager = new GridLayoutManager(getActivity(), 1);
        rvQuickPay.setLayoutManager(manager);

        loadUserQuickPay();
        return rootView;
    }

    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        public SpacesItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view,
                                   RecyclerView parent, RecyclerView.State state) {
            outRect.left = space;
            outRect.right = space;
            outRect.bottom = space;
            outRect.top = space;
        }
    }

    public void loadUserQuickPay() {
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            Log.i("Quick Url", ApiUrl.URL_QUICK_GET_PAY_LIST);
            postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_QUICK_GET_PAY_LIST, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject JsonObj) {
                    try {
                        Log.i("Quick Response", JsonObj.toString());
                        String message = JsonObj.getString("message");
                        String code = JsonObj.getString("code");

                        if (code != null && code.equals("T01")) {
//                            loadDlg.dismiss();
//                            CustomToast.showMessage(getActivity(), "You have insufficient balance. Please wait while you are redirected to load money.");
                            Intent mainMenuDetailActivity = new Intent(getActivity(), WebViewActivity.class);
                            mainMenuDetailActivity.putExtra("TYPE", "QWICKPAY");
                            mainMenuDetailActivity.putExtra("splitAmount", amount);
                            mainMenuDetailActivity.putExtra("URL", ApiUrl.URL_QUICK_GET_PAY_LIST);
                            mainMenuDetailActivity.putExtra("REQUESTOBJECT", jsonRequest.toString());
                            startActivity(mainMenuDetailActivity);


                        } else if (code != null && code.equals("S00")) {
                            QwikPayModel.deleteAll(QwikPayModel.class);
                            String jsonString = JsonObj.getString("response");
                            JSONObject response = new JSONObject(jsonString);

                            JSONArray operatorArray = response.getJSONArray("details");
                            for (int i = 0; i < operatorArray.length(); i++) {
                                JSONObject c = operatorArray.getJSONObject(i);
                                double amountD = c.getDouble("amount");
                                String amountPaid = String.valueOf(amountD);
                                long dateTimeL = c.getLong("date");
                                String dateTime = String.valueOf(dateTimeL);

                                String refNo = c.getString("transactionRefNo");
                                String description = c.getString("description");
                                String title = c.getString("service");
                                boolean isFav = c.getBoolean("favourite");

                                QwikPayModel quickModel = new QwikPayModel(title, dateTime, description, amountPaid, isFav, refNo);
                                if (isFav) {
                                    quickModel.save();
                                    Log.i("Save", isFav + "");
                                }
                                payList.add(quickModel);
                            }

                            if (payList != null && payList.size() != 0) {
                                Collections.reverse(payList);
                                QwikPaymentAdapter rcAdapter = new QwikPaymentAdapter(getActivity(), payList);
                                rvQuickPay.setAdapter(rcAdapter);
                                pbQuickPayList.setVisibility(View.GONE);
                                rvQuickPay.setVisibility(View.VISIBLE);
                                llNoQuickPayList.setVisibility(View.GONE);
                            } else {
                                llNoQuickPayList.setVisibility(View.VISIBLE);
                                pbQuickPayList.setVisibility(View.GONE);
                                rvQuickPay.setVisibility(View.GONE);
                            }


                        } else if (code != null && code.equals("F03")) {
                            pbQuickPayList.setVisibility(View.GONE);
                            rvQuickPay.setVisibility(View.GONE);
                            showInvalidSessionDialog();
                        } else {
                            pbQuickPayList.setVisibility(View.GONE);
                            rvQuickPay.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        pbQuickPayList.setVisibility(View.GONE);
                        rvQuickPay.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    pbQuickPayList.setVisibility(View.GONE);
                    rvQuickPay.setVisibility(View.GONE);
//                    CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
                    error.printStackTrace();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "123");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        PayQwikApplication.getInstance().cancelPendingRequests(postReq);
    }

    @Override
    public void onDetach() {
        postReq.cancel();
        super.onDetach();
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            rvQuickPay.setVisibility(View.GONE);
            llNoQuickPayList.setVisibility(View.GONE);
            pbQuickPayList.setVisibility(View.VISIBLE);
            payList.clear();
            loadUserQuickPay();
        }
    };
}