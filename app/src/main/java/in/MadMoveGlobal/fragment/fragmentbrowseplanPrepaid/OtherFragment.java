package in.MadMoveGlobal.fragment.fragmentbrowseplanPrepaid;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.adapter.MobilePlanAdapter;
import in.MadMoveGlobal.util.MobilePlansCheck;

/**
 * Created by Dushant on 08/27/2017.
 */
@SuppressLint("ValidFragment")
public class OtherFragment extends Fragment {

    MobilePlansCheck plans = MobilePlansCheck.getInstance();

    private View rootview;
    private ListView listOfDataPackPlans;
    private TextView tvNoPlans;
    private SharedPreferences sharedpreferences;
    private String prepaidNo;
    String type;
    @SuppressLint("ValidFragment")
    public OtherFragment(String prepaidNo, String type) {
        this.prepaidNo = prepaidNo;
        this.type=type;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.fragment_full_talk_time, container,false);

        listOfDataPackPlans = (ListView) rootview.findViewById(R.id.lvMobileDataPackPlans);
        tvNoPlans = (TextView) rootview.findViewById(R.id.tvNoPlans);
        sharedpreferences = getActivity().getSharedPreferences("prepaidValue", Context.MODE_PRIVATE);

        if (plans.getOtherPlans().size() == 0) {
            listOfDataPackPlans.setVisibility(View.GONE);
            tvNoPlans.setVisibility(View.VISIBLE);
        } else {
            listOfDataPackPlans.setVisibility(View.VISIBLE);
            tvNoPlans.setVisibility(View.GONE);

            MobilePlanAdapter tabMobileList = new MobilePlanAdapter(getActivity(), plans.getOtherPlans(), 0, sharedpreferences, prepaidNo, type);
            listOfDataPackPlans.setAdapter(tabMobileList);
        }

        return rootview;
    }

}
