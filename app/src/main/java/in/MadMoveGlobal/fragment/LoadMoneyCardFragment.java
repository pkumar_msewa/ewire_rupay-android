package in.MadMoveGlobal.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.SSLSocketFactory;

import in.MadMoveGlobal.custom.CustomAlertDialog;
import in.MadMoveGlobal.custom.CustomDisclaimerDialog;
import in.MadMoveGlobal.custom.CustomSuccessDialog;
import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.metadata.AppMetadata;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.EwireRuPay.activity.MainActivity;
import in.MadMoveGlobal.util.CheckLog;
import in.MadMoveGlobal.util.NetworkErrorHandler;
import in.MadMoveGlobal.util.PayingDetailsValidation;
import in.MadMoveGlobal.util.TLSSocketFactory;

/**
 * Created by Ksf on 4/11/2016.
 */
public class LoadMoneyCardFragment extends AppCompatActivity implements PaymentResultListener {

    private static final String TAG = "values";
    private View rootView;
    private MaterialEditText etLoadMoneyAmount;
    private Button btnLoadMoney;
    private RadioButton rbLoadMoneyVBank, rbLoadMoneyOther;
    private View focusView = null;
    private boolean cancel;
    private String amount = null;
    AlertDialog.Builder payDialog;
    private UserModel session = UserModel.getInstance();
    private LoadingDialog loadDlg;
    private RequestQueue rq;
    private boolean isVBank = true;
    private String inValidMessage = "";
    private String tag_json_obj = "load_money";
    private JSONObject jsonRequest;
    String autoFill, address1, address2, city, state, pinCode;
    double loadAmount;
    private Toolbar toolbar;
    private String transcationID, authRefNo;
    private JsonObjectRequest postReq;
    private ImageButton ivBackBtn;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_ask_amount_to_load_money);
        payDialog = new AlertDialog.Builder(LoadMoneyCardFragment.this, R.style.AppCompatAlertDialogStyle);
        loadDlg = new LoadingDialog(LoadMoneyCardFragment.this);
        toolbar = (Toolbar) findViewById(R.id.toolbars);
        ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtns);
        setSupportActionBar(toolbar);
        ivBackBtn.setVisibility(View.VISIBLE);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent shoppingIntent = new Intent(LoadMoneyCardFragment.this, MainActivity.class);
                startActivity(shoppingIntent);
                finish();
            }
        });
        try {
            final SSLSocketFactory sslSocketFactory = new TLSSocketFactory();
            rq = Volley.newRequestQueue(LoadMoneyCardFragment.this, new HurlStack(null, sslSocketFactory));
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        btnLoadMoney = (Button) findViewById(R.id.btnLoadMoney);
        etLoadMoneyAmount = (MaterialEditText) findViewById(R.id.etLoadMoneyAmount);
        rbLoadMoneyVBank = (RadioButton) findViewById(R.id.rbLoadMoneyVBank);
        rbLoadMoneyOther = (RadioButton) findViewById(R.id.rbLoadMoneyOther);

        rbLoadMoneyVBank.setVisibility(View.GONE);

        address1 = getIntent().getStringExtra("address1");
        address2 = getIntent().getStringExtra("address2");
        city = getIntent().getStringExtra("city");
        state = getIntent().getStringExtra("state");
        pinCode = getIntent().getStringExtra("pinCode");

        autoFill = getIntent().getStringExtra("AutoFill");
        Checkout.preload(LoadMoneyCardFragment.this);

        if (autoFill.equals("yes")) {
            String loadAmountString = getIntent().getStringExtra("splitAmount");
            loadAmount = Math.ceil(Double.parseDouble(loadAmountString));
            DecimalFormat format = new DecimalFormat("0.#");
            if (loadAmount < 10) {
                loadAmount = 10;
            }
            etLoadMoneyAmount.setText(String.valueOf(format.format(loadAmount)));
//            etLoadMoneyAmount.setEnabled(false);
        }

        btnLoadMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (rbLoadMoneyVBank.isChecked()) {
                    isVBank = true;
                } else if (rbLoadMoneyOther.isChecked()) {
                    isVBank = false;
                } else {
                    isVBank = true;
                }
                attemptLoad();

            }
        });


        //DONE CLICK ON VIRTUAL KEYPAD
        etLoadMoneyAmount.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (rbLoadMoneyVBank.isChecked()) {
                        isVBank = true;
                    } else if (rbLoadMoneyOther.isChecked()) {
                        isVBank = false;
                    } else {
                        isVBank = true;
                    }
                    attemptLoad();
                }
                return false;
            }
        });

    }


    private void attemptLoad() {
        etLoadMoneyAmount.setError(null);
        cancel = false;
        amount = etLoadMoneyAmount.getText().toString();
        checkPayAmount(amount);
//        checkUserType();

        if (cancel) {
            focusView.requestFocus();
        } else {
//            if (isVBank) {
//                checkTrxTime();
//            } else {
            showCustomDisclaimerDialog();
//            }
        }
    }

    public void showCustomDisclaimerDialog() {
        CustomDisclaimerDialog builder = new CustomDisclaimerDialog(LoadMoneyCardFragment.this);
        builder.setPositiveButton(this.getResources().getString(R.string.Accept), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                checkTrxTime();
            }
        });
        builder.setNegativeButton(this.getResources().getString(R.string.Dismiss), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public void showNonKYCDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(LoadMoneyCardFragment.this, R.string.dialog_title2, Html.fromHtml(generateKYCMessage()));
        builder.setNegativeButton(this.getResources().getString(R.string.Dismiss), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.show();
    }


    private void checkPayAmount(String amount) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
        try {
            if (!gasCheckLog.isValid) {
                etLoadMoneyAmount.setError(getString(gasCheckLog.msg));
                focusView = etLoadMoneyAmount;
                cancel = true;
            } else if (Integer.valueOf(amount) < 200) {
                etLoadMoneyAmount.setError(this.getResources().getString(R.string.Amount_cant_be_less_than_200));
                focusView = etLoadMoneyAmount;
                cancel = true;
            } else if (Integer.valueOf(amount) >= 10000) {
                etLoadMoneyAmount.setError(this.getResources().getString(R.string.Enter_amount_less_than_10000));
                focusView = etLoadMoneyAmount;
                cancel = true;
            } else if (autoFill.equals("yes")) {
                if (Integer.valueOf(amount) < loadAmount) {
                    DecimalFormat format = new DecimalFormat("0.#");
                    etLoadMoneyAmount.setError(this.getResources().getString(R.string.Amount_cant_be_less_than)+ format.format(loadAmount));
                    focusView = etLoadMoneyAmount;
                    cancel = true;
                }
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }

    private boolean checkUserType() {
        if (amount != null && !amount.isEmpty()) {
            if (Integer.valueOf(amount) > 10000) {
                focusView = etLoadMoneyAmount;
                cancel = true;
                if (session.getUserAcName().equals("Non-KYC")) {
                    showNonKYCDialog();
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }

    }

    public String generateMessage() {
        String source = "<b><font color=#000000>"+this.getResources().getString(R.string.Amount_to_load)+"</font></b>" + "<font color=#000000>" + amount + "</font><br>" +
                "<br><b><font color=#ff0000>"+this.getResources().getString(R.string.Are_you_sure_you_want_to_proceed)+"</font></b><br>";
        return source;
    }


    public String generateKYCMessage() {
        return "<b><font color=#000000>"+this.getResources().getString(R.string.Amount_to_load)+"</font></b>" + "<font color=#000000>" + amount + "</font><br>" +
                "<br><b><font color=#ff0000>"+this.getResources().getString(R.string.Sorry_you_cannot_load_more_than_10000_at_a_time)+"</font></b><br>" +
                "<br><b><font color=#ff0000>"+this.getResources().getString(R.string.Please_enter_10000_or_lesser_amount_to_continue)+"</font></b><br>";
    }

    public void checkTrxTime() {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("amount", etLoadMoneyAmount.getText().toString());

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("TRXTIMEURL", ApiUrl.URL_VALIDATE_CARD_TRX_TIME);
            Log.i("TRXTIMEREQ", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_VALIDATE_CARD_TRX_TIME, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.i("TRXTIMERES", response.toString());
                    try {
                        String message = response.getString("message");
                        String code = response.getString("code");
                        if (code != null && code.equals("S00")) {
                            loadDlg.dismiss();
//                            boolean success = response.getBoolean("status");
//                            if (success) {
                            try {
                                final Activity activity = LoadMoneyCardFragment.this;

                                final Checkout co = new Checkout();
                                co.setImage(R.drawable.toolbart_niki);

                                JSONObject options = new JSONObject();

                                options.put("name", "Load Money");
                                transcationID = response.getString("transactionRefNo");
                                authRefNo = response.getString("authReferenceNo");
                                SharedPreferences.Editor editor = getSharedPreferences("transactionRefNo", Context.MODE_PRIVATE).edit();
                                editor.clear();
                                editor.putString("transactionRefNo", transcationID);
                                editor.putString("authReferenceNo", response.getString("authReferenceNo")).apply();

                                options.put("description", transcationID);
                                //You can omit the image option to fetch the image from dashboard
                                options.put("image", ApiUrl.URL_DOMAIN_ + "resources/admin/assets/images/logo.png");
                                options.put("currency", "INR");
                                options.put("authReferenceNo", authRefNo);
                                options.put("amount", (Integer.parseInt(etLoadMoneyAmount.getText().toString()) * 100));

                                JSONObject preFill = new JSONObject();
                                preFill.put("email", session.getUserEmail());
                                preFill.put("contact", session.getUserMobileNo());

                                options.put("prefill", preFill);
                                Log.i("amount", preFill.toString());

                                co.open(activity, options);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

////                                verifyTransaction(amount);
//                            } else {
//                                CustomToast.showMessage(LoadMoneyFragment.this, message);
//                            }
                        } else if (code != null && code.equals("F03")) {
                            loadDlg.dismiss();
                            showInvalidSessionDialog();
                        } else {
                            loadDlg.dismiss();
                            CustomToast.showMessage(LoadMoneyCardFragment.this, message);
                        }

                    } catch (
                            JSONException e)

                    {
                        loadDlg.dismiss();
                        CustomToast.showMessage(LoadMoneyCardFragment.this, getResources().getString(R.string.server_exception2));
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    CustomToast.showMessage(LoadMoneyCardFragment.this, getResources().getString(R.string.server_exception));

                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);

        }

    }




    @SuppressWarnings("unused")
    @Override
    public void onPaymentSuccess(String razorpayPaymentID) {
        Log.i("values", razorpayPaymentID);
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
//      Toast.makeText(LoadMoneyFragment.this, "Payment Successful: " + razorpayPaymentID, Toast.LENGTH_SHORT).show();
//      URL_LOAD_MONEY_RESPONSE
            SharedPreferences sharedpreferences = getSharedPreferences("transactionRefNo", Context.MODE_PRIVATE);
//            JSONObject jsonObject = new JSONObject();
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("status", "Captured");
//            jsonRequest.put("authReferenceNo", sharedpreferences.getString("authReferenceNo", ""));
            jsonRequest.put("amount", String.valueOf(Integer.parseInt(etLoadMoneyAmount.getText().toString()) * 100));
//            jsonRequest.put("amount", sharedpreferences.getString("amount", ""));
            jsonRequest.put("transactionRefNo", sharedpreferences.getString("transactionRefNo", ""));
            jsonRequest.put("paymentId", razorpayPaymentID);
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }


//      Log.i("loadmoneys", jsonObject.toString());
//      CustomToast.showMessage(LoadMoneyFragment.this, jsonObject.toString());
//      AndroidNetworking.post(ApiUrl.URL_LOAD_MONEY_RESPONSE)
//        .addJSONObjectBody(jsonObject) // posting json
//        .setTag("test")
//        .setPriority(Priority.IMMEDIATE)
//        .build()
//        .getAsJSONObject(new JSONObjectRequestListener() {


        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            Log.i("Receipt Url", ApiUrl.URL_LOAD_MONEY_CARD_RESPONSE);
            postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_LOAD_MONEY_CARD_RESPONSE, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.i("respobjsaj", response.toString());
                    loadDlg.dismiss();
                    try {
                        if (response.getString("code").equalsIgnoreCase("S00")) {
                            if (!getIntent().getBooleanExtra("phycard", false)) {
                                CustomSuccessDialog customAlertDialog = new CustomSuccessDialog(LoadMoneyCardFragment.this, "", "Load money Successful");
                                customAlertDialog.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        SharedPreferences sharedpreferences = getSharedPreferences("transcationID", Context.MODE_PRIVATE);
                                        sharedpreferences.edit().clear().remove("transcationID").remove("amount").apply();
                                        sendRefresh();
                                    }
                                });
                                customAlertDialog.show();
                            } else {
                                loadCardDetails();
                            }
//                CustomToast.showMessage(LoadMoneyFragment.this, "Successful");
                        } else {
                            loadDlg.dismiss();
                            SharedPreferences sharedpreferences = getSharedPreferences("transcationID", Context.MODE_PRIVATE);
                            sharedpreferences.edit().clear().remove("transcationID").remove("amount").apply();
                            CustomToast.showMessage(LoadMoneyCardFragment.this, response.getString("message"));
                        }
                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    try {
                        loadDlg.dismiss();
                        CustomToast.showMessage(LoadMoneyCardFragment.this, getResources().getString(R.string.server_exception2));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }

    }

    public void loadCardDetails() {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {


            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("address1", address1);
            jsonRequest.put("address2", address2);
            jsonRequest.put("city", city);
            jsonRequest.put("state", state);
            jsonRequest.put("pinCode", pinCode);


        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            Log.i("Card Url", ApiUrl.URL_MATCHMOVE_GENRATEPHYSICALCARD);
            postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_MATCHMOVE_GENRATEPHYSICALCARD, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject JsonObj) {
                    try {

                        Log.i("Card Response", JsonObj.toString());
                        String message = JsonObj.getString("message");
                        String code = JsonObj.getString("code");

                        if (code != null && code.equals("S00")) {
//                            sendRefresh();
                            Toast.makeText(LoadMoneyCardFragment.this, message, Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(LoadMoneyCardFragment.this, MainActivity.class));
                            finishAffinity();

                        }
                        if (code != null && code.equals("F03")) {
                            loadDlg.dismiss();
                            Toast.makeText(LoadMoneyCardFragment.this, getResources().getString(R.string.Your_session_is_expire_please_login), Toast.LENGTH_SHORT).show();
                            sendLogout();

                        } else {
                            loadDlg.dismiss();
                            Toast.makeText(LoadMoneyCardFragment.this, message, Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        loadDlg.dismiss();
                        CustomToast.showMessage(LoadMoneyCardFragment.this, getResources().getString(R.string.server_exception2));
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    loadDlg.dismiss();
                    CustomToast.showMessage(LoadMoneyCardFragment.this, NetworkErrorHandler.getMessage(error, LoadMoneyCardFragment.this));
                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };

            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }

    }


    private void sendRefresh() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "1");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        startActivity(new Intent(LoadMoneyCardFragment.this, MainActivity.class));
        finishAffinity();
    }

    @SuppressWarnings("unused")
    @Override
    public void onPaymentError(int code, String response) {
        try {
            Toast.makeText(LoadMoneyCardFragment.this, getResources().getString(R.string.Payment_failed)+": " + code + " " + response, Toast.LENGTH_SHORT).show();
            LoadMoneyCardFragment.this.finish();
        } catch (Exception e) {
            Log.e(TAG, "Exception in onPaymentError", e);
        }
    }

    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(LoadMoneyCardFragment.this, R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton(this.getResources().getString(R.string.agree), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton(this.getResources().getString(R.string.Dismiss), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(LoadMoneyCardFragment.this).sendBroadcast(intent);
    }

    private void showInvalidTranDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(LoadMoneyCardFragment.this, R.string.dialog_title2, inValidMessage);
        builder.setNegativeButton(this.getResources().getString(R.string.Dismiss), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.show();
    }


    @Override
    public void onResume() {
        super.onResume();
    }

}
