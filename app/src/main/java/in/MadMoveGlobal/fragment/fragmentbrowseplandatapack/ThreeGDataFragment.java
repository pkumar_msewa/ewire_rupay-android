package in.MadMoveGlobal.fragment.fragmentbrowseplandatapack;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import in.MadMoveGlobal.adapter.MobilePlanAdapter;
import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.util.MobilePlansCheck;


/**
 * Created by Dushant on 08/27/2017.
 */
@SuppressLint("ValidFragment")
public class ThreeGDataFragment extends Fragment {
    private final String type;
    private View rootView;
    MobilePlansCheck plans = MobilePlansCheck.getInstance();
    private ListView listOfDataPackPlans;
    private TextView tvNoPlans;
    private SharedPreferences sharedpreferences;
    private String prepaidNo;

    @SuppressLint("ValidFragment")
    public ThreeGDataFragment(String prepaidNo, String type) {
        this.prepaidNo = prepaidNo;
        this.type=type;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_full_talk_time,container,false);
        listOfDataPackPlans = (ListView) rootView.findViewById(R.id.lvMobileDataPackPlans);
        tvNoPlans = (TextView) rootView.findViewById(R.id.tvNoPlans);
        sharedpreferences = getActivity().getSharedPreferences("prepaidValue", Context.MODE_PRIVATE);
        if (plans.getThreeGPlans().size() == 0) {
            listOfDataPackPlans.setVisibility(View.GONE);
            tvNoPlans.setVisibility(View.VISIBLE);
        } else {
            listOfDataPackPlans.setVisibility(View.VISIBLE);
            tvNoPlans.setVisibility(View.GONE);

            MobilePlanAdapter tabMobileList = new MobilePlanAdapter(getActivity(), plans.getThreeGPlans(), 0, sharedpreferences, prepaidNo, type);
            listOfDataPackPlans.setAdapter(tabMobileList);
        }

        return rootView;
    }
}
