package in.MadMoveGlobal.fragment.fragmentnaviagtionitems;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.SSLSocketFactory;

import in.MadMoveGlobal.custom.CustomAlertDialog;
import in.MadMoveGlobal.custom.CustomSuccessDialog;
import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.metadata.AppMetadata;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.util.CheckLog;
import in.MadMoveGlobal.util.EmailCouponsUtil;
import in.MadMoveGlobal.util.NetworkErrorHandler;
import in.MadMoveGlobal.util.PayingDetailsValidation;
import in.MadMoveGlobal.util.TLSSocketFactory;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;


/**
 * Created by Dushant on 08/27/2017.
 */
public class SharePointsFragment extends Fragment {
    public static final int PICK_CONTACT = 1;
    private static final int MOBILE_DIGITS = 10;
    private View rootView;
    private MaterialEditText etSharePointsNo;
    private MaterialEditText etSharePointsPoints;
    //    private MaterialEditText etSharePointsName, etSharePointsMessage;
    private TextView tvCurrentPoints;
    private UserModel session = UserModel.getInstance();
    private View focusView = null;
    private boolean cancel;
    private String transferNo, transferName, transferAmount, transferMessage;
    private RequestQueue rq;
    private LoadingDialog loadDlg;
    private JSONObject jsonRequest;

    //Volley Tag
    private String tag_json_obj = "json_share_points";


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            final SSLSocketFactory sslSocketFactory = new TLSSocketFactory();
            rq = Volley.newRequestQueue(getActivity(), new HurlStack(null, sslSocketFactory));
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        loadDlg = new LoadingDialog(getActivity());
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_share_points, container, false);
        tvCurrentPoints = (TextView) rootView.findViewById(R.id.tvCurrentPoints);
        tvCurrentPoints.setText("Available points: " + session.getUserPoints());
        etSharePointsPoints = (MaterialEditText) rootView.findViewById(R.id.etSharePointsPoints);
//        etSharePointsName = (MaterialEditText) rootView.findViewById(R.id.etSharePointsName);
        etSharePointsNo = (MaterialEditText) rootView.findViewById(R.id.etSharePointsNo);
//        etSharePointsMessage = (MaterialEditText) rootView.findViewById(R.id.etSharePointsMessage);

        Button btnSharePoints = (Button) rootView.findViewById(R.id.btnSharePoints);
        ImageButton ibSharePointsPhoneBook = (ImageButton) rootView.findViewById(R.id.ibSharePointsPhoneBook);


        ibSharePointsPhoneBook.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                startActivityForResult(intent, PICK_CONTACT);
            }
        });


        btnSharePoints.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptPayment();

            }
        });
        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String[] projection = {ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.NUMBER};
        switch (requestCode) {
            case (PICK_CONTACT):
                if (resultCode == Activity.RESULT_OK) {
                    Uri contactUri = data.getData();
                    Cursor c = getActivity().getContentResolver().query(contactUri, projection, null, null, null);
                    if (c != null) {
                        if (c.moveToFirst()) {
                            String name = c.getString(c
                                    .getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
//                            etSharePointsName.setText(name);
                            String phoneNumber = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            String finalNumber = phoneNumber.replaceAll("[^0-9\\+]", "");
                            if (finalNumber != null && !finalNumber.isEmpty()) {
                                removeCountryCode(finalNumber);
                            }
                        }
                        c.close();
                    }
                }
        }
    }

    private void attemptPayment() {
        etSharePointsNo.setError(null);
//        etSharePointsName.setError(null);
        etSharePointsPoints.setError(null);
//        etSharePointsMessage.setError(null);
        cancel = false;

        transferAmount = etSharePointsPoints.getText().toString();
        transferNo = etSharePointsNo.getText().toString();
//        transferName = etSharePointsName.getText().toString();
//        transferMessage = etSharePointsMessage.getText().toString();
        checkPayAmount(transferAmount);
        checkPhone(transferNo);
//        checkName(transferName);
//        checkMessage(transferMessage);
        if (cancel) {
            focusView.requestFocus();
        } else {
            promoteSharePoints();
        }
    }

    private void checkPhone(String phNo) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkMobileTenDigit(phNo);
        if (!gasCheckLog.isValid) {
            etSharePointsNo.setError(getString(gasCheckLog.msg));
            focusView = etSharePointsNo;
            cancel = true;
        }
    }


    private void checkPayAmount(String amount) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
        if (!gasCheckLog.isValid) {
            etSharePointsPoints.setError(getString(gasCheckLog.msg));
            focusView = etSharePointsPoints;
            cancel = true;
        } else if (Integer.valueOf(etSharePointsPoints.getText().toString()) < 10) {
            etSharePointsPoints.setError("Points should not be less than 10");
            focusView = etSharePointsPoints;
            cancel = true;
        }
    }

//    private void checkName(String name) {
//        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(name);
//        if (!gasCheckLog.isValid) {
//            etSharePointsName.setError(getString(gasCheckLog.msg));
//            focusView = etSharePointsName;
//            cancel = true;
//        }
//    }

//    private void checkMessage(String name) {
//        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(name);
//        if (!gasCheckLog.isValid) {
//            etSharePointsMessage.setError(getString(gasCheckLog.msg));
//            focusView = etSharePointsMessage;
//            cancel = true;
//        }
//    }


    private void removeCountryCode(String number) {
        if (hasCountryCode(number)) {
            int country_digits = number.length() - MOBILE_DIGITS;
            number = number.substring(country_digits);
            etSharePointsNo.setText(number);
        } else if (hasZero(number)) {
            if (number.length() >= 10) {
                int country_digits = number.length() - MOBILE_DIGITS;
                number = number.substring(country_digits);
                etSharePointsNo.setText(number);
            } else {
                CustomToast.showMessage(getActivity(), "Please select 10 digit no");
            }

        } else {
            etSharePointsNo.setText(number);
        }

    }

    private boolean hasCountryCode(String number) {
        return number.charAt(0) == '+';
    }

    private boolean hasZero(String number) {
        return number.charAt(0) == '0';
    }


    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    public void promoteSharePoints() {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("points", Integer.valueOf(etSharePointsPoints.getText().toString()));
            jsonRequest.put("receiverUsername", etSharePointsNo.getText().toString());
            jsonRequest.put("sessionId", session.getUserSessionId());
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_SHARE_POINTS, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.i("Send Money", response.toString());
                    try {
                        String code = response.getString("code");
                        if (code != null && code.equals("S00")) {
                            String jsonString = response.getString("response");
                            JSONObject jsonObject = new JSONObject(jsonString);
                            String sucessMessage = jsonObject.getString("details");
                            etSharePointsNo.getText().clear();
                            etSharePointsPoints.getText().clear();
                            loadDlg.dismiss();
                            sendRefresh();

                            try {
                                EmailCouponsUtil.emailForCoupons(tag_json_obj);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            showSuccessDialog();
//                            CustomToast.showMessage(getActivity(), sucessMessage);
                        } else if (code != null && code.equals("F03")) {
                            loadDlg.dismiss();
                            showInvalidSessionDialog();
                        } else {
                            loadDlg.dismiss();
                            if (response.has("message") && response.getString("message") != null) {
                                String message = response.getString("message");
                                CustomToast.showMessage(getActivity(), message);
                            } else {
                                CustomToast.showMessage(getActivity(), "Error message is null");
                            }
                        }
                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));

                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }

    }

    private void sendRefresh() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "3");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }


    private void showSuccessDialog() {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(getSuccessMessage(), Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(getSuccessMessage());
        }
        CustomSuccessDialog builder = new CustomSuccessDialog(getActivity(), "Shared Successfully", result);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    public String getSuccessMessage() {
        String source = "<b><font color=#000000> Receiver No: </font></b>" + "<font>" + transferNo + "</font><br>" +
                "<b><font color=#000000> Points: </font></b>" + "<font>"  + transferAmount + "</font><br><br>";
        return source;
    }

}
