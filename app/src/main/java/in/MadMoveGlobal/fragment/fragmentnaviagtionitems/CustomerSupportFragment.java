package in.MadMoveGlobal.fragment.fragmentnaviagtionitems;


import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;

import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.EwireRuPay.activity.CreateIssuesActivity;
import in.MadMoveGlobal.EwireRuPay.activity.MainActivity;
import in.MadMoveGlobal.model.UserModel;
import io.fabric.sdk.android.Fabric;


/**
 * Created by dushant on 20-06-2017.
 */

public class CustomerSupportFragment extends AppCompatActivity {

    private View rootView;
    private CardView cvallissues, cvcreateissues, cvstatus, cvhelpline, cvemail;
    private TextView tvcreateissues, tvhelpline, tvEmails;
    private Toolbar toolbar;
    private UserModel session = UserModel.getInstance();
    private ImageButton ivBackBtn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        // TODO: Move this to where you establish a user session
        logUser();

        setContentView(R.layout.fragment_customer_suport);
        setStatusBarGradiant(CustomerSupportFragment.this);

        tvcreateissues = (TextView) findViewById(R.id.tvcreateissues);
        tvhelpline = (TextView) findViewById(R.id.tvhelpline);
        tvEmails = (TextView) findViewById(R.id.tvEmails);

        cvallissues = (CardView) findViewById(R.id.cvallissues);
        cvcreateissues = (CardView) findViewById(R.id.cvcreateissues);
        //cvstatus = (CardView)rootView.findViewById(R.id.cvstatus);
        cvhelpline = (CardView) findViewById(R.id.cvhelpline);
        cvemail = (CardView) findViewById(R.id.cvemail);

        toolbar = (Toolbar) findViewById(R.id.toolbars);
        ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtns);
        setSupportActionBar(toolbar);
        ivBackBtn.setVisibility(View.VISIBLE);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent shoppingIntent = new Intent(CustomerSupportFragment.this, MainActivity.class);
                startActivity(shoppingIntent);
                finish();
            }
        });



        cvcreateissues.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(CustomerSupportFragment.this, CreateIssuesActivity.class));
            }
        });
        cvallissues.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(CustomerSupportFragment.this, IssuesActivity.class));
            }
        });

        cvhelpline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + "+919582120120"));
                if (ActivityCompat.checkSelfPermission(CustomerSupportFragment.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                startActivity(callIntent);
            }
        });

        cvemail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", "info@ewiresofttech.com", null));
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Customer Support");
                emailIntent.putExtra(Intent.EXTRA_CC, "info@ewiresofttech.com");
                emailIntent.putExtra(Intent.EXTRA_CC, "info@ewiresofttech.com");
                emailIntent.putExtra(Intent.EXTRA_CC, "info@ewiresofttech.com");
                emailIntent.putExtra(Intent.EXTRA_TEXT, "");
                startActivity(Intent.createChooser(emailIntent, "Send email..."));
            }
        });


    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void setStatusBarGradiant(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            Drawable background = activity.getResources().getDrawable(R.drawable.bg_gradient_new);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
    }
    public void onBackPressed() {


        Intent shoppingIntent = new Intent(CustomerSupportFragment.this, MainActivity.class);
        startActivity(shoppingIntent);
        finish();
    }
    private void logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        Crashlytics.setUserIdentifier(session.getUserMobileNo());
        Crashlytics.setUserEmail(session.getUserEmail());
        Crashlytics.setUserName(session.getUserFirstName() + " " + session.getUserLastName());
    }



}
