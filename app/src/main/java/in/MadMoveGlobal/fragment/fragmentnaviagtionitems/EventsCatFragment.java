package in.MadMoveGlobal.fragment.fragmentnaviagtionitems;

import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.MadMoveGlobal.adapter.EventCatAdapter;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.model.EventCategoryModel;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;

/**
 * Created by Dushant on 08/27/2017.
 */
public class EventsCatFragment extends Fragment {

    private RecyclerView rvEventCat;
    //    private JSONObject jsonRequest;
    private ProgressBar pbEventCat;
    private UserModel session = UserModel.getInstance();

    private String auth_key = "";

    //Volley
    private String tag_json_obj = "json_events";
    private JsonObjectRequest postReq;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_event_cat, container, false);
        rvEventCat = (RecyclerView) rootView.findViewById(R.id.rvEventCat);
        pbEventCat = (ProgressBar) rootView.findViewById(R.id.pbEventCat);
        checkAuth();
        return rootView;
    }

    public void checkAuth() {
        pbEventCat.setVisibility(View.VISIBLE);
        rvEventCat.setVisibility(View.GONE);
        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            Log.i("EventsAuthUrl", ApiUrl.URL_EVENT_AUTH);
            postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_EVENT_AUTH, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject JsonObj) {
                    try {
                        Log.i("EventsAuthResponse", JsonObj.toString());
                        String code = JsonObj.getString("code");
                        if (code != null && code.equals("S00")) {
                            auth_key = JsonObj.getString("response");
                            getEventCategory(auth_key);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getActivity(), "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    public void getEventCategory(String auth) {
        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("major", 1);
            jsonRequest.put("access_token", auth);
            Log.i("GetEvents", jsonRequest.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (jsonRequest != null) {
            Log.i("JsonRequestEVentCat", jsonRequest.toString());
            Log.i("EventsAuthUrl", ApiUrl.URL_EVENT_CAT_LIST);

            postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_EVENT_CAT_LIST, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject JsonObj) {
                    try {
                        Log.i("EventsCatResponse", JsonObj.toString());
                        String code = JsonObj.getString("code");

                        if (code != null && code.equals("S00")) {
                            List<EventCategoryModel> eventCatArray = null;
                            JSONArray eveCatArray = JsonObj.getJSONArray("details");

                            for (int i = 0; i < eveCatArray.length(); i++) {
                                JSONObject c = eveCatArray.getJSONObject(i);
                                long catId = c.getLong("categoryID");
                                String catName = c.getString("categoryName");
                                String catTicketSetting = c.getString("ticketSetting");
                                String catImage = c.getString("defaultThumbnailPath");
                                EventCategoryModel eveCatModel = new EventCategoryModel(catId, catName, catTicketSetting, catImage);
                                eventCatArray.add(eveCatModel);
                            }
                            if (eventCatArray != null && eventCatArray.size() != 0) {
                                int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.recycle_spacing);
                                rvEventCat.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
                                GridLayoutManager manager = new GridLayoutManager(getActivity(), 2);
                                rvEventCat.setLayoutManager(manager);
                                rvEventCat.setHasFixedSize(true);
                                EventCatAdapter eveCatAdp = new EventCatAdapter(getActivity(), eventCatArray);
                                rvEventCat.setAdapter(eveCatAdp);
                                pbEventCat.setVisibility(View.GONE);
                                rvEventCat.setVisibility(View.VISIBLE);

                            } else {
                                Toast.makeText(getActivity(), "Empty Array", Toast.LENGTH_SHORT).show();
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getActivity(), "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        public SpacesItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view,
                                   RecyclerView parent, RecyclerView.State state) {
            outRect.left = space;
            outRect.right = space;
            outRect.bottom = space;
            outRect.top = space;
        }
    }
}
