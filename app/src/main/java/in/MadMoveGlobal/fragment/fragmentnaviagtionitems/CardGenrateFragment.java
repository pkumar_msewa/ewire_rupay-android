package in.MadMoveGlobal.fragment.fragmentnaviagtionitems;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.crashlytics.android.Crashlytics;
import com.orm.query.Select;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.MadMoveGlobal.EwireRuPay.activity.PhysicalReciptActivity;
import in.MadMoveGlobal.custom.CustomAlertDialog;
import in.MadMoveGlobal.custom.CustomDisclaimerDialog;
import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.metadata.AppMetadata;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.EwireRuPay.activity.MainActivity;
import io.fabric.sdk.android.Fabric;

/**
 * Created by Dushant on 01/01/2018.
 */
public class CardGenrateFragment extends Fragment {
    private EditText etRegisterFName, etRegisterDob, etRegisterEmail, etHouse, etStreet, etState, etCity, etpin;

    private LoadingDialog loadDlg;


    private String gender = "M";
    private View rootView;
    private ImageView ivImagelogo, ivclose;
    private JSONObject jsonRequest;
    private Button btnBackPage, btnsecendpage, btnFirstback, btnThirdPage, btnsecondpageback, btnApply;
    private String quesCode;
    private Boolean isQuesSet = false;
    private LinearLayout llFirstPage, llnextpage, llcopy, llSecendpage, llFname, lldob, llEmail, llfirstpageback, llThirddpage, llphycrd, llcard;
    private ProgressBar pbcard;
    private String userBalance;
    private String transcationID, authRefNo;
    public static final String PROPERTY_REG_ID = "registration_id";
    private String tag_json_obj = "json_user";
    private Boolean phycard = true;
    //Volley Tag


    private String regId = "";
    private static final String KEY_NAME = "santosh";
    private UserModel session = UserModel.getInstance();
    private LinearLayout flmain;
    private JsonObjectRequest postReq;
    private String cardNo, expiryDate, cvv, accountHolder;
    private TextView tvCardnumber, tvCardholder, tvdate, tvcvv, tvtext, tvidcardfee;
    private int BalancedD;
    private String userBalancenew;
    private String State;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Fabric.with(getActivity(), new Crashlytics());
        // TODO: Move this to where you establish a user session
        logUser();

        rootView = inflater.inflate(R.layout.fragment_genratecard, container, false);
        loadDlg = new LoadingDialog(getActivity());
        llFirstPage = (LinearLayout) rootView.findViewById(R.id.llFirstPage);
        llnextpage = (LinearLayout) rootView.findViewById(R.id.llnextpage);
        llSecendpage = (LinearLayout) rootView.findViewById(R.id.llSecendpage);
        llFname = (LinearLayout) rootView.findViewById(R.id.llFname);
        lldob = (LinearLayout) rootView.findViewById(R.id.lldob);
        llEmail = (LinearLayout) rootView.findViewById(R.id.llEmail);
        llfirstpageback = (LinearLayout) rootView.findViewById(R.id.llfirstpageback);
        llThirddpage = (LinearLayout) rootView.findViewById(R.id.llThirddpage);
        llcopy = (LinearLayout) rootView.findViewById(R.id.llcopy);
        flmain = (LinearLayout) rootView.findViewById(R.id.flmain);
        ivclose = (ImageView) rootView.findViewById(R.id.ivclose);
        llphycrd = (LinearLayout) rootView.findViewById(R.id.llphycrd);
        llcard = (LinearLayout) rootView.findViewById(R.id.llcard);

        btnBackPage = (Button) rootView.findViewById(R.id.btnBackPage);
        btnsecendpage = (Button) rootView.findViewById(R.id.btnsecendpage);
        btnFirstback = (Button) rootView.findViewById(R.id.btnFirstback);
        btnThirdPage = (Button) rootView.findViewById(R.id.btnThirdPage);
        btnsecondpageback = (Button) rootView.findViewById(R.id.btnsecondpageback);
        btnApply = (Button) rootView.findViewById(R.id.btnApply);
        pbcard = (ProgressBar) rootView.findViewById(R.id.pbcard);

        tvCardnumber = (TextView) rootView.findViewById(R.id.tvCardnumber);
        tvCardholder = (TextView) rootView.findViewById(R.id.tvCardholder);
        tvdate = (TextView) rootView.findViewById(R.id.tvdate);
        tvcvv = (TextView) rootView.findViewById(R.id.tvcvv);

        etRegisterFName = (EditText) rootView.findViewById(R.id.etRegisterFName);
        etRegisterDob = (EditText) rootView.findViewById(R.id.etRegisterDob);
        etRegisterEmail = (EditText) rootView.findViewById(R.id.etRegisterEmail);
        etHouse = (EditText) rootView.findViewById(R.id.etHouse);
        etStreet = (EditText) rootView.findViewById(R.id.etStreet);
        etState = (EditText) rootView.findViewById(R.id.etState);
        etCity = (EditText) rootView.findViewById(R.id.etCity);
        etpin = (EditText) rootView.findViewById(R.id.etpin);
        tvidcardfee = (TextView) rootView.findViewById(R.id.tvidcardfee);
//        getUserBalance();
        etpin.addTextChangedListener(new MyTextWatcher(etpin));
        tvidcardfee.setText(getResources().getString(R.string.Card_issuance_fee_is) + session.getUsercardFees() +" "+ getResources().getString(R.string.including_taxes));
        btnBackPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PhysicalFragment fragment = new PhysicalFragment();
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.flmain, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

//                getActivity().finish();
            }
        });

        ivclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PhysicalFragment fragment = new PhysicalFragment();
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.flmain, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

//                getActivity().finish();
            }
        });

        btnsecendpage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                llFirstPage.setVisibility(View.GONE);
                llSecendpage.setVisibility(View.VISIBLE);

                Animation animation;
                animation = AnimationUtils.loadAnimation(getActivity(),
                        R.anim.slide_in_right);
                llSecendpage.startAnimation(animation);

                etRegisterFName.setFocusableInTouchMode(true);
                etRegisterFName.setFocusable(true);
                etRegisterFName.requestFocus();
                etRegisterFName.setText(session.getUserFirstName());
                etRegisterDob.setText(session.getUserDob());
                etRegisterEmail.setText(session.getUserEmail());

            }
        });


        btnFirstback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                llSecendpage.setVisibility(View.GONE);
                llFirstPage.setVisibility(View.VISIBLE);

                Animation animation;
                animation = AnimationUtils.loadAnimation(getActivity(),
                        R.anim.slide_in_left);
                llFirstPage.startAnimation(animation);

                etRegisterFName.setFocusableInTouchMode(false);
                etRegisterFName.setFocusable(false);
                etRegisterFName.requestFocus();


            }
        });


        btnThirdPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                submitForm();


            }
        });
        btnsecondpageback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                llThirddpage.setVisibility(View.GONE);
                llSecendpage.setVisibility(View.VISIBLE);

                Animation animation;
                animation = AnimationUtils.loadAnimation(getActivity(),
                        R.anim.slide_in_left);
                llSecendpage.startAnimation(animation);

                etHouse.setFocusableInTouchMode(false);
                etHouse.setFocusable(false);
                etHouse.requestFocus();
                etHouse.setFocusableInTouchMode(true);
                etHouse.setFocusable(true);
                etHouse.requestFocus();


            }
        });

        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                submitForm1();


            }
        });


        etRegisterDob.setFocusable(true);
        etRegisterDob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dateOfBirthDialog();
            }
        });
        etRegisterEmail.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    submitForm();
                }
                return false;
            }
        });


        return rootView;
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    private void submitForm() {


        if (!validateFName()) {
            return;
        }
        if (!validateDob()) {
            return;
        }
        if (!validateEmail()) {
            return;
        }

        llThirddpage.setVisibility(View.VISIBLE);
        llSecendpage.setVisibility(View.GONE);
        Animation animation;
        animation = AnimationUtils.loadAnimation(getActivity(),
                R.anim.slide_in_right);
        llThirddpage.startAnimation(animation);
        etHouse.setFocusableInTouchMode(true);
        etHouse.setFocusable(true);
        etHouse.requestFocus();
        etRegisterFName.setFocusableInTouchMode(false);
        etRegisterFName.setFocusable(false);
        etRegisterFName.requestFocus();

    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    private void submitForm1() {

        if (!validatePostal()) {
            return;
        }
        if (!validateCity()) {
            return;
        }
        if (!validateState()) {
            return;
        }
        if (!validateHName()) {
            return;
        }
        if (!validateSName()) {
            return;
        }


//        int ammount = Integer.parseInt(session.getUserminimumCardBalance());
//
//        if (BalancedD <= ammount) {
//
        DebitFromCard();
//        loadCardDetails();

//
//        } else {

//            attemptLoad();

//        }


    }


    private String getRegistrationId() {
        final SharedPreferences prefs = getGCMPreferences();
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            return "";
        }
        return registrationId;
    }

    private SharedPreferences getGCMPreferences() {
        return getActivity().getSharedPreferences("FIREBASE",
                Context.MODE_PRIVATE);
    }


    public void dateOfBirthDialog() {
        DatePickerDialog dialogDate = new DatePickerDialog(getActivity(), R.style.DatePicker, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar userAge = new GregorianCalendar(year, (monthOfYear + 1), dayOfMonth);
                Calendar minAdultAge = new GregorianCalendar();
                minAdultAge.add(Calendar.YEAR, -18);
                if (minAdultAge.before(userAge)) {
                    etRegisterDob.setError(getResources().getString(R.string.You_must_be_at_least_18_years_old_to_Register));
                    requestFocus(etRegisterDob);
                } else {

                    etRegisterDob.setText(String.valueOf(year) + "-"
                            + String.valueOf(monthOfYear + 1) + "-"
                            + String.valueOf(dayOfMonth));
                }
            }
        }, 1990, 1, 1);
        dialogDate.setButton(DatePickerDialog.BUTTON_POSITIVE, this.getResources().getString(R.string.zxing_button_ok), dialogDate);
        dialogDate.setButton(DatePickerDialog.BUTTON_NEGATIVE, this.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        dialogDate.getDatePicker().setMaxDate(new Date().getTime());
        dialogDate.getDatePicker().setSpinnersShown(true);
        dialogDate.getDatePicker().setCalendarViewShown(false);
        dialogDate.show();

    }


    private boolean validateFName() {
        if (etRegisterFName.getText().toString().trim().isEmpty()) {
            etRegisterFName.setError(getResources().getString(R.string.Enter_full_name));
            requestFocus(etRegisterFName);
            return false;
        } else {
            etRegisterFName.setError(null);
        }

        return true;
    }

    private boolean validateDob() {
        if (etRegisterDob.getText().toString().trim().isEmpty()) {
            etRegisterDob.setError(getResources().getString(R.string.Enter_date_of_birth));
            requestFocus(etRegisterDob);
            return false;
        } else {
            etRegisterDob.setError(null);
        }

        return true;
    }

    private boolean validateEmail() {
        String email = etRegisterEmail.getText().toString().trim();
        if (email.isEmpty() || !isValidEmail(email)) {
            etRegisterEmail.setError(getResources().getString(R.string.Enter_valid_email));
            requestFocus(etRegisterEmail);
            return false;
        } else {
            etRegisterEmail.setError(null);
        }

        return true;
    }

    private boolean validateHName() {
        if (etHouse.getText().toString().trim().isEmpty()) {
            etHouse.setError(getResources().getString(R.string.Enter_Flat_No_House_Name));
            requestFocus(etHouse);
            return false;
        } else {
            etHouse.setError(null);
        }

        return true;
    }

    private boolean validateSName() {
        if (etStreet.getText().toString().trim().isEmpty()) {
            etStreet.setError(getResources().getString(R.string.Landmark_Street_Name));
            requestFocus(etStreet);
            return false;
        } else {
            etStreet.setError(null);
        }

        return true;
    }

    private boolean validateState() {
        if (etState.getText().toString().trim().isEmpty()) {
            etState.setError(getResources().getString(R.string.Enter_State_name));
            requestFocus(etState);
            return false;
        } else {
            etState.setError(null);
        }

        return true;
    }

    private boolean validateCity() {
        if (etCity.getText().toString().trim().isEmpty()) {
            etCity.setError(getResources().getString(R.string.Enter_City_name));
            requestFocus(etCity);
            return false;
        } else {
            etCity.setError(null);
        }

        return true;
    }

    private boolean validatePostal() {
        if (etpin.getText().toString().trim().isEmpty() || etpin.getText().toString().trim().length() < 6) {
            etpin.setError(getResources().getString(R.string.Enter_Postal_Code));
            requestFocus(etpin);
            return false;
        } else {
            etpin.setError(null);
        }

        return true;
    }


    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }


    static String getEmail(Context context) {
        AccountManager accountManager = AccountManager.get(context);
        Account account = getAccount(accountManager);

        if (account == null) {
            return null;
        } else {
            return account.name;
        }
    }


    private static Account getAccount(AccountManager accountManager) {
        Account[] accounts = accountManager.getAccountsByType("com.google");
        Account account;
        if (accounts.length > 0) {
            account = accounts[0];
        } else {
            account = null;
        }
        return account;
    }

    private void sendRefresh() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "1");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    public String formateDateFromstring(String time) {
        String inputPattern = "yyyy-MM";
        String outputPattern = "MM/yy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        java.util.Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    public void getUserBalance() {
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("GET USER PARAMS", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_GET_USER_BALANCE, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.i("GET USER RESPONSE", response.toString());
                        String code = response.getString("code");

                        if (code != null && code.equals("S00")) {
                            double userBalanceD = response.getDouble("balance");
                            BalancedD = response.getInt("balance");
                            userBalance = String.valueOf(userBalanceD);
                            userBalancenew = String.valueOf(BalancedD);

                            List<UserModel> currentUserList = Select.from(UserModel.class).list();
                            UserModel currentUser = currentUserList.get(0);
                            currentUser.setUserBalance(userBalance);
                            session.setUserBalance(userBalance);
                            currentUser.save();
//                            MyBallance.setText(getResources().getString(R.string.rupease) + " " + userBalanceD);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hash", "1234");
                    return map;
                }

            };

            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    public void DebitFromCard() {
        btnApply.setVisibility(View.GONE);
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("amount", session.getUsercardFees());

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("GET USER PARAMS", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_GET_CARD_USER_BALANCE, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.i("GET USER RESPONSE", response.toString());
                        String code = response.getString("code");
                        String message = response.getString("message");

                        if (code != null && code.equals("S00")) {
//                            loadDlg.dismiss();

                            loadCardDetails();
//                            MyBallance.setText(getResources().getString(R.string.rupease) + " " + userBalanceD);
                        } else if (code != null && code.equals("F00")) {
                            CustomToast.showMessage(getActivity(), message);
                            loadDlg.dismiss();
                            btnApply.setVisibility(View.VISIBLE);
                        } else if (code != null && code.equals("F03")) {
                            loadDlg.dismiss();
                            btnApply.setVisibility(View.VISIBLE);
                            CustomToast.showMessage(getActivity(), getResources().getString(R.string.Please_login_and_try_again));
//                            Toast.makeText(getActivity(), "Your session is expire, please login", Toast.LENGTH_SHORT).show();
                            sendLogout();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hash", "1234");
                    return map;
                }

            };

            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton(getResources().getString(R.string.agree), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton(getResources().getString(R.string.Dismiss), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    public void loadCardDetails() {
        loadDlg.show();

        jsonRequest = new JSONObject();
        try {


            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("address1", etHouse.getText().toString());
            jsonRequest.put("address2", etStreet.getText().toString());
            jsonRequest.put("city", etCity.getText().toString());
            jsonRequest.put("state", etState.getText().toString());
            jsonRequest.put("pinCode", etpin.getText().toString());


        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            Log.i("Card Url", ApiUrl.URL_MATCHMOVE_GENRATEPHYSICALCARD);
            postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_MATCHMOVE_GENRATEPHYSICALCARD, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject JsonObj) {
                    try {

                        Log.i("Card Response", JsonObj.toString());
                        String message = JsonObj.getString("message");
                        String code = JsonObj.getString("code");

                        if (code != null && code.equals("S00")) {
//                            loadDlg.dismiss();

                            List<UserModel> currentUserList = Select.from(UserModel.class).list();
                            UserModel currentUser = currentUserList.get(0);
                            currentUser.setHasPhysicalRequest(true);
                            session.setHasPhysicalRequest(true);
                            currentUser.save();
//                            sendRefresh();
                            reciptsDialog(message);
//                            Toast.makeText(getActivity(),message, Toast.LENGTH_SHORT).show();

//                            sendRefresh();
//                            startActivity(new Intent(getActivity(), MainActivity.class));
//                            getActivity().finish();


                        }
                        if (code != null && code.equals("F03")) {
                            loadDlg.dismiss();
                            CustomToast.showMessage(getActivity(), getResources().getString(R.string.Please_login_and_try_again));
                            //Toast.makeText(getActivity(), "Your session is expire, please login", Toast.LENGTH_SHORT).show();
                            sendLogout();

                        } else {
                            loadDlg.dismiss();
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        loadDlg.dismiss();
                        CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    loadDlg.dismiss();
                    CustomToast.showMessage(getActivity(), getResources().getString(R.string.Network_connection_error));
                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };

            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }

    }


    @Override
    public void onResume() {
        super.onResume();
    }

    private void attemptLoad() {

        showCustomDisclaimerDialog();


    }

    public void showCustomDisclaimerDialog() {
        CustomDisclaimerDialog builder = new CustomDisclaimerDialog(getActivity());
        builder.setPositiveButton(getResources().getString(R.string.Accept), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                startActivity(new Intent(getActivity(), MainActivity.class));
                getActivity().finish();
            }
        });
        builder.setNegativeButton(getResources().getString(R.string.Dismiss), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

//    public void successDialogs() {
//        CustomDisclaimerDialog builder = new CustomDisclaimerDialog(getActivity());
//        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                dialog.dismiss();
////                sendRefresh();
//
//
//                Intent intent = new Intent(getActivity(), PhysicalReciptActivity.class);
//                intent.putExtra("State", etState.getText().toString());
//                startActivity(intent);
//
//
////                startActivity(new Intent(getActivity(), PhysicalReciptActivity.class));
////                intent.putExtra("fname", etFName.getText().toString());
////                getActivity().finish();
//
//
//            }
//        });
//
//        builder.show();
//    }


    public void reciptsDialog(String reciptmessages) {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(reciptmessages));
        builder.setPositiveButton(getResources().getString(R.string.agree), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
//                dialog.dismiss();
//                sendRefresh();


                Intent intent = new Intent(getActivity(), PhysicalReciptActivity.class);
                intent.putExtra("State", etState.getText().toString());
                startActivity(intent);


//                startActivity(new Intent(getActivity(), PhysicalReciptActivity.class));
//                intent.putExtra("fname", etFName.getText().toString());
//                getActivity().finish();


            }
        });

        builder.show();
    }


    private void logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        Crashlytics.setUserIdentifier(session.getUserMobileNo());
        Crashlytics.setUserEmail(session.getUserEmail());
        Crashlytics.setUserName(session.getUserFirstName() + " " + session.getUserLastName());
    }


    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            int i = view.getId();
            if (i == R.id.etpin) {
                if (editable.length() < CardGenrateFragment.this.getResources().getInteger(R.integer.passwordLength)) {

                    validatePostalCode();
                } else {
                    loadPostalCode();
                }
            }
        }

    }

    private boolean validatePostalCode() {
        if (etpin.getText().toString().trim().isEmpty()) {
            etpin.setError(getResources().getString(R.string.Required_Postal_Code));
            requestFocus(etpin);
            return false;
        } else if (etpin.getText().toString().trim().length() != 6) {
            etpin.setError(getResources().getString(R.string.password_validity));
            requestFocus(etpin);
            return false;
        } else {
            etpin.setError(null);
        }

        return true;
    }


    public void loadPostalCode() {
        loadDlg.show();
//        jsonRequest = new JSONObject();
//        try {
//            jsonRequest.put("sessionId", session.getUserSessionId());
//        } catch (JSONException e) {
//            e.printStackTrace();
//            jsonRequest = null;
//        }

//        if (jsonRequest != null) {
//            Log.i("LOADCIRCLEOP Request", jsonRequest.toString());
//            Log.i("LOADCIRCLEOP URL", ApiUrl.URL_CIRCLE_OPERATOR);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, "http://postalpincode.in/api/pincode/" + etpin.getText().toString(), (String) null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject JsonObj) {
                        loadDlg.dismiss();
                        Log.i("PostalCode", JsonObj.toString());
                        try {

                            //New Implementation

//                    String jsonString = JsonObj.getString("response");
//                    Log.i("Escape str", jsonEscape(jsonString));
//
//                    JSONObject response = new JSONObject(jsonString);

                            JSONArray operatorArray = JsonObj.getJSONArray("PostOffice");

//                        operatorList.add(new OperatorsModel("", "Select your operator", ""));

                            for (int i = 0; i < operatorArray.length(); i++) {
                                JSONObject c = operatorArray.getJSONObject(i);
                                String Circle = c.getString("Circle");
                                String State = c.getString("State");
//                                String op_default_code = c.getString("serviceLogo");

                                etCity.setText(Circle);
                                etState.setText(State);
//                                OperatorsModel oModel = new OperatorsModel(op_code, op_name, op_default_code);
//                                oModel.save();
//                                operatorList.add(oModel);
//                                operatorLists.add(oModel);
//                                Log.i("OperatorsModel", String.valueOf(operatorList.get(i).getCode()));
//                                operatorIdCode.put(op_code, i + 1);
                            }
//                            spService.setHint("Operator");
//
//                            JSONArray circleArray = JsonObj.getJSONArray("circles");
//
//                            circleList.add(new CircleModel("", "Select your circle"));
//                            for (int i = 0; i < circleArray.length(); i++) {
//                                JSONObject c = circleArray.getJSONObject(i);
//                                String op_code = c.getString("code");
//                                String op_name = c.getString("name");
//                                CircleModel cModel = new CircleModel(op_code, op_name);
//                                cModel.save();
//                                circleList.add(cModel);
//                                circleLists.add(cModel);
//                                circleIdCode.put(op_code, i + 1);
//                            }
//                            spArea.setHint("Circle");

//                            if (operatorList != null && operatorList.size() != 0) {
//                                try {
//                                    operatorList.add(0, new OperatorsModel("", "Select your operator", ""));
//                                    spinnerAdapterOperator = new OperatorSpinnerAdapter(getActivity(), android.R.layout.simple_spinner_item, operatorList);
//                                    spinnerPrepaidProvider.setAdapter(spinnerAdapterOperator);
//                                } catch (Exception e) {
//                                    e.printStackTrace();
//                                }
//                            }

//                    if (circleList != null && circleList.size() != 0) {
//                        try {
//                            circleList.add(0, new CircleModel("", "Select your circle"));
//                            spinnerAdapterCircle = new CircleSpinnerAdapter(getActivity(), android.R.layout.simple_spinner_item, circleList);
//                            spinnerPrepaidCircle.setAdapter(spinnerAdapterCircle);
//
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), getResources().getString(R.string.server_exception2), Toast.LENGTH_SHORT).show();
//                            Snackbar.make(llLayouts, getResources().getString(R.string.server_exception2), Snackbar.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loadDlg.dismiss();
                error.printStackTrace();
                Toast.makeText(getActivity(), getResources().getString(R.string.server_exception), Toast.LENGTH_SHORT).show();
//                Snackbar.make(llLayouts, getResources().getString(R.string.server_exception), Snackbar.LENGTH_LONG).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> map = new HashMap<>();
                map.put("hash", "1234");

                String basicAuth = "Basic " + new String(Base64.encode("user:pass".getBytes(), Base64.NO_WRAP));
                map.put("Authorization", basicAuth);


                return map;
            }

        };
        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonObjReq.setRetryPolicy(policy);
        PayQwikApplication.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);


    }


}



