package in.MadMoveGlobal.fragment.fragmentnaviagtionitems;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.MadMoveGlobal.custom.CustomAlertDialog;
import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.metadata.AppMetadata;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.util.NetworkErrorHandler;

/**
 * Created by Dushant on 08/27/2017.
 */
public class YuppTvFragment extends Fragment {

    private View rootView;

    private TextView tvLabel;
    private UserModel session = UserModel.getInstance();
   private Button btnSubscribe;
    private RequestQueue rq;
    private LoadingDialog loadDlg;
    private JSONObject jsonRequest;

    //Volley Tag
    private String tag_json_obj = "json_share_points";


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadDlg = new LoadingDialog(getActivity());
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_yupptv, container, false);
       tvLabel = (TextView) rootView.findViewById(R.id.tvLabel);
        btnSubscribe = (Button) rootView.findViewById(R.id.btnSubscribe);
        startYupTV();

        return rootView;
    }


    private void startYupTV() {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("firstName", session.getUserFirstName());
            jsonRequest.put("lastName", session.getUserLastName());
            jsonRequest.put("mobile", session.getUserMobileNo());
            jsonRequest.put("unique_id", session.getUserMobileNo());
            jsonRequest.put("region", "bangalore");
            jsonRequest.put("email", session.getUserEmail());

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("YUPTVREQ", jsonRequest.toString());
            Log.i("YUPTVURL", ApiUrl.URL_YUP_TV_REGISTER);
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_YUP_TV_REGISTER, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.i("YUPTVRES", response.toString());
                        String messageAPI = response.getString("message");
                        String code = response.getString("code");
                        if (code != null && code.equals("S00") || code.equals("S01") || code.equals("F00")) {
//                          String responseStr = response.getString("response");
//                            JSONObject responseObj = new JSONObject(responseStr);
//                            String detailsStr = responseObj.getString("details");
//                            JSONObject details = new JSONObject(detailsStr);
//                            JSONObject userdetail = details.getJSONObject("Userdetail");
                            String token = response.getString("token");
                            String userId = response.getString("userId");
                            String partnerId = response.getString("partnerId");
                            loadDlg.dismiss();
//                            openYuppTvApp(getActivity(), "com.tru",partnerId,userId,token);
                            http://www.yupptv.in/#!/home
                            openYuppTvWeb();
                            getActivity().finish();

                        } else if (code != null && code.equals("F03")) {
                            loadDlg.dismiss();
                            showInvalidSessionDialog();
                        } else {
                            CustomToast.showMessage(getActivity(), messageAPI);
                            loadDlg.dismiss();

                        }
                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
                        Log.i("Type", "jSON");
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.i("Type", "ERro");
                    CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
                    loadDlg.dismiss();


                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "12345");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    public void openYuppTvApp(Context context, String packageName, String partnerID, String userID, String tokenID) {
        Intent intent = context.getPackageManager().getLaunchIntentForPackage(packageName);
        if (intent != null) {
            // We found the activity now start the activity
            Intent intent1=new Intent(Intent.ACTION_VIEW);
            intent1.setData (Uri.parse("http://yupptv.in/#!/partner/"+partnerID+"/"+userID+"/"+tokenID));
            startActivity(intent1);
        } else {
            // Bring user to the market or let them choose an app?
            intent = new Intent(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.tru&hl=en"));
            context.startActivity(intent);
        }
    }

    public void openYuppTvWeb(){
        Fragment fragment = null;
        Bundle navBundle = new Bundle();
        navBundle.putString(AppMetadata.FRAGMENT_TYPE, AppMetadata.FRAGMENT_TYPE);
        fragment = new TravelHealthFragment();
        Bundle bType = new Bundle();
        bType.putString("URL", "http://www.yupptv.in/#!/home");
        fragment.setArguments(bType);
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }



    private void sendRefresh() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "3");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }


//    private void showSuccessDialog() {
//        Spanned result;
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
//            result = Html.fromHtml(getSuccessMessage(), Html.FROM_HTML_MODE_LEGACY);
//        } else {
//            result = Html.fromHtml(getSuccessMessage());
//        }
//        CustomSuccessDialog builder = new CustomSuccessDialog(getActivity(), "Shared Successfully", result);
//        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                dialog.dismiss();
//
//            }
//        });
//        builder.show();
//    }

//    public String getSuccessMessage() {
//        String source = "<b><font color=#000000> Receiver No: </font></b>" + "<font>" + transferNo + "</font><br>" +
//                "<b><font color=#000000> Points: </font></b>" + "<font>"  + transferAmount + "</font><br><br>";
//        return source;
//    }

}
