package in.MadMoveGlobal.fragment.fragmentnaviagtionitems;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.text.ClipboardManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.crashlytics.android.Crashlytics;
import com.orm.query.Select;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.MadMoveGlobal.EwireRuPay.EkycActivity;
import in.MadMoveGlobal.EwireRuPay.activity.LoadMoneyActivity;
import in.MadMoveGlobal.custom.CustomAlertDialog;
import in.MadMoveGlobal.custom.CustomSuccessDialog;
import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.EwireRuPay.activity.MainActivity;
import in.MadMoveGlobal.util.NetworkErrorHandler;
import io.fabric.sdk.android.Fabric;


import static in.MadMoveGlobal.metadata.ApiUrl.URL_BLOCK_VIRTUAL_CARD;
import static in.MadMoveGlobal.metadata.ApiUrl.URL_REACTIVATE_CARD;

/**
 * Created by Dushant on 01/01/2018.
 */
public class PhysicalFragment extends Fragment {
    public static final String PHONE = "phone";
    private LoadingDialog loadDlg;
    private LinearLayout llField, llActiv, llGC, llcard, llcopys, llActivation, llBlock, llUnBlock;
    private TextView tvGeneratecard, tvActivate;
    private View rootView;
    private ImageView addCard, close, ivcloses, ivCopy;
    private UserModel session = UserModel.getInstance();
    private ProgressBar pbcard;
    private FrameLayout flmain;
    private JSONObject jsonRequest;
    //Volley Tag
    private String tag_json_obj = "json_user";
    private JsonObjectRequest postReq;
    private String cardNo, expiryDate, cvv, accountHolder;
    private TextView tvCardnumber, tvCardholder, tvdate, tvcvv, tvtext;
    private EditText etProxyNumber, pin, conformPin;
    private Button btnActiv, btnActivate;
    private String userBalance, userBalancenew;
    private LinearLayout llreset, lltabs;
    private ImageView imagToolbar, tvfedral;
    private double BalancedD;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Fabric.with(getActivity(), new Crashlytics());
        // TODO: Move this to where you establish a user session
        logUser();

        rootView = inflater.inflate(R.layout.fragment_physical_card, container, false);
        flmain = (FrameLayout) rootView.findViewById(R.id.flmain);
        llField = (LinearLayout) rootView.findViewById(R.id.llField);
        llcard = (LinearLayout) rootView.findViewById(R.id.llcard);
        tvGeneratecard = (TextView) rootView.findViewById(R.id.tvGeneratecard);
        etProxyNumber = (EditText) rootView.findViewById(R.id.etproxyno);
//        pin = (EditText) rootView.findViewById(R.id.etApin);
//        conformPin = (EditText) rootView.findViewById(R.id.etAcpin);
        Button btnBackPage = (Button) rootView.findViewById(R.id.btnBackPage);
        btnActiv = (Button) rootView.findViewById(R.id.btnActiv);
        tvActivate = (TextView) rootView.findViewById(R.id.tvActivate);
        addCard = (ImageView) rootView.findViewById(R.id.ivAdd);
        close = (ImageView) rootView.findViewById(R.id.ivclose);
        ivcloses = (ImageView) rootView.findViewById(R.id.ivcloses);
        tvCardnumber = (TextView) rootView.findViewById(R.id.tvCardnumber);
        tvCardholder = (TextView) rootView.findViewById(R.id.tvCardholder);
        tvdate = (TextView) rootView.findViewById(R.id.tvdate);
        tvcvv = (TextView) rootView.findViewById(R.id.tvcvv);
        llcopys = (LinearLayout) rootView.findViewById(R.id.llCopys);
        llActivation = (LinearLayout) rootView.findViewById(R.id.llActivation);
        llGC = (LinearLayout) rootView.findViewById(R.id.llGC);
        llActiv = (LinearLayout) rootView.findViewById(R.id.llActiv);
        pbcard = (ProgressBar) rootView.findViewById(R.id.pbcard);
        llreset = (LinearLayout) rootView.findViewById(R.id.llreset);
        btnActivate = (Button) rootView.findViewById(R.id.btnActivate);
        loadDlg = new LoadingDialog(getActivity());
        imagToolbar = (ImageView) rootView.findViewById(R.id.imagToolbar);
        ivCopy = (ImageView) rootView.findViewById(R.id.ivCopy);
        tvfedral = (ImageView) rootView.findViewById(R.id.tvfedral);
        lltabs = (LinearLayout) rootView.findViewById(R.id.lltabs);
        llBlock = (LinearLayout) rootView.findViewById(R.id.llBlock);
        llUnBlock = (LinearLayout) rootView.findViewById(R.id.llUnBlock);
        llField.setVisibility(View.VISIBLE);
        llGC.setVisibility(View.GONE);
        llActiv.setVisibility(View.GONE);

        Log.i("VCARD", String.valueOf(session.isHasVcard()));
        Log.i("PCARD", String.valueOf(session.isHasPcard()));
        Log.i("PHYREQ", String.valueOf(session.isHasPhyReq()));
        Log.i("PHYSTATUS", String.valueOf(session.getUserPhysicalRequest()));

        lltabs.setVisibility(View.GONE);

        if (session.getUserPhysicalRequest() != null && session.getUserPhysicalRequest().trim().equals("Inactive") && session.isHasPcard()) {

            successActivateDialog(ActivateMessages());


        } else {
        }

        if (session.getUserPhysicalRequest().equals("Inactive")) {


            btnActiv.setVisibility(View.VISIBLE);
        }


        if (!session.isHasPcard()) {
            if (session.isHasPhyReq()) {
                Picasso.with(getActivity()).load("gkdhfg").resize(600, 200).placeholder(R.drawable.phy_atcrd).into(imagToolbar);

            } else {

                Picasso.with(getActivity()).load("gkdhfg").resize(600, 200).placeholder(R.drawable.physical_background).into(imagToolbar);
            }

        }
        getUserBalance();


        if (session.isHasPcard() && session.getUserPhysicalRequest().equalsIgnoreCase("Active")) {

            if (session.isPCStatus && session.getUserPhysicalBlock().equalsIgnoreCase("Inactive")) {
                llcard.setVisibility(View.VISIBLE);
                llcard.setBackgroundResource(R.drawable.debitblock);
                tvCardholder.setVisibility(View.GONE);
                tvcvv.setVisibility(View.GONE);
                tvdate.setVisibility(View.GONE);
                tvCardnumber.setVisibility(View.GONE);
                lltabs.setVisibility(View.VISIBLE);
                llUnBlock.setVisibility(View.VISIBLE);
                llBlock.setVisibility(View.GONE);
            } else
                physicalCardFetch();
        } else {


            addCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if (session.isHasPhyReq()) {

                        if (session.getUserAcName() != null && !session.getUserAcName().trim().equals("KYC")) {


                            if (session.isHaskycRequest()) {
                                requestDialog(genraterequest());

                            } else {
                                successDialogss(generateVerifyMessagess());
                            }
                        } else

                            llActivation.setVisibility(View.VISIBLE);

                    } else {

                        if (!session.isHaskycRequest() && !session.getUserAcName().trim().equals("KYC")) {
                            requestDialog(genraterequestKyc());

                        } else {
//                        Log.i("BALALA", userBalance);

                            double ammount = Double.parseDouble((session.getUserminimumCardBalance()));

                            Log.i("Ammount", String.valueOf(ammount) + "   BalancedD : " + session.isHasPcard());

                            if (session.isHasPcard()) {
                                attemptLoad();
                            } else {


                                if (BalancedD < ammount) {
                                    attemptLoad();

                                } else {
                                    CardGenrateFragment fragment = new CardGenrateFragment();
                                    FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                                    fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, 0);
                                    fragmentTransaction.replace(R.id.flmain, fragment);
                                    fragmentTransaction.addToBackStack(null);
                                    fragmentTransaction.commit();
                                }
                            }
                        }
                    }
                }
            });

            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    addCard.setVisibility(View.VISIBLE);
                    close.setVisibility(View.GONE);
                    llField.setVisibility(View.GONE);
                    llGC.setVisibility(View.GONE);
                    llActiv.setVisibility(View.GONE);
                    tvGeneratecard.setVisibility(View.GONE);
                    tvActivate.setVisibility(View.GONE);
                    flmain.setBackgroundResource(R.drawable.physical_background);

                }
            });


            tvGeneratecard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    CardGenrateFragment fragment = new CardGenrateFragment();
                    FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, 0);
                    fragmentTransaction.replace(R.id.flmain, fragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();


                }
            });


            tvActivate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    llActivation.setVisibility(View.VISIBLE);


                }
            });


            ivcloses.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    llActivation.setVisibility(View.GONE);

                }
            });

            btnActiv.setOnClickListener(new View.OnClickListener() {
                @SuppressLint("NewApi")
                @Override
                public void onClick(View v) {


                    submitForm();

                }
            });

        }

        llreset.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View v) {

                successDialog(generateVerifyMessage());


            }
        });

        llBlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                blockVirtualCard();
                llBlock.setVisibility(View.GONE);
                llUnBlock.setVisibility(View.VISIBLE);
            }
        });

        llUnBlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//        Intent menuIntent = new Intent(getActivity(), MainMenuDetailActivity.class);
//        menuIntent.putExtra(AppMetadata.FRAGMENT_TYPE, "Points");
//        startActivity(menuIntent);
                activateVirtualCard();
                llUnBlock.setVisibility(View.GONE);
                llBlock.setVisibility(View.VISIBLE);


            }
        });
        return rootView;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void submitForm() {


        if (!ProxyNo()) {
            return;
        }
//        if (!validateMPin()) {
//            return;
//        }
        AddPhysicalCard();


    }


    public void physicalCardFetch() {

        loadDlg.show();

        jsonRequest = new JSONObject();
        try {

            jsonRequest.put("sessionId", session.getUserSessionId());


        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }
//
        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            Log.i("Card Url", ApiUrl.URL_MATCHMOVE_PhysicalCardFETCH);
            postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_MATCHMOVE_PhysicalCardFETCH, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject JsonObj) {
                    try {

                        Log.i("Card Response", JsonObj.toString());
                        String message = JsonObj.getString("message");
                        String code = JsonObj.getString("code");

                        if (code != null && code.equals("S00")) {
                            loadDlg.dismiss();

                            cardNo = JsonObj.getString("walletNumber");
                            expiryDate = JsonObj.getString("expiryDate");
                            cvv = JsonObj.getString("cvv");
                            accountHolder = JsonObj.getString("holderName");
//
                            flmain.setBackgroundResource(R.color.text_white);
//                            Picasso.with(getActivity()).load("gkdhfg").resize(200, 200).placeholder(R.drawable.phy_atcrd).into((Target) flmain);

                            llcard.setVisibility(View.VISIBLE);
//                            llcopy.setVisibility(View.VISIBLE);
//                            llreset.setVisibility(View.VISIBLE);

                            if (session.getUserPhysicalRequest() != null && session.getUserPhysicalRequest().trim().equals("Inactive")) {

                                lltabs.setVisibility(View.GONE);

                            } else {


                                addCard.setVisibility(View.GONE);

//                                pbcard.setVisibility(View.GONE);
                                tvCardholder.setText(accountHolder);
                                tvCardnumber.setText(cardNo.replaceAll("\\D", "").replaceAll("(\\d{4}(?!$))", "$1 "));
                                Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "OCR-a___.ttf");
//                                card_test
                                tvdate.setTypeface(tf);
                                tvCardholder.setTypeface(tf);
                                tvCardnumber.setTypeface(tf);
                                tvcvv.setTypeface(tf);

                                tvCardholder.setVisibility(View.VISIBLE);
                                tvCardnumber.setVisibility(View.VISIBLE);
                                ivCopy.setVisibility(View.VISIBLE);
                                tvdate.setVisibility(View.VISIBLE);
                                tvcvv.setVisibility(View.VISIBLE);
                                tvfedral.setVisibility(View.VISIBLE);

                                llcard.setBackgroundResource(R.drawable.debitcard);

                                String date_before = expiryDate.toLowerCase();
                                String date_after = formateDateFromstring(date_before);
                                tvdate.setText("Valid Thru  " + date_after);
                                tvcvv.setText("cvv  " + cvv);

                                lltabs.setVisibility(View.VISIBLE);


                                llcopys.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        ClipboardManager cm = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
                                        cm.setText(tvCardnumber.getText());
                                        Toast.makeText(getActivity(), getResources().getString(R.string.Card_Number_Copied_to_clipboard), Toast.LENGTH_SHORT).show();
                                    }
                                });

//                                llcard.setOnClickListener(new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View view) {
//                                        ClipboardManager cm = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
//                                        cm.setText(tvCardnumber.getText());
//                                        Toast.makeText(getActivity(), "Copied to clipboard", Toast.LENGTH_SHORT).show();
//                                    }
//                                });


//                                llcard.setOnLongClickListener(new View.OnLongClickListener() {
//
//                                    @Override
//                                    public boolean onLongClick(View v) {
//                                        ClipboardManager cm = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
//                                        cm.setText(tvCardnumber.getText());
//                                        Toast.makeText(getActivity(), "Copied to clipboard", Toast.LENGTH_SHORT).show();
//                                        return true;
//                                    }
//                                });
                            }

                        } else if (code != null && code.equals("F03")) {
                            loadDlg.dismiss();
                            llcard.setVisibility(View.VISIBLE);
                            llcard.setBackgroundResource(R.drawable.debitcard);
                            tvCardnumber.setVisibility(View.GONE);
                            ivCopy.setVisibility(View.GONE);
                            tvdate.setVisibility(View.GONE);
                            tvcvv.setVisibility(View.GONE);
                            tvCardholder.setVisibility(View.GONE);
                            addCard.setVisibility(View.GONE);
                            lltabs.setVisibility(View.GONE);
                            CustomToast.showMessage(getActivity(), getResources().getString(R.string.Please_login_and_try_again));
                            sendLogout();


                        } else if (code != null && code.equals("F00")) {
                            loadDlg.dismiss();
                            llcard.setVisibility(View.VISIBLE);
                            llcard.setBackgroundResource(R.drawable.debitcard);
                            tvCardnumber.setVisibility(View.GONE);
                            tvdate.setVisibility(View.GONE);
                            tvcvv.setVisibility(View.GONE);
                            lltabs.setVisibility(View.GONE);
                            addCard.setVisibility(View.GONE);
                            tvCardholder.setVisibility(View.GONE);


                        } else {
                            loadDlg.dismiss();
                            llcard.setVisibility(View.VISIBLE);
                            tvCardnumber.setVisibility(View.GONE);
                            ivCopy.setVisibility(View.GONE);
                            llcard.setBackgroundResource(R.drawable.debitcard);
                            tvdate.setVisibility(View.GONE);
                            tvcvv.setVisibility(View.GONE);
                            lltabs.setVisibility(View.GONE);
                            tvCardholder.setVisibility(View.GONE);
                            addCard.setVisibility(View.GONE);
                            llcard.setBackgroundResource(R.color.text_white);
//                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        loadDlg.dismiss();
                        CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    llcard.setBackgroundResource(R.color.text_white);
                    lltabs.setVisibility(View.GONE);
//                    pbcard.setVisibility(View.GONE);
                    CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }
            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }

    }


    public void AddPhysicalCard() {
        loadDlg.show();


        jsonRequest = new JSONObject();
        try {

            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("proxy_number", etProxyNumber.getText().toString());

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            Log.i("Card Url", ApiUrl.URL_ADD_PHYSICALCARD);
            postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_ADD_PHYSICALCARD, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject JsonObj) {
                    try {

                        Log.i("Card Response", JsonObj.toString());
                        String message = JsonObj.getString("message");
                        String code = JsonObj.getString("code");

                        if (code != null && code.equals("S00")) {

                            loadDlg.dismiss();
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

                            List<UserModel> currentUserList = Select.from(UserModel.class).list();
                            UserModel currentUser = currentUserList.get(0);
                            currentUser.setPhysicalCardStatus("Inactive");
                            session.setPhysicalCardStatus("Inactive");
                            currentUser.save();


                            loadDlg.dismiss();
                            sendRefresh();


                            Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                            activateCard();


                        }
                        if (code != null && code.equals("F03")) {

                            loadDlg.dismiss();
//                            Toast.makeText(getActivity(), "Your session is expire, please login", Toast.LENGTH_SHORT).show();
                            CustomToast.showMessage(getActivity(), getResources().getString(R.string.Please_login_and_try_again));
                            sendLogout();

                        }
                        if (code != null && code.equals("F00")) {
                            loadDlg.dismiss();
                            Toast.makeText(getActivity(), getResources().getString(R.string.Your_Activation_request_is_failed), Toast.LENGTH_SHORT).show();

                        } else {
                            loadDlg.dismiss();
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        loadDlg.dismiss();
                        CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }

    }

    public String formateDateFromstring(String time) {
        String inputPattern = "yyyy-MM";
        String outputPattern = "MM/yy";
        @SuppressLint("SimpleDateFormat") SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        @SuppressLint("SimpleDateFormat") SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        java.util.Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    private void sendRefresh() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "1");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }


    private boolean ProxyNo() {
        if (etProxyNumber.getText().toString().trim().isEmpty()) {
            etProxyNumber.setError(getResources().getString(R.string.Enter_proxy_number));
            requestFocus(etProxyNumber);
            return false;
        } else {
            etProxyNumber.setError(null);
        }

        return true;
    }


    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    public void showSuccessDialog() {
        CustomSuccessDialog builder = new CustomSuccessDialog(getActivity(), getResources().getString(R.string.Card_Activated), getResources().getString(R.string.Your_card_is_Activated));
        builder.setPositiveButton(getResources().getString(R.string.agree), new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                sendRefresh();
                getActivity().finish();
                //   getActivity().finish();
            }
        });
        builder.show();
    }

    private void activateCard() {

        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_ACTIVATION_CARD, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObj) {
                    try {
                        String code = jsonObj.getString("code");
                        String message = jsonObj.getString("message");
                        if (code != null & code.equals("S00")) {
                            loadDlg.dismiss();

                            List<UserModel> currentUserList = Select.from(UserModel.class).list();
                            UserModel currentUser = currentUserList.get(0);
                            currentUser.setHasPcard(true);
                            currentUser.setPhysicalCardStatus("Active");
                            session.setHasPcard(true);
                            session.setPhysicalCardStatus("Active");
                            currentUser.save();


                            Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                            Intent mainIntent = new Intent(getActivity(), MainActivity.class);
                            startActivity(mainIntent);
                            getActivity().finish();

                            if (code != null & code.equals("F00")) {
                                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                                Intent mainIntents = new Intent(getActivity(), MainActivity.class);
                                startActivity(mainIntents);
                                getActivity().finish();
                            }
                        } else {
                            loadDlg.dismiss();


                        }
                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));

                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    public void getUserBalance() {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("GET USER PARAMS", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_GET_USER_BALANCE, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.i("GET USER RESPONSE", response.toString());
                        String code = response.getString("code");

                        if (code != null && code.equals("S00")) {
                            double userBalanceD = response.getDouble("balance");
                            BalancedD = userBalanceD;

                            userBalance = String.valueOf(userBalanceD);
                            userBalancenew = String.valueOf(BalancedD);


                            List<UserModel> currentUserList = Select.from(UserModel.class).list();
                            UserModel currentUser = currentUserList.get(0);
                            currentUser.setUserBalance(userBalance);
                            session.setUserBalance(userBalance);
                            currentUser.save();
//                            MyBallance.setText(getResources().getString(R.string.rupease) + " " + userBalanceD);
                        }
                        loadDlg.dismiss();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hash", "1234");
                    return map;
                }

            };

            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    public void getReset() {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("GET USER PARAMS", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_GET_USER_RESET, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.i("GET USER RESPONSE", response.toString());
                        String messagess = response.getString("message");
                        String code = response.getString("code");

                        if (code != null && code.equals("S00")) {
                            loadDlg.dismiss();

                            startActivity(new Intent(getActivity(), MainActivity.class));
                            getActivity().finish();

//                            successDialogs(messagess);
//                            Toast.makeText(getActivity(), messages, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hash", "1234");
                    return map;
                }

            };

            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }


    private void attemptLoad() {

        successDialogs(Messages());


    }


    public void successDialog(String message) {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(message));
        builder.setPositiveButton(getResources().getString(R.string.Accept), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                getReset();


            }
        });
        builder.setNegativeButton(getResources().getString(R.string.Decline), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public String generateVerifyMessage() {
        String source = "<b><font color=#000000>" + getResources().getString(R.string.Your_four_digit_PIN_will_be_sent_to_your_registered_Email_id) + "</font></b>" +
                "<br><br><b><font color=#ff0000>" + getResources().getString(R.string.Accept_Cont) + "</font></b><br></br>";
        return source;
    }


    public void successDialogs(String Messages) {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(Messages));
        builder.setPositiveButton(getResources().getString(R.string.Accept), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

                Intent shoppingIntent = new Intent(getActivity(), LoadMoneyActivity.class);
                shoppingIntent.putExtra("AutoFill", "no");
                shoppingIntent.putExtra("splitAmount", "0");

                startActivity(shoppingIntent);


            }
        });
        builder.setNegativeButton(getResources().getString(R.string.Decline), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.show();
    }


    public void successActivateDialog(String message) {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(message));
        builder.setPositiveButton(getResources().getString(R.string.Accept), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();


                ConformActivateDialog(ConformActivateMessages());


            }
        });
        builder.setNegativeButton(getResources().getString(R.string.Decline), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

                Intent mainIntents = new Intent(getActivity(), MainActivity.class);
                getActivity().finish();
                startActivity(mainIntents);


            }
        });

        builder.show();
    }

    public void ConformActivateDialog(String message) {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(message));
        builder.setPositiveButton(getResources().getString(R.string.Proceed), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

                activateCard();


            }
        });


        builder.show();
    }


    public String ActivateMessages() {
        String source = "<b><font color=#000000>" + getResources().getString(R.string.Did_you_received_your_Physical_card) + "</font></b>" + "</b></font><br>" +
                "<br><br><b><font color=#ff0000>" + getResources().getString(R.string.Continue_to_proceed) + "</font></b><br></br>";
        return source;
    }

    public String ConformActivateMessages() {
        String source = "<br><br><b><font color=#ff0000>" + getResources().getString(R.string.Proceed_to_activate_your_Physical_card) + "</font></b><br></br>";

        return source;
    }

    public String Messages() {
        String source = "<b><font color=#000000>" + getResources().getString(R.string.Insufficient_balance_in_virtual_card_You_must_have_minimum_balance_of) + "</font></b>" + "<font><b>" + session.getUserminimumCardBalance() + "</b></font><br>" +
                "<br><br><b><font color=#ff0000>" + getResources().getString(R.string.Please_load_money) + "</font></b><br></br>";
        return source;
    }

    private void logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        Crashlytics.setUserIdentifier(session.getUserMobileNo());
        Crashlytics.setUserEmail(session.getUserEmail());
        Crashlytics.setUserName(session.getUserFirstName() + " " + session.getUserLastName());
    }

    private void blockVirtualCard() {
        loadDlg.show();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("sessionId", session.getUserSessionId());
            jsonObject.put("requestType", "suspend");
            jsonObject.put("cardType", "physicalCard");
        } catch (Exception e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, URL_BLOCK_VIRTUAL_CARD, jsonObject,

                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("blockPhysicalCard", response.toString());
                        Log.d("blockPhysicalCard", URL_BLOCK_VIRTUAL_CARD);
                        try {
                            if (response.get("code").toString().equalsIgnoreCase("s00")) {
                                loadDlg.dismiss();
//                                Picasso.with(getActivity()).load("gkdhfg").resize(300, 100).placeholder(R.drawable.ic_inactivcards).into(ivpoints);

//                                cardStatus.setText("BLOCKED");
//                                ivCopy.setVisibility(View.GONE);
                                tvCardnumber.setVisibility(View.GONE);
                                tvCardholder.setVisibility(View.GONE);
                                tvdate.setVisibility(View.GONE);
                                tvcvv.setVisibility(View.GONE);

                                llcard.setBackgroundResource(R.drawable.debitblock);


                                List<UserModel> currentUserList = Select.from(UserModel.class).list();
                                UserModel currentUser = currentUserList.get(0);
                                currentUser.setPCStatus(true);
                                currentUser.setUserPhysicalBlock("Inactive");
                                session.setUserPhysicalBlock("Inactive");
                                session.setPCStatus(true);
                                currentUser.save();
                                session.save();

                                llUnBlock.setVisibility(View.VISIBLE);
                                llBlock.setVisibility(View.GONE);

                                loadDlg.dismiss();
                                sendRefresh();

//                                llpoints.setVisibility(View.GONE);
//                                llunblock.setVisibility(View.VISIBLE);

//                                Toast.makeText(getApplicationContext(), response.get("message").toString(), Toast.LENGTH_SHORT).show();
                            } else {
//                                Toast.makeText(getApplicationContext(), response.get("message").toString(), Toast.LENGTH_SHORT).show();
                                loadDlg.dismiss();

                                llUnBlock.setVisibility(View.GONE);
                                llBlock.setVisibility(View.VISIBLE);
                            }
                        } catch (Exception e) {
                            loadDlg.dismiss();
                            llUnBlock.setVisibility(View.GONE);
                            llBlock.setVisibility(View.VISIBLE);
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loadDlg.dismiss();
                        llUnBlock.setVisibility(View.GONE);
                        llBlock.setVisibility(View.VISIBLE);
                        Log.d("VirtualCard", "Inside volleyError");
                    }

                }

        );

        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonObjectRequest.setRetryPolicy(policy);
        PayQwikApplication.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    private void activateVirtualCard() {
        loadDlg.show();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("sessionId", session.getUserSessionId());
            jsonObject.put("cardType", "physicalCard");
//            jsonObject.put("requestType","suspend");
        } catch (Exception e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, URL_REACTIVATE_CARD
                , jsonObject,

                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("activateVirtualCard", response.toString());
                        Log.d("activateVirtualCardURL", URL_REACTIVATE_CARD);

                        try {
                            if (response.get("code").toString().equalsIgnoreCase("s00")) {

//                                applyforcard.setVisibility(View.GONE);
                                tvCardnumber.setVisibility(View.GONE);
                                tvCardholder.setVisibility(View.GONE);
                                tvdate.setVisibility(View.GONE);
                                tvcvv.setVisibility(View.GONE);
//                                tvhidecvv.setVisibility(View.GONE);
//                                btnCopy.setVisibility(View.VISIBLE);
//                                ivCopy.setVisibility(View.GONE);
//                                llpoints.setVisibility(View.VISIBLE);
//                                llunblock.setVisibility(View.GONE);
//                                loadDlg.dismiss();
                                llcard.setBackgroundResource(R.drawable.debitcard);

                                llUnBlock.setVisibility(View.GONE);
                                llBlock.setVisibility(View.VISIBLE);
                                loadDlg.dismiss();


                                List<UserModel> currentUserList = Select.from(UserModel.class).list();
                                UserModel currentUser = currentUserList.get(0);
                                currentUser.setPCStatus(false);
                                currentUser.setUserPhysicalBlock("Active");
                                session.setUserPhysicalBlock("Active");
                                session.setPCStatus(false);
                                currentUser.save();
                                session.save();

                                loadDlg.dismiss();
//                                sendRefresh();

                                physicalCardFetch();
                                Toast.makeText(getContext(), response.get("message").toString(), Toast.LENGTH_SHORT).show();
                            } else {
                                loadDlg.dismiss();
                                llUnBlock.setVisibility(View.VISIBLE);
                                llBlock.setVisibility(View.GONE);
                                Toast.makeText(getContext(), response.get("message").toString(), Toast.LENGTH_SHORT).show();

                            }
                        } catch (Exception e) {
                            loadDlg.dismiss();
                            llUnBlock.setVisibility(View.VISIBLE);
                            llBlock.setVisibility(View.GONE);
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loadDlg.dismiss();
                        llUnBlock.setVisibility(View.VISIBLE);
                        llBlock.setVisibility(View.GONE);
                        Log.d("VirtualCard", "Inside volleyError");
                    }

                }

        );

        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonObjectRequest.setRetryPolicy(policy);
        PayQwikApplication.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    public void requestDialog(String messagess) {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(messagess));
        builder.setPositiveButton(getResources().getString(R.string.Accept), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                Intent Promocode = new Intent(getActivity(), EkycActivity.class);
                startActivity(Promocode);
                getActivity().finish();


            }
        });

        builder.show();
    }

    public void successDialogss(String message) {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(message));
        builder.setPositiveButton(getResources().getString(R.string.Accept), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                Intent Promocode = new Intent(getActivity(), EkycActivity.class);
                startActivity(Promocode);
                getActivity().finish();


            }
        });


        builder.show();
    }

    public String genraterequest() {
        String source = "<b><font color=#000000>" + getResources().getString(R.string.Kyc_verification) + "</font></b>" +
                "<br><br><b><font color=#ff0000>" + getResources().getString(R.string.Accept_Cont) + "</font></b><br></br>";
        return source;
    }

    public String generateVerifyMessagess() {
        String source = "<b><font color=#000000>" + getResources().getString(R.string.Kyc_Information) + "</font></b>" +
                "<br><br><b><font color=#ff0000>" + getResources().getString(R.string.Accept_Cont) + "</font></b><br></br>";
        return source;
    }

    public String genraterequestKyc() {
        String source = "<b><font color=#000000>" + "Please upgrade your KYC with OSV to apply for physical card or contact customer care at" + "</font></b>" +
                "<br><br><b><font color=#3752a4>" + "info@ewiresofttech.com" + "</font></b><br></br>" +
                "<br><br><b><font color=#ff0000>" + getResources().getString(R.string.Accept_Cont) + "</font></b><br></br>";
        return source;
    }

}