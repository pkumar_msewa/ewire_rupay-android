package in.MadMoveGlobal.fragment.fragmentnaviagtionitems;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.ArrayList;

import in.MadMoveGlobal.adapter.ReceiptAdapter;
import in.MadMoveGlobal.custom.CustomAlertDialog;
import in.MadMoveGlobal.metadata.AppMetadata;
import in.MadMoveGlobal.model.StatementModel;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.EwireRuPay.R;

/**
 * Created by Dushant on 08/27/2017.
 */
public class ReceiptVFragment extends Fragment {
    private View rootView;
    private ReceiptAdapter receiptAdapter;
    private ListView lvReceipt;
    private UserModel session = UserModel.getInstance();
    private JSONObject jsonRequest;
    private ArrayList<StatementModel> receiptList;
    private ProgressBar pbReceipt;
    private LinearLayout llNoReceipt;
    private TextView tvNoReceipt;
    //Volley
    private String tag_json_obj = "json_receipt";
    private JsonObjectRequest postReq;

    //Pagination
    private int currentPage = 0;
    private int totalPage = 0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_receipt, container, false);
        lvReceipt = (ListView) rootView.findViewById(R.id.lvReceipt);
        pbReceipt = (ProgressBar) rootView.findViewById(R.id.pbReceipt);
        tvNoReceipt = (TextView) rootView.findViewById(R.id.tvNoReceipt);
        llNoReceipt = (LinearLayout) rootView.findViewById(R.id.llNoReceipt);
        llNoReceipt.setVisibility(View.GONE);
        receiptList = getArguments().getParcelableArrayList("data");
        if (receiptList != null && receiptList.size() != 0) {
            receiptAdapter = new ReceiptAdapter(getActivity(), receiptList);
            lvReceipt.setAdapter(receiptAdapter);
            pbReceipt.setVisibility(View.GONE);
            lvReceipt.setVisibility(View.VISIBLE);
            llNoReceipt.setVisibility(View.GONE);
        }
        return rootView;
    }


//    public void loadUserStatement() {
//        pbReceipt.setVisibility(View.VISIBLE);
//        lvReceipt.setVisibility(View.GONE);
//        jsonRequest = new JSONObject();
//        try {
//
//            jsonRequest.put("sessionId", session.getUserSessionId());
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//            jsonRequest = null;
//        }
//
//        if (jsonRequest != null) {
//            Log.i("JsonRequest", jsonRequest.toString());
//            Log.i("Receipt Url", ApiUrl.URL_RECEIPT);
//            postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_RECEIPT, jsonRequest, new Response.Listener<JSONObject>() {
//                @Override
//                public void onResponse(JSONObject JsonObj) {
//                    try {
//
//                        Log.i("Receipts Response", JsonObj.toString());
//                        String message = JsonObj.getString("message");
//                        String code = JsonObj.getString("code");
//
//                        if (code != null && code.equals("S00")) {
//                            sendRefresh();
//
//                            String vttrans = JsonObj.getString("virtualTransactions");
//                            JSONObject jsonDetails = new JSONObject(vttrans);
//
////                            if (totalElements == 0) {
//                            llNoReceipt.setVisibility(View.VISIBLE);
//                            pbReceipt.setVisibility(View.GONE);
//                            lvReceipt.setVisibility(View.GONE);
//
//
//                            JSONArray operatorArray = jsonDetails.getJSONArray("transactions");
//
//                            for (int i = 0; i < operatorArray.length(); i++) {
//                                JSONObject c = operatorArray.getJSONObject(i);
//                                String indicator = c.getString("indicator");
//                                String date = c.getString("date");
//                                String amount = c.getString("amount");
//                                String description = c.getString("description");
//                                String status = c.getString("status");
//
//                                receiptAdapter = new ReceiptAdapter(getActivity(), receiptList);
//                                lvReceipt.setAdapter(receiptAdapter);
//                                justifyListViewHeightBasedOnChildren(lvReceipt);
//                                pbReceipt.setVisibility(View.GONE);
//                                lvReceipt.setVisibility(View.VISIBLE);
//                                llNoReceipt.setVisibility(View.GONE);
//
//
//                                StatementModel statementModel = new StatementModel(indicator, date, amount, description, status, "", "");
//                                receiptList.add(statementModel);
//
//
//                            }
//
//                        }
//                        if (receiptList != null && receiptList.size() != 0) {
//                            receiptAdapter = new ReceiptAdapter(getActivity(), receiptList);
//                            lvReceipt.setAdapter(receiptAdapter);
//                            justifyListViewHeightBasedOnChildren(lvReceipt);
//                            pbReceipt.setVisibility(View.GONE);
//                            lvReceipt.setVisibility(View.VISIBLE);
//                            llNoReceipt.setVisibility(View.GONE);
//
//
//                        } else if (code != null && code.equals("F03")) {
//                            pbReceipt.setVisibility(View.GONE);
//                            lvReceipt.setVisibility(View.GONE);
//                            showInvalidSessionDialog();
//                        } else {
//                            pbReceipt.setVisibility(View.GONE);
//                            lvReceipt.setVisibility(View.GONE);
////                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
//                        }
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                        pbReceipt.setVisibility(View.GONE);
//                        lvReceipt.setVisibility(View.GONE);
//                        CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
//                    }
//                }
//            }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    llNoReceipt.setVisibility(View.VISIBLE);
//                    pbReceipt.setVisibility(View.GONE);
//                    lvReceipt.setVisibility(View.GONE);
//                    try {
//                        tvNoReceipt.setText("No Transations Found");
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//            }) {
//                @Override
//                public Map<String, String> getHeaders() throws AuthFailureError {
//                    HashMap<String, String> map = new HashMap<>();
//                    map.put("hash", "1234");
//                    return map;
//                }
//
//            };
//            int socketTimeout = 60000;
//            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//            postReq.setRetryPolicy(policy);
//            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
//        }
//
//    }
//
//    public void loadMoreUserStatement() {
//        currentPage = currentPage + 1;
//        jsonRequest = new JSONObject();
//        try {
//            jsonRequest.put("sessionId", session.getUserSessionId());
//
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//            jsonRequest = null;
//        }
//
//        if (jsonRequest != null) {
//            Log.i("JsonRequest", jsonRequest.toString());
//            Log.i("Receipt Url", ApiUrl.URL_RECEIPT);
//            postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_RECEIPT, jsonRequest, new Response.Listener<JSONObject>() {
//                @Override
//                public void onResponse(JSONObject JsonObj) {
//                    try {
//
//                        Log.i("Receipts Response", JsonObj.toString());
//                        String message = JsonObj.getString("message");
//                        String code = JsonObj.getString("code");
//
//                        if (code != null && code.equals("S00")) {
//                            sendRefresh();
//
//                            String details = JsonObj.getString("details");
//                            JSONObject jsonDetails = new JSONObject(details);
////                            if (totalElements == 0) {
//                            llNoReceipt.setVisibility(View.VISIBLE);
////                                pbReceipt.setVisibility(View.GONE);
////                                lvReceipt.setVisibility(View.GONE);
//
//
//                            JSONArray operatorArray = jsonDetails.getJSONArray("transactions");
//
//                            for (int i = 0; i < operatorArray.length(); i++) {
//                                JSONObject c = operatorArray.getJSONObject(i);
//                                String indicator = c.getString("indicator");
//                                String date = c.getString("date");
//                                String amount = c.getString("amount");
//                                String description = c.getString("description");
//                                String status = c.getString("status");
//
//
//                                String refNo = "";
//
//
//                                String authNo = "";
//
//
//                                if (!refNo.equalsIgnoreCase("")) {
//
//                                    StatementModel statementModel = new StatementModel(indicator, date, amount, description, status, refNo, authNo);
//                                    receiptList.add(statementModel);
//                                }
//
////                                        }
//                            }
//
//
//                        } else if (code != null && code.equals("F03")) {
//                            showInvalidSessionDialog();
//                        } else {
////                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
//                        }
//                        lvReceipt.onLoadMoreComplete();
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                        lvReceipt.onLoadMoreComplete();
//                        CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
//                    }
//                }
//            }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    error.printStackTrace();
//                    CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
//                    lvReceipt.onLoadMoreComplete();
//                }
//            }) {
//                @Override
//                public Map<String, String> getHeaders() throws AuthFailureError {
//                    HashMap<String, String> map = new HashMap<>();
//                    map.put("hash", "1234");
//                    return map;
//                }
//
//            };
//            int socketTimeout = 60000;
//            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//            postReq.setRetryPolicy(policy);
//            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
//        }
//
//    }

    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    private void sendRefresh() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "1");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    @Override
    public void onDetach() {
        postReq.cancel();
        super.onDetach();
    }
}
