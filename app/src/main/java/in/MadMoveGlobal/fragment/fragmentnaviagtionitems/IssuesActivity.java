package in.MadMoveGlobal.fragment.fragmentnaviagtionitems;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;

import in.MadMoveGlobal.fragment.fragmentissues.AllissueFragment;
import in.MadMoveGlobal.fragment.fragmentissues.OpenissueFragment;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.EwireRuPay.R;
import io.fabric.sdk.android.Fabric;



/*
import in.payqwik.fragment.fragmentissues.AllissueFragment;
import in.payqwik.fragment.fragmentissues.CloseissueFragment;
import in.payqwik.fragment.fragmentissues.OpenissueFragment;
import in.payqwik.test.R;
*/

/**
 * Created by Dushant on 08/27/2017.
 */

public class IssuesActivity extends AppCompatActivity {


    private TabLayout mSlidingTabLayout;
    private ViewPager mainPager;
    public TextView MyBallance;
    private UserModel session = UserModel.getInstance();
    private FragmentManager fragmentManager;
    CharSequence TitlesEnglish[] = {"All", "Open"};
    int NumbOfTabs = 2;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        // TODO: Move this to where you establish a user session
        logUser();

        fragmentManager = getSupportFragmentManager();
        setContentView(R.layout.activity_tab_layout);
        mainPager = (ViewPager) findViewById(R.id.mainPager);
        mSlidingTabLayout = (TabLayout) findViewById(R.id.sliding_tabs_home);
        mainPager.setAdapter(new ViewPagerAdapter(fragmentManager, TitlesEnglish, NumbOfTabs));
        mSlidingTabLayout.setupWithViewPager(mainPager);
        LocalBroadcastManager.getInstance(this).registerReceiver(mSignUpReceiver, new IntentFilter("sign-up"));

        Toolbar issuetoolbar = (Toolbar) findViewById(R.id.toolbars);
        setSupportActionBar(issuetoolbar);
        ImageButton issueIb = (ImageButton) findViewById(R.id.ivBackBtns);
        issueIb.setVisibility(View.VISIBLE);
        issueIb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private BroadcastReceiver mSignUpReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            mainPager.setCurrentItem(1);

        }
    };

    public class ViewPagerAdapter extends FragmentPagerAdapter {
        CharSequence Titles[]; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
        int NumbOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created


        // Build a Constructor and assign the passed Values to appropriate values in the class
        public ViewPagerAdapter(FragmentManager fm, CharSequence mTitles[], int mNumbOfTabsumb) {
            super(fm);
            this.Titles = mTitles;
            this.NumbOfTabs = mNumbOfTabsumb;

        }

        @Override
        public Fragment getItem(int position) {

            if (position == 0) {
                return new AllissueFragment();
            } else if (position == 1) {
                return new OpenissueFragment();
            } else {
                OpenissueFragment closeissueFragment = new OpenissueFragment();
                return closeissueFragment;
            }

        }

        @Override
        public CharSequence getPageTitle(int position) {
            return Titles[position];
        }

        @Override
        public int getCount() {
            return TitlesEnglish.length;
        }
    }
    private void logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        Crashlytics.setUserIdentifier(session.getUserMobileNo());
        Crashlytics.setUserEmail(session.getUserEmail());
        Crashlytics.setUserName(session.getUserFirstName() + " " + session.getUserLastName());
    }
}