package in.MadMoveGlobal.fragment.fragmentnaviagtionitems;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.crashlytics.android.Crashlytics;
import com.orm.query.Select;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.EwireRuPay.activity.MainActivity;
import io.fabric.sdk.android.Fabric;
import me.philio.pinentry.PinEntryView;

//import in.payqwik.util.EncryptDecryptUserUtility;


/**
 * Created by Ksf on 3/27/2016.
 */
public class physicalCardOTPFragment extends Fragment {

    private View rootView;
    private PinEntryView etVerifyOTP;
    private Button btnVerify;
    private LoadingDialog loadDlg;
    private String userMobileNo;
    private String otpCode = null;
    private Button btnVerifyResend;
    private JSONObject jsonRequest;
    private TextView tvTimer;
    private ImageView ivTimer;
    private Boolean fullyFilled;
    private UserModel session = UserModel.getInstance();


    //Volley Tag
    private String tag_json_obj = "json_user";


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Fabric.with(getActivity(), new Crashlytics());
        // TODO: Move this to where you establish a user session
        logUser();

        rootView = inflater.inflate(R.layout.fragment_phyotp_page, container, false);
        Bundle b = getArguments();
        loadDlg = new LoadingDialog(getActivity());
//        userMobileNo = b.getString("mobileNumber");
        btnVerifyResend = (Button) rootView.findViewById(R.id.btnVerifyResend);
        btnVerify = (Button) rootView.findViewById(R.id.btnVerify);
        etVerifyOTP = (PinEntryView) rootView.findViewById(R.id.etcreatePin);
        ivTimer = (ImageView) rootView.findViewById(R.id.ivTimer);
        getActivity().registerReceiver(broadcastReceiver, new IntentFilter("broadCastName"));
        tvTimer = (TextView) rootView.findViewById(R.id.tvTimer);
        Picasso.with(getActivity()).load("fsdfs").placeholder(R.drawable.circlegradient).into(ivTimer);

        reverseTimer(120, tvTimer);
        etVerifyOTP.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 6) {
                    if (!validateOtp()) {
                        return;
                    }
                    loadDlg.show();
                    verifyOTP();

                }

            }
        });


        if (otpCode != null && !otpCode.isEmpty()) {
            etVerifyOTP.setText(otpCode);
            loadDlg.show();
            verifyOTP();
        }


        btnVerify.setTextColor(Color.parseColor("#ffffff"));


        btnVerifyResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadDlg.show();
                resendOTP();
            }
        });


        btnVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadDlg.show();
                verifyOTP();
            }
        });
        return rootView;
    }

    private void resendOTP() {
        btnVerifyResend.setVisibility(View.GONE);
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_RESEND_ACTIVATION_CODE, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObj) {
                    try {
                        String code = jsonObj.getString("code");
                        String detail = jsonObj.getString("details");
                        if (code != null & code.equals("S00")) {
                            loadDlg.dismiss();
                            reverseTimer(120, tvTimer);
                            if (!etVerifyOTP.getText().toString().isEmpty()) {
                                etVerifyOTP.setText("");
                            }
                            Toast.makeText(getActivity(), detail, Toast.LENGTH_SHORT).show();
                        } else {
                            loadDlg.dismiss();


                        }
                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));

                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }


    private void verifyOTP() {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("activationCode", etVerifyOTP.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_ACTIVATE_PHYSICAL_CARD, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        String level = response.getString("status");
                        String message = response.getString("message");
                        String code = response.getString("code");

                        if (code != null & code.equals("S00")) {
                            loadDlg.dismiss();
                            sendRefresh();

                            List<UserModel> currentUserList = Select.from(UserModel.class).list();
                            UserModel currentUser = currentUserList.get(0);
                            currentUser.setHasPcard(true);
                            session.setHasPcard(true);
                            currentUser.save();


                            Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                            Intent mainIntent = new Intent(getActivity(), MainActivity.class);
                            startActivity(mainIntent);
                            getActivity().finish();

                        } else {
                            loadDlg.dismiss();
                            Toast.makeText(getActivity(), "Invalid OTP", Toast.LENGTH_SHORT).show();


                        }
                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(getContext(), getResources().getString(R.string.server_exception2));
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    CustomToast.showMessage(getContext(), getResources().getString(R.string.server_exception));
                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }


    public String generateMessage() {
        String source = "<b><font color=#000000> Verified Successfully.</font></b>";
        return source;
    }

    private boolean validateOtp() {
        if (etVerifyOTP.getText().toString().trim().isEmpty() || etVerifyOTP.getText().toString().trim().length() < 6) {
            requestFocus(etVerifyOTP);
            return false;
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    public void reverseTimer(int Seconds, final TextView tv) {

        new CountDownTimer(Seconds * 1000 + 1000, 1000) {

            public void onTick(long millisUntilFinished) {
                int seconds = (int) (millisUntilFinished / 1000);
                int minutes = seconds / 60;
                seconds = seconds % 60;
//                if (seconds == 0) {
//                    resendOTP();
//                }
                tv.setText(String.format("%02d", minutes)
                        + ":" + String.format("%02d", seconds));
            }

            public void onFinish() {
                tv.setText("00:00");
                btnVerifyResend.setVisibility(View.VISIBLE);
            }
        }.start();
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle b = intent.getExtras();
            if (b != null) {
                String message = b.getString("message");
                Log.i("BroadCast", message);
                message = message.replaceAll("\\.", "");
                Log.i("BroadCast", message);
                getActivity().unregisterReceiver(broadcastReceiver);
                etVerifyOTP.setText(message);


            }

        }
    };

    private void sendRefresh() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "1");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }
    private void logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        Crashlytics.setUserIdentifier(session.getUserMobileNo());
        Crashlytics.setUserEmail(session.getUserEmail());
        Crashlytics.setUserName(session.getUserFirstName() + " " + session.getUserLastName());
    }
}
