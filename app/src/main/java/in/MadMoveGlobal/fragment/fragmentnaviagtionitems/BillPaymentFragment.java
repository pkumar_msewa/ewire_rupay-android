package in.MadMoveGlobal.fragment.fragmentnaviagtionitems;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import in.MadMoveGlobal.fragment.fragmentmobilerecharge.PostpaidFragment;
import in.MadMoveGlobal.fragment.fragmentpaybills.GasFragment;
import in.MadMoveGlobal.fragment.fragmentpaybills.LandlineFragment;
import in.MadMoveGlobal.metadata.AppMetadata;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;

/**
 * Created by Dushant on 08/27/2017.
 */
public class BillPaymentFragment extends Fragment {

    CharSequence TitlesEnglish[] = null;

    /*
     * Sliding tabs Setup
     */
    int NumbOfTabs = 3;
    private View rootView;
    private TabLayout mSlidingTabLayout;
    private ViewPager mainPager;
    private FragmentManager fragmentManager;
    private String navType = "";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TitlesEnglish = new CharSequence[]{getString(R.string.topup_postpaid), getResources().getString(R.string.bill_landline),getResources().getString(R.string.bill_gas)};
        Bundle navBundle = getArguments();
        onDetach();
        if (navBundle != null) {
            navType = navBundle.getString(AppMetadata.FRAGMENT_TYPE);
        }
        fragmentManager = getActivity().getSupportFragmentManager();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_scroll_tab, container, false);
        mainPager = (ViewPager) rootView.findViewById(R.id.mainPager);
        mSlidingTabLayout = (TabLayout) rootView.findViewById(R.id.tabLayoutMobileTopUp);
        FragmentManager fragmentManager = getChildFragmentManager();
        mainPager.setAdapter(new ViewPagerAdapter(fragmentManager, TitlesEnglish, NumbOfTabs));
        mSlidingTabLayout.setupWithViewPager(mainPager);

        mSlidingTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mainPager.setCurrentItem(tab.getPosition());


//                if (tab.getPosition() == 0) {

                LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(getActivity());
                Intent i = new Intent("TAG_REFRESH");
                lbm.sendBroadcast(i);

//                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        onDetach();
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        PayQwikApplication.getInstance().trackScreenView("Bill Payment");
    }

    public class ViewPagerAdapter extends FragmentStatePagerAdapter {

        CharSequence Titles[]; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
        int NumbOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created


        // Build a Constructor and assign the passed Values to appropriate values in the class
        public ViewPagerAdapter(FragmentManager fm, CharSequence mTitles[], int mNumbOfTabsumb) {
            super(fm);
            this.Titles = mTitles;
            this.NumbOfTabs = mNumbOfTabsumb;

        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                PostpaidFragment tab1 = new PostpaidFragment();
                onDetach();
                return tab1;
            } else if (position == 1) {
                LandlineFragment tab2 = new LandlineFragment();
                onDetach();
                return tab2;
            }
//            else if (position == ) {
//                ElectricityFragment tab2 = new ElectricityFragment();
//                onDetach();
//                return tab2;
//            } else if (position == 4) {
//                InsuranceFragment tab2 = new InsuranceFragment();
//                onDetach();
//                return tab2;
//            }
            else {
                GasFragment tab3 = new GasFragment();
                onDetach();
                return tab3;
            }


        }

        @Override
        public CharSequence getPageTitle(int position) {
            return Titles[position];
        }

        @Override
        public int getCount() {
            return NumbOfTabs;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        //hide keyboard when any fragment of this class has been detached
        showSoftwareKeyboard(false);
    }

    protected void showSoftwareKeyboard(boolean showKeyboard) {
        final Activity activity = getActivity();
        final InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);

//        inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), showKeyboard ? InputMethodManager.SHOW_FORCED : InputMethodManager.HIDE_NOT_ALWAYS);
    }
}
