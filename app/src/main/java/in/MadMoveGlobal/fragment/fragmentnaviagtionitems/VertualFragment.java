package in.MadMoveGlobal.fragment.fragmentnaviagtionitems;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;

import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.SwitchCompat;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.orm.query.Select;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.MadMoveGlobal.custom.CustomAlertDialog;
import in.MadMoveGlobal.custom.LoadingDialog;

import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.metadata.AppMetadata;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;

import android.text.ClipboardManager;


import static in.MadMoveGlobal.metadata.ApiUrl.URL_BLOCK_VIRTUAL_CARD;
import static in.MadMoveGlobal.metadata.ApiUrl.URL_REACTIVATE_CARD;


/**
 * Created by Dushant on 01/01/2018.
 */

public class VertualFragment extends Fragment {


    private LoadingDialog loadDlg;
    private LinearLayout llcard, llheaders, llCopy, llBlock, llUnBlock, llcardinacive, llIcon;
    private TextView tvCardnumber, tvCardholder, tvdate, tvcvv, tvcvvhide, tvtext, tvinitial, tvinitialunHide;
    private View rootView;
    //    private ImageView addCard;
    private FrameLayout flmain;
    //    private FloatingActionButton addCard;
    private UserModel session = UserModel.getInstance();
    private ProgressBar pbcard;
    //Volley Tag
    private String tag_json_obj = "json_user";
    private JSONObject jsonRequest;
    private JsonObjectRequest postReq;
    private String cardNo, expiryDate, cvv, accountHolder;
    private SwitchCompat scheck;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadDlg = new LoadingDialog(getActivity());

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_vertual_card, container, false);

        llcard = (LinearLayout) rootView.findViewById(R.id.llcard);
        llheaders = (LinearLayout) rootView.findViewById(R.id.llheaders);
        llCopy = (LinearLayout) rootView.findViewById(R.id.llCopy);
        llBlock = (LinearLayout) rootView.findViewById(R.id.llBlock);
        llIcon = (LinearLayout) rootView.findViewById(R.id.llIcon);
        llUnBlock = (LinearLayout) rootView.findViewById(R.id.llUnBlock);
        llcardinacive = (LinearLayout) rootView.findViewById(R.id.llcardinacive);
        flmain = (FrameLayout) rootView.findViewById(R.id.flmain);
        tvCardnumber = (TextView) rootView.findViewById(R.id.tvCardnumber);
        tvCardholder = (TextView) rootView.findViewById(R.id.tvCardholder);
        tvdate = (TextView) rootView.findViewById(R.id.tvdate);
        tvcvv = (TextView) rootView.findViewById(R.id.tvcvv);
        tvinitial = (TextView) rootView.findViewById(R.id.tvinitial);
        tvinitialunHide = (TextView) rootView.findViewById(R.id.tvinitialunHide);
        tvcvv = (TextView) rootView.findViewById(R.id.tvcvv);
        tvcvvhide = (TextView) rootView.findViewById(R.id.tvcvvhide);
        scheck = (SwitchCompat) rootView.findViewById(R.id.swinitials);
//        addCard = (ImageView) rootView.findViewById(R.id.ivAdd);

//        addCard = (FloatingActionButton) rootView.findViewById(R.id.ivAdd);
        pbcard = (ProgressBar) rootView.findViewById(R.id.pbcard);
        Log.i("VALUE", String.valueOf(!session.isHasVcard()));
//        tvtext = (TextView) rootView.findViewById(R.id.tvtext);
        scheck.getTrackDrawable().setColorFilter(ContextCompat.getColor(getActivity(), R.color.black), PorterDuff.Mode.SRC_IN);


        if (!session.isHasVcard())

            llheaders.setBackgroundResource(R.drawable.debitcard);

        else {



            if (session.isHasVcard() && session.isVCStatus()) {

                llheaders.setBackgroundResource(R.drawable.debitblock);
                llUnBlock.setVisibility(View.VISIBLE);
                scheck.setClickable(false);
                llBlock.setVisibility(View.GONE);
            } else

                cardFetch();
        }

        llBlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                blockVirtualCard();
                llBlock.setVisibility(View.GONE);
                llUnBlock.setVisibility(View.VISIBLE);
            }
        });

        llUnBlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//        Intent menuIntent = new Intent(getActivity(), MainMenuDetailActivity.class);
//        menuIntent.putExtra(AppMetadata.FRAGMENT_TYPE, "Points");
//        startActivity(menuIntent);
                activateVirtualCard();
                llUnBlock.setVisibility(View.GONE);
                llBlock.setVisibility(View.VISIBLE);


            }
        });

        return rootView;
    }

    public String formateDateFromstring(String time) {
        String inputPattern = "yyyy-MM";
        String outputPattern = "MM/yy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        java.util.Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }


    public void cardFetch() {

//        loadDlg = new LoadingDialog(getActivity());
//        loadDlg.show();
        llheaders.setBackgroundResource(R.drawable.debitcard);
        pbcard.setVisibility(View.VISIBLE);
        jsonRequest = new JSONObject();
        try {

            jsonRequest.put("sessionId", session.getUserSessionId());


        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }
//
        if (jsonRequest != null) {
            Log.i("JsonRequestVITRUAl", jsonRequest.toString());
            Log.i("Card Url", ApiUrl.URL_MATCHMOVE_CARDDETAILSFETCH);
            postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_MATCHMOVE_CARDDETAILSFETCH, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject JsonObj) {
                    try {
                        Log.i("Card Response", JsonObj.toString());
                        String message = JsonObj.getString("message");
                        String code = JsonObj.getString("code");

                        if (code != null && code.equals("S00")) {
                            llCopy.setClickable(true);
                            scheck.setClickable(true);
                            cardNo = JsonObj.getString("walletNumber");
                            expiryDate = JsonObj.getString("expiryDate");
                            cvv = JsonObj.getString("cvv");
                            accountHolder = JsonObj.getString("holderName");


                            tvCardholder.setText(accountHolder);
                            tvCardnumber.setText(cardNo.replaceAll("\\D", "").replaceAll("(\\d{4}(?!$))", "$1 "));
                            try {
//                                Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "OCR-a___.ttf");
                                Typeface tf = Typeface.createFromAsset(getActivity().getResources().getAssets(), "OCR-a___.ttf");

                                tvdate.setTypeface(tf);
                                tvCardholder.setTypeface(tf);
                                tvCardnumber.setTypeface(tf);
                                tvcvv.setTypeface(tf);
                                tvcvvhide.setTypeface(tf);


                            } catch (NullPointerException e) {

                            }


                            tvCardholder.setVisibility(View.VISIBLE);
                            tvCardnumber.setVisibility(View.VISIBLE);
//                            ivCopy.setVisibility(View.VISIBLE);
                            tvdate.setVisibility(View.VISIBLE);
                            tvcvv.setVisibility(View.GONE);
                            tvcvvhide.setVisibility(View.VISIBLE);
//                            tvfedral.setVisibility(View.VISIBLE);

//                            llcard.setBackgroundResource(R.drawable.debitcard);
                            llheaders.setBackgroundResource(R.drawable.debitcard);
                            llcard.setVisibility(View.VISIBLE);

                            String date_before = expiryDate.toLowerCase();
                            String date_after = formateDateFromstring(date_before);
                            tvdate.setText("Valid Thru  " + expiryDate);
                            tvcvv.setText("cvv  " + cvv);
                            tvcvvhide.setText("CVV  XXX");

//                            Log.i("QWERTY", String.valueOf(session.isHasVcard()) + "    " + String.valueOf(session.isHasPcard()) + "     " + session.getUserPhysicalRequest());


                            if (session.isHasVcard() && session.isHasPcard() && session.getUserPhysicalRequest().equalsIgnoreCase("Active")) {

                                llheaders.setBackgroundResource(R.drawable.debit_inactive);
                                tvcvv.setText("cvv  " + cvv);
                                llIcon.setVisibility(View.GONE);

                            } else {

                                llheaders.setBackgroundResource(R.drawable.debitcard);
                            }


                            llCopy.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    ClipboardManager cm = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
                                    cm.setText(tvCardnumber.getText());
                                    Toast.makeText(getActivity(), getResources().getString(R.string.Card_Number_Copied_to_clipboard), Toast.LENGTH_SHORT).show();
                                }
                            });
                            pbcard.setVisibility(View.GONE);

                            scheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton compoundButton, boolean bChecked) {
                                    if (bChecked) {
                                        tvinitial.setVisibility(View.VISIBLE);
                                        tvinitialunHide.setVisibility(View.GONE);
                                        tvcvvhide.setVisibility(View.GONE);
                                        tvcvv.setVisibility(View.VISIBLE);

                                    } else {
                                        tvinitial.setVisibility(View.GONE);
                                        tvinitialunHide.setVisibility(View.VISIBLE);
                                        tvcvvhide.setVisibility(View.VISIBLE);
                                        tvcvv.setVisibility(View.GONE);
                                        tvcvvhide.setText("CVV XXX");
                                    }

                                }
                            });
//                            llcard.setOnLongClickListener(new View.OnLongClickListener() {
//
//                                @Override
//                                public boolean onLongClick(View v) {
//                                    ClipboardManager cm = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
//                                    cm.setText(tvCardnumber.getText());
//                                    Toast.makeText(getActivity(), "Copied to clipboard", Toast.LENGTH_SHORT).show();
//                                    return true;
//                                }
//                            });


                        } else if (code != null && code.equals("F03")) {
                            pbcard.setVisibility(View.GONE);
                            llcard.setVisibility(View.VISIBLE);
                            llheaders.setBackgroundResource(R.drawable.debitcard);
                            tvCardnumber.setVisibility(View.GONE);
//                            ivCopy.setVisibility(View.GONE);
                            tvdate.setVisibility(View.GONE);
                            tvcvv.setVisibility(View.GONE);
                            tvCardholder.setVisibility(View.GONE);
//                            CustomToast.showMessage(getActivity(), "Please login and try again");
                            Snackbar.make(llcard, getResources().getString(R.string.Please_login_and_try_again), Snackbar.LENGTH_LONG).show();
                            sendLogout();


                        } else {
                            pbcard.setVisibility(View.GONE);
                            llcard.setVisibility(View.VISIBLE);
                            tvCardnumber.setVisibility(View.GONE);
//                            ivCopy.setVisibility(View.GONE);
                            tvdate.setVisibility(View.GONE);
                            tvcvv.setVisibility(View.GONE);
                            tvCardholder.setVisibility(View.GONE);
                            llheaders.setBackgroundResource(R.drawable.debitcard);
                            llCopy.setClickable(false);
                            scheck.setClickable(false);
//                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        pbcard.setVisibility(View.GONE);
//                        CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
                        Snackbar.make(llcard, getResources().getString(R.string.server_exception2), Snackbar.LENGTH_LONG).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    pbcard.setVisibility(View.GONE);
                    llcard.setVisibility(View.VISIBLE);
                    llheaders.setBackgroundResource(R.drawable.debitcard);
                    pbcard.setVisibility(View.GONE);
//                    CustomToast.showMessage(getActivity(), "Network connection error");
                    Snackbar.make(llcard, getResources().getString(R.string.Network_connection_error), Snackbar.LENGTH_LONG).show();
                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;

                }
            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }

    }
//    public void cardFetch() {
//
//        llheaders.setBackgroundResource(R.drawable.debitcard);
//        pbcard.setVisibility(View.VISIBLE);
////        addCard.setVisibility(View.GONE);
//        jsonRequest = new JSONObject();
//        try {
//
//            jsonRequest.put("sessionId", session.getUserSessionId());
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//            jsonRequest = null;
//        }
//
//        if (jsonRequest != null) {
//            Log.i("JsonRequest", jsonRequest.toString());
//            Log.i("Card Url", ApiUrl.URL_MATCHMOVE_CARDDETAILSFETCH);
//            postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_MATCHMOVE_CARDDETAILSFETCH, jsonRequest, new Response.Listener<JSONObject>() {
//                @Override
//                public void onResponse(JSONObject JsonObj) {
//                    try {
//
//                        Log.i("Card Response", JsonObj.toString());
//                        String message = JsonObj.getString("message");
//                        String code = JsonObj.getString("code");
//
//                        if (code != null && code.equals("S00")) {
//                            sendRefresh();
//                            cardNo = JsonObj.getString("cardNo");
//                            expiryDate = JsonObj.getString("expiryDate");
//                            cvv = JsonObj.getString("cvv");
//                            accountHolder = JsonObj.getString("accountHolder");
//
//                            pbcard.setVisibility(View.GONE);
////                            addCard.setVisibility(View.GONE);
////                            flmain.setBackgroundResource(R.drawable.debitcard);
//                            llheaders.setBackgroundResource(R.drawable.debitcard);
//                            pbcard.setVisibility(View.GONE);
//                            llcard.setVisibility(View.VISIBLE);
//                            tvCardholder.setText(accountHolder);
//                            tvCardnumber.setText(cardNo.replaceAll("\\D", "").replaceAll("(\\d{4}(?!$))", "$1 "));
//                            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "card_test.ttf");
//                            tvdate.setTypeface(tf);
//                            tvCardholder.setTypeface(tf);
//                            tvCardnumber.setTypeface(tf);
//                            tvcvv.setTypeface(tf);
//                            llcopy.setVisibility(View.VISIBLE);
//
//
//                            String date_before = expiryDate.toLowerCase();
//                            String date_after = formateDateFromstring(date_before);
//                            tvdate.setText("Valid To:" + date_after);
//                            tvcvv.setText("cvv:" + cvv);
//
//                            llcard.setOnLongClickListener(new View.OnLongClickListener() {
//
//                                @Override
//                                public boolean onLongClick(View v) {
//                                    ClipboardManager cm = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
//                                    cm.setText(tvCardnumber.getText());
//                                    Toast.makeText(getActivity(), "Copied to clipboard", Toast.LENGTH_SHORT).show();
//                                    return true;
//                                }
//                            });
//
//
//                        }
//                        if (code != null && code.equals("F03")) {
//                            pbcard.setVisibility(View.GONE);
//                            Toast.makeText(getActivity(), "Your session is expire, please login", Toast.LENGTH_SHORT).show();
//                            sendLogout();
//
//                        } else {
//                            pbcard.setVisibility(View.GONE);
////                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
//                        }
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                        pbcard.setVisibility(View.GONE);
//                        CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
//                    }
//                }
//            }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    loadDlg.dismiss();
//                    pbcard.setVisibility(View.VISIBLE);
//                    CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
//                    error.printStackTrace();
//
//                }
//            }) {
//
//
//            };
//            int socketTimeout = 60000;
//            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//            postReq.setRetryPolicy(policy);
//            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
//        }
//
//    }

    private void sendRefresh() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "1");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton(getResources().getString(R.string.agree), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton(getResources().getString(R.string.Dismiss), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    private void blockVirtualCard() {
        loadDlg.show();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("sessionId", session.getUserSessionId());
            jsonObject.put("requestType", "suspend");
            jsonObject.put("cardType", "virtualCard");
        } catch (Exception e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, URL_BLOCK_VIRTUAL_CARD, jsonObject,

                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("blockVirtualCard", response.toString());
                        Log.d("blockVirtualCardURL", URL_BLOCK_VIRTUAL_CARD);
                        try {
                            if (response.get("code").toString().equalsIgnoreCase("s00")) {
                                loadDlg.dismiss();
//                                Picasso.with(getActivity()).load("gkdhfg").resize(300, 100).placeholder(R.drawable.ic_inactivcards).into(ivpoints);

//                                cardStatus.setText("BLOCKED");
//                                ivCopy.setVisibility(View.GONE);
                                tvCardnumber.setVisibility(View.GONE);
                                tvCardholder.setVisibility(View.GONE);
                                tvdate.setVisibility(View.GONE);
                                tvcvv.setVisibility(View.GONE);
                                tvcvvhide.setVisibility(View.GONE);
//                                tvhidecvv.setVisibility(View.GONE);
//                                btnCopy.setVisibility(View.GONE);
                                scheck.setChecked(false);
                                llheaders.setBackgroundResource(R.drawable.debitblock);

                                llCopy.setClickable(false);
                                scheck.setClickable(false);

                                llUnBlock.setVisibility(View.VISIBLE);
                                llBlock.setVisibility(View.GONE);

                                List<UserModel> currentUserList = Select.from(UserModel.class).list();
                                UserModel currentUser = currentUserList.get(0);
                                currentUser.setVCStatus(true);
                                session.setVCStatus(true);
                                currentUser.save();
                                session.save();

                                loadDlg.dismiss();
                                sendRefresh();

//                                llpoints.setVisibility(View.GONE);
//                                llunblock.setVisibility(View.VISIBLE);

//                                Toast.makeText(getApplicationContext(), response.get("message").toString(), Toast.LENGTH_SHORT).show();
                            } else {
//                                Toast.makeText(getApplicationContext(), response.get("message").toString(), Toast.LENGTH_SHORT).show();
                                loadDlg.dismiss();
                            }
                        } catch (Exception e) {
                            loadDlg.dismiss();
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loadDlg.dismiss();
                        Log.d("VirtualCard", "Inside volleyError");
                    }

                }

        );

        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonObjectRequest.setRetryPolicy(policy);
        PayQwikApplication.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    private void activateVirtualCard() {
        loadDlg.show();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("sessionId", session.getUserSessionId());
            jsonObject.put("cardType", "virtualCard");
//            jsonObject.put("requestType","suspend");
        } catch (Exception e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, URL_REACTIVATE_CARD
                , jsonObject,

                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("activateVirtualCard", response.toString());
                        Log.d("activateVirtualCardURL", URL_REACTIVATE_CARD);

                        try {
                            if (response.get("code").toString().equalsIgnoreCase("s00")) {

//                                applyforcard.setVisibility(View.GONE);
                                tvCardnumber.setVisibility(View.GONE);
                                tvCardholder.setVisibility(View.GONE);
                                tvdate.setVisibility(View.GONE);
                                tvcvv.setVisibility(View.GONE);
//                                tvhidecvv.setVisibility(View.GONE);
//                                btnCopy.setVisibility(View.VISIBLE);
//                                ivCopy.setVisibility(View.GONE);
//                                llpoints.setVisibility(View.VISIBLE);
//                                llunblock.setVisibility(View.GONE);
//                                loadDlg.dismiss();
                                llheaders.setBackgroundResource(R.drawable.debitcard);

                                llUnBlock.setVisibility(View.GONE);
                                llBlock.setVisibility(View.VISIBLE);

                                loadDlg.dismiss();

                                List<UserModel> currentUserList = Select.from(UserModel.class).list();
                                UserModel currentUser = currentUserList.get(0);
                                currentUser.setVCStatus(false);
                                session.setVCStatus(false);
                                currentUser.save();
                                session.save();

                                loadDlg.dismiss();
//                                sendRefresh();


                                cardFetch();
                                Toast.makeText(getContext(), response.get("message").toString(), Toast.LENGTH_SHORT).show();
                            } else {
                                loadDlg.dismiss();
                                Toast.makeText(getContext(), response.get("message").toString(), Toast.LENGTH_SHORT).show();

                            }
                        } catch (Exception e) {
                            loadDlg.dismiss();
                            llUnBlock.setVisibility(View.VISIBLE);
                            llBlock.setVisibility(View.GONE);
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loadDlg.dismiss();
                        llUnBlock.setVisibility(View.VISIBLE);
                        llBlock.setVisibility(View.GONE);
                        Log.d("VirtualCard", "Inside volleyError");
                    }

                }

        );

        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonObjectRequest.setRetryPolicy(policy);
        PayQwikApplication.getInstance().addToRequestQueue(jsonObjectRequest);
    }

}