//package in.paulpay.fragment.fragmentnaviagtionitems;
//
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.design.widget.TabLayout;
//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentManager;
//import android.support.v4.app.FragmentStatePagerAdapter;
//import android.support.v4.view.ViewPager;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//
//import in.paulpay.fragment.fragmenttravel.BusTravelActivity;
//import in.paulpay.fragment.fragmenttravel.CabTravelActivity;
//import in.paulpay.fragment.fragmenttravel.FlightTravelActivity;
//import in.paulpay.fragment.fragmenttravel.HotelTravelActivity;
//import in.paulpay.metadata.AppMetadata;
//import in.paulpay.vpayqwiktest.PayQwikApplication;
//import in.paulpay.vpayqwiktest.R;
//
///**
// * Created by Dushant on 08/27/2017.
//         */
//
//public class TravelFragment extends Fragment {
//
//    CharSequence TitlesEnglish[] = null;
////    CharSequence TitlesEnglish[] = {"Bus", "Flight", "Hotel", "Cab"};
////    CharSequence TitlesEnglish[] = {getResources().getString(R.string.Bus), getResources().getString(R.string.Flight), getResources().getString(R.string.Hotel), getResources().getString(R.string.Cab)};
//
//
//    int NumbOfTabs = 4;
//    private View rootView;
//    private TabLayout mSlidingTabLayout;
//    private ViewPager mainPager;
//    private FragmentManager fragmentManager;
//    private String navType = "";
//
//    @Override
//    public void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        TitlesEnglish = new CharSequence[]{getResources().getString(R.string.Bus), getResources().getString(R.string.Flight), getResources().getString(R.string.Hotel), getResources().getString(R.string.Cab)};
//        Bundle navBundle = getArguments();
//        if (navBundle != null) {
//            navType = navBundle.getString(AppMetadata.FRAGMENT_TYPE);
//        }
//        fragmentManager = getActivity().getSupportFragmentManager();
//    }
//
//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View rootView = inflater.inflate(R.layout.fragment_mobile_topup, container, false);
//        ViewPager mainPager = (ViewPager) rootView.findViewById(R.id.mainPager);
//        TabLayout mSlidingTabLayout = (TabLayout) rootView.findViewById(R.id.tabLayoutMobileTopUp);
//        FragmentManager fragmentManager=getChildFragmentManager();
//        mainPager.setAdapter(new ViewPagerAdapter(fragmentManager, TitlesEnglish, NumbOfTabs));
//        mSlidingTabLayout.setupWithViewPager(mainPager);
//        return rootView;
//    }
//    @Override
//    public void onResume() {
//        super.onResume();
//        PayQwikApplication.getInstance().trackScreenView("Travel");
//    }
//
////    public class ViewPagerAdapter extends FragmentStatePagerAdapter {
////
////        CharSequence Titles[]; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
////        int NumbOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created
////
////
////        // Build a Constructor and assign the passed Values to appropriate values in the class
////        public ViewPagerAdapter(FragmentManager fm, CharSequence mTitles[], int mNumbOfTabsumb) {
////            super(fm);
////            this.Titles = mTitles;
////            this.NumbOfTabs = mNumbOfTabsumb;
////
////        }
////
//////        @Override
//////        public Fragment getItem(int position) {
//////            if (position == 0) {
//////                return new BusTravelActivity();
//////            } else if (position == 1) {
//////                return new FlightTravelActivity();
//////            } else if (position == 2) {
//////                return new HotelTravelActivity();
//////            } else {
//////                return new CabTravelActivity();
//////            }
//////        }
////
////        @Override
////        public CharSequence getPageTitle(int position) {
////            return Titles[position];
////        }
////
////        @Override
////        public int getCount() {
////            return NumbOfTabs;
////        }
////    }
//}
//
