package in.MadMoveGlobal.fragment.fragmentnaviagtionitems;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import in.MadMoveGlobal.EwireRuPay.activity.MainActivity;
import in.MadMoveGlobal.EwireRuPay.activity.MainMenuDetailActivity;
import in.MadMoveGlobal.adapter.HomeSliderAdapter;
import in.MadMoveGlobal.custom.CirclePageIndicator;
import in.MadMoveGlobal.custom.CustomAlertsDialog;
import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.custom.PageIndicator;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.custom.CustomAlertDialog;
import in.MadMoveGlobal.custom.CustomSuccessDialog;
import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.metadata.AppMetadata;
import in.MadMoveGlobal.model.GroupModel;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.util.CheckLog;
import in.MadMoveGlobal.util.NetworkErrorHandler;
import in.MadMoveGlobal.util.PayingDetailsValidation;
import in.MadMoveGlobal.util.ServiceUtility;


/**
 * Created by Ksf on 4/9/2016.
 */
public class DonationFragment extends Fragment {

    private View rootView;
    private TextView tvDonee;
    private EditText etAmount, etMessage;
    private Button btnDonate;

    private UserModel session = UserModel.getInstance();
    private View focusView = null;
    private boolean cancel;

    private String merchantId = "";
    private JSONObject jsonRequest;

    private LoadingDialog loadDlg;
    //Volley Tag
    private String tag_json_obj = "json_merchant_pay";

    //Donee List
    private ArrayList<GroupModel> groupModelList;
    private String dContatctNo;
    private in.MadMoveGlobal.custom.spinerdialog.SpinnerDialog spinnerDialogC;
    private int postion;
    private ArrayList<String> nameArrayC = new ArrayList<>();
    private ArrayList<String> msgArray = new ArrayList<>();
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    private int[] IMAGES;
    public ViewPager mPager;
    public CirclePageIndicator indicator;
    private TextView tvTAC, tvDonees;
    private int position;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadDlg = new LoadingDialog(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_donation, container, false);
        tvDonee = (TextView) rootView.findViewById(R.id.tvDonee);
        etAmount = (EditText) rootView.findViewById(R.id.etAmount);
        etMessage = (EditText) rootView.findViewById(R.id.etMessage);
        btnDonate = (Button) rootView.findViewById(R.id.btnDonate);
        mPager = (ViewPager) rootView.findViewById(R.id.pagerHome);
        indicator = (CirclePageIndicator) rootView.findViewById(R.id.productHeaderImageSlider);
        tvTAC = (TextView) rootView.findViewById(R.id.tvTAC);
        tvDonees = (TextView) rootView.findViewById(R.id.tvDonees);
        tvDonees.setText((MainActivity.group));

//        showTransferDialog();

        getImageSlider();
        loadDoneeList();
        btnDonate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptPayment();
            }
        });

        tvTAC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent menuIntent = new Intent(getActivity(), MainMenuDetailActivity.class);
                menuIntent.putExtra(AppMetadata.FRAGMENT_TYPE, "DonationTandC");
                getActivity().startActivity(menuIntent);

//                TravelHealthFragment fragment = new TravelHealthFragment();
//                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
//                Bundle bType = new Bundle();
//                bType.putString("URL", "http://ewiresofttech.com/gtandc.html");
//                fragment.setArguments(bType);
//                fragmentTransaction.replace(R.id.llLRMain, fragment);
//                fragmentTransaction.commit();
            }
        });

        return rootView;
    }


    public void loadDoneeList() {
        loadDlg.show();
     /*   jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }*/

//        if (jsonRequest != null) {
//            Log.i("LOADCIROPRE Request", jsonRequest.toString());
        Log.i("GRPURL", ApiUrl.URL_DONEE_LIST);
        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_DONEE_LIST, "", new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject JsonObj) {
                loadDlg.dismiss();
               Log.v("SSS","SSS Response "+JsonObj);
                try {
                    String code = JsonObj.getString("code");
                    if (code.equals("S00")) {
                        groupModelList = new ArrayList<>();
                        JSONArray circleArray = JsonObj.getJSONArray("donationList");
                        for (int i = 0; i < circleArray.length(); i++) {
                            JSONObject c = circleArray.getJSONObject(i);
                            String op_code = c.getString("contactNo");
                            String op_name = c.getString("organization");

                            if (op_name.equalsIgnoreCase(MainActivity.group)) {
                                dContatctNo = op_code;
                            }
                          //  Log.i("CONTACTNO.    GROUPNAME", op_code + "  " + op_name);
                            /*GroupModel cModel = new GroupModel(op_name, op_code);
                            groupModelList.add(cModel);
                            nameArrayC.add(op_name);
                            msgArray.add(c.getString("message"));*/
                        }

                        /*if (groupModelList != null && groupModelList.size() != 0) {

                            Log.v("SSS","SSS groupModel "+groupModelList.size());
                            try {
                                spinnerDialogC = new in.MadMoveGlobal.custom.spinerdialog.SpinnerDialog(getActivity(), nameArrayC, "Select your Group", R.style.DialogAnimations_SmileWindow, "Close");
                                spinnerDialogC.bindOnSpinerListener((item, position) -> {
                                    dContatctNo = groupModelList.get(position).getId();
                                    tvDonee.setText(nameArrayC.get(position));
                                    tvDonee.setError(null);
                                    etMessage.setText(msgArray.get(position));
                                    ServiceUtility.hideKeyboard(getActivity());
                                });

                                receiverCV.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Log.v("SSS","SSS onClick");
                                       spinnerDialogC.showSpinerDialog();
                                    }
                                });


                                if (groupModelList != null && groupModelList.size() != 0) {
                                    spinnerDialogC.bindOnSpinerListener((item, position) -> {
                                        dContatctNo = groupModelList.get(position).getId();
                                        tvDonee.setText(nameArrayC.get(position));
                                        tvDonee.setError(null);
                                        etMessage.setText(msgArray.get(position));
                                        ServiceUtility.hideKeyboard(getActivity());
                                    });
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }*/
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), getResources().getString(R.string.server_exception2), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loadDlg.dismiss();
                error.printStackTrace();
                Toast.makeText(getActivity(), getResources().getString(R.string.server_exception), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> map = new HashMap<>();
//                map.put("hash", "1234");
                map.put("Content-Type", "application/json");
                return map;
            }

        };
        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);
        PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
//        }

    }

    private void attemptPayment() {
        etAmount.setError(null);
        cancel = false;
//        checkDonee(tvDonee.getText().toString());
        checkPayAmount(etAmount.getText().toString());
        if (cancel) {
            focusView.requestFocus();
        } else {
            promoteDonationPay();
        }
    }

    private void checkPayAmount(String amount) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
        if (!gasCheckLog.isValid) {
            etAmount.setError(getResources().getString(R.string.Please_enter_valid_amount));
            focusView = etAmount;
            cancel = true;
        } else if (Integer.valueOf(etAmount.getText().toString()) < 1 || Integer.valueOf(etAmount.getText().toString()) > 10000) {
            etAmount.setError(getResources().getString(R.string.Amount_should_be_between_10_to_10000));
            focusView = etAmount;
            cancel = true;
        }
    }

    private void checkDonee(String amount) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
        if (!gasCheckLog.isValid) {
            tvDonee.setError(getString(gasCheckLog.msg));
            focusView = tvDonee;
            cancel = true;
        }
    }

//    public void promoteDonationPay() {
//        loadDlg.show();
//        jsonRequest = new JSONObject();
//        try {
////            jsonRequest.put("sessionId", session.getUserSessionId());
////            jsonRequest.put("mobileNumber", dContatctNo);
////            jsonRequest.put("amount", etAmount.getText().toString());
////            jsonRequest.put("message", etMessage.getText().toString());
//
//
//            jsonRequest.put("recipientNo", dContatctNo);
//            jsonRequest.put("amount",  etAmount.getText().toString());
//            jsonRequest.put("message", " ");
//            jsonRequest.put("sessionId", session.getUserSessionId());
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//            jsonRequest = null;
//        }
//
//        if (jsonRequest != null) {
//            Log.i("DONREQ", jsonRequest.toString());
//            Log.i("DONURL", ApiUrl.URL_FUND_DONATION);
//            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_FUND_DONATION, jsonRequest, new Response.Listener<JSONObject>() {
//                @Override
//                public void onResponse(JSONObject response) {
//                    Log.i("DONRES", response.toString());
//                    try {
//                        String code = response.getString("code");
//                        if (code != null && code.equals("S00")) {
//                            loadDlg.dismiss();
//                            String jsonString = response.getString("response");
//                            JSONObject jsonObject = new JSONObject(jsonString);
//                            String successMessage = jsonObject.getString("message");
//                            sendRefresh();
//                            showSuccessDialog();
//
//                        } else if (code != null && code.equals("F03")) {
//                            loadDlg.dismiss();
//                            showInvalidSessionDialog();
//                        } else {
//                            loadDlg.dismiss();
//                            String errorMessage = response.getString("message");
//                            CustomToast.showMessage(getActivity(), errorMessage);
//                        }
//
//                    } catch (JSONException e) {
//                        loadDlg.dismiss();
//                        CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
//                        e.printStackTrace();
//                    }
//                }
//            }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    loadDlg.dismiss();
//                    CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));
//                    error.printStackTrace();
//
//                }
//            }) {
//                @Override
//                public Map<String, String> getHeaders() throws AuthFailureError {
//                    HashMap<String, String> map = new HashMap<>();
//                    map.put("hash", "1234");
//                    return map;
//                }
//
//            };
//            int socketTimeout = 60000;
//            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//            postReq.setRetryPolicy(policy);
//            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
//        }
//
//    }


    public void promoteDonationPay() {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("recipientNo", dContatctNo);
            jsonRequest.put("amount", etAmount.getText().toString());
            jsonRequest.put("message", etMessage.getText().toString());
            jsonRequest.put("sessionId", session.getUserSessionId());

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
           Log.v("SSS","SSS API "+ApiUrl.URL_FUND_DONATION +" Request "+jsonRequest);
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_FUND_DONATION, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.i("SSS Response", response.toString());
                    try {
                        String code = response.getString("code");
                        if (code != null && code.equals("S00")) {

                            sendRefresh();
                            loadDlg.dismiss();
                            showSuccessDialog();
//                            if (response.has("message") && response.getString("message") != null) {
//                                String message = response.getString("message");
//                                CustomToast.showMessage(getActivity(), message);
//                            } else {
//                                CustomToast.showMessage(getActivity(), "Error message is null");
//                            }
                        } else if (code != null && code.equals("F03")) {
                            loadDlg.dismiss();
                            showInvalidSessionDialog();
                        } else {
                            loadDlg.dismiss();
                            if (response.has("message") && response.getString("message") != null) {
                                String message = response.getString("message");
                                CustomToast.showMessage(getActivity(), message);
                            } else {
                                CustomToast.showMessage(getActivity(), getResources().getString(R.string.Error_message_is_null));
                            }
                        }

                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
                    error.printStackTrace();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }

    }


    private void sendRefresh() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "1");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    private void sendLogout() {
        getActivity().finish();
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    public void showInvalidSessionDialog() {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(AppMetadata.getInvalidSession(), Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(AppMetadata.getInvalidSession());
        }
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, result);
        builder.setPositiveButton(getResources().getString(R.string.agree), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                sendLogout();
            }
        });
        builder.setNegativeButton(getResources().getString(R.string.Dismiss), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }


    public void showSuccessDialog() {
        CustomSuccessDialog builder = new CustomSuccessDialog(getActivity(), getResources().getString(R.string.Payment_Successful), getResources().getString(R.string.Donation_Successful_Thank_you_for_your_kind_Donation));
        builder.setPositiveButton(getResources().getString(R.string.agree), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                Intent mainActivity = new Intent(getActivity(), MainActivity.class);
                startActivity(mainActivity);
                getActivity().finish();
            }
        });
        builder.show();
    }


    private void getImageSlider() {
        IMAGES = new int[]{R.drawable.toolbar_background, R.drawable.toolbar_background, R.drawable.toolbar_background, R.drawable.toolbar_background, R.drawable.toolbar_background};
        mPager.setAdapter(new HomeSliderAdapter(getActivity(), IMAGES));
        indicator.setViewPager(mPager);
        final float density = this.getResources().getDisplayMetrics().density;
        indicator.setRadius(5 * density);

        NUM_PAGES = IMAGES.length;

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 2000, 3000);

        // Pager listener over indicator
        indicator.setOnPageChangeListener(new PageIndicator() {
            @Override
            public void setViewPager(ViewPager view) {

            }

            @Override
            public void setViewPager(ViewPager view, int initialPosition) {

            }

            @Override
            public void setCurrentItem(int item) {

            }

            @Override
            public void setOnPageChangeListener(ViewPager.OnPageChangeListener listener) {

            }

            @Override
            public void notifyDataSetChanged() {

            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }


    public void showTransferDialog() {
        String[] listItems = {"Via Card", "Via Bank"};

        CustomAlertsDialog builder = new CustomAlertsDialog(getActivity(), R.string.dialog_title2, "");
        int checkedItem = 0;
        builder.setTitle("Select your payment method.");
        builder.setSingleChoiceItems(listItems, checkedItem, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                position = which;
//                Toast.makeText(getActivity(), "Position: " + which + " Value: " + listItems[which], Toast.LENGTH_LONG).show();
            }
        });

        builder.setPositiveButton(getResources().getString(R.string.agree), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                if (position == 0) {
                    dialog.dismiss();
                }
                if (position == 1) {

                    Intent menuIntent = new Intent(getActivity(), MainMenuDetailActivity.class);
                    menuIntent.putExtra(AppMetadata.FRAGMENT_TYPE, "DonationBankTransfer");
                    getActivity().startActivity(menuIntent);
                    getActivity().finish();

                }


            }
        });

        builder.show();
    }


}

