package in.MadMoveGlobal.fragment.fragmentnaviagtionitems;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.MadMoveGlobal.EwireRuPay.activity.MainActivity;
import in.MadMoveGlobal.custom.CustomAlertDialog;
import in.MadMoveGlobal.custom.CustomSuccessDialog;
import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.util.CheckLog;
import in.MadMoveGlobal.util.EmailCouponsUtil;
import in.MadMoveGlobal.util.GenerateAPIHeader;
import in.MadMoveGlobal.util.NetworkErrorHandler;
import in.MadMoveGlobal.util.PayingDetailsValidation;

/**
 * Created by Dushant on 08/27/2017.
 */
public class FundTransferByMobileFragment extends Fragment {

    public static final int PICK_CONTACT = 1;
    private View rootView;
    private Button btnFundTransfer;
    private ImageButton ibFundTransferPhoneBook;
    private MaterialEditText etFundTransferNo;
    private MaterialEditText etFundTransferName, etFundTransferAmount;
    private UserModel session = UserModel.getInstance();

    private View focusView = null;
    private boolean cancel;
    private String transferNo, transferName, transferAmount;
    private LoadingDialog loadDlg;
    private JSONObject jsonRequest;
    private String sucessMessage;

    //Volley Tag
    private String tag_json_obj = "json_fund_pay";


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadDlg = new LoadingDialog(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_fund_transfer, container, false);
        etFundTransferAmount = (MaterialEditText) rootView.findViewById(R.id.etFundTransferAmount);
        etFundTransferName = (MaterialEditText) rootView.findViewById(R.id.etFundTransferName);
        etFundTransferNo = (MaterialEditText) rootView.findViewById(R.id.etFundTransferNo);
        btnFundTransfer = (Button) rootView.findViewById(R.id.btnFundTransfer);
        ibFundTransferPhoneBook = (ImageButton) rootView.findViewById(R.id.ibFundTransferPhoneBook);
        onDetach();

        ibFundTransferPhoneBook.setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                        intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                        startActivityForResult(intent, PICK_CONTACT);
                    }
                });


        btnFundTransfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSoftwareKeyboard(false);
                alertdialog1(generateVerifyMessage1());

            }
        });

        //DONE CLICK ON VIRTUAL KEYPAD
        etFundTransferAmount.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    attemptPayment();
                }
                return false;
            }
        });


        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        String[] projection = {ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.NUMBER};
        switch (requestCode) {
            case (PICK_CONTACT):
                if (resultCode == Activity.RESULT_OK) {
                    Uri contactUri = data.getData();
                    Cursor c = getActivity().getContentResolver().query(contactUri, projection, null, null, null);
                    if (c.moveToFirst()) {
                        String phoneNumber = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        String finalNumber = phoneNumber.replaceAll("[^0-9\\+]", "");
                        if (finalNumber != null && !finalNumber.isEmpty()) {
                            removeCountryCode(finalNumber);
                        }
                    }
                }
        }
    }

    private void attemptPayment() {

        etFundTransferNo.setError(null);
//        etFundTransferName.setError(null);
        etFundTransferAmount.setError(null);

        cancel = false;

        transferAmount = etFundTransferAmount.getText().toString();
        transferNo = etFundTransferNo.getText().toString();
        transferName = etFundTransferName.getText().toString();

        checkPayAmount(transferAmount);
        checkPhone(transferNo);
//        checkName(transferName);

        if (cancel) {
            focusView.requestFocus();
        } else {

            alertdialog(generateVerifyMessage());

        }


    }

    private void checkPhone(String phNo) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkMobileTenDigit(phNo);
        if (!gasCheckLog.isValid) {
            etFundTransferNo.setError(getString(gasCheckLog.msg));
            focusView = etFundTransferNo;
            cancel = true;
        }
    }

    private void checkPayAmount(String amount) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
        if (!gasCheckLog.isValid) {
            etFundTransferAmount.setError(getString(gasCheckLog.msg));
            focusView = etFundTransferAmount;
            cancel = true;
        } else if (Integer.valueOf(etFundTransferAmount.getText().toString()) < 1) {
            etFundTransferAmount.setError(getString(R.string.lessAmount));
            focusView = etFundTransferAmount;
            cancel = true;
        }
    }

    private void checkName(String name) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(name);
        if (!gasCheckLog.isValid) {
            etFundTransferName.setError(getString(gasCheckLog.msg));
            focusView = etFundTransferName;
            cancel = true;
        }
    }

    private void removeCountryCode(String number) {
        if (hasCountryCode(number)) {
            int country_digits = number.length() - 10;
            number = number.substring(country_digits);
            etFundTransferNo.setText(number);
        } else if (hasZero(number)) {
            if (number.length() >= 10) {
                int country_digits = number.length() - 10;
                number = number.substring(country_digits);
                etFundTransferNo.setText(number);
            } else {
                CustomToast.showMessage(getActivity(), getResources().getString(R.string.Please_select_10_digit_no));
            }

        } else {
            etFundTransferNo.setText(number);
        }

    }

    private boolean hasZero(String number) {
        return number.charAt(0) == '0';
    }

    private boolean hasCountryCode(String number) {
        return number.charAt(0) == '+';
    }


    public void promoteSendMoney() {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("recipientNo", etFundTransferNo.getText().toString());
            jsonRequest.put("amount", etFundTransferAmount.getText().toString());
            jsonRequest.put("message", etFundTransferName.getText().toString());
            jsonRequest.put("sessionId", session.getUserSessionId());
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_FUND_TRANSFER, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.i("Send Money", response.toString());
                    try {
                        String code = response.getString("code");
                        if (code != null && code.equals("S00")) {
//                            String jsonString = response.getString("response");
//                            JSONObject jsonObject = new JSONObject(jsonString);
                            sucessMessage = response.getString("message");
                            etFundTransferNo.getText().clear();
                            etFundTransferAmount.getText().clear();
                            etFundTransferName.getText().clear();

                            loadDlg.dismiss();
//                            sendRefresh();


                            try {
                                EmailCouponsUtil.emailForCoupons(tag_json_obj);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            showSuccessDialog();
//                            CustomToast.showMessage(getActivity(), sucessMessage);
                        } else if (code != null && code.equals("F03")) {
                            loadDlg.dismiss();
                            CustomToast.showMessage(getActivity(), getResources().getString(R.string.Please_login_and_try_again));
                            sendLogout();
                        } else if (code != null && code.equals(ApiUrl.C_SESSION_BLOCKED)) {
                            loadDlg.dismiss();

                        } else {
                            loadDlg.dismiss();
                            if (response.has("message") && response.getString("message") != null) {
                                String message = response.getString("message");
                                CustomToast.showMessage(getActivity(), message);
                            }
                        }
                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));

                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();

                    CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));

                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hash", GenerateAPIHeader.getHeader(ApiUrl.H_SENDMONEY + session.getUserSessionId()));
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }

    }

    private void sendRefresh() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "1");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }


    public static void hideKeyboard(Context ctx) {
        InputMethodManager inputManager = (InputMethodManager) ctx
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View v = ((Activity) ctx).getCurrentFocus();
        if (v == null)
            return;

        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    @Override
    public void onResume() {
        super.onResume();
        PayQwikApplication.getInstance().trackScreenView("Fund transfer");
    }


    private String getConvenienceCharge() {
        if (Double.parseDouble(transferAmount) <= 999) {
            return "1";
        } else if (Double.parseDouble(transferAmount) > 999 && Double.parseDouble(transferAmount) <= 4999) {
            return "2";
        } else if (Double.parseDouble(transferAmount) > 4999 && Double.parseDouble(transferAmount) <= 9999) {
            return "5";
        } else if (Double.parseDouble(transferAmount) > 9999) {
            return "10";
        }
        return null;
    }

//    private boolean canSendIt() {
//        if ((Double.parseDouble(transferAmount) + Double.parseDouble(getConvenienceCharge()) > Double.parseDouble(session.getUserBalance()))) {
//            return false;
//        } else {
//            return true;
//        }
//    }

    private void showSuccessDialog() {

        CustomSuccessDialog builder = new CustomSuccessDialog(getActivity(), getResources().getString(R.string.Transferred_Successfully), sucessMessage);
        builder.setPositiveButton(getResources().getString(R.string.agree), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                Intent mainActivity = new Intent(getActivity(), MainActivity.class);
                startActivity(mainActivity);
            }
        });
        builder.show();
    }


    @Override
    public void onDetach() {
        super.onDetach();

        //hide keyboard when any fragment of this class has been detached
        showSoftwareKeyboard(false);
    }

    protected void showSoftwareKeyboard(boolean showKeyboard) {
        final Activity activity = getActivity();
        final InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);

//        inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), showKeyboard ? InputMethodManager.SHOW_FORCED : InputMethodManager.HIDE_NOT_ALWAYS);
    }

    public void alertdialog(String message) {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(message));
        builder.setPositiveButton(getResources().getString(R.string.Accept), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                promoteSendMoney();


            }
        });
        builder.setNegativeButton(getResources().getString(R.string.Decline), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public void alertdialog1(String message) {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(message));
        builder.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                attemptPayment();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.show();
    }


    public String generateVerifyMessage() {


        String source = "<b><font color=#000000> ₹ </font></b>" + "<font>" + transferAmount + " " + getResources().getString(R.string.will_be_credited_to) + " " + transferNo + "</font><br>" +
                "<br><br><b><font color=#ff0000>" + getResources().getString(R.string.Accept_Cont) + "</font></b><br></br>";
        return source;
    }

    public String generateVerifyMessage1() {


        String source = "<b><font color=#000000> </font></b>" + "<font>" + "Please Note:-  There will be an additional charges of ₹ 2/- in order to transfer the amount." + "</font><br>";
        return source;
    }

}
