package in.MadMoveGlobal.fragment.fragmentnaviagtionitems;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.orm.query.Select;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.MadMoveGlobal.adapter.HomeMenuAdapter;
import in.MadMoveGlobal.custom.CustomAlertDialog;
import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.metadata.MenuMetadata;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;


/**
 * Created by Dushant on 08/27/2017.
 */
public class HomeFragment extends Fragment {

    //Music Dailog
    SharedPreferences musicPreferences;
    boolean ShowMusicDailog;
    private View rootView;
    private RecyclerView rvHome;
    private UserModel session = UserModel.getInstance();
    private ImageButton play_button, ibClose;
    private ImageView images, ivLoadMoney, ivCopy, ivpoints, ivinitials;
    private TextView text_shown;
    private TextView tvVC, tvPC;
    private MediaPlayer player;
    private String cardNo, expiryDate, cvv, accountHolder;
    private TextView tvCardnumber, tvCardholder, tvdate, tvcvv, applyforcard, tvhidecvv;
    private LinearLayout llcard, llCards, llunblock, llpoints, llinitial;
    private LoadingDialog loadDlg;
    private JSONObject jsonRequest;
    private JsonObjectRequest postReq;
    private String tag_json_obj = "json_user";

    //984422549
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        ZohoSalesIQ.init(getActivity().getApplication(), "oK28J%2BCX8OhaqWXFf3UwX21OPnGi0KIpremUyHBt7ilHWyHvdo2vsf45rut%2FXM4j", "7SoPb1HVk5zfO4ZDnVYw3kBD8ZSkqOnMhJfBXsenq7rYHga5HOMyonQfsblnhd45snm%2FBrO1iNEIuKe9Ae%2FXraT0nLe5H24lGcmu2DI8yb%2BmJzCTEOfGppARWdMlF7oyT6w7y%2BMAyqVJlFIXY4yciBWX75UgJ%2FToGEU0%2F7NqXs4%3D");
        musicPreferences = getActivity().getSharedPreferences("ShowMusicDailog", Context.MODE_PRIVATE);
        ShowMusicDailog = musicPreferences.getBoolean("ShowMusicDailog", false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_home_new, container, false);
        rvHome = (RecyclerView) rootView.findViewById(R.id.rvHome);
        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.recycle_spacing);
        rvHome.addItemDecoration(new SpacesItemDecoration(spacingInPixels));

        GridLayoutManager manager = new GridLayoutManager(getActivity(), 3);
//        manager.setOrientation(GridLayoutManager.VERTICAL);
        rvHome.setLayoutManager(manager);
        rvHome.setHasFixedSize(true);

        HomeMenuAdapter rcAdapter = new HomeMenuAdapter(getActivity(), MenuMetadata.getMenu(getActivity()));
        rvHome.setAdapter(rcAdapter);

        loadDlg = new LoadingDialog(getActivity());

        TextView tvUserName = (TextView) rootView.findViewById(R.id.tvUserName);


        if (!session.isHasVcard()) {

            loadCardDetails();


        }


        return rootView;
    }

    private Bitmap decodeFromBase64ToBitmap(String encodedImage)

    {
        byte[] decodedString = Base64.decode(encodedImage, Base64.DEFAULT);

        return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

    }


    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    public void showCustomDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(generateVerifyMessage()));
        builder.setPositiveButton(getResources().getString(R.string.Ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    public String generateVerifyMessage() {
        return "<b><font color=#000000> Please check your inbox and verify email, to proceed.</font></b>" +
                "<br><br><b><font color=#ff0000> You can check registered email address in edit profile section. </font></b><br></br>";
    }

    public void showCustomDialogNoService() {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(generateVerifyMessageNoService()));
        builder.setPositiveButton(getResources().getString(R.string.Ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    public String generateVerifyMessageNoService() {
        String source = "<b><font color=#000000></font></b>" +"<font>" +getResources().getString(R.string.Dear_User_Ewire)+"</font></b>"+
                "<br><br><b><font color=#ff0000></font></b><br></br>" +getResources().getString(R.string.Kindly_wait_for_official_release)+"</font></b>";
        return source;
    }

    @Override
    public void onResume() {
        super.onResume();
        PayQwikApplication.getInstance().trackScreenView("Home Screen");
    }

    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        public SpacesItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view,
                                   RecyclerView parent, RecyclerView.State state) {
            outRect.left = space;
            outRect.right = space;
            outRect.bottom = space;
            outRect.top = space;
        }
    }

    public void loadCardDetails() {
        loadDlg.show();
//        pbcard.setVisibility(View.VISIBLE);
        jsonRequest = new JSONObject();
        try {

            jsonRequest.put("sessionId", session.getUserSessionId());

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequestcards", jsonRequest.toString());
            Log.i("Card Url", ApiUrl.URL_MATCHMOVE_CARDDETAILS);
            postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_MATCHMOVE_CARDDETAILS, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject JsonObj) {
                    try {

                        Log.i("Card Response", JsonObj.toString());
                        String message = JsonObj.getString("message");
                        String code = JsonObj.getString("code");

                        if (code != null && code.equals("S00")) {
                            loadDlg.dismiss();
//                            pbcard.setVisibility(View.GONE);

                            List<UserModel> currentUserList = Select.from(UserModel.class).list();
                            UserModel currentUser = currentUserList.get(0);
                            currentUser.setHasVcard(true);
                            session.setHasVcard(true);
                            currentUser.save();


                        } else if (code != null && code.equals("F03")) {
                            loadDlg.dismiss();
//
//                            Snackbar.make(llcard, "Please login and try again", Snackbar.LENGTH_LONG).show();
//                            Toast.makeText(getActivity(), "Your session is expire, please login", Toast.LENGTH_SHORT).show();
                            sendLogout();

                        } else {
                            loadDlg.dismiss();
//
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        loadDlg.dismiss();
//                        pbcard.setVisibility(View.GONE);
//                        CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
                        Snackbar.make(llcard, getResources().getString(R.string.server_exception2), Snackbar.LENGTH_LONG).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
//                    pbcard.setVisibility(View.GONE);
//                    CustomToast.showMessage(getActivity(), "Network connection error");
                    Snackbar.make(llcard, getResources().getString(R.string.Network_connection_error), Snackbar.LENGTH_LONG).show();
                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }
            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }

    }

}
