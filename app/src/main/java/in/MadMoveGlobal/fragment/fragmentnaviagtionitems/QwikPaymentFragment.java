package in.MadMoveGlobal.fragment.fragmentnaviagtionitems;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import in.MadMoveGlobal.fragment.fragmentqwikpayment.FavouriteFragment;
import in.MadMoveGlobal.fragment.fragmentqwikpayment.QwikFragment;
import in.MadMoveGlobal.EwireRuPay.R;

/**
 * Created by Dushant on 08/27/2017.
 */
public class QwikPaymentFragment extends Fragment {
    CharSequence TitlesEnglish[] = null;

    /*
     * Sliding tabs Setup
	 */
    int NumbOfTabs = 2;
    private View rootView;
    private TabLayout mSlidingTabLayout;
    private ViewPager mainPager;
    private FragmentManager fragmentManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentManager = getActivity().getSupportFragmentManager();
        TitlesEnglish = new CharSequence[]{getActivity().getResources().getString(R.string.tab_quick_pay), getActivity().getResources().getString(R.string.tab_fav)};
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_mobile_topup, container, false);
        mainPager = (ViewPager) rootView.findViewById(R.id.mainPager);
        mSlidingTabLayout = (TabLayout) rootView.findViewById(R.id.tabLayoutMobileTopUp);
        mainPager.setAdapter(new ViewPagerAdapter(fragmentManager, TitlesEnglish, NumbOfTabs));
        mSlidingTabLayout.setupWithViewPager(mainPager);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public class ViewPagerAdapter extends FragmentStatePagerAdapter {

        CharSequence Titles[]; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
        int NumbOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created


        // Build a Constructor and assign the passed Values to appropriate values in the class
        public ViewPagerAdapter(FragmentManager fm, CharSequence mTitles[], int mNumbOfTabsumb) {
            super(fm);
            this.Titles = mTitles;
            this.NumbOfTabs = mNumbOfTabsumb;

        }

        @Override
        public Fragment getItem(int position) {

            if (position == 0) {
                QwikFragment tab1 = new QwikFragment();
                return tab1;
            } else {
                FavouriteFragment tab3 = new FavouriteFragment();
                return tab3;
            }


        }

        @Override
        public CharSequence getPageTitle(int position) {
            return Titles[position];
        }

        @Override
        public int getCount() {
            return NumbOfTabs;
        }
    }
}

