package in.MadMoveGlobal.fragment.fragmentnaviagtionitems;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Html;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.EwireRuPay.activity.MainActivity;
import in.MadMoveGlobal.custom.AESCrypt;
import in.MadMoveGlobal.custom.CustomAlertDialog;
import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.metadata.AppMetadata;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.util.CheckLog;
import in.MadMoveGlobal.util.EncryptDecryptUserUtility;
import in.MadMoveGlobal.util.NetworkErrorHandler;
import in.MadMoveGlobal.util.PayingDetailsValidation;

/**
 * Created by Dushant on 08/27/2017.
 */
public class SetPinFragment extends Fragment {
    public static final int PICK_CONTACT = 1;
    private View rootView;
    private Button btnRequest;
    private MaterialEditText etSetPin;
    private MaterialEditText etdob;
    private String pin;
    private String date;
    private boolean cancel;
    private View focusView;
    private ImageButton iBtnShowLoginPwd;
    String userKey = "udkrtnfg";
    String userPasswordKey = "30869142";

    private UserModel session = UserModel.getInstance();

    private JSONObject jsonRequest;
    private LoadingDialog loadDlg;

    //Volley Tag
    private String tag_json_obj = "json_invite_freinds";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadDlg = new LoadingDialog(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_setpin, container, false);
        etSetPin = (MaterialEditText) rootView.findViewById(R.id.etSetPin);
        etdob = (MaterialEditText) rootView.findViewById(R.id.etdob);
        btnRequest = (Button) rootView.findViewById(R.id.btnRequest);
        iBtnShowLoginPwd = (ImageButton) rootView.findViewById(R.id.iBtnShowLoginPwd);


        btnRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptRequestPin();

            }
        });


        iBtnShowLoginPwd.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    etSetPin.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    etSetPin.setSelection(etSetPin.length());
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    etSetPin.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    etSetPin.setSelection(etSetPin.length());
                }

                return false;
            }
        });


        //DONE CLICK ON VIRTUAL KEYPAD
        btnRequest.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    attemptRequestPin();
                }
                return false;
            }
        });
        etdob.setFocusable(true);
        etdob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dateOfBirthDialog();
            }
        });

        return rootView;
    }


    private void attemptRequestPin() {
        etSetPin.setError(null);
        etdob.setError(null);
        cancel = false;

        pin = etSetPin.getText().toString();
        date = etdob.getText().toString();


        checkInviteNumber(pin);
        if (!validateDob()) {
            return;
        } else if (cancel) {
            focusView.requestFocus();
        } else {

//            Intent intent2 = new Intent(); intent2.setAction(Intent.ACTION_SEND);
//            intent2.setType("text/plain");
//            intent2.putExtra(Intent.EXTRA_TEXT, "Hey!! I just started using PaulPay and its awesome. You should try using it as well! Click here to download the app: https://play.google.com/store/apps/details?id=in.msspay.paulpay");
//            startActivity(Intent.createChooser(intent2, "Share via"));
//            promoteInviteFriend();
            requestNewPin();
        }
    }


    private void checkInviteName(String inviteName) {
        CheckLog inviteNameCheckLog = PayingDetailsValidation.checkFirstName(inviteName);
        if (!inviteNameCheckLog.isValid) {
            etdob.setError(getString(inviteNameCheckLog.msg));
            focusView = etdob;
            cancel = true;
        }

    }

    private void checkInviteNumber(String inviteNumber) {
        CheckLog inviteNumberCheckLog = PayingDetailsValidation.checkPin(inviteNumber);
        if (!inviteNumberCheckLog.isValid) {
            etSetPin.setError(getString(inviteNumberCheckLog.msg));
            focusView = etSetPin;
            cancel = true;
        }

    }

    private void requestNewPin() {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("pin", etSetPin.getText().toString());
            jsonRequest.put("dob", etdob.getText().toString());


        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {

            JSONObject data = new JSONObject();
            try {
                data.put("customPinRequest", AESCrypt.encrypt(jsonRequest.toString()));
            } catch (Exception e) {
                e.printStackTrace();
            }

            Log.i("JsonRequest", jsonRequest.toString());
            Log.i("URLRequest", ApiUrl.URL_SET_PIN);
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_SET_PIN, data, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.i("SET PIN", response.toString());
                        String code = response.getString("code");
                        loadDlg.dismiss();
                        if (code != null && code.equals("S00")) {
                            loadDlg.dismiss();
                            etSetPin.getText().clear();
                            etdob.getText().clear();
                            if (response.has("message") && response.getString("message") != null) {
                                String message = response.getString("message");
                                successDialog(message);

//                                CustomToast.showMessage(getActivity(), message);
                            }

                        } else if (code != null && code.equals("F03")) {
                            loadDlg.dismiss();
                            showInvalidSessionDialog();
                        } else {
                            if (response.has("message") && response.getString("message") != null) {
                                String message = response.getString("message");
                                CustomToast.showMessage(getActivity(), message);
                            }
                        }

                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));

                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    error.printStackTrace();
                    CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));


                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    map.put("Authorization", EncryptDecryptUserUtility.encryptAuth(userKey + ":" + userPasswordKey));

                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }


    public void dateOfBirthDialog() {
//        DatePickerDialog dialogDate = new DatePickerDialog(getActivity(), R.style.DatePicker, new DatePickerDialog.OnDateSetListener() {
//            @SuppressLint("SetTextI18n")
//            @Override
//            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
//                Calendar userAge = new GregorianCalendar(year, (monthOfYear + 1), dayOfMonth);
//                Calendar minAdultAge = new GregorianCalendar();
//                minAdultAge.add(Calendar.YEAR, -18);
//                if (minAdultAge.before(userAge)) {
//                    etdob.setError("User must be at least 18 years old to Register");
//                    CustomToast.showMessage(getActivity(), "User must be at least 18 years old to Register");
//                    etdob.requestFocus();
//                } else {
//
//                    etdob.setText(String.valueOf(year) + "-"
//                            + String.valueOf(monthOfYear + 1) + "-"
//                            + String.valueOf(dayOfMonth));
//                }
//            }
//        }, 1990, 1, 1);
//        dialogDate.setButton(DatePickerDialog.BUTTON_POSITIVE, this.getResources().getString(R.string.zxing_button_ok), dialogDate);
//        dialogDate.setButton(DatePickerDialog.BUTTON_NEGATIVE, this.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//                dialogInterface.dismiss();
//            }
//        });
//        dialogDate.getDatePicker().setMaxDate(new Date().getTime());
//        dialogDate.show();

        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        Calendar c = Calendar.getInstance();

        Date myDate = c.getTime();

        int year = c.get(Calendar.YEAR) - 18;
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_YEAR);

        Log.i("datesss", String.valueOf(year));
        new SpinnerDatePickerDialogBuilder()
                .context(getActivity())
                .callback(new com.tsongkha.spinnerdatepicker.DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(com.tsongkha.spinnerdatepicker.DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar calendar = new GregorianCalendar(year, monthOfYear, dayOfMonth);
                        etdob.setText(simpleDateFormat.format(calendar.getTime()));
                    }
                })
                .spinnerTheme(R.style.NumberPickerStyle)
                .maxDate(year, month, day)
                .minDate(1947, 7, 15)

                .build()
                .show();


    }

    private boolean validateDob() {
        if (etdob.getText().toString().trim().isEmpty()) {
            etdob.setError(getResources().getString(R.string.Enter_date_of_birth));
            requestFocus(etdob);
            return false;
        } else {
            etdob.setError(null);
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton(getResources().getString(R.string.agree), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton(getResources().getString(R.string.Dismiss), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    public void successDialog(String resetmsg) {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(resetmsg));
        builder.setPositiveButton(getResources().getString(R.string.agree), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

                Intent Promocode = new Intent(getActivity(), MainActivity.class);
                startActivity(Promocode);
                getActivity().finish();

            }
        });
//        builder.setNegativeButton(getResources().getString(R.string.Dismiss), new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                dialog.dismiss();
//
//            }
//        });
        builder.show();
    }
}
