package in.MadMoveGlobal.fragment.fragmentnaviagtionitems;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

//import in.msewa.adapter.ShoppingAdapter;
//import in.msewa.custom.CustomToast;
import javax.net.ssl.SSLSocketFactory;

import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.model.PQCart;
import in.MadMoveGlobal.model.ShoppingModel;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.util.AddRemoveCartListner;
import in.MadMoveGlobal.util.TLSSocketFactory;

/**
 * Created by Dushant on 08/27/2017.
 */
public class ShoppingFragment extends Fragment implements AddRemoveCartListner {

    private View rootView;
    private GridView gvShopping;
    private TextView tvcart_point;

    private List<ShoppingModel> shoppingArray;
    private RequestQueue rq;

    private String cartValue = "0";

    private PQCart pqCart = PQCart.getInstance();


//    private ShoppingAdapter shoppingAdapter;
    private UserModel session = UserModel.getInstance();

    private Button btnGoCart;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            final SSLSocketFactory sslSocketFactory = new TLSSocketFactory();
            rq = Volley.newRequestQueue(getActivity(), new HurlStack(null, sslSocketFactory));
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
//        Select specificProductQueryGt = Select.from(ShoppingModel.class);
//        shoppingArray = specificProductQueryGt.list();
//        cartValue =  String.valueOf(pqCart.getProductsInCartArray().size());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_shopping, container, false);
        gvShopping = (GridView) rootView.findViewById(R.id.gvShopping);
        btnGoCart = (Button) rootView.findViewById(R.id.btnGoCart);
        tvcart_point = (TextView) rootView.findViewById(R.id.tvshopping_cart_value);
        tvcart_point.setText(cartValue);

        if (shoppingArray != null && shoppingArray.size() != 0) {
//            shoppingAdapter = new ShoppingAdapter(shoppingArray, getActivity(), this);
//            gvShopping.setAdapter(shoppingAdapter);
        }

//        btnGoCart.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (cartValue.equals("0")) {
//                    CustomToast.showMessage(getActivity(), "You have no products in cart.");
//                } else {
//                    Intent goToCartIntent = new Intent(getActivity(), TeleBuyPaymentActivity.class);
//                    startActivity(goToCartIntent);
//                }
//            }
//        });


//        getShoppingItems();
        return rootView;
    }


    private void getShoppingItems() {
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, ApiUrl.URL_GET_ALL_PRODUCTS, (String) null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            shoppingArray.clear();
                            ShoppingModel.deleteAll(ShoppingModel.class);

                            JSONArray productJArray = response.getJSONArray("products");

                            for (int i = 0; i < productJArray.length(); i++) {
                                JSONObject c = productJArray.getJSONObject(i);
                                String pId = c.getString("id");
                                String pImage = c.getString("imagepath");
                                String pDesc = c.getString("desc");
                                String pPrice = c.getString("salevalue");
                                String pMRP = c.getString("mrp");
                                String pDeal = c.getString("deal");
//                                ShoppingModel sModel = new ShoppingModel(pId, pDesc, "", pPrice, pMRP, "http://122.183.176.98/Payqwikweb_web/" + pImage, pDeal);
//                                sModel.save();
//                                shoppingArray.add((sModel));
                            }


                            if (shoppingArray != null && shoppingArray.size() != 0) {
//                                shoppingAdapter = new ShoppingAdapter(shoppingArray, getActivity(), ShoppingFragment.this);
//                                gvShopping.setAdapter(shoppingAdapter);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {

        };
        rq.add(jsonObjReq);

    }

    @Override
    public void taskCompleted(String response) {
        if (response.equals("Add")) {
            cartValue = String.valueOf(Integer.parseInt(cartValue) + 1);
            tvcart_point.setText(cartValue);
        } else {
            cartValue = String.valueOf(Integer.parseInt(cartValue) - 1);
            tvcart_point.setText(cartValue);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        PayQwikApplication.getInstance().trackScreenView("Shopping");
    }


}
