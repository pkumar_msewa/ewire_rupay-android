package in.MadMoveGlobal.fragment.fragmentnaviagtionitems;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.orm.query.Select;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.MadMoveGlobal.EwireRuPay.EkycActivity;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.EwireRuPay.activity.MainActivity;
import in.MadMoveGlobal.custom.CustomAlertDialog;
import in.MadMoveGlobal.custom.CustomBlockDialog;
import in.MadMoveGlobal.custom.CustomIFSCDialogSearch;
import in.MadMoveGlobal.custom.CustomSuccessDialog;
import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.custom.VerifyMPinDialog;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.metadata.AppMetadata;
import in.MadMoveGlobal.model.BankListModel;
import in.MadMoveGlobal.model.IFSCModel;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.util.CheckLog;
import in.MadMoveGlobal.util.IFSCSelectedListener;
import in.MadMoveGlobal.util.MPinVerifiedListner;
import in.MadMoveGlobal.util.NetworkErrorHandler;
import in.MadMoveGlobal.util.PayingDetailsValidation;

/**
 * Created by Dushant on 08/27/2017.
 */


public class DonationBankTransferFragment extends Fragment implements MPinVerifiedListner {
    private View rootView;
    private MaterialEditText Descriptione, transferAmount, transferIFSC, accountHolderName, accountHolderAccountNo;
    private Button btnBankTransfer, btnUpgradeKYC;
    private UserModel session = UserModel.getInstance();
    private LoadingDialog loadDlg;
    private View focusView = null;
    private boolean cancel;
    private String tag_json_obj = "json_bank_transfer";
    private JSONObject jsonRequest;
    private String accHolderName, accNo, accIFSCCode, ammount, description;
    private String Comission;
    private Double totalAmount;
    private CheckBox cbBenifisiary;

    private TextView tvBankTransferInfo;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        onDetach();
        loadDlg = new LoadingDialog(getActivity());

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_donation_bank_transfer, container, false);

        accountHolderName = (MaterialEditText) rootView.findViewById(R.id.etName);
        accountHolderAccountNo = (MaterialEditText) rootView.findViewById(R.id.etBankTransferAccNo);
        transferIFSC = (MaterialEditText) rootView.findViewById(R.id.etBankIFSC);
        transferAmount = (MaterialEditText) rootView.findViewById(R.id.etBankTransferAmount);
        Descriptione = (MaterialEditText) rootView.findViewById(R.id.etDescriptione);
        btnBankTransfer = (Button) rootView.findViewById(R.id.btnBankTransfer);
        cbBenifisiary = (CheckBox) rootView.findViewById(R.id.cbBenifisiary);
        tvBankTransferInfo = (TextView) rootView.findViewById(R.id.tvBankTransferInfo);
        btnUpgradeKYC = (Button) rootView.findViewById(R.id.btnUpgradeKYC);


        onDetach();
        Comission = session.getimpsCommissionAmt();


        if (session.getUserAcName() != null && session.getUserAcName().trim().equals("KYC")) {
            tvBankTransferInfo.setVisibility(View.GONE);
            accountHolderName.setFocusable(true);
            accountHolderAccountNo.setFocusable(true);
            transferIFSC.setFocusable(true);
            transferAmount.setFocusable(true);
            Descriptione.setFocusable(true);
            btnBankTransfer.setClickable(true);

            btnUpgradeKYC.setVisibility(View.GONE);
            btnBankTransfer.setVisibility(View.VISIBLE);


            btnBankTransfer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    attemptPayment();
                }
            });


        } else {
            tvBankTransferInfo.setText(getResources().getString(R.string.bank_transfer_upgrade));
            tvBankTransferInfo.setVisibility(View.VISIBLE);
            btnBankTransfer.setClickable(false);
            btnBankTransfer.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.light_text));
            accountHolderName.setFocusable(false);
            accountHolderAccountNo.setFocusable(false);
            transferIFSC.setFocusable(false);
            transferAmount.setFocusable(false);
            Descriptione.setFocusable(false);
            btnUpgradeKYC.setVisibility(View.VISIBLE);
            btnBankTransfer.setVisibility(View.GONE);


            btnUpgradeKYC.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent Promocode = new Intent(getActivity(), EkycActivity.class);
                    startActivity(Promocode);
                    getActivity().finish();

                }
            });
        }


        return rootView;
    }

    private void attemptPayment() {


        accountHolderName.setError(null);
        accountHolderAccountNo.setError(null);
        transferIFSC.setError(null);
        transferAmount.setError(null);
        Descriptione.setError(null);

        cancel = false;

        accHolderName = accountHolderName.getText().toString();
        accNo = accountHolderAccountNo.getText().toString();
        accIFSCCode = transferIFSC.getText().toString();
        ammount = transferAmount.getText().toString();
        description = Descriptione.getText().toString();

        checkHolderName(accHolderName);
        checkAccNo(accNo);


        if (!validateIFSECode()) {
            return;
        }



        checkPayAmount(ammount);

        if (!validateDescriptione()) {
            return;
        }


        if (cancel) {
            focusView.requestFocus();
        } else {
            showCustomDialog();

        }
    }


    public void showCustomDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title, Html.fromHtml(generateMessage()));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                loadDlg.show();
                promoteBankTransfer();

            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public void showMPinDialog() {
        VerifyMPinDialog builder = new VerifyMPinDialog(getActivity(), this);
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
    }

    public String generateMessage() {
        if (session.isImpsFixed) {
            Double amount = Double.parseDouble(ammount);
            totalAmount = amount + Double.valueOf(Comission);
        } else {


            Double amount = Double.parseDouble(ammount);
            totalAmount = amount + amount * 0.04;
        }


        return "<b><font color=#000000>Acc Holder Name: </font></b>" + "<font color=#000000>" + accHolderName + "</font><br>" +
                "<b><font color=#000000> Receiver Acc No: </font></b>" + "<font>" + accNo + "</font><br>" +
                "<b><font color=#000000> IFSC Code: </font></b>" + "<font>" + accIFSCCode + "</font><br>" +
                "<b><font color=#000000> Transfer Amount : </font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + ammount + "</font><br>" +
                "<b><font color=#000000> Convenience Fee: </font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + Double.valueOf(Comission) + "</font><br>" +
                "<b><font color=#000000> Total Payable Amount: </font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + totalAmount + "</font><br>" +
                "<b><font color=#ff0000> Are you sure you want to proceed?</font></b><br>";
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void checkHolderName(String holderName) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(holderName);
        if (!gasCheckLog.isValid) {
            accountHolderName.setError(getString(gasCheckLog.msg));
            focusView = accountHolderName;
            cancel = true;
        }
    }

    private void checkAccNo(String accNo) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkVijayaBankAc(accNo);
        if (!gasCheckLog.isValid) {
            accountHolderAccountNo.setError(getString(gasCheckLog.msg));
            focusView = accountHolderAccountNo;
            cancel = true;
        }
    }




    private boolean validateDescriptione() {
        if (Descriptione.getText().toString().trim().isEmpty()) {
            Descriptione.setError("this field is required");
            requestFocus(Descriptione);
            return false;
        } else {
            Descriptione.setError(null);
        }

        return true;
    }

    private boolean validateIFSECode() {
        if (transferIFSC.getText().toString().trim().isEmpty() || transferIFSC.getText().toString().trim().length() < 11) {
            transferIFSC.setError("Enter valid IFSC code");
            requestFocus(transferIFSC);
            return false;
        } else {
            transferIFSC.setError(null);
        }

        return true;
    }



    private void checkPayAmount(String amount) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
        if (!gasCheckLog.isValid) {
            transferAmount.setError(getString(gasCheckLog.msg));
            focusView = transferAmount;
            cancel = true;
        } else if (Integer.valueOf(transferAmount.getText().toString()) < 1) {
            transferAmount.setError("Amount should be greater than 1");
            focusView = transferAmount;
            cancel = true;
        } else if (transferAmount.getText().toString().substring(0, 1).equals("0")) {
            transferAmount.setError(getResources().getString(R.string.Amount_should_not_start_with_0));
            focusView = transferAmount;
            cancel = true;
        }

    }


    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    private void sendRefresh() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "1");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    @Override
    public void verifiedCompleted() {
        promoteBankTransfer();
    }

    @Override
    public void sessionInvalid() {
        showInvalidSessionDialog();
    }


    public void promoteBankTransfer() {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("name", accountHolderName.getText().toString());
            jsonRequest.put("accountNo", accountHolderAccountNo.getText().toString());
            jsonRequest.put("ifsc", transferIFSC.getText().toString());
            jsonRequest.put("amount", transferAmount.getText().toString());
            jsonRequest.put("remarks", Descriptione.getText().toString());
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("saveBeneficiary", "false");


        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_BANK_TRANSFER, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.i("Send Money Bank", response.toString());
                    try {
                        String code = response.getString("code");
                        if (code != null && code.equals("S00")) {
                            accountHolderName.getText().clear();
                            accountHolderAccountNo.getText().clear();
                            transferIFSC.getText().clear();
                            transferAmount.getText().clear();
                            Descriptione.getText().clear();
                            sendRefresh();
                            loadDlg.dismiss();
                            showSuccessDialog();
//                            if (response.has("message") && response.getString("message") != null) {
//                                String message = response.getString("message");
//                                CustomToast.showMessage(getActivity(), message);
//                            } else {
//                                CustomToast.showMessage(getActivity(), "Error message is null");
//                            }
                        } else if (code != null && code.equals("F03")) {
                            loadDlg.dismiss();
                            showInvalidSessionDialog();
                        } else if (code != null && code.equals(ApiUrl.C_SESSION_BLOCKED)) {
                            loadDlg.dismiss();
                            showBlockDialog();
                        } else {
                            loadDlg.dismiss();
                            if (response.has("message") && response.getString("message") != null) {
                                String message = response.getString("message");
                                CustomToast.showMessage(getActivity(), message);
                            } else {
                                CustomToast.showMessage(getActivity(), "Error message is null");
                            }
                        }

                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
                    error.printStackTrace();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }

    }


    @Override
    public void onResume() {
        super.onResume();
        PayQwikApplication.getInstance().trackScreenView("Bank transfer");
    }


    public void showSuccessDialog() {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(getSuccessMessage(), Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(getSuccessMessage());
        }
        CustomSuccessDialog builder = new CustomSuccessDialog(getActivity(), "Transferred Successfully", result);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                sendRefresh();
                startActivity(new Intent(getActivity(), MainActivity.class));
            }
        });
        builder.show();
    }

    public String getSuccessMessage() {
        String source =
                "<b><font color=#000000>Acc. No. </font></b>" + "<font color=#000000>" + " :" + accNo + "</font><br>" +
                        "<b><font color=#000000> Amount: </font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + ammount + "</font><br>";
        return source;
    }

    public void showBlockDialog() {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(AppMetadata.getBlockSession(), Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(AppMetadata.getBlockSession());
        }
        CustomBlockDialog builder = new CustomBlockDialog(getActivity(), "Please contact customer care.", result);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
                dialog.dismiss();
                getActivity().finish();
            }
        });
        builder.show();
    }

    @Override
    public void onDetach() {
        super.onDetach();

        //hide keyboard when any fragment of this class has been detached
        showSoftwareKeyboard(false);
    }

    protected void showSoftwareKeyboard(boolean showKeyboard) {
        final Activity activity = getActivity();
        final InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);

//        inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), showKeyboard ? InputMethodManager.SHOW_FORCED : InputMethodManager.HIDE_NOT_ALWAYS);
    }
}
