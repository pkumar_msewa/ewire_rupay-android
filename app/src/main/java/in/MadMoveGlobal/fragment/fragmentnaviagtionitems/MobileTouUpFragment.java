package in.MadMoveGlobal.fragment.fragmentnaviagtionitems;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import in.MadMoveGlobal.fragment.fragmentmobilerecharge.DatacardFragment;
import in.MadMoveGlobal.fragment.fragmentmobilerecharge.PrepaidFragment;
import in.MadMoveGlobal.metadata.AppMetadata;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;

/**
 * Created by Dushant on 08/27/2017.
 */
public class MobileTouUpFragment extends Fragment {
    CharSequence TitlesEnglish[] = null;
    int NumbOfTabs = 1;
    private TabLayout mSlidingTabLayout;
    private ViewPager mainPager;
    private FragmentManager fragmentManager;
    private View rootView;
    private String navType = "";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        onDetach();
        TitlesEnglish = new CharSequence[]{String.valueOf(getResources().getString(R.string.topup_prepaid))/*,  String.valueOf(getResources().getString(R.string.topup_data)*/};
//        fragmentManager = getActivity().getSupportFragmentManager();
        Bundle navBundle = getArguments();
        if (navBundle != null) {
            navType = navBundle.getString(AppMetadata.FRAGMENT_TYPE);
        }
        fragmentManager = getActivity().getSupportFragmentManager();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_mobile_topup, container, false);
        mainPager = (ViewPager) rootView.findViewById(R.id.mainPager);
        mSlidingTabLayout = (TabLayout) rootView.findViewById(R.id.tabLayoutMobileTopUp);
        FragmentManager fragmentManager = getChildFragmentManager();
        mainPager.setAdapter(new ViewPagerAdapter(fragmentManager, TitlesEnglish, NumbOfTabs));
        mSlidingTabLayout.setupWithViewPager(mainPager);

        mSlidingTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mainPager.setCurrentItem(tab.getPosition());


//                if (tab.getPosition() == 0) {

                LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(getActivity());
                Intent i = new Intent("TAG_REFRESH");
                lbm.sendBroadcast(i);

//                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });




        SharedPreferences sharedpreferences = getActivity().getSharedPreferences("prepaidValue", Context.MODE_PRIVATE);
        if (sharedpreferences.getBoolean("prepaid", false)) {
            mainPager.setCurrentItem(0);
            mSlidingTabLayout.getTabAt(0).select();
        }
        else if (sharedpreferences.getBoolean("datacard", false)) {
            mainPager.setCurrentItem(1);
            mSlidingTabLayout.getTabAt(1).select();
        }

        onDetach();
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        PayQwikApplication.getInstance().trackScreenView("Mobile TopUp");
    }

    public class ViewPagerAdapter extends FragmentStatePagerAdapter {

        CharSequence Titles[]; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
        int NumbOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created


        // Build a Constructor and assign the passed Values to appropriate values in the class
        public ViewPagerAdapter(FragmentManager fm, CharSequence mTitles[], int mNumbOfTabsumb) {
            super(fm);
            this.Titles = mTitles;
            this.NumbOfTabs = mNumbOfTabsumb;

        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                onDetach();
                PrepaidFragment prepaidFragment = new PrepaidFragment();
                return prepaidFragment;

            } else {
                onDetach();
                return new DatacardFragment();
            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return Titles[position];
        }

        @Override
        public int getCount() {
            return NumbOfTabs;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}

