package in.MadMoveGlobal.fragment;


public interface IAuthenticateListener {

    void onAuthenticate(String decryptPassword);
}

