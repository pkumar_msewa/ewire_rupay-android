package in.MadMoveGlobal.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;

import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.fragment.fragmenttravel.BusTravelActivity;
import in.MadMoveGlobal.EwireRuPay.R;

/**
 * Created by acer on 10-10-2017.
 */

public class TravelsFragment extends Fragment {

    private View rootView;
    private ImageButton ibBus, ibFlight,ibHotel,ibCab;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        onDetach();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_travels, container, false);
onDetach();
        ibBus= (ImageButton) rootView.findViewById(R.id.ibBus);
        ibFlight= (ImageButton) rootView.findViewById(R.id.ibFlight);
        ibHotel= (ImageButton) rootView.findViewById(R.id.ibHotel);


        ibBus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                startActivity(new Intent(getActivity(), BusTravelActivity.class));
                Intent bustravel = new Intent(getActivity(), BusTravelActivity.class);
                startActivity(bustravel);
//                Log.i("TAGGG",bustravel.toString());
            }
        });

        ibFlight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                startActivity(new Intent(getActivity(), FlightTravelActivity.class));
                CustomToast.showMessage(getActivity(), "COMING SOON");
//                Intent flightravel = new Intent(getActivity(), FlightTravelActivity.class);
//                startActivity(flightravel);
            }
        });


        ibHotel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CustomToast.showMessage(getActivity(), "COMING SOON");


//                Intent hoteltravel = new Intent(getActivity(), HotelTravelActivity.class);
//                startActivity(hoteltravel);
            }
        });


        return rootView;
    }
    @Override
    public void onDetach() {
        super.onDetach();

        //hide keyboard when any fragment of this class has been detached
        showSoftwareKeyboard(false);
    }

    protected void showSoftwareKeyboard(boolean showKeyboard){
        final Activity activity = getActivity();
        final InputMethodManager inputManager = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);

        inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), showKeyboard ? InputMethodManager.SHOW_FORCED : InputMethodManager.HIDE_NOT_ALWAYS);
    }

}
