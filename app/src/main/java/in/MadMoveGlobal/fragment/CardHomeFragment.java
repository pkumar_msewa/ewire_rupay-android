package in.MadMoveGlobal.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.MadMoveGlobal.adapter.ReceiptHestoryAdapter;
import in.MadMoveGlobal.custom.CirclePageIndicator;
import in.MadMoveGlobal.custom.CustomAlertDialog;
import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.custom.LoadMoreListView;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.metadata.AppMetadata;
import in.MadMoveGlobal.model.StatementModel;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.util.NetworkErrorHandler;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;


/**
 * Created by acer on 11-09-2017.
 */

public class CardHomeFragment extends Fragment implements ViewPager.OnPageChangeListener {


    private View rootView;
    private ReceiptHestoryAdapter receiptAdapter;
    private LoadMoreListView lvReceipt;
    private JSONObject jsonRequest;
    private ArrayList<StatementModel> receiptList;
    private ProgressBar pbReceipt;
    private LinearLayout llNoReceipt;
    private TextView tvNoReceipt;
    //Volley
    private String tag_json_obj = "json_receipt";
    private JsonObjectRequest postReq;

    //Pagination
    private int currentPage = 0;
    private int totalPage = 0;

    private UserModel session = UserModel.getInstance();

    private ImageView ivPrv, ivCenter, ivNext;

    private int currentImage = 0;


    //image slider
    private static int currentimagePage = 0;
    private static int NUM_PAGES = 0;
    private int[] IMAGES;
    public ViewPager mPager;
    public CirclePageIndicator indicator;

    //Music Dailog
    SharedPreferences musicPreferences;
    boolean ShowMusicDailog;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        receiptList = new ArrayList<>();
        musicPreferences = getActivity().getSharedPreferences("ShowMusicDailog", Context.MODE_PRIVATE);
        ShowMusicDailog = musicPreferences.getBoolean("ShowMusicDailog", false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_card_home, container, false);
        showSoftwareKeyboard(false);
//        InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//        inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
//        ivCenter = (ImageView) rootView.findViewById(R.id.ivCenter);
//        ivPrv = (ImageView) rootView.findViewById(R.id.ivPrv);
//        ivNext = (ImageView) rootView.findViewById(R.id.ivNext);
        mPager = (ViewPager) rootView.findViewById(R.id.pagerHome);
        indicator = (CirclePageIndicator) rootView.findViewById(R.id.productHeaderImageSlider);
        //Just set one Click listener for the image


//        ivPrv.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                currentImage++;
//                ivPrv.setVisibility(View.VISIBLE);
//
//                Animation slide_left_in = AnimationUtils.loadAnimation(getActivity(), R.anim.left_to_right_animation);
//                ivCenter.startAnimation(slide_left_in);
//                currentImage = currentImage % images.length;
//                ivCenter.setImageResource(images[currentImage]);
//            }
//        });
//
//
        lvReceipt = (LoadMoreListView) rootView.findViewById(R.id.lvReceipt);
        pbReceipt = (ProgressBar) rootView.findViewById(R.id.pbReceipt);
        tvNoReceipt = (TextView) rootView.findViewById(R.id.tvNoReceipt);
        llNoReceipt = (LinearLayout) rootView.findViewById(R.id.llNoReceipt);
        llNoReceipt.setVisibility(View.GONE);
//        ivNext.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                currentImage++;
//                ivNext.setVisibility(View.VISIBLE);
//
//                Animation slide_right_in = AnimationUtils.loadAnimation(getActivity(), R.anim.right_to_left_animation);
//                ivCenter.startAnimation(slide_right_in);
//                currentImage = currentImage % images.length;
//                ivCenter.setImageResource(images[currentImage]);
//            }
//        });


//        {
//
//            @Override
//            public void onPageSelected(int position) {
//                currentPage = position;
//
//            }
//
//            @Override
//            public void onPageScrolled(int pos, float arg1, int arg2) {
//
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int pos) {
//
//            }
//        });
//    }


//        loadUserStatement();

//        lvReceipt.setOnLoadMoreListener(new LoadMoreListView.OnLoadMoreListener() {
//            @Override
//            public void onLoadMore() {
//                if (currentPage < totalPage) {
////                    loadMoreUserStatement();
//                } else {
////                    lvReceipt.onLoadMoreComplete();
//                }
//            }
//        });


        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
//        loadUserStatement();

    }

    public void loadUserStatement() {
        pbReceipt.setVisibility(View.VISIBLE);
        lvReceipt.setVisibility(View.GONE);
        jsonRequest = new JSONObject();
        try {

            jsonRequest.put("sessionId", session.getUserSessionId());


        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            Log.i("Receipt Url", ApiUrl.URL_RECEIPT);
            postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_RECEIPT, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject JsonObj) {
                    try {

                        Log.i("Receipts Response", JsonObj.toString());
                        String message = JsonObj.getString("message");
                        String code = JsonObj.getString("code");

                        if (code != null && code.equals("S00")) {
                            sendRefresh();

                            String details = JsonObj.getString("details");
                            JSONObject jsonDetails = new JSONObject(details);
//                            if (totalElements == 0) {
                            llNoReceipt.setVisibility(View.VISIBLE);
//                                pbReceipt.setVisibility(View.GONE);
//                                lvReceipt.setVisibility(View.GONE);


                            JSONArray operatorArray = jsonDetails.getJSONArray("transactions");

                            for (int i = 0; i < operatorArray.length(); i++) {
                                JSONObject c = operatorArray.getJSONObject(i);
                                String indicator = c.getString("indicator");
                                String date = c.getString("date");
                                String amount = c.getString("amount");
                                String description = c.getString("description");
                                String status = c.getString("status");


                                String refNo = "";


                                String authNo = "";


                                if (!refNo.equalsIgnoreCase("")) {

                                    StatementModel statementModel = new StatementModel(indicator, date, amount, description, status, refNo, authNo);
                                    receiptList.add(statementModel);
                                }

//                                        }
                            }


                        } else if (code != null && code.equals("F03")) {
                            showInvalidSessionDialog();
                        } else {
//                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                        lvReceipt.onLoadMoreComplete();

                    } catch (JSONException e) {
                        e.printStackTrace();
                        lvReceipt.onLoadMoreComplete();
                        CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
                    lvReceipt.onLoadMoreComplete();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }

    }

    public void loadMoreUserStatement() {
        currentPage = currentPage + 1;
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            Log.i("Receipt Url", ApiUrl.URL_RECEIPT);
            postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_RECEIPT, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject JsonObj) {
                    try {

                        Log.i("Receipts Response", JsonObj.toString());
                        String message = JsonObj.getString("message");
                        String code = JsonObj.getString("code");

                        if (code != null && code.equals("S00")) {
                            sendRefresh();

                            String details = JsonObj.getString("details");
                            JSONObject jsonDetails = new JSONObject(details);
//                            if (totalElements == 0) {
                            llNoReceipt.setVisibility(View.VISIBLE);
//                                pbReceipt.setVisibility(View.GONE);
//                                lvReceipt.setVisibility(View.GONE);


                            JSONArray operatorArray = jsonDetails.getJSONArray("transactions");

                            for (int i = 0; i < operatorArray.length(); i++) {
                                JSONObject c = operatorArray.getJSONObject(i);
                                String indicator = c.getString("indicator");
                                String date = c.getString("date");
                                String amount = c.getString("amount");
                                String description = c.getString("description");
                                String status = c.getString("status");


                                String refNo = "";


                                String authNo = "";


                                if (!refNo.equalsIgnoreCase("")) {

                                    StatementModel statementModel = new StatementModel(indicator, date, amount, description, status, refNo, authNo);
                                    receiptList.add(statementModel);
                                }

//                                        }
                            }


                        } else if (code != null && code.equals("F03")) {
                            showInvalidSessionDialog();
                        } else {
//                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                        lvReceipt.onLoadMoreComplete();

                    } catch (JSONException e) {
                        e.printStackTrace();
                        lvReceipt.onLoadMoreComplete();
                        CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
                    lvReceipt.onLoadMoreComplete();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }


    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                dialog.dismiss();
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    private void sendRefresh() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "1");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    @Override
    public void onDetach() {
        super.onDetach();

        //hide keyboard when any fragment of this class has been detached
        showSoftwareKeyboard(false);
    }

    protected void showSoftwareKeyboard(boolean showKeyboard) {
        final Activity activity = getActivity();
        final InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);

        inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), showKeyboard ? InputMethodManager.SHOW_FORCED : InputMethodManager.HIDE_NOT_ALWAYS);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        showSoftwareKeyboard(false);

    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}


