//package in.payqwik.fragment.fragmenttelebuy;
//
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentManager;
//import android.support.v4.app.FragmentTransaction;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.AdapterView;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.Spinner;
//import android.widget.TextView;
//
//import com.android.volley.Request;
//import com.android.volley.RequestQueue;
//import com.android.volley.Response;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.JsonObjectRequest;
//import com.android.volley.toolbox.Volley;
//
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.io.UnsupportedEncodingException;
//import java.net.URLEncoder;
//
//import in.msewa.custom.CustomToast;
//import in.msewa.custom.LoadingDialog;
//import in.msewa.custom.TelebuyAddressAdapter;
//import in.msewa.metadata.ApiUrl;
//import in.msewa.model.AddressEntityModel;
//import in.msewa.model.AddressModel;
//import in.msewa.model.PQTelebuyPaymentHolder;
//import in.msewa.model.UserModel;
//import in.msewa.test.R;
//import in.msewa.util.CheckLog;
//import in.msewa.util.PayingDetailsValidation;
//
///**
// * Created by Ksf on 4/6/2016.
// */
//public class TeleBuyDeleveryFragmentOld extends Fragment {
//
//    private UserModel session = UserModel.getInstance();
//    private AddressModel address = new AddressModel();
//
//    private View rootView;
//    private Button btndeliveryProduct;
//    private EditText etdeliveryName, etdeliveryAddressOne, etdeliveryAddressTwo, etdeliveryAddressThree,etdeliveryPinCode, etdeliveryMobile;
//    private Boolean cancel;
//    private View focusView = null;
//    private FragmentManager fragmentManager;
//    private FragmentTransaction fragmentTransaction;
//    private TextView tvDescriptions, tvDetails;
//
//    private Spinner spcaState;
//    private Spinner spcaCity;
//
//    private boolean isPrimaryAddressPresent;
//    PQTelebuyPaymentHolder payTelebuy = PQTelebuyPaymentHolder.getInstance();
//
//    private String contactName;
//    private String contactAddressId;
//    private String contactAddress3;
//    private String contactAddress2;
//    private String contactAddress1;
//    private String contactStateId;
//    private String contactState;
//    private String contactCityId;
//    private String contactCity;
//    private String contactZipCode;
//    private String contactPhone;
//
//
//
//    private String eContactName;
//    private String eContactAddress3;
//    private String eContactAddress2;
//    private String eContactAddress1;
//    private String eContactCity;
//    private String eContactZipCode;
//    private String eContactPhone;
//    private String eContactState;
//
//    private String url;
//
//    private AddressEntityModel selectedState;
//    private AddressEntityModel selectedCity;
//    private double totalAmount;
//
//    private LoadingDialog loadingDialog;
//
//    private RequestQueue rq;
//
//    private boolean isFirst = true;
//
//
//    @Override
//    public void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        rq = Volley.newRequestQueue(getActivity());
//        loadingDialog = new LoadingDialog(getActivity());
//    }
//
//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        rootView = inflater.inflate(R.layout.fragment_incart_delivery,container,false);
//        btndeliveryProduct = (Button) rootView.findViewById(R.id.btn_deleivery_ok);
//        etdeliveryAddressOne = (EditText) rootView.findViewById(R.id.edt_delivery_address1);
//        etdeliveryAddressTwo = (EditText) rootView.findViewById(R.id.edt_delivery_address2);
//        etdeliveryAddressThree = (EditText) rootView.findViewById(R.id.edt_delivery_address3);
//        etdeliveryMobile = (EditText) rootView.findViewById(R.id.edt_delivery_mobile_number);
//        etdeliveryPinCode = (EditText) rootView.findViewById(R.id.edt_deleivery_pin_code);
//        etdeliveryName = (EditText) rootView.findViewById(R.id.edt_delivery_full_name);
//        spcaState = (Spinner) rootView.findViewById(R.id.spcaState);
//        spcaCity = (Spinner) rootView.findViewById(R.id.spcaCity);
//        contactPhone = session.getUserMobileNo();
//        contactName = session.getUserFirstName()+" " +session.getUserLastName();
//        etdeliveryMobile.setText(contactPhone);
//        etdeliveryName.setText(contactName);
//
//        getShoppingAddress();
//
//
//        spcaState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                selectedState = address.getStateList().get(position);
//                final String stateId = selectedState.getId();
//                contactStateId = selectedState.getId();
//                if(!isFirst)
//                    refreshCity(stateId);
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
//
//
//        spcaCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                selectedCity= address.getCityList().get(position);
//                contactCityId= selectedCity.getId();
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
//        btndeliveryProduct.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                attemptPayment();
//            }
//        });
//
//        return rootView;
//    }
//
//    private void getShoppingAddress() {
//        loadingDialog.show();
//        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, ApiUrl.URL_GET_ADDRESS+session.getUserMobileNo(), (String)null,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        try {
//
//                            String responseCode = response.getString("response_code");
//                            if (responseCode.equals("0")) {
//                                JSONArray contactAddressArray = response.getJSONArray("contactaddress");
//                                if (contactAddressArray.length() != 0) {
//                                    isPrimaryAddressPresent = true;
//                                    JSONObject contactAddress = (JSONObject) contactAddressArray.get(contactAddressArray.length()-1);
//                                    contactAddressId = contactAddress.getString("id");
//                                    contactAddress3 = contactAddress.getString("add3");
//                                    contactAddress2 = contactAddress.getString("add2");
//                                    contactAddress1 = contactAddress.getString("add1");
//                                    contactStateId = contactAddress.getString("stateid");
//                                    contactState = contactAddress.getString("statedesc");
//
//                                    contactCityId = contactAddress.getString("cityid");
//                                    contactCity = contactAddress.getString("citydesc");
//                                    contactZipCode = contactAddress.getString("pincode");
//
//                                    etdeliveryAddressThree.setText(contactAddress3);
//                                    etdeliveryAddressTwo.setText(contactAddress2);
//                                    etdeliveryAddressOne.setText(contactAddress1);
//                                    etdeliveryPinCode.setText(contactZipCode);
//                                    getState();
//
//                                } else {
//                                    loadingDialog.dismiss();
//                                    isPrimaryAddressPresent = false;
//                                    getState();
//
//
//                                }
//                            }
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                            loadingDialog.dismiss();
//                            CustomToast.showMessage(getActivity(),"Error occurred while fetching address");
//                        }
//
//                    }
//                }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//            }
//        }) {
//
//        };
//        rq.add(jsonObjReq);
//    }
//
//    private void getState() {
//        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, ApiUrl.URL_GET_STATE, (String)null,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        if(response!=null){
//                            address.createStateMap(response.toString(), AddressModel.COUNTRY_ID);
//                        }
//                        if (getActivity() != null) {
//                            TelebuyAddressAdapter dataAdapter = new TelebuyAddressAdapter(getActivity(), android.R.layout.simple_spinner_item, address.getStateList());
//                            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//                            spcaState.setAdapter(dataAdapter);
//
//                            if (address.getStateMap() != null && address.getStateMap().get(contactStateId) != null) {
//                                spcaState.setSelection(address.getStateMap().get(contactStateId).getIndex());
//                                getCity();
//                            }
//
//                        }
//                    }
//                }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                loadingDialog.dismiss();
//                CustomToast.showMessage(getActivity(),"Error occurred while fetching state");
//            }
//        }) {
//
//        };
//        rq.add(jsonObjReq);
//    }
//
//    private void getCity() {
//        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, ApiUrl.URL_GET_CITY+contactStateId, (String)null,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//
//                        if(response!=null){
//                            address.createCityMap(response.toString(), contactStateId);
//                        }
//                        if (getActivity() != null) {
//                            TelebuyAddressAdapter dataAdapter = new TelebuyAddressAdapter(getActivity(), android.R.layout.simple_spinner_item, address.getCityList());
//                            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//                            spcaCity.setAdapter(dataAdapter);
//
//                            if (address.getCityMap() != null && address.getCityMap().get(contactCityId) != null) {
//                                spcaCity.setSelection(address.getCityMap().get(contactCityId).getIndex());
//                                isFirst = false;
//                            }
//                        }
//                        loadingDialog.dismiss();
//                    }
//                }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                loadingDialog.dismiss();
//                CustomToast.showMessage(getActivity(),"Error occurred while fetching city");
//            }
//        }) {
//
//        };
//        rq.add(jsonObjReq);
//    }
//
//
//    private void refreshCity(String stateId) {
//        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, ApiUrl.URL_GET_CITY+stateId, (String)null,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//
//                        if(response!=null){
//                            address.createCityMap(response.toString(), contactStateId);
//                        }
//                        if (getActivity() != null) {
//                            TelebuyAddressAdapter dataAdapter = new TelebuyAddressAdapter(getActivity(), android.R.layout.simple_spinner_item, address.getCityList());
//                            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//                            spcaCity.setAdapter(dataAdapter);
//                        }
//                    }
//                }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//            }
//        }) {
//
//        };
//        rq.add(jsonObjReq);
//    }
//
//
//
//    private void attemptPayment() {
//        etdeliveryMobile.setError(null);
//        etdeliveryName.setError(null);
//        etdeliveryAddressOne.setError(null);
//        etdeliveryAddressTwo.setError(null);
//        etdeliveryAddressThree.setError(null);
//        etdeliveryPinCode.setError(null);
//
//        cancel = false;
//
//
//        checkPin(etdeliveryPinCode.getText().toString());
//        checkMobile(etdeliveryMobile.getText().toString());
//        checkAddress1(etdeliveryAddressOne.getText().toString());
//        checkAddress2(etdeliveryAddressTwo.getText().toString());
//        checkAddress3(etdeliveryAddressThree.getText().toString());
//        checkName(etdeliveryName.getText().toString());
//
//        if (cancel) {
//            focusView.requestFocus();
//        } else {
//            gotoPay();
//        }
//    }
//
//    private void checkMobile(String phNo) {
//        CheckLog gasCheckLog = PayingDetailsValidation.checkMobileTenDigit(phNo);
//        if (!gasCheckLog.isValid) {
//            etdeliveryMobile.setError(getString(gasCheckLog.msg));
//            focusView = etdeliveryMobile;
//            cancel = true;
//        }
//    }
//
//    private void checkPin(String amount) {
//        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
//        if (!gasCheckLog.isValid) {
//            etdeliveryPinCode.setError(getString(gasCheckLog.msg));
//            focusView = etdeliveryPinCode;
//            cancel = true;
//        }
//    }
//
//    private void checkAddress1(String amount) {
//        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
//        if (!gasCheckLog.isValid) {
//            etdeliveryAddressOne.setError(getString(gasCheckLog.msg));
//            focusView = etdeliveryAddressOne;
//            cancel = true;
//        }
//    }
//
//    private void checkName(String amount) {
//        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
//        if (!gasCheckLog.isValid) {
//            etdeliveryName.setError(getString(gasCheckLog.msg));
//            focusView = etdeliveryName;
//            cancel = true;
//        }
//    }
//
//    private void checkAddress2(String amount) {
//        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
//        if (!gasCheckLog.isValid) {
//            etdeliveryAddressTwo.setError(getString(gasCheckLog.msg));
//            focusView = etdeliveryAddressTwo;
//            cancel = true;
//        }
//    }
//    private void checkAddress3(String amount) {
//        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
//        if (!gasCheckLog.isValid) {
//            etdeliveryAddressThree.setError(getString(gasCheckLog.msg));
//            focusView = etdeliveryAddressThree;
//            cancel = true;
//        }
//    }
//
//    private void gotoPay() {
//        if (isPrimaryAddressPresent) {
//            try {
////                eContactName = URLEncoder.encode(contactName, "UTF-8");
////                eContactAddress1= URLEncoder.encode(contactAddress1, "UTF-8");
////                eContactAddress2= URLEncoder.encode(contactAddress2, "UTF-8");
////                eContactAddress3= URLEncoder.encode(contactAddress3, "UTF-8");
////                eContactCity= URLEncoder.encode(contactCity, "UTF-8");
////                eContactPhone= URLEncoder.encode(contactPhone, "UTF-8");
////                eContactState= URLEncoder.encode(contactState, "UTF-8");
////                eContactZipCode= URLEncoder.encode(contactZipCode, "UTF-8");
//
//                eContactName = URLEncoder.encode(etdeliveryName.getText().toString(), "UTF-8");
//                eContactAddress1= URLEncoder.encode(etdeliveryAddressOne.getText().toString(), "UTF-8");
//                eContactAddress2= URLEncoder.encode(etdeliveryAddressTwo.getText().toString(), "UTF-8");
//                eContactAddress3= URLEncoder.encode(etdeliveryAddressThree.getText().toString(), "UTF-8");
//                eContactPhone= URLEncoder.encode(etdeliveryMobile.getText().toString(), "UTF-8");
//
//                eContactCity= URLEncoder.encode(contactCity, "UTF-8");
//                eContactState= URLEncoder.encode(contactState, "UTF-8");
//                eContactZipCode= URLEncoder.encode(etdeliveryPinCode.getText().toString(), "UTF-8");
//
//            } catch (UnsupportedEncodingException e) {
//                e.printStackTrace();
//            }
//
//            url = ApiUrl.URL_TELEBUY_MAIN+"?API=AddContactAddress&CONTNAME="+eContactName+"&PHONE="+session.getUserMobileNo()+"&ADD1="+eContactAddress1+"&ADD2="+eContactAddress2+"&ADD3="+eContactAddress3+"&COUNTRYID=5"+"&COUNTRYDESC=INDIA"+"&STATEID="+contactStateId+"&STATEDESC="+eContactState+"&CITYID="+contactCityId+"&CITYDESC="+eContactCity+"&PINCODE="+eContactZipCode+"&MEDIAID=1&MEDIADESC=ACV&LANGUAGEID=1&LANGUAGEDESC=ENGLISH&PRIMARYCONTACT=Y";
//            payTelebuy.setAddress(contactAddress1 + ", " + contactAddress2 + ", " + contactAddress3);
//            payTelebuy.setName(contactName);
//            payTelebuy.setCity(contactCity);
//            payTelebuy.setState(contactState);
//            payTelebuy.setZip(contactZipCode);
//            payTelebuy.setTelephone(contactPhone);
//            addContact();
//
//        }
//
//        else {
//            try {
////                eContactName = URLEncoder.encode(contactName, "UTF-8");
////                eContactAddress1= URLEncoder.encode(contactAddress1, "UTF-8");
////                eContactAddress2= URLEncoder.encode(contactAddress2, "UTF-8");
////                eContactAddress3= URLEncoder.encode(contactAddress3, "UTF-8");
////                eContactCity= URLEncoder.encode(contactCity, "UTF-8");
////                eContactPhone= URLEncoder.encode(contactPhone, "UTF-8");
////                eContactState= URLEncoder.encode(contactState, "UTF-8");
////                eContactZipCode= URLEncoder.encode(contactZipCode, "UTF-8");
//                eContactName = URLEncoder.encode(etdeliveryName.getText().toString(), "UTF-8");
//                eContactAddress1= URLEncoder.encode(etdeliveryAddressOne.getText().toString(), "UTF-8");
//                eContactAddress2= URLEncoder.encode(etdeliveryAddressTwo.getText().toString(), "UTF-8");
//                eContactAddress3= URLEncoder.encode(etdeliveryAddressThree.getText().toString(), "UTF-8");
//                eContactPhone= URLEncoder.encode(etdeliveryMobile.getText().toString(), "UTF-8");
//
//                eContactCity= URLEncoder.encode(contactCity, "UTF-8");
//                eContactState= URLEncoder.encode(contactState, "UTF-8");
//                eContactZipCode= URLEncoder.encode(etdeliveryPinCode.getText().toString(), "UTF-8");
//            } catch (UnsupportedEncodingException e) {
//                e.printStackTrace();
//            }
//
//            url = ApiUrl.URL_TELEBUY_MAIN+"?API=AddContactDetails&CONTNAME="+eContactName+"&PHONE="+session.getUserMobileNo()+"&ADD1="+eContactAddress1+"&ADD2="+eContactAddress2+"&ADD3="+eContactAddress3+"&COUNTRYID=5"+"&COUNTRYDESC=INDIA"+"&STATEID="+contactStateId+"&STATEDESC="+eContactState+"&CITYID="+contactCityId+"&CITYDESC="+eContactCity+"&PINCODE="+eContactZipCode+"&MEDIAID=1&MEDIADESC=ACV&LANGUAGEID=1&LANGUAGEDESC=ENGLISH&PRIMARYCONTACT=Y";
//
//            payTelebuy.setAddress(contactAddress1 + ", " + contactAddress2 + ", " + contactAddress3);
//            payTelebuy.setName(contactName);
//            payTelebuy.setCity(contactCity);
//            payTelebuy.setState(contactState);
//            payTelebuy.setZip(contactZipCode);
//            payTelebuy.setTelephone(contactPhone);
//            addContact();;
//
//        }
//    }
//
//    public void addContact(){
//        loadingDialog.show();
//        Log.i("Adding Contact Url",url);
//        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, url, (String)null,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//
//                        Log.i("Telebuy Address",response.toString());
//
//                        try {
//                            String responseCode = response.getString("response_code");
//                            if(responseCode.equals("0")){
//                                contactAddressId = response.getString("delvid");
//                                payTelebuy.setContactAddressId(contactAddressId);
//
//                                fragmentManager = getFragmentManager();
//                                fragmentTransaction = fragmentManager.beginTransaction();
//                                fragmentTransaction.addToBackStack(getTag());
//                                loadingDialog.dismiss();
//
//                                Fragment secureShoppingfragment = new TeleBuyPaymentFragmentOld();
//                                Bundle args = new Bundle();
//                                args.putDouble("total_amount", totalAmount);
//                                secureShoppingfragment.setArguments(args);
//                                fragmentTransaction.replace(R.id.frameInCart, secureShoppingfragment);
//                                fragmentTransaction.commit();
//
//                            }
//                            else{
//                                payTelebuy.setContactAddressId("0");
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//            }
//        }) {
//
//        };
//        rq.add(jsonObjReq);
//    }
//}
