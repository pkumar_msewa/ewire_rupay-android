package in.MadMoveGlobal.fragment.fragmentgiftcard;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import in.MadMoveGlobal.model.GiftCardCatModel;
import in.MadMoveGlobal.EwireRuPay.R;


//import in.payqwik.payqwik.activity.AdventureActivity.VideoPlayerActivity;

/**
 * Created by Dushant on 08/27/2017.
 */

public class DescriptionFragment extends Fragment {

    private View rootView;

    private TextView tvDescription;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        rootView = inflater.inflate(R.layout.fragment_description, container, false);
        Bundle bundle = getArguments();
        GiftCardCatModel giftCardCatModel = bundle.getParcelable("model");
        tvDescription = (TextView) rootView.findViewById(R.id.tvDescription);
        tvDescription.setText(Html.fromHtml(giftCardCatModel.getTnc_mobile()));

//        Bundle args = getArguments();
//        //String categoryId = args.getString("index");
//
//        String categoryDS = getArguments().getString("Model");
//        //String categoryId = getArguments().getString("category_id");
//        TextView textView = (TextView) rootView.findViewById(R.id.label);
//        tvDescription.setText(categoryDS);




        return rootView;
    }
}
