package in.MadMoveGlobal.fragment.fragmentgiftcard;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import com.androidquery.AQuery;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import in.MadMoveGlobal.adapter.AmountAdapter;
import in.MadMoveGlobal.adapter.GiftCardItemAdapter;
import in.MadMoveGlobal.custom.AESCrypt;
import in.MadMoveGlobal.custom.CustomAlertDialog;
import in.MadMoveGlobal.custom.CustomSuccessDialog;
import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.metadata.AppMetadata;
import in.MadMoveGlobal.model.GiftCardCatModel;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.EwireRuPay.WebViewActivity;
import in.MadMoveGlobal.EwireRuPay.activity.MainActivity;
import in.MadMoveGlobal.util.AddRemoveCartListner;
import in.MadMoveGlobal.util.NetworkErrorHandler;


//import in.payqwik.payqwik.activity.AdventureActivity.VideoPlayerActivity;

/**
 * Created by Dushant on 08/27/2017.
 */

public class PurchaseFragment extends Fragment implements View.OnClickListener {

    private View rootView;

    private EditText etFname, etLname, etEmail, etMobile, etStreet, etCity, etRegion, etDistrict, etPostalCode;
    private Button btnpayment;
    private TextView tvdenName;
    private TextInputLayout ilRegName, ilRegMobile, ilRegEmail, ilmsg;
    private String Fname, Lname, Email, Mobile, Street, City, Region, District, PostalCode;

    private Toolbar toolbar;
    private ImageButton ivBackBtn;
    private LoadingDialog loadDlg;
    private RecyclerView rvGiftCarditem;
    private LinearLayout llpbGiftCarditem;
    private String itemhash, itemImage, brandName;
    private UserModel session = UserModel.getInstance();
    private String tag_json_obj = "json_events";
    private JsonObjectRequest postReq;
    private JSONObject jsonRequests;
    private AQuery aq;


    private GiftCardItemAdapter itemAdp;
    private GiftCardCatModel giftCardCatModel;
    private RecyclerView llAmount;

    private final Pattern hasUppercase = Pattern.compile("[A-Z]");
    private final Pattern hasLowercase = Pattern.compile("[a-z]");
    private final Pattern hasNumber = Pattern.compile("\\d");
    private final Pattern hasSpecialChar = Pattern.compile("[^a-zA-Z0-9 ]");
    Calendar cl = Calendar.getInstance();

    private HashMap<String, String> hashMap;

    private static String Ammount;
    int postion;
    private TextView[] myTextViews;
    private Button[] button;
    private AmountAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;
    private TextView tvExtraCharge;
    private JSONObject jsonRequest;
    private GiftCardCatModel model;
    private UserModel userModel = UserModel.getInstance();


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        rootView = inflater.inflate(R.layout.fragment_purchases, container, false);
        hashMap = new HashMap<>();
        etFname = (EditText) rootView.findViewById(R.id.etFName);
        etLname = (EditText) rootView.findViewById(R.id.etLName);
        etEmail = (EditText) rootView.findViewById(R.id.etEmail);
        etMobile = (EditText) rootView.findViewById(R.id.etMobile);
        etStreet = (EditText) rootView.findViewById(R.id.etStreet);
        etCity = (EditText) rootView.findViewById(R.id.etCity);
        etRegion = (EditText) rootView.findViewById(R.id.etRegion);
        etDistrict = (EditText) rootView.findViewById(R.id.etDist);
        etPostalCode = (EditText) rootView.findViewById(R.id.etPincode);
        tvExtraCharge = (TextView) rootView.findViewById(R.id.tvExtraCharge);

        //tvdenName = (TextView) rootView.findViewById(R.id.tvdenName);

        btnpayment = (Button) rootView.findViewById(R.id.btnRegPay);
        llAmount = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        llAmount.setHasFixedSize(true);

        // The number of Columns
        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        llAmount.setLayoutManager(mLayoutManager);
        loadDlg = new LoadingDialog((getActivity()));

        rvGiftCarditem = (RecyclerView) rootView.findViewById(R.id.rvGiftCarditem);
        llpbGiftCarditem = (LinearLayout) rootView.findViewById(R.id.llpbGiftCarditem);
        Bundle bundle = getArguments();
        giftCardCatModel = bundle.getParcelable("model");
        assert giftCardCatModel != null;
        if (giftCardCatModel.getCustom_denominations() != null && !giftCardCatModel.getCustom_denominations().isEmpty()) {
            String[] amounts = giftCardCatModel.getCustom_denominations().split(",");
            mAdapter = new AmountAdapter(getActivity(), amounts, new AddRemoveCartListner() {
                @Override
                public void taskCompleted(String response) {
                    Ammount = response;
                }
            });
            llAmount.setAdapter(mAdapter);
        }

        getCardCategory();
//        Button btnRegPay = (Button) rootView.findViewById(R.id.btnRegPay);
        btnpayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitForm();
            }
        });
        return rootView;
    }

    private void submitForm() {

        if (!validateFName()) {
            return;
        }
        if (!validateLName()) {
            return;
        }
        if (!validateEmail()) {
            return;
        }
        if (!validateMobile()) {
            return;
        }
        if (!validateStreet()) {
            return;
        }
        if (!validateRegion()) {
            return;
        }
        if (!validateCity()) {
            return;
        }
        if (!validateDistrict()) {
            return;
        }
        if (!validatePostel()) {
            return;
        }


//        loadDlg.show();
//
        Fname = etFname.getText().toString();
        Lname = etLname.getText().toString();
        Email = etEmail.getText().toString();
        Mobile = etMobile.getText().toString();
        Street = etStreet.getText().toString();
        City = etCity.getText().toString();
        Region = etRegion.getText().toString();
        District = etDistrict.getText().toString();
        PostalCode = etPostalCode.getText().toString();


//        address = etRegMobile.getText().toString();
       /* city = etCity.getText().toString();
        state = etState.getText().toString();
        country = etCountry.getText().toString();*/


//        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
//        date = df.format(cl.getTime());
//
//        Log.i("DATE", "DATE : " + date);
//        Log.i("HAsoMap", hashMap.toString());
//        if (Ammount != null && !Ammount.toString().isEmpty()) {
//            String amout = Ammount.replace("₹", "");
//            Log.i("amout", amout.trim());
//            try {
//                JSONObject jsonObject = new JSONObject(hashMap);
//                promotePay(jsonObject.get(amout.trim()).toString());
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//            String s = null;
//            for (String key : hashMap.keySet()) {
//                if (key.equalsIgnoreCase(amout)) {
//                    s = hashMap.get(key);

//
//                }



//            }
//            if(s!=null){
//                Log.i("amout1",s);
//                promotePay(s);
//            }
//            Log.i("gaspMap", hashMap.get(amout).toString());
//
//        } else {
//            CustomToast.showMessage(getActivity(), "please select Amount");
//        }

        promotePay();

    }


    private boolean validateFName() {
        if (etFname.getText().toString().trim().isEmpty()) {
            etFname.setError("Enter First Name");
            requestFocus(etFname);
            return false;
        } else {
            etFname.setError(null);
        }

        return true;
    }

    private boolean validateLName() {
        if (etLname.getText().toString().trim().isEmpty()) {
            etLname.setError("Enter Last Name");
            requestFocus(etLname);
            return false;
        } else {
            etLname.setError(null);
        }

        return true;
    }

    private boolean validateEmail() {
        String email = etEmail.getText().toString().trim();
        if (email.isEmpty() || !isValidEmail(email)) {
            etEmail.setError("Enter valid email");
            requestFocus(etEmail);
            return false;
        } else {
            etEmail.setError(null);
        }

        return true;
    }

    private boolean validateMobile() {
        if (etMobile.getText().toString().trim().isEmpty() || etMobile.getText().toString().trim().length() < 10) {
            etMobile.setError("Enter 10 digit mobile no");
            requestFocus(etMobile);
            return false;
        } else {
            etMobile.setError(null);
        }

        return true;
    }

    private boolean validateStreet() {
        if (etStreet.getText().toString().trim().isEmpty()) {
            etStreet.setError("Enter Street Name");
            requestFocus(etStreet);
            return false;
        } else {
            etStreet.setError(null);
        }

        return true;
    }

    private boolean validateCity() {
        if (etCity.getText().toString().trim().isEmpty()) {
            etCity.setError("Enter City Name");
            requestFocus(etCity);
            return false;
        } else {
            etCity.setError(null);
        }

        return true;
    }

    private boolean validateRegion() {
        if (etRegion.getText().toString().trim().isEmpty()) {
            etRegion.setError("Enter Region Name");
            requestFocus(etRegion);
            return false;
        } else {
            etRegion.setError(null);
        }

        return true;
    }


    private boolean validateDistrict() {
        if (etDistrict.getText().toString().trim().isEmpty()) {
            etDistrict.setError("Enter City Name");
            requestFocus(etDistrict);
            return false;
        } else {
            etDistrict.setError(null);
        }

        return true;
    }

    private boolean validatePostel() {
        if (etPostalCode.getText().toString().trim().isEmpty() || etPostalCode.getText().toString().trim().length() < 6) {
            etPostalCode.setError("Enter 10 digit mobile no");
            requestFocus(etPostalCode);
            return false;
        } else {
            etPostalCode.setError(null);
        }

        return true;
    }


    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }


    public void getCardCategory() {

        try {
            loadDlg.show();
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(giftCardCatModel.getProduct_id());

            jsonRequest = new JSONObject();
            jsonRequest.put("sessionId", userModel.getUserSessionId());
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("productIds", jsonArray);

            jsonRequest.put("data", AESCrypt.encrypt(jsonObject.toString().trim()));

        } catch (JSONException e) {
            e.printStackTrace();
            loadDlg.dismiss();
        }  catch (Exception e) {
            e.printStackTrace();
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            final JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_WOOHOO_RECHECKPRICE, jsonRequest, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject jsonObj) {
                    loadDlg.dismiss();
                    try {
                        String code = jsonObj.getString("success");
                        String message = jsonObj.getString("message");

                        loadDlg.dismiss();
                        if (code != null && code.equals("true")) {
                            if (!jsonObj.isNull("handling_amount")) {
                                tvExtraCharge.setText(getActivity().getResources().getString(R.string.rupease) + " " + jsonObj.getString("handling_amount"));
                            }


                        } else {
                            loadDlg.dismiss();
                            if (message.equalsIgnoreCase("Please login and try again.")) {
                                showInvalidSessionDialog();

                            } else if (code.equalsIgnoreCase("F03")) {
                                showInvalidSessionDialog();
                            } else {
                                CustomToast.showMessage(getActivity(), message);
                            }
                        }


                    } catch (
                            JSONException e)

                    {
                        loadDlg.dismiss();
                        e.printStackTrace();
                        Toast.makeText(getActivity(), "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
                    }
                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.i("Type", "ERro");
                    CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
                    loadDlg.dismiss();


                }
            })
//                {
//                @Override
//                public Map<String, String> getHeaders() throws AuthFailureError {
//                    HashMap<String, String> map = new HashMap<>();
//                    map.put("hash", "12345");
//                    return map;
//                }

//            }
                    ;
            int socketTimeout = 60000 * 5;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }


    private void promotePay() {
        loadDlg.show();


        jsonRequests = new JSONObject();
        JSONObject jsonObject = new JSONObject();
        try {
            Log.i("amountssss", Ammount);

//            String[] demo = s.split(",");
            jsonObject.put("firstname",Fname );
            jsonObject.put("lastname",Lname);
            jsonObject.put("email",Email );
            jsonObject.put("telephone",Mobile );
            jsonObject.put("line_1",Street );
            jsonObject.put("line_2",City );
            jsonObject.put("city", District);
            jsonObject.put("region",Region );
            jsonObject.put("country_id", "In");
            jsonObject.put("postcode", PostalCode);
            jsonObject.put("product_id", giftCardCatModel.getProduct_id());
            jsonObject.put("price", Ammount.replace("₹", "").trim());
            jsonObject.put("qty", "1");

            jsonRequests.put("data", AESCrypt.encrypt(StringEscapeUtils.unescapeJson(jsonObject.toString()).replaceAll("\\n", "")).replaceAll("\\\\/", ""));
            jsonRequests.put("sessionId", userModel.getUserSessionId());

            Log.i("NAME", giftCardCatModel.getProduct_id() + " " + Mobile + " " + " " + Email);


//            jsonRequests.put("clientOrderId", ServiceUtility.createOrderId());
//            jsonRequests.put("brandHash", giftCardCatModel.getCardHash());
//            jsonRequests.put("sessionId", session.getUserSessionId());
//            jsonRequests.put("productType", demo[0]);
//            jsonRequests.put("skuId", demo[1]);

//            jsonRequests.put("denomination", price);
//            jsonRequests.put("productType", "digital");
//            jsonRequests.put("quantity", quantity);

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequests = null;
        }catch (Exception e) {
            e.printStackTrace();
        }

        if (jsonRequests != null) {
            Log.i("CardRequest", jsonRequests.toString());
            Log.i("Registration API", ApiUrl.URL_WOOHOO_PAYMENT);
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_WOOHOO_PAYMENT, jsonRequests, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.i("Registration Response", response.toString());
                        String messageAPI = response.getString("message");
                        String code = response.getString("code");
                        loadDlg.dismiss();
                        if (code != null && code.equals("T01")) {
                            Intent mainMenuDetailActivity = new Intent(getActivity(), WebViewActivity.class);
                            mainMenuDetailActivity.putExtra("TYPE", "GIFTCARD");
                            mainMenuDetailActivity.putExtra("SUBTYPE", "");
                            mainMenuDetailActivity.putExtra("splitAmount", Ammount.replace("₹", "").trim());
                            mainMenuDetailActivity.putExtra("URL", ApiUrl.URL_GIFT_CARD_ORDER);
                            mainMenuDetailActivity.putExtra("REQUESTOBJECT", jsonRequests.toString());
                            startActivity(mainMenuDetailActivity);
                        }

                        else if (code != null && code.equals("S00")) {
                            showSuccessDialog("", "Congratulations Order Successful");


                        } else if (code != null && code.equals("F03")) {
                            loadDlg.dismiss();
                            showInvalidSessionDialog();
                        } else {
                            loadDlg.dismiss();
                            if (messageAPI.equalsIgnoreCase("Please login and try again.")) {
                                showInvalidSessionDialog();

                            } else {
                                CustomToast.showMessage(getActivity(), messageAPI);
                            }
                        }
                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
                        Log.i("Type", "jSON");
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.i("Type", "ERro");
                    CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
                    loadDlg.dismiss();


                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "12345");
                    return map;
                }

            };
            int socketTimeout = 60000 * 5;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    //
    public void showSuccessDialog(String message, String Title) {
        CustomSuccessDialog builder = new CustomSuccessDialog(getActivity(), Title, message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                Intent mainActivityIntent = new Intent(getActivity(), MainActivity.class);
                sendRefresh();
                startActivity(mainActivityIntent);
                getActivity().finish();
            }
        });
        builder.show();
    }

    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    @Override
    public void onClick(View v) {

    }

    private void sendRefresh() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "1");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }


    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        public SpacesItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view,
                                   RecyclerView parent, RecyclerView.State state) {
            outRect.left = space;
            outRect.right = space;
            outRect.bottom = space;
            outRect.top = space;
        }
    }

}
