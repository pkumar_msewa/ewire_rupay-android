package in.MadMoveGlobal.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.model.SplitMoneyGroupModel;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.util.AddGroupsListner;
import in.MadMoveGlobal.util.CheckLog;
import in.MadMoveGlobal.util.PayingDetailsValidation;

/**
 * Created by Ksf on 6/22/2016.
 */
public class AddGroupsFragmentDialog extends DialogFragment {
    private EditText etGroupName,etGroupAddPeople,etGroupAmount,etGroupPeopleName;
    private Button btnAddGroup,btnDismissGroup;
    private LinearLayout llGroupPeoples;

    private ImageButton iBtnGroupPhoneBook;
    private FloatingActionButton btnAddPeople;
    private Spinner spinner_group_type;

    private boolean cancel;
    private View focusView;

    private UserModel session = UserModel.getInstance();
    //Variables to add people

    private List<SplitMoneyGroupModel> peopleArray;

    private String peopleIdentity;
    private String peopleName;

    private String totalPayAmount;
    private int totalSplitAmount,totalNetSplit = 0;
    private String groupName;

    public AddGroupsFragmentDialog() {
        // Empty constructor required for DialogFragment
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme);
        peopleArray = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dialog_add_groups, container);
        etGroupName = (EditText) view.findViewById(R.id.etGroupName);
        etGroupAddPeople = (EditText) view.findViewById(R.id.etAddPeople);
        etGroupAmount = (EditText) view.findViewById(R.id.etGroupAmount);
        etGroupPeopleName = (EditText) view.findViewById(R.id.etGroupPeopleName);
        btnAddGroup = (Button) view.findViewById(R.id.btnAddGroup);
        btnDismissGroup = (Button) view.findViewById(R.id.btnDismissGroup);
        llGroupPeoples = (LinearLayout) view.findViewById(R.id.llGroupPeoples);
        iBtnGroupPhoneBook = (ImageButton) view.findViewById(R.id.iBtnGroupPhoneBook);
        btnAddPeople = (FloatingActionButton) view.findViewById(R.id.btnAddPeople);
        spinner_group_type  = (Spinner) view.findViewById(R.id.spinner_group_type);


        btnDismissGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        btnAddGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (spinner_group_type.getSelectedItemPosition() == 0) {
                    CustomToast.showMessage(getActivity(), "Select a group type to continue");
                }
                else{
                    if(peopleArray.size()==0){
                        CustomToast.showMessage(getActivity(),"Please add people to continue");
                    }
                    else if (checkTotalAmount(peopleArray,totalPayAmount)) {
                        String groupType = String.valueOf(spinner_group_type.getSelectedItemPosition());
                        AddGroupsListner activity = (AddGroupsListner) getActivity();
                        activity.addGroupCompleted(etGroupName.getText().toString(),etGroupAmount.getText().toString(),groupType,peopleArray);
                        dismiss();

                    }
                }

            }
        });

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(true);

        iBtnGroupPhoneBook.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                startActivityForResult(intent, 1);
            }
        });


        btnAddPeople.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptAddPeople();
            }
        });

        return view;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        String[] projection = {ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.NUMBER};
        switch (requestCode) {
            case (1):
                if (resultCode == Activity.RESULT_OK) {
                    Uri contactUri = data.getData();
                    Cursor c = getActivity().getContentResolver().query(
                            contactUri, projection, null, null, null);
                    if(c!=null) {
                        if (c.moveToFirst()) {
                            String name = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                            etGroupPeopleName.setText(name);
                            String phoneNumber = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            String finalNumber = phoneNumber.replaceAll("[^0-9\\+]", "");
                            removeCountryCode(finalNumber);
                        }
                        c.close();
                    }

                }
        }
    }

    private void removeCountryCode(String number) {
        if (hasCountryCode(number)) {
            int country_digits = number.length() - 10;
            number = number.substring(country_digits);
            etGroupAddPeople.setText(number);
        } else {
            etGroupAddPeople.setText(number);
        }
    }

    private boolean hasCountryCode(String number) {
        return number.charAt(0) == '+';
    }

    private void attemptAddPeople() {
        etGroupPeopleName.setError(null);
        etGroupAddPeople.setError(null);
        etGroupName.setError(null);
        etGroupAmount.setError(null);

        cancel = false;

        peopleIdentity = etGroupAddPeople.getText().toString();
        peopleName = etGroupPeopleName.getText().toString();

        groupName = etGroupName.getText().toString();
        totalPayAmount = etGroupAmount.getText().toString();


        checkInviteName(peopleName);
        checkGroupName(groupName);
        checkAmount(totalPayAmount);


        if(isNumeric(peopleIdentity)){
            checkInviteNumber(peopleIdentity);
        }
        else{
            checkInviteEmail(peopleIdentity);
        }
        if (cancel) {
            focusView.requestFocus();
        } else {
            promoteAddPeople();

        }
    }

    private void checkInviteNumber(String peopleNumber) {
        CheckLog inviteNumberCheckLog = PayingDetailsValidation.checkMobileTenDigit(peopleNumber);
        if (!inviteNumberCheckLog.isValid) {
            etGroupAddPeople.setError(getString(inviteNumberCheckLog.msg));
            focusView = etGroupAddPeople;
            cancel = true;
        }

    }

    private boolean isNumeric(String str)
    {
        for (char c : str.toCharArray())
        {
            if (!Character.isDigit(c)) return false;
        }
        return true;
    }

    private void checkInviteName(String inviteName) {
        CheckLog inviteNameCheckLog = PayingDetailsValidation.checkFirstName(inviteName);
        if (!inviteNameCheckLog.isValid) {
            etGroupPeopleName.setError(getString(inviteNameCheckLog.msg));
            focusView = etGroupPeopleName;
            cancel = true;
        }

    }

    private void checkGroupName(String inviteName) {
        CheckLog inviteNameCheckLog = PayingDetailsValidation.checkFirstName(inviteName);
        if (!inviteNameCheckLog.isValid) {
            etGroupName.setError(getString(inviteNameCheckLog.msg));
            focusView = etGroupName;
            cancel = true;
        }

    }

    private void checkAmount(String inviteName) {
        CheckLog inviteNameCheckLog = PayingDetailsValidation.checkAmount(inviteName);
        if (!inviteNameCheckLog.isValid) {
            etGroupAmount.setError(getString(inviteNameCheckLog.msg));
            focusView = etGroupAmount;
            cancel = true;
        }

    }

    private boolean checkTotalAmount(List<SplitMoneyGroupModel> peopleArray, String totalAmount) {
            for(int i = 0;i<peopleArray.size();i++){
                totalSplitAmount = Integer.parseInt(peopleArray.get(i).getAmountPerson());
                totalNetSplit = totalNetSplit + totalSplitAmount;

            }
        Log.i("TOTAL People size",peopleArray.size() +"");
        Log.i("TOTAL SPLIT",totalNetSplit+"");
        Log.i("TOTAL AMOUNT",totalAmount+"");

        if(totalNetSplit>Integer.valueOf(totalAmount)){
            etGroupAmount.setError("Spilt amount cannot be greater than total amount.");
            focusView = etGroupAmount;
            return false;
        }
        else{
            return true;
        }
    }

    private boolean checkInviteEmail(String email) {
        CheckLog inviteNameCheckLog = PayingDetailsValidation.checkEmail(email);
        if (email.isEmpty() || !isValidEmail(email)) {
            etGroupAddPeople.setError(getString(inviteNameCheckLog.msg));
            focusView = etGroupAddPeople;
            cancel = true;
        }
        return true;
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void promoteAddPeople(){
        if(peopleArray.size()==0){
            int peoplePay = Integer.parseInt(totalPayAmount)/2;
            SplitMoneyGroupModel splitMoneyGroupModel = new SplitMoneyGroupModel(0,peopleName,String.valueOf(peoplePay),peopleIdentity);
            SplitMoneyGroupModel splitMoneyGroupModelMy = new SplitMoneyGroupModel(0,"My Contribution",String.valueOf(peoplePay),session.getUserMobileNo());
            peopleArray.add(splitMoneyGroupModel);
            peopleArray.add(splitMoneyGroupModelMy);
        }
        else{
            int peoplePay = Integer.parseInt(totalPayAmount)/(peopleArray.size()+1);
            for (int i = 0; i < peopleArray.size(); i++) {
                peopleArray.get(i).setAmountPerson(String.valueOf(peoplePay));
            }
            SplitMoneyGroupModel splitMoneyGroupModel = new SplitMoneyGroupModel(0,peopleName,String.valueOf(peoplePay),peopleIdentity);
            peopleArray.add(splitMoneyGroupModel);
        }
        llGroupPeoples.removeAllViews();

        for (int i = 0; i < peopleArray.size(); i++) {

            LinearLayout llInner = new LinearLayout(getActivity());
            llInner.setOrientation(LinearLayout.HORIZONTAL);

            final EditText et = new EditText(getActivity());
            et.setId(i);

            et.setText(peopleArray.get(i).getAmountPerson());

            et.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    peopleArray.get(et.getId()).setAmountPerson(String.valueOf(et.getText().toString()));
                    totalNetSplit = 0;
                    totalSplitAmount = 0;

                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });

            TextView tv = new TextView(getActivity());
            tv.setText(String.valueOf(peopleArray.get(i).getNamePerson()+" - " +getResources().getString(R.string.rupease)));

            llInner.addView(tv);
            llInner.addView(et);

            llGroupPeoples.addView(llInner);
            etGroupAddPeople.getText().clear();
            etGroupPeopleName.getText().clear();
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        }
    }
}
