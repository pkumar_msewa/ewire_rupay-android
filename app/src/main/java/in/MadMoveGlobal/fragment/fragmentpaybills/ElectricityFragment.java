package in.MadMoveGlobal.fragment.fragmentpaybills;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.orm.query.Select;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import in.MadMoveGlobal.EwireRuPay.WebViewsActivity;
import in.MadMoveGlobal.EwireRuPay.activity.MainActivity;
import in.MadMoveGlobal.adapter.OperatorSpinnerAdapter;
import in.MadMoveGlobal.custom.CustomAlertDialog;
import in.MadMoveGlobal.custom.CustomBlockDialog;
import in.MadMoveGlobal.custom.CustomServiceProviderDialogSearch;
import in.MadMoveGlobal.custom.CustomSuccessDialog;
import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.custom.VerifyMPinDialog;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.metadata.AppMetadata;
import in.MadMoveGlobal.model.CircleModel;
import in.MadMoveGlobal.model.OperatorsModel;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.util.CheckLog;
import in.MadMoveGlobal.util.GenerateAPIHeader;
import in.MadMoveGlobal.util.MPinVerifiedListner;
import in.MadMoveGlobal.util.NetworkErrorHandler;
import in.MadMoveGlobal.util.PayingDetailsValidation;
import in.MadMoveGlobal.util.ServicesSelectedListener;
import in.MadMoveGlobal.util.Utility;

//import static in.MadMoveGlobal.metadata.ApiUrl.URL_CIRCLE_OPERATORELC;


/**
 * Created by Ksf on 3/16/2016.
 */
public class ElectricityFragment extends Fragment implements MPinVerifiedListner {
    private View rootView;
    private Spinner spinner_electricity_operator;
    private MaterialEditText etElecAmount;
    private MaterialEditText etElecCycleNo;
    private MaterialEditText etElecACNo;
    private MaterialEditText etElecCityName, spService;
    private Button btnElecPay;
    private ImageButton btnDeuElec;
    String dueAmount, duedate, billnumber, billdate, customername, referenceNumber;
    Integer DueAmounts;

    private OperatorSpinnerAdapter operatorSpinnerAdapter;

    private View focusView = null;
    private boolean cancel;

    private List<CircleModel> circleList;
    private List<OperatorsModel> operatorList;
    private ArrayList<OperatorsModel> operatorLists;

    private static HashMap<String, Integer> operatorIdCode;
    private static HashMap<String, Integer> circleIdCode;
    //User Values

    private LoadingDialog loadDlg;
    private UserModel session = UserModel.getInstance();
    private String amount, serviceProvider, customerNo, serviceProviderName;
    private String cycleNo = "";
    private String cityName = "";
    private String operatorCode, selectedFromServiceCode, selectedFromServiceName;

    private JSONObject jsonRequest;

    private Integer additionalCharges = 0;
    private String payAmount;
    private MyReceiver r;
    //Volley Tag
    private String tag_json_obj = "json_bill_pay";
    private LinearLayout llLayouts;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        onDetach();
        loadDlg = new LoadingDialog(getActivity());
        circleList = Select.from(CircleModel.class).list();
        operatorList = Select.from(OperatorsModel.class).list();
        operatorIdCode = new HashMap<>();
        circleIdCode = new HashMap<>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_elecricty, container, false);
        onDetach();
        spinner_electricity_operator = (Spinner) rootView.findViewById(R.id.spinner_electricity_operator);
        etElecACNo = (MaterialEditText) rootView.findViewById(R.id.etElecACNo);
        etElecCycleNo = (MaterialEditText) rootView.findViewById(R.id.etElecCycleNo);
        etElecAmount = (MaterialEditText) rootView.findViewById(R.id.etElecAmount);
        etElecCityName = (MaterialEditText) rootView.findViewById(R.id.etElecCityName);
        btnElecPay = (Button) rootView.findViewById(R.id.btnElecPay);
        btnDeuElec = (ImageButton) rootView.findViewById(R.id.btnDeuElec);
        llLayouts = (LinearLayout) rootView.findViewById(R.id.llLayouts);
        spService = (MaterialEditText) rootView.findViewById(R.id.spService);
        operatorLists = new ArrayList<>();

        etElecAmount.setFocusableInTouchMode(false);
        etElecAmount.setFocusable(false);

//        operatorSpinnerAdapter = new OperatorSpinnerAdapter(getActivity(), android.R.layout.simple_spinner_item, MenuMetadata.getElectricity());
//        spinner_electricity_operator.setAdapter(operatorSpinnerAdapter);

//    spinner_electricity_operator.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//      @Override
//      public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//        if (i != 0) {
//          serviceProvider = ((OperatorsModel) spinner_electricity_operator.getSelectedItem()).code;
//          serviceProviderName = ((OperatorsModel) spinner_electricity_operator.getSelectedItem()).name;
//          if (serviceProvider.equals("VREE")) {
//            etElecCycleNo.setVisibility(View.VISIBLE);
//            etElecCycleNo.setText("");
//            etElecACNo.setHint("Account Number");
//          } else if (serviceProvider.equals("VTPE")) {
//            etElecCityName.setVisibility(View.VISIBLE);
//            etElecCityName.setText("");
//            etElecACNo.setHint("Service Connection Number");
//          } else if (serviceProvider.equals("VTPE")) {
//            etElecACNo.setHint("Service Connection Number");
//          } else if (serviceProvider.equals("VCCE")) {
//            etElecACNo.setHint("BP Number");
//          } else if (serviceProvider.equals("VDHE")) {
//            etElecACNo.setHint("Account Number");
//          } else if (serviceProvider.equals("VSAE") || serviceProvider.equals("VDNE")) {
//            etElecACNo.setHint("Service Connection Number");
//          } else if (serviceProvider.equals("VJUE")) {
//            etElecACNo.setHint("Business Partner Number");
//          } else if (serviceProvider.equals("VARE") || serviceProvider.equals("VJRE") || serviceProvider.equals("VDRE")) {
//            etElecACNo.setHint("BK Number");
//          } else {
//            etElecACNo.setHint("Consumer Number");
//            cycleNo = "";
//            etElecCycleNo.setVisibility(View.GONE);
//          }
//
//          showExactAmountDialog();
//        }
//
//      }
//
//      @Override
//      public void onNothingSelected(AdapterView<?> adapterView) {
//
//      }
//    });

        //DONE CLICK ON VIRTUAL KEYPAD
        etElecAmount.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    attemptPayment();
                }
                return false;
            }
        });

        btnElecPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptPayment();
            }
        });


        btnDeuElec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptDeu();
            }
        });


        loadOperatorCircleNew();

        spService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final CustomServiceProviderDialogSearch search = new CustomServiceProviderDialogSearch(getActivity(), operatorLists, "from", new ServicesSelectedListener() {
                    @Override
                    public void serviceselect(String type, long servicecode, String serviceName) {

                    }

                    @Override
                    public void serviceFilter(OperatorsModel type) {

                        spService.setText(type.getName());
                        serviceProvider = type.getCode();
                        serviceProviderName = type.getName();

                        if (serviceProvider.equals("REEIN")) {
                            etElecCycleNo.setVisibility(View.VISIBLE);
                            etElecCycleNo.setText("");
                            etElecACNo.setHint(getResources().getString(R.string.Account_Number));
                        } else if (serviceProvider.equals("TPEIN")) {
                            etElecCityName.setVisibility(View.VISIBLE);
                            etElecCityName.setText("");
                            etElecACNo.setHint(getResources().getString(R.string.Service_Connection_Number));
                        } else if (serviceProvider.equals("TPEIN")) {
                            etElecACNo.setHint(getResources().getString(R.string.Service_Connection_Number));
                        } else if (serviceProvider.equals("CCEIN")) {
                            etElecACNo.setHint(getResources().getString(R.string.BP_Number));
                        } else if (serviceProvider.equals("DHEIN")) {
                            etElecACNo.setHint(getResources().getString(R.string.Account_Number));
                        } else if (serviceProvider.equals("SAEIN") || serviceProvider.equals("DNEIN")) {
                            etElecACNo.setHint(getResources().getString(R.string.Service_Connection_Number));
                        } else if (serviceProvider.equals("JUEIN")) {
                            etElecACNo.setHint(getResources().getString(R.string.Business_Partner_Number));
                        } else if (serviceProvider.equals("AREIN") || serviceProvider.equals("JREIN") || serviceProvider.equals("DREIN")) {
                            etElecACNo.setHint(getResources().getString(R.string.BK_Number));
                        } else {
                            etElecACNo.setHint(getResources().getString(R.string.Consumer_Number));
                            cycleNo = "";
                            cityName = "";
                            etElecCycleNo.setVisibility(View.GONE);
                            etElecCityName.setVisibility(View.GONE);
                        }

                        showExactAmountDialog();
                    }
                });
                search.show();
            }
        });


        return rootView;
    }

    public void loadOperatorCircleNew() {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("topUpType", "elec");
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }
//
        if (jsonRequest != null) {
            Log.i("LOADCIRCLEOP Request", jsonRequest.toString());
            Log.i("LOADCIRCLEOP URL", ApiUrl.URL_CIRCLE_OPERATOR);
//
//        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, URL_CIRCLE_OPERATORELC, (String) null,
//                new Response.Listener<JSONObject>() {
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_CIRCLE_OPERATOR, jsonRequest, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject JsonObj) {
                    loadDlg.dismiss();
                    Log.i("LOADCIRCLEOP Response", JsonObj.toString());
                    /*try {
                        operatorList.clear();
                        circleList.clear();

                        operatorIdCode.clear();
                        circleIdCode.clear();

                        OperatorsModel.deleteAll(OperatorsModel.class);
                        CircleModel.deleteAll(CircleModel.class);
                        //New Implementation

                        String jsonString = JsonObj.getString("details");
                        Log.i("Escape str", jsonEscape(jsonString));

                        JSONObject response = new JSONObject(jsonString);

                        JSONArray operatorArray = response.getJSONArray("operators");

//                        operatorList.add(new OperatorsModel("", "Select your operator", ""));

                        for (int i = 0; i < operatorArray.length(); i++) {
                            JSONObject c = operatorArray.getJSONObject(i);
                            String op_code = c.getString("serviceCode");
                            String op_name = c.getString("name");
                            String op_default_code = c.getString("code");
                            OperatorsModel oModel = new OperatorsModel(op_code, op_name, op_default_code);
                            oModel.save();
                            operatorList.add(oModel);
                            Log.i("OperatorsModel", String.valueOf(operatorList.get(i).getCode()));
                            operatorIdCode.put(op_code, i + 1);
                        }


                        JSONArray circleArray = response.getJSONArray("circles");

                        circleList.add(new CircleModel("", "Select your circle"));
                        for (int i = 0; i < circleArray.length(); i++) {
                            JSONObject c = circleArray.getJSONObject(i);
                            String op_code = c.getString("code");
                            String op_name = c.getString("name");
                            CircleModel cModel = new CircleModel(op_code, op_name);
                            cModel.save();
                            circleList.add(cModel);
                            circleIdCode.put(op_code, i + 1);
                        }


                        if (operatorList != null && operatorList.size() != 0) {
                            try {
                                operatorList.add(0, new OperatorsModel("", "Select your operator", ""));
                                spinnerAdapterOperator = new OperatorSpinnerAdapter(getActivity(), android.R.layout.simple_spinner_item, operatorList);
                                spinnerPrepaidProvider.setAdapter(spinnerAdapterOperator);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        if (circleList != null && circleList.size() != 0) {
                            try {
                                circleList.add(0, new CircleModel("", "Select your circle"));
                                spinnerAdapterCircle = new CircleSpinnerAdapter(getActivity(), android.R.layout.simple_spinner_item, circleList);
                                spinnerPrepaidCircle.setAdapter(spinnerAdapterCircle);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }*/
                    try {
                        operatorList.clear();
                        circleList.clear();

                        operatorIdCode.clear();
                        circleIdCode.clear();

                        OperatorsModel.deleteAll(OperatorsModel.class);
                        CircleModel.deleteAll(CircleModel.class);
                        //New Implementation

//                    String jsonString = JsonObj.getString("response");
//                    Log.i("Escape str", jsonEscape(jsonString));
//
//                    JSONObject response = new JSONObject(jsonString);

                        JSONArray operatorArray = JsonObj.getJSONArray("details");

//                        operatorList.add(new OperatorsModel("", "Select your operator", ""));

                        for (int i = 0; i < operatorArray.length(); i++) {
                            JSONObject c = operatorArray.getJSONObject(i);
                            String op_code = c.getString("code");
                            String op_name = c.getString("name");
                            String op_default_code = c.getString("serviceLogo");
                            OperatorsModel oModel = new OperatorsModel(op_code, op_name, op_default_code);
                            oModel.save();
                            operatorList.add(oModel);
                            operatorLists.add(oModel);
                            Log.i("OperatorsModel", String.valueOf(operatorList.get(i).getCode()));
                            operatorIdCode.put(op_code, i + 1);
                        }
                        spService.setHint(getResources().getString(R.string.Service_Provider));

//                    JSONArray circleArray = response.getJSONArray("circles");
//
//                        circleList.add(new CircleModel("", "Select your circle"));
//                    for (int i = 0; i < circleArray.length(); i++) {
//                        JSONObject c = circleArray.getJSONObject(i);
//                        String op_code = c.getString("code");
//                        String op_name = c.getString("name");
//                        CircleModel cModel = new CircleModel(op_code, op_name);
//                        cModel.save();
//                        circleList.add(cModel);
//                        circleIdCode.put(op_code, i + 1);
//                    }


//                  if (operatorList != null && operatorList.size() != 0) {
//                    try {
//                      operatorList.add(0, new OperatorsModel("", "Select your operator", ""));
//                      operatorSpinnerAdapter = new OperatorSpinnerAdapter(getActivity(), android.R.layout.simple_spinner_item, operatorList);
//                      spinner_electricity_operator.setAdapter(operatorSpinnerAdapter);
//                    } catch (Exception e) {
//                      e.printStackTrace();
//                    }
//                  }

//                    if (circleList != null && circleList.size() != 0) {
//                        try {
//                            circleList.add(0, new CircleModel("", "Select your circle"));
//                            spinnerAdapterCircle = new CircleSpinnerAdapter(getActivity(), android.R.layout.simple_spinner_item, circleList);
//                            spinnerPrepaidCircle.setAdapter(spinnerAdapterCircle);
//
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }

                    } catch (JSONException e) {
                        e.printStackTrace();
//                  Toast.makeText(getActivity(), getResources().getString(R.string.server_exception2), Toast.LENGTH_SHORT).show();
                        Snackbar.make(llLayouts, getResources().getString(R.string.server_exception2), Snackbar.LENGTH_LONG).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    error.printStackTrace();
//        Toast.makeText(getActivity(), getResources().getString(R.string.server_exception), Toast.LENGTH_SHORT).show();
                    Snackbar.make(llLayouts, getResources().getString(R.string.server_exception), Snackbar.LENGTH_LONG).show();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");


//                String basicAuth = "Basic " + new String(Base64.encode("user:pass".getBytes(), Base64.NO_WRAP));
//                map.put("Authorization", basicAuth);
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);

        }
    }


    private void attemptPayment() {

        etElecACNo.setError(null);
        etElecCycleNo.setError(null);
        etElecAmount.setError(null);
        etElecCityName.setError(null);

        if (spService.getText().toString().equals("")) {
//      CustomToast.showMessage(getActivity(), "Select provider to continue");
//            Snackbar.make(llLayouts, "Select operator to continue", Snackbar.LENGTH_LONG).show();
            spService.setError(getResources().getString(R.string.Select_Service_Provider_to_continue));
            return;
        }


//        if (spinner_electricity_operator.getSelectedItemPosition() == 0) {
////      CustomToast.showMessage(getActivity(), "Select provider to continue");
//            Snackbar.make(llLayouts, "Select provider to continue", Snackbar.LENGTH_LONG).show();
//            return;
//        } else if (spinner_electricity_operator.getSelectedItemPosition() == 1) {
//            additionalCharges = "2";
//        } else if (spinner_electricity_operator.getSelectedItemPosition() == 2) {
//            additionalCharges = "2";
//        } else if (spinner_electricity_operator.getSelectedItemPosition() == 4) {
//            additionalCharges = "2";
//        } else if (spinner_electricity_operator.getSelectedItemPosition() == 5) {
//            additionalCharges = "2";
//        } else if (spinner_electricity_operator.getSelectedItemPosition() == 7) {
//            additionalCharges = "2";
//        } else if (spinner_electricity_operator.getSelectedItemPosition() == 8) {
//            additionalCharges = "2";
//        } else if (spinner_electricity_operator.getSelectedItemPosition() == 9) {
//            additionalCharges = "2";
//        } else if (spinner_electricity_operator.getSelectedItemPosition() == 10) {
//            additionalCharges = "2";
//        } else if (spinner_electricity_operator.getSelectedItemPosition() == 11) {
//            additionalCharges = "2";
//        } else if (spinner_electricity_operator.getSelectedItemPosition() == 12) {
//            additionalCharges = "2";
//        } else if (spinner_electricity_operator.getSelectedItemPosition() == 13) {
//            additionalCharges = "2";
//        } else if (spinner_electricity_operator.getSelectedItemPosition() == 14) {
//            additionalCharges = "2";
//        } else if (spinner_electricity_operator.getSelectedItemPosition() == 15) {
//            additionalCharges = "2";
//        } else if (spinner_electricity_operator.getSelectedItemPosition() == 16) {
//            additionalCharges = "2";
//        } else if (spinner_electricity_operator.getSelectedItemPosition() == 17) {
//            additionalCharges = "2";
//        } else if (spinner_electricity_operator.getSelectedItemPosition() == 18) {
//            additionalCharges = "2";
//        } else if (spinner_electricity_operator.getSelectedItemPosition() == 19) {
//            additionalCharges = "2";
//        } else if (spinner_electricity_operator.getSelectedItemPosition() == 22) {
//            additionalCharges = "2";
//        } else if (spinner_electricity_operator.getSelectedItemPosition() == 23) {
//            additionalCharges = "2";
//        } else if (spinner_electricity_operator.getSelectedItemPosition() == 24) {
//            additionalCharges = "2";
//        } else if (spinner_electricity_operator.getSelectedItemPosition() == 25) {
//            additionalCharges = "2";
//        } else {
//            additionalCharges = "0";
//        }

        cancel = false;

        amount = etElecAmount.getText().toString();
        customerNo = etElecACNo.getText().toString();
        cycleNo = etElecCycleNo.getText().toString();
        cityName = etElecCityName.getText().toString();


        if (!validateEtcNo()) {
            return;
        }

        if (etElecCycleNo.getVisibility() == View.VISIBLE) {
//            checkCycle(cycleNo);

            if (!validateCycleNo()) {
                return;
            }
        }

        if (etElecCityName.getVisibility() == View.VISIBLE) {
            if (!validateCityName()) {
                return;
            }
        }

        if (etElecAmount.getText().toString().equalsIgnoreCase("")){
            etElecAmount.setError("Mandatory Field");
            return;
        }


        checkPayAmount(amount);

        if (cancel) {
            focusView.requestFocus();
        } else {
            showCustomDialog();
        }
    }

    private void attemptDeu() {
        etElecACNo.setError(null);
        etElecCycleNo.setError(null);
        etElecCityName.setError(null);
        if (spService.getText().toString().equals("")) {
//      CustomToast.showMessage(getActivity(), "Select provider to continue");
//            spService.setError("Select operator to continue");
            Snackbar.make(llLayouts, getResources().getString(R.string.Select_Service_Provider_to_continue), Snackbar.LENGTH_LONG).show();

            return;
        }


        cancel = false;

        customerNo = etElecACNo.getText().toString();
        cycleNo = etElecCycleNo.getText().toString();

//        checkCustomerNo(customerNo);
        if (!validateEtcNo()) {
            return;
        }

        if (etElecCycleNo.getVisibility() == View.VISIBLE) {
            checkCycle(cycleNo);

            if (!validateCycleNo()) {
                return;
            }

        }

        if (etElecCityName.getVisibility() == View.VISIBLE) {
            if (!validateCityName()) {
                return;
            }
        }


        if (cancel) {
            focusView.requestFocus();
        } else {
            getDeuAmount();
        }
    }

    private boolean validateEtcNo() {
        String amt = etElecACNo.getText().toString().trim();
        if (amt.isEmpty() || amt.length() < 10 || amt.length() > 20 || amt.equalsIgnoreCase("0000000000") || amt.equals("00000000000") || amt.equals("000000000000") || amt.equals("0000000000000") || amt.equals("00000000000000") || amt.equals("000000000000000") || amt.equals("0000000000000000") || amt.equals("00000000000000000") || amt.equals("000000000000000000") || amt.equals("0000000000000000000") || amt.equals("00000000000000000000")) {
            String mes = "Enter valid " + etElecACNo.getHint();
            etElecACNo.setError(mes);
            return false;
        }
        return true;
    }

    private boolean validateCycleNo() {
        String amt = etElecCycleNo.getText().toString().trim();
        if (amt.isEmpty() || amt.length() < 6 || amt.length() > 15 || amt.equalsIgnoreCase("000000") || amt.equals("0000000") || amt.equals("00000000") || amt.equals("000000000") || amt.equals("0000000000") || amt.equals("00000000000") || amt.equals("000000000000") || amt.equals("0000000000000") || amt.equals("00000000000000") || amt.equals("000000000000000")) {
            etElecCycleNo.setError(getResources().getString(R.string.Enter_valid_cycle_No));
            return false;
        }
        return true;
    }

//    private void checkCustomerNo(String acNo) {
//        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(acNo);
//        if (!gasCheckLog.isValid) {
////      etElecACNo.setError(getString(gasCheckLog.msg));
//            Snackbar.make(llLayouts, getString(gasCheckLog.msg), Snackbar.LENGTH_LONG).show();
//            focusView = etElecACNo;
//            cancel = true;
//        }
//    }

    private void checkPayAmount(String amount) {
        if (amount.equalsIgnoreCase("null")) {

        } else {
            if(!amount.equalsIgnoreCase("")) {
                etElecAmount.setError(null);
                Double protein = Double.valueOf(amount);
                DueAmounts = Integer.valueOf(protein.intValue());
                CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
                if (!gasCheckLog.isValid) {
//      etElecAmount.setError(getString(gasCheckLog.msg));
                    Snackbar.make(llLayouts, getString(gasCheckLog.msg), Snackbar.LENGTH_LONG).show();
                    focusView = etElecAmount;
                    cancel = true;
                } else if (DueAmounts < 10) {
//      etElecAmount.setError(getString(R.string.lessAmount));
                    Snackbar.make(llLayouts, getActivity().getResources().getString(R.string.lessAmount), Snackbar.LENGTH_LONG).show();
                    focusView = etElecAmount;
                    cancel = true;
                } else if (DueAmounts > 50000) {
//      etElecAmount.setError(getString(R.string.error_invalid_amount_5000));
                    Snackbar.make(llLayouts, getString(R.string.error_invalid_amount_5000), Snackbar.LENGTH_LONG).show();
                    focusView = etElecAmount;
                    cancel = true;
                } else if (etElecAmount.getText().toString().substring(0, 1).equals("0")) {

                    etElecAmount.setError(getResources().getString(R.string.Amount_should_not_start_with_0));
                    focusView = etElecAmount;
                    cancel = true;
                }
            }else{
                etElecAmount.setError("Mandatory Field");
                return;
            }
        }
    }

    private boolean validateCityName() {
        String amt = etElecCityName.getText().toString().trim();
        if (amt.isEmpty()) {
            etElecCityName.setError(getResources().getString(R.string.Enter_city_name));
            return false;
        }
        return true;
    }

    private void checkCycle(String cycle) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(cycle);
        if (!gasCheckLog.isValid) {
//      etElecCycleNo.setError(getString(gasCheckLog.msg));
            Snackbar.make(llLayouts, getString(gasCheckLog.msg), Snackbar.LENGTH_LONG).show();
            focusView = etElecCycleNo;
            cancel = true;
        }
    }

    private void checkUpBalance() {
        loadDlg.show();
        Utility.hideKeyboard(getActivity());
        jsonRequest = new JSONObject();
        try {
//            jsonRequest.put("serviceProvider", serviceProvider);
//            jsonRequest.put("accountNumber", customerNo);
//            jsonRequest.put("amount", amount);
//            jsonRequest.put("sessionId", session.getUserSessionId());
//            if (etElecCycleNo.getVisibility() == View.VISIBLE) {
//                jsonRequest.put("cycleNumber", cycleNo);
//            } else {
//                jsonRequest.put("cycleNumber", "");
//            }
//            if (etElecCityName.getVisibility() == View.VISIBLE) {
//                jsonRequest.put("cityName", cycleNo);
//            } else {
//                jsonRequest.put("cityName", "");
//            }
            jsonRequest.put("serviceCode", serviceProvider);
            jsonRequest.put("accountNumber", customerNo);
            jsonRequest.put("amount", amount);
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("topUpType", "Electricity");
            jsonRequest.put("processingCycle", "");
            jsonRequest.put("billingUnit", "");
            jsonRequest.put("referenceNumber", referenceNumber);
            if (etElecCycleNo.getVisibility() == View.VISIBLE) {
                jsonRequest.put("cycleNumber", cycleNo);
            } else {
                jsonRequest.put("cycleNumber", "");
            }
            if (etElecCityName.getVisibility() == View.VISIBLE) {
                jsonRequest.put("cityName", cycleNo);
            } else {
                jsonRequest.put("cityName", "");
            }
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_PAYMENTS, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.i("DTH RESPONSE", response.toString());
                        String code = response.getString("code");
                        if (code != null && code.equals("S00")) {
                            showSuccessDialog();
//                        } else if (code != null && code.equals("T01")) {
//                            loadDlg.dismiss();
//                            JSONObject dtoObject = response.getJSONObject("dto");
//                            String splitAmount = dtoObject.getString("splitAmount");
//                            String message = response.getString("message");
////                            CustomToast.showMessage(getActivity(),"You have insufficient balance. Please wait while you are redirected to load money.");
//                            Intent mainMenuDetailActivity = new Intent(getActivity(), WebViewActivity.class);
////                            mainMenuDetailActivity.putExtra(AppMetadata.FRAGMENT_TYPE, "LoadMoney");
////                            mainMenuDetailActivity.putExtra("AutoFill", "yes");
////                            if (!splitAmount.isEmpty()){
//                            mainMenuDetailActivity.putExtra("TYPE", "BILLPAY");
//                            mainMenuDetailActivity.putExtra("SUBTYPE", "ELECTRIC");
//                            mainMenuDetailActivity.putExtra("splitAmount", amount);
//                            mainMenuDetailActivity.putExtra("URL", ApiUrl.URL_ELECTRICITY_PAYMENT);
//                            mainMenuDetailActivity.putExtra("REQUESTOBJECT", jsonRequest.toString());
//                            startActivity(mainMenuDetailActivity);
////                            }else{
////                                CustomToast.showMessage(getActivity(),"split amount can't be empty");
////                            }


                        } else if (code != null && code.equals("F03")) {
                            loadDlg.dismiss();
                            showInvalidSessionDialog();
                        } else if (code != null && code.equals(ApiUrl.C_SESSION_BLOCKED)) {
                            loadDlg.dismiss();
                            showBlockDialog();
                        } else {
                            loadDlg.dismiss();
                            if (response.has("message") && response.getString("message") != null) {
                                String message = response.getString("message");
                                CustomToast.showMessage(getActivity(), message);
                                Snackbar.make(llLayouts, message, Snackbar.LENGTH_LONG).show();

                            } else {
//                                CustomToast.showMessage(getActivity(), "Error message is null");
                                Snackbar.make(llLayouts, getResources().getString(R.string.Error_message_is_null), Snackbar.LENGTH_LONG).show();
                            }


                        }

                    } catch (JSONException e) {
                        loadDlg.dismiss();
//            CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
                        Snackbar.make(llLayouts, getResources().getString(R.string.server_exception2), Snackbar.LENGTH_LONG).show();


                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    error.printStackTrace();
//          CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
                    Snackbar.make(llLayouts, NetworkErrorHandler.getMessage(error, getActivity()), Snackbar.LENGTH_LONG).show();


                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", GenerateAPIHeader.getHeader(ApiUrl.H_ELECTRICITY + session.getUserSessionId()));
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    private void promotePayment() {
        jsonRequest = new JSONObject();


        try {
            jsonRequest.put("serviceCode", serviceProvider);
            jsonRequest.put("accountNumber", customerNo);
            jsonRequest.put("amount", amount);
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("topUpType", "Electricity");
            jsonRequest.put("processingCycle", "");
            jsonRequest.put("billingUnit", "");
            jsonRequest.put("referenceNumber", referenceNumber);
            if (etElecCycleNo.getVisibility() == View.VISIBLE) {
                jsonRequest.put("cycleNumber", cycleNo);
            } else {
                jsonRequest.put("cycleNumber", "");
            }
            if (etElecCityName.getVisibility() == View.VISIBLE) {
                jsonRequest.put("cityName", cycleNo);
            } else {
                jsonRequest.put("cityName", "");
            }

        } catch (Exception e) {
            e.printStackTrace();
            jsonRequest = null;
        }
        if (jsonRequest != null) {
            Log.i("Prepaid Request", jsonRequest.toString());
            Log.i("Prepaid URL", ApiUrl.URL_PREPAID_TOPUP);
            StringRequest postReq = new StringRequest(Request.Method.POST, ApiUrl.URL_PREPAID_TOPUP, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.i("DEU Response", response.toString());


                    loadDlg.dismiss();


                    Intent mainMenuDetailActivity = new Intent(getActivity(), WebViewsActivity.class);
                    mainMenuDetailActivity.putExtra("TYPE", "BILLPAY");
                    mainMenuDetailActivity.putExtra("SUBTYPE", "ELECTRIC");
                    mainMenuDetailActivity.putExtra("amount", amount);
                    mainMenuDetailActivity.putExtra("response", response);
                    mainMenuDetailActivity.putExtra("REQUESTOBJECT", jsonRequest.toString());
                    mainMenuDetailActivity.putExtra("URL", ApiUrl.URL_PREPAID_TOPUP);
                    startActivity(mainMenuDetailActivity);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
//                CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));
                    Snackbar.make(llLayouts, getResources().getString(R.string.server_exception), Snackbar.LENGTH_LONG).show();

                }
            }) {
//            @Override
//            protected Map<String, String> getParams() {
//                Map<String, String> params = new HashMap<>();
//
//
//                params.put("sessionId", session.getUserSessionId());
//                params.put("amount", etPrepaidAmount.getText().toString());
//                params.put("serviceCode", serviceProvider);
//                params.put("topUpType", "Prepaid");
//                params.put("area", areacode);
//                params.put("mobileNumber", etPrepaidNo.getText().toString());
//
//
////                if (etLandLineAcNo.getVisibility() == View.VISIBLE) {
////                    params.put("accountNumber", acNo);
////                }
//                Log.i("HEADER", params.toString());
//                return params;
//            }

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return jsonRequest == null ? null : jsonRequest.toString().getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", jsonRequest, "utf-8");
                        return null;
                    }
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("hash", "123");
                    params.put("Content-Type", "application/json");
                    return params;
                }
            };

            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    public void showCustomDialog() {
        payAmount = String.valueOf(additionalCharges + DueAmounts);
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title, Html.fromHtml(generateMessage()));
        builder.setPositiveButton(getResources().getString(R.string.agree), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //                getResources().getInteger(R.integer.transactionValue)
//                if (session.isMPin() && Integer.valueOf(amount) >= 200000) {
//                    showMPinDialog();
//                } else {
                checkUpBalance();
//                }
            }
        });
        builder.setNegativeButton(getResources().getString(R.string.Dismiss), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public String generateMessage() {
        String source = "";
        if (cycleNo != null && !cycleNo.isEmpty()) {
            source = "<b><font color=#000000>" + getActivity().getResources().getString(R.string.Service_Provider) + ": " + "</font></b>" + "<font color=#000000>" + serviceProviderName + "</font><br>" +
                    "<b><font color=#000000>" + getActivity().getResources().getString(R.string.Acc_no) + ": " + "</font></b>" + "<font>" + customerNo + "</font><br>" +
                    "<b><font color=#000000>" + getActivity().getResources().getString(R.string.Cycle_No) + ": " + "</font></b>" + "<font>" + cycleNo + "</font><br>" +
                    "<b><font color=#000000>" + getActivity().getResources().getString(R.string.amount) + "</font></b>" + "<font>" + amount + "</font><br>" +

                    "<b><font color=#ff0000> Are you sure you want to proceed?</font></b><br>";
        } else {
            source = "<b><font color=#000000>" + getActivity().getResources().getString(R.string.Service_Provider) + ": " + "</font></b>" + "<font color=#000000>" + serviceProviderName + "</font><br>" +
                    "<b><font color=#000000>" + getActivity().getResources().getString(R.string.Acc_no) + ": " + "</font></b>" + "<font>" + customerNo + "</font><br>" +
                    "<b><font color=#000000>" + getActivity().getResources().getString(R.string.amount) + ": " + "</font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + amount + "</font><br>" +

                    "<b><font color=#ff0000>" + getActivity().getResources().getString(R.string.Are_you_sure_you_want_to_proceed) + "</font></b><br>";
        }
        return source;
    }

    private void sendRefresh() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "3");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton(getResources().getString(R.string.agree), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton(getResources().getString(R.string.Dismiss), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    private void showExactAmountDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, getResources().getString(R.string.exactAmount));
        builder.setPositiveButton(getResources().getString(R.string.agree), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public void showMPinDialog() {
        VerifyMPinDialog builder = new VerifyMPinDialog(getActivity(), this);
        builder.setNegativeButton(getResources().getString(R.string.Dismiss), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void verifiedCompleted() {
        promotePayment();
    }

    @Override
    public void sessionInvalid() {
        showInvalidSessionDialog();
    }


    private void getDeuAmount() {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {

            jsonRequest.put("serviceProvider", serviceProvider);
            jsonRequest.put("accountNumber", customerNo);

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            Log.i("URL", ApiUrl.URL_CHECK_DEU);
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_CHECK_DEU, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.i("DUE RESPONSE", response.toString());
                        String code = response.getString("code");
                        String message = response.getString("message");
                        loadDlg.dismiss();
                        if (code != null && code.equals("S00")) {
                            loadDlg.dismiss();
                            JSONObject detailsObject = response.getJSONObject("details");
//                            JSONObject particularsObject = new JSONObject(detailsObject.getString("particulars"));
                            dueAmount = detailsObject.getString("dueAmount");
                            duedate = detailsObject.getString("dueDate");
                            billnumber = detailsObject.getString("billNumber");
                            billdate = detailsObject.getString("billDate");
                            customername = detailsObject.getString("customerName");
                            referenceNumber = detailsObject.getString("referenceNumber");

                            etElecAmount.setText(dueAmount);

//                            showCustomDisclaimerDialog();
                            Snackbar.make(llLayouts, getResources().getString(R.string.Your_Due_amount_is_INR) + dueAmount + " ", Snackbar.LENGTH_LONG).show();


                        } else if (code != null && code.equals("F00")) {
                            loadDlg.dismiss();

                            startActivity(new Intent(getContext(), MainActivity.class));

                            CustomToast.showMessage(getActivity(), message);
                        } else if (code == null) {
                            loadDlg.dismiss();

                        } else {
                            if (response.has("message") && response.getString("message") != null) {
                                String messages = response.getString("message");
                                loadDlg.dismiss();
                                Snackbar.make(llLayouts, messages, Snackbar.LENGTH_LONG).show();

                            }
                        }

                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));

                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    error.printStackTrace();
                    CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));


                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");

//                    String basicAuth = "Basic " + new String(Base64.encode("user:pass".getBytes(), Base64.NO_WRAP));
//                    map.put("Authorization", basicAuth);

                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }


//    public void getDeuAmount() {
//        loadDlg.show();
//        StringRequest postReq = new StringRequest(Request.Method.POST, ApiUrl.URL_CHECK_DEU, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.i("DEU Response", response.toString());
//                if (checkIfDouble(response)) {
//                    String[] refactorNo = response.split("\\.");
//                    etElecAmount.setText(refactorNo[0]);
////          CustomToast.showMessage(getActivity(), "Your Due amount is INR. " + refactorNo[0] + " ");
//                    Snackbar.make(llLayouts, "Your Due amount is INR. " + refactorNo[0] + " ", Snackbar.LENGTH_LONG).show();
//
//                } else {
//                    try {
//                        JSONObject resObj = new JSONObject(response);
//                        if (resObj.has("ipay_errordesc") && resObj.getString("ipay_errordesc") != null) {
//                            String ipay_errordesc = resObj.getString("ipay_errordesc");
////              CustomToast.showMessage(getActivity(), ipay_errordesc);
//
//                            Snackbar.make(llLayouts, ipay_errordesc, Snackbar.LENGTH_LONG).show();
//                        }
//                    } catch (JSONException e) {
//                        e.printStackTrace();
////            CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
//                        Snackbar.make(llLayouts, getResources().getString(R.string.server_exception2), Snackbar.LENGTH_LONG).show();
//
//                    }
//                }
//
//                loadDlg.dismiss();
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                loadDlg.dismiss();
////        CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));
//                Snackbar.make(llLayouts, NetworkErrorHandler.getMessage(error, getActivity()), Snackbar.LENGTH_LONG).show();
//
//            }
//        }) {
//            @Override
//            protected Map<String, String> getParams() {
//                Map<String, String> params = new HashMap<>();
//                params.put("serviceProvider", serviceProvider);
//                params.put("accountNumber", customerNo);
////                if (etElecCycleNo.getVisibility() == View.VISIBLE) {
////                    params.put("cycleNumber", cycleNo);
////                }
////                if (etElecCityName.getVisibility() == View.VISIBLE) {
////                    params.put("cityName", cycleNo);
////                }
//                Log.i("HEADER", params.toString());
//
//                return params;
//            }
//
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> map = new HashMap<>();
//                map.put("hash", "123");
//
//                String basicAuth = "Basic " + new String(Base64.encode("user:pass".getBytes(), Base64.NO_WRAP));
//                map.put("Authorization", basicAuth);
//
//                return map;
//            }
//        };
//
//        int socketTimeout = 60000;
//        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//        postReq.setRetryPolicy(policy);
//        PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
//    }

    private boolean checkIfDouble(String value) {
        String decimalPattern = "([0-9]*)\\.([0-9]*)";
        return Pattern.matches(decimalPattern, value);
    }

    public void showSuccessDialog() {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(getSuccessMessage(), Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(getSuccessMessage());
        }
        CustomSuccessDialog builder = new CustomSuccessDialog(getActivity(), getResources().getString(R.string.Payment_Successful), result);
        builder.setPositiveButton(getResources().getString(R.string.agree), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                getActivity().finish();
            }
        });
        builder.show();
    }

    public String getSuccessMessage() {
        String source = "";

        if (cycleNo != null && !cycleNo.isEmpty()) {
            source = "<b><font color=#000000>" + getActivity().getResources().getString(R.string.Service_Provider) + "</font></b>" + "<font color=#000000>" + serviceProviderName + ": " + "</font><br>" +
                    "<b><font color=#000000>" + getActivity().getResources().getString(R.string.ACC_NO) + ": " + "</font></b>" + "<font>" + customerNo + "</font><br>" +
                    "<b><font color=#000000>" + getActivity().getResources().getString(R.string.Cycle_No) + ": " + ": " + "</font></b>" + "<font>" + cycleNo + "</font><br>" +
                    "<b><font color=#000000>" + getActivity().getResources().getString(R.string.amount) + ": " + "</font></b>" + "<font>" + amount + "</font><br>";
        } else {
            source = "<b><font color=#000000>" + getActivity().getResources().getString(R.string.Service_Provider) + ": " + "</font></b>" + "<font color=#000000>" + serviceProviderName + "</font><br>" +
                    "<b><font color=#000000>" + getActivity().getResources().getString(R.string.ACC_NO) + ": " + "</font></b>" + "<font>" + customerNo + "</font><br>" +
                    "<b><font color=#000000>" + getActivity().getResources().getString(R.string.amount) + ": " + "</font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + amount + "</font><br>";
        }
        return source;
    }

//    public String generateLoadmoneyMessage() {
//        return "<b><font color=#000000 > <b> Convenience Fee: </b></font></b>" + "<font color=#000000>" + session.getloadMoneyComm() + "%" + "</font><br>" +
//                "<br><b><font color=#ff0000> Please note that there is a Convenience fee of </font></b>" + "<b><font color=#ff0000>" + session.getloadMoneyComm() + "% of total load money amount that will be deducted." + "</font></b><br>" +
//                "<b><font color=#000000> <b> Convenience Fee: </b></font></b>" + "<font color=#000000>" + session.getloadMoneyComm() + "%" + "</font><br>" +
//                "<b><font color=#000000> <b> Load Money: </b></font></b>" + "<font color=#000000>" + "₹ " + a + "</font><br>" +
//                "<b><font color=#000000> <b> Payment gateway charge: </b></font></b>" + "<font color=#000000>" + "₹ " + totalcom + "</font><br>" +
//                "<b><font color=#000000> <b> Total amount: </b></font></b>" + "<font color=#000000>" + "₹ " + totsamns + "</font><br>" +
//                "<br><b><font color=#0F8006> Load money through UPI without any convenience fee.</font></b><br>";
//    }

    public void showBlockDialog() {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(AppMetadata.getBlockSession(), Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(AppMetadata.getBlockSession());
        }
        CustomBlockDialog builder = new CustomBlockDialog(getActivity(), getResources().getString(R.string.Please_contact_customer_care), result);
        builder.setPositiveButton(getResources().getString(R.string.agree), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
                dialog.dismiss();
                getActivity().finish();
            }
        });
        builder.show();
    }

//    public void showCustomDisclaimerDialog() {
//        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(generateLoadmoneyMessage()));
//
//        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//
//
//            }
//        });
//
//        builder.show();
//    }

    @Override
    public void onDetach() {
        super.onDetach();

        //hide keyboard when any fragment of this class has been detached
        showSoftwareKeyboard(false);
    }

    protected void showSoftwareKeyboard(boolean showKeyboard) {
        final Activity activity = getActivity();
        final InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);

        View focusedView = getActivity().getCurrentFocus();


        if (focusedView != null) {
            inputManager.hideSoftInputFromWindow(focusedView.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
//        inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), showKeyboard ? InputMethodManager.SHOW_FORCED : InputMethodManager.HIDE_NOT_ALWAYS);
    }

    public void refresh() {
        //yout code in refresh.
        Log.i("Refresh", "YES");


        etElecACNo.setError(null);
        etElecCycleNo.setError(null);
        etElecAmount.setError(null);
        etElecCityName.setError(null);
        spService.setError(null);

        etElecACNo.setText("");
        etElecCycleNo.setText("");
        etElecAmount.setText("");
        etElecCityName.setText("");
        spService.setText("");

    }

    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(r);
    }

    public void onResume() {
        super.onResume();
        r = new MyReceiver();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(r,
                new IntentFilter("TAG_REFRESH"));
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            ElectricityFragment.this.refresh();
        }
    }


}
