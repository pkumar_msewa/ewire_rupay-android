package in.MadMoveGlobal.fragment.fragmentpaybills;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.orm.query.Select;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.MadMoveGlobal.EwireRuPay.WebViewsActivity;
import in.MadMoveGlobal.adapter.OperatorSpinnerAdapter;
import in.MadMoveGlobal.custom.CustomAlertDialog;
import in.MadMoveGlobal.custom.CustomBlockDialog;
import in.MadMoveGlobal.custom.CustomServiceProviderDialogSearch;
import in.MadMoveGlobal.custom.CustomSuccessDialog;
import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.custom.VerifyMPinDialog;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.metadata.AppMetadata;
import in.MadMoveGlobal.metadata.MenuMetadata;
import in.MadMoveGlobal.model.CircleModel;
import in.MadMoveGlobal.model.OperatorsModel;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.util.CheckLog;
import in.MadMoveGlobal.util.GenerateAPIHeader;
import in.MadMoveGlobal.util.MPinVerifiedListner;
import in.MadMoveGlobal.util.NetworkErrorHandler;
import in.MadMoveGlobal.util.PayingDetailsValidation;
import in.MadMoveGlobal.util.ServicesSelectedListener;
import in.MadMoveGlobal.util.Utility;

//import static in.MadMoveGlobal.metadata.ApiUrl.URL_CIRCLE_OPERATORDTH;


/**
 * Created by Ksf on 3/16/2016.
 */
public class DthFragment extends Fragment implements MPinVerifiedListner {
    private View rootView;
    private Spinner spinner_dth_provider;
    private OperatorSpinnerAdapter operatorSpinnerAdapter;
    private Button btn_pay_dth;
    private MaterialEditText edt_dth_amount;
    private MaterialEditText edt_dth_number, spService;


    private List<CircleModel> circleList;
    private List<OperatorsModel> operatorList;
    private ArrayList<OperatorsModel> operatorLists;
    private static HashMap<String, Integer> operatorIdCode;
    private static HashMap<String, Integer> circleIdCode;
    private LinearLayout llLayouts;

    private View focusView = null;
    private boolean cancel;
    private JSONObject jsonRequest;
    //User Values
    private String operatorCode, selectedFromServiceCode, selectedFromServiceName;

    private String amount, serviceProvider, customerNo, serviceProviderName;

    private LoadingDialog loadDlg;
    private UserModel session = UserModel.getInstance();
    //Volley Tag
    private String tag_json_obj = "json_bill_pay";


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadDlg = new LoadingDialog(getActivity());

        circleList = Select.from(CircleModel.class).list();
        operatorList = Select.from(OperatorsModel.class).list();

        operatorIdCode = new HashMap<>();
        circleIdCode = new HashMap<>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_dth, container, false);
        spinner_dth_provider = (Spinner) rootView.findViewById(R.id.spinner_dth_provider);
        btn_pay_dth = (Button) rootView.findViewById(R.id.btn_pay_dth);
        edt_dth_amount = (MaterialEditText) rootView.findViewById(R.id.edt_dth_amount);
        edt_dth_number = (MaterialEditText) rootView.findViewById(R.id.edt_dth_number);
        spService = (MaterialEditText) rootView.findViewById(R.id.spService);
        llLayouts = (LinearLayout) rootView.findViewById(R.id.llLayouts);
        operatorSpinnerAdapter = new OperatorSpinnerAdapter(getActivity(), android.R.layout.simple_spinner_item, MenuMetadata.getDTH());
        spinner_dth_provider.setAdapter(operatorSpinnerAdapter);

        //DONE CLICK ON VIRTUAL KEYPAD
        edt_dth_amount.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    attemptPayment();
                }
                return false;
            }
        });
        operatorLists = new ArrayList<>();
        btn_pay_dth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptPayment();
            }
        });
        loadOperatorCircleNew();


        spService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final CustomServiceProviderDialogSearch search = new CustomServiceProviderDialogSearch(getActivity(), operatorLists, "from", new ServicesSelectedListener() {
                    @Override
                    public void serviceselect(String type, long servicecode, String serviceName) {

                    }

                    @Override
                    public void serviceFilter(OperatorsModel type) {

                        spService.setText(type.getName());
                        selectedFromServiceCode = type.getCode();
                        selectedFromServiceName = type.getName();

                    }
                });
                search.show();
            }
        });


        return rootView;
    }

    private void attemptPayment() {
        edt_dth_amount.setError(null);
        edt_dth_number.setError(null);
        cancel = false;

        amount = edt_dth_amount.getText().toString();
        customerNo = edt_dth_number.getText().toString();
        serviceProvider = ((OperatorsModel) spinner_dth_provider.getSelectedItem()).code;
        serviceProviderName = ((OperatorsModel) spinner_dth_provider.getSelectedItem()).name;


        if (spService.getText().toString().equals("")) {
//            CustomToast.showMessage(getActivity(), "Select operator to continue");
//            Snackbar.make(llLayouts, "Select operator to continue", Snackbar.LENGTH_LONG).show();
            spService.setError(getResources().getString(R.string.Select_Service_Provider_to_continue));
            return;
        }
        checkCustomerNo(customerNo);
        if (!validateAccount()) {
            return;
        }

        if (!validateAmount()) {
            return;
        }

        checkPayAmount(amount);

        if (cancel) {
            focusView.requestFocus();
        } else {
            showCustomDialog();
        }
    }


    private void checkCustomerNo(String acNo) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkDTH(acNo);
        if (!gasCheckLog.isValid) {
            edt_dth_number.setError(getString(gasCheckLog.msg));
            focusView = edt_dth_number;
            cancel = true;
        }
    }

    private boolean validateAccount() {
        String amt = edt_dth_number.getText().toString().trim();
        if (amt.isEmpty() || amt.equalsIgnoreCase("0000000000") || amt.equals("00000000000") || amt.equals("000000000000") || amt.equals("0000000000000") || amt.equals("00000000000000") || amt.equals("000000000000000") || amt.equals("0000000000000000") || amt.equals("00000000000000000") || amt.equals("000000000000000000") || amt.equals("0000000000000000000") || amt.equals("00000000000000000000")) {
            edt_dth_number.setError(getResources().getString(R.string.Please_enter_valid_DTH_No));
            return false;
        }
        return true;
    }


    private boolean validateAmount() {
        String amt = edt_dth_amount.getText().toString().trim();
        if (amt.isEmpty()) {
            edt_dth_amount.setError(getResources().getString(R.string.Please_enter_valid_amount));
            return false;
        } else if (amt.substring(0, 1).equals("0")) {
            edt_dth_amount.setError(getResources().getString(R.string.Amount_should_not_start_with_0));
            return false;
        }
        return true;
    }

    private void checkPayAmount(String amount) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
        if (!gasCheckLog.isValid) {
            edt_dth_amount.setError(getString(gasCheckLog.msg));
            focusView = edt_dth_amount;
            cancel = true;
        } else if (Integer.valueOf(edt_dth_amount.getText().toString()) < 10) {
            edt_dth_amount.setError(getString(R.string.lessAmount));
            focusView = edt_dth_amount;
            cancel = true;
        }
    }

    public void loadOperatorCircleNew() {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("topUpType", "dthbill");
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }
//
        if (jsonRequest != null) {
            Log.i("LOADCIRCLEOP Request", jsonRequest.toString());
            Log.i("LOADCIRCLEOP URL", ApiUrl.URL_CIRCLE_OPERATOR);

//        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, URL_CIRCLE_OPERATORDTH, (String) null,
//                new Response.Listener<JSONObject>() {
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_CIRCLE_OPERATOR, jsonRequest, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject JsonObj) {
                    loadDlg.dismiss();
                    Log.i("LOADCIRCLEOP Response", JsonObj.toString());
                    /*try {
                        operatorList.clear();
                        circleList.clear();

                        operatorIdCode.clear();
                        circleIdCode.clear();

                        OperatorsModel.deleteAll(OperatorsModel.class);
                        CircleModel.deleteAll(CircleModel.class);
                        //New Implementation

                        String jsonString = JsonObj.getString("details");
                        Log.i("Escape str", jsonEscape(jsonString));

                        JSONObject response = new JSONObject(jsonString);

                        JSONArray operatorArray = response.getJSONArray("operators");

//                        operatorList.add(new OperatorsModel("", "Select your operator", ""));

                        for (int i = 0; i < operatorArray.length(); i++) {
                            JSONObject c = operatorArray.getJSONObject(i);
                            String op_code = c.getString("serviceCode");
                            String op_name = c.getString("name");
                            String op_default_code = c.getString("code");
                            OperatorsModel oModel = new OperatorsModel(op_code, op_name, op_default_code);
                            oModel.save();
                            operatorList.add(oModel);
                            Log.i("OperatorsModel", String.valueOf(operatorList.get(i).getCode()));
                            operatorIdCode.put(op_code, i + 1);
                        }


                        JSONArray circleArray = response.getJSONArray("circles");

                        circleList.add(new CircleModel("", "Select your circle"));
                        for (int i = 0; i < circleArray.length(); i++) {
                            JSONObject c = circleArray.getJSONObject(i);
                            String op_code = c.getString("code");
                            String op_name = c.getString("name");
                            CircleModel cModel = new CircleModel(op_code, op_name);
                            cModel.save();
                            circleList.add(cModel);
                            circleIdCode.put(op_code, i + 1);
                        }


                        if (operatorList != null && operatorList.size() != 0) {
                            try {
                                operatorList.add(0, new OperatorsModel("", "Select your operator", ""));
                                spinnerAdapterOperator = new OperatorSpinnerAdapter(getActivity(), android.R.layout.simple_spinner_item, operatorList);
                                spinnerPrepaidProvider.setAdapter(spinnerAdapterOperator);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        if (circleList != null && circleList.size() != 0) {
                            try {
                                circleList.add(0, new CircleModel("", "Select your circle"));
                                spinnerAdapterCircle = new CircleSpinnerAdapter(getActivity(), android.R.layout.simple_spinner_item, circleList);
                                spinnerPrepaidCircle.setAdapter(spinnerAdapterCircle);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }*/
                    try {
                        operatorList.clear();
                        circleList.clear();

                        operatorIdCode.clear();
                        circleIdCode.clear();

                        OperatorsModel.deleteAll(OperatorsModel.class);
                        CircleModel.deleteAll(CircleModel.class);
                        //New Implementation

//                    String jsonString = JsonObj.getString("response");
//                    Log.i("Escape str", jsonEscape(jsonString));
//
//                    JSONObject response = new JSONObject(jsonString);

                        JSONArray operatorArray = JsonObj.getJSONArray("details");

//                        operatorList.add(new OperatorsModel("", "Select your operator", ""));

                        for (int i = 0; i < operatorArray.length(); i++) {
                            JSONObject c = operatorArray.getJSONObject(i);
                            String op_code = c.getString("code");
                            String op_name = c.getString("name");
                            String op_default_code = c.getString("serviceLogo");
                            OperatorsModel oModel = new OperatorsModel(op_code, op_name, op_default_code);
                            oModel.save();
                            operatorList.add(oModel);
                            operatorLists.add(oModel);
                            Log.i("OperatorsModel", String.valueOf(operatorList.get(i).getCode()));
                            operatorIdCode.put(op_code, i + 1);
                        }
                        spService.setHint(getResources().getString(R.string.Service_Provider));

//                    JSONArray circleArray = response.getJSONArray("circles");
//
//                        circleList.add(new CircleModel("", "Select your circle"));
//                    for (int i = 0; i < circleArray.length(); i++) {
//                        JSONObject c = circleArray.getJSONObject(i);
//                        String op_code = c.getString("code");
//                        String op_name = c.getString("name");
//                        CircleModel cModel = new CircleModel(op_code, op_name);
//                        cModel.save();
//                        circleList.add(cModel);
//                        circleIdCode.put(op_code, i + 1);
//                    }


//                            if (operatorList != null && operatorList.size() != 0) {
//                                try {
//                                    operatorList.add(0, new OperatorsModel("", "Select your operator", ""));
//                                    operatorSpinnerAdapter = new OperatorSpinnerAdapter(getActivity(), android.R.layout.simple_spinner_item, operatorList);
//                                    spinner_dth_provider.setAdapter(operatorSpinnerAdapter);
//                                } catch (Exception e) {
//                                    e.printStackTrace();
//                                }
//                            }

//                    if (circleList != null && circleList.size() != 0) {
//                        try {
//                            circleList.add(0, new CircleModel("", "Select your circle"));
//                            spinnerAdapterCircle = new CircleSpinnerAdapter(getActivity(), android.R.layout.simple_spinner_item, circleList);
//                            spinnerPrepaidCircle.setAdapter(spinnerAdapterCircle);
//
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }

                    } catch (JSONException e) {
                        e.printStackTrace();
//                            Toast.makeText(getActivity(), getResources().getString(R.string.server_exception2), Toast.LENGTH_SHORT).show();
                        Snackbar.make(llLayouts, getResources().getString(R.string.server_exception2), Snackbar.LENGTH_LONG).show();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    error.printStackTrace();
//                Toast.makeText(getActivity(), getResources().getString(R.string.server_exception), Toast.LENGTH_SHORT).show();
                    Snackbar.make(llLayouts, getResources().getString(R.string.server_exception), Snackbar.LENGTH_LONG).show();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");


//                    String basicAuth = "Basic " + new String(Base64.encode("user:pass".getBytes(), Base64.NO_WRAP));
//                    map.put("Authorization", basicAuth);
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);

        }
    }

    private void checkUpBalance() {
        loadDlg.show();
        Utility.hideKeyboard(getActivity());
        jsonRequest = new JSONObject();
        try {
//            jsonRequest.put("serviceProvider", serviceProvider);
//            jsonRequest.put("dthNo", edt_dth_number.getText().toString());
//            jsonRequest.put("amount", amount);
//            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("serviceCode", selectedFromServiceCode);
            jsonRequest.put("dthNumber", edt_dth_number.getText().toString());
            jsonRequest.put("amount", edt_dth_amount.getText().toString());
            jsonRequest.put("topUpType", "DTH");
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            Log.i("JsonRequest", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_PAYMENTS, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.i("DTH RESPONSE", response.toString());
                        String code = response.getString("code");

                        if (code != null && code.equals(ApiUrl.C_SUCCESS)) {
                            showSuccessDialog();

//                        } else if (code != null && code.equals("T01")) {
//                            loadDlg.dismiss();
//                            JSONObject dtoObject = response.getJSONObject("dto");
//                            String splitAmount = dtoObject.getString("splitAmount");
//                            String message = response.getString("message");
//                            Snackbar.make(llLayouts, "You have insufficient balance. Please wait while you are redirected to load money.", Snackbar.LENGTH_LONG).show();
//
////                            CustomToast.showMessage(getActivity(), "You have insufficient balance. Please wait while you are redirected to load money.");
//                            Intent mainMenuDetailActivity = new Intent(getActivity(), LoadMoneyFragment.class);
////                            mainMenuDetailActivity.putExtra(AppMetadata.FRAGMENT_TYPE, "LoadMoney");
//                            mainMenuDetailActivity.putExtra("AutoFill", "yes");
//                            if (!splitAmount.isEmpty()) {
//                                mainMenuDetailActivity.putExtra("TYPE", "BILLPAY");
//                                mainMenuDetailActivity.putExtra("SUBTYPE", "DTH");
//                                mainMenuDetailActivity.putExtra("splitAmount", splitAmount);
//                                mainMenuDetailActivity.putExtra("URL", ApiUrl.URL_DTH_PAYMENT);
//                                mainMenuDetailActivity.putExtra("REQUESTOBJECT", jsonRequest.toString());
//                                mainMenuDetailActivity.putExtra("spiltPay", true);
//                                startActivity(mainMenuDetailActivity);
//                            } else {
////                                CustomToast.showMessage(getActivity(), "split amount can't be empty");
//                                Snackbar.make(llLayouts, "split amount can't be empty", Snackbar.LENGTH_LONG).show();
//
//                            }


                        } else if (code != null && code.equals(ApiUrl.C_SESSION_EXPIRE)) {
                            loadDlg.dismiss();
                            showInvalidSessionDialog();
                        } else if (code != null && code.equals(ApiUrl.C_SESSION_BLOCKED)) {
                            loadDlg.dismiss();
                            showBlockDialog();
                        } else {
                            loadDlg.dismiss();
                            if (response.has("message") && response.getString("message") != null) {
                                String message = response.getString("message");
//                                CustomToast.showMessage(getActivity(), message);
                                Snackbar.make(llLayouts, message, Snackbar.LENGTH_LONG).show();

                            } else {
//                                CustomToast.showMessage(getActivity(), "Error message is null");
                                Snackbar.make(llLayouts, getResources().getString(R.string.Error_message_is_null), Snackbar.LENGTH_LONG).show();

                            }

                        }

                    } catch (JSONException e) {
                        loadDlg.dismiss();
//                        CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
                        Snackbar.make(llLayouts, getResources().getString(R.string.server_exception2), Snackbar.LENGTH_LONG).show();

                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    error.printStackTrace();
//                    CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
                    Snackbar.make(llLayouts, NetworkErrorHandler.getMessage(error, getActivity()), Snackbar.LENGTH_LONG).show();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", GenerateAPIHeader.getHeader(ApiUrl.H_DTH + session.getUserSessionId()));
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    private void promotePayment() {
        loadDlg.show();
        jsonRequest = new JSONObject();

        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("serviceCode", selectedFromServiceCode);
            jsonRequest.put("dthNumber", edt_dth_number.getText().toString());
            jsonRequest.put("amount", edt_dth_amount.getText().toString());
            jsonRequest.put("topUpType", "DTH");


        } catch (Exception e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("Prepaid Request", jsonRequest.toString());
            Log.i("Prepaid URL", ApiUrl.URL_PREPAID_TOPUP);
            StringRequest postReq = new StringRequest(Request.Method.POST, ApiUrl.URL_PREPAID_TOPUP, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.i("DEU Response", response.toString());


                    loadDlg.dismiss();


                    Intent intent = new Intent(getActivity(), WebViewsActivity.class);
                    intent.putExtra("TYPE", "BILLPAY");
                    intent.putExtra("SUBTYPE", "DTH");
                    intent.putExtra("amount", amount);
                    intent.putExtra("response", response);
                    intent.putExtra("REQUESTOBJECT", jsonRequest.toString());
                    intent.putExtra("URL", ApiUrl.URL_PREPAID_TOPUP);
                    startActivity(intent);


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
//                CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));
                    Snackbar.make(llLayouts, getResources().getString(R.string.server_exception), Snackbar.LENGTH_LONG).show();
                    loadDlg.dismiss();
                }
            }) {
//            @Override
//            protected Map<String, String> getParams() {
//                Map<String, String> params = new HashMap<>();
//
//
//                params.put("sessionId", session.getUserSessionId());
//                params.put("amount", etPrepaidAmount.getText().toString());
//                params.put("serviceCode", serviceProvider);
//                params.put("topUpType", "Prepaid");
//                params.put("area", areacode);
//                params.put("mobileNumber", etPrepaidNo.getText().toString());
//
//
////                if (etLandLineAcNo.getVisibility() == View.VISIBLE) {
////                    params.put("accountNumber", acNo);
////                }
//                Log.i("HEADER", params.toString());
//                return params;
//            }

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return jsonRequest == null ? null : jsonRequest.toString().getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", jsonRequest, "utf-8");
                        return null;
                    }
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("hash", "123");
                    params.put("Content-Type", "application/json");
                    return params;
                }
            };

            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }


    public void showCustomDialog() {

        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title, Html.fromHtml(generateMessage()));
        builder.setPositiveButton(getResources().getString(R.string.agree), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
//                if (session.isMPin() && Integer.valueOf(amount) >= getResources().getInteger(R.integer.transactionValue)) {
//                    showMPinDialog();
//                } else {
//                    checkUpBalance();
//                    loadDlg.show();
//                    promoteTopUpNew();
//                }
                checkUpBalance();
            }
        });
        builder.setNegativeButton(getResources().getString(R.string.Dismiss), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public void showMPinDialog() {
        VerifyMPinDialog builder = new VerifyMPinDialog(getActivity(), this);
        builder.setNegativeButton(getResources().getString(R.string.Dismiss), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
    }

    public String generateMessage() {
        return "<b><font color=#000000>"+getActivity().getResources().getString(R.string.Service_Provider)+": "+"</font></b>" + "<font color=#000000>" + selectedFromServiceName + "</font><br>" +
                "<b><font color=#000000>"+getActivity().getResources().getString(R.string.DTH_No)+"</font></b>" + "<font>" + customerNo + "</font><br>" +
                "<b><font color=#000000>"+getActivity().getResources().getString(R.string.amount)+": "+"</font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + amount + "</font><br><br>" +
                "<b><font color=#ff0000>"+getActivity().getResources().getString(R.string.Are_you_sure_you_want_to_proceed)+": "+"</font></b><br>";
    }

    private void sendRefresh() {
        getActivity().finish();
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "3");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }


    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton(getResources().getString(R.string.agree), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton(getResources().getString(R.string.agree), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    public void showSuccessDialog() {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(getSuccessMessage(), Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(getSuccessMessage());
        }
        CustomSuccessDialog builder = new CustomSuccessDialog(getActivity(), getResources().getString(R.string.Payment_Successful), result);
        builder.setPositiveButton(getResources().getString(R.string.agree), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                sendRefresh();

            }
        });
        builder.show();
    }

    public String getSuccessMessage() {
        String source =
                "<b><font color=#000000>"+getActivity().getResources().getString(R.string.Service_Provider)+": "+"</font></b>" + "<font color=#000000>" + selectedFromServiceName + "</font><br>" +
                        "<b><font color=#000000>"+getActivity().getResources().getString(R.string.DTH_No)+"</font></b>" + "<font>" + customerNo + "</font><br>" +
                        "<b><font color=#000000>"+getActivity().getResources().getString(R.string.amount)+": "+"</font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + amount + "</font><br><br>";
        return source;
    }


    public void showBlockDialog() {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(AppMetadata.getBlockSession(), Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(AppMetadata.getBlockSession());
        }
        CustomBlockDialog builder = new CustomBlockDialog(getActivity(), getResources().getString(R.string.Please_contact_customer_care), result);
        builder.setPositiveButton(getResources().getString(R.string.agree), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
                dialog.dismiss();
                getActivity().finish();
            }
        });
        builder.show();
    }


    @Override
    public void verifiedCompleted() {
        checkUpBalance();
    }

    @Override
    public void sessionInvalid() {
        showInvalidSessionDialog();
    }

}
