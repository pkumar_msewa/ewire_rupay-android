package in.MadMoveGlobal.fragment.fragmenttravel;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.rengwuxian.materialedittext.MaterialEditText;

import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.util.CheckLog;
import in.MadMoveGlobal.util.PayingDetailsValidation;

/**
 * Created by Ksf on 9/26/2016.
 */
public class HotelTravelActivity extends AppCompatActivity {

    private MaterialEditText etHotelLocation, etHotelCheckIn, etHotelCheckOut, etHotelRooms, etHotelGuestNo;
    private View focusView = null;
    private boolean cancel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {

        super.onCreate(savedInstanceState, persistentState);
        setContentView(R.layout.activity_travels_hotel);
        etHotelLocation = (MaterialEditText) findViewById(R.id.etHotelLocation);
        etHotelCheckIn = (MaterialEditText) findViewById(R.id.etHotelCheckIn);
        etHotelCheckOut = (MaterialEditText)findViewById(R.id.etHotelCheckOut);
        etHotelRooms = (MaterialEditText) findViewById(R.id.etHotelRooms);
        etHotelGuestNo = (MaterialEditText) findViewById(R.id.etHotelGuestNo);

        Button btnSearchHotel = (Button) findViewById(R.id.btnSearchHotel);
        btnSearchHotel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkValidity();
            }
        });

    }

    private void checkValidity() {
        etHotelLocation.setError(null);
        etHotelCheckIn.setError(null);
        etHotelCheckOut.setError(null);
        etHotelRooms.setError(null);
        etHotelGuestNo.setError(null);
        cancel = false;

        checkLocation(etHotelLocation.getText().toString());
        checkCheckIn(etHotelCheckIn.getText().toString());
        checkCheckOut(etHotelCheckOut.getText().toString());
        checkRooms(etHotelRooms.getText().toString());
        checkGuestNo(etHotelGuestNo.getText().toString());

        if (cancel) {
            focusView.requestFocus();
        } else {
            promoteSearch();
        }
    }

    private void checkLocation(String hotelLocation) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(hotelLocation);
        if (!gasCheckLog.isValid) {
            etHotelLocation.setError(getString(gasCheckLog.msg));
            focusView = etHotelLocation;
            cancel = true;
        }
    }

    private void checkCheckIn(String hotelCheckIn) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(hotelCheckIn);
        if (!gasCheckLog.isValid) {
            etHotelCheckIn.setError(getString(gasCheckLog.msg));
            focusView = etHotelCheckIn;
            cancel = true;
        }
    }

    private void checkCheckOut(String hotelCheckOut) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(hotelCheckOut);
        if (!gasCheckLog.isValid) {
            etHotelCheckOut.setError(getString(gasCheckLog.msg));
            focusView = etHotelCheckOut;
            cancel = true;
        }
    }

    private void checkRooms(String hotelRooms) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(hotelRooms);
        if (!gasCheckLog.isValid) {
            etHotelRooms.setError(getString(gasCheckLog.msg));
            focusView = etHotelRooms;
            cancel = true;
        }
    }

    private void checkGuestNo(String hotelGuest) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(hotelGuest);
        if (!gasCheckLog.isValid) {
            etHotelGuestNo.setError(getString(gasCheckLog.msg));
            focusView = etHotelGuestNo;
            cancel = true;
        }
    }


    private void promoteSearch(){
        CustomToast.showMessage(this,"Ready to search");
    }
}
