package in.MadMoveGlobal.fragment.fragmenttravel;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;

import com.rengwuxian.materialedittext.MaterialEditText;

import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.util.CheckLog;
import in.MadMoveGlobal.util.PayingDetailsValidation;

/**
 * Created by Ksf on 9/26/2016.
 */
public class CabTravelActivity extends AppCompatActivity {

    private RadioButton rbCabOneWay,rbCabRoundTrip;
    private MaterialEditText etCabFrom,etCabTo,etCabPickDate,etCabPickTime,etCabDays;
    private Button btnSearchCab;

    private View focusView = null;
    private boolean cancel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        setContentView(R.layout.activity_travels_cab);
        etCabFrom = (MaterialEditText)findViewById(R.id.etCabFrom);
        etCabTo = (MaterialEditText) findViewById(R.id.etCabTo);
        etCabPickDate = (MaterialEditText) findViewById(R.id.etCabPickDate);
        etCabPickTime = (MaterialEditText)findViewById(R.id.etCabPickTime);
        etCabDays = (MaterialEditText)findViewById(R.id.etCabDays);

        rbCabOneWay = (RadioButton) findViewById(R.id.rbCabOneWay);
        rbCabRoundTrip = (RadioButton) findViewById(R.id.rbCabRoundTrip);

        Button btnSearchCab = (Button) findViewById(R.id.btnSearchCab);
        btnSearchCab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkValidity();
            }
        });

    }

    private void checkValidity() {
        etCabFrom.setError(null);
        etCabTo.setError(null);
        etCabPickDate.setError(null);
        etCabPickTime.setError(null);
        etCabDays.setError(null);
        cancel = false;

        checkFrom(etCabFrom.getText().toString());
        checkTo(etCabTo.getText().toString());
        checkDate(etCabPickDate.getText().toString());
        checkTime(etCabPickTime.getText().toString());
        checkDays(etCabDays.getText().toString());

        if (cancel) {
            focusView.requestFocus();
        } else {
            promoteSearch();
        }
    }

    private void checkFrom(String cabFrom) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(cabFrom);
        if (!gasCheckLog.isValid) {
            etCabFrom.setError(getString(gasCheckLog.msg));
            focusView = etCabFrom;
            cancel = true;
        }
    }


    private void checkTo(String cabTo) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(cabTo);
        if (!gasCheckLog.isValid) {
            etCabTo.setError(getString(gasCheckLog.msg));
            focusView = etCabTo;
            cancel = true;
        }
    }

    private void checkDate(String cabDate) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(cabDate);
        if (!gasCheckLog.isValid) {
            etCabPickDate.setError(getString(gasCheckLog.msg));
            focusView = etCabPickDate;
            cancel = true;
        }
    }

    private void checkDays(String cabDays) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(cabDays);
        if (!gasCheckLog.isValid) {
            etCabDays.setError(getString(gasCheckLog.msg));
            focusView = etCabDays;
            cancel = true;
        }
    }

    private void checkTime(String cabTime) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(cabTime);
        if (!gasCheckLog.isValid) {
            etCabPickTime.setError(getString(gasCheckLog.msg));
            focusView = etCabPickTime;
            cancel = true;
        }
    }

    private void promoteSearch(){
        CustomToast.showMessage(this,"Ready to search cab");
    }

}
