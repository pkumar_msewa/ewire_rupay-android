//package in.paulpay.fragment.fragmenttravel;
//
//import android.content.Context;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.content.SharedPreferences;
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.v4.content.LocalBroadcastManager;
//import android.support.v7.app.AlertDialog;
//import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.Toolbar;
//import android.text.Html;
//import android.text.Spanned;
//import android.util.Log;
//import android.view.KeyEvent;
//import android.view.View;
//import android.view.inputmethod.EditorInfo;
//import android.widget.Button;
//import android.widget.ImageButton;
//import android.widget.RadioButton;
//import android.widget.TextView;
//
//import com.android.volley.AuthFailureError;
//import com.android.volley.DefaultRetryPolicy;
//import com.android.volley.Request;
//import com.android.volley.RequestQueue;
//import com.android.volley.Response;
//import com.android.volley.RetryPolicy;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.HurlStack;
//import com.android.volley.toolbox.JsonObjectRequest;
//import com.android.volley.toolbox.StringRequest;
//import com.android.volley.toolbox.Volley;
//import com.ebs.android.sdk.Config.Encryption;
//import com.ebs.android.sdk.Config.Mode;
//import com.ebs.android.sdk.EBSPayment;
//import com.ebs.android.sdk.PaymentRequest;
//import com.rengwuxian.materialedittext.MaterialEditText;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.security.KeyManagementException;
//import java.security.NoSuchAlgorithmException;
//import java.text.DecimalFormat;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.Map;
//
//import javax.net.ssl.SSLSocketFactory;
//
//import in.paulpay.custom.CustomAlertDialog;
//import in.paulpay.custom.CustomDisclaimerDialog;
//import in.paulpay.custom.CustomSuccessDialog;
//import in.paulpay.custom.CustomToast;
//import in.paulpay.custom.LoadingDialog;
//import in.paulpay.metadata.ApiUrl;
//import in.paulpay.metadata.AppMetadata;
//import in.paulpay.model.UserModel;
//import in.paulpay.util.CheckLog;
//import in.paulpay.util.NetworkErrorHandler;
//import in.paulpay.util.PayingDetailsValidation;
//import in.paulpay.util.TLSSocketFactory;
//import in.paulpay.vpayqwiktest.PayQwikApplication;
//import in.paulpay.vpayqwiktest.R;
//import in.paulpay.vpayqwiktest.activity.MainActivity;
//import in.paulpay.vpayqwiktest.activity.flightinneracitivty.FlightVnetWebViewActivity;
//
//
//public class FlightPaymentActivity extends AppCompatActivity {
//
//    private View rootView;
//    private MaterialEditText etLoadMoneyAmount;
//    private Button btnLoadMoney;
//    private RadioButton rbLoadMoneyVBank, rbLoadMoneyOther;
//    private View focusView = null;
//    private boolean cancel;
//    private String amount = null;
//    AlertDialog.Builder payDialog;
//    private UserModel session = UserModel.getInstance();
//    private LoadingDialog loadDlg;
//    private RequestQueue rq;
//    private boolean isVBank = true;
//    private String inValidMessage = "";
//    private String tag_json_obj = "load_money";
//    private JSONObject jsonRequest;
//    String autoFill;
//    double loadAmount;
//    private String jsonBokking;
//    private SharedPreferences loadMoneyPref;
//    private RadioButton rbLoadMoneyWallet;
//    private boolean roundTrip;
//    private boolean contingFlight;
//    private Toolbar toolbar;
//    private ImageButton ivBackBtn;
//
//    @Override
//    public void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.fragment_flght_load_money);
//        toolbar = (Toolbar) findViewById(R.id.toolbars);
//        ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtns);
//        setSupportActionBar(toolbar);
//        ivBackBtn.setVisibility(View.VISIBLE);
//        ivBackBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                onBackPressed();
//            }
//        });
//        payDialog = new AlertDialog.Builder(FlightPaymentActivity.this, R.style.AppCompatAlertDialogStyle);
//        loadDlg = new LoadingDialog(FlightPaymentActivity.this);
//        rbLoadMoneyWallet = (RadioButton) findViewById(R.id.rbLoadMoneyWallet);
//        try {
//            loadMoneyPref = getSharedPreferences("type", Context.MODE_PRIVATE);
//        } catch (NullPointerException e) {
//
//        }
//        try {
//            final SSLSocketFactory sslSocketFactory = new TLSSocketFactory();
//            rq = Volley.newRequestQueue(FlightPaymentActivity.this, new HurlStack(null, sslSocketFactory));
//        } catch (KeyManagementException | NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        }
//
//        btnLoadMoney = (Button) findViewById(R.id.btnLoadMoney);
//        etLoadMoneyAmount = (MaterialEditText) findViewById(R.id.etLoadMoneyAmount);
//        rbLoadMoneyVBank = (RadioButton) findViewById(R.id.rbLoadMoneyVBank);
//        rbLoadMoneyOther = (RadioButton) findViewById(R.id.rbLoadMoneyOther);
//
//
//        String loadAmountString = String.valueOf(getIntent().getDoubleExtra("Amount", 0));
//        jsonBokking = getIntent().getStringExtra("flightbooking");
//        roundTrip = getIntent().getBooleanExtra("roundTrip", false);
//        contingFlight = getIntent().getBooleanExtra("conting", false);
//        Log.i("value", String.valueOf(contingFlight));
//        Log.i("jsonPayment", jsonBokking.toString());
//        loadAmount = Math.ceil(Double.parseDouble(loadAmountString));
//        DecimalFormat format = new DecimalFormat("0.#");
//        amount = loadAmountString;
//        etLoadMoneyAmount.setText(String.valueOf(format.format(loadAmount)));
////
//        btnLoadMoney.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (rbLoadMoneyVBank.isChecked()) {
//                    isVBank = true;
//                } else if (rbLoadMoneyOther.isChecked()) {
//                    isVBank = false;
//                } else if (rbLoadMoneyWallet.isChecked()) {
//                    isVBank = false;
//                } else {
//                    isVBank = true;
//                }
//                attemptLoad();
//
//            }
//        });
//
//
//        //DONE CLICK ON VIRTUAL KEYPAD
//        etLoadMoneyAmount.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                if (actionId == EditorInfo.IME_ACTION_DONE) {
//                    if (rbLoadMoneyVBank.isChecked()) {
//                        isVBank = true;
//                    } else if (rbLoadMoneyOther.isChecked()) {
//                        isVBank = false;
//                    } else {
//                        isVBank = true;
//                    }
//                    attemptLoad();
//                }
//                return false;
//            }
//        });
//
//    }
//
//
//    private void attemptLoad() {
//        etLoadMoneyAmount.setError(null);
//        cancel = false;
//        amount = etLoadMoneyAmount.getText().toString();
//        checkPayAmount(amount);
////        checkUserType();
//
//        if (cancel) {
//            focusView.requestFocus();
//        } else {
//            if (isVBank) {
//                checkTrxTime();
//            } else if (rbLoadMoneyWallet.isChecked()) {
//                JSONObject jsonRequest = null;
//                try {
//                    jsonRequest = new JSONObject(jsonBokking);
//                    jsonRequest.put("paymentMethod","Wallet");
//                    longLog(jsonRequest.toString());
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                    jsonRequest = null;
//                }
//
//                String URL = "";
//
//                loadDlg.show();
//                JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_FLIGHT_BOOKING, jsonRequest, new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        try {
//                            Log.i("CheckOut Resonse", response.toString());
//                            String code = response.getString("code");
//                            loadDlg.dismiss();
//                            if (code != null && code.equals("S00")) {
//                                sendRefresh();
//                                loadDlg.dismiss();
//                                showSuccessDialog();
//
//                            } else if (code != null && code.equals("F03")) {
//                                loadDlg.dismiss();
//                                showInvalidSessionDialog();
//                            } else {
//                                String message = response.getString("message");
//                                loadDlg.dismiss();
//                                CustomToast.showMessage(FlightPaymentActivity.this, message);
//
//                            }
//                        } catch (JSONException e) {
//                            loadDlg.dismiss();
//                            CustomToast.showMessage(FlightPaymentActivity.this, getResources().getString(R.string.server_exception2));
//                            e.printStackTrace();
//                        }
//                    }
//                }, new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        loadDlg.dismiss();
//                        CustomToast.showMessage(FlightPaymentActivity.this, NetworkErrorHandler.getMessage(error, FlightPaymentActivity.this));
//                        error.printStackTrace();
//                    }
//                }) {
//                    @Override
//                    public Map<String, String> getHeaders() throws AuthFailureError {
//                        HashMap<String, String> map = new HashMap<>();
//                        return map;
//                    }
//
//                };
////
//                int socketTimeout = 6000000;
//                RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//                postReq.setRetryPolicy(policy);
//                PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
////                }
//
//            } else {
//                showCustomDisclaimerDialog();
//            }
//        }
//    }
//
//    public void showCustomDisclaimerDialog() {
//        CustomDisclaimerDialog builder = new CustomDisclaimerDialog(FlightPaymentActivity.this);
//        builder.setPositiveButton("Accept", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                checkTrxTime();
//            }
//        });
//        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                dialog.dismiss();
//            }
//        });
//        builder.show();
//    }
//
//    public void showNonKYCDialog() {
//        CustomAlertDialog builder = new CustomAlertDialog(FlightPaymentActivity.this, R.string.dialog_title2, Html.fromHtml(generateKYCMessage()));
//        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                dialog.dismiss();
//            }
//        });
//        builder.show();
//    }
//
//
//    private void checkPayAmount(String amount) {
//        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
//        try {
//            if (!gasCheckLog.isValid) {
//                etLoadMoneyAmount.setError(getString(gasCheckLog.msg));
//                focusView = etLoadMoneyAmount;
//                cancel = true;
//            }
//        } catch (NumberFormatException e) {
//            e.printStackTrace();
//        }
//    }
//
//    private boolean checkUserType() {
//        if (amount != null && !amount.isEmpty()) {
//            if (Integer.valueOf(amount) > 10000) {
//                focusView = etLoadMoneyAmount;
//                cancel = true;
//                if (session.getUserAcName().equals("Non-KYC")) {
//                    showNonKYCDialog();
//                    return true;
//                } else {
//                    return false;
//                }
//            } else {
//                return false;
//            }
//        } else {
//            return false;
//        }
//
//    }
//
//    public String generateMessage() {
//        String source = "<b><font color=#000000> Amount to load: </font></b>" + "<font color=#000000>" + amount + "</font><br>" +
//                "<br><b><font color=#ff0000> Are you sure you want to proceed?</font></b><br>";
//        return source;
//    }
//
//
//    public String generateKYCMessage() {
//        return "<b><font color=#000000> Amount to load: </font></b>" + "<font color=#000000>" + amount + "</font><br>" +
//                "<br><b><font color=#ff0000> Sorry you cannot load more than 10,000 at a time.</font></b><br>" +
//                "<br><b><font color=#ff0000> Please enter 10,000 or lesser amount to continue.</font></b><br>";
//    }
//
//    public void checkTrxTime() {
//        loadDlg.show();
//        jsonRequest = new JSONObject();
//        try {
//            jsonRequest.put("sessionId", session.getUserSessionId());
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//            jsonRequest = null;
//        }
//
//        if (jsonRequest != null) {
//            Log.i("TRXTIMEURL", ApiUrl.URL_VALIDATE_TRX_TIME);
//            Log.i("TRXTIMEREQ", jsonRequest.toString());
//            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_VALIDATE_TRX_TIME, jsonRequest, new Response.Listener<JSONObject>() {
//                @Override
//                public void onResponse(JSONObject response) {
//                    Log.i("TRXTIMERES", response.toString());
//
//                    try {
//                        String message = response.getString("message");
//                        String code = response.getString("code");
//                        String msg = response.getString("message");
//                        if (code != null && code.equals("S00")) {
//                            loadDlg.dismiss();
//                            boolean success = response.getBoolean("success");
//                            if (success) {
//                                verifyTransaction(amount);
//                            } else {
//                                CustomToast.showMessage(FlightPaymentActivity.this, message);
//                            }
//                        } else if (code != null && code.equals("F03")) {
//                            loadDlg.dismiss();
//                            showInvalidSessionDialog();
//                        } else {
//                            loadDlg.dismiss();
//                            CustomToast.showMessage(FlightPaymentActivity.this, message);
//                        }
//
//                    } catch (JSONException e) {
//                        loadDlg.dismiss();
//                        CustomToast.showMessage(FlightPaymentActivity.this, getResources().getString(R.string.server_exception2));
//                        e.printStackTrace();
//                    }
//                }
//            }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    loadDlg.dismiss();
//                    CustomToast.showMessage(FlightPaymentActivity.this, getResources().getString(R.string.server_exception));
//
//                    error.printStackTrace();
//
//                }
//            }) {
//                @Override
//                public Map<String, String> getHeaders() throws AuthFailureError {
//                    HashMap<String, String> map = new HashMap<>();
//                    map.put("hash", "1234");
//                    return map;
//                }
//
//            };
//            int socketTimeout = 60000;
//            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//            postReq.setRetryPolicy(policy);
//            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
//
//        }
//
//    }
//
//    private void verifyTransaction(final String amount) {
////        loadDlg.show();
////        StringRequest postReq = new StringRequest(Request.Method.POST, ApiUrl.URL_VALIDATE_TRANSACTION, new Response.Listener<String>() {
////            @Override
////            public void onResponse(String responseString) {
////                try {
////
////                    JSONObject response = new JSONObject(responseString);
////
////                    String code = response.getString("code");
////                    inValidMessage = response.getString("message");
////                    if (code != null && code.equals("S00")) {
////                        loadDlg.dismiss();
////                        String details = response.getString("details");
////                        JSONObject jsonDetails = new JSONObject(details);
////                        boolean isValid = jsonDetails.getBoolean("valid");
////                        if (isValid) {
//        if (isVBank) {
//            Intent loadMoneyIntent = new Intent(FlightPaymentActivity.this, FlightVnetWebViewActivity.class);
//            loadMoneyIntent.putExtra("amountToLoad", amount);
//            loadMoneyIntent.putExtra("isVBank", isVBank);
//            loadMoneyIntent.putExtra("jsonBokking", jsonBokking);
//            loadMoneyIntent.putExtra("roundTrip", roundTrip);
//            loadMoneyIntent.putExtra("conting", contingFlight);
//            startActivity(loadMoneyIntent);
//        } else {
//            //Initiate and call EBS KIT
//            loadDlg.show();
//            initialLoadMoneyEBS(amount);
//        }
////                        } else {
////                            inValidMessage = jsonDetails.getString("message");
////                            showInvalidTranDialog();
////                        }
////                    } else if (code != null && code.equals("F03")) {
////                        loadDlg.dismiss();
////                        showInvalidSessionDialog();
////                    } else {
////                        loadDlg.dismiss();
////                        CustomToast.showMessage(FlightPaymentActivity.this, inValidMessage);
////                    }
////
////                } catch (JSONException e) {
////                    loadDlg.dismiss();
////                    e.printStackTrace();
////                    CustomToast.showMessage(FlightPaymentActivity.this, getResources().getString(R.string.server_exception2));
////
////                }
////            }
////        }, new Response.ErrorListener() {
////            @Override
////            public void onErrorResponse(VolleyError error) {
////                loadDlg.dismiss();
////                error.printStackTrace();
////                CustomToast.showMessage(FlightPaymentActivity.this, NetworkErrorHandler.getMessage(error, FlightPaymentActivity.this));
////
////            }
////        }) {
////            @Override
////            protected Map<String, String> getParams() {
////                Map<String, String> params = new HashMap<>();
////                params.put("sessionId", session.getUserSessionId());
////                params.put("amount", amount);
////                return params;
////            }
////
////        };
////        int socketTimeout = 60000;
////        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
////        postReq.setRetryPolicy(policy);
////        rq.add(postReq);
//
//    }
//
//    public void showInvalidSessionDialog() {
//        CustomAlertDialog builder = new CustomAlertDialog(FlightPaymentActivity.this, R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
//        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                sendLogout();
//            }
//        });
//        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                dialog.dismiss();
//
//            }
//        });
//        builder.show();
//    }
//
//    private void sendLogout() {
//        Intent intent = new Intent("setting-change");
//        intent.putExtra("updates", "4");
//        LocalBroadcastManager.getInstance(FlightPaymentActivity.this).sendBroadcast(intent);
//    }
//
//    private void showInvalidTranDialog() {
//        CustomAlertDialog builder = new CustomAlertDialog(FlightPaymentActivity.this, R.string.dialog_title2, inValidMessage);
//        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                dialog.dismiss();
//            }
//        });
//        builder.show();
//    }
//
//    public void initialLoadMoneyEBS(final String amount) {
//        Log.i("INITIATE URL", ApiUrl.URL_FLIGHT_INITIATE_LOAD_MONEY);
//        SharedPreferences.Editor editor = loadMoneyPref.edit();
//        editor.clear();
//        editor.putString("type", "flight");
//        editor.putString("jsonBooking", jsonBokking);
//        editor.putBoolean("roundTrip", roundTrip);
//        editor.putBoolean("conting", contingFlight);
//        editor.apply();
//        StringRequest postReq = new StringRequest(Request.Method.POST, ApiUrl.URL_FLIGHT_INITIATE_LOAD_MONEY, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                loadDlg.dismiss();
//                Log.i("Initiate Response", response.toString());
//                try {
//                    JSONObject resObj = new JSONObject(response);
//                    boolean successMsg = resObj.getBoolean("success");
//
//                    String msg = resObj.getString("message");
//                    if (successMsg) {
//                        String referenceNo = resObj.getString("referenceNo");
//                        String currency = resObj.getString("currency");
//                        String description = resObj.getString("description");
//                        String name = resObj.getString("name");
//                        String email = resObj.getString("email");
//                        String address = resObj.getString("address");
//                        String cityName = resObj.getString("city");
//                        String stateName = resObj.getString("state");
//                        String countryName = resObj.getString("country");
//                        String postalCode = resObj.getString("postalCode");
//                        String shipName = resObj.getString("shipName");
//                        String shipAddress = resObj.getString("shipAddress");
//                        String shipCity = resObj.getString("shipCity");
//                        String shipState = resObj.getString("shipState");
//                        String shipCountry = resObj.getString("shipCountry");
//                        String shipPostalCode = resObj.getString("shipPostalCode");
//                        String shipPhone = resObj.getString("shipPhone");
//
//                        int ACC_ID = 20696;
//                        String SECRET_KEY = "6496e4db9ebf824ffe2269afee259447";
//                        String HOST_NAME = getResources().getString(R.string.hostname);
//
//                        PaymentRequest.getInstance().setTransactionAmount(amount);
//                        PaymentRequest.getInstance().setAccountId(ACC_ID);
//                        PaymentRequest.getInstance().setSecureKey(SECRET_KEY);
//
//                        PaymentRequest.getInstance().setReferenceNo(referenceNo);
//                        PaymentRequest.getInstance().setBillingEmail(email);
//                        PaymentRequest.getInstance().setFailureid("1");
//                        PaymentRequest.getInstance().setCurrency(currency);
//                        PaymentRequest.getInstance().setTransactionDescription(description);
//                        PaymentRequest.getInstance().setBillingName(name);
//                        PaymentRequest.getInstance().setBillingAddress(address);
//                        PaymentRequest.getInstance().setBillingCity(cityName);
//                        PaymentRequest.getInstance().setBillingPostalCode(postalCode);
//                        PaymentRequest.getInstance().setBillingState(stateName);
//                        PaymentRequest.getInstance().setBillingCountry(countryName);
//                        PaymentRequest.getInstance().setBillingPhone(session.getUserMobileNo());
//                        PaymentRequest.getInstance().setShippingName(shipName);
//                        PaymentRequest.getInstance().setShippingAddress(shipAddress);
//                        PaymentRequest.getInstance().setShippingCity(shipCity);
//                        PaymentRequest.getInstance().setShippingPostalCode(shipPostalCode);
//                        PaymentRequest.getInstance().setShippingState(shipState);
//                        PaymentRequest.getInstance().setShippingCountry(shipCountry);
//                        PaymentRequest.getInstance().setShippingEmail(email);
//                        PaymentRequest.getInstance().setShippingPhone(shipPhone);
//                        PaymentRequest.getInstance().setLogEnabled("1");
//
//                        PaymentRequest.getInstance().setHidePaymentOption(true);
//                        PaymentRequest.getInstance().setHideCashCardOption(true);
//                        PaymentRequest.getInstance().setHideCreditCardOption(false);
//                        PaymentRequest.getInstance().setHideDebitCardOption(false);
//                        PaymentRequest.getInstance().setHideNetBankingOption(false);
//                        PaymentRequest.getInstance().setHideStoredCardOption(true);
//
//                        ArrayList<HashMap<String, String>> custom_post_parameters = new ArrayList<>();
//
//                        HashMap<String, String> hashpostvalues = new HashMap<>();
//                        hashpostvalues.put("account_details", "saving");
//                        hashpostvalues.put("merchant_type", "vpayqwik");
//                        custom_post_parameters.add(hashpostvalues);
//
//                        PaymentRequest.getInstance().setCustomPostValues(custom_post_parameters);
//                        loadDlg.dismiss();
//                        EBSPayment.getInstance().init(FlightPaymentActivity.this, ACC_ID, SECRET_KEY, Mode.ENV_TEST, Encryption.ALGORITHM_MD5, HOST_NAME);
//
//                    } else {
//                        loadDlg.dismiss();
//                        CustomToast.showMessage(FlightPaymentActivity.this, msg);
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                    loadDlg.dismiss();
//                    CustomToast.showMessage(FlightPaymentActivity.this, getResources().getString(R.string.server_exception2));
//
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                loadDlg.dismiss();
//                CustomToast.showMessage(FlightPaymentActivity.this, getResources().getString(R.string.server_exception));
//            }
//        }) {
//            @Override
//            protected Map<String, String> getParams() {
//                Map<String, String> params = new HashMap<>();
//                params.put("sessionId", session.getUserSessionId());
//                params.put("amount", amount);
//                params.put("name", session.getUserFirstName());
//                params.put("email", session.getUserEmail());
//                params.put("phone", session.getUserMobileNo());
//
//                Log.d("LoadMoneyParam", "LoadParams: " + params);
//                return params;
//            }
//
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> map = new HashMap<>();
//                map.put("hash", "123");
//                return map;
//            }
//        };
//
//        int socketTimeout = 60000;
//        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//        postReq.setRetryPolicy(policy);
//        PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//    }
//
//    private void sendRefresh() {
//        Intent intent = new Intent("setting-change");
//        intent.putExtra("updates", "1");
//        LocalBroadcastManager.getInstance(FlightPaymentActivity.this).sendBroadcast(intent);
//    }
//
//
//    public void showSuccessDialog() {
//        Spanned result;
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
//            result = Html.fromHtml(getSuccessMessage(), Html.FROM_HTML_MODE_LEGACY);
//        } else {
//            result = Html.fromHtml(getSuccessMessage());
//        }
//        CustomSuccessDialog builder = new CustomSuccessDialog(FlightPaymentActivity.this, "Booked Successful", result);
//        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                dialog.dismiss();
//                sendRefresh();
//                Intent i = new Intent(FlightPaymentActivity.this, MainActivity.class);
//                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                finish();
//                startActivity(i);
//            }
//        });
//        builder.show();
//    }
//
//    public String getSuccessMessage() {
//        String source =
//                "<b><font color=#000000> " + "<font color=#000000>Ticket Book Successful, </font><br>" +
//                        "<b><font color=#000000> Please check email for detail </font></b>" + "<font>";
//        return source;
//    }
//
//    public static void longLog(String str) {
//        if (str.length() > 4000) {
//            Log.d("bokking", str.substring(0, 4000));
//            longLog(str.substring(4000));
//        } else
//            Log.d("bokking", str);
//    }
//}
