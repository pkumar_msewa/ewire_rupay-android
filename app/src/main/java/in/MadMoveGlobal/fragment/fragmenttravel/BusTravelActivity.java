package in.MadMoveGlobal.fragment.fragmenttravel;

import android.app.DatePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.orm.query.Select;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import in.MadMoveGlobal.EwireRuPay.activity.MainActivity;
import in.MadMoveGlobal.adapter.BusBookListAdapter;
import in.MadMoveGlobal.adapter.HomeSliderAdapter;
import in.MadMoveGlobal.custom.CirclePageIndicator;
import in.MadMoveGlobal.custom.CustomAlertDialog;
import in.MadMoveGlobal.custom.CustomAlertDialogSearch;
import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.metadata.AppMetadata;
import in.MadMoveGlobal.model.BusBookedTicketModel;
import in.MadMoveGlobal.model.BusCityModel;
import in.MadMoveGlobal.model.BusPassengerModel;
import in.MadMoveGlobal.model.BusSaveModel;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.util.CitySelectedListener;
import in.MadMoveGlobal.util.NetworkErrorHandler;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.EwireRuPay.activity.businneractivity.BusListActivity;
import in.MadMoveGlobal.EwireRuPay.activity.businneractivity.BusNewCitySearchActivity;


/**
 * Created by Ksf on 9/26/2016.
 */
public class BusTravelActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {

    //Views
//    private RadioButton rbBusOneWay, rbBusRoundTrip;
    private MaterialEditText etBusFrom, etBusTo, etBusDepartDate;
    //    private MaterialEditText etBusReturnDate;
    private Button btnSearchBus;
//    private RadioGroup rgBusTripType;

    private View focusView = null;

    @Override
    public void onBackPressed() {
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
    }

    private boolean cancel;
    private MaterialEditText spBusFrom, spBusTo;


    //Variables
    private long selectedFromCityCode, selectedToCityCode;
    private String selectedFromCityName, selectedToCityName;
    private ArrayList<String> cities = new ArrayList<>();

    //search city
    private JSONObject jsonRequest;
    private LoadingDialog loadingDialog;
    private UserModel session = UserModel.getInstance();
    private ArrayList<BusCityModel> busCityModelList;
    private String tag_json_obj = "json_bus_city_search";
    private ArrayAdapter cityAdapter;
    private boolean lock = false;
    //image slider
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    private int[] IMAGES;
    public ViewPager mPager;
    public CirclePageIndicator indicator;
    private ImageView ivExchange;

    //Previous book tickets
    private RecyclerView rvPreviousTickets;
    private LinearLayout llpbPreviousTickets;
    private ArrayList<BusPassengerModel> busPassengerArrayList;
    private TextView tvHeaderBookedTickets;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_travels_bus);
        btnSearchBus = (Button) findViewById(R.id.btnSearchBus);
        etBusFrom = (MaterialEditText) findViewById(R.id.etBusFrom);
        etBusTo = (MaterialEditText) findViewById(R.id.etBusTo);
        etBusDepartDate = (MaterialEditText) findViewById(R.id.etBusDepartDate);
        spBusFrom = (MaterialEditText) findViewById(R.id.spBusFrom);
        spBusTo = (MaterialEditText) findViewById(R.id.spBusTo);
        mPager = (ViewPager) findViewById(R.id.pagerHome);
        ivExchange = (ImageView) findViewById(R.id.ivExchange);
        indicator = (CirclePageIndicator) findViewById(R.id.productHeaderImageSlider);
        tvHeaderBookedTickets = (TextView) findViewById(R.id.tvHeaderBookedTickets);
        loadingDialog = new LoadingDialog(this);
        rvPreviousTickets = (RecyclerView) findViewById(R.id.rvPreviousTickets);
        llpbPreviousTickets = (LinearLayout) findViewById(R.id.llpbPreviousTickets);
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
        String formattedDate = df.format(c.getTime());
        etBusDepartDate.setText(formattedDate);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbars);
        ImageButton ivBackBtns = (ImageButton) findViewById(R.id.ivBackBtns);
        ivBackBtns.setVisibility(View.VISIBLE);
        setSupportActionBar(toolbar);
        ivBackBtns.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                onBackPressed();
                Intent intent = new Intent(BusTravelActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        etBusFrom.setText("");
        etBusTo.setText("");
        busCityModelList = new ArrayList<>();
        LocalBroadcastManager.getInstance(this).registerReceiver(bMessageReceiver, new IntentFilter("booking-done"));
        getImageSlider();
        List<BusSaveModel> saveModels = Select.from(BusSaveModel.class).list();

        if (saveModels != null && saveModels.size() != 0) {
            JSONArray citydata = null;
            try {
                citydata = new JSONArray(saveModels.get(0).getCityname());
                for (int i = 0; i < citydata.length(); i++) {
                    JSONObject cityValues = citydata.getJSONObject(i);
                    long cityId = cityValues.getLong("cityId");
                    String cityName = cityValues.getString("cityName");
                    BusCityModel cModel = new BusCityModel(cityId, cityName);
                    busCityModelList.add(cModel);
                }
                if (busCityModelList.size() != 0) {
                    ArrayList<BusCityModel> busCityModels = AppMetadata.getBusCities();
                    for (int i = 0; i < busCityModelList.size(); i++) {
                        for (int j = 0; j < busCityModels.size(); j++) {
                            if (busCityModelList.get(i).getCityname().equalsIgnoreCase(busCityModels.get(j).getCityname())) {
                                Collections.swap(busCityModelList, j, i);
                            }

                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            getBookedTicketsList();
        } else {
            getCity();
        }
//        spBusFrom.setTitle("Select Source City");
        spBusFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final CustomAlertDialogSearch search = new CustomAlertDialogSearch(BusTravelActivity.this, busCityModelList, "from", new CitySelectedListener() {
                    @Override
                    public void citySelected(String type, long cityCode, String cityName) {

                    }

                    @Override
                    public void cityFilter(BusCityModel type) {

                        spBusFrom.setText(type.getCityname());
                        selectedFromCityCode = type.getCityId();
                        selectedFromCityName = type.getCityname();
                        if (selectedToCityCode != 0) {
                            if (selectedToCityCode == selectedFromCityCode) {
                                spBusFrom.setText("");

                                CustomToast.showMessage(BusTravelActivity.this, getResources().getString(R.string.source_city_could_not_be_same_as_destination_city));
                            }
                        }

                    }
                });
                search.show();
            }
        });
//        spBusFrom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                selectedFromCityCode = busCityModelList.get(position).getCityId();
//                selectedFromCityName = busCityModelList.get(position).getCityname();
//                if(selectedToCityCode!=0){
//                    lock= selectedToCityCode == selectedFromCityCode;
//                    if(lock) {
//                        Toast.makeText(getActivity(), "source city could not be same as destination city.", Toast.LENGTH_SHORT).show();
//                    }
//                }
//            }
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
//        spBusTo.setTitle("Select Destination City");
//        spBusTo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                selectedToCityCode = busCityModelList.get(position).getCityId();
//                selectedToCityName = busCityModelList.get(position).getCityname();
//                if (selectedFromCityCode != 0) {
//                    lock = selectedToCityCode == selectedFromCityCode;
//                    if (lock) {
//                        Toast.makeText(getActivity(), "destination city could not be same as source city.", Toast.LENGTH_SHORT).show();
//                    }
//                }
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
        spBusTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomAlertDialogSearch search = new CustomAlertDialogSearch(BusTravelActivity.this, busCityModelList, getResources().getString(R.string.To), new CitySelectedListener() {
                    @Override
                    public void citySelected(String type, long cityCode, String cityName) {

                    }

                    @Override
                    public void cityFilter(BusCityModel type) {
                        spBusTo.setText(type.getCityname());
                        selectedToCityCode = type.getCityId();
                        selectedToCityName = type.getCityname();
                        if (selectedFromCityCode != 0) {
                            if (selectedToCityCode == selectedFromCityCode) {
                                spBusTo.setText("");
                            }
                        }

                    }
                });
                search.show();
            }
        });
//
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("search-complete"));

        etBusDepartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (etBusDepartDate.getText().toString() != null && !etBusDepartDate.getText().toString().isEmpty()) {
                    String[] dateSplit = etBusDepartDate.getText().toString().split("-");

                    int mYear = Integer.valueOf(dateSplit[2]);
                    int mMonth = Integer.valueOf(dateSplit[1]) - 1;
                    int mDay = Integer.valueOf(dateSplit[0]);

                    DatePickerDialog dialog = new DatePickerDialog(BusTravelActivity.this, R.style.DatePicker, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                            String formattedDay, formattedMonth;
                            if (dayOfMonth < 10) {
                                formattedDay = "0" + dayOfMonth;
                            } else {
                                formattedDay = dayOfMonth + "";
                            }

                            if ((monthOfYear + 1) < 10) {
                                formattedMonth = "0" + String.valueOf(monthOfYear + 1);
                            } else {
                                formattedMonth = String.valueOf(monthOfYear + 1) + "";
                            }
                            etBusDepartDate.setText(String.valueOf(formattedDay) + "-"
                                    + String.valueOf(formattedMonth) + "-"
                                    + String.valueOf(year));
                        }
                    }, mYear, mMonth, mDay);
                    dialog.getDatePicker().setMinDate(System.currentTimeMillis());
                    dialog.setCustomTitle(null);
                    dialog.setTitle("");
                    dialog.show();
                } else {
                    final Calendar c = Calendar.getInstance();
                    int mYear = c.get(Calendar.YEAR);
                    int mMonth = c.get(Calendar.MONTH);
                    int mDay = c.get(Calendar.DAY_OF_MONTH);

                    DatePickerDialog dialog = new DatePickerDialog(BusTravelActivity.this, R.style.DatePicker, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                            String formattedDay, formattedMonth;
                            if (dayOfMonth < 10) {
                                formattedDay = "0" + dayOfMonth;
                            } else {
                                formattedDay = dayOfMonth + "";
                            }

                            if ((monthOfYear + 1) < 10) {
                                formattedMonth = "0" + String.valueOf(monthOfYear + 1);
                            } else {
                                formattedMonth = String.valueOf(monthOfYear + 1) + "";
                            }
                            etBusDepartDate.setText(String.valueOf(formattedDay) + "-"
                                    + String.valueOf(formattedMonth) + "-"
                                    + String.valueOf(year));
                        }
                    }, mYear, mMonth, mDay);

                    dialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                    dialog.setTitle("");
                    dialog.setCustomTitle(null);
                    dialog.show();
                }

            }
        });
        etBusFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent searchCityIntent = new Intent(BusTravelActivity.this, BusNewCitySearchActivity.class);
                searchCityIntent.putExtra("searchType", "From");
                startActivity(searchCityIntent);

            }
        });

        etBusTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent searchCityIntent = new Intent(BusTravelActivity.this, BusNewCitySearchActivity.class);
                searchCityIntent.putExtra("searchType", "To");
                startActivity(searchCityIntent);

            }
        });

        btnSearchBus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptSearch();
            }
        });

        ivExchange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                spBusFrom.setText(selectedToCityName);
                spBusTo.setText(selectedFromCityName);
                String temp = selectedFromCityName;
                selectedFromCityName = selectedToCityName;
                selectedToCityName = temp;

                Long temps = selectedFromCityCode;
                selectedFromCityCode = selectedToCityCode;
                selectedToCityCode = temps;


            }
        });

    }

    private void attemptSearch() {

        etBusDepartDate.setError(null);
        if (!spBusFrom.getText().toString().equals("")) {
            if (!spBusTo.getText().toString().equals("") && !spBusTo.getText().toString().equals("")) {
                if (etBusDepartDate.getText().toString().isEmpty()) {
                    etBusDepartDate.setError(getResources().getString(R.string.Please_select_departure_date));
                    focusView = etBusDepartDate;
                    focusView.requestFocus();
                } else {
                    if (lock) {
                        CustomToast.showMessage(BusTravelActivity.this, getResources().getString(R.string.Please_select_destination_city_other_then_source_city));
                    } else {
                        promoteSearch();
                    }
                }
            } else {
                CustomToast.showMessage(BusTravelActivity.this, getResources().getString(R.string.Select_Destination_City));
            }
        } else {
            CustomToast.showMessage(BusTravelActivity.this, getResources().getString(R.string.Select_Destination_City));
        }
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getStringExtra("searchType");

            if (action.equals("To")) {
                selectedToCityCode = intent.getLongExtra("selectedCityCode", 0);
                selectedToCityName = intent.getStringExtra("selectedCityName");
                if (selectedToCityCode == selectedFromCityCode) {
                    CustomToast.showMessage(BusTravelActivity.this, getResources().getString(R.string.Please_select_destination_city_other_then_source_city));
                } else {
                    etBusTo.setText(selectedToCityName);
                }

            } else {
                selectedFromCityCode = intent.getLongExtra("selectedCityCode", 0);
                selectedFromCityName = intent.getStringExtra("selectedCityName");
                etBusFrom.setText(selectedFromCityName);
            }
        }
    };

    private void getImageSlider() {
        IMAGES = new int[]{R.drawable.bus_img_slider_1, R.drawable.bus_img_slider_2, R.drawable.bus_img_slider_3};
        mPager.setAdapter(new HomeSliderAdapter(BusTravelActivity.this, IMAGES));
        indicator.setViewPager(mPager);
        final float density = this.getResources().getDisplayMetrics().density;
        indicator.setRadius(5 * density);

        NUM_PAGES = IMAGES.length;

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);

        // Pager listener over indicator
        indicator.setOnPageChangeListener(this);
//        {
//
//            @Override
//            public void onPageSelected(int position) {
//                currentPage = position;
//
//            }
//
//            @Override
//            public void onPageScrolled(int pos, float arg1, int arg2) {
//
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int pos) {
//
//            }
//        });
//    }
    }

//    @Override
//    public void onDestroy() {
//        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
//        LocalBroadcastManager.getInstance(this).unregisterReceiver(bMessageReceiver);
//        super.onDestroy();
//    }

    private void promoteSearch() {
        Intent getBusIntent = new Intent(this, BusListActivity.class);
        getBusIntent.putExtra("date", etBusDepartDate.getText().toString());
        getBusIntent.putExtra("destination", selectedToCityCode);
        getBusIntent.putExtra("source", selectedFromCityCode);
        getBusIntent.putExtra("sourceName", selectedFromCityName);
        getBusIntent.putExtra("destinationName", selectedToCityName);
        startActivity(getBusIntent);
    }

    private void getCity() {
        try {
            loadingDialog.show();

            jsonRequest = new JSONObject();
            try {
                jsonRequest.put("sessionId", session.getUserSessionId());

            } catch (JSONException e) {
                e.printStackTrace();
                jsonRequest = null;
            }

            if (jsonRequest != null) {
                Log.i("CityRequest", jsonRequest.toString());
                Log.i("CITYURL", ApiUrl.URL_BUS_GET_CITY);
                JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_BUS_GET_CITY, jsonRequest, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
//                AndroidNetworking.get(session.getMdexUrl() + "/api/travel/bus/citylist")
//                        .addJSONObjectBody(jsonRequest) // posting json
//                        .setTag("test")
//                        .addHeaders("Authorization", EncryptDecryptUserUtility.encryptAuth(session.getMdexUsername()+":"+session.getMdexPassword()))
//                        .setPriority(Priority.HIGH)
//                        .build()
//                        .getAsJSONObject(new JSONObjectRequestListener() {
//                            @Override
//                            public void onResponse(JSONObject response) {

                        try {
                            if (response.has("code")) {
                                String code = response.getString("code");
                                Log.i("BUSCITYRSE", response.toString());
                                if (code != null && code.equals("S00")) {
                                    loadingDialog.dismiss();
                                    JSONArray citydata = response.getJSONArray("details");
                                    BusSaveModel busSaveModel = new BusSaveModel(citydata.toString());
                                    busSaveModel.save();



                                    for (int i = 0; i < citydata.length(); i++) {
                                        JSONObject c = citydata.getJSONObject(i);
                                        long cityId = c.getLong("cityId");
                                        String cityName = c.getString("cityName");
                                        BusCityModel cModel = new BusCityModel(cityId, cityName);
                                        busCityModelList.add(cModel);
                                    }
                                    if (busCityModelList.size() != 0) {
                                        ArrayList<BusCityModel> busCityModels = AppMetadata.getBusCities();
                                        for (int i = 0; i < busCityModelList.size(); i++) {
                                            for (int j = 0; j < busCityModels.size(); j++) {
                                                if (busCityModelList.get(i).getCityname().equalsIgnoreCase(busCityModels.get(j).getCityname())) {
                                                    Collections.swap(busCityModelList, j, i);
                                                }

                                            }
                                        }
                                        loadingDialog.dismiss();
                                        getBookedTicketsList();
//                                        cityAdapter = new ArrayAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item, cities);
//                                        spBusFrom.setAdapter(cityAdapter);
//                                        spBusTo.setAdapter(cityAdapter);
                                    }

                                } else if (code != null && code.equals("F03")) {
                                    loadingDialog.dismiss();
                                    showInvalidSessionDialog();

                                } else {
                                    loadingDialog.dismiss();
                                    if (response.has("message") && response.getString("message") != null) {
                                        String message = response.getString("message");
                                        CustomToast.showMessage(BusTravelActivity.this, message);
                                    }
                                }
                            }
                        } catch (JSONException e) {
                            loadingDialog.dismiss();
                            CustomToast.showMessage(BusTravelActivity.this, getResources().getString(R.string.server_exception2));
                            e.printStackTrace();
                            try {
                                loadingDialog.dismiss();
                            } catch (NullPointerException e1) {

                            }
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            loadingDialog.dismiss();
                        } catch (NullPointerException e) {

                        }
                        CustomToast.showMessage(BusTravelActivity.this, NetworkErrorHandler.getMessage(error, BusTravelActivity.this));

                    }
                }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> map = new HashMap<>();
                        map.put("hash", "1234");

                        String basicAuth = "Basic " + new String(Base64.encode("user:pass".getBytes(), Base64.NO_WRAP));
                        map.put("Authorization", basicAuth);
                        return map;

                    }

                };
                int socketTimeout = 60000;
                RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                postReq.setRetryPolicy(policy);
                PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
            }
        } catch (NullPointerException e) {

        }
    }

    private BroadcastReceiver bMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getStringExtra("detail");
            if (action.equals("1")) {
                getBookedTicketsList();
            }

        }
    };

    public void getBookedTicketsList() {
        llpbPreviousTickets.setVisibility(View.VISIBLE);
        rvPreviousTickets.setVisibility(View.GONE);
        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (jsonRequest != null) {
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_BUS_GET_BOOKED_TICKETS, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObj) {
                    try {
                        Log.i("BUSBOOKEDRES", jsonObj.toString());
                        String code = jsonObj.getString("code");
                        String message = jsonObj.getString("message");

                        if (code != null && code.equals("S00")) {
                            ArrayList<BusBookedTicketModel> busBookedTicketArrayList = new ArrayList<>();
                            String details = jsonObj.getString("details");
                            JSONArray busBookedTicketarray = new JSONArray(details);

                            for (int i = 0; i < busBookedTicketarray.length(); i++) {
                                JSONObject c = busBookedTicketarray.getJSONObject(i);
                                String busTicketString = c.getString("busTicket");
                                JSONObject busTicket = new JSONObject(busTicketString);
                                String userMobile = busTicket.getString("userMobile");
                                String userEmail = busTicket.getString("userEmail");
                                String ticketPnr = busTicket.getString("ticketPnr");
                                String operatorPnr = busTicket.getString("operatorPnr");
                                String emtTxnId = busTicket.getString("emtTxnId");
                                String busId = busTicket.getString("busId");
                                double totalFare = busTicket.getDouble("totalFare");
                                String journeyDate = busTicket.getString("journeyDate");
                                String source = busTicket.getString("source");
                                String destination = busTicket.getString("destination");
                                String boardingId = busTicket.getString("boardingId");
                                String boardingAddress = busTicket.getString("boardingAddress");
                                String busOperator = busTicket.getString("busOperator");
                                String arrTime = busTicket.getString("arrTime");
                                String deptTime = busTicket.getString("deptTime");
                                String travellerDetailsSTring = c.getString("travellerDetails");
                                JSONArray travellerDetails = new JSONArray(travellerDetailsSTring);
                                if (travellerDetails.length() != 0) {
                                    busPassengerArrayList = new ArrayList<>();
                                    for (int j = 0; j < travellerDetails.length(); j++) {
                                        JSONObject p = travellerDetails.getJSONObject(j);
                                        String fName = p.getString("fName");
                                        String lName = p.getString("lName");
                                        String age = p.getString("age");
                                        String gender = p.getString("gender");
                                        String seatNo = p.getString("seatNo");
                                        String seatType = p.getString("seatType");
                                        String fare = p.getString("fare");
                                        BusPassengerModel busPassengerModel = new BusPassengerModel(fName, lName, age, gender, seatNo, seatType, fare);
                                        busPassengerArrayList.add(busPassengerModel);
                                    }
                                }
                                BusBookedTicketModel busBookedTicketModel = new BusBookedTicketModel(userMobile, userEmail, ticketPnr, operatorPnr, emtTxnId, busId, totalFare, journeyDate, source, destination, boardingId, boardingAddress, busOperator, arrTime, deptTime, busPassengerArrayList, "", "");
                                busBookedTicketArrayList.add(busBookedTicketModel);

                            }
                            if (busBookedTicketArrayList != null && busBookedTicketArrayList.size() != 0) {
//                                int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.recycle_space_double);
                                rvPreviousTickets.addItemDecoration(new SpacesItemDecoration(4));
                                GridLayoutManager manager = new GridLayoutManager(BusTravelActivity.this, 1);
                                rvPreviousTickets.setLayoutManager(manager);
                                rvPreviousTickets.setHasFixedSize(true);

                                BusBookListAdapter itemAdp = new BusBookListAdapter(BusTravelActivity.this, busBookedTicketArrayList);
                                rvPreviousTickets.setAdapter(itemAdp);
                                tvHeaderBookedTickets.setVisibility(View.VISIBLE);
                                llpbPreviousTickets.setVisibility(View.GONE);
                                rvPreviousTickets.setVisibility(View.VISIBLE);

                            } else {

                                llpbPreviousTickets.setVisibility(View.GONE);
                            }
                        } else if (code != null && code.equals("F03")) {
                            llpbPreviousTickets.setVisibility(View.GONE);
//                            if(isAdded()){
                            showInvalidSessionDialog();
//                            }
                        } else {
                            llpbPreviousTickets.setVisibility(View.GONE);
                            CustomToast.showMessage(BusTravelActivity.this, message);

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(BusTravelActivity.this, getResources().getString(R.string.Oops_something_went_wrong_please_try_again_later), Toast.LENGTH_SHORT).show();
                        llpbPreviousTickets.setVisibility(View.GONE);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    llpbPreviousTickets.setVisibility(View.GONE);
                    CustomToast.showMessage(BusTravelActivity.this, NetworkErrorHandler.getMessage(error, BusTravelActivity.this));
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        public SpacesItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view,
                                   RecyclerView parent, RecyclerView.State state) {
            outRect.left = space;
            outRect.right = space;
            outRect.bottom = space;
            outRect.top = space;
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        currentPage = position;
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(BusTravelActivity.this, R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton(getResources().getString(R.string.agree), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton(getResources().getString(R.string.Dismiss), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(BusTravelActivity.this).sendBroadcast(intent);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (loadingDialog != null) {
            loadingDialog.dismiss();
            loadingDialog = null;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (loadingDialog != null) {
            loadingDialog.dismiss();
            loadingDialog = null;
        }
    }
}