package in.MadMoveGlobal.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.SSLSocketFactory;

import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.EwireRuPay.WebViewActivity;
import in.MadMoveGlobal.EwireRuPay.activity.MainActivity;
import in.MadMoveGlobal.custom.AESCrypt;
import in.MadMoveGlobal.custom.CustomAlertDialog;
import in.MadMoveGlobal.custom.CustomDisclaimerDialogs;
import in.MadMoveGlobal.custom.CustomSuccessDialog;
import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.metadata.AppMetadata;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.util.CheckLog;
import in.MadMoveGlobal.util.EncryptDecryptUserUtility;
import in.MadMoveGlobal.util.NetworkErrorHandler;
import in.MadMoveGlobal.util.PayingDetailsValidation;
import in.MadMoveGlobal.util.TLSSocketFactory;
import io.fabric.sdk.android.Fabric;

/**
 * Created by Ksf on 4/11/2016.
 */
public class LoadMoneyPaymentFragment extends AppCompatActivity implements PaymentResultListener {

    private static final String TAG = "values";
    private View rootView;
    private MaterialEditText etLoadMoneyAmount;
    private Button btnLoadMoney;
    private RadioButton rbLoadMoneyVBank, rbLoadMoneyOther;
    private View focusView = null;
    private boolean cancel;
    private String amount = null;
    AlertDialog.Builder payDialog;
    private UserModel session = UserModel.getInstance();
    private LoadingDialog loadDlg;
    private RequestQueue rq;
    private boolean isVBank = true;
    private String inValidMessage = "";
    private String tag_json_obj = "load_money";
    private JSONObject jsonRequest;
    String autoFill, address1, address2, city, state, pinCode;
    String userKey = "udkrtnfg";
    String userPasswordKey = "30869142";
    double loadAmount, commission;
    private Toolbar toolbar;
    private String transcationID, Amount, authRefNo, message, secretKey, keyId, loadcomision, totsamns, totalcom, a;
    private JsonObjectRequest postReq;
    private Double value;
    private ImageButton ivBackBtn;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.fragment_ask_amount_payment_to_load_money);
        payDialog = new AlertDialog.Builder(LoadMoneyPaymentFragment.this, R.style.AppCompatAlertDialogStyle);
        loadDlg = new LoadingDialog(LoadMoneyPaymentFragment.this);
        toolbar = (Toolbar) findViewById(R.id.toolbars);
        ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtns);
        loadcomision = session.loadMoneyComm;


        setSupportActionBar(toolbar);
        ivBackBtn.setVisibility(View.VISIBLE);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent shoppingIntent = new Intent(LoadMoneyPaymentFragment.this, MainActivity.class);
                startActivity(shoppingIntent);
                finish();
            }
        });
        try {
            final SSLSocketFactory sslSocketFactory = new TLSSocketFactory();
            rq = Volley.newRequestQueue(LoadMoneyPaymentFragment.this, new HurlStack(null, sslSocketFactory));
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        btnLoadMoney = (Button) findViewById(R.id.btnLoadMoney);
        etLoadMoneyAmount = (MaterialEditText) findViewById(R.id.etLoadMoneyAmount);
        rbLoadMoneyVBank = (RadioButton) findViewById(R.id.rbLoadMoneyVBank);
        rbLoadMoneyOther = (RadioButton) findViewById(R.id.rbLoadMoneyOther);


        address1 = getIntent().getStringExtra("address1");
        address2 = getIntent().getStringExtra("address2");
        city = getIntent().getStringExtra("city");
        state = getIntent().getStringExtra("state");
        pinCode = getIntent().getStringExtra("pinCode");

        autoFill = getIntent().getStringExtra("AutoFill");
        Checkout.preload(LoadMoneyPaymentFragment.this);

        if (autoFill.equals("yes")) {
            String loadAmountString = getIntent().getStringExtra("splitAmount");
            loadAmount = Math.ceil(Double.parseDouble(loadAmountString));
            DecimalFormat format = new DecimalFormat("0.#");
            if (loadAmount < 10) {
                loadAmount = 10;
            }
            etLoadMoneyAmount.setText(String.valueOf(format.format(loadAmount)));


            etLoadMoneyAmount.setEnabled(false);
        }


        btnLoadMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!validateamount()) {

                    return;
                }


                a = etLoadMoneyAmount.getText().toString();

                double b = Double.parseDouble(a);

                commission = Double.parseDouble(loadcomision);
                double totcom = (b * (commission / 100));

//                totsamns = String.valueOf((b+totcom));
                Double dv = b + totcom;
                DecimalFormat df = new DecimalFormat("#.##");

                totsamns = String.valueOf(df.format(dv));
                totalcom = String.valueOf(df.format(totcom));

                value = (Double.parseDouble(totsamns)) * 100;


                Log.i("GHHHGGHGH", String.valueOf(totsamns));

                if (rbLoadMoneyVBank.isChecked()) {
                    isVBank = true;
                } else if (rbLoadMoneyOther.isChecked()) {
                    isVBank = false;
                } else {
                    isVBank = true;
                }

                attemptLoad();

            }
        });


        //DONE CLICK ON VIRTUAL KEYPAD
        etLoadMoneyAmount.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {


                if (actionId == EditorInfo.IME_ACTION_DONE) {


                    if (etLoadMoneyAmount.getText().toString().trim().isEmpty()) {
                        etLoadMoneyAmount.setError("Enter your amount");
                    } else {

                        a = etLoadMoneyAmount.getText().toString();

                        double b = Double.parseDouble(a);

                        commission = Double.parseDouble(loadcomision);
                        double totcom = (b * (commission / 100));

//                totsamns = String.valueOf((b+totcom));
                        Double dv = b + totcom;
                        DecimalFormat df = new DecimalFormat("#.##");

                        totsamns = String.valueOf(df.format(dv));
                        totalcom = String.valueOf(df.format(totcom));

                        value = (Double.parseDouble(totsamns)) * 100;


                        Log.i("GHHHGGHGH", String.valueOf(totsamns));


                        if (rbLoadMoneyVBank.isChecked()) {
                            isVBank = true;
                        } else if (rbLoadMoneyOther.isChecked()) {
                            isVBank = false;
                        } else {
                            isVBank = true;
                        }


                        attemptLoad();
                    }
                }
                return false;
            }
        });

    }


    private void attemptLoad() {
        etLoadMoneyAmount.setError(null);
        cancel = false;
        amount = etLoadMoneyAmount.getText().toString();
        checkPayAmount(amount);
//        checkUserType();

        if (cancel) {
            focusView.requestFocus();
        } else {

            if (rbLoadMoneyVBank.isChecked()) {
                showCustomDisclaimerDialog();
            } else {
                showCustomsDisclaimersDialogs();
            }
//            if (isVBank) {
//                checkTrxTime();
//            } else {

//            }
        }
    }

    public void showCustomDisclaimerDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(LoadMoneyPaymentFragment.this, R.string.dialog_title2, Html.fromHtml(generateLoadmoneyMessage()));

        builder.setPositiveButton("Accept", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                checkTrxTime();


            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.show();
    }


    public void showCustomsDisclaimersDialogs() {
        CustomDisclaimerDialogs builder = new CustomDisclaimerDialogs(LoadMoneyPaymentFragment.this);

        builder.setPositiveButton("Accept", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                checkTrxTimeupi();

            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.show();
    }


    public void showNonKYCDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(LoadMoneyPaymentFragment.this, R.string.dialog_title2, Html.fromHtml(generateKYCMessage()));
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.show();
    }


    private void checkPayAmount(String amount) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
        try {
            if (!gasCheckLog.isValid) {
                etLoadMoneyAmount.setError(getString(gasCheckLog.msg));
                focusView = etLoadMoneyAmount;
                cancel = true;
            } else if (Integer.valueOf(amount) < 1) {
                etLoadMoneyAmount.setError("Amount cant be less than 1");
                focusView = etLoadMoneyAmount;
                cancel = true;
            } else if (Integer.valueOf(amount) >= 10000) {
                etLoadMoneyAmount.setError("Enter amount less than 10000");
                focusView = etLoadMoneyAmount;
                cancel = true;
            } else if (autoFill.equals("yes")) {
                if (Integer.valueOf(amount) < loadAmount) {
                    DecimalFormat format = new DecimalFormat("0.#");
                    etLoadMoneyAmount.setError("Amount cant be less than " + format.format(loadAmount));
                    focusView = etLoadMoneyAmount;
                    cancel = true;
                }
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }

    private boolean checkUserType() {
        if (amount != null && !amount.isEmpty()) {
            if (Integer.valueOf(amount) > 10000) {
                focusView = etLoadMoneyAmount;
                cancel = true;
                if (session.getUserAcName().equals("Non-KYC")) {
                    showNonKYCDialog();
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }

    }

    public String generateMessage() {
        String source = "<b><font color=#000000> Amount to load: </font></b>" + "<font color=#000000>" + "₹ " + amount + "</font><br>" +
                "<br><b><font color=#ff0000> Are you sure you want to proceed?</font></b><br>";
        return source;
    }

    public String generateLoadmoneyMessage() {
        return "<b><font color=#000000 > You will now be redirected to payment gateway portal.Kindly note that it is an external website that is not controlled by E-WIRE. Please ensure that you are entering the correct credentials.</font></b><br>" +
                "<br><b><font color=#ff0000> Please note that there is a Convenience fee of </font></b>" + "<b><font color=#ff0000>" + session.getloadMoneyComm() + "% of total load money amount that will be deducted." + "</font></b><br>" +
                "<b><font color=#000000> <b> Convenience Fee: </b></font></b>" + "<font color=#000000>" + session.getloadMoneyComm() + "%" + "</font><br>" +
                "<b><font color=#000000> <b> Load Money: </b></font></b>" + "<font color=#000000>" + "₹ " + a + "</font><br>" +
                "<b><font color=#000000> <b> Payment gateway charge: </b></font></b>" + "<font color=#000000>" + "₹ " + totalcom + "</font><br>" +
                "<b><font color=#000000> <b> Total amount: </b></font></b>" + "<font color=#000000>" + "₹ " + totsamns + "</font><br>" +
                "<br><b><font color=#0F8006> Load money through UPI without any convenience fee.</font></b><br>";
    }

    public String generateKYCMessage() {
        return "<b><font color=#000000> Amount to load: </font></b>" + "<font color=#000000>" + "₹ " + amount + "</font><br>" +
                "<br><b><font color=#ff0000> Sorry you cannot load more than 10,000 at a time.</font></b><br>" +
                "<br><b><font color=#ff0000> Please enter 10,000 or lesser amount to continue.</font></b><br>";
    }

    public void checkTrxTimeupi() {
        loadDlg.show();

        jsonRequest = new JSONObject();

        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("amount", etLoadMoneyAmount.getText().toString());


        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            JSONObject data = new JSONObject();
            try {
                data.put("upiRequest", AESCrypt.encrypt(jsonRequest.toString()));
            } catch (Exception e) {
                e.printStackTrace();
            }

            JsonObjectRequest postReqs = null;
            try {
                postReqs = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_VALIDATE_TRX_TIMEUPI, data, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            message = response.getString("message");
                            String code = response.getString("code");
                            if (code != null && code.equals("S00")) {
                                loadDlg.dismiss();
//                            boolean success = response.getBoolean("status");
//                            if (success) {

                                transcationID = response.getString("transactionRefNo");
                                Amount = response.getString("amount");

                                Intent intent = new Intent(LoadMoneyPaymentFragment.this, WebViewActivity.class);
                                intent.putExtra("amount", Amount);
                                intent.putExtra("transactionRefNo", transcationID);

                                startActivity(intent);


////                                verifyTransaction(amount);
//                            } else {
//                                CustomToast.showMessage(LoadMoneyFragment.this, message);
//                            }
                            } else if (code != null && code.equals("F00")) {
                                loadDlg.dismiss();
                                showDialog();
                            } else if (code != null && code.equals("F03")) {
                                loadDlg.dismiss();
                                showInvalidSessionDialog();

                            } else {
                                loadDlg.dismiss();
                                CustomToast.showMessage(LoadMoneyPaymentFragment.this, message);
                            }

                        } catch (
                                JSONException e)

                        {
                            loadDlg.dismiss();
                            CustomToast.showMessage(LoadMoneyPaymentFragment.this, getResources().getString(R.string.server_exception2));
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(LoadMoneyPaymentFragment.this, getResources().getString(R.string.server_exception));

                        error.printStackTrace();

                    }
                }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> map = new HashMap<>();
                        map.put("hash", "1234");
                        map.put("Authorization",EncryptDecryptUserUtility.encryptAuth(userKey + ":" + userPasswordKey));
//                        String basicAuth = "Basic " + new String(Base64.encode("user:pass".getBytes(), Base64.NO_WRAP));
                        return map;
                    }

                };
            } catch (Exception e) {
                e.printStackTrace();
            }
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReqs.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReqs, tag_json_obj);

        }

    }

    public void checkTrxTime() {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("amount", etLoadMoneyAmount.getText().toString());

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
//            Log.i("TRXTIMEURL", ApiUrl.URL_VALIDATE_TRX_TIME);
//            Log.i("TRXTIMEREQ", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_VALIDATE_TRX_TIME, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
//                    Log.i("TRXTIMERES", response.toString());
                    try {
                        message = response.getString("message");
                        String code = response.getString("code");
                        if (code != null && code.equals("S00")) {
                            loadDlg.dismiss();
//                            boolean success = response.getBoolean("status");
//                            if (success) {
                            try {
                                final Activity activity = LoadMoneyPaymentFragment.this;

                                final Checkout co = new Checkout();
                                co.setImage(R.drawable.toolbart_niki);

                                JSONObject options = new JSONObject();

                                options.put("name", "Load Money");
                                transcationID = response.getString("transactionRefNo");
                                authRefNo = response.getString("transactionRefNo");
                                keyId = response.getString("key_id");
                                secretKey = response.getString("key_secret");
                                Double amountsr = response.getDouble("amount");
                                amountsr = Math.round(amountsr * 100.0 * 100.0) / 100.0;

//                                Log.i("hgdhgsah", String.valueOf(amountsr));

                                SharedPreferences.Editor editor = getSharedPreferences("transactionRefNo", Context.MODE_PRIVATE).edit();
                                editor.clear();
                                editor.putString("transactionRefNo", transcationID);
                                editor.putString("authReferenceNo", response.getString("authReferenceNo")).apply();

                                options.put("description", transcationID);
                                //You can omit the image option to fetch the image from dashboard
                                options.put("image", ApiUrl.URL_DOMAIN_ + "resources/User/NewUi/img/logo.svg");
                                options.put("currency", "INR");
                                options.put("authReferenceNo", authRefNo);

                                options.put("amount", Integer.valueOf(amountsr.intValue()));

                                JSONObject preFill = new JSONObject();
                                preFill.put("email", session.getUserEmail());
                                preFill.put("contact", session.getUserMobileNo());

                                JSONObject readonly = new JSONObject();
                                readonly.put("contact", true);
                                readonly.put("email", true);
                                options.put("readonly", readonly);

                                options.put("prefill", preFill);
//                                Log.i("amount", preFill.toString());

                                co.open(activity, options);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

////                                verifyTransaction(amount);
//                            } else {
//                                CustomToast.showMessage(LoadMoneyFragment.this, message);
//                            }
                        } else if (code != null && code.equals("F00")) {
                            loadDlg.dismiss();
                            showDialog();
                        } else if (code != null && code.equals("F03")) {
                            loadDlg.dismiss();
                            showInvalidSessionDialog();
                        } else {
                            loadDlg.dismiss();
                            CustomToast.showMessage(LoadMoneyPaymentFragment.this, message);
                        }

                    } catch (
                            JSONException e)

                    {
                        loadDlg.dismiss();
                        CustomToast.showMessage(LoadMoneyPaymentFragment.this, getResources().getString(R.string.server_exception2));
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    CustomToast.showMessage(LoadMoneyPaymentFragment.this, getResources().getString(R.string.server_exception));

                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);

        }

    }


    @SuppressWarnings("unused")
    @Override
    public void onPaymentSuccess(String razorpayPaymentID) {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
//      Toast.makeText(LoadMoneyFragment.this, "Payment Successful: " + razorpayPaymentID, Toast.LENGTH_SHORT).show();
//      URL_LOAD_MONEY_RESPONSE
            SharedPreferences sharedpreferences = getSharedPreferences("transactionRefNo", Context.MODE_PRIVATE);
//            JSONObject jsonObject = new JSONObject();
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("status", "Captured");
//            jsonRequest.put("authReferenceNo", sharedpreferences.getString("authReferenceNo", ""));
            if (session.hasPcard) {
                jsonRequest.put("physicalCard", "yes");
            } else {
                jsonRequest.put("virtualCard", "yes");
            }
            jsonRequest.put("amount", Integer.valueOf(value.intValue()));
//            jsonRequest.put("amount", sharedpreferences.getString("amount", ""));
            jsonRequest.put("transactionRefNo", sharedpreferences.getString("transactionRefNo", ""));
            jsonRequest.put("paymentId", razorpayPaymentID);
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
            loadDlg.dismiss();
        }


//      Log.i("loadmoneys", jsonObject.toString());
//      CustomToast.showMessage(LoadMoneyFragment.this, jsonObject.toString());
//      AndroidNetworking.post(ApiUrl.URL_LOAD_MONEY_RESPONSE)
//        .addJSONObjectBody(jsonObject) // posting json
//        .setTag("test")
//        .setPriority(Priority.IMMEDIATE)
//        .build()
//        .getAsJSONObject(new JSONObjectRequestListener() {


        if (jsonRequest != null) {
//            Log.i("JsonRequest", jsonRequest.toString());
//            Log.i("Receipt Url", ApiUrl.URL_LOAD_MONEY_RESPONSE);
            postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_LOAD_MONEY_RESPONSE, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
//                    Log.i("respobjsaj", response.toString());
                    loadDlg.dismiss();
                    try {
                        if (response.getString("code").equalsIgnoreCase("S00")) {
                            if (!getIntent().getBooleanExtra("phycard", false)) {
                                CustomSuccessDialog customAlertDialog = new CustomSuccessDialog(LoadMoneyPaymentFragment.this, "", "Load money Successful");
                                customAlertDialog.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        SharedPreferences sharedpreferences = getSharedPreferences("transcationID", Context.MODE_PRIVATE);
                                        sharedpreferences.edit().clear().remove("transcationID").remove("amount").apply();
                                        sendRefresh();
                                    }
                                });
                                customAlertDialog.show();
                            } else {
                                loadCardDetails();
                            }
//                CustomToast.showMessage(LoadMoneyFragment.this, "Successful");
                        } else {
                            loadDlg.dismiss();
                            SharedPreferences sharedpreferences = getSharedPreferences("transcationID", Context.MODE_PRIVATE);
                            sharedpreferences.edit().clear().remove("transcationID").remove("amount").apply();
                            CustomToast.showMessage(LoadMoneyPaymentFragment.this, response.getString("message"));
                        }
                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    try {
                        loadDlg.dismiss();
                        CustomToast.showMessage(LoadMoneyPaymentFragment.this, getResources().getString(R.string.server_exception2));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }

    }

    public void loadCardDetails() {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {


            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("address1", address1);
            jsonRequest.put("address2", address2);
            jsonRequest.put("city", city);
            jsonRequest.put("state", state);
            jsonRequest.put("pinCode", pinCode);


        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
//            Log.i("JsonRequest", jsonRequest.toString());
//            Log.i("Card Url", ApiUrl.URL_MATCHMOVE_GENRATEPHYSICALCARD);
            postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_MATCHMOVE_GENRATEPHYSICALCARD, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject JsonObj) {
                    try {

//                        Log.i("Card Response", JsonObj.toString());
                        String message = JsonObj.getString("message");
                        String code = JsonObj.getString("code");

                        if (code != null && code.equals("S00")) {
//                            sendRefresh();
                            Toast.makeText(LoadMoneyPaymentFragment.this, message, Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(LoadMoneyPaymentFragment.this, MainActivity.class));
                            finishAffinity();

                        }
                        if (code != null && code.equals("F03")) {
                            loadDlg.dismiss();

                            finish();


                        } else {
                            loadDlg.dismiss();
                            Toast.makeText(LoadMoneyPaymentFragment.this, message, Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        loadDlg.dismiss();
                        CustomToast.showMessage(LoadMoneyPaymentFragment.this, getResources().getString(R.string.server_exception2));
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    loadDlg.dismiss();
                    CustomToast.showMessage(LoadMoneyPaymentFragment.this, NetworkErrorHandler.getMessage(error, LoadMoneyPaymentFragment.this));
                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };

            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }

    }


    private void sendRefresh() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "1");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        startActivity(new Intent(LoadMoneyPaymentFragment.this, MainActivity.class));
    }
//

    /**
     * The name of the function has to be
     * onPaymentError
     * Wrap your code in try catch, as shown, to ensure that this method runs correctly
     */
    @SuppressWarnings("unused")
    @Override
    public void onPaymentError(int code, String response) {
        try {
            Toast.makeText(LoadMoneyPaymentFragment.this, "Payment failed: " + code + " " + response, Toast.LENGTH_SHORT).show();
            LoadMoneyPaymentFragment.this.finish();
        } catch (Exception e) {
//            Log.e(TAG, "Exception in onPaymentError", e);
        }
    }

    private boolean validateamount() {
        if (etLoadMoneyAmount.getText().toString().trim().isEmpty()) {
            etLoadMoneyAmount.setError("Enter your amount");
            requestFocus(etLoadMoneyAmount);
            return false;
        } else {
            etLoadMoneyAmount.setError(null);
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(LoadMoneyPaymentFragment.this, R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    public void showDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(LoadMoneyPaymentFragment.this, R.string.dialog_title2, message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(LoadMoneyPaymentFragment.this).sendBroadcast(intent);
    }

    private void showInvalidTranDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(LoadMoneyPaymentFragment.this, R.string.dialog_title2, inValidMessage);
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

//  public void initialLoadMoneyEBS(final String amount) {
//    Log.i("INITIATE URL", ApiUrl.URL_INITIATE_LOAD_MONEY);
//    StringRequest postReq = new StringRequest(Request.Method.POST, ApiUrl.URL_INITIATE_LOAD_MONEY, new Response.Listener<String>() {
//      @Override
//      public void onResponse(String response) {
//        Log.i("Initiate Response", response.toString());
//        try {
//          JSONObject resObj = new JSONObject(response);
//          boolean successMsg = resObj.getBoolean("success");
//          if (successMsg) {
//            String referenceNo = resObj.getString("referenceNo");
//            String currency = resObj.getString("currency");
//            String description = resObj.getString("description");
//            String name = resObj.getString("name");
//            String email = resObj.getString("email");
//            String address = resObj.getString("address");
//            String cityName = resObj.getString("city");
//            String stateName = resObj.getString("state");
//            String countryName = resObj.getString("country");
//            String postalCode = resObj.getString("postalCode");
//            String shipName = resObj.getString("shipName");
//            String shipAddress = resObj.getString("shipAddress");
//            String shipCity = resObj.getString("shipCity");
//            String shipState = resObj.getString("shipState");
//            String shipCountry = resObj.getString("shipCountry");
//            String shipPostalCode = resObj.getString("shipPostalCode");
//            String shipPhone = resObj.getString("shipPhone");
//
//            int ACC_ID = 20696;
//            String SECRET_KEY = "6496e4db9ebf824ffe2269afee259447";
//            String HOST_NAME = getResources().getString(R.string.hostname);
//
//            PaymentRequest.getInstance().setTransactionAmount(amount);
//            PaymentRequest.getInstance().setAccountId(ACC_ID);
//            PaymentRequest.getInstance().setSecureKey(SECRET_KEY);
//
//            PaymentRequest.getInstance().setReferenceNo(referenceNo);
//            PaymentRequest.getInstance().setBillingEmail(email);
//            PaymentRequest.getInstance().setFailureid("1");
//            PaymentRequest.getInstance().setCurrency(currency);
//            PaymentRequest.getInstance().setTransactionDescription(description);
//            PaymentRequest.getInstance().setBillingName(name);
//            PaymentRequest.getInstance().setBillingAddress(address);
//            PaymentRequest.getInstance().setBillingCity(cityName);
//            PaymentRequest.getInstance().setBillingPostalCode(postalCode);
//            PaymentRequest.getInstance().setBillingState(stateName);
//            PaymentRequest.getInstance().setBillingCountry(countryName);
//            PaymentRequest.getInstance().setBillingPhone(session.getUserMobileNo());
//            PaymentRequest.getInstance().setShippingName(shipName);
//            PaymentRequest.getInstance().setShippingAddress(shipAddress);
//            PaymentRequest.getInstance().setShippingCity(shipCity);
//            PaymentRequest.getInstance().setShippingPostalCode(shipPostalCode);
//            PaymentRequest.getInstance().setShippingState(shipState);
//            PaymentRequest.getInstance().setShippingCountry(shipCountry);
//            PaymentRequest.getInstance().setShippingEmail(email);
//            PaymentRequest.getInstance().setShippingPhone(shipPhone);
//            PaymentRequest.getInstance().setLogEnabled("1");
//
//            PaymentRequest.getInstance().setHidePaymentOption(true);
//            PaymentRequest.getInstance().setHideCashCardOption(true);
//            PaymentRequest.getInstance().setHideCreditCardOption(false);
//            PaymentRequest.getInstance().setHideDebitCardOption(false);
//            PaymentRequest.getInstance().setHideNetBankingOption(false);
//            PaymentRequest.getInstance().setHideStoredCardOption(true);
//
//            ArrayList<HashMap<String, String>> custom_post_parameters = new ArrayList<>();
//
//            HashMap<String, String> hashpostvalues = new HashMap<>();
//            hashpostvalues.put("account_details", "saving");
//            hashpostvalues.put("merchant_type", "vpayqwik");
//            custom_post_parameters.add(hashpostvalues);
//
//            PaymentRequest.getInstance().setCustomPostValues(custom_post_parameters);
//            loadDlg.dismiss();
//            EBSPayment.getInstance().init(LoadMoneyFragment.this, ACC_ID, SECRET_KEY, Mode.ENV_LIVE, Encryption.ALGORITHM_MD5, HOST_NAME);
//
//          } else {
//            CustomToast.showMessage(LoadMoneyFragment.this, "Unable to perform transaction. Please try again");
//          }
//        } catch (JSONException e) {
//          e.printStackTrace();
//          CustomToast.showMessage(LoadMoneyFragment.this, getResources().getString(R.string.server_exception2));
//
//        }
//      }
//    }, new Response.ErrorListener() {
//      @Override
//      public void onErrorResponse(VolleyError error) {
//        CustomToast.showMessage(LoadMoneyFragment.this, getResources().getString(R.string.server_exception));
//      }
//    }) {
//      @Override
//      protected Map<String, String> getParams() {
//        Map<String, String> params = new HashMap<>();
//        params.put("sessionId", session.getUserSessionId());
//        params.put("amount", amount);
//        params.put("name", session.getUserFirstName());
//        params.put("email", session.getUserEmail());
//        params.put("phone", session.getUserMobileNo());
//
//        Log.d("LoadMoneyParam", "LoadParams: " + params);
//        return params;
//      }
//
//      @Override
//      public Map<String, String> getHeaders() throws AuthFailureError {
//        HashMap<String, String> map = new HashMap<>();
//        map.put("hash", "123");
//        return map;
//      }
//    };
//
//    int socketTimeout = 60000;
//    RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//    postReq.setRetryPolicy(policy);
//    PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
//  }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        Crashlytics.setUserIdentifier(session.getUserMobileNo());
        Crashlytics.setUserEmail(session.getUserEmail());
        Crashlytics.setUserName(session.getUserFirstName() + " " + session.getUserLastName());
    }


}
