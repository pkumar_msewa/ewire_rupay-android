package in.MadMoveGlobal.fragment.fragmentuser;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.crashlytics.android.Crashlytics;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.MadMoveGlobal.EwireRuPay.activity.MainMenuDetailActivity;
import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.fragment.fragmentnaviagtionitems.TravelHealthFragment;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.metadata.AppMetadata;
import in.MadMoveGlobal.util.NetworkErrorHandler;
import in.MadMoveGlobal.util.SecurityUtil;
import io.fabric.sdk.android.Fabric;

//import in.payqwik.util.EncryptDecryptUserUtility;


/**
 * Created by Ksf on 3/27/2016.
 */
public class LogRegMobFragment extends Fragment {
    private View rootView;


    private Button btnRegister;
    private EditText etLoginMobile;
    private JSONObject jsonRequest;
    private LoadingDialog loadDlg;
    private String tag_json_obj = "json_user";
    private Boolean fullyFilled;
    private ImageView imagToolbar;
    private Button btnTermCondition;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Fabric.with(getActivity(), new Crashlytics());
        rootView = inflater.inflate(R.layout.fragment_card_log_regmob, container, false);
        loadDlg = new LoadingDialog(getActivity());
        btnRegister = (Button) rootView.findViewById(R.id.btnRegister);
        etLoginMobile = (EditText) rootView.findViewById(R.id.etLoginMobile);
        etLoginMobile.addTextChangedListener(new MyTextWatcher(etLoginMobile));
        imagToolbar = (ImageView) rootView.findViewById(R.id.imagToolbar);
        btnTermCondition = (Button) rootView.findViewById(R.id.btnTermCondition);
        Picasso.with(getActivity()).load("gkdhfg").resize(200, 200).placeholder(R.drawable.logo_mob_reg).into(imagToolbar);

        PayQwikApplication.getInstance().trackScreenView("Login");
        PayQwikApplication.getInstance().trackEvent("Login", "", "");
        btnRegister.setOnClickListener(new View.OnClickListener() {

            @Override

            public void onClick(View v) {

                if (!validateMobile()) {
                    return;
                } else {

                    otpGenration();
                }
            }
        });

        btnTermCondition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent menuIntent = new Intent(getActivity(), MainMenuDetailActivity.class);
                menuIntent.putExtra(AppMetadata.FRAGMENT_TYPE, "TermsAndCondition");
                getActivity().startActivity(menuIntent);

//                TravelHealthFragment fragment = new TravelHealthFragment();
//                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
//                Bundle bType = new Bundle();
//                bType.putString("URL", "http://ewiresofttech.com/tandc.html");
//                fragment.setArguments(bType);
//                fragmentTransaction.replace(R.id.llLRMain, fragment);
//                fragmentTransaction.commit();




            }
        });


        return rootView;
    }

    private boolean validateMobile() {
        if (etLoginMobile.getText().toString().trim().isEmpty() || etLoginMobile.getText().toString().trim().length() < 10) {
            etLoginMobile.setError("Enter 10 digit no");
            requestFocus(etLoginMobile);
            return false;
        } else {
            etLoginMobile.setError(null);
        }

        return true;
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            int i = view.getId();

            if (i == R.id.etLoginMobile) {
                Log.i("RRRRRRR", String.valueOf(validateMobile()));
                if (validateMobile()) {
                    btnRegister.setVisibility(View.VISIBLE);
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(etLoginMobile.getWindowToken(), 0);
                }
            }
        }
    }


    private void otpGenration() {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("contactNo", etLoginMobile.getText().toString());
            jsonRequest.put("androidDeviceID", SecurityUtil.getAndroidId(getActivity()));


        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {

            Log.i("URL LOGIN", ApiUrl.URL_VERIFIED_MOBILE_OTP);
            Log.i("LoginRequest", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_VERIFIED_MOBILE_OTP, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.i("Registration Response", response.toString());
                        String messageAPI = response.getString("message");
                        String code = response.getString("code");
                        if (code != null && code.equals("S00")) {
                            fullyFilled = response.getBoolean("fullyFilled");

                            loadDlg.dismiss();


                            LogRegOTPFragment fragment = new LogRegOTPFragment();
                            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                            Bundle args = new Bundle();
                            args.putString("mobileNumber", etLoginMobile.getText().toString());
                            fragment.setArguments(args);
                            fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, 0);
                            fragmentTransaction.replace(R.id.llLRMain, fragment);
                            fragmentTransaction.addToBackStack("");
                            fragmentTransaction.commit();


                        } else if (code != null && code.equals("F00") && response.getBoolean("fullyFilled") && response.getBoolean("hasError")) {
                            loadDlg.dismiss();
                            CustomToast.showMessage(getActivity(), messageAPI);
//                            LogRegFragment fragment = new LogRegFragment();
//                            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
//                            fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, 0);
//                            fragmentTransaction.replace(R.id.llLRMain, fragment);
//                            fragmentTransaction.addToBackStack("");
//                            fragmentTransaction.commit();
                        } else if (code != null && code.equals("F00") && !response.getBoolean("fullyFilled") && !response.getBoolean("hasError")) {
                            loadDlg.dismiss();
                            LogRegBasicFragment fragment = new LogRegBasicFragment();
                            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                            Bundle args = new Bundle();
                            args.putString("mobileNumber", etLoginMobile.getText().toString());
                            fragment.setArguments(args);
                            fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, 0);
                            fragmentTransaction.replace(R.id.llLRMain, fragment);
                            fragmentTransaction.addToBackStack("");
                            fragmentTransaction.commit();
                        } else {

                            CustomToast.showMessage(getActivity(), messageAPI);


                            loadDlg.dismiss();
                        }
                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
                    loadDlg.dismiss();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().overridePendingTransition(R.anim.slide_in_right, 0);
    }
}
