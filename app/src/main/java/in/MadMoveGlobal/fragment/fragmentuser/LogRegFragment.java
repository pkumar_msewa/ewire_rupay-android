package in.MadMoveGlobal.fragment.fragmentuser;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.crashlytics.android.Crashlytics;
import com.squareup.picasso.Picasso;

import in.MadMoveGlobal.EwireRuPay.R;
import io.fabric.sdk.android.Fabric;

//import in.payqwik.util.EncryptDecryptUserUtility;


/**
 * Created by Ksf on 3/27/2016.
 */
public class LogRegFragment extends Fragment {
    private View rootView;
private ImageView ivtri;

    private Button btnLogin, btnSignup;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Fabric.with(getActivity(), new Crashlytics());
        rootView = inflater.inflate(R.layout.fragment_card_log_reg, container, false);
        btnLogin = (Button) rootView.findViewById(R.id.btnLogin);
        ivtri = (ImageView) rootView.findViewById(R.id.ivtri);
        btnSignup = (Button) rootView.findViewById(R.id.btnSignup);



            Picasso.with(getContext()).load(R.drawable.trio).into(ivtri);



        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogRegMobFragment fragment = new LogRegMobFragment();
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, 0);
                fragmentTransaction.replace(R.id.llLRMain, fragment);
                fragmentTransaction.addToBackStack("");
                fragmentTransaction.commit();
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                Intent verifyIntent = new Intent(getActivity(), LoginFragment.class);


                startActivity(verifyIntent);
            }
        });

        return rootView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().overridePendingTransition(R.anim.right_to_left, 0);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getActivity().overridePendingTransition(R.anim.right_to_left, 0);
    }

}
