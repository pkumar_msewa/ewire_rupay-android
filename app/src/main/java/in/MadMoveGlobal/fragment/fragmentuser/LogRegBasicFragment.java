package in.MadMoveGlobal.fragment.fragmentuser;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.crashlytics.android.Crashlytics;
//import com.expletus.mobiruck.MobiruckEvent;
//import com.expletus.mobiruck.MobiruckSdk;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.squareup.picasso.Picasso;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;

import in.MadMoveGlobal.adapter.OperatorSpinnerAdapter;
import in.MadMoveGlobal.custom.CustomAlertDialog;
import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.loyaltyoffer.ShowWebviewActivity;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.metadata.MenuMetadata;
import in.MadMoveGlobal.model.OperatorsModel;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;
import io.fabric.sdk.android.Fabric;

import static in.MadMoveGlobal.fragment.fragmentuser.RegisterFragment.getEmail;

//import in.payqwik.util.EncryptDecryptUserUtility;


/**
 * Created by Ksf on 3/27/2016.
 */
public class LogRegBasicFragment extends Fragment {
    private View rootView;
    private static boolean VISIBLE_PASSWORD = false;
    private String tag_json_obj = "json_user";
    private String groupKey;
    private Button btnReg;
    private JSONObject jsonRequest;
    private String userMobileNo;
    private LoadingDialog loadDlg;
    private final Pattern hasUppercase = Pattern.compile("[A-Z]");
    private final Pattern hasLowercase = Pattern.compile("[a-z]");
    private final Pattern hasNumber = Pattern.compile("\\d");
    private final Pattern hasSpecialChar = Pattern.compile("[^a-zA-Z0-9 ]");
    private OperatorSpinnerAdapter operatorSpinnerAdapter;
    private Spinner spinner_govtid_provider;
    private RadioButton rbRegisterMale, rbRegisterFeMale;
    private LinearLayout llfirst, llVoterid;
    private EditText etRegisterFName, etRegisterDob, etRegisterEmail, etRegisterPassword, etRegisterLName, etRegisterMName, etaadhar;
    private TextView tvWelcome, tvBack, tvNext;
    private String serviceProvider, serviceProviderName, serviceProviderType;
    private String blockCharacterSet = "~#^|$%&*`~@()_-+=|:;></.,'{}[]()!abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private String govtId;
    private ImageView imagToolbar, ivPassw;
    private CardView aadhar, drivinglicense, pancard, passport, voterid;
    private int postion;
    //new Changes for group
    private ArrayList<String> groupnames;
    private HashMap<String, String> contactNos;
    private MaterialEditText etGroupList, etRemark;
    private CheckBox rewardCheckBox;
    private TextView termsTV;
    private boolean isRewardEnable;

    @SuppressLint("ClickableViewAccessibility")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Fabric.with(getActivity(), new Crashlytics());
        rootView = inflater.inflate(R.layout.fragment_basic_details, container, false);
        Bundle b = getArguments();
        userMobileNo = b.getString("mobileNumber");
        Log.i("MOBILE", userMobileNo);
        loadGroupList();
        groupnames = new ArrayList<>();
        contactNos = new HashMap<>();
        btnReg = (Button) rootView.findViewById(R.id.btnReg);
        etRegisterFName = (EditText) rootView.findViewById(R.id.etRegisterFName);
        etRegisterDob = (EditText) rootView.findViewById(R.id.etRegisterDob);
        etRegisterEmail = (EditText) rootView.findViewById(R.id.etRegisterEmail);
        etRegisterPassword = (EditText) rootView.findViewById(R.id.etRegisterPassword);
        etRegisterLName = (EditText) rootView.findViewById(R.id.etRegisterLName);
        tvWelcome = (TextView) rootView.findViewById(R.id.tvWelcome);
        ivPassw = (ImageView) rootView.findViewById(R.id.ivPassw);
        aadhar = (CardView) rootView.findViewById(R.id.cvAadhar);
        etGroupList = rootView.findViewById(R.id.etGroupList);
        etRemark = rootView.findViewById(R.id.etRemark);
        etRegisterMName = (EditText) rootView.findViewById(R.id.etRegisterMName);
        rbRegisterMale = (RadioButton) rootView.findViewById(R.id.rbRegisterMale);
        rbRegisterFeMale = (RadioButton) rootView.findViewById(R.id.rbRegisterFeMale);
//        drivinglicense = (CardView) rootView.findViewById(R.id.cvdl);
//        pancard = (CardView) rootView.findViewById(R.id.cvPancard);
//        passport = (CardView) rootView.findViewById(R.id.cvPassport);
//        voterid = (CardView) rootView.findViewById(R.id.cvVI);

        etaadhar = (EditText) rootView.findViewById(R.id.etAadhar);


        spinner_govtid_provider = (Spinner) rootView.findViewById(R.id.spinner_govtid_provider);
        llfirst = (LinearLayout) rootView.findViewById(R.id.llfirst);
        llVoterid = (LinearLayout) rootView.findViewById(R.id.llVoterid);
        tvNext = (TextView) rootView.findViewById(R.id.tvNext);
        tvBack = (TextView) rootView.findViewById(R.id.tvBack);


       rewardCheckBox = rootView.findViewById(R.id.reward_checkbox);
      termsTV = rootView.findViewById(R.id.terms_tv);

        termsTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ShowWebviewActivity.class));
            }
        });

        rewardCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rewardCheckBox.isChecked()){
                    isRewardEnable = true;
                }else {
                    isRewardEnable =false;
                }
            }
        });

//        etGroupList.setEnabled(false);
        etGroupList.setFocusable(false);


        etGroupList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showGrouplist();
            }
        });

        ivPassw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (VISIBLE_PASSWORD) {
                    VISIBLE_PASSWORD = false;
                    ivPassw.setImageResource(R.drawable.ic_visibilityeye);
                    etRegisterPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());

                } else {
                    VISIBLE_PASSWORD = true;
                    ivPassw.setImageResource(R.drawable.ic_eyevisisvility);
//                    etRegisterPassword.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    etRegisterPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }

            }
        });


//        etRegisterPassword.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                final int DRAWABLE_LEFT = 0;
//                final int DRAWABLE_TOP = 1;
//                final int DRAWABLE_RIGHT = 2;
//                final int DRAWABLE_BOTTOM = 3;
//
//                if (event.getAction() == MotionEvent.ACTION_UP) {
//                    if (event.getRawX() >= (etRegisterPassword.getRight() - etRegisterPassword.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
//                        // your action here
//                        //Helper.toast(LoginActivity.this, "Toggle visibility");
//                        if (VISIBLE_PASSWORD) {
//                            VISIBLE_PASSWORD = false;
//                            etRegisterPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
//                            etRegisterPassword.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_password_01, 0, R.drawable.ic_visibilityeye, 0);
//                        } else {
//                            VISIBLE_PASSWORD = true;
//                            etRegisterPassword.setInputType(InputType.TYPE_CLASS_TEXT);
//                            etRegisterPassword.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_password_01, 0, R.drawable.ic_eyevisisvility, 0);
//                        }
//                        return false;
//                    }
//                }
//                return false;
//            }
//        });


        if (getEmail(getActivity()) != null && !getEmail(getActivity()).isEmpty()) {
            etRegisterEmail.setText(getEmail(getActivity()));
        }
//
//        tvBack = (TextView) rootView.findViewById(R.id.tvBack);
        imagToolbar = (ImageView) rootView.findViewById(R.id.imagToolbar);
        Picasso.with(getActivity()).load("gkdhfg").resize(200, 200).placeholder(R.drawable.logo_mob_reg).into(imagToolbar);

//        tvBack.setVisibility(View.GONE);
        operatorSpinnerAdapter = new OperatorSpinnerAdapter(getActivity(), R.layout.spinners, MenuMetadata.getgovtId());
        spinner_govtid_provider.setAdapter(operatorSpinnerAdapter);

        loadDlg = new LoadingDialog(getActivity());

        spinner_govtid_provider.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                OperatorsModel operatorsModel = (OperatorsModel) adapterView.getSelectedItem();
                if (i != 0) {
                    postion = Integer.valueOf(operatorsModel.code);
                }
                aadhar.setVisibility(View.VISIBLE);
                etaadhar.setHint(operatorsModel.getName());
                etaadhar.setText("");
                govtId = operatorsModel.getName();

//                if (operatorsModel.getName().equalsIgnoreCase("DRIVING LICENSE") || spinner_govtid_provider.getSelectedItemPosition() == 2) {
//                    showDLDialog(generateDLMessageDialog());
//                }

//                if (postion == 0) {
//                    etaadhar.setInputType(InputType.TYPE_CLASS_NUMBER);
//                } else {
//                    etaadhar.setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
////                    etaadhar.setKeyListener(DigitsKeyListener.getInstance("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJLMNOPQRSTUVWXYZ-"));
//                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        etaadhar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (govtId != null && govtId.equalsIgnoreCase("aadhaar")) {
                    if (charSequence.length() > 0) {
                        int length = etaadhar.getText().toString().length();
                        if (i2 != 0) {
                            String str = "" + charSequence.charAt(i2 - 1);
                            if (blockCharacterSet.contains(str)) {
                                if (i2 > length - 1) {
                                    etaadhar.setText(etaadhar.getText().toString().substring(i, length - 1));
                                } else if (i2 - 1 == 0) {
                                    etaadhar.setText(etaadhar.getText().toString().substring(i + 1, length));
                                } else {
                                    String st = etaadhar.getText().toString().substring(i, i2 - 1) + etaadhar.getText().toString().substring(i2, length);
                                    etaadhar.setText(st);
                                }
                            } else {
                                return;
                            }
                        }
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!validateValues(spinner_govtid_provider.getSelectedItemPosition())) {
                    return;
                }


            }
        });

        int[] color = {Color.RED, Color.BLUE};
        float[] position = {0, 1};
        Shader.TileMode tile_mode0 = Shader.TileMode.REPEAT; // or TileMode.REPEAT;
        LinearGradient lin_grad0 = new LinearGradient(560, 200, 0, 0, color, position, tile_mode0);
        Shader shader_gradient0 = lin_grad0;
        tvWelcome.getPaint().setShader(shader_gradient0);

        tvNext.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View v) {

                submitForm1();
            }
        });

        tvBack.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View v) {

                llfirst.setVisibility(View.VISIBLE);
                llVoterid.setVisibility(View.GONE);
                etRegisterFName.setFocusable(true);

            }
        });

        etRegisterDob.setFocusable(true);
        etRegisterDob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dateOfBirthDialog();
            }
        });


        btnReg.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View v) {
                submitForm();
            }
        });


        return rootView;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void submitForm1() {

        if (!validateFName()) {
            return;
        }
        if (!validateLName()) {
            return;
        }
        if (!validateEmail()) {

            return;
        }
        if (!validatePassword()) {
            return;
        }
        if (!validateDob()) {
            return;
        }
        if (etGroupList.getText() == null && etGroupList.getText().toString().equalsIgnoreCase("")) {
            etGroupList.setError("Please select any group");
            return;
        }
        validatemail();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void submitForm() {


//        if (!validatePassword()) {
//            return;
//        }


        serviceProvider = ((OperatorsModel) spinner_govtid_provider.getSelectedItem()).code;
        serviceProviderName = ((OperatorsModel) spinner_govtid_provider.getSelectedItem()).name;
        serviceProviderType = ((OperatorsModel) spinner_govtid_provider.getSelectedItem()).defaultCode;
        if (spinner_govtid_provider.getSelectedItemPosition() == 0) {
            CustomToast.showMessage(getActivity(), "Please select any valid ID to continue");
            return;
        }

        if (!validateValues(spinner_govtid_provider.getSelectedItemPosition())) {
            return;
        }


        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        promoteRegister();

    }


    private void promoteRegister() {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("firstName", etRegisterFName.getText().toString());
            jsonRequest.put("dateOfBirth", etRegisterDob.getText().toString());
            jsonRequest.put("email", etRegisterEmail.getText().toString());

            String basicAuth = new String(Base64.encode(etRegisterPassword.getText().toString().getBytes(), Base64.NO_WRAP));
            jsonRequest.put("password", basicAuth);
//            jsonRequest.put("password", etRegisterPassword.getText().toString());
            jsonRequest.put("middleName", etRegisterMName.getText().toString());
            jsonRequest.put("lastName", etRegisterLName.getText().toString());
            if (rbRegisterFeMale.isChecked()) {
                jsonRequest.put("gender", "female");
            } else {
                jsonRequest.put("gender", "male");
            }
            jsonRequest.put("contactNo", userMobileNo);
            jsonRequest.put("idNo", etaadhar.getText().toString());
            jsonRequest.put("idType", serviceProviderType);
            jsonRequest.put("group", contactNos.get(groupKey));
            jsonRequest.put("remark", etRemark.getText());
            jsonRequest.put("hasClavex",isRewardEnable);


        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {


            Log.i("LoginRequest", jsonRequest.toString());
            Log.i("URL LOGIN", ApiUrl.URL_REGISTRATION);
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_REGISTRATION, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        loadDlg.dismiss();
                        Log.i("Registration Response", response.toString());
                        String messageAPI = response.getString("message");
                        String code = response.getString("code");
                        if (code != null && code.equals("S00")) {

//                            MobiruckEvent mobiruckEvent = new MobiruckEvent();
//                            mobiruckEvent.setEvent("Registration");  // event name should match as added in the dashboard.
//                            MobiruckSdk.getInstance().logEvent(mobiruckEvent);


                            loadDlg.dismiss();
                            successDialog(messageAPI);

//                            Intent verifyIntent = new Intent(getActivity(), LoginFragment.class);
//                            verifyIntent.putExtra("userMobileNo", userMobileNo);
//                            verifyIntent.putExtra("OtpCode", "");
//
//                            loadDlg.dismiss();
////                            startActivity(autoIntent);
//                            startActivity(verifyIntent);


                        } else {

                            CustomToast.showMessage(getActivity(), messageAPI);

                            loadDlg.dismiss();
                        }
                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
//                    CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
                    loadDlg.dismiss();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }


    private void validatemail() {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("email", etRegisterEmail.getText().toString());
            jsonRequest.put("contactNo", userMobileNo);


        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {

            Log.i("URLMAIL", ApiUrl.URL_VALIDATE_MAIL);
            Log.i("LoginRequest", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_VALIDATE_MAIL, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        loadDlg.dismiss();
                        Log.i("MAIL Response", response.toString());
                        String messageAPI = response.getString("message");
                        String code = response.getString("code");
                        if (code != null && code.equals("S00")) {
                            llfirst.setVisibility(View.GONE);
                            llVoterid.setVisibility(View.VISIBLE);
                            etRegisterFName.setFocusable(false);
                            etRegisterPassword.setFocusable(true);
                            loadDlg.dismiss();
//                            successDialog(generateVerifyMessage());

//                            Intent verifyIntent = new Intent(getActivity(), LoginFragment.class);
//                            verifyIntent.putExtra("userMobileNo", userMobileNo);
//                            verifyIntent.putExtra("OtpCode", "");
//
//                            loadDlg.dismiss();
////                            startActivity(autoIntent);
//                            startActivity(verifyIntent);
                        } else if (code != null && code.equals("F00")) {

                            loadDlg.dismiss();
                            CustomToast.showMessage(getActivity(), messageAPI);


                        } else {

                            CustomToast.showMessage(getActivity(), messageAPI);

                            loadDlg.dismiss();
                        }
                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
//                    CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
                    loadDlg.dismiss();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }


    private boolean validatePasswordListner() {
        if (etRegisterPassword.getText().toString().trim().isEmpty() || etRegisterPassword.getText().toString().trim().length() < 6) {
            etRegisterPassword.setError(getResources().getString(R.string.password_validity));
            requestFocus(etRegisterPassword);
            return false;
        } else {
//            etRegisterPassword.setErrorEnabled(false);
            checkPwdStr();
        }

        return true;
    }

    private String checkPwdStr() {
        if (hasUppercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasLowercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasNumber.matcher(etRegisterPassword.getText().toString().trim()).find() && hasSpecialChar.matcher(etRegisterPassword.getText().toString().trim()).find()) {
            etRegisterPassword.setError("Strong Password");
        } else if (hasUppercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasLowercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasNumber.matcher(etRegisterPassword.getText().toString().trim()).find()) {
            etRegisterPassword.setError("Strong Password");
        } else if (hasSpecialChar.matcher(etRegisterPassword.getText().toString().trim()).find() && hasLowercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasNumber.matcher(etRegisterPassword.getText().toString().trim()).find()) {
            etRegisterPassword.setError("Strong Password");
        } else if (hasSpecialChar.matcher(etRegisterPassword.getText().toString().trim()).find() && hasUppercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasNumber.matcher(etRegisterPassword.getText().toString().trim()).find()) {
            etRegisterPassword.setError("Strong Password");
        } else if (hasUppercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasNumber.matcher(etRegisterPassword.getText().toString().trim()).find()) {
            etRegisterPassword.setError("Good Password");
        } else if (hasUppercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasSpecialChar.matcher(etRegisterPassword.getText().toString().trim()).find()) {
            etRegisterPassword.setError("Good Password");
        } else if (hasUppercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasLowercase.matcher(etRegisterPassword.getText().toString().trim()).find()) {
            etRegisterPassword.setError("Good Password");
        } else if (hasNumber.matcher(etRegisterPassword.getText().toString().trim()).find() && hasSpecialChar.matcher(etRegisterPassword.getText().toString().trim()).find()) {
            etRegisterPassword.setError("Good Password");
        } else if (hasNumber.matcher(etRegisterPassword.getText().toString().trim()).find() && hasLowercase.matcher(etRegisterPassword.getText().toString().trim()).find()) {
            etRegisterPassword.setError("Good Password");
        } else {
            etRegisterPassword.setError("Weak Password");
        }


        return null;
    }

    private boolean validatePassword() {
        if (etRegisterPassword.getText().toString().trim().isEmpty()) {
            etRegisterPassword.setError("*Required password");
            requestFocus(etRegisterPassword);
            return false;
        } else if (etRegisterPassword.getText().toString().trim().length() < 6) {
            etRegisterPassword.setError(getResources().getString(R.string.password_validity));
            requestFocus(etRegisterPassword);
            return false;
        } else {
            etRegisterPassword.setError(null);
        }

        return true;
    }

    private boolean validateFName() {
        if (etRegisterFName.getText().toString().trim().isEmpty()) {
            etRegisterFName.setError("*Enter First name");
            requestFocus(etRegisterFName);
            return false;
        } else {
            etRegisterFName.setError(null);
        }

        return true;
    }

    private boolean validateValues(int postion) {
        if (postion == 1) {
            InputFilter[] FilterArray = new InputFilter[1];
            FilterArray[0] = new InputFilter.LengthFilter(12);
            etaadhar.setFilters(FilterArray);
            return validateaadhar();
        } else if (postion == 2) {
            InputFilter[] FilterArray = new InputFilter[1];
            FilterArray[0] = new InputFilter.LengthFilter(20);
            etaadhar.setFilters(FilterArray);
            return validatedl();
        } else if (postion == 3) {
            InputFilter[] FilterArray = new InputFilter[1];
            FilterArray[0] = new InputFilter.LengthFilter(16);
            etaadhar.setFilters(FilterArray);
            return validatepassport();
        } else if (postion == 4) {
            InputFilter[] FilterArray = new InputFilter[1];
            FilterArray[0] = new InputFilter.LengthFilter(20);
            etaadhar.setFilters(FilterArray);
            return validatevoterid();
        } else if (postion == 0) {
            InputFilter[] FilterArray = new InputFilter[1];
            FilterArray[0] = new InputFilter.LengthFilter(20);
            etaadhar.setFilters(FilterArray);
            return validateSelectId();
        }
        return true;
    }

    private boolean validateLName() {
        if (etRegisterLName.getText().toString().trim().isEmpty()) {
            etRegisterLName.setError("*Enter Last name");
            requestFocus(etRegisterLName);
            return false;
        } else {
            etRegisterLName.setError(null);
        }

        return true;
    }

    private boolean validateEmail() {
        String email = etRegisterEmail.getText().toString().trim();
        if (email.isEmpty() || !isValidEmail(email)) {
            etRegisterEmail.setError("*Enter valid email");
            requestFocus(etRegisterEmail);
            return false;
        } else {
            etRegisterEmail.setError(null);
        }

        return true;
    }

    private boolean validateDob() {
        if (etRegisterDob.getText().toString().trim().isEmpty()) {
            etRegisterDob.setError("*Enter Date of birth");
            requestFocus(etRegisterDob);
            return false;
        } else {
            etRegisterDob.setError(null);
        }

        return true;
    }

//    private boolean validateIdno() {
//        if (etGovtIdNo.getText().toString().trim().isEmpty() || etGovtIdNo.getText().toString().trim().length() < 11) {
//            etGovtIdNo.setError("Enter valid Id number");
//            requestFocus(etGovtIdNo);
//            return false;
//        } else {
//            etGovtIdNo.setError(null);
//        }
//
//        return true;
//    }


    private boolean validateaadhar() {
        if (etaadhar.getText().toString().trim().isEmpty() || etaadhar.getText().toString().trim().length() != 12) {
            etaadhar.setError("Enter valid Aadhaar number");
            requestFocus(etaadhar);
            return false;
        } else {
            etaadhar.setError(null);
        }

        return true;
    }

    private boolean validatedl() {
        if (etaadhar.getText().toString().trim().isEmpty() || etaadhar.getText().toString().trim().length() < 10) {
            etaadhar.setError("Enter valid DL number, do not use special characters and space");
            requestFocus(etaadhar);
            return false;
        } else {
            etaadhar.setError(null);
        }

        return true;
    }

    private boolean validatepancard() {
        if (etaadhar.getText().toString().trim().isEmpty() || etaadhar.getText().toString().trim().length() != 10) {
            etaadhar.setError("Enter valid Pan Card number");
            requestFocus(etaadhar);
            return false;
        } else {
            etaadhar.setError(null);
        }

        return true;
    }

    private boolean validatepassport() {
        if (etaadhar.getText().toString().trim().isEmpty() || etaadhar.getText().toString().trim().length() < 8) {
            etaadhar.setError("Enter valid passport number, do not use special characters and space");
            requestFocus(etaadhar);
            return false;
        } else {
            etaadhar.setError(null);
        }

        return true;
    }

    private boolean validateSelectId() {
        if (spinner_govtid_provider.getSelectedItemPosition() == 0) {
            etaadhar.setError("Please select any valid ID to continue");


            return false;
        } else {

//            etGovtIdNo.setError(null);
        }

        return true;
    }


    private boolean validatevoterid() {
        if (etaadhar.getText().toString().trim().isEmpty() || etaadhar.getText().toString().trim().length() < 10) {
            etaadhar.setError("Enter valid voter id number, do not use special characters and space");
            requestFocus(etaadhar);
            return false;
        } else {
            etaadhar.setError(null);
        }

        return true;
    }


    public void dateOfBirthDialog() {
//        DatePickerDialog dialogDate = new DatePickerDialog(getActivity(), R.style.DatePicker, new DatePickerDialog.OnDateSetListener() {
//            @Override
//            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
//                Calendar userAge = new GregorianCalendar(year, (monthOfYear + 1), dayOfMonth);
//                Calendar minAdultAge = new GregorianCalendar();
//                minAdultAge.add(Calendar.YEAR, -18);
//                if (minAdultAge.before(userAge)) {
//                    etRegisterDob.setError("User must be at least 18 years old to Register");
//                    CustomToast.showMessage(getActivity(), "User must be at least 18 years old to Register");
//                    etRegisterDob.requestFocus();
//                } else {
//
//                    etRegisterDob.setText(String.valueOf(year) + "-"
//                            + String.valueOf(monthOfYear + 1) + "-"
//                            + String.valueOf(dayOfMonth));
//                }
//            }
//        }, 1990, 1, 1);
//        dialogDate.setButton(DatePickerDialog.BUTTON_POSITIVE, this.getResources().getString(R.string.zxing_button_ok), dialogDate);
//        dialogDate.setButton(DatePickerDialog.BUTTON_NEGATIVE, this.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//                dialogInterface.dismiss();
//            }
//        });
//        dialogDate.getDatePicker().setMaxDate(new Date().getTime());
//        dialogDate.show();

        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        Calendar c = Calendar.getInstance();

        Date myDate = c.getTime();

        int year = c.get(Calendar.YEAR) - 18;
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_YEAR);

        Log.i("datesss", String.valueOf(year));
        new SpinnerDatePickerDialogBuilder()
                .context(getActivity())
                .callback(new com.tsongkha.spinnerdatepicker.DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(com.tsongkha.spinnerdatepicker.DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar calendar = new GregorianCalendar(year, monthOfYear, dayOfMonth);
                        etRegisterDob.setText(simpleDateFormat.format(calendar.getTime()));
                    }
                })
                .spinnerTheme(R.style.NumberPickerStyle)

                .maxDate(year, month, day)
                .minDate(1947, 7, 15)

                .build()
                .show();


    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public void successDialog(String message) {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(message));
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                Intent verifyIntent = new Intent(getActivity(), LoginFragment.class);
                verifyIntent.putExtra("userMobileNo", userMobileNo);
                verifyIntent.putExtra("OtpCode", "");

                loadDlg.dismiss();
//                            startActivity(autoIntent);
                startActivity(verifyIntent);


            }
        });
//        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                dialog.dismiss();
//            }
//        });
        builder.show();
    }

    public void showDLDialog(String message) {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(message));
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();


            }
        });
//        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                dialog.dismiss();
//            }
//        });
        builder.show();
    }

    public String generateVerifyMessage() {
        String source = "<b><font color=#000000> Your profile has been created.</font></b>" +
                "<br><br><b><font color=#ff0000> Continue to login. </font></b><br></br>";
        return source;
    }

    public String generateDLMessageDialog() {
        String source = "<b><font color=#000000> Please don't enter any special characters while entering driving license number.</font></b>";

        return source;
    }

    private void loadGroupList() {
        loadDlg = new LoadingDialog(getContext());
//    loadDlg.show();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_GROUP_LIST, new JSONObject(),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Group_List_Response", response.toString());
                        try {
                            String code = response.getString("code");

                            if (code != null) {
                                if (code.equalsIgnoreCase("S00")) {
                                    loadDlg.dismiss();
                                    JSONArray jsonArray = response.getJSONArray("groupName");

                                    for (int i = 0; i < jsonArray.length(); i++) {
//                      groupDatas.put(jsonArray.getJSONObject(i).getString("groupName"),jsonArray.getJSONObject(i).getString("contactNo"));
                                        groupnames.add(jsonArray.getJSONObject(i).getString("groupName"));
                                        contactNos.put(jsonArray.getJSONObject(i).getString("groupName"), jsonArray.getJSONObject(i).getString("contactNo"));
                                    }

                                } else {
                                    loadDlg.dismiss();
                                    CustomToast.showMessage(getContext(), response.getString("message"));
                                }
                            }
                        } catch (Exception e) {
                            loadDlg.dismiss();
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loadDlg.dismiss();
            }
        }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> map = new HashMap<>();
//        map.put("hash", "1234");
                map.put("Content-Type", "application/json");
                return map;
            }
        };
        int socketTimeout = 240000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonObjectRequest.setRetryPolicy(policy);
        PayQwikApplication.getInstance().addToRequestQueue(jsonObjectRequest, tag_json_obj);
    }

    private void showGrouplist() {

        if (groupnames.size() > 0) {
            android.support.v7.app.AlertDialog.Builder placeDialog =
                    new android.support.v7.app.AlertDialog.Builder(getContext(), R.style.AppCompatAlertDialogStyle);
            placeDialog.setTitle("Select Your Group");

            placeDialog.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                }
            });

            placeDialog.setItems(groupnames.toArray(new String[groupnames.size()]),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int i) {
                            etGroupList.setText(groupnames.get(i));
                            groupKey = groupnames.get(i);
                            if (!groupKey.equalsIgnoreCase("None")) {
                                etRemark.setVisibility(View.VISIBLE);
                            } else {
                                etRemark.setVisibility(View.GONE);
                                etRemark.setText("");
                            }
                        }
                    });
            placeDialog.show();
        }
    }
}
