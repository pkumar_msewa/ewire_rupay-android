package in.MadMoveGlobal.fragment.fragmentuser;

import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.crashlytics.android.Crashlytics;
import com.squareup.picasso.Picasso;

import in.MadMoveGlobal.EwireRuPay.R;
import io.fabric.sdk.android.Fabric;

//import in.payqwik.util.EncryptDecryptUserUtility;


/**
 * Created by Ksf on 3/27/2016.
 */
public class LogRegDoneFragment extends Fragment {
    private View rootView;
    private ImageView rlmain;
    private Button btnAccess;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Fabric.with(getActivity(), new Crashlytics());
        rootView = inflater.inflate(R.layout.fragment_access_acc, container, false);
        rlmain=(ImageView)rootView.findViewById(R.id.rlmain);

        Picasso.with(getActivity()).load("fsdfs").placeholder(R.drawable.toolbar_color).into(rlmain);
        btnAccess = (Button) rootView.findViewById(R.id.btnAccess);

        int[] color = {Color.RED, Color.BLUE};
        float[] position = {0, 1};
        Shader.TileMode tile_mode0 = Shader.TileMode.REPEAT; // or TileMode.REPEAT;
        LinearGradient lin_grad0 = new LinearGradient(560, 200, 0, 0, color, position, tile_mode0);
        Shader shader_gradient0 = lin_grad0;
        btnAccess.getPaint().setShader(shader_gradient0);
        btnAccess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogRegFragment fragment = new LogRegFragment();
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.slide_in_left, 0);
                fragmentTransaction.replace(R.id.llLRMain, fragment);
                fragmentTransaction.addToBackStack("");
                fragmentTransaction.commit();
            }
        });
        return rootView;
    }


}
