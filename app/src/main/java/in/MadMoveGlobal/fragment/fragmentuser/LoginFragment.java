package in.MadMoveGlobal.fragment.fragmentuser;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.text.InputType;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import in.MadMoveGlobal.custom.CustomFingerPrintSuccessDialog;
import in.MadMoveGlobal.custom.CustomToast;
//import in.MadMoveGlobal.custom.FingerprintHandler;
import in.MadMoveGlobal.custom.LoadingDialog;

import in.MadMoveGlobal.fragment.FingerprintsHandlers;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.model.LoyaltyPointModel;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.EwireRuPay.activity.OtpVerificationActivity;
import in.MadMoveGlobal.util.NetworkErrorHandler;
import in.MadMoveGlobal.util.SecurityUtil;
import in.MadMoveGlobal.util.Utility;
import in.MadMoveGlobal.util.VerifyLoginOTPListner;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.EwireRuPay.activity.MainActivity;
import in.MadMoveGlobal.EwireRuPay.activity.OtpVerificationMidLayerActivity;
import io.fabric.sdk.android.Fabric;
import me.philio.pinentry.PinEntryView;
import pl.droidsonroids.gif.GifImageView;

//import in.payqwik.util.EncryptDecryptUserUtility;


/**
 * Created by Ksf on 3/27/2016.
 */
public class LoginFragment extends AppCompatActivity implements VerifyLoginOTPListner {
    private View rootView;
    private EditText etLoginMobile;
    private EditText etLoginPassword;
    private TextInputLayout ilLoginMobile;
    private TextInputLayout ilLoginPassword;
    private TextView tvRegisterSubmit, tvForgot, tvFrgMpin, tvwelcome, tvFingerprint;
    private String cardNo, cvv, cardHolder, expiryDate;
    private boolean VISIBLE_PASSWORD = false;
    private Button btnLogin;
    private ImageButton iBtnShowLoginPwd;
    private LoadingDialog loadDlg;
    private JSONObject jsonRequest;
    private Button ok;
    private SwitchCompat scheck;
    private String userMobileNo;
    private LinearLayout llmobile, llPassword, llPin, llFPin, llFpsw;
    boolean Hasdeviceid = true;
    private static final String KEY_NAME = "DushantMssPaymentHive";
    private Cipher cipher;
    SharedPreferences musicPreferences;
    private KeyStore keyStore;
    private GifImageView gifImage;
//    private JSONObject jsonRequestED;

    private UserModel session = UserModel.getInstance();
private  SharedPreferences loyaltysharedPreferences;
    private  LoyaltyPointModel loyaltyPointModel;

    public static final String PHONE = "phone";
    SharedPreferences phonePreferences;
    private String phoneNo;
    private PinEntryView DialogVerifyPin;
    private static final int PERMISSION_REQUEST_CODE = 12;
    //FIREBASE
    public static final String PROPERTY_REG_ID = "registration_id";
    private String regId = "";
//    int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);


    //Volley Tag
    private String tag_json_obj = "json_user";
    private boolean Mpin;
    private ImageView imagToolbar;

//    Fingerprint message

    private String message1, message2, message3, message4;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.fragment_new_login);
        musicPreferences = getSharedPreferences("ShowMusicDailog", Context.MODE_PRIVATE);
        phonePreferences = getSharedPreferences(PHONE, Context.MODE_PRIVATE);
        phoneNo = phonePreferences.getString("phone", "");
        tvFingerprint = (TextView) findViewById(R.id.tvFingerprint);
        gifImage = (GifImageView) findViewById(R.id.gifImage);

        etLoginMobile = (EditText) findViewById(R.id.etLoginMobile);
        etLoginPassword = (EditText) findViewById(R.id.etLoginPassword);
        btnLogin = (Button) findViewById(R.id.btnLogin);

        tvForgot = (TextView) findViewById(R.id.tvForgot);
        imagToolbar = (ImageView) findViewById(R.id.imagToolbar);
        iBtnShowLoginPwd = (ImageButton) findViewById(R.id.iBtnShowLoginPwd);
        Picasso.with(this).load("gkdhfg").resize(200, 200).placeholder(R.drawable.logo_mob_reg).into(imagToolbar);
//        etLoginMobile.addTextChangedListener(new MyTextWatcher(etLoginMobile));
//        etLoginPassword.addTextChangedListener(new MyTextWatcher(etLoginPassword));
//        Bundle b = getArguments();
        loadDlg = new LoadingDialog(LoginFragment.this);
//        userMobileNo = b.getString("mobileNumber");


        etLoginMobile.setText(getStoredPhone());
//        showSuccessDialogmsg("Enjoy super click login with your fingerprint!!");= new LoyaltyPointModel()

        loyaltyPointModel = new LoyaltyPointModel();

        loyaltysharedPreferences = getSharedPreferences("LoyaltyPoint",Context.MODE_PRIVATE);



        //DONE CLICK ON VIRTUAL KEYPAD
        etLoginPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    submitForm();
                }
                return false;
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitForm();

            }
        });


        tvForgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!validateMobile()) {
                    return;
                }

                loadDlg.show();
                promoteForgetPwd();
            }
        });


        iBtnShowLoginPwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (VISIBLE_PASSWORD) {
                    VISIBLE_PASSWORD = false;
                    iBtnShowLoginPwd.setImageResource(R.drawable.ic_visibilityeye);
                    etLoginPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());

                } else {
                    VISIBLE_PASSWORD = true;
                    iBtnShowLoginPwd.setImageResource(R.drawable.ic_eyevisisvility);
//                    etRegisterPassword.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    etLoginPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }

            }
        });

//        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
//            tvFingerprint.setVisibility(View.GONE);
//            gifImage.setVisibility(View.GONE);
////            tvFingerprint.setText("This Android version does not support fingerprint authentication.");
//
//        } else {
//
//            tvFingerprint.setVisibility(View.VISIBLE);
//            gifImage.setVisibility(View.VISIBLE);
//
//            try {
//                KeyguardManager keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
//                FingerprintManager fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);
//
//
//                if (!fingerprintManager.isHardwareDetected()) {
//
//                    showSuccessDialogmsg("Your Device does not have a Fingerprint Sensor");
//                } else {
//                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
//                        showSuccessDialogmsg("Fingerprint authentication permission not enabled");
//                    } else {
//                        if (!fingerprintManager.hasEnrolledFingerprints()) {
//                            showSuccessDialogmsg("Register at least one fingerprint in Settings");
//                        } else {
//                            if (!keyguardManager.isKeyguardSecure()) {
//                                showSuccessDialogmsg("Lock screen security not enabled in Settings");
//                            } else {
//                                generateKey();
//
//
//                                if (cipherInit()) {
//                                    FingerprintManager.CryptoObject cryptoObject = new FingerprintManager.CryptoObject(cipher);
//                                    FingerprintsHandlers helper = new FingerprintsHandlers(this);
//                                    helper.startAuth(fingerprintManager, cryptoObject);
//
//
//                                }
//                            }
//                        }
//                    }
//                }
//            } catch (NullPointerException ne) {
//
//            }
//        }
    }

    private String getStoredPhone() {
        if (phoneNo != null && !phoneNo.isEmpty()) {
            return phoneNo;
        } else {
            return "";
        }
    }


    private boolean validatePassword() {
        if (etLoginPassword.getText().toString().trim().isEmpty()) {
            etLoginPassword.setError("Required password");
            requestFocus(etLoginPassword);
            return false;
        } else if (etLoginPassword.getText().toString().trim().length() < 6) {
            etLoginPassword.setError(getResources().getString(R.string.password_validity));
            requestFocus(etLoginPassword);
            return false;
        } else {
            etLoginPassword.setError(null);
        }

        return true;
    }

    private boolean validateMobile() {
        if (etLoginMobile.getText().toString().trim().isEmpty() || etLoginMobile.getText().toString().trim().length() < 10) {
            etLoginMobile.setError("Enter 10 digit no");
            requestFocus(etLoginMobile);
            return false;
        } else {
            etLoginMobile.setError(null);
        }

        return true;
    }


    private void submitForm() {

        if (!validateMobile()) {
            return;
        } else if (!validatePassword()) {
            return;
        } else {
           loadDlg.show();
            checkPermission();
//
//

        }

    }


    private void requestFocus(View view) {
        if (view.requestFocus()) {
            LoginFragment.this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(loadDlg != null){

            loadDlg.dismiss();
            loadDlg = null;
        }


    }

    //
// private class MyTextWatcher implements TextWatcher {
//
//        private View view;
//
//        private MyTextWatcher(View view) {
//            this.view = view;
//        }
//
//        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//        }
//
//        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//        }
//
//        public void afterTextChanged(Editable editable) {
//            int i = view.getId();
//            if (i == R.id.etLoginMobile) {
//                validateMobile();
//            } else if (i == R.id.etLoginPassword) {
//                if (editable.length() < LoginFragment.this.getResources().getInteger(R.integer.passwordLength)) {
//                    btnLogin.setVisibility(View.VISIBLE);
//                    validatePassword();
//                } else {
//                    submitForm();
//                }
//            }
//        }
//
//    }

    public void attemptLoginNew() {
        Utility.hideKeyboard(LoginFragment.this);
        jsonRequest = new JSONObject();
        String encryptValue = "";

        try {
            if (Mpin) {
                jsonRequest.put("mPin", DialogVerifyPin.getText().toString());
                jsonRequest.put("hasDeviceId", Hasdeviceid);
                jsonRequest.put("ipAddress", "");
                if (SecurityUtil.getIMEI(LoginFragment.this) != null) {
                    jsonRequest.put("androidDeviceID", SecurityUtil.getAndroidId(LoginFragment.this));
//                    jsonRequest.put("androidDeviceID", SecurityUtil.getAndroidId(getActivity()) + "-" + SecurityUtil.getIMEI(getActivity()));
                } else {
                    jsonRequest.put("androidDeviceID", SecurityUtil.getAndroidId(LoginFragment.this));
                }
            } else {
                jsonRequest.put("username", etLoginMobile.getText().toString());

                String basicAuth = new String(Base64.encode(etLoginPassword.getText().toString().getBytes(), Base64.NO_WRAP));
                jsonRequest.put("password", basicAuth);
                regId = getRegistrationId();
                if (!regId.isEmpty()) {
//                    Log.i("registrationId", regId);
                    jsonRequest.put("registrationId", regId);
                }
                if (SecurityUtil.getIMEI(LoginFragment.this) != null) {
                    jsonRequest.put("ipAddress", SecurityUtil.getAndroidId(LoginFragment.this) + "-" + SecurityUtil.getIMEI(LoginFragment.this));
                } else {
                    jsonRequest.put("ipAddress", SecurityUtil.getAndroidId(LoginFragment.this));
                }
            }
//

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {

            Log.i("URL LOGIN", ApiUrl.URL_LOGIN);
            Log.i("LoginRequest", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_LOGIN, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.i("LoginResponse", response.toString());
                        String code = response.getString("code");

                        if (code != null && code.equals("S00")) {

                            //New Implementation
                            JSONObject jsonDetail = response.getJSONObject("details");

                            String userSessionId = jsonDetail.getString("sessionId");
                            JSONObject accDetail = jsonDetail.getJSONObject("accountDetail");
                            boolean isImpsFixed = jsonDetail.getBoolean("isImpsFixed");
                            String impsCommissionAmt = jsonDetail.getString("impsCommissionAmt");

                            double userBalanceD = accDetail.getDouble("balance");
                            String userBalance = String.valueOf(new DecimalFormat("#.##").format(userBalanceD));

                            long userAcNoL = accDetail.getLong("accountNumber");
                            String userAcNo = String.valueOf(userAcNoL);

                            int userPointsI = accDetail.getInt("points");
                            String userPoints = String.valueOf(userPointsI);

                            JSONObject accType = accDetail.getJSONObject("accountType");
                            String accName = accType.getString("name");
                            String accCode = accType.getString("code");


                            double accBalanceLimitD = accType.getDouble("balanceLimit");
                            double accMonthlyLimitD = accType.getDouble("monthlyLimit");
                            double accDailyLimitD = accType.getDouble("dailyLimit");

                            String accBalanceLimit = String.valueOf(accBalanceLimitD);
                            String accMonthlyLimit = String.valueOf(accMonthlyLimitD);
                            String accDailyLimit = String.valueOf(accDailyLimitD);

                            JSONObject jsonUserDetail = jsonDetail.getJSONObject("userDetail");


                            MainActivity.group = response.getString("groupName");
                            String userFName = jsonUserDetail.getString("firstName");
                            String userLName = jsonUserDetail.getString("lastName");
                            String userMobile = jsonUserDetail.getString("contactNo");
                            String userEmail = jsonUserDetail.getString("email");
                            String userEmailStatus = jsonUserDetail.getString("emailStatus");
                            String userAddress = jsonUserDetail.getString("locality");
                            String userImage = jsonUserDetail.getString("image");
                            String encodedImage = response.getString("encodedImage");

                            if (!response.isNull("cardDetails")) {
                                JSONObject cardDetails = response.getJSONObject("cardDetails");

                                cardNo = cardDetails.getString("walletNumber");
                                cvv = cardDetails.getString("cvv");
                                expiryDate = cardDetails.getString("expiryDate");
                                cardHolder = cardDetails.getString("holderName");
                            } else {
                                cardNo = "XXXXXXXXXXXXXXXX";
                                cvv = "XXX";
                                expiryDate = "XXXX-XX";
                                cardHolder = "XXXXXXXXXXXXXX";

                            }


                            String images = "";
                            if (!encodedImage.equals("")) {
                                images = encodedImage;
                            } else {
                                images = userImage;
                            }


//                            boolean isMPin = jsonUserDetail.getBoolean("mpinPresent");
                            String userDob = jsonUserDetail.getString("dateOfBirth");
                            boolean hasRefer = response.getBoolean("hasRefer");
                            boolean hasvertualcard = response.getBoolean("hasVirtualCard");
                            boolean hasphysicalCard = response.getBoolean("hasPhysicalCard");
                            String physicalCardStatus = response.getString("physicalCardStatus");
                            boolean hasPhysicalRequest = response.getBoolean("hasPhysicalRequest");
                            String cardMinBalance = response.getString("minimumCardBalance");
                            String cardFees = response.getString("cardFees");
                            String cardBaseFare = response.getString("cardBaseFare");
                            boolean haskycrequest = response.getBoolean("kycRequest");
                            String loadMoneyComm = response.getString("loadMoneyComm");

                            boolean PCStatus = response.getBoolean("physicalIsBlock");
                            boolean VCStatus = response.getBoolean("virtualIsBlock");
                            String virtualBlock = response.getString("virtualCardBlock");
                            String physicalBlock = response.getString("physicalCardBlock");

                            boolean isCorporateUser = response.getBoolean("corporateUser");
                            Log.v("SSS","SSS checkk "+isCorporateUser);

                            JSONObject loyaltyJSONObject = response.getJSONObject("clavexDetails");

                            SharedPreferences sharedPreferences1 = getSharedPreferences("CorporateUser",Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor1 = sharedPreferences1.edit();
                            editor1.putBoolean("isCorporateUser", isCorporateUser).commit();

                            if (loyaltyJSONObject.has("cardNo")
                            && loyaltyJSONObject.has("expiryDate")
                                    && loyaltyJSONObject.has("pin")) {

                                loyaltyPointModel.setCardNo(loyaltyJSONObject.getString("cardNo"));
                                loyaltyPointModel.setExpiryDate(loyaltyJSONObject.getString("expiryDate"));
                                loyaltyPointModel.setPin(loyaltyJSONObject.getString("pin"));
                                loyaltyPointModel.setHasClavax(true);

                                SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("EWIRE",Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString("cardNo", loyaltyJSONObject.getString("cardNo"));
                                editor.putString("expiryDate", loyaltyJSONObject.getString("expiryDate"));
                                editor.putString("pin", loyaltyJSONObject.getString("pin"));
                                editor.putBoolean("hasClavax",true);
                                editor.commit();
/*
                                SharedPreferences.Editor prefEditor = loyaltysharedPreferences.edit();
                                Gson gson = new Gson();
                                String json = gson.toJson(loyaltyJSONObject);
                                prefEditor.putString("Loyalty",json);
                                prefEditor.commit();*/

                            }else{
                                loyaltyPointModel.setHasClavax(false);
                                SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("EWIRE",Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putBoolean("hasClavax",false);
                                editor.commit();

                            }
                            //in sab data ko getuserdetails me pass kara.
                            //getuserdatails(sesionId,firstname,snds,dwsd,sds,d);


//
//                            String userGender = "";
//                            if (!jsonUserDetail.isNull("gender")) {
//                                userGender = jsonUserDetail.getString("gender");
//                            }
                            UserModel.deleteAll(UserModel.class);
                            try {
                                //Encrypting sessionId
                                UserModel userModel = new UserModel(userSessionId, userFName, userLName, userMobile, userEmail, userEmailStatus, 1, userAcNo, accName, accCode, userBalance, accBalanceLimit, accDailyLimit, accMonthlyLimit, userAddress, encodedImage, userDob, "", false, userPoints, hasRefer, hasvertualcard, hasphysicalCard, cardNo, cvv, expiryDate, cardHolder, hasPhysicalRequest, physicalCardStatus, cardMinBalance, cardFees, haskycrequest, loadMoneyComm, cardBaseFare, PCStatus, VCStatus, virtualBlock, physicalBlock,impsCommissionAmt,isImpsFixed);
                                userModel.save();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            session.setUserSessionId(userSessionId);
                            session.setUserFirstName(userFName);
                            session.setUserLastName(userLName);
                            session.setUserMobileNo(userMobile);
                            session.setUserEmail(userEmail);
                            session.setEmailIsActive(userEmailStatus);
                            session.setUserAcCode(accCode);
                            session.setUserAcNo(userAcNo);
                            session.setUserBalance(userBalance);
                            session.setUserAcName(accName);
                            session.setUserMonthlyLimit(accMonthlyLimit);
                            session.setUserDailyLimit(accDailyLimit);
                            session.setUserBalanceLimit(accBalanceLimit);
                            session.setUserImage(images);
                            session.setUserAddress(userAddress);
                            session.setUsercardNo(cardNo);
                            session.setUsercvv(cvv);
                            session.setUserexpiryDate(expiryDate);
                            session.setUsercardHolder(cardHolder);
                            session.setUserminimumCardBalance(cardMinBalance);
                            session.setUsercardFees(cardFees);
                            session.setUsercardBaseFare(cardBaseFare);
//                            session.setUserGender(userGender);
                            session.setUserDob(userDob);
//                            session.setMPin(isMPin);
                            session.setHasRefer(hasRefer);
                            session.setHasVcard(hasvertualcard);
                            session.setHasPcard(hasphysicalCard);
                            session.setHasPhysicalRequest(hasPhysicalRequest);
                            session.setPhysicalCardStatus(physicalCardStatus);
                            session.setIsValid(1);
                            session.setIsImpsFixed(isImpsFixed);
                            session.setImpsCommissionAmt(impsCommissionAmt);
                            session.setUserPoints(userPoints);
                            session.setHaskycRequest(haskycrequest);
                            session.setloadMoneyComm(loadMoneyComm);
                            session.setPCStatus(PCStatus);
                            session.setVCStatus(VCStatus);
                            session.setUserVirtualBlock(virtualBlock);
                            session.setUserPhysicalBlock(physicalBlock);
                            SharedPreferences.Editor editor = phonePreferences.edit();
                            editor.clear();
                            editor.putString("phone", userMobile);
                            editor.apply();

                            SharedPreferences.Editor musiceditor = musicPreferences.edit();
                            musiceditor.clear();
                            musiceditor.putBoolean("ShowMusicDailog", true);
                            musiceditor.apply();

                            loadDlg.dismiss();
//
//                            try {
//                                ZohoSalesIQ.registerVisitor(session.getUserMobileNo());
//                                ZohoSalesIQ.Visitor.setEmail(session.getUserEmail());
//                                ZohoSalesIQ.Visitor.setContactNumber(session.getUserMobileNo());
//                                ZohoSalesIQ.Visitor.setName(session.getUserFirstName());
//                            } catch (InvalidVisitorIDException e) {
//                                e.printStackTrace();
//                            }


//                            startActivity((new Intent(getActivity(),ContactReadActivity.class)));
//                            getActivity().finish();
//                            Intent i = new Intent(getActivity(), ContactReadActivity.class);
//                            startActivity(i);
//                            getUserDetail();
                            startActivity(new Intent(LoginFragment.this, MainActivity.class));
//                            startActivity(new Intent(LoginFragment.this, PhysicalReciptActivity.class));
                            overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
                            finish();
                        } else if (code != null && code.equals("L01")) {
                            loadDlg.dismiss();

                            Intent otpIntent = new Intent(LoginFragment.this, OtpVerificationMidLayerActivity.class);
                            otpIntent.putExtra("userMobileNo", etLoginMobile.getText().toString());
                            otpIntent.putExtra("intentType", "Device");
                            otpIntent.putExtra("devPassword", etLoginPassword.getText().toString());
                            otpIntent.putExtra("devRegId", regId);
                            startActivityForResult(otpIntent, 0);
                            finish();
//                            new VerifyLoginOTPDialog(getActivity(), LoginFragment.this, etLoginMobile.getText().toString(), etLoginPassword.getText().toString(), regId);
                        } else {
                            String message = response.getString("message");
                            loadDlg.dismiss();
                            CustomToast.showMessage(LoginFragment.this, message);

                        }
                    } catch (JSONException e) {
                        Log.v("SSS","SSS eee"+e.getMessage());
                        loadDlg.dismiss();
                        CustomToast.showMessage(LoginFragment.this, getResources().getString(R.string.server_exception2));
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    CustomToast.showMessage(LoginFragment.this, NetworkErrorHandler.getMessage(error, LoginFragment.this));
                    error.printStackTrace();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
//                    Log.i("Header", map.get("hash"));
                    return map;
                }

            };

            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }

    }

    private void promoteForgetPwd() {
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("username", etLoginMobile.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_FORGET_PWD, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        String message = response.getString("message");
                        String code = response.getString("code");
                        if (code != null && code.equals("S00")) {
//                            Intent autoIntent = new Intent(getActivity(), ChangePwdActivity.class);
//                            autoIntent.putExtra("OtpCode","");
//                            autoIntent.putExtra("userMobileNo",etLoginMobile.getText().toString());


                            Intent verifyIntent = new Intent(LoginFragment.this, OtpVerificationActivity.class);
                            verifyIntent.putExtra("userMobileNo", etLoginMobile.getText().toString());
                            verifyIntent.putExtra("OtpCode", "");

                            loadDlg.dismiss();
//                            startActivity(autoIntent);
                            startActivity(verifyIntent);

                        } else {
                            loadDlg.dismiss();
                            Toast.makeText(LoginFragment.this, message, Toast.LENGTH_SHORT).show();

                        }
                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(LoginFragment.this, getResources().getString(R.string.server_exception2));

                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    error.printStackTrace();
                    CustomToast.showMessage(LoginFragment.this, NetworkErrorHandler.getMessage(error, LoginFragment.this));

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    private String getRegistrationId() {
        final SharedPreferences prefs = getGCMPreferences();
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            return "";
        }
        return registrationId;
    }

    private SharedPreferences getGCMPreferences() {
        return LoginFragment.this.getSharedPreferences("FIREBASE",
                Context.MODE_PRIVATE);
    }


    private void promoteTest() {
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("auth_facebook", "true");
            jsonRequest.put("access_token", "qwekqjsaldkaSDLAUREQWDAMSD");
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, "http://swasthyanews.com/api/oauth", jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.i("Test Response", response.toString());

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    error.printStackTrace();
                    CustomToast.showMessage(LoginFragment.this, NetworkErrorHandler.getMessage(error, LoginFragment.this));

                }
            }) {

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    @Override
    public void onVerifySuccess() {
        SharedPreferences.Editor editor = phonePreferences.edit();
        editor.clear();
        editor.putString("phone", etLoginMobile.getText().toString());
        editor.commit();
        startActivity(new Intent(LoginFragment.this, MainActivity.class));
        finish();
    }

    @Override
    public void onVerifyError() {
//        CustomToast.showMessage(getActivity(),"Error occurred please try again");
    }

//    private void logUser() {
//        // TODO: Use the current user's information
//        // You can call any combination of these three methods
//        Crashlytics.setUserIdentifier(session.getUserMobileNo());
//        Crashlytics.setUserEmail(session.getUserEmail());
//        Crashlytics.setUserName(session.getUserFirstName() + " " + session.getUserLastName());
//    }

    public static class AppPermission {

        public static boolean isMarshmallowPlusDevice() {
            return Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1;
        }

        @TargetApi(Build.VERSION_CODES.M)
        public static boolean isPermissionRequestRequired(Activity activity, @NonNull String[] permissions, int requestCode) {
            if (isMarshmallowPlusDevice() && permissions.length > 0) {
                List<String> newPermissionList = new ArrayList<>();
                for (String permission : permissions) {
                    if (PackageManager.PERMISSION_GRANTED != activity.checkSelfPermission(permission)) {
                        newPermissionList.add(permission);
                    }
                }
                if (newPermissionList.size() > 0) {
                    activity.requestPermissions(newPermissionList.toArray(new String[newPermissionList.size()]), requestCode);
                    return true;
                }
            }
            return false;
        }
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(LoginFragment.this, android.Manifest.permission.READ_PHONE_STATE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            requestPermission();
            return true;

        } else {
            requestPermission();
            return false;

        }
    }

    private void requestPermission() {


        ActivityCompat.requestPermissions(LoginFragment.this, new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_PHONE_STATE,  android.Manifest.permission.WAKE_LOCK, android.Manifest.permission.CALL_PHONE}, PERMISSION_REQUEST_CODE);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED && grantResults[3] == PackageManager.PERMISSION_GRANTED && grantResults[4] == PackageManager.PERMISSION_GRANTED) {
                    attemptLoginNew();

                } else {
                    loadDlg.dismiss();

                }
                break;
        }
    }

//    @TargetApi(Build.VERSION_CODES.M)
//    protected void generateKey() {
//        try {
//            keyStore = KeyStore.getInstance("AndroidKeyStore");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//
//        KeyGenerator keyGenerator;
//        try {
//            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");
//        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
//            throw new RuntimeException("Failed to get KeyGenerator instance", e);
//        }
//
//
//        try {
//            keyStore.load(null);
//            keyGenerator.init(new
//                    KeyGenParameterSpec.Builder(KEY_NAME,
//                    KeyProperties.PURPOSE_ENCRYPT |
//                            KeyProperties.PURPOSE_DECRYPT)
//                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
//                    .setUserAuthenticationRequired(true)
//                    .setEncryptionPaddings(
//                            KeyProperties.ENCRYPTION_PADDING_PKCS7)
//                    .build());
//            keyGenerator.generateKey();
//        } catch (NoSuchAlgorithmException |
//                InvalidAlgorithmParameterException
//                | CertificateException | IOException e) {
//            throw new RuntimeException(e);
//        }
//    }
//
//    @TargetApi(Build.VERSION_CODES.M)
//    public boolean cipherInit() {
//        try {
//            cipher = Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES + "/" + KeyProperties.BLOCK_MODE_CBC + "/" + KeyProperties.ENCRYPTION_PADDING_PKCS7);
//        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
//            throw new RuntimeException("Failed to get Cipher", e);
//        }
//
//
//        try {
//            keyStore.load(null);
//            SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME,
//                    null);
//            cipher.init(Cipher.ENCRYPT_MODE, key);
//            return true;
//        } catch (KeyPermanentlyInvalidatedException e) {
//            return false;
//        } catch (KeyStoreException | CertificateException | UnrecoverableKeyException | IOException | NoSuchAlgorithmException | InvalidKeyException e) {
//            throw new RuntimeException("Failed to init Cipher", e);
//        }
//
//
//    }



}