package in.MadMoveGlobal.fragment.fragmentuser;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.Html;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.crashlytics.android.Crashlytics;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.EwireRuPay.activity.MainActivity;
import in.MadMoveGlobal.EwireRuPay.activity.MainMenuDetailActivity;
import in.MadMoveGlobal.EwireRuPay.activity.ReciptFingoolActivity;
import in.MadMoveGlobal.adapter.MemberAdapter;
import in.MadMoveGlobal.custom.CustomAlertDialog;
import in.MadMoveGlobal.custom.CustomSuccessDialog;
import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.fragment.fragmentnaviagtionitems.FingoolTandCFragment;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.metadata.AppMetadata;
import in.MadMoveGlobal.model.MemberModel;
import in.MadMoveGlobal.model.UserModel;
import io.fabric.sdk.android.Fabric;

//import in.payqwik.util.EncryptDecryptUserUtility;


/**
 * Created by Ksf on 3/27/2016.
 */
public class FingoolFragment extends Fragment {
    private View rootView;
    private ImageView ivGroup, ivAmount, ivMemberDetail;
    private Button btnTen_Lakh, btnFive_Lakh, btnTwo_Lakh, btnAddMember, btnPayByWallet, btnTnC;
    private TextView tvTotalPerson, tvTotalAmount, tvHistory;
    private EditText etFull_Name, etPhone_Number, etNumDays, etAmount, etFromDate, etToDate;
    private LoadingDialog loadDlg;
    private JSONObject jsonRequest;
    ListView listView;
    ArrayList<MemberModel> memberModelList;
    private UserModel session = UserModel.getInstance();
    private JsonObjectRequest postReq;
    private String tag_json_obj = "json_user";
    private String batchNo, batchNo2, batchNo5, batchNo10, amount, batchNumber, amountBatch, listString;
    private int i, i2, i5, i10, j = 0, k = 0;
    private Boolean isFirstTime = true, isTwo = false, isFive = false, isTen = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Fabric.with(getActivity(), new Crashlytics());
        rootView = inflater.inflate(R.layout.fragment_fingool, container, false);
        loadDlg = new LoadingDialog(getActivity());
        ivGroup = (ImageView) rootView.findViewById(R.id.ivGroup);
        ivAmount = (ImageView) rootView.findViewById(R.id.ivAmount);
        ivMemberDetail = (ImageView) rootView.findViewById(R.id.ivMemberDetail);

        listView = rootView.findViewById(R.id.lvMemberList);
        btnTen_Lakh = (Button) rootView.findViewById(R.id.btnTen_Lakh);
        btnFive_Lakh = (Button) rootView.findViewById(R.id.btnFive_Lakh);
        btnTwo_Lakh = (Button) rootView.findViewById(R.id.btnTwo_Lakh);
        btnAddMember = (Button) rootView.findViewById(R.id.btnAddMember);
        btnPayByWallet = (Button) rootView.findViewById(R.id.btnPayByWallet);
        btnTnC = (Button) rootView.findViewById(R.id.btnTnC);
        tvTotalPerson = (TextView) rootView.findViewById(R.id.tvTotalPerson);
        tvTotalAmount = (TextView) rootView.findViewById(R.id.tvTotalAmount);
        tvHistory = (TextView) rootView.findViewById(R.id.tvHistory);

        memberModelList = new ArrayList<>();

        etFull_Name = (EditText) rootView.findViewById(R.id.etFull_Name);
        etPhone_Number = (EditText) rootView.findViewById(R.id.etPhone_Number);
        etNumDays = (EditText) rootView.findViewById(R.id.etNumDays);
        etAmount = (EditText) rootView.findViewById(R.id.etAmount);
        etFromDate = (EditText) rootView.findViewById(R.id.etFromDate);
        etToDate = (EditText) rootView.findViewById(R.id.etToDate);


        Picasso.with(getActivity()).load("fsdfs").placeholder(R.drawable.group).into(ivGroup);
        Picasso.with(getActivity()).load("fsdfs").placeholder(R.drawable.ac_ammount).into(ivAmount);
        Picasso.with(getActivity()).load("fsdfs").placeholder(R.drawable.persion).into(ivMemberDetail);

        serviceList();
        etNumDays.addTextChangedListener(new MyTextWatcher(etNumDays));

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        Date tomorrow = calendar.getTime();
        DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
        String tomorrowAsString = dateFormat.format(tomorrow);
        etFromDate.setText(tomorrowAsString);

        tvHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), ReciptFingoolActivity.class);
                startActivity(intent);

//                paymentProcess();
            }
        });

        btnPayByWallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Integer[] temp = new Integer[memberModelList.size()];
//                listString = Arrays.toString(memberModelList.toArray(temp));

                paymentProcess();
            }
        });


        btnTnC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                FingoolTandCFragment fragment = new FingoolTandCFragment();
//                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
//                Bundle bType = new Bundle();
//                bType.putString("URL", "https://liveewire.com/Fingoole");
//                fragment.setArguments(bType);
//                fragmentTransaction.replace(R.id.frameInCart, fragment);
//                fragmentTransaction.commit();

                Intent menuIntent = new Intent(getActivity(), MainMenuDetailActivity.class);
                menuIntent.putExtra(AppMetadata.FRAGMENT_TYPE, "PremiumInsurace");
                getActivity().startActivity(menuIntent);

            }
        });

        btnTwo_Lakh.setOnClickListener(new View.OnClickListener() {

            @SuppressLint({"NewApi", "ResourceAsColor"})
            @Override
            public void onClick(View view) {

                if (isFirstTime) {

                    j = 0;
                    k = 0;
                    etAmount.setText("");
                    etNumDays.setText("");
                    etFull_Name.setText("");
                    etPhone_Number.setText("");
                    etToDate.setText("");
                    tvTotalPerson.setText(getResources().getString(R.string.Total_Person) + j);
                    tvTotalAmount.setText(getResources().getString(R.string.Total_Amounts) + k);
                    btnPayByWallet.setVisibility(View.GONE);
                    btnTwo_Lakh.setBackgroundColor(Color.parseColor("#3ac6f0"));
                    btnFive_Lakh.setBackgroundColor(Color.parseColor("#ffffff"));
                    btnTen_Lakh.setBackgroundColor(Color.parseColor("#ffffff"));
                    batchNo = batchNo2;
                    i = i2;
                    memberModelList.clear();
                    isTwo = true;

                    selectAmount();
                } else {

                    showCustomDialogTwo();
                }


            }
        });
        btnFive_Lakh.setOnClickListener(new View.OnClickListener() {
            @SuppressLint({"NewApi", "ResourceAsColor"})
            @Override
            public void onClick(View view) {

                if (isFirstTime) {

                    j = 0;
                    k = 0;
                    etAmount.setText("");
                    etNumDays.setText("");
                    etFull_Name.setText("");
                    etPhone_Number.setText("");
                    etToDate.setText("");
                    tvTotalPerson.setText(getResources().getString(R.string.Total_Person) + j);
                    tvTotalAmount.setText(getResources().getString(R.string.Total_Amounts) + k);
                    btnTwo_Lakh.setBackgroundColor(Color.parseColor("#ffffff"));
                    btnFive_Lakh.setBackgroundColor(Color.parseColor("#3ac6f0"));
                    btnTen_Lakh.setBackgroundColor(Color.parseColor("#ffffff"));
                    btnPayByWallet.setVisibility(View.GONE);
                    batchNo = batchNo5;
                    i = i5;
                    memberModelList.clear();
                    isTwo = true;
                    selectAmount();

                } else {

                    showCustomDialogFive();
                }

            }
        });
        btnTen_Lakh.setOnClickListener(new View.OnClickListener() {
            @SuppressLint({"NewApi", "ResourceAsColor"})
            @Override
            public void onClick(View view) {

                if (isFirstTime) {
                    j = 0;
                    k = 0;
                    etAmount.setText("");
                    etNumDays.setText("");
                    etFull_Name.setText("");
                    etPhone_Number.setText("");
                    etToDate.setText("");
                    tvTotalPerson.setText(getResources().getString(R.string.Total_Person) + j);
                    tvTotalAmount.setText(getResources().getString(R.string.Total_Amounts) + k);
                    btnTwo_Lakh.setBackgroundColor(Color.parseColor("#ffffff"));
                    btnFive_Lakh.setBackgroundColor(Color.parseColor("#ffffff"));
                    btnTen_Lakh.setBackgroundColor(Color.parseColor("#3ac6f0"));
                    btnPayByWallet.setVisibility(View.GONE);
                    batchNo = batchNo10;
                    i = i10;
                    memberModelList.clear();
                    isTwo = true;
                    selectAmount();
                } else {

                    showCustomDialogTen();
                }

            }
        });

        btnAddMember.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View view) {


                if (isTwo || isFive || isTen) {
                    if (j < 7)

                    {
                        submitForm();
                    } else

                    {
                        CustomToast.showMessage(getActivity(), getResources().getString(R.string.You_can_add_maximum_six_members));

                    }
                } else
                    CustomToast.showMessage(getActivity(), getResources().getString(R.string.Please_select_premium_plan));


            }
        });
        return rootView;
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    private void submitForm() {

        if (!validateFullName()) {
            return;
        }
        if (!validateMobile()) {
            return;
        }
        if (!validateDay()) {

            return;
        }
        if (!validaAmount()) {
            return;
        }
        if (!validateFromDay()) {
            return;
        }
        if (!validateToDay()) {
            return;
        }

        if (memberModelList.size() == 0 || memberModelList.isEmpty()) {
            fingoolRegister();
        } else {

            for (int j = 1; j <= memberModelList.size(); j++) {

                if (etFull_Name.getText().toString().equals(memberModelList.get(j - 1).getMemberName())) {

                    CustomToast.showMessage(getActivity(), getResources().getString(R.string.Enter_name_already_exists_Enter_new_name));

                } else {
                    fingoolRegister();
                }

            }
        }
    }


    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }


    private boolean validateFullName() {
        if (etFull_Name.getText().toString().trim().isEmpty()) {
            etFull_Name.setError(getResources().getString(R.string.Enter_full_name));
            requestFocus(etFull_Name);
            return false;
        } else {
            etFull_Name.setError(null);
        }

        return true;
    }

    private boolean validateMobile() {
        if (etPhone_Number.getText().toString().trim().isEmpty() || etPhone_Number.getText().toString().trim().length() < 10) {
            etPhone_Number.setError(getResources().getString(R.string.Enter_your_ten_digit_monbile_number));
            requestFocus(etPhone_Number);
            return false;
        } else {
            etPhone_Number.setError(null);
        }

        return true;
    }

    private boolean validateDay() {

        if (etNumDays.getText().toString().isEmpty()) {
            etNumDays.setText("0");


        }
        if (etNumDays.getText().toString().trim().isEmpty() || Integer.valueOf(etNumDays.getText().toString()) > 365 || Integer.valueOf(etNumDays.getText().toString()) < 1) {
            etNumDays.setError(getResources().getString(R.string.Enter_number_of_days));
            requestFocus(etNumDays);
            return false;
        } else {
            etNumDays.setError(null);
        }

        return true;
    }

    private boolean validateFromDay() {
        if (etFromDate.getText().toString().trim().isEmpty()) {
            etFromDate.setError(getResources().getString(R.string.Select_Insurance_starting_date));
            requestFocus(etFromDate);
            return false;
        } else {
            etFromDate.setError(null);
        }

        return true;
    }

    private boolean validateToDay() {
        if (etToDate.getText().toString().trim().isEmpty()) {
            etToDate.setError(getResources().getString(R.string.Select_Insurance_ending_date));
            requestFocus(etToDate);
            return false;
        } else {
            etToDate.setError(null);
        }

        return true;
    }

    private boolean validaAmount() {


        if (etAmount.getText().toString().trim().isEmpty()) {
            etAmount.setError(getResources().getString(R.string.Enter_amount));
            requestFocus(etAmount);
            return false;
        } else {
            etAmount.setError(null);
        }

        return true;
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }


    public void serviceList() {
        loadDlg.show();

        jsonRequest = new JSONObject();
        try {


            jsonRequest.put("sessionId", session.getUserSessionId());


        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            Log.i("Card Url", ApiUrl.URL_FINGOOL_SERVICES);
            postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_FINGOOL_SERVICES, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject JsonObj) {
                    try {

                        Log.i("Card Response", JsonObj.toString());
                        String message = JsonObj.getString("message");
                        String code = JsonObj.getString("code");

                        if (code != null && code.equals("S00")) {
                            loadDlg.dismiss();


                            String servicList = JsonObj.getString("serviceList");
                            JSONObject servicListObject = new JSONObject(servicList);
                            JSONArray details = servicListObject.getJSONArray("details");
                            for (int i = 0; i < details.length(); i++) {
                                JSONObject c = details.getJSONObject(i);

                                String insCode = c.getString("code");
                                if (insCode.equalsIgnoreCase("ODPA2LIN")) {

                                    btnTwo_Lakh.setText("₹ 2 lakh");
                                    batchNo2 = "ODPA2LIN";
                                    i2 = 2;

                                }
                                if (insCode.equalsIgnoreCase("ODPA5LIN")) {
                                    btnFive_Lakh.setText("₹ 5 lakh");
                                    batchNo5 = "ODPA5LIN";
                                    i5 = 5;
                                }
                                if (insCode.equalsIgnoreCase("ODPA10LIN")) {
                                    btnTen_Lakh.setText("₹ 10 lakh");
                                    batchNo10 = "ODPA10LIN";
                                    i10 = 10;
                                }
                            }

                        } else if (code != null && code.equals("F03")) {
                            loadDlg.dismiss();
                            CustomToast.showMessage(getActivity(), getResources().getString(R.string.Please_login_and_try_again));
                            //Toast.makeText(getActivity(), "Your session is expire, please login", Toast.LENGTH_SHORT).show();
                            sendLogout();

                        } else {
                            loadDlg.dismiss();
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        loadDlg.dismiss();
                        CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    loadDlg.dismiss();
                    CustomToast.showMessage(getActivity(), getResources().getString(R.string.Network_connection_error));
                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };

            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }

    }


    public void selectAmount() {
        loadDlg.show();

        jsonRequest = new JSONObject();
        try {


            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("batchNumber", batchNo);


        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            Log.i("Card Url", ApiUrl.URL_FINGOOL_AMOUNT);
            postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_FINGOOL_AMOUNT, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject JsonObj) {
                    try {

                        Log.i("Card Response", JsonObj.toString());
                        String message = JsonObj.getString("message");
                        String code = JsonObj.getString("code");

                        if (code != null && code.equals("S00")) {
                            loadDlg.dismiss();

                            amount = JsonObj.getString("amount");
                            amountBatch = JsonObj.getString("batch");
                            Log.i("AMOUNT", amount);
                            etPhone_Number.setFocusable(true);

                        } else if (code != null && code.equals("F03")) {
                            loadDlg.dismiss();
                            CustomToast.showMessage(getActivity(), getResources().getString(R.string.Please_login_and_try_again));
                            //Toast.makeText(getActivity(), "Your session is expire, please login", Toast.LENGTH_SHORT).show();
                            sendLogout();

                        } else {
                            loadDlg.dismiss();
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        loadDlg.dismiss();
                        CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    loadDlg.dismiss();
                    CustomToast.showMessage(getActivity(), getResources().getString(R.string.Network_connection_error));
                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };

            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }

    }


    public void fingoolRegister() {
        loadDlg.show();

        jsonRequest = new JSONObject();
        try {


            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("name", etFull_Name.getText().toString());
            jsonRequest.put("mobileNumber", etPhone_Number.getText().toString());
            jsonRequest.put("noOfDays", etNumDays.getText().toString());
            jsonRequest.put("serviceProvider", batchNo);
            jsonRequest.put("batchNumber", amountBatch);
            jsonRequest.put("amount", etAmount.getText().toString());


        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequests", jsonRequest.toString());
            Log.i("Card Url", ApiUrl.URL_FINGOOL_REGISTER);
            postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_FINGOOL_REGISTER, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject JsonObj) {
                    try {

                        Log.i("Card Response", JsonObj.toString());
                        String message = JsonObj.getString("message");
                        String code = JsonObj.getString("code");

                        if (code != null && code.equals("S00")) {
                            loadDlg.dismiss();
                            MemberModel memberModel = new MemberModel();
                            memberModel.setMemberName(etFull_Name.getText().toString());
                            memberModel.setMobile(etPhone_Number.getText().toString());
                            memberModel.setNoOfDays(etNumDays.getText().toString());
                            memberModel.setAmount(etAmount.getText().toString());
                            memberModel.setServiceProvider(amountBatch);

                            memberModelList.add(memberModel);

                            MemberAdapter memberAdapter = new MemberAdapter(getContext(), R.layout.member_adapter, memberModelList);
                            listView.setAdapter(memberAdapter);
                            batchNumber = JsonObj.getString("batch");
                            Log.i("BATCHNUMBER", batchNumber);
                            CustomToast.showMessage(getActivity(), message);
                            j = j + 1;
                            k = k + Integer.parseInt(etAmount.getText().toString());
                            Log.i("TOTALAMOUNTS", String.valueOf(k));
                            tvTotalPerson.setText(getResources().getString(R.string.Total_Person)+ j);
                            tvTotalAmount.setText(getResources().getString(R.string.Total_Amounts)+ k);
                            btnPayByWallet.setVisibility(View.VISIBLE);
                            isFirstTime = false;

                        } else if (code != null && code.equals("F03")) {
                            loadDlg.dismiss();
                            CustomToast.showMessage(getActivity(), getResources().getString(R.string.Please_login_and_try_again));
                            //Toast.makeText(getActivity(), "Your session is expire, please login", Toast.LENGTH_SHORT).show();
                            sendLogout();

                        } else {
                            loadDlg.dismiss();
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        loadDlg.dismiss();
                        CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    loadDlg.dismiss();
                    CustomToast.showMessage(getActivity(), getResources().getString(R.string.Network_connection_error));
                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };

            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }

    }

    public void paymentProcess() {
        loadDlg.show();

        jsonRequest = new JSONObject();
        try {


            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("batchNumber", amountBatch);

            JSONArray jsonArray = new JSONArray();
            for (MemberModel memberModel : memberModelList) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("name", memberModel.getMemberName());
                jsonObject.put("mobileNumber", memberModel.getMobile());
                jsonObject.put("noOfDays", memberModel.getNoOfDays());
                jsonObject.put("serviceProvider", memberModel.getServiceProvider());
                jsonObject.put("amount", memberModel.getAmount());
                jsonArray.put(jsonObject);
            }
            jsonRequest.put("registrationData", jsonArray);


        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            Log.i("Card Url", ApiUrl.URL_FINGOOL_PROCESS);
            postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_FINGOOL_PROCESS, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject JsonObj) {
                    try {

                        Log.i("Card Response", JsonObj.toString());
                        String message = JsonObj.getString("message");
                        String code = JsonObj.getString("code");

                        if (code != null && code.equals("S00")) {
                            loadDlg.dismiss();

                            successDialogss(message);

                        } else if (code != null && code.equals("F03")) {
                            loadDlg.dismiss();
                            CustomToast.showMessage(getActivity(), getResources().getString(R.string.Please_login_and_try_again));
                            //Toast.makeText(getActivity(), "Your session is expire, please login", Toast.LENGTH_SHORT).show();
                            sendLogout();

                        } else {
                            loadDlg.dismiss();
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        loadDlg.dismiss();
                        CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    loadDlg.dismiss();
                    CustomToast.showMessage(getActivity(), getResources().getString(R.string.Network_connection_error));
                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };

            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }

    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            int a = view.getId();
            if (a == R.id.etNumDays) {
                if (etNumDays.getText().toString().trim().isEmpty() || Integer.valueOf(etNumDays.getText().toString()) > 365 || Integer.valueOf(etNumDays.getText().toString()) < 1) {
                    etNumDays.setError(getResources().getString(R.string.Number_OF_DAYS));
                } else {

                    int numDays = Integer.parseInt(etNumDays.getText().toString());
                    String days = String.valueOf(numDays * i);
                    etAmount.setText(days);


                    Calendar calendar = Calendar.getInstance();
                    calendar.add(Calendar.DAY_OF_YEAR, numDays + 1);
                    Date tomorrow = calendar.getTime();
                    DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
                    String tomorrowAsString = dateFormat.format(tomorrow);
                    etToDate.setText(tomorrowAsString);


                }

            }
        }

    }

    public void showCustomDialogTwo() {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(generateVerifyMessageNoService(), Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(generateVerifyMessageNoService());
        }
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, result);
        builder.setPositiveButton(getResources().getString(R.string.agree), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                loadDlg.show();
                j = 0;
                k = 0;
                etAmount.setText("");
                etNumDays.setText("");
                etFull_Name.setText("");
                etPhone_Number.setText("");
                etToDate.setText("");
                tvTotalPerson.setText(getResources().getString(R.string.Total_Person) + j);
                tvTotalAmount.setText(getResources().getString(R.string.Total_Amounts)+ k);
                btnPayByWallet.setVisibility(View.GONE);
                btnTwo_Lakh.setBackgroundColor(Color.parseColor("#3ac6f0"));
                btnFive_Lakh.setBackgroundColor(Color.parseColor("#ffffff"));
                btnTen_Lakh.setBackgroundColor(Color.parseColor("#ffffff"));
                batchNo = batchNo2;
                i = i2;
                memberModelList.clear();
                isTwo = true;
                selectAmount();
            }
        });
        builder.setNegativeButton(getResources().getString(R.string.Dismiss), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public void showCustomDialogFive() {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(generateVerifyMessageNoService(), Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(generateVerifyMessageNoService());
        }
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, result);
        builder.setPositiveButton(getResources().getString(R.string.agree), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                loadDlg.show();
                j = 0;
                k = 0;
                etAmount.setText("");
                etNumDays.setText("");
                etFull_Name.setText("");
                etPhone_Number.setText("");
                etToDate.setText("");
                tvTotalPerson.setText(getResources().getString(R.string.Total_Person) + j);
                tvTotalAmount.setText(getResources().getString(R.string.Total_Amounts)+ k);
                btnTwo_Lakh.setBackgroundColor(Color.parseColor("#ffffff"));
                btnFive_Lakh.setBackgroundColor(Color.parseColor("#3ac6f0"));
                btnTen_Lakh.setBackgroundColor(Color.parseColor("#ffffff"));
                btnPayByWallet.setVisibility(View.GONE);
                batchNo = batchNo5;
                i = i5;
                memberModelList.clear();
                isFive = true;
                selectAmount();
            }
        });
        builder.setNegativeButton(getResources().getString(R.string.Dismiss), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.show();
    }


    public void showCustomDialogTen() {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(generateVerifyMessageNoService(), Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(generateVerifyMessageNoService());
        }
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, result);
        builder.setPositiveButton(getResources().getString(R.string.agree), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                loadDlg.show();
                j = 0;
                k = 0;
                etAmount.setText("");
                etNumDays.setText("");
                etFull_Name.setText("");
                etPhone_Number.setText("");
                etToDate.setText("");
                tvTotalPerson.setText(getResources().getString(R.string.Total_Person) + j);
                tvTotalAmount.setText(getResources().getString(R.string.Total_Amounts)+ k);
                btnTwo_Lakh.setBackgroundColor(Color.parseColor("#ffffff"));
                btnFive_Lakh.setBackgroundColor(Color.parseColor("#ffffff"));
                btnTen_Lakh.setBackgroundColor(Color.parseColor("#3ac6f0"));
                btnPayByWallet.setVisibility(View.GONE);
                batchNo = batchNo10;
                i = i10;
                isTen = true;
                memberModelList.clear();

                selectAmount();
            }
        });
        builder.setNegativeButton(getResources().getString(R.string.Dismiss), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public String generateVerifyMessageNoService() {
        String source = "<b><font color=#000000>"+getResources().getString(R.string.Dear_User_you_are_opting_to_change_the_plan_Saved_data_will_be_lost)+"</font></b>" +
                "<br><br><b><font color=#ff0000>"+getResources().getString(R.string.Do_you_wish_to_continue)+"</font></b><br></br>";
        return source;
    }

    public void successDialogss(String message) {
        CustomSuccessDialog builder = new CustomSuccessDialog(getActivity(), getResources().getString(R.string.dialog_title2), Html.fromHtml(message));
        builder.setPositiveButton(getResources().getString(R.string.agree), new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {
                sendRefresh();
                Intent bustravel = new Intent(getActivity(), MainActivity.class);
                startActivity(bustravel);
            }


        });


        builder.show();
    }
    private void sendRefresh() {
        getActivity().finish();
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "1");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }
}
