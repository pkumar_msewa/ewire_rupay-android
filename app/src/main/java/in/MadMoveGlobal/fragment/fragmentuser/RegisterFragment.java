package in.MadMoveGlobal.fragment.fragmentuser;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.DatePickerDialog;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.fingerprint.FingerprintManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.crashlytics.android.Crashlytics;
import com.orm.query.Select;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.KeyStore;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;

import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.model.QuestionListModel;
import in.MadMoveGlobal.util.NetworkErrorHandler;
import in.MadMoveGlobal.util.SecurityUtil;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.EwireRuPay.activity.OtpVerificationMidLayerActivity;
import io.fabric.sdk.android.Fabric;

/**
 * Created by Ksf on 3/27/2016.
 */
public class RegisterFragment extends Fragment {
    private EditText etRegisterPassword, etRegisterEmail, etRegisterMobile, etRegisterFirstName, etRegisterLastName, etRegisterDob, etRegisterPinCode, etVBAcc, etSecQuest, etSecAns, etRegisterRePassword, etRegisterMpin, etRegisterMRPin, etRegisterAadhar;
    private RadioButton rbVbYes, rbVbNo;


    //    private RadioButton rbRegisterMale,rbRegisterFeMale;
    private String firstName, lastName, newPassword, rePassword, email, mobileNo;

    private LoadingDialog loadDlg;

    private TextInputLayout ilRegisterMobile, ilRegisterPassword, ilRegisterEmail, ilRegisterDob, ilRegisterFName, ilRegisterPinCode, ilVBAcc, ilSecQuest, ilSecAns;

    private String gender = "M";
    private View rootView;
    private ImageView ivImagelogo;
    private JSONObject jsonRequest;
    private Button btnNextPsw, btnNext, btnBack, btnBackReg, btnNextOtp;
    private String quesCode;
    private Boolean isQuesSet = false;
    private LinearLayout llRegpage, llOtp, llFname, llLname, llEmail, llmobile, lldob, llPin, llsecqstn, llSecAns, llAadhar, llPassword, llRePassword, llMPin, llMRPin;
    //Strong password or weak
    private final Pattern hasUppercase = Pattern.compile("[A-Z]");
    private final Pattern hasLowercase = Pattern.compile("[a-z]");
    private final Pattern hasNumber = Pattern.compile("\\d");
    private final Pattern hasSpecialChar = Pattern.compile("[^a-zA-Z0-9 ]");

    private List<QuestionListModel> QuestionListModelList;

    public static final String PROPERTY_REG_ID = "registration_id";
    private String tag_json_obj = "json_user";
    //Volley Tag


    private String regId = "";
    private static final String KEY_NAME = "santosh";
    private Cipher cipher;
    private KeyStore keyStore;
    private KeyGenerator keyGenerator;
    private TextView textView;
    private FingerprintManager.CryptoObject cryptoObject;
    private FingerprintManager fingerprintManager;
    private KeyguardManager keyguardManager;
    private byte[] fingerprintStatus;
    private boolean status;
    private boolean cancel;
    private View focusView = null;
    private boolean hasmpin = true;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        loadDlg = new LoadingDialog(getActivity());
        QuestionListModelList = Select.from(QuestionListModel.class).list();


        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Fabric.with(getActivity(), new Crashlytics());
        rootView = inflater.inflate(R.layout.fragment_register, container, false);
        etRegisterFirstName = (EditText) rootView.findViewById(R.id.etRegisterFName);
        etRegisterLastName = (EditText) rootView.findViewById(R.id.etRegisterLName);
        etRegisterEmail = (EditText) rootView.findViewById(R.id.etRegisterEmail);
        etRegisterMobile = (EditText) rootView.findViewById(R.id.etRegisterMobile);
        etRegisterDob = (EditText) rootView.findViewById(R.id.etRegisterDob);
        etRegisterPinCode = (EditText) rootView.findViewById(R.id.etRegisterPinCode);
        etSecAns = (EditText) rootView.findViewById(R.id.etRegisterScurityAns);
        etSecQuest = (EditText) rootView.findViewById(R.id.etRegisterScurityQst);
        etRegisterAadhar = (EditText) rootView.findViewById(R.id.etRegisterAadhar);
        ivImagelogo = (ImageView) rootView.findViewById(R.id.ivImagelogo);

        etRegisterPassword = (EditText) rootView.findViewById(R.id.etRegisterPassword);
        etRegisterRePassword = (EditText) rootView.findViewById(R.id.etRegisterRePassword);
        etRegisterMpin = (EditText) rootView.findViewById(R.id.etRegisterMpin);
        etRegisterMRPin = (EditText) rootView.findViewById(R.id.etRegisterMRPin);

        etRegisterMobile.addTextChangedListener(new MyTextWatcher(etRegisterMobile));
        etRegisterEmail.addTextChangedListener(new MyTextWatcher(etRegisterEmail));
        etRegisterPassword.addTextChangedListener(new MyTextWatcher(etRegisterPassword));

        llFname = (LinearLayout) rootView.findViewById(R.id.llFname);
        llLname = (LinearLayout) rootView.findViewById(R.id.llLname);
        llEmail = (LinearLayout) rootView.findViewById(R.id.llEmail);
        llmobile = (LinearLayout) rootView.findViewById(R.id.llmobile);
        lldob = (LinearLayout) rootView.findViewById(R.id.lldob);
        llPin = (LinearLayout) rootView.findViewById(R.id.llPin);
        llsecqstn = (LinearLayout) rootView.findViewById(R.id.llsecqstn);
        llSecAns = (LinearLayout) rootView.findViewById(R.id.llSecAns);
        llAadhar = (LinearLayout) rootView.findViewById(R.id.llAadhar);
        llOtp = (LinearLayout) rootView.findViewById(R.id.llOtp);
        llRegpage = (LinearLayout) rootView.findViewById(R.id.llRegpage);


        llPassword = (LinearLayout) rootView.findViewById(R.id.llPassword);
        llRePassword = (LinearLayout) rootView.findViewById(R.id.llRePassword);
        llMPin = (LinearLayout) rootView.findViewById(R.id.llMPin);
        llMRPin = (LinearLayout) rootView.findViewById(R.id.llMRPin);


        btnNext = (Button) rootView.findViewById(R.id.btnNextPswprv);
        btnBack = (Button) rootView.findViewById(R.id.btnBackPsw);
        btnBackReg = (Button) rootView.findViewById(R.id.btnBackReg);
        btnNextOtp = (Button) rootView.findViewById(R.id.btnNextOtp);


        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etRegisterPassword.setFocusableInTouchMode(true);
                etRegisterPassword.setFocusable(true);
                etRegisterPassword.requestFocus();
                submitForm();

            }
        });

        btnNextOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                submitForm1();

            }
        });

        btnBackReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                llFname.setVisibility(View.VISIBLE);
                llLname.setVisibility(View.VISIBLE);
                llEmail.setVisibility(View.VISIBLE);
                llmobile.setVisibility(View.VISIBLE);
                lldob.setVisibility(View.VISIBLE);
                llPin.setVisibility(View.VISIBLE);
                llsecqstn.setVisibility(View.VISIBLE);
                llSecAns.setVisibility(View.VISIBLE);
                llAadhar.setVisibility(View.VISIBLE);
                llRegpage.setVisibility(View.VISIBLE);


                llPassword.setVisibility(View.GONE);
                llRePassword.setVisibility(View.GONE);
                llMRPin.setVisibility(View.GONE);
                llMPin.setVisibility(View.GONE);
                ivImagelogo.setVisibility(View.GONE);
                llOtp.setVisibility(View.GONE);


            }
        });


        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent verifyIntent = new Intent(getActivity(), LoginFragment.class);

                loadDlg.dismiss();
//                            startActivity(autoIntent);
                startActivity(verifyIntent);
            }
        });


        etRegisterDob.setFocusable(true);
        etRegisterDob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dateOfBirthDialog();
            }
        });
        etRegisterEmail.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    submitForm();
                }
                return false;
            }
        });

        GetQuestion getQuesTask = new GetQuestion();
        getQuesTask.execute();

        etSecQuest.setFocusable(false);
        etSecQuest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (QuestionListModelList == null || QuestionListModelList.size() == 0) {
                    CustomToast.showMessage(getActivity(), "Sorry error occured please try again");
                } else {
                    showQuesDialog();
                }

            }
        });

        etSecAns.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!etSecQuest.getText().toString().isEmpty()) {
                    setAnsFocus(true);
                } else {
                    setAnsFocus(false);
                    CustomToast.showMessage(getActivity(), "Select Bank first");
                }
            }
        });


        return rootView;
    }

    public void setAnsFocus(Boolean isQuesSet) {
        if (isQuesSet) {
            etSecQuest.setNextFocusDownId(R.id.etRegisterScurityAns);
            etSecAns.setFocusable(true);
            etSecAns.setFocusableInTouchMode(true);
            etSecAns.requestFocus();
        } else
            etSecAns.setFocusable(false);
    }

    private void showQuesDialog() {
        final ArrayList<String> quesVal = new ArrayList<>();
        for (QuestionListModel questionListModel : QuestionListModelList) {
            quesVal.add(questionListModel.getQuestion());
        }

        android.support.v7.app.AlertDialog.Builder bankDialog =
                new android.support.v7.app.AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
        bankDialog.setTitle("Select Your Security Question");

        bankDialog.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        bankDialog.setItems(quesVal.toArray(new String[quesVal.size()]),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int i) {
                        etSecQuest.setText(quesVal.get(i));
                        etSecAns.getText().clear();
                        quesCode = QuestionListModelList.get(i).getQuesCode();
                    }
                });
        bankDialog.show();

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void submitForm() {


        if (!validateFName()) {
            return;
        }
        if (!validateLName()) {
            return;
        }
        if (!validateEmail()) {
            return;
        }
        if (!validateMobile()) {
            return;
        }
        if (!validateDob()) {
            return;
        }
        if (!validatePin()) {
            return;
        }
        if (!validateSecqstn()) {
            return;
        }
        if (!validateSecans()) {
            return;
        }

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        llFname.setVisibility(View.GONE);
        llLname.setVisibility(View.GONE);
        llEmail.setVisibility(View.GONE);
        llmobile.setVisibility(View.GONE);
        lldob.setVisibility(View.GONE);
        llPin.setVisibility(View.GONE);
        llsecqstn.setVisibility(View.GONE);
        llSecAns.setVisibility(View.GONE);
        llAadhar.setVisibility(View.GONE);
        llRegpage.setVisibility(View.GONE);


        llPassword.setVisibility(View.VISIBLE);
        llRePassword.setVisibility(View.VISIBLE);
        llMRPin.setVisibility(View.VISIBLE);
        llMPin.setVisibility(View.VISIBLE);
        llOtp.setVisibility(View.VISIBLE);
        ivImagelogo.setVisibility(View.VISIBLE);


    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void submitForm1() {

        if (!validatePassword()) {
            return;
        }
        if (!validateMPin()) {
            return;
        }
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        promoteRegister();
    }


    private void promoteRegister() {
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("firstName", etRegisterFirstName.getText().toString());
            jsonRequest.put("lastName", etRegisterLastName.getText().toString());
            jsonRequest.put("password", etRegisterPassword.getText().toString());
            jsonRequest.put("email", etRegisterEmail.getText().toString());
            jsonRequest.put("contactNo", etRegisterMobile.getText().toString());
            jsonRequest.put("dateOfBirth", etRegisterDob.getText().toString());
            jsonRequest.put("gender", "NA");
            jsonRequest.put("confirmPassword", etRegisterPassword.getText().toString());
            jsonRequest.put("locationCode", etRegisterPinCode.getText().toString());
            jsonRequest.put("mpin", etRegisterMpin.getText().toString());
            jsonRequest.put("hasMpin", hasmpin);
            jsonRequest.put("androidDeviceID", SecurityUtil.getAndroidId(getActivity()));
            if (etSecAns.getText() != null && !etSecAns.getText().toString().isEmpty()) {
                jsonRequest.put("secQuestionCode", quesCode);
                jsonRequest.put("secAnswer", etSecAns.getText().toString());
            }
//            if (SecurityUtil.getIMEI(getActivity()) != null) {
//                jsonRequest.put("ipAddress", SecurityUtil.getAndroidId(getActivity()) + "-" + SecurityUtil.getIMEI(getActivity()));
//
//            } else {
//                jsonRequest.put("ipAddress", SecurityUtil.getAndroidId(getActivity()));
//
//            }
//            if (rbVbYes.isChecked()) {
//                jsonRequest.put("accountNumber", etVBAcc.getText().toString());
//                jsonRequest.put("vbankCustomer", "true");
//            } else {
//                jsonRequest.put("accountNumber", "");
//                jsonRequest.put("vbankCustomer", "false");
//            }

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {

            Log.i("URL LOGIN", ApiUrl.URL_REGISTRATION);
            Log.i("LoginRequest", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_REGISTRATION, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.i("Registration Response", response.toString());
                        String messageAPI = response.getString("message");
                        String code = response.getString("code");
                        if (code != null && code.equals("S00")) {

                            String jsonString = response.getString("response");
                            JSONObject jsonObject = new JSONObject(jsonString);
                            loadDlg.dismiss();

                            Intent otpIntent = new Intent(getActivity(), OtpVerificationMidLayerActivity.class);
                            otpIntent.putExtra("userMobileNo", etRegisterMobile.getText().toString());
                            otpIntent.putExtra("intentType", "Register");
                            startActivityForResult(otpIntent, 0);
                            getActivity().finish();

                        } else {
                            if (response.has("details")) {
                                if (!response.isNull("details")) {
                                    String messageDetail = response.getString("details");
                                    CustomToast.showMessage(getActivity(), messageDetail);
                                } else {
                                    CustomToast.showMessage(getActivity(), messageAPI);
                                }
                            }

                            loadDlg.dismiss();
                        }
                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
                    loadDlg.dismiss();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    private String getRegistrationId() {
        final SharedPreferences prefs = getGCMPreferences();
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            return "";
        }
        return registrationId;
    }

    private SharedPreferences getGCMPreferences() {
        return getActivity().getSharedPreferences("FIREBASE",
                Context.MODE_PRIVATE);
    }

    private class GetQuestion extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadDlg.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            getQuesList();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            etSecQuest.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (QuestionListModelList == null || QuestionListModelList.size() == 0) {
                        CustomToast.showMessage(getActivity(), "Error fetching questions list, please try again later");
                    } else {
                        showQuesDialog();
                    }
                }
            });

            etSecAns.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!etSecQuest.getText().toString().isEmpty()) {
                        setAnsFocus(true);
                    } else {
                        setAnsFocus(false);
                        CustomToast.showMessage(getActivity(), "Select Security Question first");
                    }
                }
            });
            loadDlg.dismiss();
        }
    }

    public void dateOfBirthDialog() {
        DatePickerDialog dialogDate = new DatePickerDialog(getActivity(), R.style.DatePicker, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar userAge = new GregorianCalendar(year, (monthOfYear + 1), dayOfMonth);
                Calendar minAdultAge = new GregorianCalendar();
                minAdultAge.add(Calendar.YEAR, -18);
                if (minAdultAge.before(userAge)) {
                    etRegisterDob.setError("You must be at least 18 years old to Register");
                    requestFocus(etRegisterDob);
                } else {

                    etRegisterDob.setText(String.valueOf(year) + "-"
                            + String.valueOf(monthOfYear + 1) + "-"
                            + String.valueOf(dayOfMonth));
                }
            }
        }, 1990, 1, 1);
        dialogDate.setButton(DatePickerDialog.BUTTON_POSITIVE, this.getResources().getString(R.string.zxing_button_ok), dialogDate);
        dialogDate.setButton(DatePickerDialog.BUTTON_NEGATIVE, this.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        dialogDate.getDatePicker().setMaxDate(new Date().getTime());
        dialogDate.show();


    }

    public void getQuesList() {
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, ApiUrl.URL_LIST_QUES, (String) null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String code = response.getString("code");
                            String mes = response.getString("msg");
                            if (code != null && code.equals("S00")) {
                                QuestionListModel.deleteAll(QuestionListModel.class);
                                QuestionListModelList.clear();
                                String resp = response.getString("response");
                                JSONObject respObj = new JSONObject(resp);

                                String quesList = respObj.getString("questions");
                                JSONArray questionArray = new JSONArray(quesList);
                                for (int i = 0; i < questionArray.length(); i++) {
                                    JSONObject q = questionArray.getJSONObject(i);
                                    String quesCode = q.getString("code");
                                    String question = q.getString("question");
                                    QuestionListModel qlModel = new QuestionListModel(quesCode, question);
                                    qlModel.save();
                                    QuestionListModelList.add(qlModel);
                                }

                            }
                        } catch (JSONException e) {
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }

        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> map = new HashMap<>();
                map.put("hash", "123");
                return map;
            }

        };
        PayQwikApplication.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    private boolean validateFName() {
        if (etRegisterFirstName.getText().toString().trim().isEmpty()) {
            etRegisterFirstName.setError("Enter first name");
            requestFocus(etRegisterFirstName);
            return false;
        } else {
            etRegisterFirstName.setError(null);
        }

        return true;
    }

    private boolean validateLName() {
        if (etRegisterLastName.getText().toString().trim().isEmpty()) {
            etRegisterLastName.setError("Enter last name");
            requestFocus(etRegisterLastName);
            return false;
        } else {
            etRegisterLastName.setError(null);
        }

        return true;
    }

    private boolean validateSecqstn() {
        if (etSecQuest.getText().toString().trim().isEmpty()) {
            etSecQuest.setError("Select Security Question");
            requestFocus(etSecQuest);
            return false;
        } else {
            etSecQuest.setError(null);
        }

        return true;
    }

    private boolean validateSecans() {
        if (etSecAns.getText().toString().trim().isEmpty()) {
            etSecAns.setError("Enter Security Ans.");
            requestFocus(etSecAns);
            return false;
        } else {
            etSecAns.setError(null);
        }

        return true;
    }


    private boolean validateEmail() {
        String email = etRegisterEmail.getText().toString().trim();
        if (email.isEmpty() || !isValidEmail(email)) {
            etRegisterEmail.setError("Enter valid email");
            requestFocus(etRegisterEmail);
            return false;
        } else {
            etRegisterEmail.setError(null);
        }

        return true;
    }

    private boolean validateMobile() {
        if (etRegisterMobile.getText().toString().trim().isEmpty() || etRegisterMobile.getText().toString().trim().length() < 10) {
            etRegisterMobile.setError("Enter 10 digits mobile no");
            requestFocus(etRegisterMobile);
            return false;
        } else {
            etRegisterMobile.setError(null);
        }

        return true;
    }

    private boolean validateDob() {
        if (etRegisterDob.getText().toString().trim().isEmpty()) {
            etRegisterDob.setError("Enter Date of birth");
            requestFocus(etRegisterDob);
            return false;
        } else {
            etRegisterDob.setError(null);
        }

        return true;
    }

    private boolean validatePin() {
        if (etRegisterPinCode.getText().toString().trim().isEmpty() || etRegisterPinCode.getText().toString().trim().length() < 6) {
            etRegisterPinCode.setError("Enter Postal Code");
            requestFocus(etRegisterPinCode);
            return false;
        } else {
            etRegisterPinCode.setError(null);
        }

        return true;
    }

    private boolean validatePasswordListner() {
        if (etRegisterPassword.getText().toString().trim().isEmpty() || etRegisterPassword.getText().toString().trim().length() < 6) {
            etRegisterPassword.setError(getResources().getString(R.string.password_validity));
            requestFocus(etRegisterPassword);
            return false;
        } else {
//            etRegisterPassword.setErrorEnabled(false);
            checkPwdStr();
        }

        return true;
    }

    private String checkPwdStr() {
        if (hasUppercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasLowercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasNumber.matcher(etRegisterPassword.getText().toString().trim()).find() && hasSpecialChar.matcher(etRegisterPassword.getText().toString().trim()).find()) {
            etRegisterPassword.setError("Strong Password");
        } else if (hasUppercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasLowercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasNumber.matcher(etRegisterPassword.getText().toString().trim()).find()) {
            etRegisterPassword.setError("Strong Password");
        } else if (hasSpecialChar.matcher(etRegisterPassword.getText().toString().trim()).find() && hasLowercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasNumber.matcher(etRegisterPassword.getText().toString().trim()).find()) {
            etRegisterPassword.setError("Strong Password");
        } else if (hasSpecialChar.matcher(etRegisterPassword.getText().toString().trim()).find() && hasUppercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasNumber.matcher(etRegisterPassword.getText().toString().trim()).find()) {
            etRegisterPassword.setError("Strong Password");
        } else if (hasUppercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasNumber.matcher(etRegisterPassword.getText().toString().trim()).find()) {
            etRegisterPassword.setError("Good Password");
        } else if (hasUppercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasSpecialChar.matcher(etRegisterPassword.getText().toString().trim()).find()) {
            etRegisterPassword.setError("Good Password");
        } else if (hasUppercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasLowercase.matcher(etRegisterPassword.getText().toString().trim()).find()) {
            etRegisterPassword.setError("Good Password");
        } else if (hasNumber.matcher(etRegisterPassword.getText().toString().trim()).find() && hasSpecialChar.matcher(etRegisterPassword.getText().toString().trim()).find()) {
            etRegisterPassword.setError("Good Password");
        } else if (hasNumber.matcher(etRegisterPassword.getText().toString().trim()).find() && hasLowercase.matcher(etRegisterPassword.getText().toString().trim()).find()) {
            etRegisterPassword.setError("Good Password");
        } else {
            etRegisterPassword.setError("Weak Password");
        }


        return null;
    }

    private boolean validatePassword() {
        if (etRegisterPassword.getText().toString().length() < 6) {
            CustomToast.showMessage(getActivity(), "Enter 6 digits password");
            return false;
        } else if (etRegisterRePassword.getText().toString().length() < 6) {
            CustomToast.showMessage(getActivity(), "Enter 6 digits password");
            return false;
        } else if (!etRegisterPassword.getText().toString().equals(etRegisterRePassword.getText().toString())) {
            CustomToast.showMessage(getActivity(), "Password didn't match");
            return false;

        }
        return true;
    }

    private boolean validateMPin() {
        if (etRegisterMpin.getText().toString().length() < 4) {
            CustomToast.showMessage(getActivity(), "Please Enter 4 digits MPIN");
            return false;
        } else if (etRegisterMRPin.getText().toString().length() < 4) {
            CustomToast.showMessage(getActivity(), "Please Enter 4 digits MPIN");
            return false;
        } else if (!etRegisterMpin.getText().toString().equals(etRegisterMRPin.getText().toString())) {
            CustomToast.showMessage(getActivity(), "MPIN didn't match");
            return false;

        }
        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            int i = view.getId();
            if (i == R.id.etRegisterEmail) {
                validateEmail();
            } else if (i == R.id.etRegisterMobile) {
                validateMobile();
            } else if (i == R.id.etRegisterPassword) {
                validatePasswordListner();
            }

        }
    }

    static String getEmail(Context context) {
        AccountManager accountManager = AccountManager.get(context);
        Account account = getAccount(accountManager);

        if (account == null) {
            return null;
        } else {
            return account.name;
        }
    }


    private static Account getAccount(AccountManager accountManager) {
        Account[] accounts = accountManager.getAccountsByType("com.google");
        Account account;
        if (accounts.length > 0) {
            account = accounts[0];
        } else {
            account = null;
        }
        return account;
    }

}

