package in.MadMoveGlobal.fragment.fragmentuser;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
//import com.example.ekycsdkdemo.Config;
//import com.example.ekycsdkdemo.WebViewActivity;
//import com.example.ekycsdkdemo.mantra.model.EkycData;
//import com.example.ekycsdkdemo.mantra.model.ResponseModel;
import com.crashlytics.android.Crashlytics;
import com.orm.query.Select;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.KeyStore;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;

import in.MadMoveGlobal.adapter.OperatorSpinnerAdapter;
import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.metadata.AppMetadata;
import in.MadMoveGlobal.metadata.MenuMetadata;
import in.MadMoveGlobal.model.QuestionListModel;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.EwireRuPay.activity.MainMenuDetailActivity;
import in.MadMoveGlobal.EwireRuPay.activity.OtpVerificationMidLayerActivity;
import in.MadMoveGlobal.util.NetworkErrorHandler;
import in.MadMoveGlobal.util.SecurityUtil;
import io.fabric.sdk.android.Fabric;

/**
 * Created by Ksf on 3/27/2016.
 */
public class NewRegisterActivity extends AppCompatActivity {

    private RadioButton rbVbYes, rbVbNo;


    private EditText etAdharid, etAdharMobile, etRegisterFName, etRegisterLName, etRegisterEmail, etARegisterEmail, etRegisterMobile, etRegisterPassword, etARegisterPassword, etGovtIdNo, etAddress, etRegisterMpin, etARegisterMpin,etRegisterDob;


    //    private RadioButton rbRegisterMale,rbRegisterFeMale;
    private String firstName, lastName, newPassword, rePassword, email, mobileNo;

    private LoadingDialog loadDlg;

    private TextInputLayout ilRegisterMobile, ilRegisterPassword, ilRegisterEmail, ilRegisterDob, ilRegisterFName, ilRegisterPinCode, ilVBAcc, ilSecQuest, ilSecAns;

    private String gender = "M";
    private View rootView;
    private ImageView ivImagelogo;
    private JSONObject jsonRequest;
    private Button btnNextPsw, btnNext, btnBack, btnBackReg, btnNextOtp, backtologin, btnRegNextOtp, btnRegBack;
    private String quesCode;
    private Boolean isQuesSet = false;
    private LinearLayout llRegpage, llAadharNo, llSimpleReg, llAdharid, llAdharmobile, llGovtid, llAddress, llOtp, llRegOtp, llFname, llEmail, llmobile, lldob, llPin, llsecqstn, llSecAns, llAadhar, llPassword, llRePassword, llMPin, llMRPin;
    //Strong password or weak
    private final Pattern hasUppercase = Pattern.compile("[A-Z]");
    private final Pattern hasLowercase = Pattern.compile("[a-z]");
    private final Pattern hasNumber = Pattern.compile("\\d");
    private final Pattern hasSpecialChar = Pattern.compile("[^a-zA-Z0-9 ]");

    private List<QuestionListModel> QuestionListModelList;

    public static final String PROPERTY_REG_ID = "registration_id";
    private String tag_json_obj = "json_user";
    //Volley Tag


    private String regId = "";
    private static final String KEY_NAME = "santosh";
    private Cipher cipher;
    private KeyStore keyStore;
    private KeyGenerator keyGenerator;
    private TextView textView;
    private FingerprintManager.CryptoObject cryptoObject;
    private FingerprintManager fingerprintManager;
    private KeyguardManager keyguardManager;
    private byte[] fingerprintStatus;
    private boolean status;
    private boolean cancel;
    private Spinner spinner_govtid_provider;
    private View focusView = null;
    private boolean hasmpin = true;
    private OperatorSpinnerAdapter operatorSpinnerAdapter;
    private JSONObject jsonObject;
    private String Auid, Apincode, Aname, Agender, Adob, AKycInfo, AfName, AlastName, Adist, Astate;
    private boolean aadharValue = false;
    private ProgressBar pbRegistration;
    private Button btnTermCondition;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.fragment_new_register);
        setStatusBarGradiant(NewRegisterActivity.this);
        loadDlg = new LoadingDialog(this);
        QuestionListModelList = Select.from(QuestionListModel.class).list();


        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


        etAdharid = (EditText) findViewById(R.id.etAdharid);
        etAdharMobile = (EditText) findViewById(R.id.etAdharMobile);
        etRegisterFName = (EditText) findViewById(R.id.etRegisterFName);
        etRegisterLName = (EditText) findViewById(R.id.etRegisterLName);
        etRegisterEmail = (EditText) findViewById(R.id.etRegisterEmail);

        etARegisterEmail = (EditText) findViewById(R.id.etARegisterEmail);
        etARegisterPassword = (EditText) findViewById(R.id.etARegisterPassword);
        etARegisterMpin = (EditText) findViewById(R.id.etARegisterMpin);
        etRegisterDob = (EditText) findViewById(R.id.etRegisterDob);


        ivImagelogo = (ImageView) findViewById(R.id.ivImagelogo);
        etRegisterMobile = (EditText) findViewById(R.id.etRegisterMobile);
        etRegisterPassword = (EditText) findViewById(R.id.etRegisterPassword);
        etGovtIdNo = (EditText) findViewById(R.id.etGovtIdNo);
        etAddress = (EditText) findViewById(R.id.etAddress);
        etRegisterMpin = (EditText) findViewById(R.id.etRegisterMpin);
        spinner_govtid_provider = (Spinner) findViewById(R.id.spinner_govtid_provider);
        pbRegistration = (ProgressBar) findViewById(R.id.pbRegistration);

        llAadharNo = (LinearLayout) findViewById(R.id.llAadharNo);

        llAdharid = (LinearLayout) findViewById(R.id.llAdharid);
        llAdharmobile = (LinearLayout) findViewById(R.id.llAdharmobile);
        llFname = (LinearLayout) findViewById(R.id.llFname);
        llEmail = (LinearLayout) findViewById(R.id.llEmail);
        llmobile = (LinearLayout) findViewById(R.id.llmobile);
        llPassword = (LinearLayout) findViewById(R.id.llPassword);
        llAddress = (LinearLayout) findViewById(R.id.llAddress);
        llRegpage = (LinearLayout) findViewById(R.id.llRegpage);
        llOtp = (LinearLayout) findViewById(R.id.llOtp);
        llRegOtp = (LinearLayout) findViewById(R.id.llRegOtp);

        btnNext = (Button) findViewById(R.id.btnNextPswprv);
        btnBack = (Button) findViewById(R.id.btnBackPsw);
        btnRegBack = (Button) findViewById(R.id.btnRegBack);
        btnBackReg = (Button) findViewById(R.id.btnBackReg);
        btnNextOtp = (Button) findViewById(R.id.btnNextOtp);
        btnRegNextOtp = (Button) findViewById(R.id.btnRegNextOtp);
        backtologin = (Button) findViewById(R.id.btnlogin);
        btnTermCondition = (Button) findViewById(R.id.btnTermCondition);

        operatorSpinnerAdapter = new OperatorSpinnerAdapter(this, R.layout.spinners, MenuMetadata.getgovtId());
        spinner_govtid_provider.setAdapter(operatorSpinnerAdapter);
        llAadharNo.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                etAdharid.setFocusableInTouchMode(true);
                etAdharid.setFocusable(true);
                etAdharid.requestFocus();
                aadharValue = true;
                llAdharid.setVisibility(View.VISIBLE);
                llAadharNo.setVisibility(View.GONE);
                llFname.setVisibility(View.GONE);
                submitForm();

            }
        });


        etRegisterDob.setFocusable(true);
//        etRegisterDob.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                dateOfBirthDialog();
//            }
//        });

        etRegisterDob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DatePickerDialog dialog = new DatePickerDialog(NewRegisterActivity.this, R.style.DatePicker, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        String formattedDay, formattedMonth;
                        if (dayOfMonth < 10) {
                            formattedDay = "0" + dayOfMonth;
                        } else {
                            formattedDay = dayOfMonth + "";
                        }

                        if ((monthOfYear + 1) < 10) {
                            formattedMonth = "0" + String.valueOf(monthOfYear + 1);
                        } else {
                            formattedMonth = String.valueOf(monthOfYear + 1) + "";
                        }
                        etRegisterDob.setText(String.valueOf(year) + "-"
                                + String.valueOf(formattedMonth) + "-"
                                + String.valueOf(formattedDay));
                    }
                }, 1990, 01, 01);

                dialog.setButton(DatePickerDialog.BUTTON_POSITIVE, "OK", dialog);
                dialog.setButton(DatePickerDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener()

                {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                dialog.show();
            }
        });

        btnTermCondition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent menuIntent = new Intent(NewRegisterActivity.this, MainMenuDetailActivity.class);
                menuIntent.putExtra(AppMetadata.FRAGMENT_TYPE, "TermsAndCondition");
                startActivity(menuIntent);
            }
        });


        btnBackReg.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                etAdharid.setFocusableInTouchMode(false);
                etAdharid.setFocusable(false);
                etRegisterFName.setFocusableInTouchMode(true);
                etRegisterFName.setFocusable(true);
                etRegisterFName.requestFocus();
                llAdharid.setVisibility(View.GONE);
                llFname.setVisibility(View.VISIBLE);
                llAadharNo.setVisibility(View.VISIBLE);


            }
        });


        btnNextOtp.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {

                if (aadharValue) {

                    submitForm();
                } else {
                    submitForm1();
                }

            }
        });

        btnRegNextOtp.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {

                if (aadharValue) {

                    submitForm();
                } else {
                    submitForm1();
                }

            }
        });


        btnRegBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    private void submitForm() {


        if (!validateAadhar()) {
            return;
        }
        if (!validateMobiles()) {
            return;
        }
        if (!validateAEmail()) {
            return;
        }
        if (!validateAPassword()) {
            return;
        }
        if (!validateAMPin()) {
            return;
        }

//        EkycData ekycData = new EkycData();
//        ekycData.setTransactionType("E");
//        ekycData.setTransactionID("paulpay123");
//        ekycData.setAadharNumber(etAdharid.getText().toString());
//        ekycData.setDeviceType("Mantra(MFS100)");
//        ekycData.setKey("eba52eb1831f59390033882f4e3c4811");
//        ekycData.setLicKey("BH0M08UB1P5JMDwqXlICdwfuzWNlSrP7MFRjCDmJ+2+1SjybgDQS8JK7DI3Ifr/tn4SWW2EmfO++yobrlKxiu+emD/g3hVSiFPxk8uWdQmn/La/BTlx7wiM7OV1O1jhVQdmfkolGcQBBL0XejMZR53GZhsDA7G1h1IuVu6X4nlA=");
//
//        Intent ekycIntent = new Intent(this, WebViewActivity.class);
//        ekycIntent.putExtra(Config.ARGUMENT_DATA, ekycData);
//        startActivityForResult(ekycIntent, Config.REQUEST_CODE);


        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void submitForm1() {

        if (!validateFName()) {
            return;
        }
        if (!validateLName()) {
            return;
        }
        if (!validateEmail()) {
            return;
        }
        if (!validatePassword()) {
            return;
        }
        if (!validateMobile()) {
            return;
        } if (!validateDob()) {
            return;
        }
//        if (!validatePin()) {
//            return;
//        }
        if (!validateMPin()) {
            return;
        }
//        if (spinner_govtid_provider.getSelectedItemPosition() == 0) {
//            CustomToast.showMessage(this, "Please select any valid ID to continue");
//            return;
//        } else {
//            serviceProviderName = operator.get(spinner_mob_provider_postpaid.getSelectedItemPosition()).getName();
//            serviceProvider = operator.get(spinner_mob_provider_postpaid.getSelectedItemPosition()).getCode();
//        }
//        if (!validateValidId()) {
//            return;
//        }
//        if (!validateIdno()) {
//            return;
//        }
//        if (!validateAddress()) {
//            return;
//        }
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            promoteRegister();
        }




//    public void dateOfBirthDialog() {
//        DatePickerDialog dialogDate = new DatePickerDialog(this , R.style.DatePicker, new DatePickerDialog.OnDateSetListener() {
//            @Override
//            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
//                Calendar userAge = new GregorianCalendar(year, (monthOfYear + 1), dayOfMonth);
//                Calendar minAdultAge = new GregorianCalendar();
//                minAdultAge.add(Calendar.YEAR, -18);
//                if (minAdultAge.before(userAge)) {
//                    etRegisterDob.setError("You must be at least 18 years old to Register");
//                    requestFocus(etRegisterDob);
//                } else {
//
//                    etRegisterDob.setText(String.valueOf(year) + "-"
//                            + String.valueOf(monthOfYear + 1) + "-"
//                            + String.valueOf(dayOfMonth));
//                }
//            }
//        }, 1990, 01, 01);
//        dialogDate.setButton(DatePickerDialog.BUTTON_POSITIVE, this.getResources().getString(R.string.zxing_button_ok), dialogDate);
//        dialogDate.setButton(DatePickerDialog.BUTTON_NEGATIVE, this.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//                dialogInterface.dismiss();
//            }
//        });
//        dialogDate.getDatePicker().setMaxDate(new Date().getTime());
//        dialogDate.show();
//
//
//    }



    private void promoteRegister() {
        pbRegistration.setVisibility(View.VISIBLE);
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("firstName", etRegisterFName.getText().toString());
            jsonRequest.put("lastName", etRegisterLName.getText().toString());
            jsonRequest.put("username", etRegisterMobile.getText().toString());
            jsonRequest.put("email", etRegisterEmail.getText().toString());
            jsonRequest.put("password", etRegisterPassword.getText().toString());
            jsonRequest.put("contactNo", etRegisterMobile.getText().toString());
            jsonRequest.put("locationCode", etRegisterMpin.getText().toString());

//            jsonRequest.put("confirmPassword", etRegisterPassword.getText().toString());
//            jsonRequest.put("mpin", etRegisterMpin.getText().toString());
//            jsonRequest.put("hasMpin", hasmpin);
//            jsonRequest.put("androidDeviceID", SecurityUtil.getAndroidId(NewRegisterActivity.this));
//
            jsonRequest.put("dateOfBirth",  etRegisterDob.getText().toString());
//            jsonRequest.put("aadharNo", "");
//            jsonRequest.put("hasAadhar", false);
//            jsonRequest.put("idType", "");
//            jsonRequest.put("idNo", etGovtIdNo.getText().toString());
//            jsonRequest.put("brand", "");
//            jsonRequest.put("model", "");
//            jsonRequest.put("imeiNo", "");

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {

            Log.i("URL REGISTRATION", ApiUrl.URL_REGISTRATION);
            Log.i("REGISTRATIONREQUEST", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_REGISTRATION, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.i("REGISTRATION Response", response.toString());
                        String messageAPI = response.getString("message");
                        String code = response.getString("code");
                        if (code != null && code.equals("S00")) {
                            pbRegistration.setVisibility(View.GONE);
//                            CustomToast.showMessage(NewRegisterActivity.this, messageAPI);

                            loadDlg.dismiss();

                            Intent otpIntent = new Intent(NewRegisterActivity.this, OtpVerificationMidLayerActivity.class);
                            otpIntent.putExtra("userMobileNo", etRegisterMobile.getText().toString());
                            otpIntent.putExtra("intentType", "Register");
                            startActivityForResult(otpIntent, 0);
                            finish();

                        } else {
                            pbRegistration.setVisibility(View.GONE);
                            if (response.has("details")) {
                                if (!response.isNull("details")) {
                                    String messageDetail = response.getString("details");
                                    CustomToast.showMessage(NewRegisterActivity.this, messageDetail);
                                } else {
                                    CustomToast.showMessage(NewRegisterActivity.this, messageAPI);
                                }
                            }

                            loadDlg.dismiss();
                        }
                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        pbRegistration.setVisibility(View.GONE);
                        CustomToast.showMessage(NewRegisterActivity.this, getResources().getString(R.string.server_exception2));
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    pbRegistration.setVisibility(View.GONE);
                    CustomToast.showMessage(NewRegisterActivity.this, NetworkErrorHandler.getMessage(error, NewRegisterActivity.this));
                    loadDlg.dismiss();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    private void promoteAdharRegister() {
        pbRegistration.setVisibility(View.VISIBLE);
        jsonRequest = new JSONObject();
        try {


            jsonRequest.put("firstName", AfName);
            jsonRequest.put("lastName", AlastName);
            jsonRequest.put("password", etARegisterPassword.getText().toString());
            jsonRequest.put("email", etARegisterEmail.getText().toString());
            jsonRequest.put("contactNo", etAdharMobile.getText().toString());
            jsonRequest.put("confirmPassword", etARegisterPassword.getText().toString());
            jsonRequest.put("mpin", etARegisterMpin.getText().toString());
            jsonRequest.put("hasMpin", hasmpin);
            jsonRequest.put("androidDeviceID", SecurityUtil.getAndroidId(NewRegisterActivity.this));
            jsonRequest.put("locationCode", Apincode);
            jsonRequest.put("dateOfBirth", Adob);
            jsonRequest.put("aadharNo", Auid);
            jsonRequest.put("hasAadhar", true);
            jsonRequest.put("idType", "UID");
            jsonRequest.put("idNo", etGovtIdNo.getText().toString());
            jsonRequest.put("brand", "");
            jsonRequest.put("model", "");
            jsonRequest.put("imeiNo", "");


        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {

            Log.i("URL LOGIN", ApiUrl.URL_REGISTRATION);
            Log.i("LoginRequest", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_REGISTRATION, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.i("Registration Response", response.toString());
                        String messageAPI = response.getString("message");
                        String code = response.getString("code");
                        if (code != null && code.equals("S00")) {

                            pbRegistration.setVisibility(View.GONE);
                            String jsonString = response.getString("response");
                            JSONObject jsonObject = new JSONObject(jsonString);
                            loadDlg.dismiss();

                            Intent otpIntent = new Intent(NewRegisterActivity.this, OtpVerificationMidLayerActivity.class);
                            otpIntent.putExtra("userMobileNo", etAdharMobile.getText().toString());
                            otpIntent.putExtra("intentType", "Register");
                            startActivityForResult(otpIntent, 0);
                            finish();

                        } else {
                            pbRegistration.setVisibility(View.GONE);
                            if (response.has("details")) {
                                if (!response.isNull("details")) {
                                    String messageDetail = response.getString("details");
                                    CustomToast.showMessage(NewRegisterActivity.this, messageDetail);
                                } else {
                                    CustomToast.showMessage(NewRegisterActivity.this, messageAPI);
                                }
                            }

                            loadDlg.dismiss();
                        }
                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        pbRegistration.setVisibility(View.GONE);
                        CustomToast.showMessage(NewRegisterActivity.this, getResources().getString(R.string.server_exception2));
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    pbRegistration.setVisibility(View.GONE);
                    CustomToast.showMessage(NewRegisterActivity.this, NetworkErrorHandler.getMessage(error, NewRegisterActivity.this));
                    loadDlg.dismiss();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    private String getRegistrationId() {
        final SharedPreferences prefs = getGCMPreferences();
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            return "";
        }
        return registrationId;
    }

    private SharedPreferences getGCMPreferences() {
        return getSharedPreferences("FIREBASE",
                Context.MODE_PRIVATE);
    }


    private boolean validateFName() {
        if (etRegisterFName.getText().toString().trim().isEmpty()) {
            etRegisterFName.setError("Enter First name");
            requestFocus(etRegisterFName);
            return false;
        } else {
            etRegisterFName.setError(null);
        }

        return true;
    }


    private boolean validateAddress() {
        if (etAddress.getText().toString().trim().isEmpty()) {
            etAddress.setError("Enter Address");
            requestFocus(etAddress);
            return false;
        } else {
            etAddress.setError(null);
        }

        return true;
    }


//    private boolean validateValidId() {
//        if (etGovtId.getText().toString().trim().isEmpty()) {
//            etGovtId.setError("Select Id");
//            requestFocus(etGovtId);
//            return false;
//        } else {
//            etGovtId.setError(null);
//        }
//
//        return true;
//    }

    private boolean validateLName() {
        if (etRegisterLName.getText().toString().trim().isEmpty()) {
            etRegisterLName.setError("Enter Last name");
            requestFocus(etRegisterLName);
            return false;
        } else {
            etRegisterLName.setError(null);
        }

        return true;
    }

//    private boolean validatePin() {
//        if (etRegisterPinCode.getText().toString().trim().isEmpty() || etRegisterPinCode.getText().toString().trim().length() < 6) {
//            etRegisterPinCode.setError("Enter Postal Code");
//            requestFocus(etRegisterPinCode);
//            return false;
//        } else {
//            etRegisterPinCode.setError(null);
//        }
//
//        return true;
//    }

    private boolean validateEmail() {
        String email = etRegisterEmail.getText().toString().trim();
        if (email.isEmpty() || !isValidEmail(email)) {
            etRegisterEmail.setError("Enter valid email");
            requestFocus(etRegisterEmail);
            return false;
        } else {
            etRegisterEmail.setError(null);
        }

        return true;
    }

    private boolean validateAEmail() {
        String email = etARegisterEmail.getText().toString().trim();
        if (email.isEmpty() || !isValidEmail(email)) {
            etARegisterEmail.setError("Enter valid email");
            requestFocus(etARegisterEmail);
            return false;
        } else {
            etARegisterEmail.setError(null);
        }

        return true;
    }

    private boolean validateMobile() {
        if (etRegisterMobile.getText().toString().trim().isEmpty() || etRegisterMobile.getText().toString().trim().length() < 10) {
            etRegisterMobile.setError("Enter 10 digits mobile no");
            requestFocus(etRegisterMobile);
            return false;
        } else {
            etRegisterMobile.setError(null);
        }

        return true;
    }

    private boolean validateDob() {
        if (etRegisterDob.getText().toString().trim().isEmpty()) {
            etRegisterDob.setError("Enter Date of birth");
            requestFocus(etRegisterDob);
            return false;
        } else {
            etRegisterDob.setError(null);
        }

        return true;
    }

    private boolean validateMobiles() {
        if (etAdharMobile.getText().toString().trim().isEmpty() || etAdharMobile.getText().toString().trim().length() < 10) {
            etAdharMobile.setError("Enter 10 digits mobile no");
            requestFocus(etAdharMobile);
            return false;
        } else {
            etAdharMobile.setError(null);
        }

        return true;
    }

    private boolean validateIdno() {
        if (etGovtIdNo.getText().toString().trim().isEmpty() || etGovtIdNo.getText().toString().trim().length() < 16) {
            etGovtIdNo.setError("Enter 16 digits Id no");
            requestFocus(etGovtIdNo);
            return false;
        } else {
            etGovtIdNo.setError(null);
        }

        return true;
    }


    private boolean validatePasswordListner() {
        if (etRegisterPassword.getText().toString().trim().isEmpty() || etRegisterPassword.getText().toString().trim().length() < 6) {
            etRegisterPassword.setError(getResources().getString(R.string.password_validity));
            requestFocus(etRegisterPassword);
            return false;
        } else {
//            etRegisterPassword.setErrorEnabled(false);
            checkPwdStr();
        }

        return true;
    }

    private String checkPwdStr() {
        if (hasUppercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasLowercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasNumber.matcher(etRegisterPassword.getText().toString().trim()).find() && hasSpecialChar.matcher(etRegisterPassword.getText().toString().trim()).find()) {
            etRegisterPassword.setError("Strong Password");
        } else if (hasUppercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasLowercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasNumber.matcher(etRegisterPassword.getText().toString().trim()).find()) {
            etRegisterPassword.setError("Strong Password");
        } else if (hasSpecialChar.matcher(etRegisterPassword.getText().toString().trim()).find() && hasLowercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasNumber.matcher(etRegisterPassword.getText().toString().trim()).find()) {
            etRegisterPassword.setError("Strong Password");
        } else if (hasSpecialChar.matcher(etRegisterPassword.getText().toString().trim()).find() && hasUppercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasNumber.matcher(etRegisterPassword.getText().toString().trim()).find()) {
            etRegisterPassword.setError("Strong Password");
        } else if (hasUppercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasNumber.matcher(etRegisterPassword.getText().toString().trim()).find()) {
            etRegisterPassword.setError("Good Password");
        } else if (hasUppercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasSpecialChar.matcher(etRegisterPassword.getText().toString().trim()).find()) {
            etRegisterPassword.setError("Good Password");
        } else if (hasUppercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasLowercase.matcher(etRegisterPassword.getText().toString().trim()).find()) {
            etRegisterPassword.setError("Good Password");
        } else if (hasNumber.matcher(etRegisterPassword.getText().toString().trim()).find() && hasSpecialChar.matcher(etRegisterPassword.getText().toString().trim()).find()) {
            etRegisterPassword.setError("Good Password");
        } else if (hasNumber.matcher(etRegisterPassword.getText().toString().trim()).find() && hasLowercase.matcher(etRegisterPassword.getText().toString().trim()).find()) {
            etRegisterPassword.setError("Good Password");
        } else {
            etRegisterPassword.setError("Weak Password");
        }


        return null;
    }

    private boolean validatePassword() {
        if (etRegisterPassword.getText().toString().trim().isEmpty()) {
            etRegisterPassword.setError("Required password");
            requestFocus(etRegisterPassword);
            return false;
        } else if (etRegisterPassword.getText().toString().trim().length() < 6) {
            etRegisterPassword.setError(getResources().getString(R.string.password_validity));
            requestFocus(etRegisterPassword);
            return false;
        } else {
            etRegisterPassword.setError(null);
        }

        return true;
    }

    private boolean validateAPassword() {
        if (etARegisterPassword.getText().toString().trim().isEmpty()) {
            etARegisterPassword.setError("Required password");
            requestFocus(etARegisterPassword);
            return false;
        } else if (etARegisterPassword.getText().toString().trim().length() < 6) {
            etARegisterPassword.setError(getResources().getString(R.string.password_validity));
            requestFocus(etARegisterPassword);
            return false;
        } else {
            etARegisterPassword.setError(null);
        }

        return true;
    }


    private boolean validateMPin() {
        if (etRegisterMpin.getText().toString().trim().isEmpty() || etRegisterMpin.getText().toString().trim().length() < 6) {
            etRegisterMpin.setError("Enter valid Postal code");
            requestFocus(etRegisterMpin);
            return false;
        } else {
            etRegisterMpin.setError(null);
        }

        return true;
    }

    private boolean validateAMPin() {
        if (etARegisterMpin.getText().toString().trim().isEmpty() || etARegisterMpin.getText().toString().trim().length() < 4) {
            etARegisterMpin.setError("Enter 4digits MPIN");
            requestFocus(etARegisterMpin);
            return false;
        } else {
            etARegisterMpin.setError(null);
        }

        return true;
    }


    private boolean validateAadhar() {

        if (etAdharid.getText().toString().trim().isEmpty() || etAdharid.getText().toString().trim().length() < 12) {
            etAdharid.setError("Please Enter 12 digits Aadhaar Number");
            requestFocus(etAdharid);
            return false;
        } else {
            etAdharid.setError(null);
        }

        return true;

    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            int i = view.getId();
            if (i == R.id.etRegisterEmail) {
                validateEmail();
            } else if (i == R.id.etRegisterMobile) {
                validateMobile();
            } else if (i == R.id.etRegisterPassword) {
                validatePasswordListner();
            }

        }
    }

    static String getEmail(Context context) {
        AccountManager accountManager = AccountManager.get(context);
        Account account = getAccount(accountManager);

        if (account == null) {
            return null;
        } else {
            return account.name;
        }
    }


    private static Account getAccount(AccountManager accountManager) {
        Account[] accounts = accountManager.getAccountsByType("com.google");
        Account account;
        if (accounts.length > 0) {
            account = accounts[0];
        } else {
            account = null;
        }
        return account;
    }


//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == Config.REQUEST_CODE) {
//            try {
//                if (resultCode == Activity.RESULT_OK) {
//                    if (data != null) {
//                        try {
////Response model object holds the data which you get after the process completion.
//                            ResponseModel responseModel = (ResponseModel) data
//                                    .getSerializableExtra(Config.RESULT);
//
//
//                            AKycInfo = responseModel.getKycInfo();
//
//
//                            XmlToJson.Builder builder = new XmlToJson.Builder(AKycInfo);
//                            jsonObject = builder.build().toJson();
//                            Log.i("kjhkjhnl", responseModel.getAadhaarTs());
//                            JSONObject KycResobj = jsonObject.getJSONObject("KycRes");
//                            JSONObject UidDataobj = KycResobj.getJSONObject("UidData");
//                            Auid = UidDataobj.getString("uid");
//                            JSONObject poaobj = UidDataobj.getJSONObject("Poa");
//                            Adist = poaobj.getString("dist");
//                            Astate = poaobj.getString("state");
//                            Apincode = poaobj.getString("pc");
//                            JSONObject Poiobj = UidDataobj.getJSONObject("Poi");
//                            Aname = Poiobj.getString("name");
//
//                            String[] bits = Aname.split(" ");
//                            AlastName = bits[bits.length - 1];
//                            AfName = bits[0];
//                            Agender = Poiobj.getString("gender");
//                            Adob = Poiobj.getString("dob");
//                            promoteAdharRegister();
//
//                            Log.i("kjhkjhnl", responseModel.getAadhaarTs());
//
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//    }
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void setStatusBarGradiant(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            Drawable background = activity.getResources().getDrawable(R.drawable.bg_gradient_new);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
    }

}

