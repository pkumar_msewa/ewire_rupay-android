package in.MadMoveGlobal.fragment;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.CancellationSignal;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;

import in.MadMoveGlobal.custom.CustomFingerPrintSuccessDialog;


@RequiresApi(api = Build.VERSION_CODES.M)
public class FingerprintsHandlers extends FingerprintManager.AuthenticationCallback {


    private Context context;


    // Constructor
    public FingerprintsHandlers(Context mContext) {
        context = mContext;
    }


    public void startAuth(FingerprintManager manager, FingerprintManager.CryptoObject cryptoObject) {
        CancellationSignal cancellationSignal = new CancellationSignal();
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        manager.authenticate(cryptoObject, cancellationSignal, 0, this, null);
    }


    @Override
    public void onAuthenticationError(int errMsgId, CharSequence errString) {
        this.update("Fingerprint Authentication error\n" + errString, false);
    }


    @Override
    public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
        this.update("Fingerprint Authentication help\n" + helpString, false);
    }


    @Override
    public void onAuthenticationFailed() {
        this.update("Fingerprint Authentication failed.", false);
    }


    @Override
    public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
        this.update("Fingerprint Authentication succeeded.", true);
    }


    public void update(String e, Boolean success) {
        if (success) {
            showSuccessDialogmsg(e, success);

        }else{
            showFailourDialogmsg(e,success);
        }
    }

    public void showSuccessDialogmsg(String string, Boolean succeses) {
        CustomFingerPrintSuccessDialog builder = new CustomFingerPrintSuccessDialog(context, "Fingerprint authentication", string, succeses);
        /*builder.set("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });*/
        builder.show();
    }

    public void showFailourDialogmsg(String string, Boolean succeses) {
        CustomFingerPrintSuccessDialog builder = new CustomFingerPrintSuccessDialog(context, "Fingerprint authentication", string, succeses);
       /* builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });*/
        builder.show();
    }
}