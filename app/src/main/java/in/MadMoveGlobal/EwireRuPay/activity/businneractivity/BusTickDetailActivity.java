package in.MadMoveGlobal.EwireRuPay.activity.businneractivity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.HashMap;

//import in.paulpay.fragment.LoadMoneyFragment;
import in.MadMoveGlobal.model.BusBookedTicketModel;
import in.MadMoveGlobal.model.BusSeatModel;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.EwireRuPay.R;


/**
 * Created by Kashif-PC on 11/21/2016.
 */
public class BusTickDetailActivity extends AppCompatActivity {

    private LinearLayout llBusBookPassengers;
    private TextView MyBallance;
    private ImageButton ivLogo;
    private long desId, sourceId;
    private UserModel session = UserModel.getInstance();
    private HashMap<String, BusSeatModel> seatSelectedMap;
    private TextView tvTicketPnrNo, tvTravelsName, tvFromTo, tvJourneyDate, tvArrTime, tvDepTime, tvMobile, tvEmail, tvBoardingAddress, tvSourceCitys, tvDestinationCitys, tvDates, tvFare, tvSeat;
    private BusBookedTicketModel busBookedTicket;
    private double Fare, Fares = 0.0;
    private int k = 0;

    private String refNo = "12345";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_bus_booked_tickets);

        tvTicketPnrNo = (TextView) findViewById(R.id.tvTicketPnrNo);
        tvTravelsName = (TextView) findViewById(R.id.tvTravelsName);
        tvFromTo = (TextView) findViewById(R.id.tvFromTo);
        MyBallance = (TextView) findViewById(R.id.MyBallance);
        tvJourneyDate = (TextView) findViewById(R.id.tvJourneyDate);
        tvArrTime = (TextView) findViewById(R.id.tvArrTime);
        tvDepTime = (TextView) findViewById(R.id.tvDepTime);
        tvMobile = (TextView) findViewById(R.id.tvMobile);
        tvEmail = (TextView) findViewById(R.id.tvEmail);
        ivLogo = (ImageButton) findViewById(R.id.ivLogo);
        tvBoardingAddress = (TextView) findViewById(R.id.tvBoardingAddress);
        tvSourceCitys = (TextView) findViewById(R.id.tvSourceCitys);
        tvDestinationCitys = (TextView) findViewById(R.id.tvDestinationCitys);
        tvDates = (TextView) findViewById(R.id.tvDates);
        tvFare = (TextView) findViewById(R.id.tvFare);
        tvSeat = (TextView) findViewById(R.id.tvSeat);


        //press back button in toolbar

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbars);
        ImageButton ivBackBtns = (ImageButton) findViewById(R.id.ivBackBtns);
        ivBackBtns.setVisibility(View.VISIBLE);
        setSupportActionBar(toolbar);
        ivBackBtns.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


//        ivLogo.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                LoadMoneyFragment fragment = new LoadMoneyFragment();
//                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
//                fragmentTransaction.replace(R.id.container_body, fragment);
//                fragmentTransaction.addToBackStack(null);
//                fragmentTransaction.commit();
//            }
//        });


        llBusBookPassengers = (LinearLayout) findViewById(R.id.llBusBookPassengers);
        busBookedTicket = getIntent().getParcelableExtra("TicketValues");

        tvSourceCitys.setText(" " + busBookedTicket.getSource());
        tvDestinationCitys.setText(" " + busBookedTicket.getDestination());
        tvDates.setText(": " + busBookedTicket.getJourneyDate());
        tvTicketPnrNo.setText(" " + busBookedTicket.getTicketPnr());
        tvTravelsName.setText(": " + busBookedTicket.getBusOperator());
        tvFromTo.setText(": " + busBookedTicket.getSource() + " - " + busBookedTicket.getDestination());
        tvJourneyDate.setText(": " + busBookedTicket.getJourneyDate());
        tvArrTime.setText(": " + busBookedTicket.getArrTime());
        tvDepTime.setText(": " + busBookedTicket.getDeptTime());
        tvEmail.setText(": " + busBookedTicket.getUserEmail());
        tvMobile.setText(": " + busBookedTicket.getUserMobile());
        tvBoardingAddress.setText(": " + busBookedTicket.getBoardingAddress());
        MyBallance.setText(this.getResources().getString(R.string.rupease) + " " + session.getUserBalance());
        generateViewsForSeat();
    }

    private void generateViewsForSeat() {

        for (int i = 0; i < busBookedTicket.getBusPassengerList().size(); i++) {
            LayoutInflater layoutInflater = LayoutInflater.from(getApplicationContext());
            View passengerDetailView = layoutInflater.inflate(R.layout.view_book_passanger_detail, null);
            TextView tvBusBookSeatNo = (TextView) passengerDetailView.findViewById(R.id.tvBusBookSeatNo);
            llBusBookPassengers.addView(passengerDetailView);
            int j = i + 1;
            tvBusBookSeatNo.setText(String.valueOf(getResources().getString(R.string.Passenger)+ j));
            TextView tvPassName = (TextView) passengerDetailView.findViewById(R.id.tvPassName);
            tvPassName.setText(": " + busBookedTicket.getBusPassengerList().get(i).getfName() + " " + busBookedTicket.getBusPassengerList().get(i).getlName());
            TextView tvPassAge = (TextView) passengerDetailView.findViewById(R.id.tvPassAge);
            tvPassAge.setText(": " + busBookedTicket.getBusPassengerList().get(i).getAge());
            TextView tvPassGender = (TextView) passengerDetailView.findViewById(R.id.tvPassGender);
            tvPassGender.setText(": " + busBookedTicket.getBusPassengerList().get(i).getGender());
            TextView tvPassSeatNo = (TextView) passengerDetailView.findViewById(R.id.tvPassSeatNo);
            tvPassSeatNo.setText(": " + busBookedTicket.getBusPassengerList().get(i).getSeatNo());
            TextView tvPassSeatType = (TextView) passengerDetailView.findViewById(R.id.tvPassSeatType);
            tvPassSeatType.setText(": " + busBookedTicket.getBusPassengerList().get(i).getSeatType());
            TextView tvPassSeatFare = (TextView) passengerDetailView.findViewById(R.id.tvPassSeatFare);
            tvPassSeatFare.setText(": " + "\u20B9 " + busBookedTicket.getBusPassengerList().get(i).getFare() + "/-");
            Fare = Double.parseDouble(busBookedTicket.getBusPassengerList().get(i).getFare());
            Fares = Fares + Fare;
            k = k + 1;
        }
        tvFare.setText(" " + "\u20B9 " + Fares + "/-");
        tvSeat.setText(getResources().getString(R.string.Seats) + "\n" + k);
    }
}
