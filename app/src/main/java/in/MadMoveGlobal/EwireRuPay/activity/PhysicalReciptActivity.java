package in.MadMoveGlobal.EwireRuPay.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DecimalFormat;

import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.model.UserModel;

/**
 * Created by Ksf on 5/6/2016.
 */
public class PhysicalReciptActivity extends AppCompatActivity {

    private Button btnHome;
    private UserModel session = UserModel.getInstance();
    private LoadingDialog loadDlg;
    private Toolbar toolbar;
    private TextView tvBasicFee, tvShippingFee, tvCGST, tvSGST, tvIGST, tvSubTotal, tvRoundTotal;
    private ImageButton ivBackBtn;
    private int amount;
    private LinearLayout llCGST, llSGST, llIGST;
    private String a;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_physical_recipt);
        loadDlg = new LoadingDialog(PhysicalReciptActivity.this);

        tvBasicFee = (TextView) findViewById(R.id.tvBasicFee);
        tvShippingFee = (TextView) findViewById(R.id.tvShippingFee);
        tvCGST = (TextView) findViewById(R.id.tvCGST);
        tvSGST = (TextView) findViewById(R.id.tvSGST);
        tvIGST = (TextView) findViewById(R.id.tvIGST);
        tvSubTotal = (TextView) findViewById(R.id.tvSubTotal);
        tvRoundTotal = (TextView) findViewById(R.id.tvRoundTotal);
        btnHome = (Button) findViewById(R.id.btnHome);
        llCGST = (LinearLayout) findViewById(R.id.llCGST);
        llSGST = (LinearLayout) findViewById(R.id.llSGST);
        llIGST = (LinearLayout) findViewById(R.id.llIGST);
        loadDlg = new LoadingDialog(PhysicalReciptActivity.this);
        loadDlg.dismiss();

        Intent intent = getIntent();
//        String State = intent.getStringExtra("State");

        String State = "Karnataka";
        loadDlg.dismiss();
        if (State.equalsIgnoreCase("Karnataka")) {

            llIGST.setVisibility(View.GONE);
            llCGST.setVisibility(View.VISIBLE);
            llSGST.setVisibility(View.VISIBLE);

        } else {
            llIGST.setVisibility(View.VISIBLE);
            llCGST.setVisibility(View.GONE);
            llSGST.setVisibility(View.GONE);
        }


        amount = Integer.parseInt(session.getUsercardBaseFare());


//        a = "127";
        a = session.getUsercardBaseFare();
        double b = Double.parseDouble(a);
        double IGST = b * 0.18;
        double CGST = IGST / 2;
        double SGST = IGST / 2;
        double Total = b + IGST;
        DecimalFormat df = new DecimalFormat("#.##");
        String sSIGST = String.valueOf(df.format(IGST));
        String sCGST = String.valueOf(df.format(CGST));
        String sSGST = String.valueOf(df.format(SGST));
        String Totals = String.valueOf(df.format(Total));
        String Subtotal = String.valueOf(Math.round(Total));

        tvBasicFee.setText(this.getResources().getString(R.string.rupease) + " " + a);
        tvShippingFee.setText(this.getResources().getString(R.string.rupease) + " " + "0");
        tvIGST.setText(this.getResources().getString(R.string.rupease) + " " + sSIGST);
        tvCGST.setText(this.getResources().getString(R.string.rupease) + " " + sCGST);
        tvSGST.setText(this.getResources().getString(R.string.rupease) + " " + sSGST);
        tvSubTotal.setText(this.getResources().getString(R.string.rupease) + " " + Totals);
        tvRoundTotal.setText(this.getResources().getString(R.string.rupease) + " " + Subtotal);



        toolbar = (Toolbar) findViewById(R.id.toolbars);
        ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtns);
        setSupportActionBar(toolbar);
        ivBackBtn.setVisibility(View.VISIBLE);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent shoppingIntent = new Intent(PhysicalReciptActivity.this, MainActivity.class);
                startActivity(shoppingIntent);
                finish();
            }
        });


    }


    @Override
    public void onBackPressed() {


        Intent shoppingIntent = new Intent(PhysicalReciptActivity.this, MainActivity.class);
        startActivity(shoppingIntent);
        finish();

    }

}