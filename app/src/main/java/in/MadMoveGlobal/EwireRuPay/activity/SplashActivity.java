package in.MadMoveGlobal.EwireRuPay.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.KeyguardManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.hardware.fingerprint.FingerprintManager;
import android.media.FaceDetector;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.androidquery.AQuery;
import com.crashlytics.android.Crashlytics;
import com.orm.query.Select;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import in.MadMoveGlobal.custom.CustomAlertDialog;
import in.MadMoveGlobal.custom.CustomFingerPrintSuccessDialog;
import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.custom.FingerprintAuthenticationDialogFragment;
import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.fragment.FingerprintsHandlers;
import in.MadMoveGlobal.fragment.fragmentnaviagtionitems.HomeFragment;
import in.MadMoveGlobal.fragment.fragmentuser.LoginFragment;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.model.TutioralModel;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.util.ConnectionDetector;
import in.MadMoveGlobal.util.EncryptDecryptCheckUtility;
import in.MadMoveGlobal.util.GenerateAPIHeader;
import in.MadMoveGlobal.util.SecurityUtil;
import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.util.TLSSocketFactory;
import io.fabric.sdk.android.Fabric;

//import in.payqwik.payqwik.GooglePlayStoreAppVersionNameLoader;

//import in.payqwik.util.EncryptDecryptUserUtility;

/**
 * Created by Ksf on 8/13/2016.
 */
public class SplashActivity extends AppCompatActivity {
    private UserModel session = UserModel.getInstance();
    private List<UserModel> userArray;

    FaceDetector faceDetector;

    private static final String TAG = MainActivity.class.getSimpleName();

    private KeyGenerator keyGenerator;
    private SharedPreferences mSharedPreferences;

    private KeyguardManager keyguardManager;



    //For  now set it to true
    private TextView tvFingerprint;
    private ImageView gifImage;
    private boolean mPinActive;
    private ConnectionDetector connectionDetector;
    Boolean isInternetPresent = false;
    Boolean isResumed = false;

    private RequestQueue rq;
    private JSONObject jsonRequest;
    private ProgressBar pbSplashStatus;
    private TextView tvSplashStatus;
    private String versionName = "";

    private Cipher cipherNotInvalidated;
    private int versionCode;
    private String apiVersion = "";
    private boolean isVersion = true;
    private String cardNo, cvv, cardHolder, expiryDate;
    private LoadingDialog loadingDialog;



    private KeyStore keyStore;

    //Music Dailog
    SharedPreferences musicPreferences;
    private TutioralModel tutioralModel;

    //FingerPrint

    private static final String DIALOG_FRAGMENT_TAG = "myFragment";
    private static final String SECRET_MESSAGE = "Very secret message";
    private static final String KEY_NAME_NOT_INVALIDATED = "key_not_invalidated";
    static final String DEFAULT_KEY_NAME = "default_key";

    private KeyStore mKeyStore;
    private KeyGenerator mKeyGenerator;
    Cipher mCipher;
    String mKeyName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());

        setContentView(R.layout.activity_splash);
        musicPreferences = getSharedPreferences("ShowMusicDailog", Context.MODE_PRIVATE);
        ImageView ivBanner = (ImageView) findViewById(R.id.ivBanner);
        try {
            final javax.net.ssl.SSLSocketFactory sslSocketFactory = new TLSSocketFactory();
            rq = Volley.newRequestQueue(SplashActivity.this, new HurlStack(null, sslSocketFactory));
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        try {
            cipherNotInvalidated = Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES + "/"
                    + KeyProperties.BLOCK_MODE_CBC + "/"
                    + KeyProperties.ENCRYPTION_PADDING_PKCS7);
        }catch (Exception e){e.printStackTrace();}
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        tutioralModel = Select.from(TutioralModel.class).first();
        AQuery aQuery = new AQuery(SplashActivity.this);
//        aQuery.id(ivBanner).background(R.drawable.ic_dialogs).getContext();
        loadingDialog = new LoadingDialog(getApplicationContext());
        tvSplashStatus = (TextView) findViewById(R.id.tvSplashStatus);
        pbSplashStatus = (ProgressBar) findViewById(R.id.pbSplashStatus);
        connectionDetector = new ConnectionDetector(getApplicationContext());
        isInternetPresent = connectionDetector.isConnectingToInternet();
        gifImage = findViewById(R.id.gifImage);
        tvFingerprint = findViewById(R.id.tvFingerprint);

        ActivityCompat.requestPermissions(SplashActivity.this, new
                String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);

        setStatusBarGradiant(SplashActivity.this);


        tvFingerprint.setVisibility(View.GONE);
        gifImage.setVisibility(View.GONE);

//        getUserDetail();
        Log.i("SH256-NoH", ApiUrl.H_PREPAID + "qwertyuiop");

        Log.i("SH256", GenerateAPIHeader.getHeader(ApiUrl.H_PREPAID + "qwertyuiop"));

        firstProcess();
        // TODO: Move this to where you establish a user session
        logUser();


//
        //Music Dailog
//        SharedPreferences.Editor editor = musicPreferences.edit();
//        editor.clear();
//        editor.putBoolean("ShowMusicDailog", true);
//        editor.apply();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void setStatusBarGradiant(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            Drawable background = activity.getResources().getDrawable(R.drawable.bg_gradient_new);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
    }

    private void firstProcess() {
        connectionDetector = new ConnectionDetector(getApplicationContext());
        isInternetPresent = connectionDetector.isConnectingToInternet();
        if (isInternetPresent) {
            pbSplashStatus.setVisibility(View.VISIBLE);
            tvSplashStatus.setVisibility(View.VISIBLE);
    checkAppVersion();
//         workLoad(1000);
//
// checkLavaVersion();
        } else {
            pbSplashStatus.setVisibility(View.GONE);
            tvSplashStatus.setVisibility(View.GONE);
            showCustomDialog();
        }
    }

    private void checkAppVersion() {
        jsonRequest = new JSONObject();
        try {
            pbSplashStatus.setVisibility(View.VISIBLE);
            tvSplashStatus.setVisibility(View.VISIBLE);
            tvSplashStatus.setText("Checking app version, please wait.");

            PackageInfo pInfo = null;
            try {
                pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

            versionName = pInfo.versionName;
            jsonRequest = new JSONObject();
            Log.i("VerNo.", versionName);
            jsonRequest.put("versionName", versionName);
            jsonRequest.put("key", "DF25E45253D995EA2DE65244159BA8EA");

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {


            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_VER_CHECK, jsonRequest, new Response.Listener<JSONObject>() {
                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.i("VERSIONRESP", response.toString());
                        String code = response.getString("code");
                        String message = response.getString("message");
                        if (code != null & code.equals("V00")) {
                                if (!checkSession()) {
                                    pbSplashStatus.setVisibility(View.GONE);
                                    tvSplashStatus.setVisibility(View.GONE);
                                    Intent mainIntent = new Intent(SplashActivity.this, LoginRegActivity.class);
                                    startActivity(mainIntent);
                                    finish();
                                } else {

                                    keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
                                    Log.i("SessionSESSION", String.valueOf(checkSession()));

                                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                                        KeyguardManager km = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
                                        if (km.isKeyguardSecure()) {
                                            Intent authIntent;
                                            authIntent = km.createConfirmDeviceCredentialIntent("Unlock Ewire Rupay", "Confirm your screen lock pattern, PIN, password or fingerprint");
                                            startActivityForResult(authIntent, 0);
                                        }else {
                                            startActivity(new Intent(getApplicationContext(), LoginRegActivity.class));
                                        }
                                    }
                                }
                        } else if (code != null & code.equals("V04")) {
                            successDialog(message);
                        } else {
                            showOlderVersionDialog();
//                            tvSplashStatus.setText(message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            rq.add(postReq);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void startAuthentication(){
        try {
            mKeyStore = KeyStore.getInstance("AndroidKeyStore");
        } catch (KeyStoreException e) {
            throw new RuntimeException("Failed to get an instance of KeyStore", e);
        }
        try {
            mKeyGenerator = KeyGenerator
                    .getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new RuntimeException("Failed to get an instance of KeyGenerator", e);
        }
        Cipher defaultCipher;
        Cipher cipherNotInvalidated;
        try {
            defaultCipher = Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES + "/"
                    + KeyProperties.BLOCK_MODE_CBC + "/"
                    + KeyProperties.ENCRYPTION_PADDING_PKCS7);
            cipherNotInvalidated = Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES + "/"
                    + KeyProperties.BLOCK_MODE_CBC + "/"
                    + KeyProperties.ENCRYPTION_PADDING_PKCS7);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new RuntimeException("Failed to get an instance of Cipher", e);
        }
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        KeyguardManager keyguardManager = getSystemService(KeyguardManager.class);
        FingerprintManager fingerprintManager = getSystemService(FingerprintManager.class);

        createKey(DEFAULT_KEY_NAME, true);
        createKey(KEY_NAME_NOT_INVALIDATED, false);
        generatePopUp(cipherNotInvalidated, KEY_NAME_NOT_INVALIDATED);



        if (!keyguardManager.isKeyguardSecure()) {
            // Show a message that the user hasn't set up a fingerprint or lock screen.
            Toast.makeText(this,
                    "Secure lock screen hasn't set up.\n"
                            + "Go to 'Settings -> Security -> Fingerprint' to set up a fingerprint",
                    Toast.LENGTH_LONG).show();
            return;
        }

        // Now the protection level of USE_FINGERPRINT permission is normal instead of dangerous.
        // See http://developer.android.com/reference/android/Manifest.permission.html#USE_FINGERPRINT
        // The line below prevents the false positive inspection from Android Studio
        // noinspection ResourceType
        if (!fingerprintManager.hasEnrolledFingerprints()) {
            // This happens when no fingerprints are registered.
            Toast.makeText(this,
                    "Go to 'Settings -> Security -> Fingerprint' and register at least one" +
                            " fingerprint",
                    Toast.LENGTH_LONG).show();
            return;
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void generatePopUp(Cipher cipher, String keyName){
        mCipher = cipher;
        mKeyName = keyName;

        // Set up the crypto object for later. The object will be authenticated by use
        // of the fingerprint.
        if (initCipher(mCipher, mKeyName)) {

            // Show the fingerprint dialog. The user has the option to use the fingerprint with
            // crypto, or you can fall back to using a server-side verified password.
            FingerprintAuthenticationDialogFragment fragment
                    = new FingerprintAuthenticationDialogFragment();
            fragment.setCryptoObject(new FingerprintManager.CryptoObject(mCipher));
            boolean useFingerprintPreference = mSharedPreferences
                    .getBoolean(getString(R.string.use_fingerprint_to_authenticate_key),
                            true);
            if (useFingerprintPreference) {
                fragment.setStage(
                        FingerprintAuthenticationDialogFragment.Stage.FINGERPRINT);
            } else {
                fragment.setStage(
                        FingerprintAuthenticationDialogFragment.Stage.PASSWORD);
            }
            fragment.show(getFragmentManager(), DIALOG_FRAGMENT_TAG);
        } else {
            // This happens if the lock screen has been disabled or or a fingerprint got
            // enrolled. Thus show the dialog to authenticate with their password first
            // and ask the user if they want to authenticate with fingerprints in the
            // future
            FingerprintAuthenticationDialogFragment fragment
                    = new FingerprintAuthenticationDialogFragment();
            fragment.setCryptoObject(new FingerprintManager.CryptoObject(mCipher));
            fragment.setStage(
                    FingerprintAuthenticationDialogFragment.Stage.NEW_FINGERPRINT_ENROLLED);
            fragment.show(getFragmentManager(), DIALOG_FRAGMENT_TAG);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean initCipher(Cipher cipher, String keyName) {
        try {
            mKeyStore.load(null);
            SecretKey key = (SecretKey) mKeyStore.getKey(keyName, null);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            return true;
        } catch (KeyPermanentlyInvalidatedException e) {
            return false;
        } catch (KeyStoreException | CertificateException | UnrecoverableKeyException | IOException
                | NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException("Failed to init Cipher", e);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void onPurchased(boolean withFingerprint,
                            @Nullable FingerprintManager.CryptoObject cryptoObject) {
        if (withFingerprint) {
            // If the user has authenticated with fingerprint, verify that using cryptography and
            // then show the confirmation message.
            assert cryptoObject != null;
            tryEncrypt(cryptoObject.getCipher());
        } else {
            // Authentication happened with backup password. Just show the confirmation message.
            showConfirmation(null);
        }
    }

    // Show confirmation, if fingerprint was used show crypto information.
    private void showConfirmation(byte[] encrypted) {

        if (encrypted != null) {
            getUserDetail();
        }
    }


    private void tryEncrypt(Cipher cipher) {
        try {
            byte[] encrypted = cipher.doFinal(SECRET_MESSAGE.getBytes());
            showConfirmation(encrypted);
        } catch (BadPaddingException | IllegalBlockSizeException e) {
            Toast.makeText(this, "Failed to encrypt the data with the generated key. "
                    + "Retry the purchase", Toast.LENGTH_LONG).show();
            Log.e(TAG, "Failed to encrypt the data with the generated key." + e.getMessage());
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    public void createKey(String keyName, boolean invalidatedByBiometricEnrollment) {
        // The enrolling flow for fingerprint. This is where you ask the user to set up fingerprint
        // for your flow. Use of keys is necessary if you need to know if the set of
        // enrolled fingerprints has changed.
        try {
            mKeyStore.load(null);
            // Set the alias of the entry in Android KeyStore where the key will appear
            // and the constrains (purposes) in the constructor of the Builder

            KeyGenParameterSpec.Builder builder = new KeyGenParameterSpec.Builder(keyName,
                    KeyProperties.PURPOSE_ENCRYPT |
                            KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    // Require the user to authenticate with a fingerprint to authorize every use
                    // of the key
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7);

            // This is a workaround to avoid crashes on devices whose API level is < 24
            // because KeyGenParameterSpec.Builder#setInvalidatedByBiometricEnrollment is only
            // visible on API level +24.
            // Ideally there should be a compat library for KeyGenParameterSpec.Builder but
            // which isn't available yet.
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                builder.setInvalidatedByBiometricEnrollment(invalidatedByBiometricEnrollment);
            }
            mKeyGenerator.init(builder.build());
            mKeyGenerator.generateKey();
        } catch (NoSuchAlgorithmException | InvalidAlgorithmParameterException
                | CertificateException | IOException e) {
            throw new RuntimeException(e);
        }
    }


    @Override
    public void onBackPressed() {

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                //do something you want when pass the security
                getUserDetail();
            }else{
                startActivity(new Intent(getApplicationContext(), LoginRegActivity.class));
            }
        }
    }

    private boolean checkSession() {
        pbSplashStatus.setVisibility(View.VISIBLE);
        tvSplashStatus.setVisibility(View.VISIBLE);
        tvSplashStatus.setText("Checking session, please wait.");

        userArray = Select.from(UserModel.class).list();
        if (userArray.size() != 0) {
            try {
                //Decrypting sessionId
                session.setUserSessionId(userArray.get(0).getUserSessionId());

            } catch (Exception e) {
                e.printStackTrace();
            }
            session.setUserEmail(userArray.get(0).getUserEmail());
            session.setUserAcNo(userArray.get(0).getUserAcNo());
            session.setUserAcCode(userArray.get(0).getUserAcCode());
            session.setUserMobileNo(userArray.get(0).getUserMobileNo());
            session.setUserFirstName(userArray.get(0).getUserFirstName());
            session.setUserLastName(userArray.get(0).getUserLastName());
            session.setUserBalance(userArray.get(0).getUserBalance());
            session.setUserAddress(userArray.get(0).getUserAddress());
            session.setEmailIsActive(userArray.get(0).getEmailIsActive());
            session.setUserImage(userArray.get(0).getUserImage());
            session.setMPin(userArray.get(0).isMPin());
            session.setUserPoints(userArray.get(0).getUserPoints());
            mPinActive = session.isMPin;
            session.setIsValid(1);
            pbSplashStatus.setVisibility(View.GONE);
            tvSplashStatus.setVisibility(View.GONE);

            return true;
        } else {
            session.setIsValid(0);
            pbSplashStatus.setVisibility(View.GONE);
            tvSplashStatus.setVisibility(View.GONE);

            return false;
        }

    }


    public void showCustomDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(SplashActivity.this, R.string.dialog_title2, Html.fromHtml(generateNoInternetMessage()));

        builder.setPositiveButton("Setting", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                isResumed = true;
                dialog.dismiss();
                startActivity(new Intent(Settings.ACTION_SETTINGS));

            }
        });

        builder.setNegativeButton("Retry", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                firstProcess();
            }
        });
        builder.show();
    }

    public void showOlderVersionDialog() {
        try {
            CustomAlertDialog builder = new CustomAlertDialog(SplashActivity.this, R.string.dialog_title2, Html.fromHtml(generateOldVersionMessage()));
            builder.setCancelable(false);
            builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                    try {
                        Uri marketUri = Uri.parse("market://details?id=" + SplashActivity.this.getPackageName());
                        Intent marketIntent = new Intent(Intent.ACTION_VIEW, marketUri);
                        startActivity(marketIntent);
                    } catch (ActivityNotFoundException exception) {
                        exception.printStackTrace();
                    }
                }
            });
//            builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
//                public void onClick(DialogInterface dialog, int id) {
//                    dialog.dismiss();
//                    if (!checkSession()) {
//                        pbSplashStatus.setVisibility(View.GONE);
//                        tvSplashStatus.setVisibility(View.GONE);
//                        Intent mainIntent = new Intent(SplashActivity.this, LoginRegActivity.class);
//                        startActivity(mainIntent);
//                        finish();
//                    } else {
//                        getUserDetail();
//                    }
//                }
//            });
            builder.show();
        } catch (WindowManager.BadTokenException e) {
            e.printStackTrace();
        }
    }

    public String generateNoInternetMessage() {
        String source = "<b><font color=#000000> No internet connection.</font></b>" +
                "<br><br><b><font color=#ff0000> Please check your internet connection and try again.</font></b><br></br>";
        return source;
    }

    public String generateOldVersionMessage() {
        String source = "<b><font color=#000000> Current installed E-WIRE App version is outdated." + "</font></b>" +
                "<br><br><b><font color=#ff0000> Please update E-WIRE Card, to continue.</font></b><br></br>";
        return source;
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (isResumed) {
            workLoad(1000);
        }
        if (!isVersion) {
            showOlderVersionDialog();
        }

//       checkAppVersion();

    }

    private void workLoad(final int time) {
        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    Thread.sleep(time);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                runOnUiThread(new Runnable() {
                                  @Override
                                  public void run() {
                                      checkAppVersion();

                                      if (tutioralModel != null && tutioralModel.isCode()) {
                                          if (!checkSession()) {
                                              Log.i("SessionSESSION", String.valueOf(checkSession()));
                                              Intent mainIntent = new Intent(SplashActivity.this, LoginRegActivity.class);
//                                          overridePendingTransition(R.anim.right_to_left, 0);
                                              startActivity(mainIntent);
                                              finish();
                                          } else {
                                              Log.i("SessionSESSION", String.valueOf(checkSession()));

                                              getUserDetail();
                                          }
                                      } else {
                                          Intent mainIntent = new Intent(SplashActivity.this, LoginRegActivity.class);
//                                          overridePendingTransition(R.anim.right_to_left, 0);
                                          startActivity(mainIntent);
                                          finish();
                                      }
                                  }

                              }
                );
            }

        };
        thread.start();

    }


    public void getUserDetail() {
        pbSplashStatus.setVisibility(View.VISIBLE);
        tvSplashStatus.setVisibility(View.VISIBLE);
        tvSplashStatus.setText("Getting user details, please wait.");
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("GET USER PARAMS", jsonRequest.toString());
            Log.i("UserUrl", ApiUrl.URL_GET_USER_DETAILS);
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_GET_USER_DETAILS, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.i("GET USER RESPONSE", response.toString());
                        String code = response.getString("code");

                        if (code != null & code.equals("S00")) {

//                            String jsonString = response.getString("response");
//                            JSONObject jsonObject = new JSONObject(jsonString);

                            //New Implementation
                            JSONObject jsonDetail = response.getJSONObject("details");
                            boolean isImpsFixed = jsonDetail.getBoolean("isImpsFixed");
                            String impsCommissionAmt = jsonDetail.getString("impsCommissionAmt");
                            JSONObject accDetail = jsonDetail.getJSONObject("accountDetail");
                            double userBalanceD = accDetail.getDouble("balance");
                            String userBalance = String.valueOf(new DecimalFormat("#.##").format(userBalanceD));

//                            MainActivity.group =

                            long userAcNoL = accDetail.getLong("accountNumber");
                            String userAcNo = String.valueOf(userAcNoL);
                            int userPointsI = accDetail.getInt("points");
                            String userPoints = String.valueOf(userPointsI);


                            JSONObject accType = accDetail.getJSONObject("accountType");

                            String accName = accType.getString("name");
                            String accCode = accType.getString("code");


                            double accBalanceLimitD = accType.getDouble("balanceLimit");
                            double accMonthlyLimitD = accType.getDouble("monthlyLimit");
                            double accDailyLimitD = accType.getDouble("dailyLimit");

                            String accBalanceLimit = String.valueOf(accBalanceLimitD);
                            String accMonthlyLimit = String.valueOf(accMonthlyLimitD);
                            String accDailyLimit = String.valueOf(accDailyLimitD);

                            JSONObject jsonUserDetail = jsonDetail.getJSONObject("userDetail");
                            MainActivity.group = jsonUserDetail.getString("groupName");
                            String userFName = jsonUserDetail.getString("firstName");
                            String userLName = jsonUserDetail.getString("lastName");
                            String userMobile = jsonUserDetail.getString("contactNo");
                            String userEmail = jsonUserDetail.getString("email");
                            String userEmailStatus = jsonUserDetail.getString("emailStatus");

                            String userAddress = jsonUserDetail.getString("locality");
                            String userImage = jsonUserDetail.getString("image");
//                            boolean isMPin = jsonUserDetail.getBoolean("mpinPresent");

                            String userDob = jsonUserDetail.getString("dateOfBirth");
//                            String userGender = jsonUserDetail.getString("gender");
                            String encodedImage = response.getString("encodedImage");
                            boolean hasRefer = response.getBoolean("hasRefer");
                            boolean hasvertualcard = response.getBoolean("hasVirtualCard");
                            boolean hasphysicalCard = response.getBoolean("hasPhysicalCard");
                            boolean hasPhysicalRequest = response.getBoolean("hasPhysicalRequest");
                            String physicalCardStatus = response.getString("physicalCardStatus");
                            String cardMinBalance = response.getString("minimumCardBalance");
                            String cardFees = response.getString("cardFees");
                            String cardBaseFare = response.getString("cardBaseFare");
                            boolean haskycRequest = response.getBoolean("kycRequest");
                            String loadMoneyComm = response.getString("loadMoneyComm");
                            boolean PCStatus = response.getBoolean("physicalIsBlock");
                            boolean VCStatus = response.getBoolean("virtualIsBlock");
                            String virtualBlock = response.getString("virtualCardBlock");
                            String physicalBlock = response.getString("physicalCardBlock");


                            if (!response.isNull("cardDetails")) {
                                JSONObject cardDetails = response.getJSONObject("cardDetails");

                                cardNo = cardDetails.getString("walletNumber");
                                cvv = cardDetails.getString("cvv");
                                expiryDate = cardDetails.getString("expiryDate");
                                cardHolder = cardDetails.getString("holderName");
                            } else {
                                cardNo = "XXXXXXXXXXXXXXXX";
                                cvv = "XXX";
                                expiryDate = "XX/XX";
                                cardHolder = "XXXXXXXXXXXXXX";

                            }

                            UserModel.deleteAll(UserModel.class);

                            try {
                                //Encrypting sessionId
                                UserModel userModel = new UserModel(session.getUserSessionId(), userFName, userLName, userMobile, userEmail, userEmailStatus, 1, userAcNo, accName, accCode, userBalance, accBalanceLimit, accDailyLimit, accMonthlyLimit, userAddress, encodedImage, userDob, "", false, userPoints, hasRefer, hasvertualcard, hasphysicalCard, cardNo, cvv, expiryDate, cardHolder, hasPhysicalRequest, physicalCardStatus, cardMinBalance, cardFees, haskycRequest,loadMoneyComm,cardBaseFare,PCStatus, VCStatus, virtualBlock, physicalBlock,impsCommissionAmt,isImpsFixed);
                                userModel.save();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            session.setUserFirstName(userFName);
                            session.setUserLastName(userLName);
                            session.setUserMobileNo(userMobile);
                            session.setUserEmail(userEmail);
                            session.setUserAddress(userAddress);
                            session.setEmailIsActive(userEmailStatus);
                            session.setIsImpsFixed(isImpsFixed);
                            session.setImpsCommissionAmt(impsCommissionAmt);
                            session.setUserAcCode(accCode);
                            session.setUserAcNo(userAcNo);
                            session.setUserBalance(userBalance);
                            session.setUserAcName(accName);
                            session.setUserMonthlyLimit(accMonthlyLimit);
                            session.setUserDailyLimit(accDailyLimit);
                            session.setUserBalanceLimit(accBalanceLimit);
                            session.setUserImage(encodedImage);
                            session.setUserAddress(userAddress);
//                            session.setUserGender(userGender);
                            session.setUserDob(userDob);

                            session.setUsercardNo(cardNo);
                            session.setUsercvv(cvv);
                            session.setUserexpiryDate(expiryDate);
                            session.setUsercardHolder(cardHolder);
                            session.setUserminimumCardBalance(cardMinBalance);
                            session.setUsercardFees(cardFees);
                            session.setUsercardBaseFare(cardBaseFare);

//                            session.setMPin(isMPin);
                            session.setIsValid(1);
                            session.setUserPoints(userPoints);
                            session.setHasRefer(hasRefer);
                            session.setHasVcard(hasvertualcard);
                            session.setHasPcard(hasphysicalCard);
                            session.setHasPhysicalRequest(hasPhysicalRequest);
                            session.setPhysicalCardStatus(physicalCardStatus);
                            session.setHaskycRequest(haskycRequest);
                            session.setloadMoneyComm(loadMoneyComm);
                            session.setPCStatus(PCStatus);
                            session.setVCStatus(VCStatus);
                            session.setUserVirtualBlock(virtualBlock);
                            session.setUserPhysicalBlock(physicalBlock);
//                            mPinActive = isMPin;
                            Log.i("uSER dETAILS", " uSER dETAILS");


//                            if (mPinActive) {
//                                pbSplashStatus.setVisibility(View.GONE);
//                                tvSplashStatus.setVisibility(View.GONE);
//
//                                Intent mainIntent = new Intent(SplashActivity.this, MainActivity.class);
//                                startActivity(mainIntent);
////                                Intent mainIntent = new Intent(SplashActivity.this, VerifyMPinNewActivity.class);
////                                startActivity(mainIntent);
//                                finish();
//                            } else {
//                                pbSplashStatus.setVisibility(View.GONE);
//                                tvSplashStatus.setVisibility(View.GONE);

                            Intent mainIntent = new Intent(SplashActivity.this, MainActivity.class);
                            startActivity(mainIntent);
                            finish();
//                            }

                        } else if (code.equals("F03")) {
                            CustomToast.showMessage(SplashActivity.this, "Please login and try again");
                            promoteLogout();
                        } else {
//                            if (mPinActive) {
//                                pbSplashStatus.setVisibility(View.GONE);
//                                tvSplashStatus.setVisibility(View.GONE);
//
//                                Intent mainIntent = new Intent(SplashActivity.this, VerifyMPinNewActivity.class);
//                                startActivity(mainIntent);
//                                finish();
//                            } else {
                            pbSplashStatus.setVisibility(View.GONE);
                            tvSplashStatus.setVisibility(View.GONE);

                            Intent mainIntent = new Intent(SplashActivity.this, MainActivity.class);
                            startActivity(mainIntent);
                            finish();
//                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
//                        if (mPinActive) {
//                            pbSplashStatus.setVisibility(View.GONE);
//                            tvSplashStatus.setVisibility(View.GONE);
//
//                            Intent mainIntent = new Intent(SplashActivity.this, VerifyMPinNewActivity.class);
//                            startActivity(mainIntent);
//                            finish();
//                        } else {
                        pbSplashStatus.setVisibility(View.GONE);
                        tvSplashStatus.setVisibility(View.GONE);

                        Intent mainIntent = new Intent(SplashActivity.this, LoginRegActivity.class);
                        startActivity(mainIntent);
                        finish();
//                        }

                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
//                    if (mPinActive) {
//                        pbSplashStatus.setVisibility(View.GONE);
//                        tvSplashStatus.setVisibility(View.GONE);
//
//                        Intent mainIntent = new Intent(SplashActivity.this, VerifyMPinNewActivity.class);
//                        startActivity(mainIntent);
//                        finish();
//                    } else {
                    pbSplashStatus.setVisibility(View.GONE);
                    tvSplashStatus.setVisibility(View.GONE);

                    Intent mainIntent = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(mainIntent);
                    finish();
//                    }


                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hash", "1234");
                    return map;
                }

            };

            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            rq.add(postReq);
        }
    }


    private String getAppVersion() {
        try {
            PackageInfo packageInfo = getPackageManager()
                    .getPackageInfo(getPackageName(), 0);
            return packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    private void promoteLogout() {
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_LOGOUT, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObj) {
                    try {
                        String code = jsonObj.getString("code");
                        if (code != null && code.equals("S00")) {
                            UserModel.deleteAll(UserModel.class);
                            loadingDialog.dismiss();
                            startActivity(new Intent(getApplicationContext(), LoginRegActivity.class));
                            finish();
                        } else {
                            loadingDialog.dismiss();
                            UserModel.deleteAll(UserModel.class);
                            loadingDialog.dismiss();
                            startActivity(new Intent(getApplicationContext(), LoginRegActivity.class));
                            finish();
                        }
                    } catch (JSONException e) {
                        loadingDialog.dismiss();
                        CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception2));
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadingDialog.dismiss();
                    CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception));
                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            rq.add(postReq);
        }
    }


    public static final int compareVersions(String ver1, String ver2) {

        String[] vals1 = ver1.split("\\.");
        String[] vals2 = ver2.split("\\.");
        int i = 0;
        while (i < vals1.length && i < vals2.length && vals1[i].equals(vals2[i])) {
            i++;
        }

        if (i < vals1.length && i < vals2.length) {
            int diff = Integer.valueOf(vals1[i]).compareTo(Integer.valueOf(vals2[i]));
            return diff < 0 ? -1 : diff == 0 ? 0 : 1;
        }

        return vals1.length < vals2.length ? -1 : vals1.length == vals2.length ? 0 : 1;
    }

    private void checkLavaVersion() {
        jsonRequest = new JSONObject();
        try {
            pbSplashStatus.setVisibility(View.VISIBLE);
            tvSplashStatus.setVisibility(View.VISIBLE);
            tvSplashStatus.setText("Checking app version, please wait.");

            jsonRequest = new JSONObject();
            jsonRequest.put("device", android.os.Build.DEVICE);
            jsonRequest.put("imei", SecurityUtil.getIMEI(SplashActivity.this));
            jsonRequest.put("model", android.os.Build.MODEL);

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, "https://www.vpayqwik.com/ws/api/partner/registerDevice", jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        boolean status = response.getBoolean("success");
                        if (status) {
 //                           checkAppVersion();
                        } else {
//                            CustomToast.showMessage(SplashActivity.this, response.getString("message"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
//                    if (!checkSession()) {
//                        pbSplashStatus.setVisibility(View.GONE);
//                        tvSplashStatus.setVisibility(View.GONE);
//                        Intent mainIntent = new Intent(SplashActivity.this, LoginRegActivity.class);
//                        startActivity(mainIntent);
//                        finish();
//                    } else {
//                        getUserDetail();
//                    }
                    error.printStackTrace();
//                    CustomToast.showMessage(getApplicationContext(), Ne);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> stringStringHashMap = new HashMap<>();
                    stringStringHashMap.put("apiKey", "654D0FE324466FC80F934BDD848AA087");
                    return stringStringHashMap;
                }
            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            rq.add(postReq);
        }
    }

    public void successDialog(String message) {
        CustomAlertDialog builder = new CustomAlertDialog(this, R.string.dialog_title2, Html.fromHtml(message));
        builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                try {
                    Uri marketUri = Uri.parse("market://details?id=" + SplashActivity.this.getPackageName());
                    Intent marketIntent = new Intent(Intent.ACTION_VIEW, marketUri);
                    startActivity(marketIntent);
                } catch (ActivityNotFoundException exception) {
                    exception.printStackTrace();
                }


            }
        });
        builder.setNegativeButton("Decline", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Intent mainIntent = new Intent(SplashActivity.this, LoginRegActivity.class);
                startActivity(mainIntent);
                finish();
            }
        });
        builder.show();
    }

    private void logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        Crashlytics.setUserIdentifier(session.getUserMobileNo());
        Crashlytics.setUserEmail(session.getUserEmail());
        Crashlytics.setUserName(session.getUserFirstName() + " " + session.getUserLastName());
    }






}
