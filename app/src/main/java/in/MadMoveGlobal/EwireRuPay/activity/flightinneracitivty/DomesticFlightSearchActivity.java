package in.MadMoveGlobal.EwireRuPay.activity.flightinneracitivty;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;

import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.ArrayList;

import in.MadMoveGlobal.adapter.DomesticCityListAdapter;
import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.custom.ResultIPC;
import in.MadMoveGlobal.model.DomesticFlightModel;
import in.MadMoveGlobal.EwireRuPay.R;


/**
 * Created by Ksf on 10/6/2016.
 */
public class DomesticFlightSearchActivity extends AppCompatActivity {
    private ListView lvSearchFlightCity;
    private MaterialEditText etSearchFlightCity;
    private LoadingDialog loadingDialog;
    private ArrayList<DomesticFlightModel> flightCityModelList;
    private DomesticCityListAdapter domesticCityListAdapter;
    //    private FetchCityTask fetchCityTask;
    private String searchType;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_flight_airport_search);
        searchType = getIntent().getStringExtra("searchType");
        ResultIPC resultIPC = ResultIPC.get();
        int sys = getIntent().getIntExtra("city", 0);
        Log.i(";length", String.valueOf(sys));

        flightCityModelList = (ArrayList<DomesticFlightModel>) resultIPC.getaddOnsFlight(sys);
//        if (flightCityModelList.size() != 0) {
//            Log.i("fff", flightCityModelList.get(0).getAirportCode());
//        }getAirportCode
        //press back button in toolbar
        loadingDialog = new LoadingDialog(DomesticFlightSearchActivity.this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbars);
        ImageButton ivBackBtns = (ImageButton) findViewById(R.id.ivBackBtns);
        ivBackBtns.setVisibility(View.VISIBLE);
        setSupportActionBar(toolbar);
        ivBackBtns.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        etSearchFlightCity = (MaterialEditText) findViewById(R.id.etSearchFlightCity);
        lvSearchFlightCity = (ListView) findViewById(R.id.lvSearchFlightCity);

//        flightCityModelList = new ArrayList<>();
        domesticCityListAdapter = new DomesticCityListAdapter(DomesticFlightSearchActivity.this, flightCityModelList);
        lvSearchFlightCity.setAdapter(domesticCityListAdapter);


        lvSearchFlightCity.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Intent intent = new Intent("flight-search-complete");
                DomesticFlightModel domesticFlightModel=(DomesticFlightModel) adapterView.getItemAtPosition(i);
                intent.putExtra("selectedCityModel", domesticFlightModel);
                intent.putExtra("searchType", searchType);
                LocalBroadcastManager.getInstance(DomesticFlightSearchActivity.this).sendBroadcast(intent);
                loadingDialog.dismiss();
                finish();
            }
        });

        etSearchFlightCity.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                if (cs.length() >1) {
//                    searchCity(String.valueOf(cs));
                    DomesticFlightSearchActivity.this.domesticCityListAdapter.getFilter().filter(cs);;
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });

    }

//    private class FetchCityTask extends AsyncTask<String, Void, ArrayList<DomesticFlightModel>> {
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            loadingDialog.show();
//            flightCityModelList.clear();
//        }
//
//        @Override
//        protected ArrayList<DomesticFlightModel> doInBackground(String... selectedCityString) {
////            String mainJson = loadJSONFromAsset();
////            try {
////                JSONArray response = new JSONArray(mainJson);
////                for (int i = 0; i < response.length(); i++) {
////                    JSONObject c = response.getJSONObject(i);
////                    String airportCode = c.getString("AirportCode");
////                    String cityName = c.getString("City");
////                    String countryName = c.getString("Country");
////                    String airportDesc = c.getString("AirportDesc");
////                    String airportType = c.getString("Type");
//
////                    if ((cityName.toUpperCase()).contains(selectedCityString[0].toUpperCase()) || (airportCode.toUpperCase()).contains(selectedCityString[0].toUpperCase())) {
////                        DomesticFlightModel busModel = new DomesticFlightModel(airportCode, cityName, countryName, airportDesc, airportType);
////                        flightCityModelList.add(busModel);
////                    }
////                }
//
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//            return flightCityModelList;
//        }
//
//        @Override
//        protected void onPostExecute(ArrayList<DomesticFlightModel> result) {
//            loadingDialog.dismiss();
//            if (result != null && result.size() != 0) {
//                domesticCityListAdapter.notifyDataSetChanged();
//            } else {
//                flightCityModelList.clear();
//                domesticCityListAdapter.notifyDataSetChanged();
//            }
//
//        }
//    }

//    public String loadJSONFromAsset() {
//        String json;
//        try {
//            InputStream is = getAssets().open("airport.json");
//            int size = is.available();
//            byte[] buffer = new byte[size];
//            is.read(buffer);
//            is.close();
//            json = new String(buffer, "UTF-8");
//        } catch (IOException ex) {
//            ex.printStackTrace();
//            return null;
//        }
//        return json;
//    }

    private void searchCity(String cityValue) {
//        fetchCityTask = new FetchCityTask();
//        fetchCityTask.execute(cityValue);
    }

    @Override
    protected void onDestroy() {
//        if (fetchCityTask != null && !fetchCityTask.isCancelled()) {
//            fetchCityTask.cancel(true);
//        }
        super.onDestroy();

    }
}

