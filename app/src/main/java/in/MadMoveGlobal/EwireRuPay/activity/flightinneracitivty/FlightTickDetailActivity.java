package in.MadMoveGlobal.EwireRuPay.activity.flightinneracitivty;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import in.MadMoveGlobal.adapter.FlightDetailAdapter;
import in.MadMoveGlobal.custom.NonScrollListView;
import in.MadMoveGlobal.custom.ResultIPC;
import in.MadMoveGlobal.model.BusBookedTicketModel;
import in.MadMoveGlobal.model.BusSeatModel;
import in.MadMoveGlobal.model.DomesticFlightModel;
import in.MadMoveGlobal.model.FlightModel;
import in.MadMoveGlobal.EwireRuPay.R;


/**
 * Created by Kashif-PC on 11/21/2016.
 */
public class FlightTickDetailActivity extends AppCompatActivity {

    private LinearLayout llBusBookPassengers;

    private long desId, sourceId;

    private HashMap<String, BusSeatModel> seatSelectedMap;
    private TextView tvTicketPnrNo, tvTravelsName, tvFromTo, tvJourneyDate, tvArrTime, tvDepTime, tvMobile, tvEmail, tvBoardingAddress;
    private BusBookedTicketModel busBookedTicket;

    private String refNo = "12345";
    private NonScrollListView lvListFlightDetail;
    private NonScrollListView lvListFlightRoundTrip;
    private ArrayList<FlightModel> flightArray, flightArray2;
    private int citylist;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_flight_booked_tickets);

        tvTicketPnrNo = (TextView) findViewById(R.id.tvTicketPnrNo);
        tvTravelsName = (TextView) findViewById(R.id.tvTravelsName);
        tvFromTo = (TextView) findViewById(R.id.tvFromTo);
        lvListFlightDetail = (NonScrollListView) findViewById(R.id.lvListFlightDetail);
        lvListFlightRoundTrip = (NonScrollListView) findViewById(R.id.lvListFlightRoundTrip);
        tvJourneyDate = (TextView) findViewById(R.id.tvJourneyDate);
        tvArrTime = (TextView) findViewById(R.id.tvArrTime);
        tvDepTime = (TextView) findViewById(R.id.tvDepTime);
        tvMobile = (TextView) findViewById(R.id.tvMobile);
        tvEmail = (TextView) findViewById(R.id.tvEmail);
        flightArray = new ArrayList<>();
        flightArray2 = new ArrayList<>();

        //press back button in toolbar

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbars);
        ImageButton ivBackBtns = (ImageButton) findViewById(R.id.ivBackBtns);
        ivBackBtns.setVisibility(View.VISIBLE);
        setSupportActionBar(toolbar);
        ivBackBtns.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        llBusBookPassengers = (LinearLayout) findViewById(R.id.llBusBookPassengers);
        busBookedTicket = getIntent().getParcelableExtra("TicketValues");
        citylist = getIntent().getIntExtra("citylist", 0);
        ArrayList<DomesticFlightModel> resultIPC = ResultIPC.get().getaddOnsFlight(citylist);
        Log.i("bvalues", busBookedTicket.getOneway());
        if (!busBookedTicket.getOneway().equalsIgnoreCase("[]")) {
            try {
                JSONArray jsonArray = new JSONArray(busBookedTicket.getOneway());
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject d = jsonArray.getJSONObject(i);
                    String depAirportCode = d.getString("origin");
                    String depTime = d.getString("departureTime");
                    String arrivalAirportCode = d.getString("destination");
                    String arrivalTime = d.getString("arrivalTime");

                    String flightDuration = d.getString("duration");
                    String flightName = d.getString("airlineName");
                    String flightNo = d.getString("flightNumber");
                    String flightImage = d.getString("airlineName");
                    String flightCode = "";
                    String flightArrivalTimeZone = d.getString("arrivalDate");
                    String flightDepartureTimeZone = d.getString("departureDate");

                    String flightArrivalAirportName = d.getString("destination");
                    String flightDepartureAirportName = d.getString("origin");
                    String flightTpe = "";
                    String flightRule = "";

                    FlightModel flightModel = new FlightModel(depAirportCode, depTime, arrivalAirportCode, arrivalTime, flightDuration, flightName,
                            flightNo, flightImage, flightRule, flightCode, flightArrivalTimeZone, flightDepartureTimeZone, flightArrivalAirportName, flightDepartureAirportName, 1, flightTpe, "", "");
                    flightArray.add(flightModel);
                }
                if (flightArray.size() != 0) {

                    try {
                        FlightDetailAdapter flightDetailAdapter1 = new FlightDetailAdapter(getApplicationContext(), flightArray, resultIPC);
                        lvListFlightDetail.setAdapter(flightDetailAdapter1);
                    } catch (NullPointerException e) {

                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        if (!busBookedTicket.getRoundway().equalsIgnoreCase("[]")) {
            try {
                JSONArray jsonArray = new JSONArray(busBookedTicket.getOneway());
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject d = jsonArray.getJSONObject(i);
                    String depAirportCode = d.getString("origin");
                    String depTime = d.getString("departureTime");
                    String arrivalAirportCode = d.getString("destination");
                    String arrivalTime = d.getString("arrivalTime");

                    String flightDuration = d.getString("duration");
                    String flightName = d.getString("airlineName");
                    String flightNo = d.getString("flightNumber");
                    String flightImage = d.getString("airlineName");
                    String flightCode = "";
                    String flightArrivalTimeZone = d.getString("arrivalDate");
                    String flightDepartureTimeZone = d.getString("departureDate");

                    String flightArrivalAirportName = d.getString("destination");
                    String flightDepartureAirportName = d.getString("origin");
                    String flightTpe = "";
                    String flightRule = "";
                    FlightModel flightModel = new FlightModel(depAirportCode, depTime, arrivalAirportCode, arrivalTime, flightDuration, flightName,
                            flightNo, flightImage, flightRule, flightCode, flightArrivalTimeZone, flightDepartureTimeZone, flightArrivalAirportName, flightDepartureAirportName, 1, flightTpe, "", "");
                    flightArray2.add(flightModel);
                }
                if (flightArray2.size() != 0) {
                    try {
                        FlightDetailAdapter flightDetailAdapter1 = new FlightDetailAdapter(getApplicationContext(), flightArray2, resultIPC);
                        lvListFlightRoundTrip.setAdapter(flightDetailAdapter1);
                    } catch (NullPointerException e) {

                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }


        tvTicketPnrNo.setText(": " + busBookedTicket.getTicketPnr());
        tvTravelsName.setText(": " + busBookedTicket.getBusPassengerList().get(0).getfName());
        tvFromTo.setText(": " + busBookedTicket.getSource() + " - " + busBookedTicket.getDestination());
        tvJourneyDate.setText(": " + busBookedTicket.getJourneyDate());
        tvArrTime.setText(": " + busBookedTicket.getArrTime());
        tvDepTime.setText(": " + busBookedTicket.getDeptTime());
        tvEmail.setText(": " + busBookedTicket.getUserEmail());
        tvMobile.setText(": " + busBookedTicket.getUserMobile());
        generateViewsForSeat();
    }

    private void generateViewsForSeat() {
        for (int i = 0; i < busBookedTicket.getBusPassengerList().size(); i++) {
            LayoutInflater layoutInflater = LayoutInflater.from(getApplicationContext());
            View passengerDetailView = layoutInflater.inflate(R.layout.view_flight_book_passanger_detail, null);
            TextView tvBusBookSeatNo = (TextView) passengerDetailView.findViewById(R.id.tvBusBookSeatNo);
            llBusBookPassengers.addView(passengerDetailView);
            int j = i + 1;
            tvBusBookSeatNo.setText(String.valueOf("Passenger " + j));
            TextView tvPassName = (TextView) passengerDetailView.findViewById(R.id.tvPassName);
            tvPassName.setText(": " + busBookedTicket.getBusPassengerList().get(i).getfName() + " " + busBookedTicket.getBusPassengerList().get(i).getlName());
            Log.i("NAME", busBookedTicket.getBusPassengerList().get(i).getfName() + " " + busBookedTicket.getBusPassengerList().get(i).getlName());
            TextView tvPassGender = (TextView) passengerDetailView.findViewById(R.id.tvPassGender);
            tvPassGender.setText(": " + busBookedTicket.getBusPassengerList().get(i).getGender());
            Log.i("GENDER", "Gender: " + busBookedTicket.getBusPassengerList().get(i).getGender());
            TextView tvPassSeatNo = (TextView) passengerDetailView.findViewById(R.id.tvPassSeatNo);
            tvPassSeatNo.setText(": " + busBookedTicket.getBusPassengerList().get(i).getSeatNo());
            Log.i("SEATNO", "Seat No: " + busBookedTicket.getBusPassengerList().get(i).getSeatNo());
            TextView tvPassSeatType = (TextView) passengerDetailView.findViewById(R.id.tvPassSeatType);
            tvPassSeatType.setText(": " + busBookedTicket.getBusPassengerList().get(i).getSeatType());
            Log.i("SEATTYPE", "seat Type: " + busBookedTicket.getBusPassengerList().get(i).getSeatType());
            TextView tvPassSeatFare = (TextView) passengerDetailView.findViewById(R.id.tvPassSeatFare);
            tvPassSeatFare.setText(": " + "\u20B9 " + busBookedTicket.getTotalFare() + "/-");

        }

    }
}
