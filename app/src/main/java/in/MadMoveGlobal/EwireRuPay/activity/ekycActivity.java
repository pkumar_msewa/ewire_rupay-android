package in.MadMoveGlobal.EwireRuPay.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

//import com.example.ekycsdkdemo.Config;
//import com.example.ekycsdkdemo.WebViewActivity;
//import com.example.ekycsdkdemo.mantra.model.EkycData;
//import com.example.ekycsdkdemo.mantra.model.ResponseModel;

import org.json.JSONObject;

import in.MadMoveGlobal.custom.CustomAlertDialog;
import in.MadMoveGlobal.EwireRuPay.R;

public class ekycActivity extends AppCompatActivity {

    private String uid,pincode,name,gender,dob, KycInfo,fName,lastName,dist,state, AadhaarTxnId, CustomData9, CustomData8, Status, AadhaarTs, ErrorCode, TxnId, Iv, ErrorMessage,
            CustomData10, TransType, CustomData5, CustomData4, CustomData7, CustomData6, CustomData1;
    private JSONObject jsonObject;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ekyc);
        setStatusBarGradiant(ekycActivity.this);

//        EkycData ekycData = new EkycData();
//        ekycData.setTransactionType("E");
//        ekycData.setTransactionID("paulpay123");
//        ekycData.setAadharNumber("");
//        ekycData.setDeviceType("Mantra(MFS100)");
//        ekycData.setKey("eba52eb1831f59390033882f4e3c4811");
//        ekycData.setLicKey("BH0M08UB1P5JMDwqXlICdwfuzWNlSrP7MFRjCDmJ+2+1SjybgDQS8JK7DI3Ifr/tn4SWW2EmfO++yobrlKxiu+emD/g3hVSiFPxk8uWdQmn/La/BTlx7wiM7OV1O1jhVQdmfkolGcQBBL0XejMZR53GZhsDA7G1h1IuVu6X4nlA=");
//
//        Intent ekycIntent = new Intent(ekycActivity.this, WebViewActivity.class);
//        ekycIntent.putExtra(Config.ARGUMENT_DATA, ekycData);
//        startActivityForResult(ekycIntent, Config.REQUEST_CODE);

    }

//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == Config.REQUEST_CODE) {
//            try {
//                if (resultCode == Activity.RESULT_OK) {
//                    if (data != null) {
//                        try {
////Response model object holds the data which you get after the process completion.
//                            ResponseModel responseModel = (ResponseModel) data
//                                    .getSerializableExtra(Config.RESULT);
//
//
//                            KycInfo = responseModel.getKycInfo();
//
//
//                            XmlToJson.Builder builder = new XmlToJson.Builder(KycInfo);
//                            jsonObject = builder.build().toJson();
//                            JSONObject KycResobj = jsonObject.getJSONObject("KycRes");
//                            JSONObject UidDataobj= KycResobj.getJSONObject("UidData");
//                            uid = UidDataobj.getString("uid");
//                            JSONObject poaobj = UidDataobj.getJSONObject("Poa");
//
//                            dist = poaobj.getString("dist");
//                            state = poaobj.getString("state");
//                            pincode = poaobj.getString("pc");
//                            JSONObject Poiobj = UidDataobj.getJSONObject("Poi");
//                            name = Poiobj.getString("name");
//
//                            String[] bits = name.split(" ");
//                            lastName = bits[bits.length - 1];
//                            fName = bits[0];
//                            gender = Poiobj.getString("gender");
//                            dob = Poiobj.getString("dob");
//
//
//
//                            log(jsonObject.toString());
//                            AadhaarTxnId = responseModel.getAadhaarTxnId();
//                            CustomData9 = responseModel.getCustomData9();
//                            CustomData8 = responseModel.getCustomData8();
//                            Status = responseModel.getStatus();
//                            AadhaarTs = responseModel.getAadhaarTs();
//                            ErrorCode = responseModel.getErrorCode();
//                            TxnId = responseModel.getTxnId();
//                            Iv = responseModel.getIv();
//                            ErrorMessage = responseModel.getErrorMessage();
//                            CustomData10 = responseModel.getCustomData10();
//                            TransType = responseModel.getTransType();
//                            CustomData5 = responseModel.getCustomData5();
//                            CustomData4 = responseModel.getCustomData4();
//                            CustomData7 = responseModel.getCustomData7();
//                            CustomData6 = responseModel.getCustomData6();
//                            CustomData1 = responseModel.getCustomData1();
//
//                            showCustomDialog();
//
//
//
//
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//    }

    public void showCustomDialog() {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(generateMessage(), Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(generateMessage());
        }
        CustomAlertDialog builder = new CustomAlertDialog(ekycActivity.this, R.string.dialog_title2, result);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
//                getResources().getInteger(R.integer.transactionValue)

            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public String generateMessage() {


        return "<b><font color=#000000> uid: </font></b>" + "<font color=#000000>" + uid + "</font><br>" +
                "<b><font color=#000000> pincode: </font></b>" + "<font>" + pincode + "</font><br>" +
                "<b><font color=#000000> name: </font></b>" + "<font color=#000000>" + name + "</font><br>" +
                "<b><font color=#000000> gender: </font></b>" + "<font>" + gender + "</font><br>" +
                "<b><font color=#000000> dob: </font></b>" + "<font color=#000000>" + dob + "</font><br>" +
                "<b><font color=#000000> First Name: </font></b>" + "<font>" + fName + "</font><br>" +
                "<b><font color=#000000> Last Name: </font></b>" + "<font color=#000000>" + lastName + "</font><br>" +
                "<b><font color=#000000> Address: </font></b>" + "<font color=#000000>" + dist +", "+  state + "</font><br>" +
                "<b><font color=#ff0000> Are you sure you want to proceed?</font></b><br>";
    }

    public void log(String message) {
        // Split by line, then ensure each line can fit into Log's maximum length.
        for (int i = 0, length = message.length(); i < length; i++) {
            int newline = message.indexOf('\n', i);
            newline = newline != -1 ? newline : length;
            do {
                int end = Math.min(newline, i + 40000);
                Log.d("OkHttp", message.substring(i, end));
                i = end;
            } while (i < newline);
        }
    }
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void setStatusBarGradiant(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            Drawable background = activity.getResources().getDrawable(R.drawable.bg_gradient_new);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
    }
}
