package in.MadMoveGlobal.EwireRuPay.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import in.MadMoveGlobal.fragment.fragmentgiftcard.GiftCardListFragment;
import in.MadMoveGlobal.EwireRuPay.R;

//import in.payqwik.payqwik.GooglePlayStoreAppVersionNameLoader;

//import in.payqwik.util.EncryptDecryptUserUtility;

/**
 * Created by Ksf on 8/13/2016.
 */
public class GCIActivity extends AppCompatActivity {

    private Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gci);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        ImageButton ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtn);
        ImageButton ivLogo = (ImageButton) findViewById(R.id.ivLogo);
        TextView mybalance = (TextView) findViewById(R.id.MyBallance);
        ivBackBtn.setVisibility(View.VISIBLE);
        ivLogo.setVisibility(View.GONE);
        mybalance.setVisibility(View.GONE);
        setSupportActionBar(toolbar);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });



        GiftCardListFragment fragment = new GiftCardListFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.llLRMain, fragment);
        fragmentTransaction.commit();

    }

    //
    private BroadcastReceiver mSignUpReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {


        }
    };
}