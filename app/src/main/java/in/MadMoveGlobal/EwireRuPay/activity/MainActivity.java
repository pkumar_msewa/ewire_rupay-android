package in.MadMoveGlobal.EwireRuPay.activity;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.androidquery.AQuery;
import com.crashlytics.android.Crashlytics;

//import com.expletus.mobiruck.MobiruckSdk;
import com.google.gson.Gson;
import com.orm.query.Condition;
import com.orm.query.Select;
import com.squareup.picasso.Picasso;
import com.zoho.livechat.android.exception.InvalidVisitorIDException;
import com.zoho.salesiqembed.ZohoSalesIQ;
//import com.zoho.livechat.android.exception.InvalidVisitorIDException;
//import com.zoho.salesiqembed.ZohoSalesIQ;
//import com.zoho.livechat.android.exception.InvalidVisitorIDException;
//import com.zoho.salesiqembed.ZohoSalesIQ;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import in.MadMoveGlobal.custom.CustomAlertDialog;
import in.MadMoveGlobal.custom.CustomBlockDialog;
import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.custom.TextDrawable;
//import in.paulpay.fragment.LoadMoneyFragment;
import in.MadMoveGlobal.fragment.LoadMoneyFragment;
import in.MadMoveGlobal.fragment.fragmentgiftcard.ActivityGiftCardCat;
//import in.msscard.fragment.fragmentnaviagtionitems.CustomerSupportTabLayoutFragment;
import in.MadMoveGlobal.fragment.fragmentnaviagtionitems.CustomerSupportFragment;
import in.MadMoveGlobal.fragment.fragmentnaviagtionitems.ExpenseTrackFragment;
import in.MadMoveGlobal.fragment.fragmentnaviagtionitems.FundTransferByMobileFragment;
import in.MadMoveGlobal.fragment.fragmentnaviagtionitems.HomeFragment;

import in.MadMoveGlobal.fragment.fragmentnaviagtionitems.SetPinFragment;
import in.MadMoveGlobal.loyaltyoffer.LoyaltyOfferActivity;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.metadata.AppMetadata;
import in.MadMoveGlobal.model.LoyaltyPointModel;
import in.MadMoveGlobal.model.PQCart;
import in.MadMoveGlobal.model.ShoppingModel;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.util.NetworkErrorHandler;
import in.MadMoveGlobal.EwireRuPay.*;
import in.MadMoveGlobal.util.Utility;
import io.fabric.sdk.android.Fabric;

import static in.MadMoveGlobal.metadata.ApiUrl.URL_GET_USER_DETAILS;

/**
 * Created by Dushant on 09/27/2017.
 */

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private UserModel session = UserModel.getInstance();
    private DrawerLayout drawer;
    private LoadingDialog loadDlg;
    //    private FirebaseAnalytics mFirebaseAnalytics;
    public static String group, Imagee;


    //Shopping
    private List<ShoppingModel> shoppingArray;
    private PQCart pqCart = PQCart.getInstance();

    //Language Shared Preference.
    public static final String LANG = "lang";
    SharedPreferences langPreferences;
    private String language;
    private String cardNo, cvv, cardHolder, expiryDate;

    private JSONObject jsonRequest;

    private String mPinCreated = "";
    private boolean isChanged = false;

    //Volley Tag
    private String tag_json_obj = "json_user";
    private JsonObjectRequest postReq;

    private TextView tvNavUserName, tvNavUserAccNo, tvNavUserAccName, tvNavUserBlnc, tvNavUserGroup, tvbalance, tvLoadMoney, tvblnc, tvloylity, tvUser;
    private ImageButton ivLogo;
    private String cardMinBalance, Successmessagess, cardBaseFare, cardFees, Auid, Apincode, Aphoto, Aname, Agender, Adob, AKycInfo, AfName, AlastName, Adist, Astate, Uuid, Acountry, Apoa, Aco, Acode, Astreet, Aloc;
    private Button btnUpgradeWallet;
    private CircleImageView ivNavProfilPic;
    private ImageView ivID;
    private JSONObject jsonObject;
    private String userBalance;
    private FrameLayout flentity;
    private CoordinatorLayout main_content;
    private ImageView ivGroup;


    @SuppressLint("SetTextI18n")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        ZohoSalesIQ.init(this.getApplication(), "oK28J%2BCX8OhaqWXFf3UwX21OPnGi0KIpremUyHBt7ilHWyHvdo2vsf45rut%2FXM4j", "7SoPb1HVk5zfO4ZDnVYw3kBD8ZSkqOnMhJfBXsenq7rYHga5HOMyonQfsblnhd45snm%2FBrO1iNEIuKe9Ae%2FXraT0nLe5H24lGcmu2DI8yb%2BmJzCTEOfGppARWdMlF7oyT6w7y%2BMAyqVJlFIXY4yciBWX75UgJ%2FToGEU0%2F7NqXs4%3D");
        Utility.hideKeyboard(MainActivity.this);
        Fabric.with(this, new Crashlytics());

//        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        // TODO: Move this to where you establish a user session

//       ZohoSalesIQ.init(MainActivity.this, "oK28J%2BCX8OgUuIwACAfa5w4mrIh4XtJXy3grcG8lQNo%3D", "7SoPb1HVk5zfO4ZDnVYw3k%2FGrmH9MJrXep9ov2ac8wgcfPE1oMqOh028XXv%2BNW9WgYggZ%2BB4r%2B%2B1738SXpCTgfxBJRrRSiCdUxA7WM%2FGc9vEFjvXkU1GnpfWFX8wfqwNjn6%2F8BezKYHCDZlodD65aqJkpUR3Srfc1Wl69H0PJb0%3D");

        try {


            ZohoSalesIQ.registerVisitor(session.getUserMobileNo());
            ZohoSalesIQ.Visitor.setEmail(session.getUserEmail());
            ZohoSalesIQ.Visitor.setContactNumber(session.getUserMobileNo());
            ZohoSalesIQ.Visitor.setName(session.getUserFirstName());
        } catch (InvalidVisitorIDException e) {
            e.printStackTrace();
        }


        logUser();


//        overridePendingTransition(R.anim.right_to_left, R.anim.left_to_right);
        langPreferences = getSharedPreferences(LANG, Context.MODE_PRIVATE);
        language = langPreferences.getString("language", "");

        checkLangPref();


        setContentView(R.layout.activity_main);
//setStatusBarGradiant(MainActivity.this);
        main_content = (CoordinatorLayout) findViewById(R.id.main_content);
        shoppingArray = new ArrayList<>();
        loadDlg = new LoadingDialog(this);
        tvLoadMoney = (TextView) findViewById(R.id.tvLoadMoney);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        tvbalance = (TextView) findViewById(R.id.tvbalance);
//        tvloylity = (TextView) findViewById(R.id.tvloylity);
        tvUser = (TextView) findViewById(R.id.tvUser);

        tvblnc = (TextView) findViewById(R.id.tvblnc);
        ivID = (ImageView) findViewById(R.id.ivID);
        flentity = (FrameLayout) findViewById(R.id.flentity);
        ivGroup = (ImageView) findViewById(R.id.ivGroup);
        setStatusBarGradiant(MainActivity.this);
        Typeface tf = Typeface.createFromAsset(this.getAssets(), "cgbold.ttf");

        tvblnc.setTypeface(tf);
        tvLoadMoney.setTypeface(tf);
        tvbalance.setTypeface(tf);

        tvUser.setTypeface(tf);


        ivID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent invitefriends = new Intent(MainActivity.this, ReciptOrderActivity.class);
                startActivity(invitefriends);
                finish();
            }
        });


        tvLoadMoney.setVisibility(View.VISIBLE);
        tvLoadMoney.setTypeface(tf);
        getUserBalance();
//        tvloylity.setText( session.userPoints);


        tvUser.setTypeface(tf);
        tvLoadMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent shoppingIntent = new Intent(getApplicationContext(), LoadMoneyFragment.class);
                shoppingIntent.putExtra("AutoFill", "no");
                shoppingIntent.putExtra("splitAmount", "0");
                startActivity(shoppingIntent);


            }
        });

        setSupportActionBar(toolbar);

        getSupportActionBar().

                setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
        toolbar.setSubtitle(""); //toolbar.setLogo(R.drawable.ic_toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_navigation);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.navigation);

        View header = navigationView.getHeaderView(0);
//        toolbar.setLogo(R.drawable.dialog_top);
        tvNavUserName = (TextView) header.findViewById(R.id.tvNavUserName);
        tvNavUserAccNo = (TextView) header.findViewById(R.id.tvNavUserAccNo);
        tvNavUserBlnc = (TextView) header.findViewById(R.id.tvNavUserBlnc);
        tvNavUserGroup = (TextView) header.findViewById(R.id.tvNavUserGroup);
        tvNavUserAccName = (TextView) header.findViewById(R.id.tvNavUserAccName);
        btnUpgradeWallet = (Button) header.findViewById(R.id.btnUpgradeWallet);
        ivNavProfilPic = (CircleImageView) header.findViewById(R.id.ivNavProfilPic);

        tvNavUserGroup.setText(MainActivity.group);
        tvUser.setText(MainActivity.group);

        setNavigationHeader();


        drawer = (DrawerLayout)

                findViewById(R.id.drawer_layout);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.openDrawer(Gravity.LEFT);
                hideSoftKeyboard();
            }
        });


        //New Thread
        Handler mHandler = new Handler();
        mHandler.postDelayed(navigationTask, 200);
        toolbar.setTitle("");


        LocalBroadcastManager.getInstance(this).

                registerReceiver(mMessageReceiver, new IntentFilter("setting-change"));

    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void setStatusBarGradiant(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            Drawable background = activity.getResources().getDrawable(R.drawable.bg_gradient_new);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
    }

    private void hideSoftKeyBoard() {
        try {
            // hides the soft keyboard when the drawer opens
            InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("SetTextI18n")
    private void setNavigationHeader() {
        AQuery aq = new AQuery(MainActivity.this);

        Log.v("SSS", "SSS image " + session.getUserImage());
        if (session.getUserImage() != null && !session.getUserImage().equals("") && !session.getUserImage().equals("null") && !session.getUserImage().isEmpty()) {
            Log.v("SSS", "SSS check ");

            SharedPreferences sharedPreferences = this.getSharedPreferences("CorporateUser", Context.MODE_PRIVATE);

            if (sharedPreferences.getBoolean("isCorporateUser", false)) {
                Picasso.with(this).load(session.getUserImage()).into(ivGroup);
            } else {
                aq.id(ivGroup).background(R.color.zxing_transparent).image(decodeFromBase64ToBitmap(session.getUserImage())).getContext();
            }
            //aq.id(ivGroup).background(R.color.zxing_transparent).image(decodeFromBase64ToBitmap(session.getUserImage())).getContext();
        } else {
            Log.v("SSS", "SSS check 2");

//             Picasso.with(this).load(R.drawable.ic_white_svg).into(ivGroup);
            ivGroup.setImageDrawable(getResources().getDrawable(R.drawable.ic_white_svg));
//             ivGroup.setVisibility(View.VISIBLE);



//            aq.id(ivGroup).background(R.drawable.ic_white_svg);
        }

        tvNavUserName.setText(session.getUserFirstName() + " " + session.getUserLastName());

//        tvNavUserBlnc.setText(this.getResources().getString(R.string.Card_Name) + this.getResources().getString(R.string.rupease) + userBalance);
        tvNavUserAccNo.setText(this.getResources().getString(R.string.Acc_no) + session.getUserAcNo());
        tvNavUserGroup.setText(getResources().getString(R.string.Group) + MainActivity.group);


        if (session.getUserAcName() != null && session.getUserAcName().trim().equals("KYC")) {
            tvNavUserAccName.setText(session.getUserAcName());
            Typeface tf = Typeface.createFromAsset(this.getAssets(), "cgbold.ttf");
            tvNavUserAccName.setTypeface(tf);

            btnUpgradeWallet.setVisibility(View.GONE);

        } else {
            tvNavUserAccName.setText(session.getUserAcName());
            Typeface tf = Typeface.createFromAsset(this.getAssets(), "cgbold.ttf");
            tvNavUserAccName.setTypeface(tf);

            btnUpgradeWallet.setVisibility(View.GONE);

        }
    }

    public void showLangDialog() {
        final String[] items = {getResources().getString(R.string.lang_english), getResources().getString(R.string.lang_hindi), getResources().getString(R.string.lang_kannada), getResources().getString(R.string.lang_telgu), getResources().getString(R.string.lang_malyalam), getResources().getString(R.string.lang_tamil), getResources().getString(R.string.lang_marathi), getResources().getString(R.string.lang_bengali), getResources().getString(R.string.lang_gujrati)};
        android.support.v7.app.AlertDialog.Builder langDialog =
                new android.support.v7.app.AlertDialog.Builder(MainActivity.this, R.style.AppCompatAlertDialogStyle);
        langDialog.setTitle(getResources().getString(R.string.Select_your_language));
        langDialog.setNegativeButton(getResources().getString(R.string.Dismiss), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                SharedPreferences.Editor editor = langPreferences.edit();
                editor.clear();
                editor.putString("language", "English");
                editor.apply();
                dialog.dismiss();
            }
        });
        langDialog.setItems(items, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int position) {
                if (position == 0) {
                    SharedPreferences.Editor editor = langPreferences.edit();
                    editor.clear();
                    editor.putString("language", "English");
                    editor.apply();
                    Intent intent = getIntent();
                    finish();
                    startActivity(intent);

                } else if (position == 1) {
                    SharedPreferences.Editor editor = langPreferences.edit();
                    editor.clear();
                    editor.putString("language", "Hindi");
                    editor.apply();
                    Intent intent = getIntent();
                    finish();
                    startActivity(intent);

                } else if (position == 2) {
                    SharedPreferences.Editor editor = langPreferences.edit();
                    editor.clear();
                    editor.putString("language", "Kannada");
                    editor.apply();
                    Intent intent = getIntent();
                    finish();
                    startActivity(intent);
                } else if (position == 3) {
                    SharedPreferences.Editor editor = langPreferences.edit();
                    editor.clear();
                    editor.putString("language", "Telgu");
                    editor.apply();
                    Intent intent = getIntent();
                    finish();
                    startActivity(intent);
                } else if (position == 4) {
                    SharedPreferences.Editor editor = langPreferences.edit();
                    editor.clear();
                    editor.putString("language", "Malyalam");
                    editor.apply();
                    Intent intent = getIntent();
                    finish();
                    startActivity(intent);
                } else if (position == 5) {
                    SharedPreferences.Editor editor = langPreferences.edit();
                    editor.clear();
                    editor.putString("language", "Tamil");
                    editor.apply();
                    Intent intent = getIntent();
                    finish();
                    startActivity(intent);
                } else if (position == 6) {
                    SharedPreferences.Editor editor = langPreferences.edit();
                    editor.clear();
                    editor.putString("language", "Marathi");
                    editor.apply();
                    Intent intent = getIntent();
                    finish();
                    startActivity(intent);
                } else if (position == 7) {
                    SharedPreferences.Editor editor = langPreferences.edit();
                    editor.clear();
                    editor.putString("language", "Bengali");
                    editor.apply();
                    Intent intent = getIntent();
                    finish();
                    startActivity(intent);
                } else if (position == 8) {
                    SharedPreferences.Editor editor = langPreferences.edit();
                    editor.clear();
                    editor.putString("language", "Gujrati");
                    editor.apply();
                    Intent intent = getIntent();
                    finish();
                    startActivity(intent);
                }
            }

        });

        langDialog.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void setViews() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        TextDrawable textDrawable = new TextDrawable(MainActivity.this, "balance", getResources().getColorStateList((R.color.white_text)), 70, TextDrawable.VerticalAlignment.BASELINE);
        TextView textView = (TextView) MenuItemCompat.getActionView(navigationView.getMenu().findItem(R.id.nav_home));
        navigationView.setNavigationItemSelectedListener(this);
        if (session.getUserAcName() != null && session.getUserAcName().trim().equals("KYC")) {
            Menu menu = navigationView.getMenu();
            MenuItem target = menu.findItem(R.id.nav_kyc);
            target.setVisible(false);
        }
        if (session.isHasPcard() && session.getUserPhysicalRequest().equalsIgnoreCase("Inactive")) {
            Menu menu = navigationView.getMenu();
            MenuItem target = menu.findItem(R.id.nav_mobile_topup);
            MenuItem target1 = menu.findItem(R.id.nav_set_pin);
            target.setVisible(false);
            target1.setVisible(false);
        }
        if (session.isHasPcard() && session.getUserPhysicalRequest().equalsIgnoreCase("Active")) {
            Menu menu = navigationView.getMenu();
            MenuItem target = menu.findItem(R.id.nav_physical_card);
            MenuItem target1 = menu.findItem(R.id.nav_mobile_topup);
            MenuItem target2 = menu.findItem(R.id.nav_set_pin);
            target.setVisible(false);
            target1.setVisible(true);
            target2.setVisible(true);

        }
        try {

            main_content.setBackgroundColor(getResources().getColor(R.color.white_text));
//            getUserDetail("");
            getSupportFragmentManager().beginTransaction().replace(R.id.container_body_, new HomeFragment()).commit();
        } catch (IllegalStateException e) {

        }
//        displayView(1);
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getStringExtra("updates");
            if (action.equals("1")) {
                //Balance Update
                getUserBalance();
//                getUserDetail("");
                isChanged = true;
            } else if (action.equals("2")) {
                //USer Details Update
                //TODO UserSub Details
//                getUserSubDetails();
                getUserDetail("");
                isChanged = true;
            } else if (action.equals("3")) {
                getUserBalance();
                //Reward Points Update and KYC check
//                getUserPointsAndKYC();
//                isChanged = true;
            } else if (action.equals("4")) {
                promoteLogout();
            } else if (action.equals("5")) {
                loadDlg.show();
                promoteLogout();
            } else if (action.equals("6")) {
                loadDlg.show();
//                getCartItems();
            } else if (action.equals("7")) {
                getUserBalance();
            }

        }
    };


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.menuSetting) {
            Intent intent = new Intent(getApplicationContext(), SettingActivity.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.menuRate) {
            Uri marketUri = Uri.parse("market://details?id=" + getApplication().getPackageName());
            Intent marketIntent = new Intent(Intent.ACTION_VIEW, marketUri);
            startActivity(marketIntent);
            return true;
        } else if (id == R.id.menuLanguage) {
            showLangDialog();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            android.support.v7.app.AlertDialog.Builder exitDialog =
                    new android.support.v7.app.AlertDialog.Builder(MainActivity.this, R.style.AppCompatAlertDialogStyle);
            exitDialog.setTitle(getResources().getString(R.string.Do_you_really_want_to_Exit));
            exitDialog.setPositiveButton(getResources().getString(R.string.No), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                }
            });
            exitDialog.setNegativeButton(getResources().getString(R.string.Yes), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                    finishAffinity();
                    finish();
                }
            });
            exitDialog.show();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        hideSoftKeyboard();

        if (id == R.id.nav_home) {
            drawer.postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        main_content.setBackgroundColor(getResources().getColor(R.color.white_text));
                        getSupportFragmentManager().beginTransaction().replace(R.id.container_body_, new HomeFragment()).commit();
                    } catch (IllegalStateException e) {
                        isChanged = true;
                    }
                }
            }, 300);
        } else if (id == R.id.nav_Signout) {
            main_content.setBackgroundColor(getResources().getColor(R.color.white_text));
            drawer.postDelayed(new Runnable() {
                @Override
                public void run() {
                    displayView(2);
                }
            }, 300);
        } else if (id == R.id.nav_mobile_topup) {
            main_content.setBackgroundColor(getResources().getColor(R.color.white_text));
            drawer.postDelayed(new Runnable() {
                @Override
                public void run() {
                    displayView(3);
                }
            }, 300);
        } else if (id == R.id.nav_kyc) {
            main_content.setBackgroundColor(getResources().getColor(R.color.white_text));
            drawer.postDelayed(new Runnable() {
                @Override
                public void run() {
                    displayView(4);
                }
            }, 300);
        } else if (id == R.id.nav_req_money) {
            main_content.setBackgroundColor(getResources().getColor(R.color.white_text));
            drawer.postDelayed(new Runnable() {
                @Override
                public void run() {
                    displayView(5);
                }
            }, 300);
        } else if (id == R.id.nav_offer) {
            main_content.setBackgroundColor(getResources().getColor(R.color.white_text));
            drawer.postDelayed(new Runnable() {
                @Override
                public void run() {
                    displayView(6);
                }
            }, 300);
        } else if (id == R.id.nav_fund_transfer) {
            main_content.setBackgroundColor(getResources().getColor(R.color.white_text));
            drawer.postDelayed(new Runnable() {
                @Override
                public void run() {
                    displayView(8);
                }
            }, 300);
        } else if (id == R.id.nav_refer_no) {
            drawer.postDelayed(new Runnable() {
                @Override
                public void run() {
                    displayView(7);
                }
            }, 300);
        } else if (id == R.id.nav_Language) {
            main_content.setBackgroundColor(getResources().getColor(R.color.white_text));
            drawer.postDelayed(new Runnable() {
                @Override
                public void run() {
                    displayView(20);
                }
            }, 300);
        } else if (id == R.id.nav_setting) {
            main_content.setBackgroundColor(getResources().getColor(R.color.white_text));
            drawer.postDelayed(new Runnable() {
                @Override
                public void run() {
                    displayView(9);
                }
            }, 300);
        } else if (id == R.id.nav_Rateus) {
            main_content.setBackgroundColor(getResources().getColor(R.color.white_text));
            drawer.postDelayed(new Runnable() {
                @Override
                public void run() {
                    displayView(10);
                }
            }, 300);
        } else if (id == R.id.nav_invite) {
            main_content.setBackgroundColor(getResources().getColor(R.color.white_text));
            drawer.postDelayed(new Runnable() {
                @Override
                public void run() {
                    displayView(11);
                }
            }, 300);
        } else if (id == R.id.nav_statement) {
            main_content.setBackgroundColor(getResources().getColor(R.color.white_text));
            drawer.postDelayed(new Runnable() {
                @Override
                public void run() {
                    displayView(12);
                }
            }, 300);
        } else if (id == R.id.nav_Customer_Support) {
            main_content.setBackgroundColor(getResources().getColor(R.color.white_text));
            drawer.postDelayed(new Runnable() {
                @Override
                public void run() {
                    displayView(13);
                }
            }, 300);
        } else if (id == R.id.nav_physical_card) {
            main_content.setBackgroundResource(R.color.white_text);
            drawer.postDelayed(new Runnable() {
                @Override
                public void run() {
                    displayView(14);
                }
            }, 300);
        } else if (id == R.id.nav_shopping) {
            main_content.setBackgroundResource(R.color.white_text);
            drawer.postDelayed(new Runnable() {
                @Override
                public void run() {
                    displayView(15);
                }
            }, 300);
        } else if (id == R.id.navigation_notifications) {
            main_content.setBackgroundColor(getResources().getColor(R.color.white_text));
            drawer.postDelayed(new Runnable() {
                @Override
                public void run() {
                    displayView(17);
                }
            }, 300);
        } else if (id == R.id.nav_exp_tracker) {
            main_content.setBackgroundColor(getResources().getColor(R.color.white_text));
            drawer.postDelayed(new Runnable() {
                @Override
                public void run() {
                    displayView(16);
                }
            }, 300);
        } else if (id == R.id.nav_giftcard) {
            main_content.setBackgroundColor(getResources().getColor(R.color.white_text));
            drawer.postDelayed(new Runnable() {
                @Override
                public void run() {
                    displayView(18);
                }
            }, 300);
        } else if (id == R.id.nav_set_pin) {
            main_content.setBackgroundColor(getResources().getColor(R.color.white_text));
            drawer.postDelayed(new Runnable() {
                @Override
                public void run() {
                    displayView(19);
                }
            }, 300);
        }else if (id == R.id.nav_loyalty_points) {
            main_content.setBackgroundColor(getResources().getColor(R.color.white_text));
            drawer.postDelayed(new Runnable() {
                @Override
                public void run() {
                    displayView(21);
                }
            }, 300);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void displayView(int position) {
        hideSoftKeyboard();
        Fragment fragment = null;
        Bundle navBundle = new Bundle();
        navBundle.putString(AppMetadata.FRAGMENT_TYPE, AppMetadata.FRAGMENT_TYPE);

        if (position == 1) {
            getUserDetail("");
            if (session.getUserAcName() != null && session.getUserAcName().trim().equals("KYC")) {
                try {
                    Bundle bundle = new Bundle();
                    bundle.putString("postion", "");
                    HomeFragment homeFragment = new HomeFragment();
                    homeFragment.setArguments(bundle);
                    getSupportFragmentManager().beginTransaction().add(R.id.container_body_, homeFragment).commit();
                } catch (IllegalStateException e) {
                    isChanged = true;
                }
            } else {
                successDialog(generateVerifyMessage());

            }


        } else if (position == 2) {

            promoteLogout();

        } else if (position == 3) {
            showDialog(resetmessage());

        } else if (position == 4) {

            getUserDetail("checkkyc");


        } else if (position == 5) {
            Intent shoppingIntent = new Intent(getApplicationContext(), LoadMoneyFragment.class);
            shoppingIntent.putExtra("AutoFill", "no");
            shoppingIntent.putExtra("splitAmount", "0");

            startActivity(shoppingIntent);
        } else if (position == 6) {

            Intent Promocode = new Intent(MainActivity.this, OffersActivity.class);
            startActivity(Promocode);
            finish();

        } else if (position == 7) {

            if (session.getUserAcName() != null && session.getUserAcName().trim().equals("KYC")) {

                Intent Promocode = new Intent(MainActivity.this, PromoActivity.class);
                startActivity(Promocode);
                finish();

            } else if (session.getUserAcName() != null && !session.getUserAcName().trim().equals("KYC")) {
                if (session.isHaskycRequest()) {
                    requestDialog(genraterequest());

                } else {
                    successDialog(generateVerifyMessage());
                }
            }


        } else if (position == 8) {

            if (session.getUserAcName() != null && session.getUserAcName().trim().equals("KYC")) {
                fragment = new FundTransferByMobileFragment();


            } else if (session.getUserAcName() != null && !session.getUserAcName().trim().equals("KYC")) {
                if (session.isHaskycRequest()) {
                    requestDialog(genraterequest());

                } else {
                    successDialog(generateVerifyMessage());
                }
            }


        } else if (position == 9) {
            Intent invitefriends = new Intent(MainActivity.this, SettingActivity.class);
            startActivity(invitefriends);
            finish();

        } else if (position == 10) {
//            Uri marketUri = Uri.parse("market://details?id=" + getApplication().getPackageName());
            Uri marketUri = Uri.parse("https://play.google.com/store/apps/details?id=in.MadMoveGlobal.EwireRuPay");
            Intent marketIntent = new Intent(Intent.ACTION_VIEW, marketUri);
            startActivity(marketIntent);

        } else if (position == 11) {
            Intent invitefriends = new Intent(MainActivity.this, InviteFriendActivity.class);
            startActivity(invitefriends);
            finish();
        } else if (position == 12) {


            Intent invitefriends = new Intent(MainActivity.this, ReciptTransActivity.class);
            startActivity(invitefriends);
            finish();


        } else if (position == 13) {

            Intent invitefriends = new Intent(MainActivity.this, CustomerSupportFragment.class);
            startActivity(invitefriends);
            finish();
        } else if (position == 14) {
            if (session.getUserAcName() != null && !session.getUserAcName().trim().equals("KYC") && session.hasPhyReq == true) {
                if (session.isHaskycRequest()) {
                    requestDialog(genraterequest());

                } else {
                    successDialog(generateVerifyMessage());
                }
            } else {
                startActivity(new Intent(this, MainMenuDetailActivity.class).putExtra(AppMetadata.FRAGMENT_TYPE, "PhysicalCard"));
            }


        } else if (position == 15) {
            fragment = new ExpenseTrackFragment();
        } else if (position == 16) {
            fragment = new ExpenseTrackFragment();
        } else if (position == 17) {

        } else if (position == 18) {
            Intent gci = new Intent(MainActivity.this, ActivityGiftCardCat.class);
            startActivity(gci);

        } else if (position == 19) {
            fragment = new SetPinFragment();
        } else if (position == 20) {
            showLangDialog();

        } else if (position == 21) {

            startActivity(new Intent(MainActivity.this, LoyaltyOfferActivity.class));
        }
        if (fragment != null) {
            try {
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction().addToBackStack("");
                fragmentTransaction.replace(R.id.container_body_, fragment);
                fragmentTransaction.commit();
            } catch (IllegalStateException e) {
                isChanged = true;
            }
        }
    }

    public void getReset() {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_GET_USER_RESET, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        String Successmessagess = response.getString("message");
                        String code = response.getString("code");

                        if (code != null && code.equals("S00")) {
                            loadDlg.dismiss();

//                                    startActivity(new Intent(MainActivity.this, MainActivity.class));
//                                    getActivity().finish();

                            SuccessDialogss(Successmessagess);
//                            Toast.makeText(MainActivity.this, messagess, Toast.LENGTH_SHORT).show();
                        } else {
                            loadDlg.dismiss();
                            Toast.makeText(MainActivity.this, Successmessagess, Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hash", "1234");
                    return map;
                }

            };

            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }


    public void getUserBalance() {
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_GET_USER_BALANCE, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        String code = response.getString("code");

                        if (code != null && code.equals("S00")) {
                            double userBalanceD = response.getDouble("balance");
                            userBalance = String.valueOf(userBalanceD);
                            tvbalance.setText("  " + "₹" + userBalance);
                            tvNavUserBlnc.setText(getResources().getString(R.string.user_balance) + "₹" + " " + userBalance);
//                            tvNavUserBlnc.setText(this.getResources().getString(R.string.Card_Name) + this.getResources().getString(R.string.rupease) + userBalance);

                            List<UserModel> currentUserList = Select.from(UserModel.class).list();
                            UserModel currentUser = currentUserList.get(0);
                            currentUser.setUserBalance(userBalance);
                            session.setUserBalance(userBalance);
                            currentUser.save();


                        } else if (code != null && code.equals("F03")) {
                            loadDlg.dismiss();
//                            showInvalidSessionDialog();

                            CustomToast.showMessage(MainActivity.this, getResources().getString(R.string.Please_login_and_try_again));
                            sendLogout();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hash", "1234");
                    return map;
                }

            };

            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    public void getUserDetail(final String type) {
//        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, URL_GET_USER_DETAILS, jsonRequest, new Response.Listener<JSONObject>() {
                @SuppressLint("NewApi")
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        loadDlg.dismiss();
                        String code = response.getString("code");


                        if (code != null & code.equals("S00")) {

                            Log.v("SSS", "SSS response " + response);

                            //New Implementation
                            JSONObject jsonDetail = response.getJSONObject("details");
                            boolean isImpsFixed = jsonDetail.getBoolean("isImpsFixed");
                            String impsCommissionAmt = jsonDetail.getString("impsCommissionAmt");
                            JSONObject accDetail = jsonDetail.getJSONObject("accountDetail");
                            double userBalanceD = accDetail.getDouble("balance");
                            String userBalance = String.valueOf(new DecimalFormat("#.##").format(userBalanceD));

                            long userAcNoL = accDetail.getLong("accountNumber");
                            String userAcNo = String.valueOf(userAcNoL);
                            int userPointsI = accDetail.getInt("points");
                            String userPoints = String.valueOf(userPointsI);


                            JSONObject accType = accDetail.getJSONObject("accountType");

                            String accName = accType.getString("name");
                            String accCode = accType.getString("code");


                            double accBalanceLimitD = accType.getDouble("balanceLimit");
                            double accMonthlyLimitD = accType.getDouble("monthlyLimit");
                            double accDailyLimitD = accType.getDouble("dailyLimit");

                            String accBalanceLimit = String.valueOf(accBalanceLimitD);
                            String accMonthlyLimit = String.valueOf(accMonthlyLimitD);
                            String accDailyLimit = String.valueOf(accDailyLimitD);

                            JSONObject jsonUserDetail = jsonDetail.getJSONObject("userDetail");
                            String userFName = jsonUserDetail.getString("firstName");
                            String userLName = jsonUserDetail.getString("lastName");
                            String userMobile = jsonUserDetail.getString("contactNo");
                            String userEmail = jsonUserDetail.getString("email");
                            String userEmailStatus = jsonUserDetail.getString("emailStatus");
                            MainActivity.group = response.getString("groupName");


                            String userAddress = jsonUserDetail.getString("locality");
                            String userImage = jsonUserDetail.getString("image");
//                            boolean isMPin = jsonUserDetail.getBoolean("mpinPresent");

                            String userDob = jsonUserDetail.getString("dateOfBirth");
//                            String userGender = jsonUserDetail.getString("gender");
                            String encodedImage = response.getString("encodedImage");
                            boolean hasRefer = response.getBoolean("hasRefer");
                            boolean hasvertualcard = response.getBoolean("hasVirtualCard");
                            boolean hasphysicalCard = response.getBoolean("hasPhysicalCard");
                            boolean hasPhysicalRequest = response.getBoolean("hasPhysicalRequest");
                            String physicalCardStatus = response.getString("physicalCardStatus");
                            cardMinBalance = response.getString("minimumCardBalance");
                            cardFees = response.getString("cardFees");
                            cardBaseFare = response.getString("cardBaseFare");
                            boolean haskycRequest = response.getBoolean("kycRequest");
                            String loadMoneyComm = response.getString("loadMoneyComm");

                            boolean PCStatus = response.getBoolean("physicalIsBlock");
                            boolean VCStatus = response.getBoolean("virtualIsBlock");
                            String virtualBlock = response.getString("virtualCardBlock");
                            String physicalBlock = response.getString("physicalCardBlock");


                            if (!response.isNull("cardDetails")) {
                                JSONObject cardDetails = response.getJSONObject("cardDetails");

                                cardNo = cardDetails.getString("walletNumber");
                                cvv = cardDetails.getString("cvv");
                                expiryDate = cardDetails.getString("expiryDate");
                                cardHolder = cardDetails.getString("holderName");

                            } else {
                                cardNo = "XXXXXXXXXXXXXXXX";
                                cvv = "XXX";
                                expiryDate = "XXXX-XX";
                                cardHolder = "XXXXXXXXXXXXXX";

                            }
                            UserModel.deleteAll(UserModel.class);

                            try {
                                //Encrypting sessionId
                                UserModel userModel = new UserModel(session.getUserSessionId(), userFName, userLName, userMobile, userEmail, userEmailStatus, 1, userAcNo, accName, accCode, session.getUserBalance(), accBalanceLimit, accDailyLimit, accMonthlyLimit, userAddress, encodedImage, userDob, "", false, userPoints, hasRefer, hasvertualcard, hasphysicalCard, cardNo, cvv, expiryDate, cardHolder, hasPhysicalRequest, physicalCardStatus, cardMinBalance, cardFees, haskycRequest, loadMoneyComm, cardBaseFare, PCStatus, VCStatus, virtualBlock, physicalBlock, impsCommissionAmt, isImpsFixed);
                                userModel.save();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            session.setUserSessionId(session.getUserSessionId());
                            session.setUserFirstName(userFName);
                            session.setUserLastName(userLName);
                            session.setUserMobileNo(userMobile);
                            session.setUserEmail(userEmail);
                            session.setUserAddress(userAddress);
                            session.setEmailIsActive(userEmailStatus);

                            session.setUserAcCode(accCode);
                            session.setUserAcNo(userAcNo);
                            session.setUserBalance(session.getUserBalance());
                            session.setUserAcName(accName);
                            session.setUserMonthlyLimit(accMonthlyLimit);
                            session.setUserDailyLimit(accDailyLimit);
                            session.setUserBalanceLimit(accBalanceLimit);
                            session.setUserImage(encodedImage);
                            session.setUserAddress(userAddress);
//                            session.setUserGender(userGender);
                            session.setUserDob(userDob);

                            session.setUsercardNo(cardNo);
                            session.setUsercvv(cvv);
                            session.setUserexpiryDate(expiryDate);
                            session.setUsercardHolder(cardHolder);
                            session.setUserminimumCardBalance(cardMinBalance);
                            session.setUsercardFees(cardFees);
                            session.setUsercardBaseFare(cardBaseFare);
                            session.setIsValid(1);
                            session.setUserPoints(userPoints);
                            session.setHasRefer(hasRefer);
                            session.setHasVcard(hasvertualcard);
                            session.setHasPcard(hasphysicalCard);
                            session.setHasPhysicalRequest(hasPhysicalRequest);
                            session.setPhysicalCardStatus(physicalCardStatus);
                            session.setHaskycRequest(haskycRequest);
                            session.setloadMoneyComm(loadMoneyComm);
                            session.setPCStatus(PCStatus);
                            session.setVCStatus(VCStatus);
                            session.setUserVirtualBlock(virtualBlock);
                            session.setUserPhysicalBlock(physicalBlock);
                            session.setIsImpsFixed(isImpsFixed);
                            session.setImpsCommissionAmt(impsCommissionAmt);

                            if (session.getUserAcCode().equalsIgnoreCase("NONKYC")) {

                                if (type.equalsIgnoreCase("checkkyc")) {
                                    if (!session.isHaskycRequest()) {
                                        Intent Promocode = new Intent(MainActivity.this, EkycActivity.class);
                                        startActivity(Promocode);
                                        finish();

                                    } else {
                                        CustomToast.showMessage(MainActivity.this, "Your KYC request under process");
                                    }
                                }

                            }


                            try {
                                ZohoSalesIQ.registerVisitor(session.getUserMobileNo());
                                ZohoSalesIQ.Visitor.setEmail(session.getUserEmail());
                                ZohoSalesIQ.Visitor.setContactNumber(session.getUserMobileNo());
                                ZohoSalesIQ.Visitor.setName(session.getUserFirstName());
                            } catch (InvalidVisitorIDException e) {
                                e.printStackTrace();
                            }


                            setViews();
                            getUserBalance();
                        }
                    } catch (JSONException e) {

                        loadDlg.dismiss();
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hash", "1234");

                    return map;
                }

            };

            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);

            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }


    public void getCartItems() {
//        loadDlg.show();
        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("mobile", session.getUserMobileNo());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (jsonRequest != null) {
            postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_SHOPPONG_SHOW_CART, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObj) {
                    try {
                        String code = jsonObj.getString("code");
                        loadDlg.dismiss();
                        if (code != null && code.equals("S00")) {
                            shoppingArray.clear();
                            pqCart.clearCart();
                            JSONArray productJArray = jsonObj.getJSONArray("response");

                            for (int i = 0; i < productJArray.length(); i++) {
                                JSONObject c = productJArray.getJSONObject(i);
                                long qty = c.getLong("quantity");
                                JSONObject product = c.getJSONObject("product");
                                long pId = product.getLong("id");
                                Select specificShopQueryGt = Select.from(ShoppingModel.class).where(Condition.prop("pid").eq(String.valueOf(pId)));
                                ShoppingModel spM = (ShoppingModel) specificShopQueryGt.first();
                                if (spM != null) {
                                    spM.setpQty(qty);
                                    spM.save();
                                    shoppingArray.add(spM);
                                }
                            }
                            if (shoppingArray.size() != 0) {
                                pqCart.setProductsInCart(pqCart.createNewCart(shoppingArray));
                            }

                            loadDlg.dismiss();
                        }

                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.Exception_caused_while_fetching_data), Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    error.printStackTrace();
                    CustomToast.showMessage(MainActivity.this, NetworkErrorHandler.getMessage(error, MainActivity.this));
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            }

            ;
            int socketTimeout = 120000;

            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    private void promoteLogout() {
        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_LOGOUT, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObj) {
                    try {
                        String code = jsonObj.getString("code");
                        if (code != null && code.equals("S00")) {

                            UserModel.deleteAll(UserModel.class);
                            loadDlg.dismiss();
                            startActivity(new Intent(getApplicationContext(), LoginRegActivity.class));
                            finish();
                        } else {


                            UserModel.deleteAll(UserModel.class);
                            loadDlg.dismiss();
                            startActivity(new Intent(getApplicationContext(), LoginRegActivity.class));
                            finish();
                        }
                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception2));
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    CustomToast.showMessage(getApplicationContext(), NetworkErrorHandler.getMessage(error, getApplicationContext()));
                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    private void checkLangPref() {
        switch (language) {
            case "English":
                setLocale("en");
                break;
            case "Hindi":
                setLocale("hn");
                break;
            case "Kannada":
                setLocale("kd");
                break;
            case "Marathi":
                setLocale("mrt");
                break;
            case "Telgu":
                setLocale("tel");
                break;
            case "Malyalam":
                setLocale("mly");
                break;
            case "Tamil":
                setLocale("tml");
                break;
            case "Bengali":
                setLocale("bgl");
                break;
            case "Gujrati":
                setLocale("grt");
                break;
            default:
                setLocale("en");
                showLangDialog();
        }

    }

    public void setLocale(String lang) {
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onResume() {
        super.onResume();
        if (isChanged) {
            setNavigationHeader();
            setViews();
            if (!session.isHaskycRequest()) {
                Intent Promocode = new Intent(MainActivity.this, EkycActivity.class);
                startActivity(Promocode);
                finish();

            } else {
                tvNavUserAccName.setText(getResources().getString(R.string.Under_Process));
            }
        }
    }

    private Runnable navigationTask = new Runnable() {
        @RequiresApi(api = Build.VERSION_CODES.M)
        @Override
        public void run() {
            setViews();
        }
    };

    private Bitmap decodeFromBase64ToBitmap(String encodedImage) {
        try {
            byte[] decodedString = Base64.decode(encodedImage, Base64.NO_WRAP);
            ByteArrayInputStream bais = new ByteArrayInputStream(decodedString);
            Log.d("TAG", "byte array input stream size: " + bais.available());

            Bitmap decodedBitmap = BitmapFactory.decodeStream(bais);
            return decodedBitmap;
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            return null;
        }


    }

    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        kycVerify();

    }


    public void kycVerify() {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("address1", Aco + ", " + Astreet + ", " + Adist);
            jsonRequest.put("address2", Astate + ", " + Acountry + ", " + Apincode);
            jsonRequest.put("dob", Adob);
            jsonRequest.put("name", AfName + " " + AlastName);
            jsonRequest.put("trasactionId", Auid);

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_KYC, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        String code = response.getString("code");
                        String message = response.getString("message");
                        if (code != null && code.equals("S00")) {
                            loadDlg.dismiss();

                            successDialogs(message);

                        } else if (code != null && code.equals("F03")) {
                            loadDlg.dismiss();
                            showInvalidSessionDialog();
//                            CustomToast.showMessage(MainActivity.this, "No internet connection");
                            sendLogout();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hash", "1234");
                    return map;
                }

            };

            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }


    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(MainActivity.this, R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    private void sendLogout() {
        loadDlg.show();
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(MainActivity.this).sendBroadcast(intent);
        loadDlg.dismiss();
    }

    private void sendLogoutApp() {

        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "5");
        LocalBroadcastManager.getInstance(MainActivity.this).sendBroadcast(intent);

        finish();

    }

    public void showLogout() {
        Spanned result;

        result = Html.fromHtml(AppMetadata.getLogout());

        CustomBlockDialog builder = new CustomBlockDialog(MainActivity.this, getResources().getString(R.string.Successfully_Logout), result);
        builder.setPositiveButton(getResources().getString(R.string.agree), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
                dialog.dismiss();

                finish();
            }
        });
        builder.show();
    }


    public void log(String message) {
        // Split by line, then ensure each line can fit into Log's maximum length.
        for (int i = 0, length = message.length(); i < length; i++) {
            int newline = message.indexOf('\n', i);
            newline = newline != -1 ? newline : length;
            do {
                int end = Math.min(newline, i + 4000);
                Log.d("testValues", message.substring(i, end));
                i = end;
            } while (i < newline);
        }
    }

    public void successDialogs(String Messages) {
        CustomAlertDialog builder = new CustomAlertDialog(MainActivity.this, R.string.dialog_title2, Html.fromHtml(Messages));
        builder.setPositiveButton(getResources().getString(R.string.Ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                startActivity(new Intent(MainActivity.this, MainActivity.class));
                finish();


            }
        });
        builder.show();
    }


    public void successDialog(String message) {
        CustomAlertDialog builder = new CustomAlertDialog(MainActivity.this, R.string.dialog_title2, Html.fromHtml(message));
        builder.setPositiveButton(getResources().getString(R.string.Accept), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                Intent Promocode = new Intent(MainActivity.this, EkycActivity.class);
                startActivity(Promocode);
                finish();


            }
        });

        builder.show();
    }

    public void SuccessDialogss(String messagess) {
        CustomAlertDialog builder = new CustomAlertDialog(MainActivity.this, R.string.dialog_title2, Html.fromHtml(messagess));
        builder.setPositiveButton(getResources().getString(R.string.Accept), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();


            }
        });

        builder.show();
    }


    public void requestDialog(String messagess) {
        CustomAlertDialog builder = new CustomAlertDialog(MainActivity.this, R.string.dialog_title2, Html.fromHtml(messagess));
        builder.setPositiveButton(getResources().getString(R.string.Accept), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();


            }
        });

        builder.show();
    }

    public void showDialog(String resetmsg) {
        CustomAlertDialog builder = new CustomAlertDialog(MainActivity.this, R.string.dialog_title2, Html.fromHtml(resetmsg));
        builder.setPositiveButton(getResources().getString(R.string.agree), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                getReset();

                dialog.dismiss();
            }
        });
        builder.setNegativeButton(getResources().getString(R.string.Cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    public void invalidDialog(String invalidmsg) {
        CustomAlertDialog builder = new CustomAlertDialog(MainActivity.this, R.string.dialog_title2, Html.fromHtml(invalidmsg));
        builder.setPositiveButton(getResources().getString(R.string.agree), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                Intent Promocode = new Intent(MainActivity.this, MainActivity.class);
                startActivity(Promocode);
                finish();


            }
        });

        builder.show();
    }

    public String generateVerifyMessage() {
        String source = "<b><font> </font></b>" + "<font>" + getResources().getString(R.string.Kyc_Information) + "</font></b>" +
                "<br><br><b><font color=#ff0000></font></b><br></br>" + "<font>" + getResources().getString(R.string.Accept_Cont) + "</font></b>";


        return source;
    }

    public String genraterequest() {
        String source = "<b><font color=#000000></font></b>" + "<font>" + getResources().getString(R.string.Kyc_verification) + "</font></b>" +
                "<br><br><b><font color=#ff0000></font></b><br></br>" + "<font>" + getResources().getString(R.string.Accept_Cont) + "</font></b>";
        return source;
    }

    public String resetmessage() {
        String source = "<b><font color=#000000></font></b>" + "<font>" + getResources().getString(R.string.Reset_pin) + "</font></b>" +
                "<br><br><b><font color=#ff0000></font></b><br></br>" + "<font>" + getResources().getString(R.string.Ok_continue) + "</font></b>";
        return source;
    }

    public String invalidrequest() {
        String source = "<br><br><b><font color=#000000>  Load money to virtual card has been disabled please Apply for physical card to continue  </font></b><br></br>";
        return source;
    }


    private void logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        Crashlytics.setUserIdentifier(session.getUserMobileNo());
        Crashlytics.setUserEmail(session.getUserEmail());
        Crashlytics.setUserName(session.getUserFirstName() + " " + session.getUserLastName());
    }


    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle b = intent.getExtras();
            if (b != null) {

                String message = b.getString("message");
                message = message.replaceAll("\\.", "");
                MainActivity.this.unregisterReceiver(broadcastReceiver);
                getUserBalance();

            }

        }
    };


}

