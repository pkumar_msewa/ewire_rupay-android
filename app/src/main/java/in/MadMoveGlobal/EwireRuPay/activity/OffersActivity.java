package in.MadMoveGlobal.EwireRuPay.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;

import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.fragment.fragmentnaviagtionitems.TravelOfferFragment;

/**
 * Created by Ksf on 3/27/2016.
 */
public class OffersActivity extends AppCompatActivity {


    private Button btnRegisterSubmit;
    private Toolbar toolbar;
    private ImageButton ivBackBtn;


    //    private TabLayout mSlidingTabLayout;
//    private ViewPager mainPager;
//    private FragmentManager fragmentManager;
//    CharSequence TitlesEnglish[] = {"Login", "Registration"};
//    int NumbOfTabs = 2;
//
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        overridePendingTransition(R.anim.right_to_left, R.anim.left_to_right);
        setContentView(R.layout.activity_offers);
        setStatusBarGradiant(OffersActivity.this);
        Log.i("VERSION", Build.VERSION.RELEASE);
        TravelOfferFragment travelOfferFragment = new TravelOfferFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        Bundle bType = new Bundle();
        bType.putString("URL", "https://www.rupay.co.in/rupay-offers");
        travelOfferFragment.setArguments(bType);
        fragmentTransaction.replace(R.id.flRMain, travelOfferFragment);
        fragmentTransaction.commit();

        toolbar = (Toolbar) findViewById(R.id.toolbars);
        ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtns);
        setSupportActionBar(toolbar);
        ivBackBtn.setVisibility(View.VISIBLE);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent shoppingIntent = new Intent(OffersActivity.this, MainActivity.class);
                startActivity(shoppingIntent);
                finish();
            }
        });
//        fragmentTransaction.setCustomAnimations(R.anim.left_to_right, R.anim.right_to_left);
//
//        fragmentTransaction.commit();


    }

    //
    private BroadcastReceiver mSignUpReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {


        }
    };

    @Override
    public void onBackPressed() {


        finish();

    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void setStatusBarGradiant(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            Drawable background = activity.getResources().getDrawable(R.drawable.bg_gradient_new);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
    }
}
