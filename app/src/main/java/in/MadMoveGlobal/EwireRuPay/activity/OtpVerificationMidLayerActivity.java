package in.MadMoveGlobal.EwireRuPay.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import in.MadMoveGlobal.EwireRuPay.R;

/**
 * Created by Ksf on 6/22/2016.
 */
public class OtpVerificationMidLayerActivity extends AppCompatActivity {

    private String userMobileNo;
    private String intentType;


    private Button btnApplyManual;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opt_verification_mid_layer);
        setStatusBarGradiant(OtpVerificationMidLayerActivity.this);
        btnApplyManual = (Button) findViewById(R.id.btnApplyManual);

        registerReceiver(broadcastReceiver, new IntentFilter("broadCastName"));

        userMobileNo = getIntent().getStringExtra("userMobileNo");
        intentType = getIntent().getStringExtra("intentType");

        btnApplyManual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                unregisterReceiver(broadcastReceiver);
                if(intentType.equals("Register")){
                    Intent autoIntent = new Intent(OtpVerificationMidLayerActivity.this, OtpVerificationActivity.class);
                    autoIntent.putExtra("OtpCode","");
                    autoIntent.putExtra("userMobileNo",userMobileNo);
                    startActivity(autoIntent);
                    finish();
                }
                else if(intentType.equals("Device")){
                    String password = getIntent().getStringExtra("devPassword");
                    String regId = getIntent().getStringExtra("devRegId");
                    Intent autoIntent = new Intent(OtpVerificationMidLayerActivity.this, OtpDeviceVerificationActivity.class);
                    autoIntent.putExtra("devPassword",password);
                    autoIntent.putExtra("devRegId",regId);
                    autoIntent.putExtra("OtpCode","");
                    autoIntent.putExtra("userMobileNo",userMobileNo);
                    startActivity(autoIntent);
                    finish();
                }
                else{
                    Intent autoIntent = new Intent(OtpVerificationMidLayerActivity.this, ChangePwdActivity.class);
                    autoIntent.putExtra("OtpCode","");
                    autoIntent.putExtra("userMobileNo",userMobileNo);
                    startActivity(autoIntent);
                    finish();
                }
            }
        });
    }

    BroadcastReceiver broadcastReceiver =  new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle b = intent.getExtras();
            if(b!=null){
                String message = b.getString("message");
                Log.i("BroadCast",message);
                message = message.replaceAll("\\.","");
                Log.i("BroadCast",message);

                unregisterReceiver(broadcastReceiver);

                if(intentType.equals("Register")){
                    Intent autoIntent = new Intent(OtpVerificationMidLayerActivity.this, OtpVerificationActivity.class);
                    autoIntent.putExtra("OtpCode",message);
                    autoIntent.putExtra("userMobileNo",userMobileNo);
                    startActivity(autoIntent);
                    finish();
                }else if(intentType.equals("Device")){
                    String password = getIntent().getStringExtra("devPassword");
                    String regId = getIntent().getStringExtra("devRegId");
                    Intent autoIntent = new Intent(OtpVerificationMidLayerActivity.this, OtpDeviceVerificationActivity.class);
                    autoIntent.putExtra("devPassword",password);
                    autoIntent.putExtra("devRegId",regId);
                    autoIntent.putExtra("OtpCode",message);
                    autoIntent.putExtra("userMobileNo",userMobileNo);
                    startActivity(autoIntent);
                    finish();
                }
                else if (intentType.equals("Forget")){
                    Log.i("Change","Password");
                    Intent autoIntent = new Intent(OtpVerificationMidLayerActivity.this, ChangePwdActivity.class);
                    autoIntent.putExtra("OtpCode",message);
                    autoIntent.putExtra("userMobileNo",userMobileNo);
                    startActivity(autoIntent);
                    finish();
                }

            }

        }
    };
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void setStatusBarGradiant(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            Drawable background = activity.getResources().getDrawable(R.drawable.bg_gradient_new);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
    }
}
