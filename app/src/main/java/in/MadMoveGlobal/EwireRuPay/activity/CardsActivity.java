package in.MadMoveGlobal.EwireRuPay.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;

import in.MadMoveGlobal.fragment.fragmentnaviagtionitems.PhysicalFragment;
import in.MadMoveGlobal.fragment.fragmentnaviagtionitems.VertualFragment;
import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.model.UserModel;


/**
 * Created by Dushant on 11/27/2017.
 */
public class CardsActivity extends AppCompatActivity {

    UserModel session = UserModel.getInstance();



    private TabLayout mSlidingTabLayout;
    private ViewPager mainPager;
    private FragmentManager fragmentManager;
    private Toolbar toolbar;

    private BroadcastReceiver mSignUpReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            mainPager.setCurrentItem(1);

        }
    };

    CharSequence TitlesEnglish[];
    int NumbOfTabs;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentManager = getSupportFragmentManager();
        setContentView(R.layout.activity_tab_layout);
        mainPager = (ViewPager) findViewById(R.id.mainPager);
        mSlidingTabLayout = (TabLayout) findViewById(R.id.sliding_tabs_home);
        if (session.isHasVcard() && session.isHasPcard() && session.getUserPhysicalRequest().equalsIgnoreCase("Active"))
        {
            NumbOfTabs = 1;
            TitlesEnglish = new CharSequence[]{getResources().getString(R.string.Physical_Card)};
        }
        else {
            NumbOfTabs = 2;
            TitlesEnglish = new CharSequence[]{getResources().getString(R.string.Virtual_Card), getResources().getString(R.string.Physical_Card)};
        }
        mainPager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager(), TitlesEnglish, NumbOfTabs));
        mSlidingTabLayout.setupWithViewPager(mainPager);
        toolbar = (Toolbar) findViewById(R.id.toolbars);
        ImageButton ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtns);
        ivBackBtn.setVisibility(View.VISIBLE);
        setSupportActionBar(toolbar);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mainIntent = new Intent(CardsActivity.this, MainActivity.class);
                startActivity(mainIntent);
                finish();
            }
        });

//        LocalBroadcastManager.getInstance(this).registerReceiver(mSignUpReceiver, new IntentFilter("sign-up"));
    }

    public class ViewPagerAdapter extends FragmentPagerAdapter {
        CharSequence Titles[]; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
        int NumbOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created


        // Build a Constructor and assign the passed Values to appropriate values in the class
        public ViewPagerAdapter(FragmentManager fm, CharSequence mTitles[], int mNumbOfTabsumb) {
            super(fm);
            this.Titles = mTitles;
            this.NumbOfTabs = mNumbOfTabsumb;

        }

        @Override
        public Fragment getItem(int position) {

            UserModel session = UserModel.getInstance();

            if (session.isHasVcard() && session.isHasPcard() && session.getUserPhysicalRequest().equalsIgnoreCase("Active")) {
                PhysicalFragment tab2 = new PhysicalFragment();
//                ComingSoonFrag tab2= new ComingSoonFrag();
                return tab2;
            }

            else {
                if (position == 0) {
                    VertualFragment tab1 = new VertualFragment();
                    return tab1;
                } else {
                    PhysicalFragment tab2 = new PhysicalFragment();
//                    ComingSoonFrag tab2= new ComingSoonFrag();
                    return tab2;
                }
            }

        }

        @Override
        public CharSequence getPageTitle(int position) {
            return Titles[position];
        }

        @Override
        public int getCount() {
            return Titles.length;
        }
    }

}
