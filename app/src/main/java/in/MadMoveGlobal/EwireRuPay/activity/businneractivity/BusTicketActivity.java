package in.MadMoveGlobal.EwireRuPay.activity.businneractivity;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;

//import in.paulpay.fragment.LoadMoneyFragment;
import in.MadMoveGlobal.model.BusTicketModel;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.EwireRuPay.R;


/**
 * Created by Kashif-PC on 11/24/2016.
 */
public class BusTicketActivity extends AppCompatActivity {

    private BusTicketModel busTicketModel;
    private TextView MyBallance;
    private ImageButton ivLogo;
    private UserModel session = UserModel.getInstance();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bus_booking_ticket);
        busTicketModel = getIntent().getParcelableExtra("BusTicketModel");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbars);
        ImageButton ivBackBtns = (ImageButton) findViewById(R.id.ivBackBtns);
        ivBackBtns.setVisibility(View.VISIBLE);
        setSupportActionBar(toolbar);
        ivBackBtns.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void addingPassengersDetailsToView(){

        TableLayout tlTicketsPassenger = (TableLayout) findViewById(R.id.tlTicketsPassenger);
        ArrayList<String> arrayTitle = new ArrayList<>();
        arrayTitle.add("Name");
        arrayTitle.add("Gender");
        arrayTitle.add("Age");
        arrayTitle.add("Seats");
        arrayTitle.add("Fares");

        ArrayList<String> arrayValue = new ArrayList<>();
        arrayValue.add(busTicketModel.getPassengerName());
        arrayValue.add(busTicketModel.getPassengerGender());
        arrayValue.add(busTicketModel.getPassengerAge());
        arrayValue.add(busTicketModel.getPassengerSeats());
        arrayValue.add(busTicketModel.getPassengerFare());

        for (int i = 0; i < arrayValue.size(); i++) {
            TableRow row = new TableRow(this);
            TextView textH = new TextView(this);
            TextView textC = new TextView(this);
            TextView textV = new TextView(this);

            textH.setText(arrayTitle.get(i));
            textC.setText(":  ");
            textV.setText(arrayValue.get(i));
            textV.setTypeface(null, Typeface.BOLD);

            row.addView(textH);
            row.addView(textC);
            row.addView(textV);
            tlTicketsPassenger.addView(row);
        }

    }

    private void addingTicketDetailsToView() {


        TableLayout tlTickets = (TableLayout) findViewById(R.id.tlTickets);

        ArrayList<String> arrayTitle = new ArrayList<>();
        arrayTitle.add("Booking Date");
        arrayTitle.add("Mobile No.");
        arrayTitle.add("Email");
        arrayTitle.add("Reference No.");
        arrayTitle.add("PNR No.");
        arrayTitle.add("Boarding point");
        arrayTitle.add("Date of Journey");
        arrayTitle.add("Boarding point");
        arrayTitle.add("Destination");
        arrayTitle.add("Bus Name");
        arrayTitle.add("Total Fare");


        ArrayList<String> arrayValue = new ArrayList<>();
        arrayValue.add(busTicketModel.getBookingDate());
        arrayValue.add(busTicketModel.getBookingContact());
        arrayValue.add(busTicketModel.getBookingEmail());
        arrayValue.add(busTicketModel.getBookingReferenceNo());
        arrayValue.add(busTicketModel.getBookingPNRNo());
        arrayValue.add(busTicketModel.getBoardingDetails());
        arrayValue.add(busTicketModel.getDateOfJourney());
        arrayValue.add(busTicketModel.getBusFrom());
        arrayValue.add(busTicketModel.getBusTo());
        arrayValue.add(busTicketModel.getBusName());
        arrayValue.add(busTicketModel.getBusFare());

        for (int i = 0; i < arrayValue.size(); i++) {
            TableRow row = new TableRow(this);

            TextView textH = new TextView(this);
            TextView textC = new TextView(this);
            TextView textV = new TextView(this);

            textH.setText(arrayTitle.get(i));
            textC.setText(":  ");
            textV.setText(arrayValue.get(i));
            textV.setTypeface(null, Typeface.BOLD);

            row.addView(textH);
            row.addView(textC);
            row.addView(textV);

            tlTickets.addView(row);
        }

    }


}
