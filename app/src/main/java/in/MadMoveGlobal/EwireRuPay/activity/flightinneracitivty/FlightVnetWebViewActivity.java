package in.MadMoveGlobal.EwireRuPay.activity.flightinneracitivty;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.apache.http.util.EncodingUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import in.MadMoveGlobal.custom.CustomAlertDialog;
import in.MadMoveGlobal.custom.CustomSuccessDialog;
import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.metadata.AppMetadata;
import in.MadMoveGlobal.metadata.LoadMoneyParams;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.util.NetworkErrorHandler;
import in.MadMoveGlobal.util.ServiceUtility;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.EwireRuPay.activity.MainActivity;


/**
 * Created by Ksf on 5/14/2016.
 */
public class FlightVnetWebViewActivity extends AppCompatActivity {
    private String amountToLoad;
    private boolean isVBank;
    private String orderId;
    private UserModel session = UserModel.getInstance();
    private Toolbar toolbar;
    private ImageButton ivBackBtn;
    private WebView webview;
    private LinearLayout llLoadMoneyWebViewLoading;
    boolean success = false;

    //WebWiew Error
    private LinearLayout llWebError;
    private TextView tvWebError;
    private String jsonBokking;
    private boolean roundTrip;
    private LoadingDialog loadingDialog;
    private String transcation;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ebs_web);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        loadingDialog = new LoadingDialog(FlightVnetWebViewActivity.this);
        webview = (WebView) findViewById(R.id.webview);
        llLoadMoneyWebViewLoading = (LinearLayout) findViewById(R.id.llLoadMoneyWebViewLoading);
        llWebError = (LinearLayout) findViewById(R.id.llWebError);
        tvWebError = (TextView) findViewById(R.id.tvWebError);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbars);
        ImageButton ivBackBtns = (ImageButton) findViewById(R.id.ivBackBtns);
        ivBackBtns.setVisibility(View.VISIBLE);
        setSupportActionBar(toolbar);
        ivBackBtns.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        amountToLoad = getIntent().getStringExtra("amountToLoad");
        isVBank = getIntent().getBooleanExtra("isVBank", false);
        jsonBokking = getIntent().getStringExtra("jsonBokking");
        roundTrip = getIntent().getBooleanExtra("roundTrip", false);
        try {
            HashMap<String, Object> hashMap = (HashMap<String, Object>) toMap(new JSONObject(jsonBokking));
            Log.i("value", String.valueOf(hashMap.toString()));
            HashMap<String, String> hashMap1 = jsonToMap(jsonBokking);
            for (String s : hashMap1.keySet()) {
                Log.i("valueJSON", hashMap1.get(s));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        orderId = String.valueOf(System.currentTimeMillis());
        renderView();
    }

    /**
     * To get Json By Making HTTP Call
     */

    public void renderView() {
        @SuppressWarnings("unused")
        class MyJavaScriptInterface {
            @JavascriptInterface
            public void processHTML(String html) {
                try {
                    String status = null;
                    String details = null;
                    JSONObject jsonDetails = null;
                    String transactionId = "";

                    String result = Html.fromHtml(html).toString();
                    Log.i("Transaction Response", result);

                    JSONObject jsonResponse = new JSONObject(result);
                    success = jsonResponse.getBoolean("success");
                    transcation = jsonResponse.getString("transactionId");
                    Log.i("value", transactionId);
                    if (success) {
                        JSONObject jsonRequest = new JSONObject(jsonBokking);
                        Log.i("value", jsonRequest.toString());
                        loadingDialog.show();
                        String Urls = "";
//                        if (roundTrip) {
//                            if (getIntent().getBooleanExtra("conting", false)) {
//                                Urls = ApiUrl.URL_FLIGHT_CHECKOUT_ROUNDCONTING;
//                                jsonRequest.put("flightType", "RoundwayConnecting");
//                            } else {
//                                Urls = ApiUrl.URL_FLIGHT_CHECKOUT_ROUND;
//                                jsonRequest.put("flightType", "DirectRounway");
//                            }
//
//                        } else {
//                            if (getIntent().getBooleanExtra("conting", false)) {
//                                Urls = ApiUrl.URL_FLIGHT_CHECKOUT_CONTING;
//                                jsonRequest.put("flightType", "DirectConnecting");
//                            } else {
//                                Urls = ApiUrl.URL_FLIGHT_CHECKOUT;
//                                jsonRequest.put("flightType", "Direct");
//                            }
//
//                        }
                        jsonRequest.put("transactionId", transactionId);
                        jsonRequest.put("paymentMethod", "VNet");


//                        JSONObject jsonRequest = new JSONObject(jsonBokking);
//
//                        webview.setVisibility(View.GONE);
//                        llLoadMoneyWebViewLoading.setVisibility(View.VISIBLE);
//                        String Urls;
//                        if (roundTrip) {
//                            if (getIntent().getBooleanExtra("conting",false)) {
//                                Urls = ApiUrl.URL_FLIGHT_CHECKOUT_ROUNDCONTING;
//                            } else {
//                                Urls = ApiUrl.URL_FLIGHT_CHECKOUT_ROUND;
//                            }
//                        } else {
//                            if(getIntent().getBooleanExtra("conting",false)) {
//                                Urls = ApiUrl.URL_FLIGHT_CHECKOUT_CONTING;
//                            }else {
//                                Urls = ApiUrl.URL_FLIGHT_CHECKOUT;
//                            }
//
//                        }
                        webview.setVisibility(View.GONE);
                        llLoadMoneyWebViewLoading.setVisibility(View.VISIBLE);
                        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_FLIGHT_BOOKING, jsonRequest, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    Log.i("CheckOut Resonse", response.toString());
                                    String code = response.getString("code");
                                    webview.setVisibility(View.VISIBLE);
                                    llLoadMoneyWebViewLoading.setVisibility(View.GONE);
                                    if (code != null && code.equals("S00")) {
                                        sendRefresh();
                                        showSuccessDialog();

                                    } else if (code != null && code.equals("F03")) {
                                        showInvalidSessionDialog();
                                    } else {
                                        String message = response.getString("message");
                                        showCustomDialog(message);

                                    }
                                } catch (JSONException e) {
                                    showCustomDialog(getResources().getString(R.string.server_exception2));
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                showCustomDialog(NetworkErrorHandler.getMessage(error, FlightVnetWebViewActivity.this));
                                error.printStackTrace();
                            }
                        }) {
                            @Override
                            public Map<String, String> getHeaders() throws AuthFailureError {
                                HashMap<String, String> map = new HashMap<>();
                                return map;
                            }

                        };
//
                        int socketTimeout = 6000000;
                        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                        postReq.setRetryPolicy(policy);
                        PayQwikApplication.getInstance().addToRequestQueue(postReq, "loadmoney");
                    } else {
                        status = "Transaction Failed!\nPlease try again";
                    }
//
                    if (status != null) {
                        showCustomDialog(status);
                        sendRefresh();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }

        webview.getSettings().setJavaScriptEnabled(true);

        webview.setWebChromeClient(new WebChromeClient() {

            public void onProgressChanged(WebView view, int progress) {
                if (progress == 100) {
                    webview.setVisibility(View.VISIBLE);
                    llLoadMoneyWebViewLoading.setVisibility(View.GONE);
                } else {
                    webview.setVisibility(View.GONE);
                    llLoadMoneyWebViewLoading.setVisibility(View.VISIBLE);
                }
            }
        });

        webview.addJavascriptInterface(new MyJavaScriptInterface(), "HTMLOUT");
        webview.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(webview, url);
                final WebView v = view;
                if (url.indexOf("/LoadMoney/Redirect") != -1 || url.indexOf("/LoadMoney/VRedirect") != -1) {
                    llLoadMoneyWebViewLoading.setVisibility(View.GONE);
                    webview.setVisibility(View.INVISIBLE);
                    webview.loadUrl("javascript:window.HTMLOUT.processHTML('<head>'+document.getElementsByTagName('html')[0].innerHTML+'</head>');");
                } else {
                    llLoadMoneyWebViewLoading.setVisibility(View.GONE);
                    webview.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {

                llLoadMoneyWebViewLoading.setVisibility(View.GONE);
                try {
                    view.loadData("", "text/html", "UTF-8");
                    llWebError.setVisibility(View.VISIBLE);
                    tvWebError.setText(description);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @TargetApi(android.os.Build.VERSION_CODES.M)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
                llLoadMoneyWebViewLoading.setVisibility(View.GONE);
                try {
                    view.loadData("", "text/html", "UTF-8");
                    llWebError.setVisibility(View.VISIBLE);
                    tvWebError.setText(rerr.getDescription().toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                if (url.equals("https://mpi.onlinesbi.com/electraSECURE/vbv/MPIEntry.jsp")) {
                    llLoadMoneyWebViewLoading.setVisibility(View.VISIBLE);
                }
                Log.i("Page Started", url);
            }
        });


        StringBuffer params = new StringBuffer();

        params.append(ServiceUtility.addToPostParams("sessionId", session.getUserSessionId()));
        params.append(ServiceUtility.addToPostParams("channel", LoadMoneyParams.CHANNEL));
        params.append(ServiceUtility.addToPostParams("accountId", LoadMoneyParams.ACC_ID));
        params.append(ServiceUtility.addToPostParams("referenceNo", LoadMoneyParams.REF_NO));
        params.append(ServiceUtility.addToPostParams("amount", amountToLoad));
        params.append(ServiceUtility.addToPostParams("mode", LoadMoneyParams.MODE));
        params.append(ServiceUtility.addToPostParams("currency", LoadMoneyParams.CURRENCY));
        params.append(ServiceUtility.addToPostParams("description", LoadMoneyParams.DESCRIPTION));
        Log.i("return URL", LoadMoneyParams.RETURN_URL_VBNET_FLIGHT);
        params.append(ServiceUtility.addToPostParams("returnUrl", LoadMoneyParams.RETURN_URL_VBNET_FLIGHT));


        params.append(ServiceUtility.addToPostParams("name", session.getUserFirstName() + " " + session.getUserLastName()));
        params.append(ServiceUtility.addToPostParams("address", LoadMoneyParams.ADDRESS));
        params.append(ServiceUtility.addToPostParams("city", LoadMoneyParams.CITY));
        params.append(ServiceUtility.addToPostParams("state", LoadMoneyParams.STATE));
        params.append(ServiceUtility.addToPostParams("country", LoadMoneyParams.COUNTRY));
        params.append(ServiceUtility.addToPostParams("postalCode", LoadMoneyParams.POSTAL));
        params.append(ServiceUtility.addToPostParams("phone", session.getUserMobileNo()));
        params.append(ServiceUtility.addToPostParams("email", session.getUserEmail()));

        params.append(ServiceUtility.addToPostParams("shipName", LoadMoneyParams.SHIP_NAME));
        params.append(ServiceUtility.addToPostParams("shipAddress", LoadMoneyParams.SHIP_ADDRESS));
        params.append(ServiceUtility.addToPostParams("shipCity", LoadMoneyParams.SHIP_CITY));
        params.append(ServiceUtility.addToPostParams("shipState", LoadMoneyParams.SHIP_STATE));
        params.append(ServiceUtility.addToPostParams("shipCountry", LoadMoneyParams.SHIP_COUNTRY));
        params.append(ServiceUtility.addToPostParams("shipPostalCode", LoadMoneyParams.SHIP_POSTAL_CODE));
        params.append(ServiceUtility.addToPostParams("shipPhone", session.getUserMobileNo()));
        params.append(ServiceUtility.addToPostParams("useVnet", String.valueOf(isVBank)));

        String vPostParams = params.substring(0, params.length() - 1);

        Log.i("vparams", vPostParams);
        webview.postUrl(ApiUrl.URL_FLIGHT_MONEY, EncodingUtils.getBytes(vPostParams, "UTF-8"));

    }

    public void showCustomDialog(String msg) {
        CustomAlertDialog builder = new CustomAlertDialog(FlightVnetWebViewActivity.this, R.string.dialog_message_eb, msg);
        builder.setNegativeButton("Go Back", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                if (success) {
                    Log.i("VBANkSuccess", "Load money success broadcast send to mainMenu");
                    sendFinish();
                } else {
                    Log.i("VBANLFailure", "Load money failure");
                }
                finish();
            }
        });
        builder.show();
    }

    private void sendRefresh() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "1");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    @Override
    public void onBackPressed() {
        sendRefresh();
        super.onBackPressed();
    }

    private void sendFinish() {
        Log.i("Success", "Send Finish");
        Intent intent = new Intent("loadMoney-done");
        intent.putExtra("result", "1");
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
    }

    public void showInvalidSessionDialog() {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(AppMetadata.getInvalidSession(), Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(AppMetadata.getInvalidSession());
        }
        CustomAlertDialog builder = new CustomAlertDialog(FlightVnetWebViewActivity.this, R.string.dialog_title2, result);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(FlightVnetWebViewActivity.this).sendBroadcast(intent);
    }

    public void showSuccessDialog() {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(getSuccessMessage(), Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(getSuccessMessage());
        }
        CustomSuccessDialog builder = new CustomSuccessDialog(FlightVnetWebViewActivity.this, "Booked Successful", result);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                sendRefresh();
                Intent i = new Intent(FlightVnetWebViewActivity.this, MainActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                finish();
                startActivity(i);
            }
        });
        builder.show();
    }

    public String getSuccessMessage() {
        String source =
                "<b><font color=#000000> </font></b>" + "<font color=#000000>" + "Flight Ticket Booked Successfully" + "</font><br>" +
                        "<b><font color=#000000> Please check your email for details </font></b>" + "<font>";
        return source;
    }

    public HashMap<String, String> jsonToMap(String t) throws JSONException {

        HashMap<String, String> map = new HashMap<String, String>();
        JSONObject jObject = new JSONObject(t);
        Iterator<?> keys = jObject.keys();

        while (keys.hasNext()) {
            String key = (String) keys.next();
            String value = jObject.getString(key);
            map.put(key, value);

        }

        return map;
    }

    public static Object toJSON(Object object) throws JSONException {
        if (object instanceof Map) {
            JSONObject json = new JSONObject();
            Map map = (Map) object;
            for (Object key : map.keySet()) {
                json.put(key.toString(), toJSON(map.get(key)));
            }
            return json;
        } else if (object instanceof Iterable) {
            JSONArray json = new JSONArray();
            for (Object value : ((Iterable) object)) {
                json.put(value);
            }
            return json;
        } else {
            return object;
        }
    }

    public static boolean isEmptyObject(JSONObject object) {
        return object.names() == null;
    }

    public static Map<String, Object> getMap(JSONObject object, String key) throws JSONException {
        return toMap(object.getJSONObject(key));
    }

    public static Map<String, Object> toMap(JSONObject object) throws JSONException {
        Map<String, Object> map = new HashMap();
        Iterator keys = object.keys();
        while (keys.hasNext()) {
            String key = (String) keys.next();
            map.put(key, fromJson(object.get(key)));
        }
        return map;
    }

    public static List toList(JSONArray array) throws JSONException {
        List list = new ArrayList();
        for (int i = 0; i < array.length(); i++) {
            list.add(fromJson(array.get(i)));
        }
        return list;
    }

    private static Object fromJson(Object json) throws JSONException {
        if (json == JSONObject.NULL) {
            return null;
        } else if (json instanceof JSONObject) {
            return toMap((JSONObject) json);
        } else if (json instanceof JSONArray) {
            return toList((JSONArray) json);
        } else {
            return json;
        }
    }
}
