package in.MadMoveGlobal.EwireRuPay.activity.flightinneracitivty;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

//import com.androidnetworking.AndroidNetworking;
//import com.androidnetworking.common.Priority;
//import com.androidnetworking.error.ANError;
//import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.custom.CustomAlertDialog;
import in.MadMoveGlobal.custom.CustomSuccessDialog;
import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.metadata.AppMetadata;
import in.MadMoveGlobal.model.FlightListModel;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.util.CheckLog;
import in.MadMoveGlobal.util.NetworkErrorHandler;
import in.MadMoveGlobal.util.PayingDetailsValidation;
import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.EwireRuPay.activity.MainActivity;
import in.MadMoveGlobal.util.Utility;


/**
 * Created by kashifimam on 05/08/17.
 */

public class FlightBookingFormActivity extends AppCompatActivity {


    private MaterialEditText etBookFlightMobile, etBookFlightDob, etBookFlightCitizen, etBookFlightEmail;
    private RadioButton rbBookFlightMale, rbBookFlightFeMale;
    private LinearLayout llFlightAdult, llFlightChild, llFlightInfant;

    private Button btnBookFlightSubmit;

    private Toolbar toolbar;
    private ImageButton ivBackBtn;
    private LoadingDialog loadDlg;

    private String gender = "MALE";

    private String sourceCode, destinationCode, dateOfJourney;
    private int adultNo, childNo, infantNo;
    private int totalPeople;

    private String flightClass;


    private FlightListModel flightListModel;
    private UserModel session = UserModel.getInstance();

    private JSONObject segmentObject;
    private JSONObject jsonRequest;
    private JSONObject contingFlight;
    private String tag_json_obj = "json_flight";

    private String engineId;

    private boolean cancel;
    private View focusView;
    private String flightConnectDetails;
    private String latestfare;
    private String travel;
    private LinearLayout llFlightAge;
    private ArrayList<EditText> materialEditAdultFirstName, materialEditAdultLastName, materialEditChildFirstName, materialEditChildLastName;
    private ArrayList<MaterialEditText> materialEditinfantfirstName, materialEditinfantLastName, materialEditAgeFistName, materialEditAgeLAst;
    private ArrayList<MaterialEditText> materialEditAdultAge, materialEditChildAge, materialEditinfantAge;
    private String dateOfReturn = "";
    private boolean intentional;
    private ArrayList<EditText> materialEditAdultDOB, materialEditAdultPassport, materialEditAdultExpriDate, materialEditChildDOB, materialEditChildPassport, materialEditChildExpriDate, materialEditinfantDOB, materialEditinfantPassport, materialEditinfantExpriDate;
    private TextView tvadult, tvChild, tvinfant;
    private boolean intentionaloneway;

    public static boolean isEmailValid(String email) {

        String regExpn = "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";
        CharSequence inputStr = email;
        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches())
            return true;
        else
            return false;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flight_booking_form);
        loadDlg = new LoadingDialog(FlightBookingFormActivity.this);
        flightListModel = getIntent().getParcelableExtra("FlightModel");
        intentional = getIntent().getBooleanExtra("intentional", false);
        try {
            segmentObject = new JSONObject(flightListModel.getFlightJsonString());
            engineId = segmentObject.getString("engineID");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        tvadult = (TextView) findViewById(R.id.tvadult);
        tvChild = (TextView) findViewById(R.id.tvChild);
        tvinfant = (TextView) findViewById(R.id.tvinfant);
        sourceCode = getIntent().getStringExtra("sourceCode");
        destinationCode = getIntent().getStringExtra("destinationCode");
        dateOfJourney = getIntent().getStringExtra("dateOfJourney");
        flightClass = getIntent().getStringExtra("flightClass");
        latestfare = getIntent().getStringExtra("latestfare");
        travel = getIntent().getStringExtra("travel");
        intentionaloneway = getIntent().getBooleanExtra("intentionaloneway", false);
        try {
            dateOfReturn = getIntent().getStringExtra("dateOfReturn");
        } catch (NullPointerException e) {


        }
        adultNo = getIntent().getIntExtra("adultNo", 0);
        childNo = getIntent().getIntExtra("childNo", 0);
        infantNo = getIntent().getIntExtra("infantNo", 0);

        flightConnectDetails = getIntent().getStringExtra("contingFlight");
        contingFlight = new JSONObject();
        if (!flightConnectDetails.equalsIgnoreCase("{}")) {
            try {
                JSONObject flighTList = new JSONObject(flightConnectDetails);
                JSONArray jsonArray = new JSONArray();
                jsonArray.put(flighTList);
                contingFlight.put("bookSegments", jsonArray);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        totalPeople = adultNo + childNo + infantNo;
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtn);
        setSupportActionBar(toolbar);
        ivBackBtn.setVisibility(View.VISIBLE);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        etBookFlightMobile = (MaterialEditText) findViewById(R.id.etBookFlightMobile);
        etBookFlightDob = (MaterialEditText) findViewById(R.id.etBookFlightDob);
        etBookFlightCitizen = (MaterialEditText) findViewById(R.id.etBookFlightCitizen);
        etBookFlightEmail = (MaterialEditText) findViewById(R.id.etBookFlightEmail);

        llFlightAdult = (LinearLayout) findViewById(R.id.llFlightAdult);
        llFlightChild = (LinearLayout) findViewById(R.id.llFlightChild);
        llFlightInfant = (LinearLayout) findViewById(R.id.llFlightInfant);

        rbBookFlightMale = (RadioButton) findViewById(R.id.rbBookFlightMale);
        rbBookFlightFeMale = (RadioButton) findViewById(R.id.rbBookFlightFeMale);

        if (childNo == 0) {
            llFlightChild.setVisibility(View.GONE);
        }

        if (adultNo == 0) {
            llFlightAdult.setVisibility(View.GONE);
        }

        if (infantNo == 0) {
            llFlightInfant.setVisibility(View.GONE);
        }


        btnBookFlightSubmit = (Button) findViewById(R.id.btnBookFlightSubmit);


        etBookFlightDob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                new DatePickerDialog(FlightBookingFormActivity.this, R.style.DatePicker, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        String formattedDay, formattedMonth;
                        if (dayOfMonth < 10) {
                            formattedDay = "0" + dayOfMonth;
                        } else {
                            formattedDay = dayOfMonth + "";
                        }

                        if ((monthOfYear + 1) < 10) {
                            formattedMonth = "0" + String.valueOf(monthOfYear + 1);
                        } else {
                            formattedMonth = String.valueOf(monthOfYear + 1) + "";
                        }
                        etBookFlightDob.setText(String.valueOf(year) + "-"
                                + String.valueOf(formattedMonth) + "-"
                                + String.valueOf(formattedDay));
                    }
                }, 1990, 01, 01).show();
            }
        });


        btnBookFlightSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitForm();
            }
        });


        materialEditAdultFirstName = new ArrayList<>();
        materialEditAdultLastName = new ArrayList<>();
        materialEditAdultDOB = new ArrayList<>();
        materialEditAdultPassport = new ArrayList<>();
        materialEditAdultExpriDate = new ArrayList<>();
        materialEditAdultAge = new ArrayList<>();
        materialEditChildFirstName = new ArrayList<>();
        materialEditChildLastName = new ArrayList<>();
        materialEditChildDOB = new ArrayList<>();
        materialEditChildPassport = new ArrayList<>();
        materialEditChildExpriDate = new ArrayList<>();
        materialEditChildAge = new ArrayList<>();
        materialEditinfantfirstName = new ArrayList<>();
        materialEditinfantLastName = new ArrayList<>();
        materialEditinfantAge = new ArrayList<>();
        materialEditinfantDOB = new ArrayList<>();
        materialEditinfantPassport = new ArrayList<>();
        materialEditinfantExpriDate = new ArrayList<>();

        for (int i = 0; i < adultNo; i++) {
            tvadult.setVisibility(View.VISIBLE);
            ViewGroup.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT
                    , ViewGroup.LayoutParams.WRAP_CONTENT);

            final MaterialEditText etFName = new MaterialEditText(FlightBookingFormActivity.this);
            etFName.setHint("Enter adult first name");
            etFName.setIconLeft(R.drawable.et_user);
            final MaterialEditText etLName = new MaterialEditText(FlightBookingFormActivity.this);
            etLName.setHint("Enter adult  last name");
            etLName.setIconLeft(R.drawable.et_user);

            final MaterialEditText etAge = new MaterialEditText(FlightBookingFormActivity.this);
            etAge.setHint("Enter Gender");
            etAge.setIconLeft(R.drawable.et_gender);

            final MaterialEditText etDOb = new MaterialEditText(FlightBookingFormActivity.this);
            etDOb.setHint("Enter Date of Birth");
            etDOb.setIconLeft(R.drawable.et_date);
            etDOb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getDob(etDOb);
                }
            });
            if (intentionaloneway) {
                final MaterialEditText etPassport = new MaterialEditText(FlightBookingFormActivity.this);
                etPassport.setHint("Enter Passport Number");
                final MaterialEditText etPassportExpiry = new MaterialEditText(FlightBookingFormActivity.this);
                etPassportExpiry.setHint("Enter Passport Expiry Date");
                etPassportExpiry.setIconLeft(R.drawable.et_date);
                etPassportExpiry.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getexiper(etPassportExpiry);
                    }
                });
                etPassport.setIconLeft(R.drawable.et_date);
                etPassportExpiry.setFocusable(false);
                etPassportExpiry.setFocusableInTouchMode(false);
                llFlightAdult.addView(etPassport);
                llFlightAdult.addView(etPassportExpiry);
                materialEditAdultPassport.add(etPassport);
                materialEditAdultExpriDate.add(etPassportExpiry);
            }
            etDOb.setLayoutParams(lp);
            etDOb.setFocusable(false);
            etAge.setIconLeft(R.drawable.et_gender);
            etAge.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getAdult(etAge);
                }
            });
            etLName.setIconLeft(R.drawable.et_user);
            etFName.setLayoutParams(lp);
            etLName.setLayoutParams(lp);
            etAge.setLayoutParams(lp);
            etAge.setFocusable(false);

            etAge.setFocusableInTouchMode(false);
            llFlightAdult.addView(etFName);
            llFlightAdult.addView(etLName);
            llFlightAdult.addView(etAge);
            materialEditAdultFirstName.add(etFName);
            materialEditAdultLastName.add(etLName);
            materialEditAdultAge.add(etAge);
            llFlightAdult.addView(etDOb);

            materialEditAdultDOB.add(etDOb);
        }
        for (int i = 0; i < childNo; i++) {
            tvChild.setVisibility(View.VISIBLE);
            ViewGroup.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT
                    , ViewGroup.LayoutParams.WRAP_CONTENT);

            final MaterialEditText etFName = new MaterialEditText(FlightBookingFormActivity.this);
            etFName.setIconLeft(R.drawable.et_user);
            etFName.setHint("Enter child first name");
            final MaterialEditText etLName = new MaterialEditText(FlightBookingFormActivity.this);
            etLName.setHint("Enter child last name");
            etLName.setIconLeft(R.drawable.et_user);

            final MaterialEditText etAge = new MaterialEditText(FlightBookingFormActivity.this);
            etAge.setHint("Enter Gender");
            etAge.setIconLeft(R.drawable.et_gender);
            final MaterialEditText etDOb = new MaterialEditText(FlightBookingFormActivity.this);
            etDOb.setHint("Enter Date of Birth");
            etDOb.setIconLeft(R.drawable.et_date);
            etDOb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getDob(etDOb);
                }
            });
            if (intentionaloneway) {
                final MaterialEditText etPassport = new MaterialEditText(FlightBookingFormActivity.this);
                etPassport.setHint("Enter Passport Number");
                final MaterialEditText etPassportExpiry = new MaterialEditText(FlightBookingFormActivity.this);
                etPassportExpiry.setHint("Enter Passport Expiry Date");
                etPassportExpiry.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getexiper(etPassportExpiry);
                    }
                });
                etPassport.setIconLeft(R.drawable.et_date);
                etPassportExpiry.setIconLeft(R.drawable.et_date);
                etPassportExpiry.setFocusable(false);
                etPassportExpiry.setFocusableInTouchMode(false);
                llFlightChild.addView(etPassport);
                llFlightChild.addView(etPassportExpiry);
                materialEditChildExpriDate.add(etPassport);
                materialEditChildPassport.add(etPassportExpiry);
            }
            etDOb.setLayoutParams(lp);
            etDOb.setFocusable(false);

            etAge.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getAdult(etAge);
                }
            });
            etLName.setIconLeft(R.drawable.et_user);
            etFName.setLayoutParams(lp);
            etLName.setLayoutParams(lp);
            etAge.setLayoutParams(lp);
            etAge.setFocusable(false);
            etAge.setFocusableInTouchMode(false);
            etFName.setLayoutParams(lp);
            etLName.setLayoutParams(lp);
            etAge.setLayoutParams(lp);
            llFlightChild.addView(etFName);
            llFlightChild.addView(etLName);
            llFlightChild.addView(etAge);
            llFlightChild.addView(etDOb);
            materialEditChildDOB.add(etDOb);
            materialEditChildFirstName.add(etFName);
            materialEditChildLastName.add(etLName);
            materialEditChildAge.add(etAge);

        }

        for (int i = 0; i < infantNo; i++) {
            tvinfant.setVisibility(View.VISIBLE);
            ViewGroup.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT
                    , ViewGroup.LayoutParams.WRAP_CONTENT);

            final MaterialEditText etFName = new MaterialEditText(FlightBookingFormActivity.this);
            etFName.setHint("Enter infant first name");
            etFName.setIconLeft(R.drawable.et_user);
            final MaterialEditText etLName = new MaterialEditText(FlightBookingFormActivity.this);
            etLName.setHint("Enter infant last name");

            etLName.setIconLeft(R.drawable.et_user);
            final MaterialEditText etAge = new MaterialEditText(FlightBookingFormActivity.this);
            etAge.setHint("Enter Gender");
            etAge.setIconLeft(R.drawable.et_gender);

            etAge.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getAdult(etAge);
                }
            });
            etLName.setIconLeft(R.drawable.et_user);
            etFName.setLayoutParams(lp);
            etLName.setLayoutParams(lp);
            etAge.setLayoutParams(lp);
            etAge.setFocusable(false);
            etAge.setFocusableInTouchMode(false);
            etFName.setLayoutParams(lp);
            etLName.setLayoutParams(lp);
            etAge.setLayoutParams(lp);
            final MaterialEditText etDOb = new MaterialEditText(FlightBookingFormActivity.this);
            etDOb.setHint("Enter Date of Birth");
            etDOb.setIconLeft(R.drawable.et_date);
            etDOb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getDob(etDOb);
                }
            });
            if (intentionaloneway) {
                final MaterialEditText etPassport = new MaterialEditText(FlightBookingFormActivity.this);
                etPassport.setHint("Enter Passport Number");
                final MaterialEditText etPassportExpiry = new MaterialEditText(FlightBookingFormActivity.this);
                etPassportExpiry.setHint("Enter Passport Expiry Date");
                etPassportExpiry.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getexiper(etPassportExpiry);
                    }
                });

                etPassport.setLayoutParams(lp);
                etPassportExpiry.setLayoutParams(lp);
                etPassport.setIconLeft(R.drawable.et_date);
                etPassportExpiry.setIconLeft(R.drawable.et_date);
                etPassport.setFocusableInTouchMode(false);
                etPassportExpiry.setFocusable(false);
                etPassportExpiry.setFocusableInTouchMode(false);
                etPassport.setIconLeft(R.drawable.et_date);
                materialEditinfantPassport.add(etPassport);
                materialEditinfantExpriDate.add(etPassportExpiry);
                llFlightInfant.addView(etPassport);
                llFlightInfant.addView(etPassportExpiry);
            }
            etDOb.setLayoutParams(lp);
            etDOb.setFocusable(false);
            llFlightInfant.addView(etFName);
            llFlightInfant.addView(etLName);
            llFlightInfant.addView(etAge);
            llFlightInfant.addView(etDOb);
            materialEditinfantDOB.add(etDOb);
            materialEditinfantfirstName.add(etFName);
            materialEditinfantLastName.add(etLName);
            materialEditinfantAge.add(etAge);

        }

        etBookFlightEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    private void getexiper(final MaterialEditText etPassportExpiry) {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(FlightBookingFormActivity.this, R.style.DatePicker, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                String formattedDay, formattedMonth;
                if (dayOfMonth < 10) {
                    formattedDay = "0" + dayOfMonth;
                } else {
                    formattedDay = dayOfMonth + "";
                }

                if ((monthOfYear + 1) < 10) {
                    formattedMonth = "0" + String.valueOf(monthOfYear + 1);
                } else {
                    formattedMonth = String.valueOf(monthOfYear + 1) + "";
                }
                etPassportExpiry.setText(String.valueOf(year) + "-" + String.valueOf(formattedMonth) + "-" + String.valueOf(formattedDay));
            }
        }, 1990, 01, 01);
        datePickerDialog.show();
        datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
    }

    private void submitForm() {

        validateDynamicAdultField();
        validateDynamicChildField();
        validateDynamicInfantField();
        etBookFlightCitizen.setError(null);
        etBookFlightDob.setError(null);
        etBookFlightMobile.setError(null);
        etBookFlightEmail.setError(null);

        cancel = false;

        checkDynamicAdultField();
        checkDynamicChildField();
        checkDynamicInfantField();
//        checkDob(etBookFlightDob.getText().toString());
        checkEmail(etBookFlightEmail.getText().toString());
        checkMobile(etBookFlightMobile.getText().toString());

        if (cancel) {
            focusView.requestFocus();
        } else {
            attemptCheckOut();
        }

    }

    private void validateDynamicAdultField() {
        int count = llFlightAdult.getChildCount();
        for (int i = 0; i < count; i++) {
            View view = llFlightAdult.getChildAt(i);
            MaterialEditText etDynamic = (MaterialEditText) view;
            etDynamic.setError(null);
        }


    }

    private void checkDynamicAdultField() {
        int count = llFlightAdult.getChildCount();
        for (int i = 0; i < count; i++) {
            View view = llFlightAdult.getChildAt(i);
            MaterialEditText etDynamic = (MaterialEditText) view;

            if (etDynamic.getText().toString().trim().isEmpty()) {
                etDynamic.setError("This field is required");
                focusView = etDynamic;
                cancel = true;
            }
        }

    }

    private void validateDynamicChildField() {
        int count = llFlightChild.getChildCount();
        for (int i = 0; i < count; i++) {
            View view = llFlightChild.getChildAt(i);
            MaterialEditText etDynamic = (MaterialEditText) view;
            etDynamic.setError(null);
        }


    }

    private void checkDynamicChildField() {
        int count = llFlightChild.getChildCount();
        for (int i = 0; i < count; i++) {
            View view = llFlightChild.getChildAt(i);
            MaterialEditText etDynamic = (MaterialEditText) view;

            if (etDynamic.getText().toString().trim().isEmpty()) {
                etDynamic.setError("This field is required");
                focusView = etDynamic;
                cancel = true;
            }
        }

    }

    private void validateDynamicInfantField() {
        int count = llFlightInfant.getChildCount();
        for (int i = 0; i < count; i++) {
            View view = llFlightInfant.getChildAt(i);
            MaterialEditText etDynamic = (MaterialEditText) view;
            etDynamic.setError(null);
        }


    }

    private void checkDynamicInfantField() {
        int count = llFlightInfant.getChildCount();
        for (int i = 0; i < count; i++) {
            View view = llFlightInfant.getChildAt(i);
            MaterialEditText etDynamic = (MaterialEditText) view;

            if (etDynamic.getText().toString().trim().isEmpty()) {
                etDynamic.setError("This field is required");
                focusView = etDynamic;
                cancel = true;
            }
        }

    }

    private void checkEmail(String inviteName) {
        CheckLog inviteNameCheckLog = PayingDetailsValidation.checkEmail(inviteName);
        if (!inviteNameCheckLog.isValid) {
            etBookFlightEmail.setError(getString(inviteNameCheckLog.msg));
            focusView = etBookFlightEmail;
            cancel = true;
        }

    }

    private void checkMobile(String inviteName) {
        CheckLog inviteNameCheckLog = PayingDetailsValidation.checkMobileTenDigit(inviteName);
        if (!inviteNameCheckLog.isValid) {
            etBookFlightMobile.setError(getString(inviteNameCheckLog.msg));
            focusView = etBookFlightMobile;
            cancel = true;
        }

    }

    private void checkCitizenship(String inviteName) {
        CheckLog inviteNameCheckLog = PayingDetailsValidation.checkFirstName(inviteName);
        if (!inviteNameCheckLog.isValid) {
            etBookFlightCitizen.setError(getString(inviteNameCheckLog.msg));
            focusView = etBookFlightCitizen;
            cancel = true;
        }

    }

    private void checkDob(String inviteName) {
        CheckLog inviteNameCheckLog = PayingDetailsValidation.checkFirstName(inviteName);
        if (!inviteNameCheckLog.isValid) {
            etBookFlightDob.setError(getString(inviteNameCheckLog.msg));
            focusView = etBookFlightDob;
            cancel = true;
        }

    }

    public void attemptCheckOut() {
        jsonRequest = new JSONObject();
        try {
            JSONArray jsonArray = new JSONArray();
            contingFlight.put("emailAddress", etBookFlightEmail.getText().toString());
            contingFlight.put("mobileNumber", etBookFlightMobile.getText().toString());
            contingFlight.put("visatype", "Employee Visa");
            contingFlight.put("traceId", "AYTM00011111111110001");
            contingFlight.put("androidBooking", true);
            contingFlight.put("domestic", true);
            contingFlight.put("engineID", segmentObject.getString("engineID"));
            contingFlight.put("engineIDList", jsonArray.put(segmentObject.getString("engineID")));
            JSONObject paymentDetails = new JSONObject();
            paymentDetails.put("bookingAmount", latestfare);
            paymentDetails.put("bookingCurrencyCode", "INR");
            contingFlight.put("paymentDetails", paymentDetails);
            JSONObject flightSearchDetails = new JSONObject();
            flightSearchDetails.put("beginDate", dateOfJourney.replace("\n", ""));
            flightSearchDetails.put("destination", destinationCode);
            flightSearchDetails.put("origin", sourceCode);
            if (intentional) {
                JSONObject flightSearchSecond = new JSONObject();
                flightSearchSecond.put("beginDate", dateOfReturn);
                flightSearchSecond.put("destination", sourceCode);
                flightSearchSecond.put("origin", destinationCode);
                contingFlight.put("flightSearchDetails", new JSONArray().put(flightSearchDetails).put(flightSearchSecond));
            } else {
                contingFlight.put("flightSearchDetails", new JSONArray().put(flightSearchDetails));
            }
            JSONArray flighDetailsArray = new JSONArray();
            for (int i = 0; i < materialEditAdultFirstName.size(); i++) {
                JSONObject travellerAdultDetails = new JSONObject();
                travellerAdultDetails.put("fName", materialEditAdultFirstName.get(i).getText().toString());
                travellerAdultDetails.put("lName", materialEditAdultLastName.get(i).getText().toString());
                String gender = materialEditAdultAge.get(i).getText().toString();
                if (gender.equalsIgnoreCase("MALE")) {
                    travellerAdultDetails.put("title", "Mr");
                    travellerAdultDetails.put("gender", gender);

                } else {
                    travellerAdultDetails.put("title", "Miss");
                    travellerAdultDetails.put("gender", gender);
                }
                if (travellerAdultDetails.has("type")) {
                    travellerAdultDetails.remove("type");
                    travellerAdultDetails.put("type", "Adult");
                } else {
                    travellerAdultDetails.put("type", "Adult");
                }
                travellerAdultDetails.put("dob", materialEditAdultDOB.get(i).getText().toString());
                if (intentionaloneway) {
                    travellerAdultDetails.put("passNo", materialEditAdultPassport.get(i).getText().toString());
                    travellerAdultDetails.put("passExpDate", materialEditAdultExpriDate.get(i).getText().toString());
                } else {
                    travellerAdultDetails.put("passNo", "");
                    travellerAdultDetails.put("passExpDate", "");
                }
                flighDetailsArray.put(travellerAdultDetails);

            }

            for (int i = 0; i < materialEditChildFirstName.size(); i++) {
                JSONObject travellerAdultDetails = new JSONObject();
                travellerAdultDetails.put("fName", materialEditChildFirstName.get(i).getText().toString());
                travellerAdultDetails.put("lName", materialEditChildLastName.get(i).getText().toString());
                String gender = materialEditChildAge.get(i).getText().toString();
                if (gender.equalsIgnoreCase("MALE")) {
                    travellerAdultDetails.put("title", "Mr");
                    travellerAdultDetails.put("gender", gender);

                } else {
                    travellerAdultDetails.put("title", "Miss");
                    travellerAdultDetails.put("gender", gender);
                }
                if (travellerAdultDetails.has("type")) {
                    travellerAdultDetails.remove("type");
                    travellerAdultDetails.put("type", "Child");
                } else {
                    travellerAdultDetails.put("type", "Child");
                }
                travellerAdultDetails.put("dob", materialEditChildDOB.get(i).getText().toString());

                if (intentionaloneway) {
                    travellerAdultDetails.put("passNo", materialEditChildPassport.get(i).getText().toString());
                    travellerAdultDetails.put("passExpDate", materialEditChildExpriDate.get(i).getText().toString());
                } else {
                    travellerAdultDetails.put("passNo", "");
                    travellerAdultDetails.put("passExpDate", "");
                }
                flighDetailsArray.put(travellerAdultDetails);

            }

            for (int i = 0; i < materialEditinfantfirstName.size(); i++) {
                JSONObject travellerAdultDetails = new JSONObject();
                travellerAdultDetails.put("fName", materialEditinfantfirstName.get(i).getText().toString());
                travellerAdultDetails.put("lName", materialEditinfantLastName.get(i).getText().toString());
                String gender = materialEditinfantAge.get(i).getText().toString();
                if (gender.equalsIgnoreCase("MALE")) {
                    travellerAdultDetails.put("title", "Mr");
                    travellerAdultDetails.put("gender", gender);

                } else {
                    travellerAdultDetails.put("title", "Miss");
                    travellerAdultDetails.put("gender", gender);
                }
                if (travellerAdultDetails.has("type")) {
                    travellerAdultDetails.remove("type");
                    travellerAdultDetails.put("type", "Infant");
                } else {
                    travellerAdultDetails.put("type", "Infant");
                }
                travellerAdultDetails.put("dob", materialEditinfantDOB.get(i).getText().toString());
                if (intentionaloneway) {
                    travellerAdultDetails.put("passNo", materialEditinfantPassport.get(i).getText().toString());
                    travellerAdultDetails.put("passExpDate", materialEditinfantExpriDate.get(i).getText().toString());
                } else {
                    travellerAdultDetails.put("passNo", "");
                    travellerAdultDetails.put("passExpDate", "");
                }
                flighDetailsArray.put(travellerAdultDetails);

            }
            contingFlight.put("travellerDetails", flighDetailsArray);
            contingFlight.put("sessionId", session.getUserSessionId());

            JSONObject jsonObject1 = new JSONObject();
            jsonObject1.put("Tickets", travel);
            String details1 = jsonObject1.toString();
            contingFlight.put("ticketDetails", new JSONObject(details1).toString());
//            }
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (contingFlight != null) {

            attemptLoad();

      /*Intent intent = new Intent(FlightBookingFormActivity.this, FlightPaymentActivity.class);
      intent.putExtra("Amount", Double.valueOf(latestfare));
      intent.putExtra("flightbooking", contingFlight.toString());
      intent.putExtra("conting", true);
      intent.putExtra("roundTrip", false);
      startActivity(intent);*/
        }
    }

    private void attemptLoad() {
//        loadingDialog.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("paymentMethod", "Wallet");

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_FLIGHT_BOOKING, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        if (response.has("code")) {
                            String code = response.getString("code");
                            if (code != null && code.equals("S00")) {
//                                loadingDialog.dismiss();
                                sendRefresh();
                                loadDlg.dismiss();
                                showSuccessDialog();

                            } else if (code != null && code.equals("F03")) {
                                loadDlg.dismiss();
                                showInvalidSessionDialog();

                            } else if (code != null && code.equals("T01")) {
                                CustomToast.showMessage(FlightBookingFormActivity.this, "Insufficient balancey");

                            } else {
                                String message = response.getString("message");
                                loadDlg.dismiss();
                                CustomToast.showMessage(FlightBookingFormActivity.this, message);

                            }
                        }
                    } catch (JSONException e) {
//                        loadingDialog.dismiss();
                        CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception2));
                        e.printStackTrace();
                    }

                }
            }, error -> {
//                    loadingDialog.dismiss();
                CustomToast.showMessage(getApplicationContext(), NetworkErrorHandler.getMessage(error, getApplicationContext()));
                error.printStackTrace();
            }) {
            };

            int socketTimeout = 600000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    private void sendRefresh() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "1");
        LocalBroadcastManager.getInstance(FlightBookingFormActivity.this).sendBroadcast(intent);
    }


    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(FlightBookingFormActivity.this).sendBroadcast(intent);
    }


    public void showInvalidSessionDialog() {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(AppMetadata.getInvalidSession(), Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(AppMetadata.getInvalidSession());
        }
        CustomAlertDialog builder = new CustomAlertDialog(FlightBookingFormActivity.this, R.string.dialog_title2, result);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    public void showSuccessDialog() {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(getSuccessMessage(), Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(getSuccessMessage());
        }
        CustomSuccessDialog builder = new CustomSuccessDialog(FlightBookingFormActivity.this, "Booked Successful", result);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                sendRefresh();
                Intent i = new Intent(FlightBookingFormActivity.this, MainActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                finish();
                startActivity(i);
            }
        });
        builder.show();
    }

    public String getSuccessMessage() {
        String source =
                "<b><font color=#000000> Airline Name </font></b>" + "<font color=#000000>" + engineId + "</font><br>" +
                        "<b><font color=#000000> Date: </font></b>" + "<font>" + dateOfJourney + "</font><br>" +
                        "<b><font color=#000000> Please check email for detail: </font></b>" + "<font>";
        return source;
    }

    public void getAdult(final MaterialEditText materialEditText) {
        final String[] chars = {"MALE", "FEMALE"};
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(FlightBookingFormActivity.this);
        alertDialog.setItems(chars, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                materialEditText.setText(chars[i]);
                dialogInterface.dismiss();
            }
        });
        alertDialog.show();

    }

    public void getDob(final MaterialEditText materialEditText) {
        new DatePickerDialog(FlightBookingFormActivity.this, R.style.DatePicker, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                String formattedDay, formattedMonth;
                if (dayOfMonth < 10) {
                    formattedDay = "0" + dayOfMonth;
                } else {
                    formattedDay = dayOfMonth + "";
                }

                if ((monthOfYear + 1) < 10) {
                    formattedMonth = "0" + String.valueOf(monthOfYear + 1);
                } else {
                    formattedMonth = String.valueOf(monthOfYear + 1) + "";
                }
                materialEditText.setText(String.valueOf(year) + "-" + String.valueOf(formattedMonth) + "-" + String.valueOf(formattedDay));
            }
        }, 1990, 01, 01).show();

    }
}
