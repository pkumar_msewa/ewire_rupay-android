package in.MadMoveGlobal.EwireRuPay.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.crashlytics.android.Crashlytics;
import com.orm.query.Select;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.custom.CustomAlertDialog;
import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.metadata.AppMetadata;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.util.NetworkErrorHandler;
import io.fabric.sdk.android.Fabric;

/**
 * Created by Ksf on 5/6/2016.
 */
public class PromoActivity extends AppCompatActivity {
    public static final int PICK_CONTACT = 1;
    private Button btnSubmit;
    private MaterialEditText etPromocode;
    private TextView tc1, tc2, tc3, tc4;

    private boolean cancel;
    private View focusView;
    private ImageButton ibFundTransferPhoneBook;
    private UserModel session = UserModel.getInstance();
    private LinearLayout llLayouts;

    private JSONObject jsonRequest;
    private LoadingDialog loadDlg;
    private Toolbar toolbar;
    private ImageButton ivBackBtn;

    //Volley Tag
    private String tag_json_obj = "json_invite_freinds";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        // TODO: Move this to where you establish a user session
        logUser();

        setContentView(R.layout.activity_promo);
        loadDlg = new LoadingDialog(PromoActivity.this);
        etPromocode = (MaterialEditText) findViewById(R.id.etPromocode);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        tc1 = (TextView) findViewById(R.id.tvterm1);
        tc1 = (TextView) findViewById(R.id.tvterm2);
        tc1 = (TextView) findViewById(R.id.tvterm3);
        tc1 = (TextView) findViewById(R.id.tvterm4);


        toolbar = (Toolbar) findViewById(R.id.toolbars);
        ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtns);
        setSupportActionBar(toolbar);
        ivBackBtn.setVisibility(View.VISIBLE);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent shoppingIntent = new Intent(PromoActivity.this, MainActivity.class);
                startActivity(shoppingIntent);
                finish();
            }
        });


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!validatePromocode()) {
                    return;
                }

                loadDlg.show();
                promoCode();
            }
        });


    }


    private void promoCode() {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {

            jsonRequest.put("promoCode", etPromocode.getText().toString());
            jsonRequest.put("sessionId", Select.from(UserModel.class).first().getUserSessionId());


        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_PROMO_CODE, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.i("Invite RESPONSE", response.toString());
                        String code = response.getString("code");
                        String message = response.getString("message");
                        loadDlg.dismiss();
                        if (code != null && code.equals("S00")) {
                            loadDlg.dismiss();


                        } else if (code != null && code.equals("F03")) {
                            loadDlg.dismiss();

//                            Intent shoppingIntent = new Intent(PromoActivity.this, MainActivity.class);
//                            startActivity(shoppingIntent);
                            CustomToast.showMessage(PromoActivity.this, getResources().getString(R.string.No_internet_connection));
                            sendLogout();
                        } else {
                            if (response.has("message") && response.getString("message") != null) {

                                CustomToast.showMessage(PromoActivity.this, message);
                            }
                        }

                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(PromoActivity.this, getResources().getString(R.string.server_exception2));

                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    error.printStackTrace();
                    CustomToast.showMessage(PromoActivity.this, NetworkErrorHandler.getMessage(error, PromoActivity.this));


                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(this, R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton(getResources().getString(R.string.agree), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton(getResources().getString(R.string.Dismiss), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    @Override
    public void onBackPressed() {


        Intent shoppingIntent = new Intent(PromoActivity.this, MainActivity.class);
        startActivity(shoppingIntent);
        finish();

    }


    private boolean validatePromocode() {
        if (etPromocode.getText().toString().trim().isEmpty()) {
            etPromocode.setError(getResources().getString(R.string.Enter_valid_promo_code));
            requestFocus(etPromocode);
            return false;
        } else {
            etPromocode.setError(null);
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        Crashlytics.setUserIdentifier(Select.from(UserModel.class).first().getUserMobileNo());
        Crashlytics.setUserEmail(Select.from(UserModel.class).first().getUserEmail());
        Crashlytics.setUserName(Select.from(UserModel.class).first().getUserFirstName() + " " + Select.from(UserModel.class).first().getUserLastName());
    }

}