package in.MadMoveGlobal.EwireRuPay.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.crashlytics.android.Crashlytics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.MadMoveGlobal.custom.CustomAlertDialog;
import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.custom.LoadMoreListView;
import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.fragment.fragmentnaviagtionitems.ReceiptFragment;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.metadata.AppMetadata;
import in.MadMoveGlobal.model.StatementModel;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;
import io.fabric.sdk.android.Fabric;


/**
 * Created by kashifimam on 21/02/17.
 */

public class ReciptTransActivity extends AppCompatActivity {
    CharSequence TitlesEnglish[] = {"Physical Card", "Virtual Card"};
    int NumbOfTabs = 2;
    private TabLayout mSlidingTabLayout;
    private UserModel session = UserModel.getInstance();
    private ViewPager mainPager;
    private FragmentManager fragmentManager;
    private int currentPage = 1;
    private int totalPage = 0;

    private BroadcastReceiver mSignUpReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            mainPager.setCurrentItem(1);

        }
    };
    private JSONObject jsonRequest;
    private JsonObjectRequest postReq;
    private ArrayList<StatementModel> phyRecipt;
    private ArrayList<StatementModel> verRecipt;
    private String tag_json_obj;
    private ImageButton ivBackBtn;
    private LoadingDialog loadDlg;
    private Toolbar toolbar;
    private LoadMoreListView lvReceipt;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        // TODO: Move this to where you establish a user session
        logUser();

        phyRecipt = new ArrayList<>();
        verRecipt = new ArrayList<>();
        fragmentManager = getSupportFragmentManager();
        setContentView(R.layout.activity_tab_layout);
        lvReceipt = (LoadMoreListView) findViewById(R.id.lvReceipt);
        toolbar = (Toolbar) findViewById(R.id.toolbars);
        ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtns);
        loadDlg = new LoadingDialog(this);
        loadUserStatement();

        Log.i("sessionID", session.getUserSessionId());
//        try {
//            loadMoreUserStatement();
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }




//        LocalBroadcastManager.getInstance(this).registerReceiver(mSignUpReceiver, new IntentFilter("sign-up"));

        setSupportActionBar(toolbar);
        ivBackBtn.setVisibility(View.VISIBLE);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ReciptTransActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    public class ViewPagerAdapter extends FragmentPagerAdapter {
        CharSequence Titles[]; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
        int NumbOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created

        ArrayList<StatementModel> verRecipt;
        ArrayList<StatementModel> phyRecipt;

        // Build a Constructor and assign the passed Values to appropriate values in the class
        public ViewPagerAdapter(FragmentManager fm, ArrayList<StatementModel> verRecipt, ArrayList<StatementModel> phyRecipt, CharSequence[] mTitles, int mNumbOfTabsumb) {
            super(fm);
            this.Titles = mTitles;
            this.verRecipt = verRecipt;
            this.phyRecipt = phyRecipt;
            this.NumbOfTabs = mNumbOfTabsumb;

        }

        @Override
        public Fragment getItem(int position) {
            if (phyRecipt.size() != 0 && verRecipt.size() != 0) {
                if (position == 0) {
                    ReceiptFragment tab1 = new ReceiptFragment();
                    Bundle bundle = new Bundle();
                    bundle.putParcelableArrayList("data", phyRecipt);
                    bundle.putString("type", "physical");
                    bundle.putString("page", String.valueOf(currentPage));
                    tab1.setArguments(bundle);
                    return tab1;
                } else {
                    ReceiptFragment tab2 = new ReceiptFragment();
                    Bundle bundle = new Bundle();
                    bundle.putParcelableArrayList("data", verRecipt);
                    bundle.putString("page", String.valueOf(currentPage));
                    bundle.putString("type", "virtual");
                    tab2.setArguments(bundle);
                    return tab2;
                }
            } else if (verRecipt.size() != 0) {
                ReceiptFragment tab1 = new ReceiptFragment();
                Bundle bundle = new Bundle();
                bundle.putString("type", "virtual");
                bundle.putString("page", String.valueOf(currentPage));
                bundle.putParcelableArrayList("data", verRecipt);
                tab1.setArguments(bundle);
                return tab1;
            } else if (phyRecipt.size() != 0) {
                ReceiptFragment tab2 = new ReceiptFragment();
                Bundle bundle = new Bundle();
                bundle.putString("type", "physical");
                bundle.putString("page", String.valueOf(currentPage));
                bundle.putParcelableArrayList("data", phyRecipt);
                tab2.setArguments(bundle);
                return tab2;

            }

            return null;

        }

        @Override
        public CharSequence getPageTitle(int position) {
            return Titles[position];
        }

        @Override
        public int getCount() {
            return Titles.length;
        }
    }


    public void loadUserStatement() {
        loadDlg.show();

//        pbReceipt.setVisibility(View.VISIBLE);
        lvReceipt.setVisibility(View.GONE);
        jsonRequest = new JSONObject();
        try {

            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("page", currentPage);

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            Log.i("Receipt Url", ApiUrl.URL_RECEIPT);
            postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_RECEIPT, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject JsonObj) {
                    try {


//        Log.i("Receipt Url", ApiUrl.URL_RECEIPT);
//        postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_RECEIPT, new JSONObject().put("sessionId", session.getUserSessionId()),new JSONObject().put("page",page), new Response.Listener<JSONObject>() {
//            @Override
//            public void onResponse(JSONObject JsonObj) {
//                try {

                        Log.i("Receipts Response", JsonObj.toString());
                        String message = JsonObj.getString("message");
                        String code = JsonObj.getString("code");

                        if (code != null && code.equals("S00")) {
                            sendRefresh();
                            loadDlg.dismiss();
                            if (!JsonObj.isNull("customPhysicalTransaction")) {
//                                String details = JsonObj.getString("physicalCardTransactions");
//                                JSONObject jsonDetails = new JSONObject(details);
                                JSONArray operatorArray = JsonObj.getJSONArray("customPhysicalTransaction");
                                for (int i = 0; i < operatorArray.length(); i++) {
                                    JSONObject c = operatorArray.getJSONObject(i);
                                    String indicator = c.getString("indicator");
                                    String date = c.getString("date");
                                    String amount = c.getString("amount");
                                    String description = c.getString("message");;
//                                    if (c.getJSONObject("details").has("merchantname")) {
//                                        description = c.getJSONObject("details").getString("merchantname");
//                                    } else {
//                                        description = c.getString("description");
//                                    }

                                    String status = c.getString("status");
                                    String refNo = "";
                                    String authNo = "";
                                    StatementModel statementModel = new StatementModel(indicator, date, amount, description, status, refNo, authNo);
                                    phyRecipt.add(statementModel);
                                }
                            }
                            if (!JsonObj.isNull("customVirtualTransaction")) {
//                                String virTrans = JsonObj.getString("virtualTransactions");
//                                JSONObject jsonvirt = new JSONObject(virTrans);
                                JSONArray operatorVArray = JsonObj.getJSONArray("customVirtualTransaction");
                                for (int i = 0; i < operatorVArray.length(); i++) {
                                    JSONObject d = operatorVArray.getJSONObject(i);
                                    String indicator = d.getString("indicator");
                                    String date = d.getString("date");
                                    String amount = d.getString("amount");
                                    String description = d.getString("message");;
                                    String status = d.getString("status");
                                    StatementModel statementModel = new StatementModel(indicator, date, amount, description, status, "", "");
                                    verRecipt.add(statementModel);

                                }
                            }
                            mainPager = (ViewPager) findViewById(R.id.mainPager);
                            mSlidingTabLayout = (TabLayout) findViewById(R.id.sliding_tabs_home);
                            if (phyRecipt.size() != 0 && verRecipt.size() != 0) {
                                mainPager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager(), verRecipt, phyRecipt, TitlesEnglish, NumbOfTabs));
                                mSlidingTabLayout.setupWithViewPager(mainPager);
                            } else if (verRecipt.size() != 0) {
                                TitlesEnglish = new CharSequence[]{"Virtual Card"};
                                mainPager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager(), verRecipt, phyRecipt, TitlesEnglish, TitlesEnglish.length));
                                mSlidingTabLayout.setupWithViewPager(mainPager);

                            } else if (phyRecipt.size() != 0) {
                                TitlesEnglish = new CharSequence[]{"Physical Card"};
                                mainPager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager(), verRecipt, phyRecipt, TitlesEnglish, TitlesEnglish.length));
                                mSlidingTabLayout.setupWithViewPager(mainPager);

                            }


                        } else if (code != null && code.equals("F03")) {
                            loadDlg.dismiss();
//                        showInvalidSessionDialog();
                            CustomToast.showMessage(ReciptTransActivity.this, "Please login and try again");
//                        Intent intent = new Intent(ReciptTransActivity.this, MainActivity.class);
//                        startActivity(intent);
                            sendLogout();

                        } else {
                            loadDlg.dismiss();
                        }
                        lvReceipt.onLoadMoreComplete();

                    } catch (JSONException e) {
                        e.printStackTrace();
                        loadDlg.dismiss();

                        lvReceipt.onLoadMoreComplete();
                        CustomToast.showMessage(ReciptTransActivity.this, getResources().getString(R.string.server_exception2));
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    loadDlg.dismiss();

                    CustomToast.showMessage(ReciptTransActivity.this, "No internet connection");
                    lvReceipt.onLoadMoreComplete();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);


        }
    }


    public void loadMoreUserStatement() throws JSONException {
        loadDlg.show();
        currentPage = currentPage + 1;
//        pbReceipt.setVisibility(View.VISIBLE);
        lvReceipt.setVisibility(View.GONE);
        jsonRequest = new JSONObject();
        try {

            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("page", currentPage);

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", String.valueOf(currentPage));
            Log.i("Receipt Url", ApiUrl.URL_RECEIPT);
            postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_RECEIPT, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject JsonObj) {
                    try {


//        Log.i("Receipt Url", ApiUrl.URL_RECEIPT);
//        postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_RECEIPT, new JSONObject().put("sessionId", session.getUserSessionId()),new JSONObject().put("page",page), new Response.Listener<JSONObject>() {
//            @Override
//            public void onResponse(JSONObject JsonObj) {
//                try {

                        Log.i("Receipts Response", JsonObj.toString());
                        String message = JsonObj.getString("message");
                        String code = JsonObj.getString("code");

                        if (code != null && code.equals("S00")) {
                            sendRefresh();
                            loadDlg.dismiss();
                            if (!JsonObj.isNull("customPhysicalTransaction")) {
//                                String details = JsonObj.getString("physicalCardTransactions");
//                                JSONObject jsonDetails = new JSONObject(details);
                                JSONArray operatorArray = JsonObj.getJSONArray("customPhysicalTransaction");
                                for (int i = 0; i < operatorArray.length(); i++) {
                                    JSONObject c = operatorArray.getJSONObject(i);
                                    String indicator = c.getString("indicator");
                                    String date = c.getString("date");
                                    String amount = c.getString("amount");
                                    String description = c.getString("message");;
//                                    if (c.getJSONObject("details").has("merchantname")) {
//                                        description = c.getJSONObject("details").getString("merchantname");
//                                    } else {
//                                        description = c.getString("description");
//                                    }

                                    String status = c.getString("status");
                                    String refNo = "";
                                    String authNo = "";
                                    StatementModel statementModel = new StatementModel(indicator, date, amount, description, status, refNo, authNo);
                                    phyRecipt.add(statementModel);
                                }
                            }
                            if (!JsonObj.isNull("customVirtualTransaction")) {
//                                String virTrans = JsonObj.getString("virtualTransactions");
//                                JSONObject jsonvirt = new JSONObject(virTrans);
                                JSONArray operatorVArray = JsonObj.getJSONArray("customVirtualTransaction");
                                for (int i = 0; i < operatorVArray.length(); i++) {
                                    JSONObject d = operatorVArray.getJSONObject(i);
                                    String indicator = d.getString("indicator");
                                    String date = d.getString("date");
                                    String amount = d.getString("amount");
                                    String description = d.getString("message");;
                                    String status = d.getString("status");
                                    StatementModel statementModel = new StatementModel(indicator, date, amount, description, status, "", "");
                                    verRecipt.add(statementModel);

                                }

                            }
                            mainPager = (ViewPager) findViewById(R.id.mainPager);
                            mSlidingTabLayout = (TabLayout) findViewById(R.id.sliding_tabs_home);
                            if (phyRecipt.size() != 0 && verRecipt.size() != 0) {
                                mainPager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager(), verRecipt, phyRecipt, TitlesEnglish, NumbOfTabs));
                                mSlidingTabLayout.setupWithViewPager(mainPager);
                            } else if (verRecipt.size() != 0) {
                                TitlesEnglish = new CharSequence[]{"Virtual Card"};
                                mainPager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager(), verRecipt, phyRecipt, TitlesEnglish, TitlesEnglish.length));
                                mSlidingTabLayout.setupWithViewPager(mainPager);

                            } else if (phyRecipt.size() != 0) {
                                TitlesEnglish = new CharSequence[]{"Physical Card"};
                                mainPager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager(), verRecipt, phyRecipt, TitlesEnglish, TitlesEnglish.length));
                                mSlidingTabLayout.setupWithViewPager(mainPager);

                            }


                        } else if ("F00".equalsIgnoreCase(code)) {
                            totalPage = currentPage;
                            loadDlg.dismiss();
                        } else if (code != null && code.equals("F03")) {
                            loadDlg.dismiss();
//                        showInvalidSessionDialog();
                            CustomToast.showMessage(ReciptTransActivity.this, "Please login and try again");
//                        Intent intent = new Intent(ReciptTransActivity.this, MainActivity.class);
//                        startActivity(intent);
                            sendLogout();

                        } else {
                            loadDlg.dismiss();
                        }
                        lvReceipt.onLoadMoreComplete();

                    } catch (JSONException e) {
                        e.printStackTrace();
                        loadDlg.dismiss();

                        lvReceipt.onLoadMoreComplete();
                        CustomToast.showMessage(ReciptTransActivity.this, getResources().getString(R.string.server_exception2));
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    loadDlg.dismiss();

                    CustomToast.showMessage(ReciptTransActivity.this, "No internet connection");
                    lvReceipt.onLoadMoreComplete();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);


        }
    }

    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(ReciptTransActivity.this, R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void sendRefresh() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "1");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    public void onBackPressed() {


        Intent shoppingIntent = new Intent(ReciptTransActivity.this, MainActivity.class);
        startActivity(shoppingIntent);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        Crashlytics.setUserIdentifier(session.getUserMobileNo());
        Crashlytics.setUserEmail(session.getUserEmail());
        Crashlytics.setUserName(session.getUserFirstName() + " " + session.getUserLastName());
    }

}
