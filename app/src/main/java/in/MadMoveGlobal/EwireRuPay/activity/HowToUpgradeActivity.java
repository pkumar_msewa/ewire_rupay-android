package in.MadMoveGlobal.EwireRuPay.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;

import in.MadMoveGlobal.fragment.fragmenthowtoupgrade.OtherUserBankUpgradeFragment;
import in.MadMoveGlobal.fragment.fragmenthowtoupgrade.VijayaBankUserUpgradeFragment;
import in.MadMoveGlobal.EwireRuPay.R;

/**
 * Created by Ksf on 10/15/2016.
 */
public class HowToUpgradeActivity extends AppCompatActivity {

    CharSequence TitlesEnglish[] = null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upgrade_wallet);
        //press back button in toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbars);
        ImageButton ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtns);
        setSupportActionBar(toolbar);
        ivBackBtn.setVisibility(View.VISIBLE);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        TitlesEnglish = new CharSequence[]{getResources().getString(R.string.upgrade_vijaya_bank),getResources().getString(R.string.upgrade_other_bank)};
        FragmentManager fragmentManager = getSupportFragmentManager();
        ViewPager mainPager = (ViewPager) findViewById(R.id.mainPagerUpgrade);
        TabLayout mSlidingTabLayout = (TabLayout) findViewById(R.id.tabLayoutUpgrade);
        mainPager.setAdapter(new ViewPagerAdapter(fragmentManager, TitlesEnglish, 2));
        mSlidingTabLayout.setupWithViewPager(mainPager);

    }


    public class ViewPagerAdapter extends FragmentStatePagerAdapter {

        CharSequence Titles[]; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
        int NumbOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created


        // Build a Constructor and assign the passed Values to appropriate values in the class
        public ViewPagerAdapter(FragmentManager fm, CharSequence mTitles[], int mNumbOfTabsumb) {
            super(fm);
            this.Titles = mTitles;
            this.NumbOfTabs = mNumbOfTabsumb;

        }

        @Override
        public Fragment getItem(int position) {

            if (position == 0) {
                return new VijayaBankUserUpgradeFragment();
            } else {
                return new OtherUserBankUpgradeFragment();
            }

        }

        @Override
        public CharSequence getPageTitle(int position) {
            return Titles[position];
        }

        @Override
        public int getCount() {
            return NumbOfTabs;
        }
    }


}
