package in.MadMoveGlobal.EwireRuPay.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.MadMoveGlobal.custom.CustomAlertDialog;
import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.metadata.AppMetadata;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.util.CheckLog;
import in.MadMoveGlobal.util.NetworkErrorHandler;
import in.MadMoveGlobal.util.PayingDetailsValidation;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;

/**
 * Created by Ksf on 5/6/2016.
 */
public class InviteFriendActivity extends AppCompatActivity {
    public static final int PICK_CONTACT = 1;
    private Button btnInvite,btnShare;
    private MaterialEditText etInviteName;
    private MaterialEditText etInviteNumber;
    private String inviteNumber;
    private String inviteName;
    private boolean cancel;
    private View focusView;
    private ImageButton ibFundTransferPhoneBook;
    private UserModel session = UserModel.getInstance();
    private LinearLayout llLayouts;

    private JSONObject jsonRequest;
    private LoadingDialog loadDlg;
    private Toolbar toolbar;
    private ImageButton ivBackBtn;

    //Volley Tag
    private String tag_json_obj = "json_invite_freinds";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invitefriend);
        loadDlg = new LoadingDialog(InviteFriendActivity.this);
        etInviteNumber = (MaterialEditText) findViewById(R.id.etInviteNumber);
        etInviteName = (MaterialEditText) findViewById(R.id.etInviteName);
        llLayouts = (LinearLayout) findViewById(R.id.llLayouts);
        btnInvite = (Button) findViewById(R.id.btnInvite);
        btnShare = (Button) findViewById(R.id.btnShare);

        toolbar = (Toolbar) findViewById(R.id.toolbars);
        ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtns);
        setSupportActionBar(toolbar);
        ivBackBtn.setVisibility(View.VISIBLE);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent shoppingIntent = new Intent(InviteFriendActivity.this, MainActivity.class);
                startActivity(shoppingIntent);
                finish();
            }
        });

        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent2 = new Intent(Intent.ACTION_SEND);
                intent2.setType("text/plain");
                intent2.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.Hey)+"https://play.google.com/store/apps/details?id=in.MadMoveGlobal.EwireRuPay");
                startActivity(Intent.createChooser(intent2, "Share via"));

            }
        });

        ibFundTransferPhoneBook = (ImageButton) findViewById(R.id.ibFundTransferPhoneBook);

        btnInvite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptInviteFriend();

            }
        });




        //DONE CLICK ON VIRTUAL KEYPAD
        etInviteName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    attemptInviteFriend();
                }
                return false;
            }
        });

        ibFundTransferPhoneBook.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                startActivityForResult(intent, PICK_CONTACT);
            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        String[] projection = {ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.NUMBER};
        switch (requestCode) {
            case (PICK_CONTACT):
                if (resultCode == Activity.RESULT_OK) {
                    Uri contactUri = data.getData();
                    Cursor c = InviteFriendActivity.this.getContentResolver().query(
                            contactUri, projection, null, null, null);
                    if (c != null) {
                        if (c.moveToFirst()) {
                            String name = c.getString(c
                                    .getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                            etInviteName.setText(name);
                            String phoneNumber = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            String finalNumber = phoneNumber.replaceAll("[^0-9\\+]", "");
                            if (finalNumber != null && !finalNumber.isEmpty()) {
                                removeCountryCode(finalNumber);
                            }
                        }
                        c.close();
                    }
                }
        }
    }

    private void attemptInviteFriend() {
        etInviteNumber.setError(null);
        etInviteName.setError(null);
        cancel = false;

        inviteNumber = etInviteNumber.getText().toString();
        inviteName = etInviteName.getText().toString();


        checkInviteName(inviteName);
        checkInviteNumber(inviteNumber);


        if (cancel) {
            focusView.requestFocus();
        } else {

//            Intent intent2 = new Intent(); intent2.setAction(Intent.ACTION_SEND);
//            intent2.setType("text/plain");
//            intent2.putExtra(Intent.EXTRA_TEXT, "Hey!! I just started using PaulPay and its awesome. You should try using it as well! Click here to download the app: https://play.google.com/store/apps/details?id=in.msspay.paulpay");
//            startActivity(Intent.createChooser(intent2, "Share via"));
//            promoteInviteFriend();
            promoteInviteFriend();
        }
    }

    private void checkInviteNumber(String inviteNumber) {
        CheckLog inviteNumberCheckLog = PayingDetailsValidation.checkMobileTenDigit(inviteNumber);
        if (!inviteNumberCheckLog.isValid) {
//            etInviteNumber.setError(getString(inviteNumberCheckLog.msg));
            Snackbar.make(llLayouts, getString(inviteNumberCheckLog.msg), Snackbar.LENGTH_LONG).show();
            focusView = etInviteNumber;
            cancel = true;
        }

    }

    private void checkInviteName(String inviteName) {
        CheckLog inviteNameCheckLog = PayingDetailsValidation.checkFirstName(inviteName);
        if (!inviteNameCheckLog.isValid) {
//            etInviteName.setError(getString(inviteNameCheckLog.msg));
            Snackbar.make(llLayouts, getString(inviteNameCheckLog.msg), Snackbar.LENGTH_LONG).show();
            focusView = etInviteName;
            cancel = true;
        }

    }

    private void removeCountryCode(String number) {
        if (hasCountryCode(number)) {
            int country_digits = number.length() - 10;
            number = number.substring(country_digits);
            etInviteNumber.setText(number);
        } else if (hasZero(number)) {
            if (number.length() >= 10) {
                int country_digits = number.length() - 10;
                number = number.substring(country_digits);
                etInviteNumber.setText(number);
            } else {
//                CustomToast.showMessage(InviteFriendActivity.this, "Please select 10 digit no")
                Snackbar.make(llLayouts, getResources().getString(R.string.Please_select_10_digit_no), Snackbar.LENGTH_LONG).show();;
            }

        } else {
            etInviteNumber.setText(number);
        }

    }

    private boolean hasZero(String number) {
        return number.charAt(0) == '0';
    }

    private boolean hasCountryCode(String number) {
        return number.charAt(0) == '+';
    }

    private void promoteInviteFriend() {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("receiversName", etInviteName.getText().toString());
            jsonRequest.put("mobileNo", etInviteNumber.getText().toString());
            jsonRequest.put("message", "Hi! I would like to recommend Ewire to you.");

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_INVITE_FRIENDS, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        String code = response.getString("code");
                        loadDlg.dismiss();
                        if (code != null && code.equals("S00")) {
                            loadDlg.dismiss();
                            etInviteName.getText().clear();
                            etInviteNumber.getText().clear();
                            if (response.has("message") && response.getString("message") != null) {
                                String message = response.getString("message");
                                CustomToast.showMessage(InviteFriendActivity.this, message);
                            }

                        } else if (code != null && code.equals("F03")) {
                            loadDlg.dismiss();
                            CustomToast.showMessage(InviteFriendActivity.this, getResources().getString(R.string.Please_login_and_try_again));
                            sendLogout();
                        } else {
                            if (response.has("message") && response.getString("message") != null) {
                                String message = response.getString("message");
                                CustomToast.showMessage(InviteFriendActivity.this, message);
                            }
                        }

                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(InviteFriendActivity.this, getResources().getString(R.string.server_exception2));

                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    error.printStackTrace();
                    CustomToast.showMessage(InviteFriendActivity.this, NetworkErrorHandler.getMessage(error, InviteFriendActivity.this));


                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(this, R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton(getResources().getString(R.string.agree), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton(getResources().getString(R.string.Dismiss), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    @Override
    public void onBackPressed() {


        Intent shoppingIntent = new Intent(InviteFriendActivity.this, MainActivity.class);
        startActivity(shoppingIntent);
        finish();
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawer(GravityCompat.START);
//        } else {
//            android.support.v7.app.AlertDialog.Builder exitDialog =
//                    new android.support.v7.app.AlertDialog.Builder(InviteFriendActivity.this, R.style.AppCompatAlertDialogStyle);
//            exitDialog.setTitle("Do you really want to Exit?");
//            exitDialog.setPositiveButton("No", new DialogInterface.OnClickListener() {
//                public void onClick(DialogInterface dialog, int id) {
//                    dialog.dismiss();
//                }
//            });
//            exitDialog.setNegativeButton("Yes", new DialogInterface.OnClickListener() {
//                public void onClick(DialogInterface dialog, int id) {
//                    dialog.dismiss();
//                    finishAffinity();
//                    finish();
//                }
//            });
//            exitDialog.show();
//        }
    }

}