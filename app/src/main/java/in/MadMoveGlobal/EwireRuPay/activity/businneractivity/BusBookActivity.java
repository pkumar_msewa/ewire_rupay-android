package in.MadMoveGlobal.EwireRuPay.activity.businneractivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import in.MadMoveGlobal.custom.CustomAlertDialog;
import in.MadMoveGlobal.custom.CustomSuccessDialog;
import in.MadMoveGlobal.custom.CustomSuccessDialogBus;
import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.custom.LoadingDialog;
//import in.paulpay.fragment.LoadMoneyFragment;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.metadata.AppMetadata;
import in.MadMoveGlobal.model.BusBoardingPointModel;
import in.MadMoveGlobal.model.BusDropingPointModel;
import in.MadMoveGlobal.model.BusListModel;
import in.MadMoveGlobal.model.BusSeatModel;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.util.CheckLog;
import in.MadMoveGlobal.util.NetworkErrorHandler;
import in.MadMoveGlobal.util.PayingDetailsValidation;
import in.MadMoveGlobal.util.SecurityUtil;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;


/**
 * Created by Kashif-PC on 11/21/2016.
 */
public class BusBookActivity extends AppCompatActivity {

    private LinearLayout llBusBookPassengers;
    private MaterialEditText etBusBookEmail, etBusBookMobile, etBusBookAddress, etBusBookPostal;
    private MaterialEditText etBusBookState, etBusBookCity;
    private TextView MyBallance;
    private MaterialEditText etBusBookIDProofType, etBusBookIDProofCardNo, etBusBookIDProofIssuedBy;
    private MaterialEditText etBusBookBoardingLocation, etBusBookDroppingLocation;
    String amount;

    private BusBoardingPointModel boardingPointModels;
    private BusDropingPointModel droppingPointModels;

    private long desId, sourceId;

    private HashMap<String, BusSeatModel> seatSelectedMap;

    private String boardingPointId,messagess, droppingPointId, boardingTimePrime, droppingTimePrime, boardingTime, droppingTime, GetTxnIdUpdated;
    private ImageButton ivLogo;

    private boolean cancel;
    private View focusView;

    private ArrayList<MaterialEditText> listEtPassengerFName = new ArrayList<>();
    private ArrayList<MaterialEditText> listEtPassengerLName = new ArrayList<>();
    private ArrayList<MaterialEditText> listEtPassengerAge = new ArrayList<>();
    private ArrayList<MaterialEditText> listEtPassengerGender = new ArrayList<>();
    private ArrayList<MaterialEditText> listEtPassengerTitle = new ArrayList<>();
    private ArrayList<String> listSeatNo = new ArrayList<>();
    private ArrayList<String> listSeatType = new ArrayList<>();
    private ArrayList<String> listFare = new ArrayList<>();
    private ArrayList<String> listSeatId = new ArrayList<>();

    private LoadingDialog loadingDialog;
    private JSONObject jsonRequest;
    private String tag_json_obj = "json_travel";

    private String genderParam = "";
    private String ageParam = "";
    private String nameParam = "";

    private String titleParam = "";
    private String seatNoParam = "";
    private String seatCodeParam = "";
    private String seatDeatils = "";
    private String totalAmount = "";


    private String fareParam = "";
    private String serviceChargeParam = "";
    private String serviceTaxParam = "";

    private BusListModel busListModel;

    private String refNo = "12345";

    private UserModel session = UserModel.getInstance();
    private double totalamount;
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getStringExtra("ticket");
            if (action.equals("1")) {
                finish();
            }

        }
    };

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        loadingDialog = new LoadingDialog(BusBookActivity.this);
        setContentView(R.layout.activity_bus_booking);

        //press back button in toolbar


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbars);
        ImageButton ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtns);
        setSupportActionBar(toolbar);
        ivBackBtn.setVisibility(View.VISIBLE);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        ImageButton ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtn);
//        setSupportActionBar(toolbar);
//        ivBackBtn.setVisibility(View.VISIBLE);
//        ivBackBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                onBackPressed();
//            }
//        });
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("booking-done"));
        llBusBookPassengers = (LinearLayout) findViewById(R.id.llBusBookPassengers);
        etBusBookEmail = (MaterialEditText) findViewById(R.id.etBusBookEmail);
        etBusBookMobile = (MaterialEditText) findViewById(R.id.etBusBookMobile);
        etBusBookAddress = (MaterialEditText) findViewById(R.id.etBusBookAddress);
        etBusBookPostal = (MaterialEditText) findViewById(R.id.etBusBookPostal);
        etBusBookCity = (MaterialEditText) findViewById(R.id.etBusBookCity);
        etBusBookState = (MaterialEditText) findViewById(R.id.etBusBookState);

        if (session.getUserEmail() != null) {
            etBusBookEmail.setText(session.getUserEmail());
        }
        if (session.getUserMobileNo() != null) {
            String mobn = session.getUserMobileNo();
//            String mnob = mobn.substring(2, mobn.length());
            etBusBookMobile.setText(mobn);
        }
//        if (session.getUserAddress() != null) {
//            etBusBookAddress.setText(session.getUserAddress());
//        } else
//            etBusBookAddress.setText("");


        etBusBookIDProofType = (MaterialEditText) findViewById(R.id.etBusBookIDProofType);
        etBusBookIDProofCardNo = (MaterialEditText) findViewById(R.id.etBusBookIDProofCardNo);
        etBusBookIDProofIssuedBy = (MaterialEditText) findViewById(R.id.etBusBookIDProofIssuedBy);

        etBusBookBoardingLocation = (MaterialEditText) findViewById(R.id.etBusBookBoardingLocation);
        etBusBookDroppingLocation = (MaterialEditText) findViewById(R.id.etBusBookDroppingLocation);

        droppingPointModels = BusDetailActivity.dropingModel;
        boardingPointModels = BusDetailActivity.busBoardingModel;
        GetTxnIdUpdated = BusDetailActivity.GetTxnIdUpdated;
        Button btnBusBook = (Button) findViewById(R.id.btnBusBook);

        desId = BusDetailActivity.desId;
        sourceId = BusDetailActivity.sourceId;
        seatSelectedMap = BusDetailActivity.seathashValue;
        busListModel = BusDetailActivity.busModel;
        etBusBookBoardingLocation.setText(boardingPointModels.getBdlocation());
        etBusBookDroppingLocation.setText(droppingPointModels.getLocatoin());
//    etBusBookDroppingLocation.setOnClickListener(new View.OnClickListener() {
//      @Override
//      public void onClick(View view) {
//        showDroppingDialog();
//      }
//    });

//    etBusBookBoardingLocation.setOnClickListener(new View.OnClickListener() {
//      @Override
//      public void onClick(View view) {
//        showBoardingDialog();
//      }
//    });
//
        etBusBookIDProofType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String[] items = {"Pan Card", "Licence", "Adhar Card", "Passport", "Other"};
                android.support.v7.app.AlertDialog.Builder idDialog =
                        new android.support.v7.app.AlertDialog.Builder(BusBookActivity.this, R.style.AppCompatAlertDialogStyle);
                idDialog.setTitle("Select your Id type");
                idDialog.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
                idDialog.setItems(items, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int position) {
                        etBusBookIDProofType.setText(items[position].toString().trim());
                    }

                });

                idDialog.show();
            }
        });

        etBusBookIDProofIssuedBy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String[] items = {"Government", "Other"};
                android.support.v7.app.AlertDialog.Builder idDialog =
                        new android.support.v7.app.AlertDialog.Builder(BusBookActivity.this, R.style.AppCompatAlertDialogStyle);
                idDialog.setTitle("Card Issued by");
                idDialog.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
                idDialog.setItems(items, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int position) {
                        etBusBookIDProofIssuedBy.setText(items[position].toString().trim());
                    }

                });
                idDialog.show();
            }
        });

        btnBusBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                genderParam = "";
                ageParam = "";
                nameParam = "";
                titleParam = "";
                seatNoParam = "";
                fareParam = "";
                serviceChargeParam = "";
                serviceTaxParam = "";
//                promoteBooking();
                validateFields();
            }
        });
        generateViewsForSeat();
    }

    private void validateFields() {
        etBusBookEmail.setError(null);
        etBusBookMobile.setError(null);
        etBusBookAddress.setError(null);
        etBusBookPostal.setError(null);
        etBusBookIDProofType.setError(null);
        etBusBookIDProofCardNo.setError(null);
        etBusBookIDProofIssuedBy.setError(null);
        etBusBookBoardingLocation.setError(null);
        etBusBookDroppingLocation.setError(null);
        cancel = false;

        checkEmail(etBusBookEmail.getText().toString());
        checkMobileNumber(etBusBookMobile.getText().toString());
        checkAddress(etBusBookAddress.getText().toString());
//        checkPostal(etBusBookPostal.getText().toString());
//        checkCity(etBusBookCity.getText().toString());
//        checkState(etBusBookState.getText().toString());


//        checkIdType(etBusBookIDProofType.getText().toString());
//        checkIdNo(etBusBookIDProofCardNo.getText().toString());
//        checkIdIssuedBy(etBusBookIDProofIssuedBy.getText().toString());

        checkBoardingPoint(etBusBookBoardingLocation.getText().toString());

        checkPassengerInfo();

        if (cancel) {
            focusView.requestFocus();
        } else {
            loadingDialog.show();
            getTransactionId();

        }
    }

    private void checkMobileNumber(String mNumber) {
        CheckLog inviteNumberCheckLog = PayingDetailsValidation.checkMobileTenDigit(mNumber);
        if (!inviteNumberCheckLog.isValid) {
            etBusBookMobile.setError(getString(inviteNumberCheckLog.msg));
            focusView = etBusBookMobile;
            cancel = true;
        }
    }

    private void checkEmail(String email) {
        if (email.isEmpty() || !isValidEmail(email)) {
            etBusBookEmail.setError(getResources().getString(R.string.Enter_valid_email));
            focusView = etBusBookEmail;
            cancel = true;
        }
    }

    private void checkAddress(String address) {
        CheckLog inviteNameCheckLog = PayingDetailsValidation.checkFirstName(address);
        if (!inviteNameCheckLog.isValid) {
            etBusBookAddress.setError(getString(inviteNameCheckLog.msg));
            focusView = etBusBookAddress;
            cancel = true;
        }

    }

    private void checkPostal(String postal) {
        CheckLog inviteNameCheckLog = PayingDetailsValidation.checkFirstName(postal);
        if (!inviteNameCheckLog.isValid) {
            etBusBookPostal.setError(getString(inviteNameCheckLog.msg));
            focusView = etBusBookPostal;
            cancel = true;
        }

    }


    private void checkCity(String city) {
        CheckLog inviteNameCheckLog = PayingDetailsValidation.checkFirstName(city);
        if (!inviteNameCheckLog.isValid) {
            etBusBookCity.setError(getString(inviteNameCheckLog.msg));
            focusView = etBusBookCity;
            cancel = true;
        }

    }

    private void checkState(String state) {
        CheckLog inviteNameCheckLog = PayingDetailsValidation.checkFirstName(state);
        if (!inviteNameCheckLog.isValid) {
            etBusBookState.setError(getString(inviteNameCheckLog.msg));
            focusView = etBusBookState;
            cancel = true;
        }

    }

    private void checkIdType(String postal) {
        CheckLog inviteNameCheckLog = PayingDetailsValidation.checkFirstName(postal);
        if (!inviteNameCheckLog.isValid) {
            etBusBookIDProofType.setError(getString(inviteNameCheckLog.msg));
            focusView = etBusBookIDProofType;
            cancel = true;
        }

    }

    private void checkIdNo(String postal) {
        CheckLog inviteNameCheckLog = PayingDetailsValidation.checkFirstName(postal);
        if (!inviteNameCheckLog.isValid) {
            etBusBookIDProofCardNo.setError(getString(inviteNameCheckLog.msg));
            focusView = etBusBookIDProofCardNo;
            cancel = true;
        }

    }

    private void checkIdIssuedBy(String postal) {
        CheckLog inviteNameCheckLog = PayingDetailsValidation.checkFirstName(postal);
        if (!inviteNameCheckLog.isValid) {
            etBusBookIDProofIssuedBy.setError(getString(inviteNameCheckLog.msg));
            focusView = etBusBookIDProofIssuedBy;
            cancel = true;
        }

    }

    private void checkBoardingPoint(String boarding) {
        CheckLog inviteNameCheckLog = PayingDetailsValidation.checkFirstName(boarding);
        if (!inviteNameCheckLog.isValid) {
            etBusBookBoardingLocation.setError(getString(inviteNameCheckLog.msg));
            focusView = etBusBookBoardingLocation;
            cancel = true;
        }

    }

    private void generateViewsForSeat() {
        for (Map.Entry<String, BusSeatModel> entry : seatSelectedMap.entrySet()) {
            LayoutInflater layoutInflater = LayoutInflater.from(getApplicationContext());
            View passengerDetailView = layoutInflater.inflate(R.layout.view_bus_passanger_detail, null);
            TextView tvBusBookSeatNo = (TextView) passengerDetailView.findViewById(R.id.tvBusBookSeatNo);
            tvBusBookSeatNo.setText(String.valueOf(getResources().getString(R.string.Seat_No) + entry.getValue().getSeatNo()));
            listFare.add(entry.getValue().getSeatFare());
            listSeatId.add(entry.getValue().getSeatCode());
            listSeatNo.add(entry.getValue().getSeatNo());
            listSeatType.add(entry.getValue().getSeatType());

            llBusBookPassengers.addView(passengerDetailView);
            MaterialEditText etBusBookPassengerGender = (MaterialEditText) passengerDetailView.findViewById(R.id.etBusBookPassengerGender);
            MaterialEditText etBusBookPassengerTitle = (MaterialEditText) passengerDetailView.findViewById(R.id.etBusBookPassengerTitle);
            listEtPassengerTitle.add(etBusBookPassengerTitle);
            if (entry.getValue().getIsLadiesSeat().equals("true")) {
                etBusBookPassengerTitle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View view) {
                        final String[] items = {getResources().getString(R.string.Ms), getResources().getString(R.string.Mrs)};
                        android.support.v7.app.AlertDialog.Builder idDialog =
                                new android.support.v7.app.AlertDialog.Builder(BusBookActivity.this, R.style.AppCompatAlertDialogStyle);
                        idDialog.setTitle(getResources().getString(R.string.Select_your_Title));
                        idDialog.setNegativeButton(getResources().getString(R.string.Dismiss), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
                        idDialog.setItems(items, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int position) {
                                ((MaterialEditText) view).setText(items[position].trim());
                                etBusBookPassengerGender.setText(getResources().getString(R.string.Female));
                            }

                        });

                        idDialog.show();
                    }
                });
            } else {
                etBusBookPassengerTitle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View view) {
                        final String[] items = {getResources().getString(R.string.Mr), getResources().getString(R.string.Ms), getResources().getString(R.string.Mrs)};
                        android.support.v7.app.AlertDialog.Builder idDialog =
                                new android.support.v7.app.AlertDialog.Builder(BusBookActivity.this, R.style.AppCompatAlertDialogStyle);
                        idDialog.setTitle(getResources().getString(R.string.Select_your_Title));
                        idDialog.setNegativeButton(getResources().getString(R.string.Dismiss), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
                        idDialog.setItems(items, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int position) {
                                ((MaterialEditText) view).setText(items[position].trim());
                                if (items[position].equalsIgnoreCase(getResources().getString(R.string.Mr))) {
                                    etBusBookPassengerGender.setText(getResources().getString(R.string.Male));
                                } else {
                                    etBusBookPassengerGender.setText(getResources().getString(R.string.Female));
                                }
                            }

                        });

                        idDialog.show();
                    }
                });
            }


            MaterialEditText etBusBookPassengerFName = (MaterialEditText) passengerDetailView.findViewById(R.id.etBusBookPassengerFName);
            etBusBookPassengerFName.setClickable(true);
            listEtPassengerFName.add(etBusBookPassengerFName);

            MaterialEditText etBusBookPassengerLName = (MaterialEditText) passengerDetailView.findViewById(R.id.etBusBookPassengerLName);
            etBusBookPassengerLName.setClickable(true);
            listEtPassengerLName.add(etBusBookPassengerLName);

            MaterialEditText etBusBookPassengerAge = (MaterialEditText) passengerDetailView.findViewById(R.id.etBusBookPassengerAge);
            etBusBookPassengerAge.setClickable(true);
            listEtPassengerAge.add(etBusBookPassengerAge);


            listEtPassengerGender.add(etBusBookPassengerGender);
            if (entry.getValue().getIsLadiesSeat().equals("true")) {
                etBusBookPassengerGender.setText(getResources().getString(R.string.Female));
            } else {
                etBusBookPassengerGender.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View view) {
                        final String[] items = {getResources().getString(R.string.Male), getResources().getString(R.string.Female)};
                        android.support.v7.app.AlertDialog.Builder idDialog =
                                new android.support.v7.app.AlertDialog.Builder(BusBookActivity.this, R.style.AppCompatAlertDialogStyle);
                        idDialog.setTitle(getResources().getString(R.string.Select_your_Gender));
                        idDialog.setNegativeButton(getResources().getString(R.string.Dismiss), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
                        idDialog.setItems(items, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int position) {
                                ((MaterialEditText) view).setText(items[position].trim());
                            }

                        });

                        idDialog.show();
                    }
                });
            }


        }

    }

    private void checkPassengerInfo() {
        for (MaterialEditText et : listEtPassengerTitle) {
            validateField(et.getText().toString(), et);
        }
        for (MaterialEditText et : listEtPassengerFName) {
            validateField(et.getText().toString(), et);
        }
        for (MaterialEditText et : listEtPassengerLName) {
            validateField(et.getText().toString(), et);
        }
        for (MaterialEditText et : listEtPassengerAge) {
            validateField(et.getText().toString(), et);
        }
        for (MaterialEditText et : listEtPassengerGender) {
            validateField(et.getText().toString(), et);
        }
    }

    private void validateField(String data, MaterialEditText mEt) {
        CheckLog inviteNameCheckLog = PayingDetailsValidation.checkFirstName(data);
        if (!inviteNameCheckLog.isValid) {
            mEt.setError(getString(inviteNameCheckLog.msg));
            focusView = mEt;
            cancel = true;
        }

    }

    private String getServiceTaxParam() {
        for (Map.Entry<String, BusSeatModel> entry : seatSelectedMap.entrySet()) {
            if (serviceTaxParam.equals("")) {
                serviceTaxParam = serviceTaxParam + entry.getValue().getSeatServiceTax();
            } else {
                serviceTaxParam = serviceTaxParam + "~" + entry.getValue().getSeatServiceTax();

            }
        }
        return serviceTaxParam;
    }

    private String getServiceChargeParam() {
        for (Map.Entry<String, BusSeatModel> entry : seatSelectedMap.entrySet()) {
            if (serviceChargeParam.equals("")) {
                serviceChargeParam = serviceChargeParam + entry.getValue().getSeatOptServiceCharge();
            } else {
                serviceChargeParam = serviceChargeParam + "~" + entry.getValue().getSeatOptServiceCharge();
            }
        }
        return serviceChargeParam;
    }

    private String getFareParam() {
        for (Map.Entry<String, BusSeatModel> entry : seatSelectedMap.entrySet()) {
            if (fareParam.equals("")) {
                fareParam = fareParam + entry.getValue().getNetFare();
            } else {
                fareParam = fareParam + "~" + entry.getValue().getNetFare();
            }
        }
        return fareParam;
    }

    private String getNameParam() {
        for (int e = 0; e < listEtPassengerFName.size(); e++) {
            if (nameParam.equals("")) {
                nameParam = nameParam + listEtPassengerFName.get(e).getText().toString();
            } else {
                nameParam = nameParam + "~" + listEtPassengerFName.get(e).getText().toString();

            }
        }
        return nameParam;
    }

    private String getTitleParam() {
        for (int e = 0; e < listEtPassengerGender.size(); e++) {
            if (titleParam.equals("")) {
                if (listEtPassengerGender.get(e).getText().toString().equals("Male")) {
                    titleParam = titleParam + "Mr";
                } else {
                    titleParam = titleParam + "Miss";
                }
            } else {
                if (listEtPassengerGender.get(e).getText().toString().equals("Male")) {
                    titleParam = titleParam + "~" + "Mr";
                } else {
                    titleParam = titleParam + "~" + "Miss";
                }
            }
        }
        return titleParam;
    }

    private String getSeatNoParam() {
        for (Map.Entry<String, BusSeatModel> entry : seatSelectedMap.entrySet()) {
            if (seatNoParam.equals("")) {
                seatNoParam = seatNoParam + entry.getValue().getSeatNo();
            } else {
                seatNoParam = seatNoParam + "," + entry.getValue().getSeatNo();

            }
        }
        return seatNoParam;
    }

    private String getTotalAmount() {
        for (Map.Entry<String, BusSeatModel> entry : seatSelectedMap.entrySet()) {
            if (totalAmount.equals("")) {
                totalAmount = totalAmount + entry.getValue().getSeatFare();
            } else {
                totalAmount = String.valueOf(Double.parseDouble(totalAmount) + Double.parseDouble(entry.getValue().getSeatFare()));
            }
        }

        return totalAmount;
    }

    private String getSeatDeatils() {
        for (Map.Entry<String, BusSeatModel> entry : seatSelectedMap.entrySet()) {
            if (seatDeatils.equals("")) {
                seatDeatils = seatDeatils + entry.getValue().getSeatNo() + "@" + entry.getValue().getSeatFare();
            } else {
                seatDeatils = seatDeatils + "#" + entry.getValue().getSeatNo() + "@" + entry.getValue().getSeatFare();
            }
        }
        return seatDeatils;
    }


    private String getSeatCodeParam() {
        for (Map.Entry<String, BusSeatModel> entry : seatSelectedMap.entrySet()) {
            if (seatCodeParam != null && seatCodeParam.equals("")) {
                seatCodeParam = seatCodeParam + entry.getValue().getSeatCode();
            } else if (seatCodeParam != null) {
                seatCodeParam = seatCodeParam + "~" + entry.getValue().getSeatCode();
            } else {
                seatCodeParam = null;
            }
        }
        return seatCodeParam;
    }

    private String getAgeParam() {
        for (int e = 0; e < listEtPassengerAge.size(); e++) {
            if (ageParam.equals("")) {
                ageParam = ageParam + listEtPassengerAge.get(e).getText().toString();
            } else {
                ageParam = ageParam + "~" + listEtPassengerAge.get(e).getText().toString();
            }
        }
        return ageParam;
    }

    private String getGenderParam() {
        for (int e = 0; e < listEtPassengerGender.size(); e++) {
            if (genderParam.equals("")) {
                if (listEtPassengerGender.get(e).getText().toString().equals("Male")) {
                    genderParam = genderParam + "M";
                } else {
                    genderParam = genderParam + "F";
                }
            } else {
                if (listEtPassengerGender.get(e).getText().toString().equals("Male")) {
                    genderParam = genderParam + "~" + "M";
                } else {
                    genderParam = genderParam + "~" + "F";
                }

            }
        }
        return genderParam;
    }

    private JSONObject getGSTDetails() {
        JSONObject gstdetails = new JSONObject();
        try {
            gstdetails.put("phone", "");
            gstdetails.put("email", "");
            gstdetails.put("companyName", "");
            gstdetails.put("address", "");
            gstdetails.put("gstNumber", "");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return gstdetails;
    }

    private JSONArray getTravellersDetails() {
        JSONArray travellersArray = new JSONArray();
        for (int i = 0; i < seatSelectedMap.size(); i++) {
            JSONObject t = new JSONObject();
            try {
                t.put("title", listEtPassengerTitle.get(i).getText().toString());
                t.put("fName", listEtPassengerFName.get(i).getText().toString());
                t.put("mName", "");
                t.put("lName", listEtPassengerLName.get(i).getText().toString());
                t.put("age", listEtPassengerAge.get(i).getText().toString());
                t.put("gender", listEtPassengerGender.get(i).getText().toString());
                t.put("seatNo", listSeatNo.get(i));
                t.put("seatType", listSeatType.get(i));
                t.put("fare", listFare.get(i));
                t.put("seatId", listSeatId.get(i));
                travellersArray.put(t);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return travellersArray;
    }


    private void getTransactionId() {


        loadingDialog.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("mobileNo", etBusBookMobile.getText().toString());
            jsonRequest.put("emailId", etBusBookEmail.getText().toString());
            jsonRequest.put("email", etBusBookEmail.getText().toString());
            jsonRequest.put("idProofId", "");
            jsonRequest.put("idProofNo", "");
            jsonRequest.put("address", etBusBookAddress.getText().toString());
            jsonRequest.put("engineId", busListModel.getEngineId());
            jsonRequest.put("busid", busListModel.getTripId());
            jsonRequest.put("busType", busListModel.getBusType());
            jsonRequest.put("boardId", boardingPointModels.getBdid());
            jsonRequest.put("boardLocation", boardingPointModels.getBdlocation());
            jsonRequest.put("boardName", etBusBookBoardingLocation.getText().toString());
            jsonRequest.put("arrTime", busListModel.getBusArrivalTime());
            jsonRequest.put("depTime", busListModel.getBusDepTime());
            jsonRequest.put("travelName", busListModel.getBusName());
            jsonRequest.put("source", busListModel.getBusSourceName());
            jsonRequest.put("destination", busListModel.getBusDesName());
            jsonRequest.put("sourceid", sourceId);
            jsonRequest.put("destinationId", desId);
            jsonRequest.put("journeyDate", formatDate(busListModel.getJnrDate()));
            jsonRequest.put("boardpoint", etBusBookBoardingLocation.getText().toString());
            jsonRequest.put("boardprime", boardingPointModels.getPrime());
            jsonRequest.put("boardTime", boardingPointModels.getTime());
            jsonRequest.put("boardlandmark", "");
            jsonRequest.put("boardContactNo", "");
            jsonRequest.put("dropId", droppingPointModels.getDpId());
            jsonRequest.put("dropName", etBusBookDroppingLocation.getText().toString());
            jsonRequest.put("droplocatoin", droppingPointModels.getLocatoin());
            jsonRequest.put("dropprime", droppingPointModels.getDpTime());
            jsonRequest.put("dropTime", droppingPointModels.getDpTime());
            jsonRequest.put("routeId", busListModel.getRouteId());
            jsonRequest.put("seatDetail", getSeatDeatils());
            jsonRequest.put("couponCode", "");
            jsonRequest.put("discount", String.valueOf(busListModel.getDiscount()));
            jsonRequest.put("commission", String.valueOf(busListModel.getCommission()));
            jsonRequest.put("markup", String.valueOf(busListModel.getMarkup()));
            jsonRequest.put("wLCode", "Bus");
            jsonRequest.put("totalFare", Double.parseDouble(getTotalAmount()));
            jsonRequest.put("version", "1.0");
            jsonRequest.put("agentCode", "");
            jsonRequest.put("cancelPolicyList", new JSONArray(busListModel.getBusCancellationPolicy()));
            jsonRequest.put("gstDetails", getGSTDetails());
            jsonRequest.put("travellers", getTravellersDetails());
            jsonRequest.put("tripId", GetTxnIdUpdated);
            if (SecurityUtil.getIMEI(BusBookActivity.this) != null) {
                jsonRequest.put("ipAddress", SecurityUtil.getAndroidId(BusBookActivity.this) + "-" + SecurityUtil.getIMEI(BusBookActivity.this));
            } else {
                jsonRequest.put("ipAddress", SecurityUtil.getAndroidId(BusBookActivity.this));
            }
            jsonRequest.put("version", "1.0");
            jsonRequest.put("agentCode", "");
            jsonRequest.put("cancelPolicyList", new JSONArray(busListModel.getBusCancellationPolicy()));
            jsonRequest.put("gstDetails", getGSTDetails());
//            jsonRequest.put("travellers", getTravellersDetails());

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {

            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_BUS_SEAT_TRANXID, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        if (response.has("code")) {
                            String code = response.getString("code");

                            loadingDialog.dismiss();

//                            if (code != null && code.equals("T01")) {
//                                Intent mainMenuDetailActivity = new Intent(BusBookActivity.this, WebViewActivity.class);
//                                mainMenuDetailActivity.putExtra("TYPE", "BUS");
//                                mainMenuDetailActivity.putExtra("SUBTYPE", "");
//                                Double aDouble = totalamount;
//                                mainMenuDetailActivity.putExtra("splitAmount", String.valueOf(aDouble.intValue()));
//                                mainMenuDetailActivity.putExtra("URL", ApiUrl.URL_BUS_SEAT_TRANXID);
//                                mainMenuDetailActivity.putExtra("REQUESTOBJECT", jsonRequest.toString());
//                                startActivity(mainMenuDetailActivity);
//                            }
                            if (code != null && code.equals("S00")) {
                                loadingDialog.dismiss();
                                JSONObject details = response.getJSONObject("details");
//                                messagess = details.getString("message");
                                String transactionid = details.getString("transactionid");
                                String vpQTxnId = details.getString("vpQTxnId");
                                String mpQTxnId = details.getString("mpQTxnId");
                                if (details.has("seatHoldId") && details.has("priceRecheckAmt")) {
                                    String seatHoldId = details.getString("seatHoldId");
                                    String priceRecheckAmt = details.getString("priceRecheckAmt");
                                    showSuccessDialog(transactionid, vpQTxnId, mpQTxnId, seatHoldId, priceRecheckAmt, true);
                                } else {
                                    showSuccessDialog(transactionid, vpQTxnId, mpQTxnId, "", getTotalAmount(), false);
                                }
                            } else if (code != null && code.equals("F03")) {
                                loadingDialog.dismiss();
                                showInvalidSessionDialog();
                            } else {
                                loadingDialog.dismiss();
                                if (response.has("message") && response.getString("message") != null) {
                                    String message = response.getString("message");
                                    CustomToast.showMessage(getApplicationContext(), message);
                                } else {
                                    CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.Error_message_is_null));
                                }

                            }
                        }
                    } catch (JSONException e) {
                        loadingDialog.dismiss();
                        CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception2));
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadingDialog.dismiss();
                    CustomToast.showMessage(getApplicationContext(), NetworkErrorHandler.getMessage(error, getApplicationContext()));
                    error.printStackTrace();
                }
            }) {
            };

            int socketTimeout = 1800000;
            loadingDialog.dismiss();
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    private void promoteBooking(String trxId, String pqTrxId, String mdexTxnId, String seatHold, String rechckAmount, boolean recheckamounts) {
        loadingDialog.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("transactionId", trxId);
//            jsonRequest.put("vpqTxnId", pqTrxId);
//            jsonRequest.put("mdexTxnId", mdexTxnId);

            if (recheckamounts) {
                jsonRequest.put("seatHoldId", seatHold);
                jsonRequest.put("amount", rechckAmount);
            }
            try {
//        jsonObject1.put("data", AESCrypt.encrypt(StringEscapeUtils.unescapeJson(jsonRequest.toString()).replaceAll("\\n", "")).replaceAll("\\\\/", ""));
            } catch (Exception e) {
                e.printStackTrace();
            }


        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("HGHDHDH", String.valueOf(jsonRequest));
            Log.i("HGHDHDH", ApiUrl.URL_BUS_SEAT_BOOKING);
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_BUS_SEAT_BOOKING, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.i("HGHDHDH", String.valueOf(response));

                        if (response.has("code")) {
                            String code = response.getString("code");
                            String msg = response.getString("message");


//                            if (code != null && code.equals("T01")) {
//                                Intent mainMenuDetailActivity = new Intent(BusBookActivity.this, WebViewActivity.class);
//                                mainMenuDetailActivity.putExtra("TYPE", "BUS");
//                                mainMenuDetailActivity.putExtra("SUBTYPE", "");
//                                Double aDouble = totalamount;
//                                mainMenuDetailActivity.putExtra("splitAmount", String.valueOf(aDouble.intValue()));
//                                mainMenuDetailActivity.putExtra("URL", ApiUrl.URL_BUS_SEAT_BOOKING);
//                                mainMenuDetailActivity.putExtra("REQUESTOBJECT", jsonRequest.toString());
//                                startActivity(mainMenuDetailActivity);
//                            }
                            if (code != null && code.equals("S00")) {
                                loadingDialog.dismiss();
                                showPaymentSuccessDialog(getResources().getString(R.string.Congratulation), getResources().getString(R.string.Payment_Successful));

                            } else if (code != null && code.equals("F03")) {
                                loadingDialog.dismiss();
                                showInvalidSessionDialog();
                            } else {
                                loadingDialog.dismiss();
                                if (response.has("message") && response.getString("message") != null) {
                                    String message = response.getString("message");
                                    CustomToast.showMessage(getApplicationContext(), message);
                                } else {
                                    CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.Error_message_is_null));
                                }

                            }
                        }
                    } catch (JSONException e) {
                        loadingDialog.dismiss();
                        CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception2));
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadingDialog.dismiss();
                    CustomToast.showMessage(getApplicationContext(), NetworkErrorHandler.getMessage(error, getApplicationContext()));
                    error.printStackTrace();
                }
            }) {
            };

            int socketTimeout = 600000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    private void cancelTranx(String pqTrxId) {
        loadingDialog.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("vpqTxnId", pqTrxId);

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_BUS_CANCEL_INITIATION, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        if (response.has("code")) {
                            String code = response.getString("code");
                            if (code != null && code.equals("S00")) {
                                loadingDialog.dismiss();

                            } else if (code != null && code.equals("F03")) {
                                loadingDialog.dismiss();
                                showInvalidSessionDialog();
                            } else {
                                loadingDialog.dismiss();
                                if (response.has("message") && response.getString("message") != null) {
                                    String message = response.getString("message");
                                    CustomToast.showMessage(getApplicationContext(), message);
                                } else {
                                    CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.Error_message_is_null));
                                }

                            }
                        }
                    } catch (JSONException e) {
                        loadingDialog.dismiss();
                        CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception2));
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadingDialog.dismiss();
                    CustomToast.showMessage(getApplicationContext(), NetworkErrorHandler.getMessage(error, getApplicationContext()));
                    error.printStackTrace();
                }
            }) {
            };

            int socketTimeout = 600000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    private String formatDate(String dateString) {
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = fmt.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat fmtOut = new SimpleDateFormat("dd-MM-yyyy");
        return (fmtOut.format(date)).toString();
    }


    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(BusBookActivity.this, R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton(getResources().getString(R.string.agree), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendFinish();
                sendLogout();
            }
        });
        builder.show();
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(BusBookActivity.this).sendBroadcast(intent);
    }

    private void sendRefresh() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "1");
        LocalBroadcastManager.getInstance(BusBookActivity.this).sendBroadcast(intent);
    }

    private void sendFinish() {
        Intent intent = new Intent("booking-done");
        intent.putExtra("ticket", "1");
        intent.putExtra("detail", "1");
        LocalBroadcastManager.getInstance(BusBookActivity.this).sendBroadcast(intent);
    }

    public void showSuccessDialog(final String trnxID, final String PQTrnxID, final String mpQTxnId, final String seatHoldID, final String recheckAmount, final boolean recheckAmounts) {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(getSuccessMessage(recheckAmount), Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(getSuccessMessage(recheckAmount));
        }
        CustomSuccessDialogBus builder = new CustomSuccessDialogBus(BusBookActivity.this, "Please, Confirm Payment", result);
        builder.setPositiveButton(getResources().getString(R.string.agree), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

                promoteBooking(trnxID, PQTrnxID, mpQTxnId, seatHoldID, recheckAmount, recheckAmounts);

            }
        });
        builder.setNegativeButton(getResources().getString(R.string.Dismiss), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
//                cancelTranx(PQTrnxID);
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public String getSuccessMessage(String amount) {
        return "<b><font color=#000000>"+getResources().getString(R.string.Please_confirm_your_booking_details)+"</font></b><br>" +
                "<b><font color=#000000>"+getResources().getString(R.string.From)+": "+"</font></b>" + "<font>" + busListModel.getBusSourceName() + "</font><br>" +
                "<b><font color=#000000>"+getResources().getString(R.string.To)+": "+"</font></b>" + "<font>" + busListModel.getBusDesName() + "</font><br>" +
                "<b><font color=#000000>"+getResources().getString(R.string.Date)+": "+"</font></b>" + "<font>" + busListModel.getJnrDate() + "</font><br>" +
                "<b><font color=#000000>"+getResources().getString(R.string.Travels)+": "+"</font></b>" + "<font>" + busListModel.getBusTravel() + "</font><br>" +
                "<b><font color=#000000>"+getResources().getString(R.string.Seat)+": "+"</font></b>" + "<font>" + getSeatNoParam() + "</font><br>" +
                "<b><font color=#000000>"+getResources().getString(R.string.Total_Amounts)+": "+"</font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + amount + "</font><br><br>";
    }

    public void showPaymentSuccessDialog(String message, String Title) {
        CustomSuccessDialog builder = new CustomSuccessDialog(BusBookActivity.this, Title, message);
        builder.setPositiveButton(getResources().getString(R.string.agree), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                sendRefresh();
                sendFinish();
                finish();
            }
        });
        builder.show();
    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }

}
