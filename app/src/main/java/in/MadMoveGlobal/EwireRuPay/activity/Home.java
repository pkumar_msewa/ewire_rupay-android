package in.MadMoveGlobal.EwireRuPay.activity;

import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import in.MadMoveGlobal.custom.CustomAlertDialog;
import in.MadMoveGlobal.custom.CustomSuccessDialog;
import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.custom.TextDrawable;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.metadata.AppMetadata;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.model.contract;
import in.MadMoveGlobal.util.CheckLog;
import in.MadMoveGlobal.util.NetworkErrorHandler;
import in.MadMoveGlobal.util.PayingDetailsValidation;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;

public class Home extends AppCompatActivity {
    private ListView lvCheckBox;
    private CheckBox btnCheckAll, btnClearALl;
    ArrayList<String> arrList;
    UserModel userModel = UserModel.getInstance();
    ArrayList<contract> contracts;
    private HashMap<String, String> stringStringHashMap;
    private Button proceed;
    private UserModel session = UserModel.getInstance();
    private String tag_json_obj = "value";
    private LoadingDialog loadDlg;
    private static final int PLUS_SIGN_POS = 0;
    private static final int MOBILE_DIGITS = 10;
    private JSONObject jsonRequest;
    private TextView tvsel,MyBallance;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_phone);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbars);
        setSupportActionBar(toolbar);
        loadDlg = new LoadingDialog(Home.this);
        tvsel = (TextView) findViewById(R.id.tvsel);
        contracts = new ArrayList<>();
        arrList = new ArrayList<>();
        doSomethingForEachUniquePhoneNumber(Home.this);
        final ImageView count = (ImageView) findViewById(R.id.count);
        btnCheckAll = (CheckBox) findViewById(R.id.check);
        for (contract contract : contracts) {
            arrList.add(contract.getPcName());
        }
        Button btnTermCondition = (Button) findViewById(R.id.btnTermCondition);

        btnTermCondition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent menuIntent = new Intent(Home.this, MainMenuDetailActivity.class);
                menuIntent.putExtra(AppMetadata.FRAGMENT_TYPE, "TermsAndCondition");
                startActivity(menuIntent);
            }
        });
        lvCheckBox = (ListView) findViewById(R.id.list);
        lvCheckBox.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        lvCheckBox.setAdapter(new ArrayAdapter(this, android.R.layout.simple_list_item_multiple_choice, arrList.toArray()));
        stringStringHashMap = new HashMap<>();

        lvCheckBox.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView arg0, View arg1, int i, long arg3) {
                contracts.get(i);
                stringStringHashMap.put(contracts.get(i).getPcId(), contracts.get(i).getPcName());
                TextDrawable plus = new TextDrawable(Home.this, (stringStringHashMap.size()) + "/50", ColorStateList.valueOf(getResources().getColor(R.color.white_text)), 30, TextDrawable.VerticalAlignment.BASELINE);
                count.setImageDrawable(plus);

            }

        });

        arrList.clear();
        stringStringHashMap.clear();
        for (int i = 0; i < lvCheckBox.getAdapter().getCount(); i++) {
            lvCheckBox.setItemChecked(i, true);
            arrList.add(String.valueOf(i));
        }
        if (arrList.size() != 0) {
            for (int i = 0; i < arrList.size(); i++) {
                contracts.get(i);
                stringStringHashMap.put(contracts.get(i).getPcId(), contracts.get(i).getPcName());
            }
        }
        btnCheckAll.setChecked(true);
        btnCheckAll.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (btnCheckAll.isChecked()) {

                    arrList.clear();
                    stringStringHashMap.clear();

                    for (int i = 0; i < lvCheckBox.getAdapter().getCount(); i++) {
                        lvCheckBox.setItemChecked(i, true);
                        arrList.add(String.valueOf(i));
                    }
                    if (arrList.size() != 0) {
                        for (int i = 0; i < arrList.size(); i++) {
                            contracts.get(i);
                            stringStringHashMap.put(contracts.get(i).getPcId(), contracts.get(i).getPcName());
                        }
                    }
                } else {
                    arrList.clear();
                    stringStringHashMap.clear();

                    for (int i = 0; i < lvCheckBox.getAdapter().getCount(); i++) {
                        lvCheckBox.setItemChecked(i, false);
                        arrList.add(String.valueOf(i));
                    }

                }
            }
        });
        proceed = (Button) findViewById(R.id.btnProceed);
        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (stringStringHashMap.size() > 50) {
                    userModel.setHasRefer(true);
                    referearn(stringStringHashMap);
                } else {
                    AlertDialog alert = new AlertDialog.Builder(Home.this).create();
                    alert.setTitle("");
                    alert.setMessage(" Choose atleast 50 contacts!!");
                    alert.setButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    alert.show();
                }
            }
        });

    }

    private void doSomethingForEachUniquePhoneNumber(Context context) {
        String[] projection = new String[]{
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER,
                ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER,
                //plus any other properties you wish to query
        };

        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, projection, null, null, null);
        } catch (SecurityException e) {
            //SecurityException can be thrown if we don't have the right permissions
        }

        if (cursor != null) {
            try {
                HashSet<String> normalizedNumbersAlreadyFound = new HashSet<>();
                int indexOfNormalizedNumber = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER);
                int indexOfDisplayName = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
                int indexOfDisplayNumber = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);

                while (cursor.moveToNext()) {
                    String normalizedNumber = cursor.getString(indexOfNormalizedNumber);
                    if (normalizedNumbersAlreadyFound.add(normalizedNumber)) {
                        String name = cursor.getString(indexOfDisplayName);
                        String phoneNumber = cursor.getString(indexOfDisplayNumber);
                        //haven't seen this number yet: do something with this contact!
                        if (removeCountryCode(phoneNumber.replaceAll("[^0-9\\+]", "")) != null && !removeCountryCode(phoneNumber.replaceAll("[^0-9\\+]", "")).isEmpty()) {

                            if (name != null && !name.isEmpty()) {

                                contracts.add(new contract(removeCountryCode(phoneNumber.replaceAll("[^0-9\\+]", "")), name, false));
                            } else {
                                contracts.add(new contract(removeCountryCode(phoneNumber.replaceAll("[^0-9\\+]", "")), "User", false));
                            }

                        }
                    } else {
                        //don't do anything with this contact because we've already found this number
                    }
                }
            } finally {
                cursor.close();
            }
        }
    }

    private void getContactDetails(ContentResolver contentResolver) {

        Cursor phones = contentResolver.query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null,
                null, null);
        HashSet<String> mobileNoSet = new HashSet<String>();

        while (phones.moveToNext()) {
            String name = phones
                    .getString(phones
                            .getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phoneNumber = phones
                    .getString(phones
                            .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            String email = phones
                    .getString(phones
                            .getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS));
            String imagUri = phones
                    .getString(phones
                            .getColumnIndex(ContactsContract.CommonDataKinds.Photo.PHOTO_URI));
            long id = phones.getColumnIndex(ContactsContract.Contacts._ID);

            if (!mobileNoSet.contains(phoneNumber.replaceAll("[^0-9\\+]", ""))) {
                if (removeCountryCode(phoneNumber.replaceAll("[^0-9\\+]", "")) != null && !removeCountryCode(phoneNumber.replaceAll("[^0-9\\+]", "")).isEmpty()) {

                    if (name != null && !name.isEmpty()) {

                        contracts.add(new contract(removeCountryCode(phoneNumber.replaceAll("[^0-9\\+]", "")), name, false));
                    } else {
                        contracts.add(new contract(removeCountryCode(phoneNumber.replaceAll("[^0-9\\+]", "")), "User", false));
                    }
                    mobileNoSet.add(removeCountryCode(phoneNumber.replaceAll("[^0-9\\+]", "")));
                }
            }
        }

    }

    private String removeCountryCode(String number) {
        if (hasCountryCode(number)) {
            try {
                int country_digits = number.length() - MOBILE_DIGITS;
                number = number.substring(country_digits);

                CheckLog gasCheckLog = PayingDetailsValidation.checkMobileTenDigit(number.trim().replace(" ", "").replace("-", ""));
                if (!gasCheckLog.isValid) {
                    return null;
                } else {
                    return number;
                }
            } catch (StringIndexOutOfBoundsException e) {
                return null;
            }
        } else if (hasZero(number))

        {
            if (number.length() >= 10) {
                int country_digits = number.length() - MOBILE_DIGITS;
                number = number.substring(country_digits);
                CheckLog gasCheckLog = PayingDetailsValidation.checkMobileTenDigit(number.trim());
                if (!gasCheckLog.isValid) {
                    return null;
                } else {
                    return number;
                }
            }
        } else

        {
            return null;
        }
        return null;
    }

    private boolean hasCountryCode(String number) {
        return number.charAt(PLUS_SIGN_POS) == '+';
    }

    private boolean hasZero(String number) {
        return number.charAt(PLUS_SIGN_POS) == '0';
    }

    private void referearn(HashMap<String, String> stringStringHashMap) {
        loadDlg.show();
        try {
            jsonRequest = new JSONObject();
            jsonRequest.put("sessionId", userModel.getUserSessionId());
            jsonRequest.put("mobileList", getJsonFromMap(stringStringHashMap));
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_REFER_AND_EARN, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject responses) {
                    try {
                        String messageAPI = responses.getString("message");
                        String code = responses.getString("code");
                        if (code != null && code.equals("F03")) {
                            loadDlg.dismiss();
                            showInvalidSessionDialog();
                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(Home.this, R.style.AppCompatAlertDialogStyleMusic);
                            LayoutInflater inflater1 = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            View viewDialog = inflater1.inflate(R.layout.dialog_earntopay, null, false);
                            builder.setView(viewDialog);
                            builder.setCancelable(false);
                            final AlertDialog alertDialog = builder.create();

                            Button ok = (Button) viewDialog.findViewById(R.id.btnOK);

                            ok.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Intent intent = new Intent(Home.this, MainActivity.class);
                                    startActivity(intent);
                                    finish();
                                    finishAffinity();
                                    alertDialog.dismiss();
                                }
                            });


                            alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                                @Override
                                public void onCancel(DialogInterface dialogInterface) {

                                    dialogInterface.dismiss();
                                }
                            });
                            alertDialog.show();
                            loadDlg.dismiss();

                        }
                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception2));
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    CustomToast.showMessage(getApplicationContext(), NetworkErrorHandler.getMessage(error, getApplicationContext()));
                    loadDlg.dismiss();


                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "12345");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    public void showSuccessDialog(String message, String Title) {
        CustomSuccessDialog builder = new CustomSuccessDialog(Home.this, Title, message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                Intent mainActivityIntent = new Intent(Home.this, MainActivity.class);
                sendRefresh();
                startActivity(mainActivityIntent);
                Home.this.finish();
            }
        });
        builder.show();
    }

    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(Home.this, R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(Home.this).sendBroadcast(intent);
    }

    private void sendRefresh() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "1");
        LocalBroadcastManager.getInstance(Home.this).sendBroadcast(intent);
    }

    private JSONObject getJsonFromMap(Map<String, String> map) throws JSONException {
        JSONObject jsonData = new JSONObject();
        for (String key : map.keySet()) {
            Object value = map.get(key);
            if (value instanceof Map<?, ?>) {
                value = getJsonFromMap((Map<String, String>) value);
            }
            jsonData.put(key, value);
        }
        return jsonData;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.shopping_menu, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.menuCart) {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
            finishAffinity();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setMargins(View view, int left, int top, int right, int bottom) {
        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            p.setMargins(left, top, right, bottom);
            view.requestLayout();
        }
    }
}