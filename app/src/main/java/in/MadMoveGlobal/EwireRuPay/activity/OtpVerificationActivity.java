package in.MadMoveGlobal.EwireRuPay.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.crashlytics.android.Crashlytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.MadMoveGlobal.custom.CustomAlertDialog;
import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.model.UserModel;
import io.fabric.sdk.android.Fabric;
import me.philio.pinentry.PinEntryView;

import static in.MadMoveGlobal.metadata.ApiUrl.URL_RESEND_OTP;
import static in.MadMoveGlobal.metadata.ApiUrl.URL_VERIFY_PHONEOTP;


/**
 * Created by Ksf on 3/11/2016.
 */
public class OtpVerificationActivity extends AppCompatActivity {

    private static boolean VISIBLE_PASSWORD = false;
    private View rootView;
    private PinEntryView etVerifyOTP;
    private Button btnVerify;
    private LoadingDialog loadDlg;
    private String userMobileNo;
    private String otpCode = null;
    private Button btnVerifyResend;
    private JSONObject jsonRequest;
    private TextView tvTimer;
    private Boolean fullyFilled;
    private UserModel session = UserModel.getInstance();
    private EditText newPassword;
    private String tag_json_obj = "json_user";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        // TODO: Move this to where you establish a user session
//        logUser();

        setContentView(R.layout.otp_verification);
        loadDlg = new LoadingDialog(OtpVerificationActivity.this);
        setStatusBarGradiant(OtpVerificationActivity.this);
        userMobileNo = getIntent().getStringExtra("userMobileNo");
        otpCode = getIntent().getStringExtra("OtpCode");
        btnVerifyResend = (Button) findViewById(R.id.btnVerifyResend);
        btnVerify = (Button) findViewById(R.id.btnVerify);
        newPassword = (EditText) findViewById(R.id.etnewPassword);
        etVerifyOTP = (PinEntryView) findViewById(R.id.etcreatePin);
        this.registerReceiver(broadcastReceiver, new IntentFilter("broadCastName"));
        tvTimer = (TextView) findViewById(R.id.tvTimer);


        newPassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (newPassword.getRight() - newPassword.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here
                        //Helper.toast(LoginActivity.this, "Toggle visibility");
                        if (VISIBLE_PASSWORD) {
                            VISIBLE_PASSWORD = false;
                            newPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                            newPassword.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_password_01, 0, R.drawable.ic_visibilityeye, 0);
                        } else {
                            VISIBLE_PASSWORD = true;
                            newPassword.setInputType(InputType.TYPE_CLASS_TEXT);
                            newPassword.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_password_01, 0, R.drawable.ic_eyevisisvility, 0);
                        }
                        return false;
                    }
                }
                return false;
            }
        });


        reverseTimer(120, tvTimer);
        etVerifyOTP.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 6) {
                    if (!validateOtp()) {
                        return;
                    }


                }

            }
        });


        if (otpCode != null && !otpCode.isEmpty()) {
            etVerifyOTP.setText(otpCode);
//            loadDlg.show();
//            verifyOTP();
        }


        btnVerify.setTextColor(Color.parseColor("#ffffff"));


        btnVerifyResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadDlg.show();
                resendOTP();
            }
        });


        btnVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!validateOtp()) {
                    return;
                }
                if (!validatePassword()) {
                    return;
                } else {
                    loadDlg.show();
                    verifyOTP();
                }
            }
        });
    }


    private void resendOTP() {
        btnVerifyResend.setVisibility(View.GONE);
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("mobileNumber", userMobileNo);
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            Log.i("RESEND", URL_RESEND_OTP);
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, URL_RESEND_OTP, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObj) {
                    try {
                        Log.i("VERIFYOTP", jsonObj.toString());
                        String code = jsonObj.getString("code");
                        String detail = jsonObj.getString("details");
                        if (code != null & code.equals("S00")) {
                            loadDlg.dismiss();
                            reverseTimer(120, tvTimer);
                            if (!etVerifyOTP.getText().toString().isEmpty()) {
                                etVerifyOTP.setText("");
                            }
                            Toast.makeText(OtpVerificationActivity.this, "Otp sent successfully", Toast.LENGTH_SHORT).show();

                        } else {
                            loadDlg.dismiss();


                        }
                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(OtpVerificationActivity.this, getResources().getString(R.string.server_exception2));
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception));

                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }


    private void verifyOTP() {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("username", userMobileNo);
            jsonRequest.put("key", etVerifyOTP.getText().toString());

            String basicAuth = new String(Base64.encode(newPassword.getText().toString().getBytes(), Base64.NO_WRAP));
            jsonRequest.put("password",basicAuth);
            jsonRequest.put("confirmPassword", basicAuth);

//            jsonRequest.put("password", newPassword.getText().toString());
//            jsonRequest.put("confirmPassword", newPassword.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            Log.i("URLVERIFY", URL_VERIFY_PHONEOTP);
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, URL_VERIFY_PHONEOTP, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.i("RESENDRESPONSE", response.toString());
                        String level = response.getString("status");
                        String code = response.getString("code");

                        if (code != null & code.equals("S00")) {
                            loadDlg.dismiss();
                            Toast.makeText(getApplicationContext(), "Password changed", Toast.LENGTH_SHORT).show();
                            showLoginDialog();

                        } else {
                            loadDlg.dismiss();
                            Toast.makeText(getApplicationContext(), "Invalid OTP", Toast.LENGTH_SHORT).show();


                        }
                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception2));
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception));
                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }


    public void showLoginDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(OtpVerificationActivity.this, R.string.dialog_title2, Html.fromHtml(generateMessage()));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Intent otpIntent = new Intent(getApplicationContext(), LoginRegActivity.class);
                startActivity(otpIntent);
                finish();

            }
        });

        builder.show();
    }

    public String generateMessage() {
        String source = "<b><font color=#000000> You have Successfully changed your password.</font></b>" +
                "<br><br><b><font color=#ff0000> Continue to login. </font></b><br></br>";
        return source;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void setStatusBarGradiant(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            Drawable background = activity.getResources().getDrawable(R.drawable.bg_gradient_new);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
    }

    private boolean validateOtp() {
        if (etVerifyOTP.getText().toString().trim().isEmpty() || etVerifyOTP.getText().toString().trim().length() < 6) {
            requestFocus(etVerifyOTP);
            return false;
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    public void reverseTimer(int Seconds, final TextView tv) {

        new CountDownTimer(Seconds * 1000 + 1000, 1000) {

            public void onTick(long millisUntilFinished) {
                int seconds = (int) (millisUntilFinished / 1000);
                int minutes = seconds / 60;
                seconds = seconds % 60;
//                if (seconds == 0) {
//                    resendOTP();
//                }
                tv.setText(String.format("%02d", minutes)
                        + ":" + String.format("%02d", seconds));
            }

            public void onFinish() {
                tv.setText("00:00");
                btnVerifyResend.setVisibility(View.VISIBLE);
            }
        }.start();
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle b = intent.getExtras();
            if (b != null) {
                String message = b.getString("message");
                Log.i("BroadCast", message);
                message = message.replaceAll("\\.", "");
                Log.i("BroadCast", message);
                unregisterReceiver(broadcastReceiver);
                etVerifyOTP.setText(message);


            }

        }
    };

    private boolean validatePassword() {
        if (newPassword.getText().toString().trim().isEmpty()) {
            newPassword.setError("Required password");
            requestFocus(newPassword);
            return false;
        } else if (newPassword.getText().toString().trim().length() < 6) {
            newPassword.setError(getResources().getString(R.string.password_validity));
            requestFocus(newPassword);
            return false;
        } else {
            newPassword.setError(null);
        }

        return true;
    }

    private void logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        Crashlytics.setUserIdentifier(session.getUserMobileNo());
        Crashlytics.setUserEmail(session.getUserEmail());
        Crashlytics.setUserName(session.getUserFirstName() + " " + session.getUserLastName());
    }
}
