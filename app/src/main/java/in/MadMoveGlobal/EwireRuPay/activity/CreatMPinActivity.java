package in.MadMoveGlobal.EwireRuPay.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.MadMoveGlobal.custom.CustomAlertDialog;
import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.metadata.AppMetadata;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;
import me.philio.pinentry.PinEntryView;

/**
 * Created by Ksf on 5/7/2016.
 */
public class CreatMPinActivity extends AppCompatActivity {

    private PinEntryView etCreatePin, etCreateRePin;
//    private TextInputLayout ilRePin,ilPin;
    private Button btnCreatePin,btnDismissPin;
    private JSONObject jsonRequest;
    private UserModel session = UserModel.getInstance();
    private LoadingDialog loadDlg;
    private String intentType;

    //Volley Tag
    private String tag_json_obj = "json_user";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creat_mpin);
        etCreatePin = (PinEntryView) findViewById(R.id.etCreatePin);
        etCreateRePin = (PinEntryView) findViewById(R.id.etCreateRePin);
        btnDismissPin = (Button) findViewById(R.id.btnDismissPin);
        btnCreatePin = (Button) findViewById(R.id.btnCreatePin);

        loadDlg = new LoadingDialog(CreatMPinActivity.this);

        etCreatePin.addTextChangedListener(new MyTextWatcher(etCreatePin));
        intentType = getIntent().getStringExtra("IntentType");

        btnDismissPin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(intentType.equals("Setting")){
                    finish();
                }
                else{
                    startActivity(new Intent(getApplicationContext(),MainActivity.class));
                    finish();
                }

            }
        });

        btnCreatePin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!validatePin()){
                    return;
                }
                else{
                    loadDlg.show();
                    createMPin();
                }
            }
        });


    }


    private boolean validatePin() {
        if (etCreatePin.getText().toString().length()<4) {
            CustomToast.showMessage(getApplicationContext(),"Enter 4 digit pin no");
            return false;
        }
        else if (etCreateRePin.getText().toString().length()<4) {
            CustomToast.showMessage(getApplicationContext(),"Enter 4 digit pin no");
            return false;
        }
        else if (!etCreatePin.getText().toString().equals(etCreateRePin.getText().toString())) {
            CustomToast.showMessage(getApplicationContext(),"MPin din't match");
            return false;

        }
        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void createMPin() {
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("newMpin", etCreatePin.getText().toString());
            jsonRequest.put("confirmMpin", etCreateRePin.getText().toString());
            jsonRequest.put("username", session.getUserFirstName());
            jsonRequest.put("sessionId", session.getUserSessionId());
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_GENERATE_M_PIN, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        String code = response.getString("code");
                        String message = response.getString("message");

                        if (code != null && code.equals("S00")) {
                            loadDlg.dismiss();

                            if(intentType.equals("Setting")){
                                session.setMPin(true);
                                sendRefresh();
                                finish();
                            }
                            else{
                                Intent mPinIntent = new Intent(getApplicationContext(), MainActivity.class);
                                mPinIntent.putExtra("mPinBundle","mPinCreated");
                                startActivity(mPinIntent);
                                finish();
                            }

                        }
                        else if(code != null && code.equals("F03")){
                            loadDlg.dismiss();
                            showInvalidSessionDialog();
                        }
                        else {
                            loadDlg.dismiss();

                        }
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        e.printStackTrace();
                        CustomToast.showMessage(getApplicationContext(),getResources().getString(R.string.server_exception2));


                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    CustomToast.showMessage(getApplicationContext(),getResources().getString(R.string.server_exception));
                    error.printStackTrace();

                }
            }){
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    private void sendRefresh() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "1");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(CreatMPinActivity.this, R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
    }


    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            int i = view.getId();
            if (i == R.id.etCreatePin) {
                if (etCreatePin.getText().toString().trim().isEmpty() || etCreatePin.getText().toString().trim().length() ==4) {
                    etCreateRePin.requestFocus();
                }
            }
        }
    }


}
