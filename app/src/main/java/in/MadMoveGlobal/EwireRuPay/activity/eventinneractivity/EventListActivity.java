package in.MadMoveGlobal.EwireRuPay.activity.eventinneractivity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import in.MadMoveGlobal.adapter.EventListAdapter;
import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.model.EventsModel;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;

/**
 * Created by Kashif-PC on 1/3/2017.
 */
public class EventListActivity extends AppCompatActivity {
    private ListView lvEvents;
    private LinearLayout llNoEvents;
    private LoadingDialog loadingDialog;
    private JSONObject jsonRequest;
    private UserModel session = UserModel.getInstance();

    //Volley
    private String tag_json_obj = "json_events";
    private JsonObjectRequest postReq;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_list);
        loadingDialog = new LoadingDialog(EventListActivity.this);
        lvEvents = (ListView) findViewById(R.id.lvEvents);
        llNoEvents = (LinearLayout) findViewById(R.id.llNoEvents);
        getEventList();
    }

    public void getEventList() {
        loadingDialog.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_EVENT_LIST, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject JsonObj) {
                    try {
                        String code = JsonObj.getString("code");

                        if (code != null && code.equals("S00")) {
                            List<EventsModel> eventArray = null;
                            JSONArray eveCatArray = JsonObj.getJSONArray("details");

                            for (int i = 0; i < eveCatArray.length(); i++) {
                                JSONObject c = eveCatArray.getJSONObject(i);
                                long eventId = c.getLong("eventId");
                                long ownerId = c.getLong("ownerId");
                                String eventTitle = c.getString("title");
                                String bannerImage = c.getString("bannerImage");
                                String thumbImage = c.getString("thumbImage");
                                String eventStartDate = c.getString("startDate");
                                String eventEndDate = c.getString("endDate");
                                String eventCity = c.getString("cityName");
                                String eventVenue = c.getString("venueName");
                                String categoryName = c.getString("categoryName");

                                EventsModel eveCatModel = new EventsModel(eventId, ownerId, eventTitle, bannerImage, thumbImage, eventStartDate, eventEndDate, eventCity, eventVenue, categoryName);
                                eventArray.add(eveCatModel);
                            }
                            if (eventArray != null && eventArray.size() != 0) {
                                EventListAdapter eventAdp = new EventListAdapter(EventListActivity.this, eventArray);
                                lvEvents.setAdapter(eventAdp);
                                loadingDialog.dismiss();
                                llNoEvents.setVisibility(View.GONE);
                                lvEvents.setVisibility(View.VISIBLE);

                            } else {
                                loadingDialog.dismiss();
                                llNoEvents.setVisibility(View.VISIBLE);
                                lvEvents.setVisibility(View.GONE);
                                Toast.makeText(EventListActivity.this, "Empty Array", Toast.LENGTH_SHORT).show();
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }


}
