package in.MadMoveGlobal.EwireRuPay.activity.giftcardactivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;


import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.fragment.fragmentgiftcard.DescriptionFragment;
import in.MadMoveGlobal.fragment.fragmentgiftcard.PurchaseFragment;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.model.GiftCardCatModel;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;


/**
 * Created by kashifimam on 21/02/17.
 */

public class GiftCardListActivity extends AppCompatActivity {


  private TabLayout mSlidingTabLayout;
  private ViewPager mainPager;
  private FragmentManager fragmentManager;
  private String tag_json_obj = "json_events";
  ImageView ivGiftCardItem;
  CharSequence TitlesEnglish[] = {"Terms&condition's", "Purchase"};
  int NumbOfTabs = 3;
  private GiftCardCatModel model;


  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    fragmentManager = getSupportFragmentManager();
    setContentView(R.layout.activity_tabs_layout);
    mainPager = (ViewPager) findViewById(R.id.mainPager);
    model = getIntent().getParcelableExtra("Model");
    mSlidingTabLayout = (TabLayout) findViewById(R.id.sliding_tabs_home);
//    mainPager.setAdapter(new ViewPagerAdapter(fragmentManager, TitlesEnglish, NumbOfTabs, model));
    mSlidingTabLayout.setupWithViewPager(mainPager);
    ivGiftCardItem = (ImageView) findViewById(R.id.ivGiftCardItem);

    Picasso.with(GiftCardListActivity.this).load(model.getImages()).into(ivGiftCardItem);

    android.support.v7.widget.Toolbar issuentoolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(issuentoolbar);
    ImageButton issueIb = (ImageButton) findViewById(R.id.ivBackBtn);
    issueIb.setVisibility(View.VISIBLE);
    issueIb.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        finish();
      }
    });

    getCardCategory();
    //LocalBroadcastManager.getInstance(this).registerReceiver(mSignUpReceiver, new IntentFilter("sign-up"));
  }


  private BroadcastReceiver mSignUpReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      mainPager.setCurrentItem(1);

    }
  };

  public static class ViewPagerAdapter extends FragmentPagerAdapter {
    CharSequence Titles[]; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
    int NumbOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created

    GiftCardCatModel giftModel;

    // Build a Constructor and assign the passed Values to appropriate values in the class
    public ViewPagerAdapter(FragmentManager fm, CharSequence mTitles[], int mNumbOfTabsumb, GiftCardCatModel giftModel) {
      super(fm);
      this.giftModel = giftModel;
      this.Titles = mTitles;
      this.NumbOfTabs = mNumbOfTabsumb;

    }

    @Override
    public Fragment getItem(int position) {

      if (position == 1) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("model", giftModel);
        PurchaseFragment tab1 = new PurchaseFragment();
        tab1.setArguments(bundle);
        return tab1;
      } else if (position == 0) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("model", giftModel);
        DescriptionFragment tab2 = new DescriptionFragment();
        tab2.setArguments(bundle);
        return tab2;
      } else {
        Bundle bundle = new Bundle();
        bundle.putParcelable("model", giftModel);
        DescriptionFragment tab2 = new DescriptionFragment();
        tab2.setArguments(bundle);
        return tab2;
      }

    }

    @Override
    public CharSequence getPageTitle(int position) {
      return Titles[position];
    }

    @Override
    public int getCount() {
      return NumbOfTabs;
    }
  }


  public void getCardCategory() {
//    pbGiftCardCat.setVisibility(View.VISIBLE); model.getProduct_id()
//    rvGiftCardCat.setVisibility(View.GONE);
    JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, ApiUrl.URL_WOOHOO_PRODUCT + model.getProduct_id(), (String) null,
            new Response.Listener<JSONObject>() {
              @Override
              public void onResponse(JSONObject jsonObj) {
                try {
                  Log.i("EventsCatResponse", jsonObj.toString());
                  String code = jsonObj.getString("success");
                  String message = jsonObj.getString("message");

                  if (code != null && code.equals("true")) {
//              Picasso.with(GiftCardListActivity.this).load(jsonObj.getString("categoryImage")).centerInside().fit().into(ivBanner);
                    if (!jsonObj.isNull("product_id")) {
                      String product_id = jsonObj.getString("product_id");
                      String brand_id = model.getBrand_id();
                      String brand_name =model.getBrand_name();
                      String images = model.getImages();
                      String min_custom_price = jsonObj.getString("min_custom_price");
                      String max_custom_price = jsonObj.getString("max_custom_price");
                      String sku = jsonObj.getString("sku");
                      String product_type = jsonObj.getString("product_type");
                      String paywithamazon_disable = jsonObj.getString("paywithamazon_disable");
                      String custom_denominations = jsonObj.getString("custom_denominations");
                      String tnc_mobile = jsonObj.getString("tnc_mobile");
                      String order_handling_charge = jsonObj.getString("order_handling_charge");
                      String themes = jsonObj.getString("themes");
                      GiftCardCatModel eveCatModel = new GiftCardCatModel(product_id, brand_id, brand_name, images, min_custom_price, max_custom_price, custom_denominations, sku, product_type, paywithamazon_disable, tnc_mobile, order_handling_charge, themes);


                      mainPager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager(), TitlesEnglish, TitlesEnglish.length, eveCatModel));
                      mSlidingTabLayout.setupWithViewPager(mainPager);

                    } else {
                      CustomToast.showMessage(GiftCardListActivity.this, message);
                      finish();
                    }

                  } else if (code.equalsIgnoreCase("F03")) {
//              showInvalidSessionDialog();
                  } else {
                    if (message.equalsIgnoreCase("Please login and try again.")) {
//                showInvalidSessionDialog();
//                pbGiftCardCat.setVisibility(View.GONE);
                    } else {
                      CustomToast.showMessage(GiftCardListActivity.this, message);
//                pbGiftCardCat.setVisibility(View.GONE);
                    }
                  }

                } catch (JSONException e) {
                  e.printStackTrace();
//            pbGiftCardCat.setVisibility(View.GONE);
                  Toast.makeText(GiftCardListActivity.this, "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
                }

              }
            }, new Response.ErrorListener() {
      @Override
      public void onErrorResponse(VolleyError error) {
        error.printStackTrace();
        CustomToast.showMessage(GiftCardListActivity.this, "Error connecting to server");
      }
    }) {
      @Override
      public Map<String, String> getHeaders() throws AuthFailureError {
        HashMap<String, String> map = new HashMap<>();
        map.put("hash", "12345");
        return map;
      }

    };
    int socketTimeout = 60000;
    RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
    jsonObjReq.setRetryPolicy(policy);
    PayQwikApplication.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
  }
}