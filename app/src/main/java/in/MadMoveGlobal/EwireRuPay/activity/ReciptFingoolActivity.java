package in.MadMoveGlobal.EwireRuPay.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.adapter.ReceiptFingoolAdapter;
import in.MadMoveGlobal.custom.CustomAlertDialog;
import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.custom.LoadMoreListView;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.metadata.AppMetadata;
import in.MadMoveGlobal.model.FingoolHistoryModel;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.util.NetworkErrorHandler;


/**
 * Created by kashifimam on 21/02/17.
 */

public class ReciptFingoolActivity extends AppCompatActivity {
    private View rootView;
    private ReceiptFingoolAdapter receiptOrderAdapter;
    private LoadMoreListView lvReceipt;
    private UserModel session = UserModel.getInstance();
    private JSONObject jsonRequest;
    private ArrayList<FingoolHistoryModel> ordrList;
    private ProgressBar pbReceipt;
    private LinearLayout llNoReceipt;
    private TextView tvNoReceipt;
    private Toolbar toolbar;
    private ImageButton ivBackBtn;


    //Volley
    private String tag_json_obj = "json_receipt";
    private JsonObjectRequest postReq;

    //Pagination
    private int currentPage = 0;
    private int totalPage = 0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ordrList = new ArrayList<>();
        setContentView(R.layout.activity_orders_receipts);
        lvReceipt = (LoadMoreListView) findViewById(R.id.lvReceipt);
        pbReceipt = (ProgressBar) findViewById(R.id.pbReceipt);
        tvNoReceipt = (TextView) findViewById(R.id.tvNoReceipt);
        llNoReceipt = (LinearLayout) findViewById(R.id.llNoReceipt);
        toolbar = (Toolbar) findViewById(R.id.toolbars);
        ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtns);
        llNoReceipt.setVisibility(View.GONE);

        setSupportActionBar(toolbar);
        ivBackBtn.setVisibility(View.VISIBLE);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
//                finish();
            }
        });

        loadUserStatement();

//        lvReceipt.setOnLoadMoreListener(new LoadMoreListView.OnLoadMoreListener() {
//            @Override
//            public void onLoadMore() {
//                if (currentPage < totalPage) {
//                    loadMoreUserStatement();
//                } else {
//                    lvReceipt.onLoadMoreComplete();
//                }
//            }
//        });


    }

    public void loadUserStatement() {
        pbReceipt.setVisibility(View.VISIBLE);
        lvReceipt.setVisibility(View.GONE);
        jsonRequest = new JSONObject();
        try {

            jsonRequest.put("sessionId", session.getUserSessionId());
//            jsonRequest.put("page", currentPage);
//            jsonRequest.put("size", 20);

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("OrderJsonRequest", jsonRequest.toString());
            Log.i("Order Url", ApiUrl.URL_FINGOOL_HISTORY);
            postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_FINGOOL_HISTORY, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject JsonObj) {
                    try {

                        Log.i("Order Response", JsonObj.toString());
                        String message = JsonObj.getString("message");
                        String code = JsonObj.getString("code");

                        if (code != null && code.equals("S00")) {
//                            sendRefresh();

//                            totalPage = JsonObj.getInt("totalPages");


                            if (JsonObj.getJSONArray("transactions").length() == 0) {
                                llNoReceipt.setVisibility(View.VISIBLE);
                                pbReceipt.setVisibility(View.GONE);
                                lvReceipt.setVisibility(View.GONE);
//
                            } else {

                                JSONArray operatorArray = JsonObj.getJSONArray("transactions");


                                for (int i = 0; i < operatorArray.length(); i++) {
                                    JSONObject c = operatorArray.getJSONObject(i);
//                                    if (c.has("transactionRefNo")) {
//                                        if (!c.isNull("transactionRefNo")) {
//                                            if (c.getString("transactionRefNo").contains("D")) {

                                    String name = c.getString("name");
                                    double amountPaidD = c.getDouble("amount");
                                    String amountPaid = String.valueOf(amountPaidD);
                                    String coi = c.getString("coi");
                                    String transactionNo = c.getString("transactionNo");
                                    String date = c.getString("date");
                                    String mobile = c.getString("mobile");

                                    Log.i("Total", name + coi + transactionNo + mobile);
                                    FingoolHistoryModel fingoolHistoryModel = new FingoolHistoryModel(name, amountPaid, coi, transactionNo, date, mobile);
                                    ordrList.add(fingoolHistoryModel);

                                }
//                            }
                                if (ordrList != null && ordrList.size() != 0) {
                                    receiptOrderAdapter = new ReceiptFingoolAdapter(ReciptFingoolActivity.this, ordrList);
                                    lvReceipt.setAdapter(receiptOrderAdapter);
                                    pbReceipt.setVisibility(View.GONE);
                                    lvReceipt.setVisibility(View.VISIBLE);
                                    llNoReceipt.setVisibility(View.GONE);


                                }
                            }

                        } else if (code != null && code.equals("F03")) {
                            pbReceipt.setVisibility(View.GONE);
                            lvReceipt.setVisibility(View.GONE);
                            llNoReceipt.setVisibility(View.VISIBLE);
                            showInvalidSessionDialog();
                        } else {
                            pbReceipt.setVisibility(View.GONE);
                            lvReceipt.setVisibility(View.GONE);
                            llNoReceipt.setVisibility(View.VISIBLE);
//                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        pbReceipt.setVisibility(View.GONE);
                        lvReceipt.setVisibility(View.GONE);
                        CustomToast.showMessage(ReciptFingoolActivity.this, getResources().getString(R.string.server_exception2));
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    llNoReceipt.setVisibility(View.VISIBLE);
                    pbReceipt.setVisibility(View.GONE);
                    lvReceipt.setVisibility(View.GONE);
                    try {
                        tvNoReceipt.setText(NetworkErrorHandler.getMessage(error, ReciptFingoolActivity.this));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }

    }

//    public void loadMoreUserStatement() {
//        currentPage = currentPage + 1;
//        jsonRequest = new JSONObject();
//        try {
//            jsonRequest.put("sessionId", session.getUserSessionId());
//            jsonRequest.put("page", currentPage);
//            jsonRequest.put("size", 20);
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//            jsonRequest = null;
//        }
//
//        if (jsonRequest != null) {
//            Log.i("JsonRequest", jsonRequest.toString());
//            Log.i("Receipt Url", ApiUrl.URL_MYORDER);
//            postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_MYORDER, jsonRequest, new Response.Listener<JSONObject>() {
//                @Override
//                public void onResponse(JSONObject JsonObj) {
//                    try {
//
//                        Log.i("Receipts Response", JsonObj.toString());
//                        String message = JsonObj.getString("message");
//                        String code = JsonObj.getString("code");
//
//                        if (code != null && code.equals("S00")) {
//                            JSONArray operatorArray = JsonObj.getJSONArray("details");
//
//                            for (int i = 0; i < operatorArray.length(); i++) {
//                                JSONObject c = operatorArray.getJSONObject(i);
//
//                                String transactionNumber = c.getString("transactionNumber");
//                                double amountPaidD = c.getDouble("amount");
//                                String amountPaid = String.valueOf(amountPaidD);
//                                String serviceStatus = c.getString("status");
//                                String serviceName = c.getString("serviceName");
//                                String image = c.getString("imageUrl");
//                                String date = c.getString("date");
//                                String description = c.getString("description");
//
//                                Log.i("Total", transactionNumber + serviceStatus + serviceName + image);
//
//
//                                OrderModel orderModel = new OrderModel(transactionNumber, amountPaid, serviceStatus, serviceName, image, date,description);
//                                ordrList.add(orderModel);
//
//                            }
//                            receiptOrderAdapter.notifyDataSetChanged();
//                        } else if (code != null && code.equals("F03")) {
//                            showInvalidSessionDialog();
//                        } else {
////                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
//                        }
//                        lvReceipt.onLoadMoreComplete();
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                        lvReceipt.onLoadMoreComplete();
//                        CustomToast.showMessage(ReciptFingoolActivity.this, getResources().getString(R.string.server_exception2));
//                    }
//                }
//            }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    error.printStackTrace();
//                    CustomToast.showMessage(ReciptFingoolActivity.this, NetworkErrorHandler.getMessage(error, ReciptFingoolActivity.this));
//                    lvReceipt.onLoadMoreComplete();
//                }
//            }) {
//                @Override
//                public Map<String, String> getHeaders() throws AuthFailureError {
//                    HashMap<String, String> map = new HashMap<>();
//                    map.put("hash", "1234");
//                    return map;
//                }
//
//            };
//            int socketTimeout = 60000;
//            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//            postReq.setRetryPolicy(policy);
//            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
//        }
//
//    }

    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(ReciptFingoolActivity.this, R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton(getResources().getString(R.string.agree), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton(getResources().getString(R.string.Dismiss), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    @Override
    public void onBackPressed() {

        Intent menuIntent = new Intent(this, MainMenuDetailActivity.class);
        menuIntent.putExtra(AppMetadata.FRAGMENT_TYPE, "Fingool");
        this.startActivity(menuIntent);
        finish();
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(ReciptFingoolActivity.this).sendBroadcast(intent);
    }

    private void sendRefresh() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "1");
        LocalBroadcastManager.getInstance(ReciptFingoolActivity.this).sendBroadcast(intent);


    }


}
