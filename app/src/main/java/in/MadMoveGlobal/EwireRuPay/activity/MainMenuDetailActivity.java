package in.MadMoveGlobal.EwireRuPay.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.crashlytics.android.Crashlytics;
//import com.niki.config.NikiConfig;
//import com.niki.config.SessionConfig;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.MadMoveGlobal.EwireRuPay.EkycActivity;
import in.MadMoveGlobal.EwireRuPay.VehicleInsuranceFragment;
import in.MadMoveGlobal.EwireRuPay.WebViewPrimiumActivity;
import in.MadMoveGlobal.EwireRuPay.WebViewTermConditionActivity;
import in.MadMoveGlobal.EwireRuPay.WebViewbigbasketActivity;
import in.MadMoveGlobal.custom.CustomAlertDialog;
import in.MadMoveGlobal.custom.CustomBlockDialog;
import in.MadMoveGlobal.custom.CustomSuccessDialog;
import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.custom.LoadingDialog;
//import in.paulpay.fragment.LoadMoneyFragment;
import in.MadMoveGlobal.fragment.TravelsFragment;
import in.MadMoveGlobal.fragment.fragmentmobilerecharge.DatacardFragment;
import in.MadMoveGlobal.fragment.fragmentmobilerecharge.PostpaidFragment;
import in.MadMoveGlobal.fragment.fragmentnaviagtionitems.BillPaymentFragment;
import in.MadMoveGlobal.fragment.fragmentnaviagtionitems.CardGenrateFragment;
import in.MadMoveGlobal.fragment.fragmentnaviagtionitems.DonationBankTransferFragment;
import in.MadMoveGlobal.fragment.fragmentnaviagtionitems.DonationFragment;
import in.MadMoveGlobal.fragment.fragmentnaviagtionitems.FundTransferByMobileFragment;
import in.MadMoveGlobal.fragment.fragmentnaviagtionitems.FundTransferFragment;
import in.MadMoveGlobal.fragment.fragmentnaviagtionitems.InviteFreindFragment;
import in.MadMoveGlobal.fragment.fragmentnaviagtionitems.MVisaFragment;
import in.MadMoveGlobal.fragment.fragmentnaviagtionitems.MerchantPayByQrFragment;
import in.MadMoveGlobal.fragment.fragmentnaviagtionitems.MerchantPayFragment;
import in.MadMoveGlobal.fragment.fragmentnaviagtionitems.MobileTouUpFragment;
import in.MadMoveGlobal.fragment.fragmentnaviagtionitems.MyCouponsFragment;
import in.MadMoveGlobal.fragment.fragmentnaviagtionitems.PhysicalFragment;
import in.MadMoveGlobal.fragment.fragmentnaviagtionitems.QwikPaymentFragment;
import in.MadMoveGlobal.fragment.fragmentnaviagtionitems.ReceiptFragment;
import in.MadMoveGlobal.fragment.fragmentnaviagtionitems.RedeemCouponFragment;
import in.MadMoveGlobal.fragment.fragmentnaviagtionitems.RequestMoneyFragment;
import in.MadMoveGlobal.fragment.fragmentnaviagtionitems.SharePointsFragment;
import in.MadMoveGlobal.fragment.fragmentnaviagtionitems.SplitMoneyFragment;
import in.MadMoveGlobal.fragment.fragmentnaviagtionitems.TravelHealthFragment;
import in.MadMoveGlobal.fragment.fragmentnaviagtionitems.TravelOfferFragment;
import in.MadMoveGlobal.fragment.fragmentpaybills.DthFragment;
import in.MadMoveGlobal.fragment.fragmentpaybills.ElectricityFragment;
import in.MadMoveGlobal.fragment.fragmentpaybills.GasFragment;
import in.MadMoveGlobal.fragment.fragmentpaybills.InsuranceFragment;
import in.MadMoveGlobal.fragment.fragmentpaybills.LandlineFragment;
import in.MadMoveGlobal.fragment.fragmenttravel.BusTravelActivity;
import in.MadMoveGlobal.fragment.fragmenttravel.FlightTravelActivity;
import in.MadMoveGlobal.fragment.fragmentuser.FingoolFragment;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.metadata.AppMetadata;
import in.MadMoveGlobal.model.SplitMoneyGroupModel;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.util.AddGroupsListner;
import in.MadMoveGlobal.util.EmailCouponsUtil;
import in.MadMoveGlobal.util.GenerateAPIHeader;
import in.MadMoveGlobal.util.NetworkErrorHandler;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;
import io.fabric.sdk.android.Fabric;


/**
 * Created by Dushant on 4/9/2019.
 */
public class MainMenuDetailActivity extends AppCompatActivity implements AddGroupsListner {
    private String type;
    private Toolbar toolbar;
    private ImageButton ivBackBtn;
    private boolean toFinish = false;

    //split payment
    private LoadingDialog loadDlg;
    private String globalURL = "";
    private String globalObjectString = "";
    private JSONObject jsonObj = new JSONObject();
    private UserModel session = UserModel.getInstance();
    private String tag_json_obj = "json_topup_pay";
    private String serviceProviderName = "";
    private String toMobileNumber = "";
    private String amount = "";
    private TextView MyBallance;
    private ImageButton ivLogo;
    private String tokenKeyNikki, secretKeyNikki;
    private JSONObject jsonRequest, extraJson;
    //FIREBASE
    public static final String PROPERTY_REG_ID = "registration_id";
    private String regId = "";
    SharedPreferences loadMoneyPref;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        // TODO: Move this to where you establish a user session
        logUser();

        setContentView(R.layout.activity_telebuy_pay);

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("loadMoney-done"));

        //press back button in toolbar

        toolbar = (Toolbar) findViewById(R.id.toolbars);
        MyBallance = (TextView) findViewById(R.id.MyBallance);
        ivLogo = (ImageButton) findViewById(R.id.ivLogo);
        setSupportActionBar(toolbar);
        ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtns);
        setSupportActionBar(toolbar);
        ivBackBtn.setVisibility(View.VISIBLE);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent shoppingIntent = new Intent(MainMenuDetailActivity.this, MainActivity.class);
                startActivity(shoppingIntent);
                finish();
            }
        });
        loadMoneyPref = getSharedPreferences("type", Context.MODE_PRIVATE);
        MyBallance = (TextView) findViewById(R.id.MyBallance);
        ivLogo = (ImageButton) findViewById(R.id.ivLogo);

        setSupportActionBar(toolbar);

        loadDlg = new LoadingDialog(MainMenuDetailActivity.this);
        type = getIntent().getStringExtra(AppMetadata.FRAGMENT_TYPE);
        if (type.equals("MobileTopUp")) {
            MobileTouUpFragment fragment = new MobileTouUpFragment();
            Bundle bundle = new Bundle();
            bundle.putString("postion", getIntent().getStringExtra("postion"));
            fragment.setArguments(bundle);
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frameInCart, fragment);
            fragmentTransaction.commit();
        } else if (type.equals("BillPayment")) {
            BillPaymentFragment fragment = new BillPaymentFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frameInCart, fragment);
            fragmentTransaction.commit();
        } else if (type.equals("DTH")) {
            DthFragment fragment = new DthFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frameInCart, fragment);
            fragmentTransaction.commit();
        } else if (type.equals("PhysicalCard")) {
            PhysicalFragment fragment = new PhysicalFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frameInCart, fragment);
            fragmentTransaction.commit();
        } else if (type.equals("CardGenrate")) {
            CardGenrateFragment fragment = new CardGenrateFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frameInCart, fragment);
            fragmentTransaction.commit();
        } else if (type.equals("FundTransfer")) {
            FundTransferFragment fragment = new FundTransferFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frameInCart, fragment);
            fragmentTransaction.commit();
        } else if (type.equals("FundTransferbyMobile")) {
            FundTransferByMobileFragment fragment = new FundTransferByMobileFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frameInCart, fragment);
            fragmentTransaction.commit();
        } else if (type.equals("Travel")) {
            TravelsFragment fragment = new TravelsFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            Bundle bType = new Bundle();
            bType.putString("URL", "http://ewiresofttech.com/gtandc.html");
            fragment.setArguments(bType);
            fragmentTransaction.replace(R.id.frameInCart, fragment);
            fragmentTransaction.commit();


        } else if (type.equals("Ofer")) {
//            TravelBigBasketFragment fragment = new TravelBigBasketFragment();
//            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
//            Bundle bType = new Bundle();
//            bType.putString("URL", "http://bigbasket.go2cloud.org/aff_c?offer_id=271&aff_id=3683");
//            fragment.setArguments(bType);
//            fragmentTransaction.replace(R.id.frameInCart, fragment);
//            fragmentTransaction.commit();
            Intent intent = new Intent(this, WebViewbigbasketActivity.class);
            startActivity(intent);
//
        } else if (type.equals("MerchantPay")) {
            MerchantPayFragment fragment = new MerchantPayFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frameInCart, fragment);
            fragmentTransaction.commit();
        } else if (type.equals("QRPay")) {
            MerchantPayByQrFragment fragment = new MerchantPayByQrFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frameInCart, fragment);
            fragmentTransaction.commit();
        } else if (type.equals("QwikPay")) {
            QwikPaymentFragment fragment = new QwikPaymentFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frameInCart, fragment);
            fragmentTransaction.commit();
        } else if (type.equals("CouponsCode")) {
            RedeemCouponFragment fragment = new RedeemCouponFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frameInCart, fragment);
            fragmentTransaction.commit();
        } else if (type.equals("LoadMoney")) {
//
        } else if (type.equals("MyCoupons")) {
            MyCouponsFragment fragment = new MyCouponsFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frameInCart, fragment);
            fragmentTransaction.commit();
        } else if (type.equals("BankTransfer")) {
            CustomToast.showMessage(getApplicationContext(),"Coming Soon");
           /* BankTransferFragment fragment = new BankTransferFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frameInCart, fragment);
            fragmentTransaction.commit();*/
        } else if (type.equals("DonationBankTransfer")) {
            DonationBankTransferFragment fragment = new DonationBankTransferFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frameInCart, fragment);
            fragmentTransaction.commit();
        } else if (type.equals("PhysicalCard")) {
            PhysicalFragment fragment = new PhysicalFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frameInCart, fragment);
            fragmentTransaction.commit();
        } else if (type.equals("RequestMoney")) {
            RequestMoneyFragment fragment = new RequestMoneyFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frameInCart, fragment);
            fragmentTransaction.commit();
        } else if (type.equals("TermsAndCondition")) {
            Intent intent = new Intent(this, WebViewTermConditionActivity.class);
            startActivity(intent);
        } else if (type.equals("PremiumInsurace")) {

            Intent intent = new Intent(this, WebViewPrimiumActivity.class);
            startActivity(intent);
        }else if (type.equals("Offers")) {
            TravelOfferFragment fragment = new TravelOfferFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            Bundle bType = new Bundle();
            bType.putString("URL", "https://cards.mycashier.in/IplCards/requestPromo");
            fragment.setArguments(bType);
            fragmentTransaction.replace(R.id.frameInCart, fragment);
            fragmentTransaction.commit();

        } else if (type.equals("DonationTandC")) {
            TravelHealthFragment fragment = new TravelHealthFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            Bundle bType = new Bundle();
            bType.putString("URL", "http://ewiresofttech.com/gtandc.html");
            fragment.setArguments(bType);
            fragmentTransaction.replace(R.id.frameInCart, fragment);
            fragmentTransaction.commit();
        } else if (type.equals("Ofer")) {
            Intent Promocode = new Intent(MainMenuDetailActivity.this, OffersActivity.class);
            startActivity(Promocode);
            finish();
        } else if (type.equals("ipl")) {
            TravelHealthFragment fragment = new TravelHealthFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            Bundle bType = new Bundle();
            bType.putString("URL", "https://www.vpayqwik.com/Campaign");
            fragment.setArguments(bType);
            fragmentTransaction.replace(R.id.frameInCart, fragment);
            fragmentTransaction.commit();
        } else if (type.equals("Balance")) {
            ReceiptFragment fragment = new ReceiptFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frameInCart, fragment);
            fragmentTransaction.commit();
        } else if (type.equals("Points")) {
            SharePointsFragment fragment = new SharePointsFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frameInCart, fragment);
            fragmentTransaction.commit();
        } else if (type.equals("Bus")) {
            Intent bustravel = new Intent(this, BusTravelActivity.class);
            startActivity(bustravel);
        } else if (type.equals("Flight")) {
            Intent flighttravel = new Intent(this, FlightTravelActivity.class);
            startActivity(flighttravel);
        }
//
        else if (type.equals("Cards")) {
            Intent shoppingIntent = new Intent(MainMenuDetailActivity.this, CardsActivity.class);
            startActivity(shoppingIntent);
            finish();
        } else if (type.equals("Fingool")) {
            FingoolFragment fragment = new FingoolFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frameInCart, fragment);
            fragmentTransaction.commit();
        } else if (type.equalsIgnoreCase("postpaid")) {
            PostpaidFragment postpaidFragment = new PostpaidFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frameInCart, postpaidFragment);
            fragmentTransaction.commit();
        } else if (type.equalsIgnoreCase("landline")) {
            LandlineFragment landlineFragment = new LandlineFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frameInCart, landlineFragment);
            fragmentTransaction.commit();
        } else if (type.equalsIgnoreCase("gas")) {
            GasFragment gasFragment = new GasFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frameInCart, gasFragment);
            fragmentTransaction.commit();
        } else if (type.equalsIgnoreCase("datacard")) {
            DatacardFragment datacardFragment = new DatacardFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frameInCart, datacardFragment);
            fragmentTransaction.commit();
        } else if (type.equals("EKYC")) {
            Intent shoppingIntent = new Intent(MainMenuDetailActivity.this, EkycActivity.class);
            startActivity(shoppingIntent);
            finish();
        } else if (type.equals("healthCare")) {
            TravelHealthFragment fragment = new TravelHealthFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            Bundle bType = new Bundle();
            bType.putString("URL", "http://125.16.82.68:3780/?vpqw=1&utm_source=Vpayquik_easy_beta&utm_campaign=Vpayquik_easy_beta&utm_medium=buttom");
            fragment.setArguments(bType);
            fragmentTransaction.replace(R.id.frameInCart, fragment);
            fragmentTransaction.commit();
        } else if (type.equals("SplitMoney")) {
            SplitMoneyFragment fragment = new SplitMoneyFragment();
            Bundle b = new Bundle();
            b.putString("Type", "");
            fragment.setArguments(b);
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frameInCart, fragment);
            fragmentTransaction.commit();
        } else if (type.equals("InviteFriend")) {
            InviteFreindFragment fragment = new InviteFreindFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frameInCart, fragment);
            fragmentTransaction.commit();
        } else if (type.equals("MVisa")) {
            MVisaFragment fragment = new MVisaFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frameInCart, fragment);
            fragmentTransaction.commit();
        } else if (type.equals("Donation")) {
            DonationFragment fragment = new DonationFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frameInCart, fragment);
            fragmentTransaction.commit();

        } else if (type.equals("Electricity")) {
            ElectricityFragment fragment = new ElectricityFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frameInCart, fragment);
            fragmentTransaction.commit();
        } else if (type.equals("Insurance")) {
            InsuranceFragment fragment = new InsuranceFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frameInCart, fragment);
            fragmentTransaction.commit();
        } else if (type.equals("Gift Cards")) {
            Intent invitefriendIntent = new Intent(MainMenuDetailActivity.this, InviteFriendActivity.class);
            startActivity(invitefriendIntent);
            finish();
        } else if (type.equals("Vehicle Insurance")) {

           getSupportFragmentManager().beginTransaction()
                   .replace(R.id.frameInCart,new VehicleInsuranceFragment())
                   .commit();

        }

    }


    private String getRegistrationId() {
        final SharedPreferences prefs = getGCMPreferences();
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            return "";
        }
        return registrationId;
    }

    private SharedPreferences getGCMPreferences() {
        return getSharedPreferences("FIREBASE",
                Context.MODE_PRIVATE);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getStringExtra("result");
            if (action.equals("1")) {
                toFinish = true;
            }

        }
    };


    @Override
    public void addGroupCompleted(String groupName, String groupAmount, String groupType, List<SplitMoneyGroupModel> splitMoneyGroupModels) {
        SplitMoneyFragment fragment = new SplitMoneyFragment();
        Bundle b = new Bundle();
        b.putString("Type", "Add");
        b.putString("GroupName", groupName);
        b.putString("GroupAmount", groupAmount);
        b.putString("GroupType", groupType);
        b.putParcelableArrayList("PeopleArray", (ArrayList<? extends Parcelable>) splitMoneyGroupModels);
        fragment.setArguments(b);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frameInCart, fragment);
        fragmentTransaction.commit();
    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("MenuDetailValue", String.valueOf(toFinish));
        if (toFinish) {
            Log.i("TOFINISH", "toFinish is called");
            if (!globalURL.isEmpty() && !globalObjectString.isEmpty()) {
                Log.i("GLOBALCHECK", "global variables are checked");
                Log.i("MenuDetail", "True");
                Log.i("RECHARGE", "promote recharge called");
                promoteRecharge();
            } else {
                finish();
                Log.i("MenuDetail", "True");
            }

        } else {
            Log.i("MenuDetail", "false");
        }
    }

    //split money
    public void promoteRecharge() {
        Log.i("PROMOTEREC", "promote recharge is called");
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            if (getIntent().getStringExtra("TYPE").equals("TOPUP")) {
                jsonRequest.put("topupType", jsonObj.getString("topupType"));
                jsonRequest.put("serviceProvider", jsonObj.getString("serviceProvider"));
                jsonRequest.put("mobileNo", jsonObj.getString("mobileNo"));
                jsonRequest.put("amount", jsonObj.getString("amount"));
                if (!jsonObj.getString("topupType").equals("Postpaid")) {
                    jsonRequest.put("area", jsonObj.getString("area"));
                }
                jsonRequest.put("sessionId", jsonObj.getString("sessionId"));
            } else if (getIntent().getStringExtra("TYPE").equals("BILLPAY")) {
                jsonRequest.put("serviceProvider", jsonObj.getString("serviceProvider"));
                jsonRequest.put("amount", jsonObj.getString("amount"));
                jsonRequest.put("sessionId", jsonObj.getString("sessionId"));
                if (getIntent().getStringExtra("SUBTYPE").equals("DTH")) {
                    jsonRequest.put("dthNo", jsonObj.getString("dthNo"));
                } else if (getIntent().getStringExtra("SUBTYPE").equals("ELECTRIC")) {
                    jsonRequest.put("accountNumber", jsonObj.getString("accountNumber"));
                    jsonRequest.put("cycleNumber", jsonObj.getString("cycleNumber"));
                    jsonRequest.put("cityName", jsonObj.getString("cityName"));
                } else if (getIntent().getStringExtra("SUBTYPE").equals("ELECTRIC")) {
                    jsonRequest.put("accountNumber", jsonObj.getString("accountNumber"));
                } else if (getIntent().getStringExtra("SUBTYPE").equals("INSURANCE")) {
                    jsonRequest.put("policyNumber", jsonObj.getString("policyNumber"));
                    jsonRequest.put("policyDate", jsonObj.getString("policyDate"));
                } else if (getIntent().getStringExtra("SUBTYPE").equals("LANDLINE")) {
                    jsonRequest.put("stdCode", jsonObj.getString("stdCode"));
                    jsonRequest.put("accountNumber", jsonObj.getString("accountNumber"));
                    jsonRequest.put("landlineNumber", jsonObj.getString("landlineNumber"));
                }

            } else if (getIntent().getStringExtra("TYPE").equals("QWICKPAY")) {
                jsonRequest = jsonObj;
            }

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("globalURL", globalURL);
            Log.i("JsonRequestRecharge", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, globalURL, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.i("JsonResponseRecharge", response.toString());
                        String code = response.getString("code");

                        if (code != null && code.equals("S00")) {
                            loadDlg.dismiss();
                            try {
                                if (getIntent().getStringExtra("TYPE").equals("QWICKPAY")) {
                                    sendRefresh();
                                    loadDlg.dismiss();
                                    showSuccessDialog();
                                } else {
                                    String jsonString = response.getString("response");
                                    JSONObject jsonObject = new JSONObject(jsonString);
                                    sendRefresh();
                                    try {
                                        EmailCouponsUtil.emailForCoupons(tag_json_obj);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    loadDlg.dismiss();
                                    showSuccessDialog();
                                }
                            } catch (NullPointerException e) {
                                e.printStackTrace();
                            }
                        } else if (code != null && code.equals("F03")) {
                            loadDlg.dismiss();
//                            showInvalidSessionDialog();
                            CustomToast.showMessage(MainMenuDetailActivity.this, "Please login and try again");
                            sendLogout();
                        } else if (code != null && code.equals(ApiUrl.C_SESSION_BLOCKED)) {
                            loadDlg.dismiss();
                            showBlockDialog();
                        } else {
                            loadDlg.dismiss();
                            if (response.has("message") && response.getString("message") != null) {
                                String message = response.getString("message");
                                CustomToast.showMessage(MainMenuDetailActivity.this, message);
                            } else {
                                CustomToast.showMessage(MainMenuDetailActivity.this, "Error message is null");
                            }
                            finish();
                        }

                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(MainMenuDetailActivity.this, getResources().getString(R.string.server_exception2));
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    CustomToast.showMessage(MainMenuDetailActivity.this, NetworkErrorHandler.getMessage(error, MainMenuDetailActivity.this));
                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", GenerateAPIHeader.getHeader(ApiUrl.H_PREPAID + session.getUserSessionId()));
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }

    }

    public void showInvalidSessionDialog() {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(AppMetadata.getInvalidSession(), Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(AppMetadata.getInvalidSession());
        }
        CustomAlertDialog builder = new CustomAlertDialog(MainMenuDetailActivity.this, R.string.dialog_title2, result);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    private void sendRefresh() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "1");
        LocalBroadcastManager.getInstance(MainMenuDetailActivity.this).sendBroadcast(intent);
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(MainMenuDetailActivity.this).sendBroadcast(intent);
    }

    public void showSuccessDialog() {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(getSuccessMessage(), Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(getSuccessMessage());
        }
        CustomSuccessDialog builder = null;
        try {
            if (getIntent().getStringExtra("TYPE").equals("TOPUP")) {
                builder = new CustomSuccessDialog(MainMenuDetailActivity.this, "Recharge Successful", result);
            } else {
                builder = new CustomSuccessDialog(MainMenuDetailActivity.this, "Payment Successful", result);
            }
            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                    finish();
                }
            });
            builder.show();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }


    }

    public String getSuccessMessage() {
        String source = "";
        if (getIntent().getStringExtra("TYPE").equals("TOPUP")) {
            source = "<b><font color=#000000> Service Provider: </font></b>" + "<font color=#000000>" + serviceProviderName + "</font><br>" +
                    "<b><font color=#000000> Mobile No: </font></b>" + "<font>" + toMobileNumber + "</font><br>" +
                    "<b><font color=#000000> Amount: </font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + amount + "</font><br><br>";
        } else if (getIntent().getStringExtra("TYPE").equals("BILLPAY")) {
            try {
                if (getIntent().getStringExtra("SUBTYPE").equals("DTH")) {
                    source =
                            "<b><font color=#000000> Service Provider: </font></b>" + "<font color=#000000>" + jsonObj.getString("serviceProvider") + "</font><br>" +
                                    "<b><font color=#000000> DTH No: </font></b>" + "<font>" + jsonObj.getString("dthNo") + "</font><br>" +
                                    "<b><font color=#000000> Amount: </font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + jsonObj.getString("amount") + "</font><br><br>";
                } else if (getIntent().getStringExtra("SUBTYPE").equals("ELECTRIC")) {
                    String cycleNo = jsonObj.getString("cycleNumber");
                    if (cycleNo != null && !cycleNo.isEmpty()) {
                        source = "<b><font color=#000000> Service Provider: </font></b>" + "<font color=#000000>" + jsonObj.getString("serviceProvider") + "</font><br>" +
                                "<b><font color=#000000> Account No: </font></b>" + "<font>" + jsonObj.getString("accountNumber") + "</font><br>" +
                                "<b><font color=#000000> Cycle No: </font></b>" + "<font>" + cycleNo + "</font><br>" +
                                "<b><font color=#000000> Amount: </font></b>" + "<font>" + jsonObj.getString("amount") + "</font><br>";
                    } else {
                        source = "<b><font color=#000000> Service Provider: </font></b>" + "<font color=#000000>" + jsonObj.getString("serviceProvider") + "</font><br>" +
                                "<b><font color=#000000> Account No: </font></b>" + "<font>" + jsonObj.getString("accountNumber") + "</font><br>" +
                                "<b><font color=#000000> Amount: </font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + jsonObj.getString("amount") + "</font><br>";
                    }
                } else if (getIntent().getStringExtra("SUBTYPE").equals("GAS")) {
                    String payAmount = getIntent().getStringExtra("payAmount");
                    String additionalCharges = getIntent().getStringExtra("AddCharge");
                    source = "<b><font color=#000000> Service Provider: </font></b>" + "<font color=#000000>" + jsonObj.getString("serviceProvider") + "</font><br>" +
                            "<b><font color=#000000> Account No: </font></b>" + "<font>" + jsonObj.getString("accountNumber") + "</font><br>" +
                            "<b><font color=#000000> Amount: </font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + jsonObj.getString("amount") + "</font><br>" +
                            "<b><font color=#000000> Service Charge: </font></b>" + "<font>" + additionalCharges + "</font><br>" +
                            "<b><font color=#000000> Total Charge: </font></b>" + "<font>" + payAmount + "</font><br><br>" +
                            "<b><font color=#ff0000> Are you sure you want to proceed?</font></b><br>";
                } else if (getIntent().getStringExtra("SUBTYPE").equals("INSURANCE")) {
                    source = "<b><font color=#000000> Service Provider: </font></b>" + "<font color=#000000>" + jsonObj.getString("serviceProvider") + "</font><br>" +
                            "<b><font color=#000000> Policy No: </font></b>" + "<font>" + jsonObj.getString("policyNumber") + "</font><br>" +
                            "<b><font color=#000000> Date: </font></b>" + "<font>" + jsonObj.getString("policyDate") + "</font><br>" +
                            "<b><font color=#000000> Amount: </font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + jsonObj.getString("amount") + "</font><br><br>";
                } else if (getIntent().getStringExtra("SUBTYPE").equals("LANDLINE")) {
                    String acNo = jsonObj.getString("accountNumber");
                    if (acNo != null && !acNo.isEmpty()) {
                        source = "<b><font color=#000000> Service Provider: </font></b>" + "<font color=#000000>" + jsonObj.getString("serviceProvider") + "</font><br>" +
                                "<b><font color=#000000> STD Code: </font></b>" + "<font>" + jsonObj.getString("stdCode") + "</font><br>" +
                                "<b><font color=#000000> Phone No: </font></b>" + "<font>" + jsonObj.getString("landlineNumber") + "</font><br>" +
                                "<b><font color=#000000> Amount: </font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + jsonObj.getString("amount") + "</font><br>" +
                                "<b><font color=#000000> Ac No: </font></b>" + "<font>" + jsonObj.getString("accountNumber") + "</font><br><br>";
                    } else {
                        source = "<b><font color=#000000> Service Provider: </font></b>" + "<font color=#000000>" + jsonObj.getString("serviceProvider") + "</font><br>" +
                                "<b><font color=#000000> STD Code: </font></b>" + "<font>" + jsonObj.getString("stdCode") + "</font><br>" +
                                "<b><font color=#000000> Phone No: </font></b>" + "<font>" + jsonObj.getString("landlineNumber") + "</font><br>" +
                                "<b><font color=#000000> Amount: </font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + jsonObj.getString("amount") + "</font><br>";
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (getIntent().getStringExtra("TYPE").equals("QWICKPAY")) {
            source = "<b><font color=#000000> Service Provider: </font></b>" + "<font color=#000000>Payment to merchant successful </font><br>";
        }

        return source;
    }

    public void showBlockDialog() {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(AppMetadata.getBlockSession(), Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(AppMetadata.getBlockSession());
        }
        CustomBlockDialog builder = new CustomBlockDialog(MainMenuDetailActivity.this, "Please contact customer care.", result);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
                dialog.dismiss();
                finish();
            }
        });
        builder.show();
    }

    private void logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        Crashlytics.setUserIdentifier(session.getUserMobileNo());
        Crashlytics.setUserEmail(session.getUserEmail());
        Crashlytics.setUserName(session.getUserFirstName() + " " + session.getUserLastName());
    }
}
