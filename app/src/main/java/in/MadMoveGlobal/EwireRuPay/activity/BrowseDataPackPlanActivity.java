package in.MadMoveGlobal.EwireRuPay.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.fragment.fragmentbrowseplanPrepaid.FourGFragment;
import in.MadMoveGlobal.fragment.fragmentbrowseplanPrepaid.OtherFragment;
import in.MadMoveGlobal.fragment.fragmentbrowseplandatapack.ThreeGDataFragment;
import in.MadMoveGlobal.fragment.fragmentbrowseplandatapack.TwoGDataFragment;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.model.MobilePlansModel;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.util.MobilePlansCheck;


/**
 * Created by Ksf on 3/23/2016.
 */
public class BrowseDataPackPlanActivity extends AppCompatActivity {
    /*
     * Sliding tabs Setup
     */
    private TabLayout mSlidingTabLayout;
    private ViewPager mainPager;
    private FragmentManager fragmentManager;
    CharSequence TitlesEnglish[] = {"2G", "3G","4G","Plan"};
    int NumbOfTabs = 4;

    private String operatorCode;
    private String circleCode;
    private Toolbar toolbar;
    private ImageButton ivBackBtn;

    private JSONObject jsonRequest;
    private LoadingDialog loadingDialog;

    //Volley Tag
    private String tag_json_obj = "json_browse_plan";
    private String prepaidNo;
    private String type;
    private LinearLayout llLRMain;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadingDialog = new LoadingDialog(BrowseDataPackPlanActivity.this);
        fragmentManager = getSupportFragmentManager();
        setContentView(R.layout.activity_tab_layout);
        toolbar = (Toolbar) findViewById(R.id.toolbars);
        setSupportActionBar(toolbar);

        ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtns);
        ivBackBtn.setVisibility(View.VISIBLE);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        circleCode = getIntent().getStringExtra("circleCode");
        prepaidNo = getIntent().getStringExtra("prepaidNo");
        type = getIntent().getStringExtra("type");
        operatorCode = getIntent().getStringExtra("operatorCode");
        mainPager = (ViewPager) findViewById(R.id.mainPager);
        mSlidingTabLayout = (TabLayout) findViewById(R.id.sliding_tabs_home);
        llLRMain = (LinearLayout)findViewById(R.id.llLRMain);

        browsePlan();
    }

    public class ViewPagerAdapter extends FragmentStatePagerAdapter {

        CharSequence Titles[]; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
        int NumbOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created


        // Build a Constructor and assign the passed Values to appropriate values in the class
        public ViewPagerAdapter(FragmentManager fm, CharSequence mTitles[], int mNumbOfTabsumb) {
            super(fm);
            this.Titles = mTitles;
            this.NumbOfTabs = mNumbOfTabsumb;

        }

        @Override
        public Fragment getItem(int position) {

            if (position == 0) {
                TwoGDataFragment tab1 = new TwoGDataFragment(prepaidNo, type);
                return tab1;
            } else if(position==1) {
                ThreeGDataFragment tab2 = new ThreeGDataFragment(prepaidNo, type);
                return tab2;
            }else if(position==3) {
                FourGFragment tab3 = new FourGFragment(prepaidNo, type);
                return tab3;
            }else {
                OtherFragment tab4 = new OtherFragment(prepaidNo,type);
                return tab4;
            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return Titles[position];
        }


        @Override
        public int getCount() {
            return NumbOfTabs;
        }
    }


    private void browsePlan() {
        jsonRequest = new JSONObject();
        loadingDialog.show();
        try {
            jsonRequest.put("circleCode", circleCode);
            jsonRequest.put("serviceProvider", operatorCode);
            jsonRequest.put("rechargeType", "");
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_BROWSE_PLANS, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObj) {
                    ArrayList<MobilePlansModel> talkTimePlans = new ArrayList<>();
                    ArrayList<MobilePlansModel> topUpPlans = new ArrayList<>();
                    ArrayList<MobilePlansModel> localPlans = new ArrayList<>();
                    ArrayList<MobilePlansModel> planPlans = new ArrayList<>();
                    ArrayList<MobilePlansModel> stdPlans = new ArrayList<>();
                    ArrayList<MobilePlansModel> isdPlans = new ArrayList<>();
                    ArrayList<MobilePlansModel> smsPlans = new ArrayList<>();
                    ArrayList<MobilePlansModel> fourGPlans = new ArrayList<>();
                    ArrayList<MobilePlansModel> threeGPlans = new ArrayList<>();
                    ArrayList<MobilePlansModel> twoGPlans = new ArrayList<>();
                    ArrayList<MobilePlansModel> otherPlans = new ArrayList<>();



                    try {
//                        String responseString = jsonObj.getString("details");
//                        JSONObject response = new JSONObject(responseString);

                        JSONArray planArray = jsonObj.getJSONArray("details");
                        for (int i = 0; i < planArray.length(); i++) {
                            JSONObject c = planArray.getJSONObject(i);
                            String plan_name = c.getString("recharge_type").trim();
                            String plan_desc = c.getString("recharge_long_desc");
                            String plan_amount = c.getString("recharge_amount");
                            String plan_validity = c.getString("recharge_validity");
                            String plan_operator_code = c.getString("recharge_type");

                            if (plan_name.equals("Full Talktime")) {
                                talkTimePlans.add(new MobilePlansModel(plan_validity, plan_amount, plan_name, plan_desc, plan_operator_code));
                            } else if (plan_name.equals("Top up")) {
                                topUpPlans.add(new MobilePlansModel(plan_validity, plan_amount, plan_name, plan_desc, plan_operator_code));
                            } else if (plan_name.equals("Local") || plan_name.equals("Local")) {
                                localPlans.add(new MobilePlansModel(plan_validity, plan_amount, plan_name, plan_desc, plan_operator_code));
                            } else if (plan_name.equals("Plan") || plan_name.equals("Plan")) {
                                planPlans.add(new MobilePlansModel(plan_validity, plan_amount, plan_name, plan_desc, plan_operator_code));
                            }else if (plan_name.equals("STD")) {
                                stdPlans.add(new MobilePlansModel(plan_validity, plan_amount, plan_name, plan_desc, plan_operator_code));
                            } else if (plan_name.equals("ISD") || plan_name.equals("ISD")) {
                                isdPlans.add(new MobilePlansModel(plan_validity, plan_amount, plan_name, plan_desc, plan_operator_code));
                            } else if (plan_name.equals("SMS") || plan_name.equals("SMS")) {
                                smsPlans.add(new MobilePlansModel(plan_validity, plan_amount, plan_name, plan_desc, plan_operator_code));
                            }else if (plan_name.equals("4G Data")) {
                                fourGPlans.add(new MobilePlansModel(plan_validity, plan_amount, plan_name, plan_desc, plan_operator_code));
                            } else if (plan_name.equals("3G Data") || plan_name.equals("3G Plans")) {
                                threeGPlans.add(new MobilePlansModel(plan_validity, plan_amount, plan_name, plan_desc, plan_operator_code));
                            } else if (plan_name.equals("2G Data") || plan_name.equals("Data Plans")) {
                                twoGPlans.add(new MobilePlansModel(plan_validity, plan_amount, plan_name, plan_desc, plan_operator_code));
                            } else {
                                otherPlans.add(new MobilePlansModel(plan_validity, plan_amount, plan_name, plan_desc, plan_operator_code));
                            }




                        }

                        MobilePlansCheck plans = MobilePlansCheck.getInstance();
                        plans.setTalkTimePlans(talkTimePlans);
                        plans.setTopUpPlans(topUpPlans);
                        plans.setThreeGPlans(threeGPlans);
                        plans.setTwoGPlans(twoGPlans);
                        plans.setOtherPlans(otherPlans);
                        plans.setLocalPlans(localPlans);
                        plans.setSMSPlans(smsPlans);
                        plans.setSTDPlans(stdPlans);
                        plans.setISDPlans(isdPlans);
                        plans.setPlanPlans(planPlans);
                        plans.setFourGPlans(fourGPlans);

                        mainPager.setAdapter(new ViewPagerAdapter(fragmentManager, TitlesEnglish, NumbOfTabs));
                        mSlidingTabLayout.setupWithViewPager(mainPager);
                        loadingDialog.dismiss();



                    } catch (JSONException e) {
                        e.printStackTrace();
                        Snackbar.make(llLRMain, "Invalid circle or operator", Snackbar.LENGTH_LONG).show();
                        loadingDialog.dismiss();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadingDialog.dismiss();
                    error.printStackTrace();
                    Snackbar.make(llLRMain, "Error connecting to server", Snackbar.LENGTH_LONG).show();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "123");

                    String basicAuth = "Basic " + new String(Base64.encode("user:pass".getBytes(), Base64.NO_WRAP));
                    map.put("Authorization", basicAuth);
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }
//    public static final int getColors(Context context, int id) {
//        final int version = Build.VERSION.SDK_INT;
//        if (version >= 23) {
//            return ContextCompat.getColor(context, id);
//        } else {
//            return context.getResources().getColor(id);
//        }
//    }

}
