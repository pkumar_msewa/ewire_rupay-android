package in.MadMoveGlobal.EwireRuPay.activity.businneractivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import in.MadMoveGlobal.custom.CustomAlertDialog;
import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.custom.LoadingDialog;
//import in.paulpay.fragment.LoadMoneyFragment;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.metadata.AppMetadata;
import in.MadMoveGlobal.model.BusBoardingPointModel;
import in.MadMoveGlobal.model.BusDropingPointModel;
import in.MadMoveGlobal.model.BusListModel;
import in.MadMoveGlobal.model.BusSeatModel;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.util.NetworkErrorHandler;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;


/**
 * Created by Ksf on 9/30/2016.
 */
public class BusDetailActivity extends AppCompatActivity {
    static BusListModel busModel;
    private String tag_json_obj = "json_travel";
    private LoadingDialog loadingDialog;
    private ArrayList<BusSeatModel> busSeatLowerList;
    private ArrayList<BusSeatModel> busSeatUpperList;

    static String GetTxnIdUpdated;

    static long sourceId, desId;
    static String jnrDate, bpId, dpId;
    private Toolbar toolbar;
    private ImageButton ivBackBtn;
    private GridLayout glBusSeat;
    private int engineId;
    private boolean seater, sleeper, bpDpLayout;

    static HashMap seathashValue;

    private Button btnSeatLower, btnSeatUpper;
    private MaterialEditText etBusBookBoardingLocation, etBusBookDroppingLocation;
    private ArrayList<Integer> seatColArray = new ArrayList<>();
    private ArrayList<Integer> seatRowArray = new ArrayList<>();

    private TextView tvBusDetailName, tvBusDetailAbout;
    JsonObjectRequest postReqs;
    private TextView tvBusPaymentSeatsNo, tvBusPaymentAmount;
    private HashMap<String, BusSeatModel> seatSelectedMap = new HashMap<>();

    private Button btnBusPaymentDone;
    static String boardingPointId, droppingPointId, boardingTimePrime, droppingTimePrime, boardingTime, droppingTime;

    private JSONObject jsonRequest;
    private UserModel session = UserModel.getInstance();
    private int mastercol, mastercolup = 0;
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getStringExtra("ticket");
            if (action.equals("1")) {
                finish();
            }

        }
    };
    private ArrayList<BusBoardingPointModel> busBoardingPointModels;
    private ArrayList<BusDropingPointModel> busDroppingPointModels;
    static BusDropingPointModel dropingModel;
    static BusBoardingPointModel busBoardingModel;

    static String busName, busDetails;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadingDialog = new LoadingDialog(BusDetailActivity.this);
        setContentView(R.layout.activity_bus_detail_seat);
        glBusSeat = (GridLayout) findViewById(R.id.glBusSeat);

        btnSeatUpper = (Button) findViewById(R.id.btnSeatUpper);
        btnSeatLower = (Button) findViewById(R.id.btnSeatLower);
        btnBusPaymentDone = (Button) findViewById(R.id.btnBusPaymentDone);

        btnSeatLower.setBackgroundResource(R.drawable.bg_button_seats_selected);
        btnSeatUpper.setBackgroundResource(R.drawable.bg_button_seats_released);


        tvBusDetailAbout = (TextView) findViewById(R.id.tvBusDetailAbout);
        tvBusDetailName = (TextView) findViewById(R.id.tvBusDetailName);
        tvBusPaymentAmount = (TextView) findViewById(R.id.tvBusPaymentAmount);
        tvBusPaymentSeatsNo = (TextView) findViewById(R.id.tvBusPaymentSeatsNo);
        etBusBookBoardingLocation = (MaterialEditText) findViewById(R.id.etBusBookBoardingLocation);
        etBusBookDroppingLocation = (MaterialEditText) findViewById(R.id.etBusBookDroppingLocation);

        tvBusPaymentAmount.setText(getResources().getString(R.string.rupease) + " 0");

        //press back button in toolbar

        toolbar = (Toolbar) findViewById(R.id.toolbars);
        ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtns);
        setSupportActionBar(toolbar);
        ivBackBtn.setVisibility(View.VISIBLE);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        busSeatLowerList = new ArrayList<>();
        busSeatUpperList = new ArrayList<>();
        seatSelectedMap = new HashMap<>();


        //TODO send all the param to booking activity

        //Getting Data intent
        busModel = getIntent().getParcelableExtra("busValues");
        desId = getIntent().getLongExtra("destinationId", 0);
        sourceId = getIntent().getLongExtra("sourceId", 0);
        jnrDate = getIntent().getStringExtra("date");
        engineId = getIntent().getIntExtra("engineId", 0);
        seater = getIntent().getBooleanExtra("seater", false);
        sleeper = getIntent().getBooleanExtra("sleeper", false);
        busName = getIntent().getStringExtra("busName");
        busDetails = getIntent().getStringExtra("busDetails");

//        busBoardingPointModels = getIntent().getParcelableArrayListExtra("busBoardingArray");
//        busDroppingPointModels = getIntent().getParcelableArrayListExtra("busDroppingArray");
        bpId = getIntent().getStringExtra("bpId");
        dpId = getIntent().getStringExtra("dpId");
        bpDpLayout = getIntent().getBooleanExtra("bpDpLayout", false);
        tvBusDetailName.setText(busName);
        tvBusDetailAbout.setText(busDetails);

        etBusBookDroppingLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(busDroppingPointModels.size()>0)
                showDroppingDialog();
            }
        });

        etBusBookBoardingLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (busBoardingPointModels.size()>0)
                showBoardingDialog();
            }
        });

        if (busModel != null) {
            getBusSeatListNew();
        }

        btnSeatUpper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (seatSelectedMap.size() != 0) {
                    showUpperChecked();
                } else {
                    showUpper();
                }
            }
        });

        btnSeatLower.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (seatSelectedMap.size() != 0) {
                    showLowerChecked();
                } else {
                    showLower();
                }
            }
        });


        btnBusPaymentDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etBusBookBoardingLocation.getText().toString().trim().isEmpty()) {
                    CustomToast.showMessage(BusDetailActivity.this, getResources().getString(R.string.Please_select_boarding_point));
                    return;
                }
                if (etBusBookDroppingLocation.getText().toString().trim().isEmpty()) {
                    CustomToast.showMessage(BusDetailActivity.this, getResources().getString(R.string.Please_select_dropping_point));
                    return;
                }
                if (getSelectedSeatList().size() != 0 && busBoardingModel != null && dropingModel != null) {

                    getSaveSeat();

                }
            }
        });


//        btnBusPaymentDone.setOnClickListener(view -> {
//            //TODO
//
//            if (etBusBookBoardingLocation.getText().toString().trim().isEmpty()) {
//                CustomToast.showMessage(BusDetailActivity.this, "Please select boarding point");
//                return;
//            }
//            if (etBusBookDroppingLocation.getText().toString().trim().isEmpty()) {
//                CustomToast.showMessage(BusDetailActivity.this, "Please select dropping point");
//                return;
//            }
//
//            if (getSelectedSeatList().size() != 0 && busBoardingModel != null && dropingModel != null) {
//
//                JSONObject jsonObject = new JSONObject();
//                JSONObject jsonObject1 = new JSONObject();
//                try {
//                    jsonObject.put("sessionId", session.getUserSessionId());
//                    jsonObject.put("busType", busModel.getBusType());
//                    jsonObject.put("travelName", busModel.getBusTravel());
//                    jsonObject.put("journeyDate", busModel.getJnrDate());
//                    jsonObject.put("seatDetail", getSelectedSeatList().size());
//                    jsonObject.put("totalFare", tvBusPaymentAmount.getText().toString().replaceAll(getResources().getString(R.string.rupease), ""));
//                    jsonObject.put("depTime", busModel.getBusDepTime());
//                    jsonObject.put("arrTime", busModel.getBusArrivalTime());
//                    jsonObject.put("source", busModel.getBusSourceName());
//                    jsonObject.put("destination", busModel.getBusDesName());
//                    jsonObject.put("boardTime", boardingTime);
//                    jsonObject.put("busId", busModel.getTripId());
//                    jsonObject.put("boardId", boardingPointId);
//                    jsonObject.put("boardLocation", etBusBookBoardingLocation.getText().toString());
//                    jsonObject.put("boardContactNo", "");
//                    try {
////            jsonObject1.put("data", AESCrypt.encrypt(StringEscapeUtils.unescapeJson(jsonObject.toString()).replaceAll("\\n", "")).replaceAll("\\\\/", ""));
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//                loadingDialog.show();
//                try {
//                    postReqs = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_BUS_GET_SEAT_SAVE_DETAILS, jsonRequest, new Response.Listener<JSONObject>() {
//                        //                    AndroidNetworking.post(ApiUrl.URL_BUS_GET_SEAT_SAVE_DETAILS)
////                            .addJSONObjectBody(jsonObject)
////                            .build().getAsJSONObject(new JSONObjectRequestListener() {
//                        @Override
//                        public void onResponse(JSONObject response) {
//                            if (response.has("code")) {
//
//                                String code = null;
//                                try {
//                                    code = response.getString("code");
//
//                                    if (code.equalsIgnoreCase("S00")) {
//                                        loadingDialog.dismiss();
//                                        String GetTxnIdUpdated = response.getString("tripId");
//                                        Intent busBookIntent = new Intent(BusDetailActivity.this, BusBookActivity.class);
//                                        busBookIntent.putExtra("SeatHashValue", getSelectedSeatList());
//                                        busBookIntent.putExtra("busBoardingPointModels", busBoardingModel);
//                                        busBookIntent.putExtra("busDroppingPointModels", dropingModel);
//                                        busBookIntent.putExtra("desId", desId);
//                                        busBookIntent.putExtra("sourceId", sourceId);
//                                        busBookIntent.putExtra("jnrDate", jnrDate);
//                                        busBookIntent.putExtra("boardingPointId", boardingPointId);
//                                        busBookIntent.putExtra("jnrDate", jnrDate);
//                                        busBookIntent.putExtra("busValues", busModel);
//                                        busBookIntent.putExtra("busName", busName);
//                                        busBookIntent.putExtra("busDetails", busDetails);
//                                        busBookIntent.putExtra("GetTxnIdUpdated", GetTxnIdUpdated);
//                                        startActivity(busBookIntent);
//
//                                    } else if (code.equalsIgnoreCase("F03")) {
//                                        loadingDialog.dismiss();
//                                        showInvalidSessionDialog();
//                                    } else {
//                                        loadingDialog.dismiss();
//                                        CustomToast.showMessage(BusDetailActivity.this, response.getString("message"));
//                                    }
//                                } catch (JSONException e) {
//                                    e.printStackTrace();
//                                }
//
//                            }
//
//                        }
//                    }, new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            loadingDialog.dismiss();
//                            CustomToast.showMessage(BusDetailActivity.this, NetworkErrorHandler.getMessage(error, BusDetailActivity.this));
////                            Snackbar.make(llLayouts, NetworkErrorHandler.getMessage(error, getActivity()), Snackbar.LENGTH_LONG).show();
//
//                            error.printStackTrace();
//
//                        }
//                    }) {
//                        @Override
//                        public Map<String, String> getHeaders() throws AuthFailureError {
//                            HashMap<String, String> map = new HashMap<>();
//                            map.put("hash", "1234");
//                            String basicAuth = "Basic " + new String(Base64.encode("user:pass".getBytes(), Base64.NO_WRAP));
//                            map.put("Authorization", basicAuth);
//                            return map;
//                        }
//                    };
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                int socketTimeout = 60000;
//                RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//                postReqs.setRetryPolicy(policy);
//                PayQwikApplication.getInstance().addToRequestQueue(postReqs, tag_json_obj);
//
//            } else {
//                CustomToast.showMessage(BusDetailActivity.this, "Select Seat to continue");
//            }
//        });

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("booking-done"));

    }

    private void showDroppingDialog() {
        final ArrayList<String> placeVal = new ArrayList<>();

        for (BusDropingPointModel placeModel : busDroppingPointModels) {
            placeVal.add(placeModel.getDpName());
        }

        android.support.v7.app.AlertDialog.Builder placeDialog =
                new android.support.v7.app.AlertDialog.Builder(BusDetailActivity.this, R.style.AppCompatAlertDialogStyle);
        placeDialog.setTitle(getResources().getString(R.string.Please_select_dropping_point));

        placeDialog.setNegativeButton(getResources().getString(R.string.Dismiss), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        placeDialog.setItems(placeVal.toArray(new String[placeVal.size()]),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int i) {
                        etBusBookDroppingLocation.setText(busDroppingPointModels.get(i).getDpName());
                        dropingModel = (BusDropingPointModel) busDroppingPointModels.get(i);
                        droppingPointId = busDroppingPointModels.get(i).getDpId();
                        droppingTimePrime = busDroppingPointModels.get(i).getPrime();
                        droppingTime = busDroppingPointModels.get(i).getDpTime();
                    }
                });

        placeDialog.show();
    }

    private void showBoardingDialog() {
        final ArrayList<String> placeVal = new ArrayList<>();

        for (BusBoardingPointModel placeModel : busBoardingPointModels) {
            placeVal.add(placeModel.getBdLongName() + "-" + placeModel.getTime());
        }

        android.support.v7.app.AlertDialog.Builder placeDialog =
                new android.support.v7.app.AlertDialog.Builder(BusDetailActivity.this, R.style.AppCompatAlertDialogStyle);
        placeDialog.setTitle(getResources().getString(R.string.Please_select_boarding_point));

        placeDialog.setNegativeButton(getResources().getString(R.string.Dismiss), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        placeDialog.setItems(placeVal.toArray(new String[placeVal.size()]),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int i) {
                        etBusBookBoardingLocation.setText(busBoardingPointModels.get(i).getBdLongName());
                        busBoardingModel = (BusBoardingPointModel) busBoardingPointModels.get(i);
                        boardingPointId = busBoardingPointModels.get(i).getBdid();
                        boardingTimePrime = busBoardingPointModels.get(i).getPrime();
                        boardingTime = busBoardingPointModels.get(i).getTime();
                    }
                });

        placeDialog.show();
    }


    public void getSaveSeat() {
        loadingDialog.show();
        jsonRequest = new JSONObject();
//        loadDlg.show();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("busType", busModel.getBusType());
            jsonRequest.put("travelName", busModel.getBusTravel());
            jsonRequest.put("journeyDate", busModel.getJnrDate());
            jsonRequest.put("seatDetail", getSelectedSeatList().size());
            jsonRequest.put("totalFare", tvBusPaymentAmount.getText().toString().replaceAll(getResources().getString(R.string.rupease), ""));
            jsonRequest.put("depTime", busModel.getBusDepTime());
            jsonRequest.put("arrTime", busModel.getBusArrivalTime());
            jsonRequest.put("source", busModel.getBusSourceName());
            jsonRequest.put("destination", busModel.getBusDesName());
            jsonRequest.put("boardTime", boardingTime);
            jsonRequest.put("busId", busModel.getTripId());
            jsonRequest.put("boardId", boardingPointId);
            jsonRequest.put("boardLocation", etBusBookBoardingLocation.getText().toString());
            jsonRequest.put("boardContactNo", "");
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            postReqs = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_BUS_GET_SEAT_SAVE_DETAILS, jsonRequest, new Response.Listener<JSONObject>() {
                @Override

                public void onResponse(JSONObject response) {
                    if (response.has("code")) {

                        String code = null;
                        try {
                            code = response.getString("code");

                            if (code.equalsIgnoreCase("S00")) {
                                loadingDialog.dismiss();
                                GetTxnIdUpdated = response.getString("tripId");
                                Intent busBookIntent = new Intent(BusDetailActivity.this, BusBookActivity.class);
//                                busBookIntent.putExtra("SeatHashValue", getSelectedSeatList());
                                seathashValue = getSelectedSeatList();
                             /*   busBookIntent.putExtra("busBoardingPointModels", busBoardingModel);
                                busBookIntent.putExtra("busDroppingPointModels", dropingModel);
                                busBookIntent.putExtra("desId", desId);
                                busBookIntent.putExtra("sourceId", sourceId);
                                busBookIntent.putExtra("jnrDate", jnrDate);
                                busBookIntent.putExtra("boardingPointId", boardingPointId);
                                busBookIntent.putExtra("jnrDate", jnrDate);
                                busBookIntent.putExtra("busValues", busModel);
                                busBookIntent.putExtra("busName", busName);
                                busBookIntent.putExtra("busDetails", busDetails);
                                busBookIntent.putExtra("GetTxnIdUpdated", GetTxnIdUpdated);*/
                                startActivity(busBookIntent);

                            } else if (code.equalsIgnoreCase("F03")) {
                                loadingDialog.dismiss();
                                showInvalidSessionDialog();
                            } else {
                                loadingDialog.dismiss();
                                CustomToast.showMessage(BusDetailActivity.this, response.getString("message"));
                            }
                        } catch (JSONException e) {
                            loadingDialog.dismiss();
                            e.printStackTrace();
                        }

                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadingDialog.dismiss();
                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");

                    String basicAuth = "Basic " + new String(Base64.encode("user:pass".getBytes(), Base64.NO_WRAP));
                    map.put("Authorization", basicAuth);


                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReqs.setRetryPolicy(policy);
            loadingDialog.dismiss();
            PayQwikApplication.getInstance().addToRequestQueue(postReqs, tag_json_obj);
        }
    }


    private void getBusSeatListNew() {
        loadingDialog.show();
        jsonRequest = new JSONObject();
        JSONObject jsonObject = new JSONObject();
        try {
//            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("busId", busModel.getTripId());
            jsonRequest.put("seater", seater);
            jsonRequest.put("sleeper", sleeper);
            jsonRequest.put("engineId", engineId);
            jsonRequest.put("journeyDate", jnrDate);
            jsonRequest.put("bpId", bpId);
            jsonRequest.put("dpId", dpId);
            jsonRequest.put("bpdpLayout", bpDpLayout);
            jsonRequest.put("routeid", busModel.getRouteId());
            jsonRequest.put("searchReq", sourceId + "|" + desId);
            try {
//        jsonObject.put("data", AESCrypt.encrypt(StringEscapeUtils.unescapeJson(jsonRequest.toString()).replaceAll("\\n", "")).replaceAll("\\\\/", ""));
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            JsonObjectRequest postReq = null;
            try {
                postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_BUS_SEAT_DETAILS, jsonRequest, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String code = response.getString("code");
                            String msg = response.getString("message");
                            if (code != null && code.equals("S00")) {
//                                String detaill = response.getString("details");
//                                JSONObject detaills = new JSONObject(detaill);

                                if(!response.isNull("listBoardingPoints")){

                                    busBoardingPointModels = new ArrayList<>();
                                    busDroppingPointModels = new ArrayList<>();

                                    try {
                                        JSONArray jsonArray = response.getJSONArray("listBoardingPoints");
                                        JSONArray jsonArray1 = response.getJSONArray("listDropingPoints");

                                        if (jsonArray!=null && jsonArray.length()>0) {
                                            Gson gson = new Gson();
                                            Type type = new TypeToken<ArrayList<BusBoardingPointModel>>() {}.getType();
                                            busBoardingPointModels = gson.fromJson(response.getString("listBoardingPoints"),type);
                                        }
                                        if (jsonArray1!=null && jsonArray1.length()>0) {
                                            Gson gson = new Gson();
                                            Type type = new TypeToken<ArrayList<BusDropingPointModel>>() {}.getType();
                                            busDroppingPointModels = gson.fromJson(response.getString("listDropingPoints"),type);
                                        }
                                    }catch (Exception e){e.printStackTrace();}
                                }

                                if (response.isNull("lower") && response.isNull("upper")) {
                                    loadingDialog.dismiss();
                                    CustomToast.showMessage(getApplicationContext(), "Please check your Intenet Connection...");
                                } else {
                                    if (!response.isNull("lower")) {
                                        JSONObject lower = response.getJSONObject("lower");
                                        if (!lower.isNull("firstColumn")) {
                                            JSONArray firstColumn = lower.getJSONArray("firstColumn");
                                            for (int i = 0; i < firstColumn.length(); i++) {
                                                JSONObject c = firstColumn.getJSONObject(i);
                                                int column = Integer.parseInt(c.getString("column"));
                                                String seatFare = String.valueOf(c.getDouble("fare"));
                                                String netFare = String.valueOf(c.getDouble("baseFare"));
                                                String isAvailable = c.getBoolean("available") ? "true" : "false";
                                                String isLadiesSeat = c.getString("gender").equals("Male") || c.getString("gender").equals("M") ? "false" : "true";
                                                int seatLength = c.getString("seatStyle").equals("SL") ? 2 : 1;
                                                int seatWidth = c.getString("seatStyle").equals("SS") ? 2 : 1;
                                                String seatNo = c.getString("name");

                                                int seatRow = Integer.parseInt(c.getString("row"));
//                                                int column = Integer.parseInt(c.getString("row"));
//                                                int seatRow = Integer.parseInt(c.getString("column"));
                                                String seatType = c.getString("seatType");

                                                seatColArray.add(column);
                                                seatRowArray.add(seatRow);

                                                int seatZIndex = 0;
                                                String seatServiceTax = "";
                                                String seatOptServiceCharge = "";
                                                String seatCode = c.getString("id");

                                                BusSeatModel busSeatModel = new BusSeatModel(column, seatFare, netFare, isAvailable, isLadiesSeat, seatLength, seatNo, seatRow, seatWidth, seatZIndex, seatServiceTax, seatOptServiceCharge, seatCode, seatType);
                                                busSeatLowerList.add(busSeatModel);

                                            }
                                        }
                                        if (!lower.isNull("secondColumn")) {
                                            JSONArray secondColumn = lower.getJSONArray("secondColumn");
                                            for (int i = 0; i < secondColumn.length(); i++) {
                                                JSONObject c = secondColumn.getJSONObject(i);
                                                int column = Integer.parseInt(c.getString("column"));
                                                String seatFare = String.valueOf(c.getDouble("fare"));
                                                String netFare = String.valueOf(c.getDouble("baseFare"));
                                                String isAvailable = c.getBoolean("available") ? "true" : "false";
                                                String isLadiesSeat = c.getString("gender").equals("Male") || c.getString("gender").equals("M") ? "false" : "true";
                                                int seatLength = c.getString("seatStyle").equals("SL") ? 2 : 1;
                                                int seatWidth = c.getString("seatStyle").equals("SS") ? 2 : 1;
                                                int seatRow = Integer.parseInt(c.getString("row"));
//                                                int column = Integer.parseInt(c.getString("row"));
//                                                int seatRow = Integer.parseInt(c.getString("column"));
                                                String seatNo = c.getString("name");
                                                String seatType = c.getString("seatType");

                                                seatColArray.add(column);
                                                seatRowArray.add(seatRow);

                                                int seatZIndex = 0;
                                                String seatServiceTax = "";
                                                String seatOptServiceCharge = "";
                                                String seatCode = c.getString("id");

                                                BusSeatModel busSeatModel = new BusSeatModel(column, seatFare, netFare, isAvailable, isLadiesSeat, seatLength, seatNo, seatRow, seatWidth, seatZIndex, seatServiceTax, seatOptServiceCharge, seatCode, seatType);
                                                busSeatLowerList.add(busSeatModel);

                                            }
                                        }
                                        if (!lower.isNull("thirdColumn")) {
                                            JSONArray thirdColumn = lower.getJSONArray("thirdColumn");
                                            for (int i = 0; i < thirdColumn.length(); i++) {
                                                JSONObject c = thirdColumn.getJSONObject(i);
                                                int column = Integer.parseInt(c.getString("column"));
                                                String seatFare = String.valueOf(c.getDouble("fare"));
                                                String netFare = String.valueOf(c.getDouble("baseFare"));
                                                String isAvailable = c.getBoolean("available") ? "true" : "false";
                                                String isLadiesSeat = c.getString("gender").equals("Male") || c.getString("gender").equals("M") ? "false" : "true";
                                                int seatLength = c.getString("seatStyle").equals("SL") ? 2 : 1;
                                                int seatWidth = c.getString("seatStyle").equals("SS") ? 2 : 1;

                                                String seatNo = c.getString("name");
                                                int seatRow = Integer.parseInt(c.getString("row"));
//                                                int column = Integer.parseInt(c.getString("row"));
//                                                int seatRow = Integer.parseInt(c.getString("column"));
                                                String seatType = c.getString("seatType");

                                                seatColArray.add(column);
                                                seatRowArray.add(seatRow);

                                                int seatZIndex = 0;
                                                String seatServiceTax = "";
                                                String seatOptServiceCharge = "";
                                                String seatCode = c.getString("id");

                                                BusSeatModel busSeatModel = new BusSeatModel(column, seatFare, netFare, isAvailable, isLadiesSeat, seatLength, seatNo, seatRow, seatWidth, seatZIndex, seatServiceTax, seatOptServiceCharge, seatCode, seatType);
                                                busSeatLowerList.add(busSeatModel);

                                            }
                                        }
                                        if (!lower.isNull("fourthColumn")) {
                                            JSONArray fourthColumn = lower.getJSONArray("fourthColumn");
                                            for (int i = 0; i < fourthColumn.length(); i++) {
                                                JSONObject c = fourthColumn.getJSONObject(i);
                                                int column = Integer.parseInt(c.getString("column"));
                                                String seatFare = String.valueOf(c.getDouble("fare"));
                                                String netFare = String.valueOf(c.getDouble("baseFare"));
                                                String isAvailable = c.getBoolean("available") ? "true" : "false";
                                                String isLadiesSeat = c.getString("gender").equals("Male") || c.getString("gender").equals("M") ? "false" : "true";
                                                int seatLength = c.getString("seatStyle").equals("SL") ? 2 : 1;
                                                int seatWidth = c.getString("seatStyle").equals("SS") ? 2 : 1;

                                                String seatNo = c.getString("name");
                                                int seatRow = Integer.parseInt(c.getString("row"));
//                                                int column = Integer.parseInt(c.getString("row"));
//                                                int seatRow = Integer.parseInt(c.getString("column"));
                                                String seatType = c.getString("seatType");

                                                seatColArray.add(column);
                                                seatRowArray.add(seatRow);

                                                int seatZIndex = 0;
                                                String seatServiceTax = "";
                                                String seatOptServiceCharge = "";
                                                String seatCode = c.getString("id");

                                                BusSeatModel busSeatModel = new BusSeatModel(column, seatFare, netFare, isAvailable, isLadiesSeat, seatLength, seatNo, seatRow, seatWidth, seatZIndex, seatServiceTax, seatOptServiceCharge, seatCode, seatType);
                                                busSeatLowerList.add(busSeatModel);

                                            }
                                        }
                                        if (!lower.isNull("fifthColumn")) {
                                            JSONArray fifthColumn = lower.getJSONArray("fifthColumn");
                                            for (int i = 0; i < fifthColumn.length(); i++) {
                                                JSONObject c = fifthColumn.getJSONObject(i);
                                                int column = Integer.parseInt(c.getString("column"));
                                                String seatFare = String.valueOf(c.getDouble("fare"));
                                                String netFare = String.valueOf(c.getDouble("baseFare"));
                                                String isAvailable = c.getBoolean("available") ? "true" : "false";
                                                String isLadiesSeat = c.getString("gender").equals("Male") || c.getString("gender").equals("M") ? "false" : "true";
                                                int seatLength = c.getString("seatStyle").equals("SL") ? 2 : 1;
                                                int seatWidth = c.getString("seatStyle").equals("SS") ? 2 : 1;

                                                String seatNo = c.getString("name");
                                                int seatRow = Integer.parseInt(c.getString("row"));
//                                                int column = Integer.parseInt(c.getString("row"));
//                                                int seatRow = Integer.parseInt(c.getString("column"));
                                                String seatType = c.getString("seatType");

                                                seatColArray.add(column);
                                                seatRowArray.add(seatRow);

                                                int seatZIndex = 0;
                                                String seatServiceTax = "";
                                                String seatOptServiceCharge = "";
                                                String seatCode = c.getString("id");

                                                BusSeatModel busSeatModel = new BusSeatModel(column, seatFare, netFare, isAvailable, isLadiesSeat, seatLength, seatNo, seatRow, seatWidth, seatZIndex, seatServiceTax, seatOptServiceCharge, seatCode, seatType);
                                                busSeatLowerList.add(busSeatModel);

                                            }
                                        }
                                        if (!lower.isNull("sixthColumn")) {
                                            JSONArray sixthColumn = lower.getJSONArray("sixthColumn");
                                            for (int i = 0; i < sixthColumn.length(); i++) {
                                                JSONObject c = sixthColumn.getJSONObject(i);
                                                int column = Integer.parseInt(c.getString("column"));
                                                String seatFare = String.valueOf(c.getDouble("fare"));
                                                String netFare = String.valueOf(c.getDouble("baseFare"));
                                                String isAvailable = c.getBoolean("available") ? "true" : "false";
                                                String isLadiesSeat = c.getString("gender").equals("Male") || c.getString("gender").equals("M") ? "false" : "true";
                                                int seatLength = c.getString("seatStyle").equals("SL") ? 2 : 1;
                                                int seatWidth = c.getString("seatStyle").equals("SS") ? 2 : 1;

                                                String seatNo = c.getString("name");
                                                int seatRow = Integer.parseInt(c.getString("row"));
//                                                int column = Integer.parseInt(c.getString("row"));
//                                                int seatRow = Integer.parseInt(c.getString("column"));
                                                String seatType = c.getString("seatType");

                                                seatColArray.add(column);
                                                seatRowArray.add(seatRow);

                                                int seatZIndex = 0;
                                                String seatServiceTax = "";
                                                String seatOptServiceCharge = "";
                                                String seatCode = c.getString("id");

                                                BusSeatModel busSeatModel = new BusSeatModel(column, seatFare, netFare, isAvailable, isLadiesSeat, seatLength, seatNo, seatRow, seatWidth, seatZIndex, seatServiceTax, seatOptServiceCharge, seatCode, seatType);
                                                busSeatLowerList.add(busSeatModel);

                                            }
                                        }
                                        if (!lower.isNull("seventhColumn")) {
                                            JSONArray seventhColumn = lower.getJSONArray("seventhColumn");
                                            for (int i = 0; i < seventhColumn.length(); i++) {
                                                JSONObject c = seventhColumn.getJSONObject(i);
                                                int column = Integer.parseInt(c.getString("column"));
                                                String seatFare = String.valueOf(c.getDouble("fare"));
                                                String netFare = String.valueOf(c.getDouble("baseFare"));
                                                String isAvailable = c.getBoolean("available") ? "true" : "false";
                                                String isLadiesSeat = c.getString("gender").equals("Male") || c.getString("gender").equals("M") ? "false" : "true";
                                                int seatLength = c.getString("seatStyle").equals("SL") ? 2 : 1;
                                                int seatWidth = c.getString("seatStyle").equals("SS") ? 2 : 1;

                                                String seatNo = c.getString("name");
                                                int seatRow = Integer.parseInt(c.getString("row"));
//                                                int column = Integer.parseInt(c.getString("row"));
//                                                int seatRow = Integer.parseInt(c.getString("column"));
                                                String seatType = c.getString("seatType");

                                                seatColArray.add(column);
                                                seatRowArray.add(seatRow);

                                                int seatZIndex = 0;
                                                String seatServiceTax = "";
                                                String seatOptServiceCharge = "";
                                                String seatCode = c.getString("id");

                                                BusSeatModel busSeatModel = new BusSeatModel(column, seatFare, netFare, isAvailable, isLadiesSeat, seatLength, seatNo, seatRow, seatWidth, seatZIndex, seatServiceTax, seatOptServiceCharge, seatCode, seatType);
                                                busSeatLowerList.add(busSeatModel);

                                            }
                                        }
                                        if (!lower.isNull("eightColumn")) {
                                            JSONArray eightColumn = lower.getJSONArray("eightColumn");
                                            for (int i = 0; i < eightColumn.length(); i++) {
                                                JSONObject c = eightColumn.getJSONObject(i);
                                                int column = Integer.parseInt(c.getString("column"));
                                                String seatFare = String.valueOf(c.getDouble("fare"));
                                                String netFare = String.valueOf(c.getDouble("baseFare"));
                                                String isAvailable = c.getBoolean("available") ? "true" : "false";
                                                String isLadiesSeat = c.getString("gender").equals("Male") || c.getString("gender").equals("M") ? "false" : "true";
                                                int seatLength = c.getString("seatStyle").equals("SL") ? 2 : 1;
                                                int seatWidth = c.getString("seatStyle").equals("SS") ? 2 : 1;

                                                String seatNo = c.getString("name");
                                                int seatRow = Integer.parseInt(c.getString("row"));
//                                                int column = Integer.parseInt(c.getString("row"));
//                                                int seatRow = Integer.parseInt(c.getString("column"));
                                                String seatType = c.getString("seatType");

                                                seatColArray.add(column);
                                                seatRowArray.add(seatRow);

                                                int seatZIndex = 0;
                                                String seatServiceTax = "";
                                                String seatOptServiceCharge = "";
                                                String seatCode = c.getString("id");

                                                BusSeatModel busSeatModel = new BusSeatModel(column, seatFare, netFare, isAvailable, isLadiesSeat, seatLength, seatNo, seatRow, seatWidth, seatZIndex, seatServiceTax, seatOptServiceCharge, seatCode, seatType);
                                                busSeatLowerList.add(busSeatModel);

                                            }
                                        }
                                        if (!lower.isNull("ninethColumn")) {
                                            JSONArray ninethColumn = lower.getJSONArray("ninethColumn");
                                            for (int i = 0; i < ninethColumn.length(); i++) {
                                                JSONObject c = ninethColumn.getJSONObject(i);
                                                int column = Integer.parseInt(c.getString("column"));
                                                String seatFare = String.valueOf(c.getDouble("fare"));
                                                String netFare = String.valueOf(c.getDouble("baseFare"));
                                                String isAvailable = c.getBoolean("available") ? "true" : "false";
                                                String isLadiesSeat = c.getString("gender").equals("Male") || c.getString("gender").equals("M") ? "false" : "true";
                                                int seatLength = c.getString("seatStyle").equals("SL") ? 2 : 1;
                                                int seatWidth = c.getString("seatStyle").equals("SS") ? 2 : 1;

                                                String seatNo = c.getString("name");
                                                int seatRow = Integer.parseInt(c.getString("row"));
//                                                int column = Integer.parseInt(c.getString("row"));
//                                                int seatRow = Integer.parseInt(c.getString("column"));
                                                String seatType = c.getString("seatType");

                                                seatColArray.add(column);
                                                seatRowArray.add(seatRow);

                                                int seatZIndex = 0;
                                                String seatServiceTax = "";
                                                String seatOptServiceCharge = "";
                                                String seatCode = c.getString("id");

                                                BusSeatModel busSeatModel = new BusSeatModel(column, seatFare, netFare, isAvailable, isLadiesSeat, seatLength, seatNo, seatRow, seatWidth, seatZIndex, seatServiceTax, seatOptServiceCharge, seatCode, seatType);
                                                busSeatLowerList.add(busSeatModel);

                                            }
                                        }
                                    }

                                    if (!response.isNull("upper")) {
                                        btnSeatLower.setVisibility(View.VISIBLE);
                                        btnSeatUpper.setVisibility(View.VISIBLE);
                                        JSONObject upper = response.getJSONObject("upper");
                                        if (!upper.isNull("firstColumn")) {
                                            JSONArray firstColumn = upper.getJSONArray("firstColumn");
                                            for (int i = 0; i < firstColumn.length(); i++) {
                                                JSONObject c = firstColumn.getJSONObject(i);
                                                int column = Integer.parseInt(c.getString("column"));
                                                String seatFare = String.valueOf(c.getDouble("fare"));
                                                String netFare = String.valueOf(c.getDouble("baseFare"));
                                                String isAvailable = c.getBoolean("available") ? "true" : "false";
                                                String isLadiesSeat = c.getString("gender").equals("Male") || c.getString("gender").equals("M") ? "false" : "true";
                                                int seatLength = c.getString("seatStyle").equals("SL") ? 2 : 1;
                                                int seatWidth = c.getString("seatStyle").equals("SS") ? 2 : 1;

                                                String seatNo = c.getString("name");
                                                int seatRow = Integer.parseInt(c.getString("row"));
//                                                int column = Integer.parseInt(c.getString("row"));
//                                                int seatRow = Integer.parseInt(c.getString("column"));
                                                String seatType = c.getString("seatType");

                                                seatColArray.add(column);
                                                seatRowArray.add(seatRow);

                                                int seatZIndex = 1;
                                                String seatServiceTax = "";
                                                String seatOptServiceCharge = "";
                                                String seatCode = c.getString("id");

                                                BusSeatModel busSeatModel = new BusSeatModel(column, seatFare, netFare, isAvailable, isLadiesSeat, seatLength, seatNo, seatRow, seatWidth, seatZIndex, seatServiceTax, seatOptServiceCharge, seatCode, seatType);
                                                busSeatUpperList.add(busSeatModel);

                                            }
                                        }
                                        if (!upper.isNull("secondColumn")) {
                                            JSONArray secondColumn = upper.getJSONArray("secondColumn");
                                            for (int i = 0; i < secondColumn.length(); i++) {
                                                JSONObject c = secondColumn.getJSONObject(i);
                                                int column = Integer.parseInt(c.getString("column"));
                                                String seatFare = String.valueOf(c.getDouble("fare"));
                                                String netFare = String.valueOf(c.getDouble("baseFare"));
                                                String isAvailable = c.getBoolean("available") ? "true" : "false";
                                                String isLadiesSeat = c.getString("gender").equals("Male") || c.getString("gender").equals("M") ? "false" : "true";
                                                int seatLength = c.getString("seatStyle").equals("SL") ? 2 : 1;
                                                int seatWidth = c.getString("seatStyle").equals("SS") ? 2 : 1;

                                                String seatNo = c.getString("name");
                                                int seatRow = Integer.parseInt(c.getString("row"));
//                                                int column = Integer.parseInt(c.getString("row"));
//                                                int seatRow = Integer.parseInt(c.getString("column"));
                                                String seatType = c.getString("seatType");

                                                seatColArray.add(column);
                                                seatRowArray.add(seatRow);

                                                int seatZIndex = 1;
                                                String seatServiceTax = "";
                                                String seatOptServiceCharge = "";
                                                String seatCode = c.getString("id");

                                                BusSeatModel busSeatModel = new BusSeatModel(column, seatFare, netFare, isAvailable, isLadiesSeat, seatLength, seatNo, seatRow, seatWidth, seatZIndex, seatServiceTax, seatOptServiceCharge, seatCode, seatType);
                                                busSeatUpperList.add(busSeatModel);

                                            }
                                        }
                                        if (!upper.isNull("thirdColumn")) {
                                            JSONArray thirdColumn = upper.getJSONArray("thirdColumn");
                                            for (int i = 0; i < thirdColumn.length(); i++) {
                                                JSONObject c = thirdColumn.getJSONObject(i);
                                                int column = Integer.parseInt(c.getString("column"));
                                                String seatFare = String.valueOf(c.getDouble("fare"));
                                                String netFare = String.valueOf(c.getDouble("baseFare"));
                                                String isAvailable = c.getBoolean("available") ? "true" : "false";
                                                String isLadiesSeat = c.getString("gender").equals("Male") || c.getString("gender").equals("M") ? "false" : "true";
                                                int seatLength = c.getString("seatStyle").equals("SL") ? 2 : 1;
                                                int seatWidth = c.getString("seatStyle").equals("SS") ? 2 : 1;

                                                String seatNo = c.getString("name");
                                                int seatRow = Integer.parseInt(c.getString("row"));
//                                                int column = Integer.parseInt(c.getString("row"));
//                                                int seatRow = Integer.parseInt(c.getString("column"));
                                                String seatType = c.getString("seatType");

                                                seatColArray.add(column);
                                                seatRowArray.add(seatRow);

                                                int seatZIndex = 1;
                                                String seatServiceTax = "";
                                                String seatOptServiceCharge = "";
                                                String seatCode = c.getString("id");

                                                BusSeatModel busSeatModel = new BusSeatModel(column, seatFare, netFare, isAvailable, isLadiesSeat, seatLength, seatNo, seatRow, seatWidth, seatZIndex, seatServiceTax, seatOptServiceCharge, seatCode, seatType);
                                                busSeatUpperList.add(busSeatModel);

                                            }
                                        }
                                        if (!upper.isNull("fourthColumn")) {
                                            JSONArray fourthColumn = upper.getJSONArray("fourthColumn");
                                            for (int i = 0; i < fourthColumn.length(); i++) {
                                                JSONObject c = fourthColumn.getJSONObject(i);
                                                int column = Integer.parseInt(c.getString("column"));
                                                String seatFare = String.valueOf(c.getDouble("fare"));
                                                String netFare = String.valueOf(c.getDouble("baseFare"));
                                                String isAvailable = c.getBoolean("available") ? "true" : "false";
                                                String isLadiesSeat = c.getString("gender").equals("Male") || c.getString("gender").equals("M") ? "false" : "true";
                                                int seatLength = c.getString("seatStyle").equals("SL") ? 2 : 1;
                                                int seatWidth = c.getString("seatStyle").equals("SS") ? 2 : 1;

                                                String seatNo = c.getString("name");
                                                int seatRow = Integer.parseInt(c.getString("row"));
//                                                int column = Integer.parseInt(c.getString("row"));
//                                                int seatRow = Integer.parseInt(c.getString("column"));
                                                String seatType = c.getString("seatType");

                                                seatColArray.add(column);
                                                seatRowArray.add(seatRow);

                                                int seatZIndex = 1;
                                                String seatServiceTax = "";
                                                String seatOptServiceCharge = "";
                                                String seatCode = c.getString("id");

                                                BusSeatModel busSeatModel = new BusSeatModel(column, seatFare, netFare, isAvailable, isLadiesSeat, seatLength, seatNo, seatRow, seatWidth, seatZIndex, seatServiceTax, seatOptServiceCharge, seatCode, seatType);
                                                busSeatUpperList.add(busSeatModel);

                                            }
                                        }
                                        if (!upper.isNull("fifthColumn")) {
                                            JSONArray fifthColumn = upper.getJSONArray("fifthColumn");
                                            for (int i = 0; i < fifthColumn.length(); i++) {
                                                JSONObject c = fifthColumn.getJSONObject(i);
                                                int column = Integer.parseInt(c.getString("column"));
                                                String seatFare = String.valueOf(c.getDouble("fare"));
                                                String netFare = String.valueOf(c.getDouble("baseFare"));
                                                String isAvailable = c.getBoolean("available") ? "true" : "false";
                                                String isLadiesSeat = c.getString("gender").equals("Male") || c.getString("gender").equals("M") ? "false" : "true";
                                                int seatLength = c.getString("seatStyle").equals("SL") ? 2 : 1;
                                                int seatWidth = c.getString("seatStyle").equals("SS") ? 2 : 1;

                                                String seatNo = c.getString("name");
                                                int seatRow = Integer.parseInt(c.getString("row"));
//                                                int column = Integer.parseInt(c.getString("row"));
//                                                int seatRow = Integer.parseInt(c.getString("column"));
                                                String seatType = c.getString("seatType");

                                                seatColArray.add(column);
                                                seatRowArray.add(seatRow);

                                                int seatZIndex = 1;
                                                String seatServiceTax = "";
                                                String seatOptServiceCharge = "";
                                                String seatCode = c.getString("id");

                                                BusSeatModel busSeatModel = new BusSeatModel(column, seatFare, netFare, isAvailable, isLadiesSeat, seatLength, seatNo, seatRow, seatWidth, seatZIndex, seatServiceTax, seatOptServiceCharge, seatCode, seatType);
                                                busSeatUpperList.add(busSeatModel);

                                            }
                                        }
                                        if (!upper.isNull("sixthColumn")) {
                                            JSONArray sixthColumn = upper.getJSONArray("sixthColumn");
                                            for (int i = 0; i < sixthColumn.length(); i++) {
                                                JSONObject c = sixthColumn.getJSONObject(i);
                                                int column = Integer.parseInt(c.getString("column"));
                                                String seatFare = String.valueOf(c.getDouble("fare"));
                                                String netFare = String.valueOf(c.getDouble("baseFare"));
                                                String isAvailable = c.getBoolean("available") ? "true" : "false";
                                                String isLadiesSeat = c.getString("gender").equals("Male") || c.getString("gender").equals("M") ? "false" : "true";
                                                int seatLength = c.getString("seatStyle").equals("SL") ? 2 : 1;
                                                int seatWidth = c.getString("seatStyle").equals("SS") ? 2 : 1;

                                                String seatNo = c.getString("name");
                                                int seatRow = Integer.parseInt(c.getString("row"));
//                                                int column = Integer.parseInt(c.getString("row"));
//                                                int seatRow = Integer.parseInt(c.getString("column"));
                                                String seatType = c.getString("seatType");

                                                seatColArray.add(column);
                                                seatRowArray.add(seatRow);

                                                int seatZIndex = 1;
                                                String seatServiceTax = "";
                                                String seatOptServiceCharge = "";
                                                String seatCode = c.getString("id");

                                                BusSeatModel busSeatModel = new BusSeatModel(column, seatFare, netFare, isAvailable, isLadiesSeat, seatLength, seatNo, seatRow, seatWidth, seatZIndex, seatServiceTax, seatOptServiceCharge, seatCode, seatType);
                                                busSeatUpperList.add(busSeatModel);
                                            }
                                        }
                                        if (!upper.isNull("seventhColumn")) {
                                            JSONArray seventhColumn = upper.getJSONArray("seventhColumn");
                                            for (int i = 0; i < seventhColumn.length(); i++) {
                                                JSONObject c = seventhColumn.getJSONObject(i);
                                                int column = Integer.parseInt(c.getString("column"));
                                                String seatFare = String.valueOf(c.getDouble("fare"));
                                                String netFare = String.valueOf(c.getDouble("baseFare"));
                                                String isAvailable = c.getBoolean("available") ? "true" : "false";
                                                String isLadiesSeat = c.getString("gender").equals("Male") || c.getString("gender").equals("M") ? "false" : "true";
                                                int seatLength = c.getString("seatStyle").equals("SL") ? 2 : 1;
                                                int seatWidth = c.getString("seatStyle").equals("SS") ? 2 : 1;

                                                String seatNo = c.getString("name");
                                                int seatRow = Integer.parseInt(c.getString("row"));
//                                                int column = Integer.parseInt(c.getString("row"));
//                                                int seatRow = Integer.parseInt(c.getString("column"));
                                                String seatType = c.getString("seatType");

                                                seatColArray.add(column);
                                                seatRowArray.add(seatRow);

                                                int seatZIndex = 1;
                                                String seatServiceTax = "";
                                                String seatOptServiceCharge = "";
                                                String seatCode = c.getString("id");

                                                BusSeatModel busSeatModel = new BusSeatModel(column, seatFare, netFare, isAvailable, isLadiesSeat, seatLength, seatNo, seatRow, seatWidth, seatZIndex, seatServiceTax, seatOptServiceCharge, seatCode, seatType);
                                                busSeatUpperList.add(busSeatModel);

                                            }
                                        }
                                        if (!upper.isNull("eightColumn")) {
                                            JSONArray eightColumn = upper.getJSONArray("eightColumn");
                                            for (int i = 0; i < eightColumn.length(); i++) {
                                                JSONObject c = eightColumn.getJSONObject(i);
                                                int column = Integer.parseInt(c.getString("column"));
                                                String seatFare = String.valueOf(c.getDouble("fare"));
                                                String netFare = String.valueOf(c.getDouble("baseFare"));
                                                String isAvailable = c.getBoolean("available") ? "true" : "false";
                                                String isLadiesSeat = c.getString("gender").equals("Male") || c.getString("gender").equals("M") ? "false" : "true";
                                                int seatLength = c.getString("seatStyle").equals("SL") ? 2 : 1;
                                                int seatWidth = c.getString("seatStyle").equals("SS") ? 2 : 1;

                                                String seatNo = c.getString("name");
                                                int seatRow = Integer.parseInt(c.getString("row"));
//                                                int column = Integer.parseInt(c.getString("row"));
//                                                int seatRow = Integer.parseInt(c.getString("column"));
                                                String seatType = c.getString("seatType");

                                                seatColArray.add(column);
                                                seatRowArray.add(seatRow);

                                                int seatZIndex = 1;
                                                String seatServiceTax = "";
                                                String seatOptServiceCharge = "";
                                                String seatCode = c.getString("id");

                                                BusSeatModel busSeatModel = new BusSeatModel(column, seatFare, netFare, isAvailable, isLadiesSeat, seatLength, seatNo, seatRow, seatWidth, seatZIndex, seatServiceTax, seatOptServiceCharge, seatCode, seatType);
                                                busSeatUpperList.add(busSeatModel);

                                            }
                                        }
                                        if (!upper.isNull("ninethColumn")) {
                                            JSONArray ninethColumn = upper.getJSONArray("ninethColumn");
                                            for (int i = 0; i < ninethColumn.length(); i++) {
                                                JSONObject c = ninethColumn.getJSONObject(i);
                                                int column = Integer.parseInt(c.getString("column"));
                                                String seatFare = String.valueOf(c.getDouble("fare"));
                                                String netFare = String.valueOf(c.getDouble("baseFare"));
                                                String isAvailable = c.getBoolean("available") ? "true" : "false";
                                                String isLadiesSeat = c.getString("gender").equals("Male") || c.getString("gender").equals("M") ? "false" : "true";
                                                int seatLength = c.getString("seatStyle").equals("SL") ? 2 : 1;
                                                int seatWidth = c.getString("seatStyle").equals("SS") ? 2 : 1;

                                                String seatNo = c.getString("name");
                                                int seatRow = Integer.parseInt(c.getString("row"));
//                                                int column = Integer.parseInt(c.getString("row"));
//                                                int seatRow = Integer.parseInt(c.getString("column"));
                                                String seatType = c.getString("seatType");

                                                seatColArray.add(column);
                                                seatRowArray.add(seatRow);

                                                int seatZIndex = 1;
                                                String seatServiceTax = "";
                                                String seatOptServiceCharge = "";
                                                String seatCode = c.getString("id");

                                                BusSeatModel busSeatModel = new BusSeatModel(column, seatFare, netFare, isAvailable, isLadiesSeat, seatLength, seatNo, seatRow, seatWidth, seatZIndex, seatServiceTax, seatOptServiceCharge, seatCode, seatType);
                                                busSeatUpperList.add(busSeatModel);
                                            }
                                        }
                                    }

                                    //if (response.get("Seats") instanceof JSONArray) {
                                    //                            JSONArray seatArray = response.getJSONArray("Seats");

                                    //                            for (int i = 0; i < seatArray.length(); i++) {
                                    //                                JSONObject c = seatArray.getJSONObject(i);
                                    //                                int column = c.getInt("Row");
                                    //                                String seatFare = c.getString("Fare");
                                    //                                String netFare = c.getString("NetFare");
                                    //                                String isAvailable = c.getString("IsAvailableSeat");
                                    //                                String isLadiesSeat = c.getString("IsLadiesSeat");
                                    //                                int seatLength = c.getInt("Length");
                                    //                                String seatNo = c.getString("Number");
                                    //                                int seatRow = c.getInt("Column");
                                    //
                                    //                                seatColArray.add(column);
                                    //                                seatRowArray.add(seatRow);
                                    //
                                    //
                                    //                                int seatWidth = c.getInt("Width");
                                    //                                int seatZIndex = c.getInt("Zindex");
                                    //                                String seatServiceTax = c.getString("Servicetax");
                                    //                                String seatOptServiceCharge = c.getString("OperatorServiceCharge");
                                    //
                                    //                                String seatCode = "";
                                    //                                if (!c.isNull("SeatCode")) {
                                    //                                    seatCode = c.getString("SeatCode");
                                    //                                }
                                    //
                                    //                                BusSeatModel busSeatModel = new BusSeatModel(column, seatFare, netFare, isAvailable, isLadiesSeat, seatLength, seatNo, seatRow, seatWidth, seatZIndex, seatServiceTax, seatOptServiceCharge, seatCode);
                                    //                                if (seatZIndex == 0) {
                                    //                                    busSeatLowerList.add(busSeatModel);
                                    //                                } else if (seatZIndex == 1) {
                                    //                                    btnSeatLower.setVisibility(View.VISIBLE);
                                    //                                    btnSeatUpper.setVisibility(View.VISIBLE);
                                    //                                    busSeatUpperList.add(busSeatModel);
                                    //                                }
                                    //                            }

                                    //Sorting and arranging Col
                                    Object[] stCol = seatColArray.toArray();
                                    for (Object s : stCol) {
                                        if (seatColArray.indexOf(s) != seatColArray.lastIndexOf(s)) {
                                            seatColArray.remove(seatColArray.lastIndexOf(s));
                                        }
                                    }

                                    Collections.sort(seatColArray);
                                    for (int i = 0; i < seatColArray.size(); i++) {
                                    }


                                    //Sorting and arranging Row
                                    Object[] stRow = seatRowArray.toArray();
                                    for (Object s : stRow) {
                                        if (seatRowArray.indexOf(s) != seatRowArray.lastIndexOf(s)) {
                                            seatRowArray.remove(seatRowArray.lastIndexOf(s));
                                        }
                                    }

                                    Collections.sort(seatRowArray);
                                    for (int i = 0; i < seatRowArray.size(); i++) {
                                    }


                                    showLower();

                                    loadingDialog.dismiss();
                                }
                            } else if (code != null && code.equals("F03")) {
                                loadingDialog.dismiss();
                                showInvalidSessionDialog();
                            } else {
                                loadingDialog.dismiss();
                                CustomToast.showMessage(BusDetailActivity.this, msg);

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            loadingDialog.dismiss();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loadingDialog.dismiss();
                        CustomToast.showMessage(getApplicationContext(), NetworkErrorHandler.getMessage(error, getApplicationContext()));
                        error.printStackTrace();
                    }
                }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> map = new HashMap<>();
                        map.put("hash", "1234");
                        String basicAuth = "Basic " + new String(Base64.encode("user:pass".getBytes(), Base64.NO_WRAP));
                        map.put("Authorization", basicAuth);
                        return map;
                    }
                };
            } catch (Exception e) {
                e.printStackTrace();
            }
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    private void showUpper() {

        btnSeatLower.setBackgroundResource(R.drawable.bg_button_seats_released);
        btnSeatUpper.setBackgroundResource(R.drawable.bg_button_seats_selected);

//        btnSeatUpper.setEnabled(false);
//        btnSeatLower.setEnabled(true);


        glBusSeat.removeAllViews();
        for (int i = 0; i < busSeatUpperList.size(); i++) {
            LayoutInflater layoutInflater = LayoutInflater.from(getApplicationContext());
            View seatView = layoutInflater.inflate(R.layout.adapter_bus_seat, null);

            ImageButton iBtnBusSeat = (ImageButton) seatView.findViewById(R.id.iBtnBusSeat);
            iBtnBusSeat.setId(i);

            iBtnBusSeat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (busSeatUpperList.get(view.getId()).getIsAvailable().equals("true") || busSeatUpperList.get(view.getId()).getIsAvailable().equals("True")) {
                        getBtnIdUpper(view.getId(), view);
                    }

                }
            });

            if (busSeatUpperList.get(i).getSeatLength() == 1 && busSeatUpperList.get(i).getSeatWidth() == 1) {
                if (!busSeatUpperList.get(i).getIsAvailable().equals("true") && !busSeatUpperList.get(i).getIsAvailable().equals("True")) {
                    iBtnBusSeat.setImageResource(R.drawable.ic_seat_booked);
                } else {
                    if (busSeatUpperList.get(i).getIsLadiesSeat().equals("true") || busSeatUpperList.get(i).getIsLadiesSeat().equals("True")) {
                        iBtnBusSeat.setImageResource(R.drawable.ic_seat_ladies);
                    } else {
                        iBtnBusSeat.setImageResource(R.drawable.ic_seat);
                    }
                }


            } else if (busSeatUpperList.get(i).getSeatLength() == 2 && busSeatUpperList.get(i).getSeatWidth() == 1) {
                if (!busSeatUpperList.get(i).getIsAvailable().equals("true") && !busSeatUpperList.get(i).getIsAvailable().equals("True")) {
                    iBtnBusSeat.setImageResource(R.drawable.ic_seat_sleeper_booked);
                } else {
                    if (busSeatUpperList.get(i).getIsLadiesSeat().equals("true") || busSeatUpperList.get(i).getIsLadiesSeat().equals("True")) {
                        iBtnBusSeat.setImageResource(R.drawable.ic_seat_sleeper_ladies);
                    } else {
                        iBtnBusSeat.setImageResource(R.drawable.ic_seat_sleeper);
                    }
                }

            } else if (busSeatUpperList.get(i).getSeatLength() == 1 && busSeatUpperList.get(i).getSeatWidth() == 2) {
                if (!busSeatUpperList.get(i).getIsAvailable().equals("true") && !busSeatUpperList.get(i).getIsAvailable().equals("True")) {
                    iBtnBusSeat.setImageResource(R.drawable.ic_seat_sleeper_horizontal_booked);
                } else {
                    if (busSeatUpperList.get(i).getIsLadiesSeat().equals("true") || busSeatUpperList.get(i).getIsLadiesSeat().equals("True")) {
                        iBtnBusSeat.setImageResource(R.drawable.ic_seat_sleeper_horizontal_ladies);
                    } else {
                        iBtnBusSeat.setImageResource(R.drawable.ic_seat_sleeper_horizontal);
                    }
                }
            }
            TextView tvBusSeats = (TextView) seatView.findViewById(R.id.tvBusSeats);
            tvBusSeats.setText(busSeatUpperList.get(i).getSeatNo());
            GridLayout.Spec colSpec = null, rowSpec = null;

            if (busSeatUpperList.get(i).getSeatLength() == 1 && busSeatUpperList.get(i).getSeatWidth() == 1) {
                colSpec = GridLayout.spec(busSeatUpperList.get(i).getColumn() - seatColArray.get(0));
                rowSpec = GridLayout.spec(busSeatUpperList.get(i).getSeatRow() - seatRowArray.get(0));

            } else if (busSeatUpperList.get(i).getSeatLength() == 2 && busSeatUpperList.get(i).getSeatWidth() == 1) {
                colSpec = GridLayout.spec(busSeatUpperList.get(i).getColumn() - seatColArray.get(0));
                rowSpec = GridLayout.spec(busSeatUpperList.get(i).getSeatRow() - seatRowArray.get(0));

            } else if (busSeatUpperList.get(i).getSeatLength() == 1 && busSeatUpperList.get(i).getSeatWidth() == 2) {
                colSpec = GridLayout.spec(busSeatUpperList.get(i).getColumn() - seatColArray.get(0));
                rowSpec = GridLayout.spec(busSeatUpperList.get(i).getSeatRow() - seatRowArray.get(0));

            }

            if (colSpec != null && rowSpec != null) {
                GridLayout.LayoutParams layPosition = new GridLayout.LayoutParams(colSpec, rowSpec);
                glBusSeat.addView(seatView, layPosition);
//                glBusSeat.setForegroundGravity(Gravity.CENTER);
            }
        }
    }

    private void showLower() {
        glBusSeat.removeAllViews();


        btnSeatLower.setBackgroundResource(R.drawable.bg_button_seats_selected);
        btnSeatUpper.setBackgroundResource(R.drawable.bg_button_seats_released);

//        btnSeatUpper.setEnabled(true);
//        btnSeatLower.setEnabled(false);

        for (int i = 0; i < busSeatLowerList.size(); i++) {
            LayoutInflater layoutInflater = LayoutInflater.from(getApplicationContext());
            View seatView = layoutInflater.inflate(R.layout.adapter_bus_seat, null);

            ImageButton iBtnBusSeat = (ImageButton) seatView.findViewById(R.id.iBtnBusSeat);
            iBtnBusSeat.setId(i);
            iBtnBusSeat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (busSeatLowerList.get(view.getId()).getIsAvailable().equals("true") || busSeatLowerList.get(view.getId()).getIsAvailable().equals("True")) {
                        getBtnIdLower(view.getId(), view);
                    }
                }
            });

            if (busSeatLowerList.get(i).getSeatLength() == 1 && busSeatLowerList.get(i).getSeatWidth() == 1) {
                if (!busSeatLowerList.get(i).getIsAvailable().equals("true") && !busSeatLowerList.get(i).getIsAvailable().equals("True")) {
                    iBtnBusSeat.setImageResource(R.drawable.ic_seat_booked);
                } else {
                    if (busSeatLowerList.get(i).getIsLadiesSeat().equals("true") || busSeatLowerList.get(i).getIsLadiesSeat().equals("True")) {
                        iBtnBusSeat.setImageResource(R.drawable.ic_seat_ladies);
                    } else {
                        iBtnBusSeat.setImageResource(R.drawable.ic_seat);
                    }
                }


            } else if (busSeatLowerList.get(i).getSeatLength() == 2 && busSeatLowerList.get(i).getSeatWidth() == 1) {
                if (!busSeatLowerList.get(i).getIsAvailable().equals("true") && !busSeatLowerList.get(i).getIsAvailable().equals("True")) {
                    iBtnBusSeat.setImageResource(R.drawable.ic_seat_sleeper_booked);
                } else {
                    if (busSeatLowerList.get(i).getIsLadiesSeat().equals("true") || busSeatLowerList.get(i).getIsLadiesSeat().equals("True")) {
                        iBtnBusSeat.setImageResource(R.drawable.ic_seat_sleeper_ladies);
                    } else {
                        iBtnBusSeat.setImageResource(R.drawable.ic_seat_sleeper);
                    }
                }
            } else if (busSeatLowerList.get(i).getSeatLength() == 1 && busSeatLowerList.get(i).getSeatWidth() == 2) {
                if (!busSeatLowerList.get(i).getIsAvailable().equals("true") && !busSeatLowerList.get(i).getIsAvailable().equals("True")) {
                    iBtnBusSeat.setImageResource(R.drawable.ic_seat_sleeper_horizontal_booked);
                } else {
                    if (busSeatLowerList.get(i).getIsLadiesSeat().equals("true") || busSeatLowerList.get(i).getIsLadiesSeat().equals("True")) {
                        iBtnBusSeat.setImageResource(R.drawable.ic_seat_sleeper_horizontal_ladies);
                    } else {
                        iBtnBusSeat.setImageResource(R.drawable.ic_seat_sleeper_horizontal);
                    }
                }
            }
            TextView tvBusSeats = (TextView) seatView.findViewById(R.id.tvBusSeats);
            tvBusSeats.setText(busSeatLowerList.get(i).getSeatNo());

            GridLayout.Spec colSpec = null, rowSpec = null;
            if (busSeatLowerList.get(i).getSeatLength() == 1 && busSeatLowerList.get(i).getSeatWidth() == 1) {
                colSpec = GridLayout.spec(busSeatLowerList.get(i).getColumn() - seatColArray.get(0));
                rowSpec = GridLayout.spec(busSeatLowerList.get(i).getSeatRow() - seatRowArray.get(0));

            } else if (busSeatLowerList.get(i).getSeatLength() == 2 && busSeatLowerList.get(i).getSeatWidth() == 1) {
                colSpec = GridLayout.spec(busSeatLowerList.get(i).getColumn() - seatColArray.get(0));
                rowSpec = GridLayout.spec(busSeatLowerList.get(i).getSeatRow() - seatRowArray.get(0));

            } else if (busSeatLowerList.get(i).getSeatLength() == 1 && busSeatLowerList.get(i).getSeatWidth() == 2) {
                colSpec = GridLayout.spec(busSeatLowerList.get(i).getColumn() - seatColArray.get(0));
                rowSpec = GridLayout.spec(busSeatLowerList.get(i).getSeatRow() - seatRowArray.get(0));

            }

            if (colSpec != null && rowSpec != null) {
                GridLayout.LayoutParams layPosition = new GridLayout.LayoutParams(colSpec, rowSpec);

                glBusSeat.addView(seatView, layPosition);
//                glBusSeat.setForegroundGravity(Gravity.CENTER);
            }
        }

    }

    private void showLowerChecked() {
        glBusSeat.removeAllViews();

        btnSeatLower.setBackgroundResource(R.drawable.bg_button_seats_selected);
        btnSeatUpper.setBackgroundResource(R.drawable.bg_button_seats_released);

        for (int i = 0; i < busSeatLowerList.size(); i++) {
            LayoutInflater layoutInflater = LayoutInflater.from(getApplicationContext());
            View seatView = layoutInflater.inflate(R.layout.adapter_bus_seat, null);

            ImageButton iBtnBusSeat = (ImageButton) seatView.findViewById(R.id.iBtnBusSeat);
            iBtnBusSeat.setId(i);
            iBtnBusSeat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (busSeatLowerList.get(view.getId()).getIsAvailable().equals("true") || busSeatLowerList.get(view.getId()).getIsAvailable().equals("True")) {
                        getBtnIdLower(view.getId(), view);
                    }
                }
            });

            if (busSeatLowerList.get(i).getSeatLength() == 1 && busSeatLowerList.get(i).getSeatWidth() == 1) {
                if (!busSeatLowerList.get(i).getIsAvailable().equals("true") && !busSeatLowerList.get(i).getIsAvailable().equals("True")) {
                    iBtnBusSeat.setImageResource(R.drawable.ic_seat_booked);
                } else {
                    if (busSeatLowerList.get(i).getIsLadiesSeat().equals("true") || busSeatLowerList.get(i).getIsLadiesSeat().equals("True")) {
                        if (seatSelectedMap.containsKey(busSeatLowerList.get(i).getSeatNo())) {
                            iBtnBusSeat.setImageResource(R.drawable.ic_seat_selected_ladies);
                        } else {
                            iBtnBusSeat.setImageResource(R.drawable.ic_seat_ladies);
                        }

                    } else {
                        if (seatSelectedMap.containsKey(busSeatLowerList.get(i).getSeatNo())) {
                            iBtnBusSeat.setImageResource(R.drawable.ic_seat_selected);
                        } else {
                            iBtnBusSeat.setImageResource(R.drawable.ic_seat);
                        }

                    }
                }
            } else if (busSeatLowerList.get(i).getSeatLength() == 2 && busSeatLowerList.get(i).getSeatWidth() == 1) {
                if (!busSeatLowerList.get(i).getIsAvailable().equals("true") && !busSeatLowerList.get(i).getIsAvailable().equals("True")) {
                    iBtnBusSeat.setImageResource(R.drawable.ic_seat_sleeper_booked);
                } else {
                    if (busSeatLowerList.get(i).getIsLadiesSeat().equals("true") || busSeatLowerList.get(i).getIsLadiesSeat().equals("True")) {
                        if (seatSelectedMap.containsKey(busSeatLowerList.get(i).getSeatNo())) {
                            iBtnBusSeat.setImageResource(R.drawable.ic_seat_sleeper_ladies_selected);
                        } else {
                            iBtnBusSeat.setImageResource(R.drawable.ic_seat_sleeper_ladies);
                        }
                    } else {
                        if (seatSelectedMap.containsKey(busSeatLowerList.get(i).getSeatNo())) {
                            iBtnBusSeat.setImageResource(R.drawable.ic_seat_sleeper_selected);
                        } else {
                            iBtnBusSeat.setImageResource(R.drawable.ic_seat_sleeper);

                        }
                    }
                }
            } else if (busSeatLowerList.get(i).getSeatLength() == 1 && busSeatLowerList.get(i).getSeatWidth() == 2) {
                if (!busSeatLowerList.get(i).getIsAvailable().equals("true") && !busSeatLowerList.get(i).getIsAvailable().equals("True")) {
                    iBtnBusSeat.setImageResource(R.drawable.ic_seat_sleeper_horizontal_booked);
                } else {
                    if (busSeatLowerList.get(i).getIsLadiesSeat().equals("true") || busSeatLowerList.get(i).getIsLadiesSeat().equals("True")) {
                        if (seatSelectedMap.containsKey(busSeatLowerList.get(i).getSeatNo())) {
                            iBtnBusSeat.setImageResource(R.drawable.ic_seat_sleeper_horizontal_ladies_selected);
                        } else {
                            iBtnBusSeat.setImageResource(R.drawable.ic_seat_sleeper_horizontal_ladies);
                        }
                    } else {
                        if (seatSelectedMap.containsKey(busSeatLowerList.get(i).getSeatNo())) {
                            iBtnBusSeat.setImageResource(R.drawable.ic_seat_sleeper_horizontal_selected);
                        } else {
                            iBtnBusSeat.setImageResource(R.drawable.ic_seat_sleeper_horizontal);
                        }

                    }
                }
            }
            TextView tvBusSeats = (TextView) seatView.findViewById(R.id.tvBusSeats);
            tvBusSeats.setText(busSeatLowerList.get(i).getSeatNo());

            GridLayout.Spec colSpec = null, rowSpec = null;
            if (busSeatLowerList.get(i).getSeatLength() == 1 && busSeatLowerList.get(i).getSeatWidth() == 1) {
                colSpec = GridLayout.spec(busSeatLowerList.get(i).getColumn() - seatColArray.get(0));
                rowSpec = GridLayout.spec(busSeatLowerList.get(i).getSeatRow() - seatRowArray.get(0));

            } else if (busSeatLowerList.get(i).getSeatLength() == 2 && busSeatLowerList.get(i).getSeatWidth() == 1) {
                colSpec = GridLayout.spec(busSeatLowerList.get(i).getColumn() - seatColArray.get(0));
                rowSpec = GridLayout.spec(busSeatLowerList.get(i).getSeatRow() - seatRowArray.get(0));

            } else if (busSeatLowerList.get(i).getSeatLength() == 1 && busSeatLowerList.get(i).getSeatWidth() == 2) {
                colSpec = GridLayout.spec(busSeatLowerList.get(i).getColumn() - seatColArray.get(0));
                rowSpec = GridLayout.spec(busSeatLowerList.get(i).getSeatRow() - seatRowArray.get(0));

            }

            if (colSpec != null && rowSpec != null) {
                GridLayout.LayoutParams layPosition = new GridLayout.LayoutParams(colSpec, rowSpec);
                glBusSeat.addView(seatView, layPosition);
//                glBusSeat.setForegroundGravity(Gravity.CENTER);
            }
        }

    }

    private void showUpperChecked() {

        btnSeatLower.setBackgroundResource(R.drawable.bg_button_seats_released);
        btnSeatUpper.setBackgroundResource(R.drawable.bg_button_seats_selected);

        glBusSeat.removeAllViews();

        for (int i = 0; i < busSeatUpperList.size(); i++) {
            LayoutInflater layoutInflater = LayoutInflater.from(getApplicationContext());
            View seatView = layoutInflater.inflate(R.layout.adapter_bus_seat, null);
            ImageButton iBtnBusSeat = (ImageButton) seatView.findViewById(R.id.iBtnBusSeat);
            iBtnBusSeat.setId(i);

            iBtnBusSeat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (busSeatUpperList.get(view.getId()).getIsAvailable().equals("true") || busSeatUpperList.get(view.getId()).getIsAvailable().equals("True")) {
                        getBtnIdUpper(view.getId(), view);
                    }

                }
            });

            if (busSeatUpperList.get(i).getSeatLength() == 1 && busSeatUpperList.get(i).getSeatWidth() == 1) {
                if (!busSeatUpperList.get(i).getIsAvailable().equals("true") && !busSeatUpperList.get(i).getIsAvailable().equals("True")) {
                    iBtnBusSeat.setImageResource(R.drawable.ic_seat_booked);
                } else {
                    if (busSeatUpperList.get(i).getIsLadiesSeat().equals("true") || busSeatUpperList.get(i).getIsLadiesSeat().equals("True")) {
                        if (seatSelectedMap.containsKey(busSeatUpperList.get(i).getSeatNo())) {
                            iBtnBusSeat.setImageResource(R.drawable.ic_seat_selected_ladies);
                        } else {
                            iBtnBusSeat.setImageResource(R.drawable.ic_seat_ladies);
                        }
                    } else {
                        if (seatSelectedMap.containsKey(busSeatUpperList.get(i).getSeatNo())) {
                            iBtnBusSeat.setImageResource(R.drawable.ic_seat_selected);
                        } else {
                            iBtnBusSeat.setImageResource(R.drawable.ic_seat);
                        }

                    }
                }


            } else if (busSeatUpperList.get(i).getSeatLength() == 2 && busSeatUpperList.get(i).getSeatWidth() == 1) {
                if (!busSeatUpperList.get(i).getIsAvailable().equals("true") && !busSeatUpperList.get(i).getIsAvailable().equals("True")) {
                    iBtnBusSeat.setImageResource(R.drawable.ic_seat_sleeper_booked);
                } else {
                    if (busSeatUpperList.get(i).getIsLadiesSeat().equals("true") || busSeatUpperList.get(i).getIsLadiesSeat().equals("True")) {
                        if (seatSelectedMap.containsKey(busSeatUpperList.get(i).getSeatNo())) {
                            iBtnBusSeat.setImageResource(R.drawable.ic_seat_sleeper_ladies_selected);
                        } else {
                            iBtnBusSeat.setImageResource(R.drawable.ic_seat_sleeper_ladies);

                        }

                    } else {
                        if (seatSelectedMap.containsKey(busSeatUpperList.get(i).getSeatNo())) {
                            iBtnBusSeat.setImageResource(R.drawable.ic_seat_sleeper_selected);
                        } else {
                            iBtnBusSeat.setImageResource(R.drawable.ic_seat_sleeper);

                        }

                    }
                }

            } else if (busSeatUpperList.get(i).getSeatLength() == 1 && busSeatUpperList.get(i).getSeatWidth() == 2) {
                if (!busSeatUpperList.get(i).getIsAvailable().equals("true") && !busSeatUpperList.get(i).getIsAvailable().equals("True")) {
                    iBtnBusSeat.setImageResource(R.drawable.ic_seat_sleeper_horizontal_booked);
                } else {
                    if (busSeatUpperList.get(i).getIsLadiesSeat().equals("true") || busSeatUpperList.get(i).getIsLadiesSeat().equals("True")) {
                        if (seatSelectedMap.containsKey(busSeatUpperList.get(i).getSeatNo())) {
                            iBtnBusSeat.setImageResource(R.drawable.ic_seat_sleeper_horizontal_ladies_selected);
                        } else {
                            iBtnBusSeat.setImageResource(R.drawable.ic_seat_sleeper_horizontal_ladies);
                        }

                    } else {
                        if (seatSelectedMap.containsKey(busSeatUpperList.get(i).getSeatNo())) {
                            iBtnBusSeat.setImageResource(R.drawable.ic_seat_sleeper_horizontal_selected);
                        } else {
                            iBtnBusSeat.setImageResource(R.drawable.ic_seat_sleeper_horizontal);
                        }

                    }
                }
            }
            TextView tvBusSeats = (TextView) seatView.findViewById(R.id.tvBusSeats);
            tvBusSeats.setText(busSeatUpperList.get(i).getSeatNo());
            GridLayout.Spec colSpec = null, rowSpec = null;

            if (busSeatUpperList.get(i).getSeatLength() == 1 && busSeatUpperList.get(i).getSeatWidth() == 1) {
                colSpec = GridLayout.spec(busSeatUpperList.get(i).getColumn() - seatColArray.get(0));
                rowSpec = GridLayout.spec(busSeatUpperList.get(i).getSeatRow() - seatRowArray.get(0));

            } else if (busSeatUpperList.get(i).getSeatLength() == 2 && busSeatUpperList.get(i).getSeatWidth() == 1) {
                colSpec = GridLayout.spec(busSeatUpperList.get(i).getColumn() - seatColArray.get(0));
                rowSpec = GridLayout.spec(busSeatUpperList.get(i).getSeatRow() - seatRowArray.get(0));

            } else if (busSeatUpperList.get(i).getSeatLength() == 1 && busSeatUpperList.get(i).getSeatWidth() == 2) {
                colSpec = GridLayout.spec(busSeatUpperList.get(i).getColumn() - seatColArray.get(0));
                rowSpec = GridLayout.spec(busSeatUpperList.get(i).getSeatRow() - seatRowArray.get(0));

            }

            if (colSpec != null && rowSpec != null) {
                GridLayout.LayoutParams layPosition = new GridLayout.LayoutParams(colSpec, rowSpec);
                glBusSeat.addView(seatView, layPosition);
//                glBusSeat.setForegroundGravity(Gravity.CENTER);
            }
        }
    }

    private void notifyChange() {
        double finalPrice = 0;
        String totalSeats = "";

        for (BusSeatModel seat : getSelectedSeatList().values()) {
            if (totalSeats.equals("")) {
                totalSeats = seat.getSeatNo();
            } else {
                totalSeats = totalSeats + ", " + seat.getSeatNo();
            }

            finalPrice = finalPrice + Double.valueOf(seat.getSeatFare());
        }

        tvBusPaymentAmount.setText(getResources().getString(R.string.rupease) + " " + String.valueOf(finalPrice));
        tvBusPaymentSeatsNo.setText(totalSeats);

    }

    private HashMap<String, BusSeatModel> getSelectedSeatList() {
        return seatSelectedMap;
    }

    private boolean checkSeat(BusSeatModel busSeatModel, View view) {
        if (busSeatModel != null) {
            if (seatSelectedMap.containsKey(busSeatModel.getSeatNo())) {
                //Remove
                seatSelectedMap.remove(busSeatModel.getSeatNo());
                if (busSeatModel.getIsLadiesSeat().equals("true") || busSeatModel.getIsLadiesSeat().equals("True")) {
                    //Ladies
                    if (busSeatModel.getSeatLength() == 1 && busSeatModel.getSeatWidth() == 1) {
                        ((ImageButton) view).setImageResource(R.drawable.ic_seat_ladies);
                    } else if (busSeatModel.getSeatLength() == 2 && busSeatModel.getSeatWidth() == 1) {
                        ((ImageButton) view).setImageResource(R.drawable.ic_seat_sleeper_ladies);
                    } else if (busSeatModel.getSeatLength() == 1 && busSeatModel.getSeatWidth() == 2) {
                        ((ImageButton) view).setImageResource(R.drawable.ic_seat_sleeper_horizontal_ladies);
                    }
                } else {
                    //Other
                    if (busSeatModel.getSeatLength() == 1 && busSeatModel.getSeatWidth() == 1) {
                        ((ImageButton) view).setImageResource(R.drawable.ic_seat);
                    } else if (busSeatModel.getSeatLength() == 2 && busSeatModel.getSeatWidth() == 1) {
                        ((ImageButton) view).setImageResource(R.drawable.ic_seat_sleeper);
                    } else if (busSeatModel.getSeatLength() == 1 && busSeatModel.getSeatWidth() == 2) {
                        ((ImageButton) view).setImageResource(R.drawable.ic_seat_sleeper_horizontal);
                    }
                }
                notifyChange();
                return false;
            } else {
                //Add
                if (seatSelectedMap.size() != 6) {
                    seatSelectedMap.put(busSeatModel.getSeatNo(), busSeatModel);
                    if (busSeatModel.getIsLadiesSeat().equals("true") || busSeatModel.getIsLadiesSeat().equals("True")) {
                        //ladies
                        if (busSeatModel.getSeatLength() == 1 && busSeatModel.getSeatWidth() == 1) {
                            ((ImageButton) view).setImageResource(R.drawable.ic_seat_selected_ladies);
                        } else if (busSeatModel.getSeatLength() == 2 && busSeatModel.getSeatWidth() == 1) {
                            ((ImageButton) view).setImageResource(R.drawable.ic_seat_sleeper_ladies_selected);
                        } else if (busSeatModel.getSeatLength() == 1 && busSeatModel.getSeatWidth() == 2) {
                            ((ImageButton) view).setImageResource(R.drawable.ic_seat_sleeper_horizontal_ladies_selected);
                        }
                    } else {
                        //Other
                        if (busSeatModel.getSeatLength() == 1 && busSeatModel.getSeatWidth() == 1) {
                            ((ImageButton) view).setImageResource(R.drawable.ic_seat_selected);
                        } else if (busSeatModel.getSeatLength() == 2 && busSeatModel.getSeatWidth() == 1) {
                            ((ImageButton) view).setImageResource(R.drawable.ic_seat_sleeper_selected);
                        } else if (busSeatModel.getSeatLength() == 1 && busSeatModel.getSeatWidth() == 2) {
                            ((ImageButton) view).setImageResource(R.drawable.ic_seat_sleeper_horizontal_selected);
                        }
                    }
                } else {
                    CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.You_can_elect_maximum_of_seats_only));
                }
                notifyChange();
                return true;
            }

        } else {
            return false;
        }
    }

    private void getBtnIdLower(int id, View view) {
        checkSeat(busSeatLowerList.get(id), view);
    }

    private void getBtnIdUpper(int id, View view) {
        checkSeat(busSeatUpperList.get(id), view);
    }

    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(BusDetailActivity.this, R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton(getResources().getString(R.string.agree), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton(getResources().getString(R.string.Dismiss), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(BusDetailActivity.this).sendBroadcast(intent);
    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }

}
