package in.MadMoveGlobal.EwireRuPay.activity;

import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.util.SparseArray;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import in.MadMoveGlobal.custom.CustomAlertDialog;
import in.MadMoveGlobal.custom.CustomSuccessDialog;
import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.metadata.AppMetadata;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.model.contract;
import in.MadMoveGlobal.util.NetworkErrorHandler;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;

public class ContactReadActivity extends AppCompatActivity {

    // List variables
    public String[] Contacts = {};
    public int[] to = {};
    public ListView myListView;

    Button save_button;
    private TextView phone;
    private String phoneNumber;
    private Cursor cursor;
    private MyAdapter ma;
    private Button select;
    private Button select1;
    private LoadingDialog loadDlg;
    private Button ok;
    private ImageView images;
    private JSONObject jsonRequest;
    private String tag_json_obj = "json_events";
    private UserModel session = UserModel.getInstance();
    private HashMap<String, String> contact = new HashMap<String, String>();
    ArrayList<contract> contracts;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_phone);
        loadDlg = new LoadingDialog(this);
        boolean ShowMusicDailog;

        //The way you are doing might create problem so i will tell you the best way you do it accordingly
        //I am closing this session of tv come to skye
        contracts = new ArrayList<>();
        getContactDetails(this.getContentResolver());
        final ListView lv = (ListView) findViewById(R.id.list);
        ma = new MyAdapter();
        lv.setAdapter(ma);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                ma.toggle(i);

            }
        });
        lv.setItemsCanFocus(false);
        lv.setTextFilterEnabled(true);
        // adding
        select = (CheckBox) findViewById(R.id.check);
        select.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                StringBuilder checkedcontacts = new StringBuilder();
                for (int i = 0; i < ma.getCount(); i++) {
                    lv.setItemChecked(i, true);
                }
//                for(int i=0;i<lv.getAdapter().getCount();i++) {
//                    // LinearLayout itemLayout = (LinearLayout) lv.getChildAt(i);
//                    View itemLayout = lv.getChildAt(i);
//                    CheckBox cb = (CheckBox) itemLayout.findViewById(R.id.tvcontactname);
//                    cb.setChecked(true);
//
//                }
            }
        });

        select1 = (Button) findViewById(R.id.btnProceed);
        select1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String, String> stringStringHashMap = new HashMap<String, String>();
                if (ma.mCheckStates.size() > 50) {
                    for (int i = 0; i < contracts.size(); i++) {
                        if (ma.mCheckStates.get(i) == true) {
                            stringStringHashMap.put(contracts.get(i).getPcName(), contracts.get(i).getPcId());
                        }

                    }
                    if (stringStringHashMap.size() != 0) {
                        referearn(stringStringHashMap);
                    }
                } else {
                    AlertDialog alert = new AlertDialog.Builder(ContactReadActivity.this).create();
                    alert.setTitle("");
                    alert.setMessage(" Choose atleast 50 contacts!!");
                    alert.setButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    alert.show();
                }
            }
        });


    }

    SparseArray<CheckBox> array = new SparseArray<CheckBox>();

    private void findAllEdittexts(ViewGroup viewGroup) {

        int count = viewGroup.getChildCount();
        for (int i = 0; i < count; i++) {
            View view = viewGroup.getChildAt(i);
            if (view instanceof ViewGroup)
                findAllEdittexts((ViewGroup) view);
            else if (view instanceof CheckBox) {
                CheckBox cbcb = (CheckBox) view;
                array.put(cbcb.getId(), cbcb);
            }
        }

    }


    private void getContactDetails(ContentResolver contentResolver) {

        Cursor phones = contentResolver.query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null,
                null, null);
        HashSet<String> mobileNoSet = new HashSet<String>();

        while (phones.moveToNext()) {
            String name = phones
                    .getString(phones
                            .getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phoneNumber = phones
                    .getString(phones
                            .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            String email = phones
                    .getString(phones
                            .getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS));
            String imagUri = phones
                    .getString(phones
                            .getColumnIndex(ContactsContract.CommonDataKinds.Photo.PHOTO_URI));
            long id = phones.getColumnIndex(ContactsContract.Contacts._ID);

            if (!mobileNoSet.contains(phoneNumber)) {
                contracts.add(new contract(phoneNumber.replaceAll("[^0-9\\+]", ""), name, false));
                mobileNoSet.add(phoneNumber);
            }
        }

    }

    class MyAdapter extends BaseAdapter implements CompoundButton.OnCheckedChangeListener {
        private SparseBooleanArray mCheckStates;
        LayoutInflater mInflater;
        TextView tv1, tv;
        CheckBox cb;

        MyAdapter() {
            mCheckStates = new SparseBooleanArray(contracts.size());
            mInflater = (LayoutInflater) ContactReadActivity.this
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return contracts.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub

            return 0;
        }

        @Override
        public View getView(final int position, View convertView,
                            ViewGroup parent) {
            // TODO Auto-generated method stub
            View vi = convertView;
            if (convertView == null)
                vi = mInflater.inflate(R.layout.contact_list, null);
            cb = (CheckBox) vi.findViewById(R.id.tvcontactname);
            cb.setText(contracts.get(position).getPcName());
            cb.setTag(position);
            cb.setChecked(mCheckStates.get(position, false));
            cb.setOnCheckedChangeListener(this);

            return vi;
        }


        public boolean isChecked(int position) {
            return mCheckStates.get(position, false);
        }

        public void setChecked(int position, boolean isChecked) {
            mCheckStates.put(position, isChecked);
        }

        public void toggle(int position) {
            setChecked(position, !isChecked(position));
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView,
                                     boolean isChecked) {
            // TODO Auto-generated method stub

            mCheckStates.put((Integer) buttonView.getTag(), isChecked);
        }

    }


    private void referearn(HashMap<String, String> stringStringHashMap) {
        loadDlg.show();
        try {
            jsonRequest = new JSONObject();
            jsonRequest.put("sessionId", session.getUserSessionId());
            JSONObject jsonObject = new JSONObject(stringStringHashMap);

            jsonRequest.put("mobileList", getJsonFromMap(stringStringHashMap));


        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {

            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_REFER_AND_EARN, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject responses) {
                    try {
                        String messageAPI = responses.getString("message");
                        String code = responses.getString("code");
                        if (code != null && code.equals("S00")) {
                            String status = responses.getString("status");
                            String message = responses.getString("message");
                            String details = responses.getString("details");
                            String sessionId = responses.getString("sessionId");
                            String amount = responses.getString("amount");
                            String info = responses.getString("info");
                            String success = responses.getString("success");
                            String valid = responses.getString("valid");
                            String transactionId = responses.getString("transactionId");
                            String response = responses.getString("response");
                            String predictList = responses.getString("predictList");
                            String totalPages = responses.getString("totalPages");


                            AlertDialog.Builder builder = new AlertDialog.Builder(ContactReadActivity.this, R.style.AppCompatAlertDialogStyleMusic);
                            LayoutInflater inflater1 = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            View viewDialog = inflater1.inflate(R.layout.dialog_earntopay, null, false);
                            builder.setView(viewDialog);
                            final AlertDialog alertDialog = builder.create();
//        seek_bar = (SeekBar) viewDialog.findViewById(R.id.seek_bar);

                            ok = (Button) viewDialog.findViewById(R.id.btnOK);

                            ok.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
//                text_shown.setText("Playing...");

                                    startActivity(new Intent(ContactReadActivity.this, MainActivity.class));
                                    finish();
                                }
                            });


                            alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                                @Override
                                public void onCancel(DialogInterface dialogInterface) {


                                }
                            });
                            alertDialog.show();


                            loadDlg.dismiss();

                        } else if (code != null && code.equals("F03")) {
                            loadDlg.dismiss();
                            showInvalidSessionDialog();
                        } else {
                            CustomToast.showMessage(getApplicationContext(), messageAPI);
                            loadDlg.dismiss();

                        }
                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception2));

                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    CustomToast.showMessage(getApplicationContext(), NetworkErrorHandler.getMessage(error, getApplicationContext()));
                    loadDlg.dismiss();


                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "12345");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    public void showSuccessDialog(String message, String Title) {
        CustomSuccessDialog builder = new CustomSuccessDialog(ContactReadActivity.this, Title, message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                Intent mainActivityIntent = new Intent(ContactReadActivity.this, MainActivity.class);
                sendRefresh();
                startActivity(mainActivityIntent);
                ContactReadActivity.this.finish();
            }
        });
        builder.show();
    }

    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(ContactReadActivity.this, R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(ContactReadActivity.this).sendBroadcast(intent);
    }

    private void sendRefresh() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "1");
        LocalBroadcastManager.getInstance(ContactReadActivity.this).sendBroadcast(intent);
    }


    // Initializing the buttons according to their ID
//        save_button = (Button) findViewById(R.id.contact_done);
//
//        // Defines listeners for the buttons
//        save_button.setOnClickListener(this);
//
//        Cursor mCursor = getContacts();
//        startManagingCursor(mCursor);
//
//        ListAdapter adapter = new SimpleCursorAdapter(
//                this,
//                android.R.layout.simple_list_item_multiple_choice,
//                mCursor,
//                Contacts = new String[] { ContactsContract.Contacts.DISPLAY_NAME },
//                to = new int[] { android.R.id.text1 });
//
//        setListAdapter(adapter);
//        myListView = getListView();
//        myListView.setItemsCanFocus(false);
//        myListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
//
//    }
//
//    private Cursor getContacts() {
//        // Run query
//        Uri uri = ContactsContract.Contacts.CONTENT_URI;
//        String[] projection = new String[] { ContactsContract.Contacts._ID,
//                ContactsContract.Contacts.DISPLAY_NAME };
//        String selection = ContactsContract.Contacts.HAS_PHONE_NUMBER + " = '"
//                + ("1") + "'";
//        String[] selectionArgs = null;
//        String sortOrder = ContactsContract.Contacts.DISPLAY_NAME
//                + " COLLATE LOCALIZED ASC";
//
//        return managedQuery(uri, projection, selection, selectionArgs,
//                sortOrder);
//    }
//
//    public void onClick(View src) {
//        Intent i;
//        switch (src.getId()) {
//        case R.id.contact_done:
//
//            SparseBooleanArray selectedPositions = myListView
//                    .getCheckedItemPositions();
//            SparseBooleanArray checkedPositions = myListView
//                    .getCheckedItemPositions();
//            if (checkedPositions != null) {
//                for (int k = 0; k < checkedPositions.size(); k++) {
//                     if (checkedPositions.valueAt(k)) {
//                          String name =
//                                 ((Cursor)myListView.getAdapter().getItem(k)).getString(1);
//
//                        }
//                }
//            }
//
//            break;
//        }
//
//    }


    private JSONObject getJsonFromMap(Map<String, String> map) throws JSONException {
        JSONObject jsonData = new JSONObject();
        for (String key : map.keySet()) {
            Object value = map.get(key);
            if (value instanceof Map<?, ?>) {
                value = getJsonFromMap((Map<String, String>) value);
            }
            jsonData.put(key, value);
        }
        return jsonData;
    }
}