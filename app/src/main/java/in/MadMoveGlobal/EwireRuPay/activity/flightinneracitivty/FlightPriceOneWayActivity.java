package in.MadMoveGlobal.EwireRuPay.activity.flightinneracitivty;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.MadMoveGlobal.adapter.FlightDetailAdapter;
import in.MadMoveGlobal.custom.CustomAlertDialog;
import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.custom.NonScrollListView;
import in.MadMoveGlobal.custom.ResultIPC;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.metadata.AppMetadata;
import in.MadMoveGlobal.model.DomesticFlightModel;
import in.MadMoveGlobal.model.FlightListModel;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.util.NetworkErrorHandler;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;


public class FlightPriceOneWayActivity extends AppCompatActivity {
    private FlightListModel flightListModel;
    private String sourceCode, destinationCode, dateOfJourney;
    private int adultNo, childNo, infantNo;
    private String flightClass;

    private Toolbar toolbar;
    private ImageButton ivBackBtn;

    private JSONObject segmentObject;

    private NonScrollListView lvListFlightDetail;
    private Button btnBookFlight;

    private TextView tvflightDetailsTotalFare, tvflightDetailsTaxFees, tvflightDetailsBaseFare, tvflightDetailsPassengers, tvflightDetailsFareRule;
    private TextView tvFlightTotalTime;


    private LoadingDialog loadDlg;
    private JSONObject jsonRequest;

    private JSONObject jsonReceived;
    private JSONObject jsonFareReceived;

    //Volley Tag
    private String tag_json_obj = "json_flight";

    private UserModel session = UserModel.getInstance();
    private JSONObject travelJson;

    private String jsonToSend = "";
    private String jsonFareToSend = "";
    private boolean intentional;
    private NonScrollListView lvListFlightRoundTrip;
    private String dateOfReturn;
    private int citylist;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flight_price_oneway);
        loadDlg = new LoadingDialog(FlightPriceOneWayActivity.this);
        travelJson = new JSONObject();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbars);
        ImageButton ivBackBtns = (ImageButton) findViewById(R.id.ivBackBtns);
        ivBackBtns.setVisibility(View.VISIBLE);
        setSupportActionBar(toolbar);
        ivBackBtns.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        citylist = getIntent().getIntExtra("citylist", 0);
        lvListFlightDetail = (NonScrollListView) findViewById(R.id.lvListFlightDetail);
        lvListFlightRoundTrip = (NonScrollListView) findViewById(R.id.lvListFlightRoundTrip);
        tvflightDetailsTotalFare = (TextView) findViewById(R.id.tvflightDetailsTotalFare);
        tvflightDetailsTaxFees = (TextView) findViewById(R.id.tvflightDetailsTaxFees);
        tvflightDetailsBaseFare = (TextView) findViewById(R.id.tvflightDetailsBaseFare);
        tvflightDetailsPassengers = (TextView) findViewById(R.id.tvflightDetailsPassengers);
        tvflightDetailsFareRule = (TextView) findViewById(R.id.tvflightDetailsFareRule);
        tvFlightTotalTime = (TextView) findViewById(R.id.tvFlightTotalTime);
        btnBookFlight = (Button) findViewById(R.id.btnBookFlight);
        TextView conenfee = (TextView) findViewById(R.id.conenfee);

        flightListModel = (FlightListModel) getIntent().getParcelableExtra("FlightArray");

        try {
            segmentObject = new JSONObject(flightListModel.getFlightJsonString());
            Log.i("val", segmentObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }


        sourceCode = getIntent().getStringExtra("sourceCode");
        destinationCode = getIntent().getStringExtra("destinationCode");
        dateOfJourney = getIntent().getStringExtra("dateOfJourney");
        flightClass = getIntent().getStringExtra("flightClass");
        jsonToSend = getIntent().getStringExtra("jsonToSend");
        try {
            dateOfReturn = getIntent().getStringExtra("dateOfReturn");
        } catch (NullPointerException e) {

        }
        jsonFareToSend = getIntent().getStringExtra("jsonFareToSend");

        adultNo = getIntent().getIntExtra("adultNo", 0);
        childNo = getIntent().getIntExtra("childNo", 0);
        infantNo = getIntent().getIntExtra("infantNo", 0);
        intentional = getIntent().getBooleanExtra("intentional", false);

        ArrayList<DomesticFlightModel> resultIPC = ResultIPC.get().getaddOnsFlight(citylist);


        LayoutInflater layoutInflater = LayoutInflater.from(getApplicationContext());
        View dateView = layoutInflater.inflate(R.layout.header_flight_detail, null);
        TextView tvHeaderBusListDate = (TextView) dateView.findViewById(R.id.tvHeaderBusListDate);
        ImageView ivHeaderListDate = (ImageView) dateView.findViewById(R.id.ivHeaderListDate);
        ImageView ivHeaderFlight = (ImageView) dateView.findViewById(R.id.ivHeaderFlight);
        TextView tvHeaderFlightName = (TextView) dateView.findViewById(R.id.tvHeaderFlightName);
        ivHeaderListDate.setImageResource(R.drawable.ic_no_flight);
        tvHeaderBusListDate.setText(sourceCode + " " + getResources().getString(R.string.arrow) + " " + destinationCode + dateOfJourney);
        tvHeaderFlightName.setText(flightListModel.getFlightListArray().get(0).getFlightName());
        ivHeaderFlight.setImageResource(R.drawable.ic_no_flight);
        lvListFlightDetail.addHeaderView(dateView);

        lvListFlightRoundTrip.setVisibility(View.VISIBLE);
        try {
            FlightDetailAdapter flightDetailAdapter1 = new FlightDetailAdapter(getApplicationContext(), flightListModel.getFlightListRoundArray(), resultIPC);
            lvListFlightRoundTrip.setAdapter(flightDetailAdapter1);
        } catch (NullPointerException e) {

        }
        FlightDetailAdapter flightDetailAdapter = new FlightDetailAdapter(getApplicationContext(), flightListModel.getFlightListArray(),resultIPC);
        lvListFlightDetail.setAdapter(flightDetailAdapter);


//        tvflightDetailsTaxFees.setText(getResources().getString(R.string.rupease) + " " + flightListModel.getFlightTaxFare() + "");
//        tvflightDetailsBaseFare.setText(getResources().getString(R.string.rupease) + " " + flightListModel.getFlightActualBaseFare() + "");

        tvflightDetailsFareRule.setText(flightListModel.getFlightListArray().get(0).getFlightRule());

        tvFlightTotalTime.setText("");
        Integer totalPassanger = adultNo + childNo + infantNo;
        tvflightDetailsTotalFare.setText(getResources().getString(R.string.rupease) + " " + flightListModel.getFlightNetFare());

        //ALL THREE
        if (adultNo != 0 && childNo != 0 && infantNo != 0) {
            tvflightDetailsPassengers.setText(adultNo + " Adults, " + childNo + " Children, " + infantNo + " Infant");
        }
        //ADULT AND CHILD
        else if (adultNo != 0 && childNo != 0 && infantNo == 0) {
            tvflightDetailsPassengers.setText(adultNo + " Adults, " + childNo + " Children");
        }
        //ADULT AND INFANT
        else if (adultNo != 0 && infantNo != 0 && childNo == 0) {
            tvflightDetailsPassengers.setText(adultNo + " Adults, " + infantNo + " Infant");
        }
        //CHILD AND INFANT
        else if (adultNo == 0 && childNo != 0 && infantNo != 0) {
            tvflightDetailsPassengers.setText(adultNo + " Children, " + infantNo + " Infant");
        }

        //ONLY ADULT
        else if (adultNo != 0 && childNo == 0 && infantNo == 0) {
            tvflightDetailsPassengers.setText(adultNo + " Adults ");
        }
        //ONLY INFANT
        else if (adultNo == 0 && childNo == 0 && infantNo != 0) {
            tvflightDetailsPassengers.setText(adultNo + " Adults, " + infantNo + " Infant");
        }
        //ONLY CHILD
        else if (adultNo == 0 && infantNo == 0 && childNo != 0) {
            tvflightDetailsPassengers.setText(childNo + " Children");
        } else {
            tvflightDetailsPassengers.setText("");
        }

        btnBookFlight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptBook();
            }
        });


    }


    public void attemptBook() {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("origin", sourceCode);
            jsonRequest.put("destination", destinationCode);
            jsonRequest.put("beginDate", dateOfJourney);
            if (intentional) {
                jsonRequest.put("tripType", "RoundTrip");
            } else {
                jsonRequest.put("tripType", "OneWay");
            }
            jsonRequest.put("cabin", flightClass);
            jsonRequest.put("adults", adultNo);
            jsonRequest.put("childs", childNo);
            jsonRequest.put("infants", infantNo);
            jsonRequest.put("traceId", "AYTM00011111111110002");
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

//        if (jsonRequest != null) {
//
//
//            Log.i("URL LOGIN", ApiUrl.URL_FLIGHT_BOOK_ONEWAY);
//            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_FLIGHT_BOOK_ONEWAY, jsonRequest, new Response.Listener<JSONObject>() {
//                @Override
//                public void onResponse(JSONObject response) {
//                    try {
//                        Log.i("LOgin Resonse", response.toString());
//                        String code = response.getString("code");
//
//                        if (code != null && code.equals("S00")) {
//                            loadDlg.dismiss();
//                            if (!flightListModel.getContingFlight().equalsIgnoreCase("{}")) {
        fetchLatestcontingPrice(flightListModel.getContingFlight());
//                            } else {
//                                fetchLatestPrice();
//                            }

//                        } else if (code != null && code.equals("F03")) {
//                            loadDlg.dismiss();
//                            showInvalidSessionDialog();
//                        } else {
//                            String message = response.getString("message");
//                            loadDlg.dismiss();
//                            CustomToast.showMessage(getApplicationContext(), message);
//
//                        }
//                    } catch (JSONException e) {
//                        loadDlg.dismiss();
//                        CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception2));
//                        e.printStackTrace();
//                    }
//                }
//            }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    loadDlg.dismiss();
//                    CustomToast.showMessage(FlightPriceOneWayActivity.this, NetworkErrorHandler.getMessage(error, FlightPriceOneWayActivity.this));
//                    error.printStackTrace();
//                }
//            }) {
//                @Override
//                public Map<String, String> getHeaders() throws AuthFailureError {
//                    HashMap<String, String> map = new HashMap<>();
//                    map.put("hash", "23704F92F9C89AC418079642E45C9EBE351F3F280395C084D78BE8461F7683B2");
//                    Log.i("Header", map.get("hash"));
//                    return map;
//                }
//
//            };
//
//            int socketTimeout = 600000;
//            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//            postReq.setRetryPolicy(policy);
//            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
//        }
    }


    public void fetchLatestPrice() {
        loadDlg.show();


        try {
            jsonReceived = new JSONObject(flightListModel.getFlightListArray().get(0).getJsrequestsegment());
            jsonFareReceived = new JSONObject(flightListModel.getFlightListArray().get(0).getJsonFareString());
            jsonReceived.remove("cabinClasses");
            jsonReceived.remove("ssrDetails");
            jsonReceived.remove("aircraftCode");
            jsonReceived.remove("aircraftType");
            jsonReceived.remove("availableSeat");
            jsonReceived.remove("fareBasisCode");
            jsonReceived.remove("flightDesignator");
            jsonReceived.remove("flightDetailRefKey");
            jsonReceived.remove("group");

            jsonReceived.remove("remarks");
            jsonReceived.remove("providerCode");
            jsonReceived.remove("numberOfStops");

            jsonReceived.put("sessionId", session.getUserSessionId());

            jsonReceived.put("engineID", segmentObject.getString("engineID"));
            jsonReceived.put("basicFare", segmentObject.getJSONArray("fare").getJSONObject(0).getDouble("basicFare"));
            jsonReceived.put("exchangeRate", segmentObject.getJSONArray("fare").getJSONObject(0).getDouble("exchangeRate"));
            jsonReceived.put("baseTransactionAmount", segmentObject.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getDouble("baseTransactionAmount"));
            jsonReceived.put("cancelPenalty", segmentObject.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getDouble("cancelPenalty"));
            jsonReceived.put("changePenalty", segmentObject.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getDouble("changePenalty"));
            jsonReceived.put("chargeCode", "");
            jsonReceived.put("chargeType", "FarePrice");
            jsonReceived.put("markUP", segmentObject.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getInt("markUP"));
            jsonReceived.put("paxType", segmentObject.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getString("paxType"));
            jsonReceived.put("refundable", segmentObject.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getBoolean("refundable"));

            jsonReceived.put("totalFare", segmentObject.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getDouble("totalFare"));
            jsonReceived.put("totalTax", segmentObject.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getDouble("totalTax"));
            jsonReceived.put("totalFareWithOutMarkUp", segmentObject.getJSONArray("fare").getJSONObject(0).getDouble("totalFareWithOutMarkUp"));
            jsonReceived.put("totalTaxWithOutMarkUp", segmentObject.getJSONArray("fare").getJSONObject(0).getDouble("totalTaxWithOutMarkUp"));
            jsonReceived.put("isCache", segmentObject.getBoolean("isCache"));

            jsonReceived.put("isHoldBooking", segmentObject.getBoolean("isHoldBooking"));
            jsonReceived.put("isInternational", segmentObject.getBoolean("isInternational"));
            jsonReceived.put("isRoundTrip", segmentObject.getBoolean("isRoundTrip"));

            jsonReceived.put("isSpecial", segmentObject.getBoolean("isSpecial"));
            jsonReceived.put("isSpecialId", segmentObject.getBoolean("isSpecialId"));
            jsonReceived.put("journeyIndex", segmentObject.getInt("journeyIndex"));
            jsonReceived.put("nearByAirport", segmentObject.getBoolean("nearByAirport"));
            jsonReceived.put("searchId", segmentObject.getString("searchId"));

            jsonReceived.put("traceId", "AYTM00011111111110002");
            jsonReceived.put("tripType", "OneWay");
            jsonReceived.put("transactionAmount", segmentObject.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getInt("transactionAmount"));

            jsonReceived.put("childs", childNo);
            jsonReceived.put("infants", infantNo);
            jsonReceived.put("adults", adultNo);


            jsonReceived.put("beginDate", dateOfJourney);
            jsonReceived.put("bondType", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("boundType"));
            jsonReceived.put("fareRule", segmentObject.getString("fareRule"));

            jsonReceived.put("isBaggageFare", segmentObject.getJSONArray("bonds").getJSONObject(0).getBoolean("isBaggageFare"));

            jsonReceived.put("isSSR", segmentObject.getJSONArray("bonds").getJSONObject(0).getBoolean("isSSR"));

            jsonReceived.put("itineraryKey", segmentObject.getString("itineraryKey"));
            jsonReceived.put("flightName", segmentObject.getString("engineID"));
            jsonReceived.put("amount", flightListModel.getTotalFare());
            jsonRequest.put("isConnecting", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getBoolean("isConnecting"));
            jsonReceived.put("journeyTime", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("duration"));

            String response = jsonReceived.toString().replace("\"boundTypes\":", "\"boundType\":");
            jsonReceived = new JSONObject(response);
        } catch (JSONException e) {
            e.printStackTrace();
            jsonReceived = null;
        }

        if (jsonReceived != null) {


            Log.i("URL", ApiUrl.URL_FETCH_RECHECK_PRICE);
            Log.i("jsonReceived", jsonReceived.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_FETCH_RECHECK_PRICE, jsonReceived, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.i("Price Resonse", response.toString());
                        String code = response.getString("code");

                        if (code != null && code.equals("S00")) {

                            JSONObject jsonDetails = response.getJSONObject("details");

                            JSONArray jsonJourney = jsonDetails.getJSONArray("journeys");
                            JSONObject jsonJrnyObj = jsonJourney.getJSONObject(0);
                            JSONArray jsonSegmentArray = jsonJrnyObj.getJSONArray("segments");
                            JSONArray travelDetails = new JSONArray();
                            for (int k = 0; k < jsonSegmentArray.length(); k++) {
                                JSONObject segmentObj = jsonSegmentArray.getJSONObject(k);
                                JSONArray bondArray = segmentObj.getJSONArray("bonds");

                                JSONObject bonObj = bondArray.getJSONObject(0);
                                JSONArray legArray = bonObj.getJSONArray("legs");
                                for (int j = 0; j < legArray.length(); j++) {

                                    JSONObject d = legArray.getJSONObject(j);
                                    d.remove("cabinClasses");
                                    d.remove("aircraftCod");
                                    d.remove("ssrDetails");
                                    d.remove("amount");
                                    d.remove("aircraftType");
                                    d.remove("availableSeat");
                                    d.remove("journeyTime");
                                    d.remove("boundType");
                                    d.remove("amount");
                                    d.remove("group");
                                    d.remove("sold");
                                    d.remove("arrivalTerminal");
                                    d.remove("currencyCode");
                                    d.remove("availableSeat");
                                    d.remove("isConnecting");
                                    d.remove("numberOfStops");
                                    d.remove("providerCode");
                                    d.remove("remarks");
                                    d.remove("aircraftCode");
                                    d.remove("ircraftType");
                                    d.remove("status");
                                    d.remove("capacity");
                                    d.remove("carrierCode");
                                    d.remove("departureTerminal");
                                    d.remove("fareBasisCode");
                                    d.remove("fareClassOfService");
                                    d.remove("flightDesignator");
                                    d.remove("flightDetailRefKey");
                                    d.put("journeyTime", bonObj.getString("journeyTime"));
                                    travelDetails.put(d);
                                }
                            }
                            travelJson.put("Oneway", travelDetails);
                            travelJson.put("Roundway", new JSONArray());
                            JSONObject jsonSegment = jsonSegmentArray.getJSONObject(0);
                            JSONArray fareArray = jsonSegment.getJSONArray("fare");
                            JSONObject fareObj = fareArray.getJSONObject(0);

                            Double totalPrice = fareObj.getDouble("totalFareWithOutMarkUp");
                            Double totalTax = fareObj.getDouble("totalTaxWithOutMarkUp");
                            Double basefare = fareObj.getDouble("basicFare");
                            loadDlg.dismiss();
                            showPriceDialog(String.valueOf(basefare), totalPrice + "", new JSONObject().toString(), String.valueOf(totalTax));

                        } else if (code != null && code.equals("F03")) {
                            loadDlg.dismiss();
                            showInvalidSessionDialog();
                        } else {
                            loadDlg.dismiss();
                            if (response.has("message")) {
                                String message = response.getString("message");
                                CustomToast.showMessage(FlightPriceOneWayActivity.this, message);
                            } else {
                                CustomToast.showMessage(FlightPriceOneWayActivity.this, "Error occurred");
                            }

                        }
                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception2));
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    CustomToast.showMessage(FlightPriceOneWayActivity.this, NetworkErrorHandler.getMessage(error, FlightPriceOneWayActivity.this));
                    error.printStackTrace();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "23704F92F9C89AC418079642E45C9EBE351F3F280395C084D78BE8461F7683B2");
                    Log.i("Header", map.get("hash"));
                    return map;
                }

            };

            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    public void fetchLatestcontingPrice(String jsonToSend) {
        loadDlg.show();

        try {
            jsonReceived = new JSONObject(jsonToSend);
            jsonReceived.put("sessionId", session.getUserSessionId());

        } catch (JSONException e) {
            e.printStackTrace();
            jsonReceived = null;
        }

        if (jsonReceived != null) {


            Log.i("URL", ApiUrl.URL_FETCH_RECHECK_PRICE);
            Log.i("jsonReceived", jsonReceived.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_FETCH_RECHECK_PRICE, jsonReceived, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        String code = response.getString("code");

                        if (code != null && code.equals("S00")) {

                            JSONObject jsonDetails = response.getJSONObject("details");

                            JSONArray jsonJourney = jsonDetails.getJSONArray("journeys");
                            JSONObject jsonJrnyObj = jsonJourney.getJSONObject(0);
                            JSONArray jsonSegmentArray = jsonJrnyObj.getJSONArray("segments");
                            JSONObject contingFlight = new JSONObject();

                            for (int k = 0; k < jsonSegmentArray.length(); k++) {
                                JSONObject segmentObj = jsonSegmentArray.getJSONObject(k);
                                JSONArray bondArray = segmentObj.getJSONArray("bonds");
                                if (bondArray.length() > 1) {
                                    JSONArray bounds = new JSONArray();
                                    JSONArray travelDetails = new JSONArray();
                                    JSONArray travelDetails1 = new JSONArray();
                                    for (int i = 0; i < bondArray.length(); i++) {
                                        JSONObject bonObj = bondArray.getJSONObject(i);
                                        JSONArray legArray = bonObj.getJSONArray("legs");
                                        JSONArray legArrays = bonObj.getJSONArray("legs");
                                        JSONObject segmentConting = jsonSegmentArray.getJSONObject(k);

                                        JSONObject bondary = bondArray.getJSONObject(i);
                                        bondary.remove("specialServices");
                                        bondary.put("baggageFare", bondary.getString("baggageFare"));
                                        bondary.put("ssrFare", bondary.getString("ssrFare"));
                                        bondary.remove("cabin");

                                        JSONArray jsonArray = new JSONArray();

                                        for (int j = 0; j < legArray.length(); j++) {
                                            String s = legArray.getJSONObject(j).toString();
                                            JSONObject dlist = new JSONObject(s);
                                            dlist.remove("cabinClasses");
                                            dlist.remove("ssrDetails");
                                            dlist.remove("journeyTime");
                                            dlist.remove("cabin");
                                            dlist.put("cabin", flightClass);
                                            dlist.remove("amount");
                                            dlist.remove("group");
                                            dlist.remove("isConnecting");
                                            dlist.remove("numberOfStops");
                                            dlist.remove("providerCode");
                                            dlist.remove("remarks");
                                            dlist.remove("journeyTime");
                                            jsonArray.put(dlist);
                                        }

                                        Log.i("valueJSON", jsonArray.toString());
                                        JSONArray legsArrays = new JSONArray();
                                        for (int j = 0; i > jsonArray.length(); i++) {
                                            JSONObject jsonObject = jsonArray.getJSONObject(j);

                                            jsonObject.remove("journeyTime");
                                            legsArrays.put(jsonObject);
                                        }
                                        bondary.put("legs", jsonArray);
                                        bounds.put(bondary);

                                        for (int j = 0; j < legArrays.length(); j++) {
                                            JSONObject travel = legArrays.getJSONObject(j);
                                            travel.remove("cabinClasses");
                                            travel.remove("aircraftCod");
                                            travel.remove("ssrDetails");
                                            travel.remove("amount");
                                            travel.remove("aircraftType");
                                            travel.remove("availableSeat");
                                            travel.remove("journeyTime");
                                            travel.remove("boundType");
                                            travel.remove("amount");
                                            travel.remove("group");
                                            travel.remove("sold");
                                            travel.remove("arrivalTerminal");
                                            travel.remove("currencyCode");
                                            travel.remove("availableSeat");
                                            travel.remove("isConnecting");
                                            travel.remove("numberOfStops");
                                            travel.remove("providerCode");
                                            travel.remove("remarks");
                                            travel.remove("aircraftCode");
                                            travel.remove("ircraftType");
                                            travel.remove("status");
                                            travel.remove("capacity");
                                            travel.remove("carrierCode");
                                            travel.remove("departureTerminal");
                                            travel.remove("fareBasisCode");
                                            travel.remove("fareClassOfService");
                                            travel.remove("flightDesignator");
                                            travel.remove("flightDetailRefKey");
                                            travel.put("journeyTime", bonObj.getString("journeyTime"));
                                            if (i == 0) {
                                                travelDetails.put(travel);
                                            } else if (i == 1) {
                                                travelDetails1.put(travel);
                                            }
                                        }

                                        String s = segmentObj.getJSONArray("fares").toString();
                                        s = s.replace("\"fares\":", "\"bookFares\":");
                                        JSONObject jsonArray1 = new JSONObject(s.substring(1, s.length() - 1));
                                        contingFlight.put("fares", jsonArray1);
                                        segmentConting.remove("bondType");
                                        contingFlight.put("baggageFare", segmentConting.getString("baggageFare"));
                                        contingFlight.put("cache", segmentConting.getString("cache"));
                                        contingFlight.put("deeplink", segmentConting.getString("deeplink"));
                                        contingFlight.remove("destination");
                                        contingFlight.put("holdBooking", segmentConting.getString("holdBooking"));
                                        contingFlight.put("international", segmentConting.getString("international"));
                                        contingFlight.put("roundTrip", segmentConting.getString("roundTrip"));
                                        contingFlight.put("special", segmentConting.getString("special"));
                                        contingFlight.put("specialId", segmentConting.getString("specialId"));
                                        contingFlight.put("engineID", segmentConting.getString("engineID"));
                                        contingFlight.put("fareRule", segmentConting.getString("fareRule"));
                                        contingFlight.put("itineraryKey", segmentConting.getString("itineraryKey"));
                                        contingFlight.put("journeyIndex", segmentConting.getString("journeyIndex"));
                                        contingFlight.put("nearByAirport", segmentConting.getString("nearByAirport"));
                                        contingFlight.put("searchId", segmentConting.getString("searchId"));
                                        contingFlight.put("remark", segmentConting.getString("remark"));


                                    }
                                    travelJson.put("Oneway", travelDetails);
                                    travelJson.put("Roundway", travelDetails1);
                                    contingFlight.put("bonds", bounds);
                                    longLog(contingFlight.toString());

                                } else {
                                    JSONObject bonObj = bondArray.getJSONObject(0);
                                    JSONArray legArray = bonObj.getJSONArray("legs");
                                    JSONArray legArrays = bonObj.getJSONArray("legs");
                                    JSONObject segmentConting = jsonSegmentArray.getJSONObject(k);

                                    JSONArray bounds = new JSONArray();
                                    JSONObject bondary = bondArray.getJSONObject(0);
                                    bondary.remove("specialServices");
                                    bondary.put("baggageFare", bondary.getString("baggageFare"));
                                    bondary.put("ssrFare", bondary.getString("ssrFare"));
                                    bondary.remove("cabin");
                                    JSONArray travelDetails = new JSONArray();
                                    JSONArray jsonArray = new JSONArray();
                                    for (int j = 0; j < legArray.length(); j++) {
                                        String s = legArray.getJSONObject(j).toString();
                                        JSONObject dlist = new JSONObject(s);
                                        dlist.remove("cabinClasses");
                                        dlist.remove("ssrDetails");
                                        dlist.remove("journeyTime");
                                        dlist.remove("cabin");
                                        dlist.put("cabin", flightClass);
                                        dlist.remove("amount");
                                        dlist.remove("group");
                                        dlist.remove("isConnecting");
                                        dlist.remove("numberOfStops");
                                        dlist.remove("providerCode");
                                        dlist.remove("remarks");
                                        dlist.remove("journeyTime");
                                        jsonArray.put(dlist);
                                    }

                                    Log.i("valueJSON", jsonArray.toString());
                                    JSONArray legsArrays = new JSONArray();
                                    for (int i = 0; i > jsonArray.length(); i++) {
                                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                                        jsonObject.remove("journeyTime");
                                        legsArrays.put(jsonObject);
                                    }
                                    bondary.put("legs", jsonArray);
                                    bounds.put(bondary);

                                    for (int j = 0; j < legArrays.length(); j++) {
                                        JSONObject travel = legArrays.getJSONObject(j);
                                        travel.remove("cabinClasses");
                                        travel.remove("aircraftCod");
                                        travel.remove("ssrDetails");
                                        travel.remove("amount");
                                        travel.remove("aircraftType");
                                        travel.remove("availableSeat");
                                        travel.remove("journeyTime");
                                        travel.remove("boundType");
                                        travel.remove("amount");
                                        travel.remove("group");
                                        travel.remove("sold");
                                        travel.remove("arrivalTerminal");
                                        travel.remove("currencyCode");
                                        travel.remove("availableSeat");
                                        travel.remove("isConnecting");
                                        travel.remove("numberOfStops");
                                        travel.remove("providerCode");
                                        travel.remove("remarks");
                                        travel.remove("aircraftCode");
                                        travel.remove("ircraftType");
                                        travel.remove("status");
                                        travel.remove("capacity");
                                        travel.remove("carrierCode");
                                        travel.remove("departureTerminal");
                                        travel.remove("fareBasisCode");
                                        travel.remove("fareClassOfService");
                                        travel.remove("flightDesignator");
                                        travel.remove("flightDetailRefKey");
                                        travel.put("journeyTime", bonObj.getString("journeyTime"));
                                        travelDetails.put(travel);
                                    }

                                    travelJson.put("Oneway", travelDetails);
                                    travelJson.put("Roundway", new JSONArray());

                                    contingFlight.put("bonds", bounds);
                                    String s = segmentObj.getJSONArray("fares").toString();
                                    s = s.replace("\"fares\":", "\"bookFares\":");

                                    JSONObject jsonArray1 = new JSONObject(s.substring(1, s.length() - 1));
                                    contingFlight.put("fares", jsonArray1);
                                    segmentConting.remove("bondType");

                                    contingFlight.put("baggageFare", segmentConting.getString("baggageFare"));
                                    contingFlight.put("cache", segmentConting.getString("cache"));
                                    contingFlight.put("deeplink", segmentConting.getString("deeplink"));
                                    contingFlight.remove("destination");
                                    contingFlight.put("holdBooking", segmentConting.getString("holdBooking"));
                                    contingFlight.put("international", segmentConting.getString("international"));
                                    contingFlight.put("roundTrip", segmentConting.getString("roundTrip"));
                                    contingFlight.put("special", segmentConting.getString("special"));
                                    contingFlight.put("specialId", segmentConting.getString("specialId"));
                                    contingFlight.put("engineID", segmentConting.getString("engineID"));
                                    contingFlight.put("fareRule", segmentConting.getString("fareRule"));
                                    contingFlight.put("itineraryKey", segmentConting.getString("itineraryKey"));
                                    contingFlight.put("journeyIndex", segmentConting.getString("journeyIndex"));
                                    contingFlight.put("nearByAirport", segmentConting.getString("nearByAirport"));
                                    contingFlight.put("searchId", segmentConting.getString("searchId"));
                                    contingFlight.put("remark", segmentConting.getString("remark"));
                                    Log.i("contingflightBooking", String.valueOf(contingFlight));

                                }
                            }

                            JSONObject jsonSegment = jsonSegmentArray.getJSONObject(0);
                            JSONArray fareArray = jsonSegment.getJSONArray("fares");
                            JSONObject fareObj = fareArray.getJSONObject(0);
                            Double basefare = fareObj.getDouble("basicFare");
                            Double totalPrice = fareObj.getDouble("totalFareWithOutMarkUp");
                            Double totalTax = fareObj.getDouble("totalTaxWithOutMarkUp");
                            loadDlg.dismiss();
                            showPriceDialog(String.valueOf(basefare), totalPrice + "", contingFlight.toString(), String.valueOf(totalTax));

                        } else if (code != null && code.equals("F03")) {
                            loadDlg.dismiss();
                            showInvalidSessionDialog();
                        } else {
                            loadDlg.dismiss();
                            if (response.has("message")) {
                                String message = response.getString("message");
                                CustomToast.showMessage(FlightPriceOneWayActivity.this, message);
                            } else {
                                CustomToast.showMessage(FlightPriceOneWayActivity.this, "Error occurred");
                            }

                        }
                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception2));
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    CustomToast.showMessage(FlightPriceOneWayActivity.this, NetworkErrorHandler.getMessage(error, FlightPriceOneWayActivity.this));
                    error.printStackTrace();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "23704F92F9C89AC418079642E45C9EBE351F3F280395C084D78BE8461F7683B2");
                    Log.i("Header", map.get("hash"));
                    return map;
                }

            };

            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    public void showInvalidSessionDialog() {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(AppMetadata.getInvalidSession(), Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(AppMetadata.getInvalidSession());
        }
        CustomAlertDialog builder = new CustomAlertDialog(FlightPriceOneWayActivity.this, R.string.dialog_title2, result);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(FlightPriceOneWayActivity.this).sendBroadcast(intent);
    }


    private void showPriceDialog(String bare, final String price, final String contingFlight, final String tax) {
        int totalAmount = adultNo + childNo + infantNo;
        String value;

        if (flightListModel.getFlightNetFare() > Double.parseDouble(price)) {
            value = "Sorry! the price for your selected flight has changed from" + flightListModel.getFlightNetFare() + "to" + price;
        } else if (flightListModel.getFlightNetFare() < Double.parseDouble(price)) {
            value = "The price for your selected flight has changed from" + flightListModel.getFlightNetFare() + "to" + price;
        } else {
            value = "";
        }
        CustomOnWayFlightPriceDialog builder = new CustomOnWayFlightPriceDialog(FlightPriceOneWayActivity.this, bare + "-" + price + "-" + tax, value);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
//                Intent bookFormIntent = new Intent(getApplicationContext(), FlightBookingFormActivity.class);
//                bookFormIntent.putExtra("FlightModel", flightListModel);
//                bookFormIntent.putExtra("sourceCode", sourceCode);
//                bookFormIntent.putExtra("destinationCode", destinationCode);
//                bookFormIntent.putExtra("dateOfJourney", dateOfJourney);
//                bookFormIntent.putExtra("adultNo", adultNo);
//                bookFormIntent.putExtra("childNo", childNo);
//                bookFormIntent.putExtra("dateOfReturn", dateOfReturn);
//                bookFormIntent.putExtra("infantNo", infantNo);
//                bookFormIntent.putExtra("intentional", intentional);
//                bookFormIntent.putExtra("contingFlight", contingFlight);
//                bookFormIntent.putExtra("travel", travelJson.toString());
//                bookFormIntent.putExtra("flightClass", flightClass);
//                String priceNew = String.valueOf(Double.parseDouble(price) + Double.parseDouble("200"));
//                bookFormIntent.putExtra("latestfare", priceNew);
//                startActivity(bookFormIntent);

            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });

        builder.show();
//
    }

    public static void longLog(String str) {
        if (str.length() > 4000) {
            Log.d("travel", str.substring(0, 4000));
            longLog(str.substring(4000));
        } else
            Log.d("travel", str);
    }


}
