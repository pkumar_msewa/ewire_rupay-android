package in.MadMoveGlobal.EwireRuPay.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.crashlytics.android.Crashlytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.util.NetworkErrorHandler;
import in.MadMoveGlobal.util.SecurityUtil;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;
import io.fabric.sdk.android.Fabric;


/**
 * Created by Ksf on 3/11/2016.
 */
public class OtpDeviceVerificationActivity extends AppCompatActivity {

    private EditText etVerifyOTP;
    private Button btnVerify, btnVerifyResend;
    private TextInputLayout ilVerifyOtp;


    private LoadingDialog loadDlg;
    private String userMobileNo;
    private String otpCode = null;
    private String password = "";
    private   String cardNo, cvv, cardHolder, expiryDate;
    private String regId = "";

    private JSONObject jsonRequest;
    //Volley Tag
    private String tag_json_obj = "json_user";
    private UserModel session = UserModel.getInstance();


    SharedPreferences phonePreferences;
    public static final String PHONE = "phone";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        // TODO: Move this to where you establish a user session
        logUser();

        setContentView(R.layout.activity_verify_device);
        setStatusBarGradiant(OtpDeviceVerificationActivity.this);
        loadDlg = new LoadingDialog(OtpDeviceVerificationActivity.this);
        phonePreferences = getSharedPreferences(PHONE, Context.MODE_PRIVATE);

        userMobileNo = getIntent().getStringExtra("userMobileNo");
        otpCode = getIntent().getStringExtra("OtpCode");
        password = getIntent().getStringExtra("devPassword");
        regId = getIntent().getStringExtra("devRegId");

        btnVerifyResend = (Button) findViewById(R.id.btnVerifyResend);
        btnVerify = (Button) findViewById(R.id.btnVerify);
        etVerifyOTP = (EditText) findViewById(R.id.etVerifyOTP);
        ilVerifyOtp = (TextInputLayout) findViewById(R.id.ilVerifyOtp);


        etVerifyOTP.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (etVerifyOTP.getText().length() == 6) {
//                    etVerifyOTP.setText(otpCode);
                    attemptLoginNew();
                }
            }
        });
        if (otpCode != null && !otpCode.isEmpty()) {
            etVerifyOTP.setText(otpCode);
        }

        btnVerifyResend.setTextColor(Color.parseColor("#ffffff"));
        btnVerify.setTextColor(Color.parseColor("#ffffff"));

        btnVerifyResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadDlg.show();
                resendOTP();
            }
        });


        btnVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                loadDlg.show();
                submitForm();
            }
        });

    }


    private void submitForm() {
        if (!validateOTP()) {
            return;
        }
        attemptLoginNew();

    }


    private boolean validateOTP() {
        if (etVerifyOTP.getText().toString().trim().isEmpty()) {
            ilVerifyOtp.setError("Please Enter OTP");
            requestFocus(etVerifyOTP);
            return false;
        } else if (etVerifyOTP.getText().toString().trim().length() < 3) {
            ilVerifyOtp.setError("Enter correct OTP");
            requestFocus(etVerifyOTP);
        } else {
            ilVerifyOtp.setErrorEnabled(false);
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void resendOTP() {
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("mobileNumber", userMobileNo);
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("OTPURL", ApiUrl.URL_RESEND_OTP_DEVICE);
            Log.i("JsonRequest", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_RESEND_OTP_DEVICE, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObj) {
                    try {
                        Log.i("RESENDOTP", jsonObj.toString());
                        String code = jsonObj.getString("code");
                        String detail = jsonObj.getString("message");
                        if (code != null & code.equals("S00")) {
                            if (!etVerifyOTP.getText().toString().isEmpty()) {
                                etVerifyOTP.setText("");
                            }
                            loadDlg.dismiss();
                            Toast.makeText(OtpDeviceVerificationActivity.this, detail, Toast.LENGTH_SHORT).show();
                        } else {
                            loadDlg.dismiss();


                        }
                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception2));
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception));

                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    public void attemptLoginNew() {
        loadDlg.show();
        jsonRequest = new JSONObject();

        try {
            jsonRequest.put("username", userMobileNo);
            jsonRequest.put("password", password);
            jsonRequest.put("validate", true);
            jsonRequest.put("mobileToken", etVerifyOTP.getText().toString());
            if (!regId.isEmpty()) {
                jsonRequest.put("registrationId", regId);
            }
            if (SecurityUtil.getIMEI(getApplicationContext()) != null) {
                jsonRequest.put("ipAddress", SecurityUtil.getAndroidId(getApplicationContext()) + "-" + SecurityUtil.getIMEI(getApplicationContext()));
            } else {
                jsonRequest.put("ipAddress", SecurityUtil.getAndroidId(getApplicationContext()));
            }

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("URL LOGIN", ApiUrl.URL_LOGIN);
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_LOGIN, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.i("LOgin Resonse", response.toString());
                        String code = response.getString("code");

                        if (code != null && code.equals("S00")) {
//                            String jsonString = response.getString("response");
//                            JSONObject jsonObject = new JSONObject(jsonString);

                            //New Implementation
                            JSONObject jsonDetail = response.getJSONObject("details");
                            String userSessionId = jsonDetail.getString("sessionId");
                            boolean isImpsFixed = jsonDetail.getBoolean("isImpsFixed");
                            String impsCommissionAmt = jsonDetail.getString("impsCommissionAmt");
                            JSONObject accDetail = jsonDetail.getJSONObject("accountDetail");

                            double userBalanceD = accDetail.getDouble("balance");
                            String userBalance = String.valueOf(new DecimalFormat("#.##").format(userBalanceD));

                            long userAcNoL = accDetail.getLong("accountNumber");
                            String userAcNo = String.valueOf(userAcNoL);

                            int userPointsI = accDetail.getInt("points");
                            String userPoints = String.valueOf(userPointsI);

                            JSONObject accType = accDetail.getJSONObject("accountType");
                            String accName = accType.getString("name");
                            String accCode = accType.getString("code");

                            double accBalanceLimitD = accType.getDouble("balanceLimit");
                            double accMonthlyLimitD = accType.getDouble("monthlyLimit");
                            double accDailyLimitD = accType.getDouble("dailyLimit");

                            String accBalanceLimit = String.valueOf(accBalanceLimitD);
                            String accMonthlyLimit = String.valueOf(accMonthlyLimitD);
                            String accDailyLimit = String.valueOf(accDailyLimitD);

                            JSONObject jsonUserDetail = jsonDetail.getJSONObject("userDetail");

                            String userFName = jsonUserDetail.getString("firstName");
                            String userLName = jsonUserDetail.getString("lastName");
                            String userMobile = jsonUserDetail.getString("contactNo");
                            String userEmail = jsonUserDetail.getString("email");
                            String userEmailStatus = jsonUserDetail.getString("emailStatus");
                            boolean PCStatus = response.getBoolean("physicalIsBlock");
                            boolean VCStatus = response.getBoolean("virtualIsBlock");
                            String virtualBlock = response.getString("virtualCardBlock");
                            String physicalBlock = response.getString("physicalCardBlock");
                            String userAddress = jsonUserDetail.getString("locality");
                            String userImage = jsonUserDetail.getString("image");
                            String encodedImage = response.getString("encodedImage");
                            String images = "";
                            if (!encodedImage.equals("")) {
                                images = encodedImage;
                            } else {
                                images = userImage;
                            }

                            if (!response.isNull("cardDetails")) {
                                JSONObject cardDetails = response.getJSONObject("cardDetails");

                                cardNo = cardDetails.getString("cardNo");
                                cvv = cardDetails.getString("cvv");
                                expiryDate = cardDetails.getString("expiryDate");
                                cardHolder = cardDetails.getString("cardHolder");
                            } else {
                                cardNo="XXXXXXXXXXXXXXXX";
                                cvv="XXX";
                                expiryDate="XXXX-XX";
                                cardHolder="XXXXXXXXXXXXXX";

                            }
//                            String userGender = "";
//                            if (!jsonUserDetail.isNull("gender")) {
//                                userGender = jsonUserDetail.getString("gender");
//                            }
                            String userDob = jsonUserDetail.getString("dateOfBirth");
                            boolean hasRefer = response.getBoolean("hasRefer");
                            String loadMoneyComm = response.getString("loadMoneyComm");

                            UserModel.deleteAll(UserModel.class);
                            try {
                                //Encrypting sessionId
                                UserModel userModel = new UserModel(userSessionId, userFName, userLName, userMobile, userEmail, userEmailStatus, 1, userAcNo, accName, accCode, userBalance, accBalanceLimit, accDailyLimit, accMonthlyLimit, userAddress, images, userDob, "", false, userPoints, hasRefer,false,false,cardNo, cvv, expiryDate, cardHolder,false,"","","",false,loadMoneyComm,"",PCStatus, VCStatus, virtualBlock, physicalBlock,impsCommissionAmt,isImpsFixed);
                                userModel.save();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            session.setUserSessionId(userSessionId);
                            session.setUserFirstName(userFName);
                            session.setUserLastName(userLName);
                            session.setUserMobileNo(userMobile);
                            session.setUserEmail(userEmail);
                            session.setEmailIsActive(userEmailStatus);
                            session.setUserAcCode(accCode);
                            session.setUserAcNo(userAcNo);
                            session.setUserBalance(userBalance);
                            session.setUserAcName(accName);
                            session.setUserMonthlyLimit(accMonthlyLimit);
                            session.setUserDailyLimit(accDailyLimit);
                            session.setUserBalanceLimit(accBalanceLimit);
                            session.setUserImage(images);
                            session.setUserAddress(userAddress);
                            session.setUsercardNo(cardNo);
                            session.setUsercvv(cvv);
                            session.setUserexpiryDate(expiryDate);
                            session.setUsercardHolder(cardHolder);
//                            session.setUserGender(userGender);
                            session.setUserDob(userDob);
//                            session.setMPin(isMPin);
                            session.setHasRefer(hasRefer);
//                            session.setHasVcard(hasvertualcard);
//                            session.setHasPcard(hasphysicalCard);
                            session.setIsValid(1);
                            session.setUserPoints(userPoints);
                            session.setloadMoneyComm(loadMoneyComm);
                            session.setloadMoneyComm(loadMoneyComm);
                            session.setPCStatus(PCStatus);
                            session.setVCStatus(VCStatus);
                            session.setUserVirtualBlock(virtualBlock);
                            session.setUserPhysicalBlock(physicalBlock);
                            session.setIsImpsFixed(isImpsFixed);
                            session.setImpsCommissionAmt(impsCommissionAmt);
                            SharedPreferences.Editor editor = phonePreferences.edit();
                            editor.clear();
                            editor.putString("phone", userMobile);
                            editor.apply();

                            if (hasRefer) {

                                AlertDialog.Builder builder = new AlertDialog.Builder(OtpDeviceVerificationActivity.this, R.style.AppCompatAlertDialogStyleMusic);
                                LayoutInflater inflater1 = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                View viewDialog = inflater1.inflate(R.layout.dialog_earn_to_pay, null, false);
                                builder.setView(viewDialog);
                                final AlertDialog alertDialog = builder.create();
//        seek_bar = (SeekBar) viewDialog.findViewById(R.id.seek_bar);

                                Button ok = (Button) viewDialog.findViewById(R.id.btnOK);

                                ok.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
//                text_shown.setText("Playing...");

                                        startActivity((new Intent(OtpDeviceVerificationActivity.this, ContactReadActivity.class)));
                                        finish();
                                    }
                                });


                                alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                                    @Override
                                    public void onCancel(DialogInterface dialogInterface) {
                                        dialogInterface.dismiss();

                                    }
                                });
                                alertDialog.show();

                            } else {

                                loadDlg.dismiss();
                                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                                finish();
                            }

                        } else {
                            String message = response.getString("message");
                            loadDlg.dismiss();
                            CustomToast.showMessage(getApplicationContext(), message);

                        }
                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception2));
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    CustomToast.showMessage(getApplicationContext(), NetworkErrorHandler.getMessage(error, getApplicationContext()));
                    error.printStackTrace();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };

            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void setStatusBarGradiant(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            Drawable background = activity.getResources().getDrawable(R.drawable.bg_gradient_new);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
    }
    private void logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        Crashlytics.setUserIdentifier(session.getUserMobileNo());
        Crashlytics.setUserEmail(session.getUserEmail());
        Crashlytics.setUserName(session.getUserFirstName() + " " + session.getUserLastName());
    }
}
