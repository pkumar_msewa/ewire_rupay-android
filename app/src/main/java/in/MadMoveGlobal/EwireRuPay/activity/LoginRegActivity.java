package in.MadMoveGlobal.EwireRuPay.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import in.MadMoveGlobal.fragment.fragmentuser.LogRegFragment;
import in.MadMoveGlobal.EwireRuPay.R;

/**
 * Created by Ksf on 3/27/2016.
 */
public class LoginRegActivity extends AppCompatActivity {


    private Button btnRegisterSubmit;

    //    private TabLayout mSlidingTabLayout;
//    private ViewPager mainPager;
//    private FragmentManager fragmentManager;
//    CharSequence TitlesEnglish[] = {"Login", "Registration"};
//    int NumbOfTabs = 2;
//
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        overridePendingTransition(R.anim.right_to_left, R.anim.left_to_right);


        setContentView(R.layout.activity_login);
        setStatusBarGradiant(LoginRegActivity.this);

//        try {
//            ZohoSalesIQ.Chat.setVisibility(MbedableComponent.CHAT,true);
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        }


        LogRegFragment fragment = new LogRegFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.left_to_right, R.anim.right_to_left);
        fragmentTransaction.replace(R.id.llLRMain, fragment);
        fragmentTransaction.commit();

    }

    //
    private BroadcastReceiver mSignUpReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {


        }
    };

    @Override
    public void onBackPressed() {

        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
            overridePendingTransition(R.anim.left_to_right,R.anim.right_to_left);

        } else {
            android.support.v7.app.AlertDialog.Builder exitDialog =
                    new android.support.v7.app.AlertDialog.Builder(LoginRegActivity.this, R.style.AppCompatAlertDialogStyle);
            exitDialog.setTitle("Do you really want to Exit?");
            exitDialog.setPositiveButton("No", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                }
            });
            exitDialog.setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                    finishAffinity();
                    finish();
                }
            });
            exitDialog.show();
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void setStatusBarGradiant(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            Drawable background = activity.getResources().getDrawable(R.drawable.bg_gradient_new);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
    }


}
