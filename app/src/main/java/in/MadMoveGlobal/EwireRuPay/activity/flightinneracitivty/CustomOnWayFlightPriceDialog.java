package in.MadMoveGlobal.EwireRuPay.activity.flightinneracitivty;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import in.MadMoveGlobal.EwireRuPay.R;


/**
 * Created by Kashif-PC on 1/21/2017.
 */
public class CustomOnWayFlightPriceDialog extends AlertDialog.Builder {

    public CustomOnWayFlightPriceDialog(Context context, String fare, String value) {
        super(context);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewDialog = inflater.inflate(R.layout.dialog_custom_oneway_price, null, false);
        TextView conenfee = (TextView) viewDialog.findViewById(R.id.conenfee);
        conenfee.setText("Convenience Fee  :   200");
        TextView cadTotalFare = (TextView) viewDialog.findViewById(R.id.cadTotalFare);
        TextView cadvalues = (TextView) viewDialog.findViewById(R.id.cadvalues);
        cadvalues.setText(value);
        String[] tax = fare.split("-");
        if (tax.length > 1) {
            cadTotalFare.setText("Base Fare   : " + " " + tax[0] + "\n" + "Total Tax   : " + " " + tax[2] + "\n" + "Convenience Fee   :   200" + "\n" + "Total Fare   :" + " " + String.valueOf(Double.parseDouble(tax[1]) + Double.parseDouble("200")));
        } else {
            cadTotalFare.setText("Convenience Fee   :   200" + "\n" + "Total Fare   : " + String.valueOf(Double.parseDouble(fare) + Double.parseDouble("200")));
        }
        this.setCancelable(false);

        this.setView(viewDialog);


    }
}

