package in.MadMoveGlobal.EwireRuPay.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.util.NetworkErrorHandler;


/**
 * Created by Ksf on 3/12/2016.
 */
public class ChangePwdActivity extends AppCompatActivity {

    private EditText etCPNewPwd;
    private EditText etCPReNewPwd;
    private Button btnChangePwd, btnVerifyResend;

    private TextInputLayout ilChangePassword, ilChangeRePassword;
    private TextInputLayout ilOTP;
    private EditText etCPOTP;
    private TextView tvCPMessage;


    private String mobileNo;
    private String otpCode;

    private LoadingDialog loadDlg;

    private Toolbar tbChangePwd;

    private JSONObject jsonRequest;

    private ImageButton iBtnShowReNewPwd, iBtnShowNewPwd;

    //Volley Tag
    private String tag_json_obj = "json_user";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        setStatusBarGradiant(ChangePwdActivity.this);
        etCPNewPwd = (EditText) findViewById(R.id.etCPNewPwd);
        etCPReNewPwd = (EditText) findViewById(R.id.etCPReNewPwd);
        etCPOTP = (EditText) findViewById(R.id.etCPOTP);
        ilChangePassword = (TextInputLayout) findViewById(R.id.ilChangePassword);
        ilChangeRePassword = (TextInputLayout) findViewById(R.id.ilChangeRePassword);
        tvCPMessage = (TextView) findViewById(R.id.tvCPMessage);
        ilOTP = (TextInputLayout) findViewById(R.id.ilOTP);
        btnChangePwd = (Button) findViewById(R.id.btnChangePwd);
        tbChangePwd = (Toolbar) findViewById(R.id.tbChangePwd);
        btnVerifyResend = (Button) findViewById(R.id.btnVerifyResend);

        iBtnShowNewPwd = (ImageButton) findViewById(R.id.iBtnShowNewPwd);
        iBtnShowReNewPwd = (ImageButton) findViewById(R.id.iBtnShowReNewPwd);

        tbChangePwd.setTitle("Change Password");

        btnChangePwd.setTextColor(Color.parseColor("#ffffff"));
        btnVerifyResend.setTextColor(Color.parseColor("#ffffff"));
        mobileNo = getIntent().getStringExtra("userMobileNo");
        otpCode = getIntent().getStringExtra("OtpCode");

        btnVerifyResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadDlg.show();
                resendOTP();
            }
        });

        if (otpCode != null && !otpCode.isEmpty()) {
            etCPOTP.setText(otpCode);
        }

        tvCPMessage.setText("An OTP code has be sent to " + mobileNo + ". Please check your sms.\nIt may take few minutes receiving OTP, depending on mobile network");

        loadDlg = new LoadingDialog(ChangePwdActivity.this);

        btnChangePwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!validateOTP()) {
                    return;
                }
                if (!validatePassword()) {
                    return;
                }


                loadDlg.show();
                promoteChangePwd();
            }
        });

        etCPReNewPwd.addTextChangedListener(new MyTextWatcher(etCPReNewPwd));
        etCPNewPwd.addTextChangedListener(new MyTextWatcher(etCPNewPwd));

        iBtnShowReNewPwd.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    etCPReNewPwd.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    etCPReNewPwd.setSelection(etCPReNewPwd.length());
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    etCPReNewPwd.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    etCPReNewPwd.setSelection(etCPReNewPwd.length());
                }

                return false;
            }
        });

        iBtnShowNewPwd.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    etCPNewPwd.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    etCPNewPwd.setSelection(etCPNewPwd.length());
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    etCPNewPwd.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    etCPNewPwd.setSelection(etCPNewPwd.length());
                }

                return false;
            }
        });

    }


    private boolean validatePassword() {
        if (etCPNewPwd.getText().toString().isEmpty()) {
            ilChangePassword.setError("Please Enter Password");
            requestFocus(etCPNewPwd);
            return false;
        } else if (etCPNewPwd.getText().toString().length() < 6) {
            ilChangePassword.setError("Please Enter 6 digit Passord");
            requestFocus(etCPNewPwd);
            return false;
        } else if (etCPReNewPwd.getText().toString().isEmpty()) {
            ilChangeRePassword.setError("Please Enter Password");
            requestFocus(etCPReNewPwd);
            return false;
        } else if (etCPReNewPwd.getText().toString().length() < 6) {
            ilChangeRePassword.setError("Please Enter 6 digit Passord");
            requestFocus(etCPReNewPwd);
            return false;
        } else if (!etCPNewPwd.getText().toString().equals(etCPReNewPwd.getText().toString())) {
            ilChangeRePassword.setError("Password mismatch");
            requestFocus(etCPReNewPwd);
            return false;
        } else {
            ilChangePassword.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validatePasswordListner() {
        if (etCPNewPwd.getText().toString().trim().isEmpty() || etCPNewPwd.getText().toString().trim().length() < 6) {
            ilChangePassword.setError(getResources().getString(R.string.password_validity));
            requestFocus(etCPNewPwd);
            return false;
        } else {
            ilChangePassword.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateRePasswordListner() {
        if (etCPReNewPwd.getText().toString().trim().isEmpty() || etCPReNewPwd.getText().toString().trim().length() < 6) {
            ilChangeRePassword.setError(getResources().getString(R.string.password_validity));
            requestFocus(etCPReNewPwd);
            return false;
        } else {
            ilChangeRePassword.setErrorEnabled(false);
        }

        return true;
    }


    private boolean validateOTP() {
        if (etCPOTP.getText().toString().trim().isEmpty()) {
            ilOTP.setError("Enter OTP");
            requestFocus(etCPOTP);
            return false;
        } else if (etCPOTP.getText().toString().trim().length() < 6) {
            ilOTP.setError("Enter 6 Digit OTP");
            requestFocus(etCPOTP);
            return false;
        } else {
            ilOTP.setErrorEnabled(false);

        }

        return true;
    }


    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void resendOTP() {
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("mobileNumber", mobileNo);
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_RESEND_OTP, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObj) {
                    try {
                        String code = jsonObj.getString("code");
                        String detail = jsonObj.getString("details");
                        if (code != null & code.equals("S00")) {
                            if (!etCPOTP.getText().toString().isEmpty()) {
                                etCPOTP.setText("");
                            }
                            loadDlg.dismiss();
                            Toast.makeText(ChangePwdActivity.this, detail, Toast.LENGTH_SHORT).show();
                        } else {
                            loadDlg.dismiss();


                        }
                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(ChangePwdActivity.this, getResources().getString(R.string.server_exception2));
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    CustomToast.showMessage(ChangePwdActivity.this, getResources().getString(R.string.server_exception));

                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    private void promoteChangePwd() {
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("username", mobileNo);

            String basicAuthPassword = new String(Base64.encode(etCPNewPwd.getText().toString().getBytes(), Base64.NO_WRAP));
            String basicAuthConfirmPassword = new String(Base64.encode(etCPReNewPwd.getText().toString().getBytes(), Base64.NO_WRAP));
//
            jsonRequest.put("password", basicAuthPassword);
            jsonRequest.put("confirmPassword", basicAuthConfirmPassword);


//            jsonRequest.put("password", etCPNewPwd.getText().toString());
//            jsonRequest.put("confirmPassword", etCPReNewPwd.getText().toString());
            jsonRequest.put("key", etCPOTP.getText().toString());

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {

            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_CHANGE_PWD, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        String code = response.getString("code");
                        String message = response.getString("details");
                        if (code != null & code.equals("S00")) {
                            loadDlg.dismiss();
                            Intent loginIntent = new Intent(ChangePwdActivity.this, LoginRegActivity.class);
                            startActivity(loginIntent);
                            finish();
                        } else {
                            loadDlg.dismiss();
                        }
                        Toast.makeText(ChangePwdActivity.this, message, Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(ChangePwdActivity.this, getResources().getString(R.string.server_exception2));
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    CustomToast.showMessage(ChangePwdActivity.this, NetworkErrorHandler.getMessage(error, ChangePwdActivity.this));

                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            int i = view.getId();
            if (i == R.id.etCPNewPwd) {
                validatePasswordListner();
            } else if (i == R.id.etCPReNewPwd) {
                validateRePasswordListner();
            }

        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void setStatusBarGradiant(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            Drawable background = activity.getResources().getDrawable(R.drawable.bg_gradient_new);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
    }

}
