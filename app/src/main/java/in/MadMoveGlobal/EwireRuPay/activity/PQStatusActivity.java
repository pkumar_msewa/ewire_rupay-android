package in.MadMoveGlobal.EwireRuPay.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import in.MadMoveGlobal.model.PQCart;
import in.MadMoveGlobal.model.PQTelebuyPaymentHolder;
import in.MadMoveGlobal.EwireRuPay.R;


public class PQStatusActivity extends AppCompatActivity {
	
	private TextView tvTransactionStatus;
	private TextView tvCongratulationTransactionStatus;

    private PQCart cart = PQCart.getInstance();
    private PQTelebuyPaymentHolder payTelebuy = PQTelebuyPaymentHolder.getInstance();
	
	@Override
	public void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.activity_status);
		

		Intent mainIntent = getIntent();
		tvTransactionStatus = (TextView) findViewById(R.id.tvTransactionStatus);
		tvCongratulationTransactionStatus = (TextView) findViewById(R.id.tvCongratulationTransactionStatus);
		
		final String transactionStatus = mainIntent.getStringExtra("transStatus");
		final String html = mainIntent.getStringExtra("html");



	}
	
}