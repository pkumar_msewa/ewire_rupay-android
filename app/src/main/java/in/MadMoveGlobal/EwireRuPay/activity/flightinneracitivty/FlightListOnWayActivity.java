package in.MadMoveGlobal.EwireRuPay.activity.flightinneracitivty;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import in.MadMoveGlobal.adapter.FlightListAdapter;
import in.MadMoveGlobal.custom.CustomAlertDialog;
import in.MadMoveGlobal.custom.CustomFiltterDialog;
import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.metadata.AppMetadata;
import in.MadMoveGlobal.model.BusCityModel;
import in.MadMoveGlobal.model.FlightListModel;
import in.MadMoveGlobal.model.FlightModel;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.util.CitySelectedListener;
import in.MadMoveGlobal.util.NetworkErrorHandler;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;


/**
 * Created by Ksf on 9/30/2016.
 */
public class FlightListOnWayActivity extends AppCompatActivity {

    private RecyclerView lvListBus;
    private LoadingDialog loadingDialog;
    private String destinationCode, sourceCode;
    private String dateOfDep, flightClass;
    private int noOfAdult, noOfChild, noOfInfant;
    private int flightType;
    AlertDialog alertDialog;
    //Volley
    private boolean allAirLinesValue = false;
    private String tag_json_obj = "json_travel";
    private LinearLayout llNoBus;
    private ArrayList<FlightListModel> flightListItem;

    private AppCompatCheckBox allAir_spice_jet_Lines, allAir_vistara_Lines, allAir_air_india_Lines, allAir_indigo_Lines, allAir_jet_air_Lines_Lines, all_go_AirLines;
    //header
    private TextView tvHeaderBusListDate;
    private ImageView ivHeaderListDate;
    private JSONObject jsonRequest;

    private UserModel session = UserModel.getInstance();

    private String jsonToSend = "";
    private String jsonFareToSend = "";
    private JSONObject jsonContiongFlightObject;

    private String nonStopValue = "null";
    private String refund = "null";
    private String before_eleven_am_, before_eleven_and_five_pm_, retrun_before_eleven_am_, above_nine_pm_, before_five_pm_nine_;
    private String retrun_before_eleven_and_five_pm_, retrun_before_five_pm_nine_, retrun_above_nine_pm_;
    private FlightListAdapter flightListAdapter;
    private String airAsia = "";
    private String airIndia = "";
    private String spicejet = "";
    private String jetairways = "";
    private String indigo = "";
    private String goAir = "";
    private LinearLayout listfilters;
    private int citylist;
    private Button reset;
    private LinearLayout llNoFiltter;
    private int currentPostion = 0, count = 0;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oneway_flight_list);
        loadingDialog = new LoadingDialog(FlightListOnWayActivity.this);
        llNoBus = (LinearLayout) findViewById(R.id.llNoBus);
        TextView tvNoBus = (TextView) findViewById(R.id.tvNoBus);
        ImageView ivNoBus = (ImageView) findViewById(R.id.ivNoBus);
        listfilters = (LinearLayout) findViewById(R.id.listfilters);
        tvHeaderBusListDate = (TextView) findViewById(R.id.tvHeaderBusListDate);
        ivHeaderListDate = (ImageView) findViewById(R.id.ivHeaderListDate);
        ivHeaderListDate.setImageResource(R.drawable.ic_no_flight);
        llNoFiltter = (LinearLayout) findViewById(R.id.llNoFiltter);
        ivHeaderListDate.setVisibility(View.INVISIBLE);

        //Setting for flight in bus xml file
        tvNoBus.setText(getResources().getString(R.string.no_flight));
        ivNoBus.setImageResource(R.drawable.ic_no_flight);
        llNoBus.setVisibility(View.GONE);

        final Button duration = (Button) findViewById(R.id.Duration);
        final Button departure = (Button) findViewById(R.id.Departure);
        final Button price = (Button) findViewById(R.id.Price);
        jsonContiongFlightObject = new JSONObject();
        noOfAdult = getIntent().getIntExtra("Adult", 0);
        noOfChild = getIntent().getIntExtra("Child", 0);
        noOfInfant = getIntent().getIntExtra("Infant", 0);
        flightClass = getIntent().getStringExtra("Class");
        destinationCode = getIntent().getStringExtra("destination");
        sourceCode = getIntent().getStringExtra("source");
        dateOfDep = getIntent().getStringExtra("date");
        flightType = getIntent().getIntExtra("flightType", 0);

        citylist = getIntent().getIntExtra("citylist", 0);
        lvListBus = (RecyclerView) findViewById(R.id.lvListBus);
        //press back button in toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbars);
        ImageButton ivBackBtns = (ImageButton) findViewById(R.id.ivBackBtns);
        ivBackBtns.setVisibility(View.VISIBLE);
        setSupportActionBar(toolbar);
        ivBackBtns.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        getFlightList();
        departure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                price.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, null, null);
                duration.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, null, null);

                if (v.isSelected()) {
                    getDepartureFilterHightToLow(flightListAdapter, lvListBus);
                    v.setSelected(false);
//                    departure.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,getResources().getDrawable(R.drawable.ic_price_high_low),null);
                } else {
                    getDepartureFilter(flightListAdapter, lvListBus);
//                    departure.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,getResources().getDrawable(R.drawable.ic_price_low_to_high),null);
                    v.setSelected(true);
                }
            }
        });
        duration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                departure.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, null, null);
                price.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, null, null);
                if (v.isSelected()) {
                    getDUrationHighTOLow(flightListAdapter, lvListBus);
//                    duration.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,getResources().getDrawable(R.drawable.ic_price_high_low),null);
                    v.setSelected(false);
                } else {
                    getDUrationLowTOHigh(flightListAdapter, lvListBus);
//                    duration.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,getResources().getDrawable(R.drawable.ic_price_low_to_high),null);
                    v.setSelected(true);
                }
            }
        });
        price.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                departure.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, null, null);
                duration.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, null, null);
                if (v.isSelected()) {
                    getpriceHighToLow(flightListAdapter, lvListBus);
//                    price.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,getResources().getDrawable(R.drawable.ic_price_high_low),null);
                    v.setSelected(false);
                } else {
                    getpriceLowToHight(flightListAdapter, lvListBus);
//                    price.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,getResources().getDrawable(R.drawable.ic_price_low_to_high),null);
                    v.setSelected(true);
                }
            }
        });
    }

    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        public SpacesItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view,
                                   RecyclerView parent, RecyclerView.State state) {
            outRect.left = space;
            outRect.right = space;
            outRect.bottom = space;
            outRect.top = space;
        }
    }

    private void getFlightList() {
        loadingDialog.show();
        flightListItem = new ArrayList<>();
        jsonRequest = new JSONObject();
        try {

            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("origin", sourceCode);
            jsonRequest.put("destination", destinationCode);
            jsonRequest.put("tripType", "OneWay");
            jsonRequest.put("cabin", flightClass);
            jsonRequest.put("adults", noOfAdult);
            jsonRequest.put("childs", noOfChild);
            jsonRequest.put("infants", noOfInfant);
            jsonRequest.put("traceId", "AYTM00011111111110002");
            jsonRequest.put("beginDate", dateOfDep);

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }


        if (jsonRequest != null) {
            Log.i("jsonRequest", jsonRequest.toString());
            Log.i("URL", ApiUrl.URL_FLIGHT_LIST_DETAILS);
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_FLIGHT_LIST_DETAILS, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.i("Flight List Response", response.toString());
                    try {

                        String code = response.getString("code");

                        if (code != null && code.equals("S00")) {

                            JSONObject jsonDetails = response.getJSONObject("details");

                            JSONArray jsonJourney = jsonDetails.getJSONArray("journeys");
                            JSONObject jsonJrnyObj = jsonJourney.getJSONObject(0);
                            JSONArray jsonSegmentArray = jsonJrnyObj.getJSONArray("segments");


                            if (jsonSegmentArray.length() != 0) {

                                for (int k = 0; k < jsonSegmentArray.length(); k++) {

                                    String jsonSegmentString = jsonSegmentArray.getJSONObject(k).toString();

                                    ArrayList<FlightModel> flightArray = new ArrayList<>();
                                    JSONObject segmentObj = jsonSegmentArray.getJSONObject(k);
                                    JSONArray bondArray = segmentObj.getJSONArray("bonds");

                                    JSONObject bonObj = bondArray.getJSONObject(0);
                                    JSONArray legArray = bonObj.getJSONArray("legs");
                                    JSONObject segmentConting = jsonSegmentArray.getJSONObject(k);
                                    JSONObject contingFlight = new JSONObject();
                                    if (legArray.length() >= 1) {
                                        JSONArray bounds = new JSONArray();
                                        JSONObject bondary = bondArray.getJSONObject(0);
                                        JSONArray jsonArray = new JSONArray();
                                        for (int j = 0; j < legArray.length(); j++) {
                                            Log.i("jsonRevalues", legArray.toString());
                                            JSONObject d = legArray.getJSONObject(j);
                                            d.remove("cabinClasses");
                                            d.remove("ssrDetails");
                                            d.remove("amount");
                                            d.remove("flightName");
                                            d.remove("connecting");
                                            d.remove("numberOfStops");
                                            d.remove("remarks");
                                            d.remove("journeyTime");
                                            jsonArray.put(d);
                                        }
                                        bondary.put("legs", jsonArray);
                                        bounds.put(bondary);
                                        contingFlight.put("bonds", bounds);
                                        String s = segmentObj.getJSONArray("fares").toString();
                                        Log.i("valuesJson", s);
                                        s = s.replace("\"fares\":", "\"bookFares\":");
                                        JSONArray jsonArray1 = new JSONArray(s);
                                        contingFlight.put("fares", jsonArray1);
                                        segmentConting.remove("bondType");
                                        segmentConting.remove("remark");
                                        contingFlight.put("baggageFare", segmentConting.getString("baggageFare"));
                                        contingFlight.put("cache", segmentConting.getString("cache"));
                                        contingFlight.put("holdBooking", segmentConting.getString("holdBooking"));
                                        contingFlight.put("international", segmentConting.getString("international"));
                                        contingFlight.put("roundTrip", segmentConting.getString("roundTrip"));
                                        contingFlight.put("special", segmentConting.getString("special"));
                                        contingFlight.put("specialId", segmentConting.getString("specialId"));
                                        contingFlight.put("engineID", segmentConting.getString("engineID"));
                                        contingFlight.put("fareRule", segmentConting.getString("fareRule"));
                                        contingFlight.put("itineraryKey", segmentConting.getString("itineraryKey"));
                                        contingFlight.put("journeyIndex", segmentConting.getString("journeyIndex"));
                                        contingFlight.put("nearByAirport", segmentConting.getString("nearByAirport"));
                                        contingFlight.put("searchId", segmentConting.getString("searchId"));
                                        contingFlight.put("origin", sourceCode);
                                        contingFlight.put("destination", destinationCode);
                                        contingFlight.put("tripType", "OneWay");
                                        contingFlight.put("cabin", flightClass);
                                        contingFlight.put("adults", noOfAdult);
                                        contingFlight.put("childs", noOfChild);
                                        contingFlight.put("infants", noOfInfant);
                                        contingFlight.put("traceId", "AYTM00011111111110002");
                                        contingFlight.put("beginDate", dateOfDep);
                                        contingFlight.put("endDate", dateOfDep);

                                    }
                                    Log.i("contingflight", String.valueOf(contingFlight));
                                    for (int j = 0; j < legArray.length(); j++) {
                                        JSONObject d = legArray.getJSONObject(j);
                                        jsonToSend = legArray.getJSONObject(j).toString();

                                        String depAirportCode = d.getString("origin");
                                        String depTime = d.getString("departureTime");
                                        String arrivalAirportCode = d.getString("destination");
                                        String arrivalTime = d.getString("arrivalTime");

                                        String flightDuration = d.getString("duration");
                                        String flightName = d.getString("airlineName");
                                        String flightNo = d.getString("flightNumber");
                                        String flightImage = d.getString("airlineName");
                                        String flightCode = d.getString("aircraftCode");


                                        String flightArrivalTimeZone = d.getString("arrivalDate");
                                        String flightDepartureTimeZone = d.getString("departureDate");

                                        String flightArrivalAirportName = d.getString("destination");
                                        String flightDepartureAirportName = d.getString("origin");
                                        String flightTpe = segmentConting.getString("engineID");
                                        String flightRule = "";

                                        JSONArray fareArray = segmentObj.getJSONArray("fares");
                                        JSONObject fareObj = fareArray.getJSONObject(0);
                                        JSONArray paxFaresArray = fareObj.getJSONArray("paxFares");
                                        JSONObject paxFaresObj = paxFaresArray.getJSONObject(0);

                                        jsonFareToSend = paxFaresArray.getJSONObject(0).toString();

                                        FlightModel flightModel = new FlightModel(depAirportCode, depTime, arrivalAirportCode, arrivalTime, flightDuration, flightName,
                                                flightNo, flightImage, flightRule, flightCode, flightArrivalTimeZone, flightDepartureTimeZone, flightArrivalAirportName, flightDepartureAirportName, 1, flightTpe, jsonToSend, jsonFareToSend);
                                        flightArray.add(flightModel);


                                    }
                                    JSONArray fareArray = segmentObj.getJSONArray("fares");
                                    JSONObject fareObj = fareArray.getJSONObject(0);
                                    JSONArray paxFaresArray = fareObj.getJSONArray("paxFares");
                                    JSONObject paxFaresObj = paxFaresArray.getJSONObject(0);

                                    jsonFareToSend = paxFaresArray.getJSONObject(0).toString();


                                    double flightActualBaseFare = paxFaresObj.getDouble("basicFare");
                                    double flightTaxFare = 0.0;
                                    double totalFare = 0.0;
                                    if (fareObj.has("totalTaxWithOutMarkUp")) {
                                        flightTaxFare = fareObj.getDouble("totalTaxWithOutMarkUp");
                                    }
                                    if (fareObj.has("totalFareWithOutMarkUp")) {
                                        totalFare = fareObj.getDouble("totalFareWithOutMarkUp");
                                    }
                                    boolean refund = paxFaresObj.getBoolean("refundable");
                                    String duration = compareDate(legArray.getJSONObject(0).getString("departureTime"), legArray.getJSONObject(legArray.length() - 1).getString("arrivalTime"));
                                    Log.i("duration", duration);
                                    FlightListModel flightModel = new FlightListModel(flightActualBaseFare, flightTaxFare, 0.0, 0.0, 0.0,
                                            totalFare, totalFare, "", flightArray, jsonSegmentString, contingFlight.toString(), new ArrayList<FlightModel>(), String.valueOf(refund), legArray.getJSONObject(0).getString("departureTime"), duration);

                                    flightListItem.add(flightModel);
                                }

                                ivHeaderListDate.setVisibility(View.VISIBLE);
                                tvHeaderBusListDate.setText(sourceCode + " " + getResources().getString(R.string.arrow) + " " + destinationCode + "\n" + dateOfDep);

                                int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.recycle_spacing);
                                lvListBus.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
                                GridLayoutManager manager = new GridLayoutManager(FlightListOnWayActivity.this, 1);
                                lvListBus.setLayoutManager(manager);
                                lvListBus.setVisibility(View.VISIBLE);
                                flightListAdapter = new FlightListAdapter(FlightListOnWayActivity.this, flightListItem, sourceCode, destinationCode, "\n" + dateOfDep, noOfAdult, noOfChild, noOfInfant, flightClass, jsonToSend, jsonFareToSend, citylist);
                                listfilters.setVisibility(View.VISIBLE);
                                getpriceLowToHight(flightListAdapter, lvListBus);
//                                lvListBus.setAdapter(flightListAdapter);
                                llNoBus.setVisibility(View.GONE);
                                lvListBus.setVisibility(View.VISIBLE);

                                loadingDialog.dismiss();
                            } else {
                                loadingDialog.dismiss();
                                llNoBus.setVisibility(View.VISIBLE);
                                lvListBus.setVisibility(View.GONE);
                            }
                        } else if (code != null && code.equals("F03")) {
                            loadingDialog.dismiss();
                            showInvalidSessionDialog();
                        } else {
                            loadingDialog.dismiss();
                            llNoBus.setVisibility(View.VISIBLE);
                            lvListBus.setVisibility(View.GONE);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        CustomToast.showMessage(getApplicationContext(), "Unexpected response from server");
                        loadingDialog.dismiss();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    CustomToast.showMessage(getApplicationContext(), NetworkErrorHandler.getMessage(error, getApplicationContext()));
                    loadingDialog.dismiss();
                }
            }) {


            };
            int socketTimeout = 600000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            jsonObjReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        }
    }

    public void showInvalidSessionDialog() {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(AppMetadata.getInvalidSession(), Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(AppMetadata.getInvalidSession());
        }
        CustomAlertDialog builder = new CustomAlertDialog(FlightListOnWayActivity.this, R.string.dialog_title2, result);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(FlightListOnWayActivity.this).sendBroadcast(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.flight_filter, menu);

        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (flightListItem != null && flightListItem.size() != 0) {
            if (id == R.id.menuFilter) {

                LayoutInflater myLayout = LayoutInflater.from(FlightListOnWayActivity.this);
                final View dialogView = myLayout.inflate(R.layout.dailog_flight_filter, null);
                final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(FlightListOnWayActivity.this);
                alertDialogBuilder.setView(dialogView);
                RadioGroup rbNonStop = (RadioGroup) dialogView.findViewById(R.id.rbNonStop);
                RadioGroup rg_refurend = (RadioGroup) dialogView.findViewById(R.id.rg_refurend);
                Button btnFilter = (Button) dialogView.findViewById(R.id.btnFilter);
                Button btnResetFilter = (Button) dialogView.findViewById(R.id.btnResetFilter);
                ImageView ivFilterClose = (ImageView) dialogView.findViewById(R.id.ivFilterClose);
                AppCompatCheckBox allAirLines = (AppCompatCheckBox) dialogView.findViewById(R.id.allAirLines);
                final Spinner spDepature = (Spinner) dialogView.findViewById(R.id.spDepature);
                RadioButton rbRefundable = (RadioButton) dialogView.findViewById(R.id.rbRefundable);
                Spinner spRetrunDepature = (Spinner) dialogView.findViewById(R.id.spRetrunDepature);
                spRetrunDepature.setVisibility(View.GONE);
                LinearLayout oneway = (LinearLayout) dialogView.findViewById(R.id.oneway);
                oneway.setVisibility(View.GONE);
                refund = "null";
                nonStopValue = "null";
                TextView retrun = (TextView) dialogView.findViewById(R.id.retrun);
                retrun.setVisibility(View.GONE);
                ArrayList<String> strings = new ArrayList<>();
                strings.add("All Time");
                strings.add("Before 11:00 am");
                strings.add("11:00-5:00 pm");
                strings.add("5:00-9:00 pm");
                strings.add("above 9:00 pm");
                ArrayAdapter<String> stringArrayAdapter = new ArrayAdapter<String>(FlightListOnWayActivity.this, android.R.layout.simple_expandable_list_item_1, strings);
                spDepature.setAdapter(stringArrayAdapter);
                spRetrunDepature.setAdapter(stringArrayAdapter);
                all_go_AirLines = (AppCompatCheckBox) dialogView.findViewById(R.id.all_go_AirLines);
                allAir_indigo_Lines = (AppCompatCheckBox) dialogView.findViewById(R.id.allAir_indigo_Lines);
                allAir_jet_air_Lines_Lines = (AppCompatCheckBox) dialogView.findViewById(R.id.allAir_jet_air_Lines_Lines);
                allAir_spice_jet_Lines = (AppCompatCheckBox) dialogView.findViewById(R.id.allAir_spice_jet_Lines);
                allAir_air_india_Lines = (AppCompatCheckBox) dialogView.findViewById(R.id.allAir_air_india_Lines);
                allAir_vistara_Lines = (AppCompatCheckBox) dialogView.findViewById(R.id.allAir_vistara_Lines);
                ivFilterClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });
                airAsia = "";
                airIndia = "";
                spicejet = "";
                jetairways = "";
                indigo = "";
                goAir = "";
                count = 0;
                all_go_AirLines.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            goAir = "GoAir";
                            count++;
                        } else {
                            count--;
                        }

                    }
                });
                allAir_indigo_Lines.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            indigo = "Indigo";
                            count++;
                        } else {
                            count--;
                        }
                    }
                });
                allAir_jet_air_Lines_Lines.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            jetairways = "Jetairways";
                            count++;
                        } else {
                            count--;
                        }
                    }
                });
                allAir_spice_jet_Lines.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            spicejet = "Spicjet";
                            count++;
                        } else {
                            count--;
                        }

                    }
                });
                allAir_air_india_Lines.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            airIndia = "Airindia";
                            count++;
                        } else {
                            count--;
                        }

                    }
                });
                allAir_vistara_Lines.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            airAsia = "AirAsia";
                            count++;
                        } else {
                            count--;
                        }

                    }
                });
                btnResetFilter.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        count = 0;
                        currentPostion = 0;
                        airAsia = "";
                        airIndia = "";
                        spicejet = "";
                        jetairways = "";
                        indigo = "";
                        goAir = "";
                        flightListAdapter = new FlightListAdapter(FlightListOnWayActivity.this, flightListItem, sourceCode, destinationCode, "\n" + dateOfDep, noOfAdult, noOfChild, noOfInfant, flightClass, jsonToSend, jsonFareToSend, citylist);
                        alertDialog.dismiss();
                        lvListBus.setAdapter(flightListAdapter);
                        flightListAdapter.notifyDataSetChanged();
                    }
                });
                allAirLines.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            all_go_AirLines.setChecked(true);
                            allAir_indigo_Lines.setChecked(true);
                            allAir_jet_air_Lines_Lines.setChecked(true);
                            allAir_spice_jet_Lines.setChecked(true);
                            allAir_air_india_Lines.setChecked(true);
                            allAir_vistara_Lines.setChecked(true);
                            allAirLinesValue = true;
                        } else {
                            all_go_AirLines.setChecked(false);
                            allAir_indigo_Lines.setChecked(false);
                            allAir_jet_air_Lines_Lines.setChecked(false);
                            allAir_spice_jet_Lines.setChecked(false);
                            allAir_air_india_Lines.setChecked(false);
                            allAir_vistara_Lines.setChecked(false);
                            allAirLinesValue = false;
                        }
                    }
                });


                rbNonStop.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                                                         @Override
                                                         public void onCheckedChanged(RadioGroup group, int checkedId) {
                                                             switch (checkedId) {
                                                                 case R.id.nonStop:
                                                                     // do operations specific to this selection
                                                                     nonStopValue = "1";
                                                                     break;
                                                                 case R.id.one_Stop:
                                                                     // do operations specific to this selection
                                                                     nonStopValue = "2";
                                                                     break;
                                                                 case R.id.moreStops:
                                                                     // do operations specific to this selection
                                                                     nonStopValue = "3";
                                                                     break;
                                                             }
                                                         }
                                                     }
                );
                rg_refurend.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                                                           @Override
                                                           public void onCheckedChanged(RadioGroup group, int checkedId) {
                                                               switch (checkedId) {
                                                                   case R.id.rbRefundable:
                                                                       // do operations specific to this selection
                                                                       refund = "true";
                                                                       break;
                                                                   case R.id.rbNonRefundable:
                                                                       // do operations specific to this selection
                                                                       refund = "false";
                                                                       break;

                                                               }
                                                           }
                                                       }
                );
                btnFilter.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        filtterList(refund, nonStopValue, spDepature.getSelectedItem().toString());
                        alertDialog.dismiss();
                    }
                });


                // create alert dialog
                alertDialog = alertDialogBuilder.create();


                // show it
                alertDialog.show();
                alertDialog.setCancelable(true);
            }
        } else {
            CustomToast.showMessage(FlightListOnWayActivity.this, getResources().getString(R.string.no_flight));
        }
        return super.onOptionsItemSelected(item);
    }

    private void filtterList(String refund, String nonStopValue, String depature) {

        ArrayList<FlightListModel> flightListModels = new ArrayList<>();
        if (refund.equalsIgnoreCase("null") || !nonStopValue.equalsIgnoreCase("null") || !refund.equalsIgnoreCase("null") || nonStopValue.equalsIgnoreCase("null")) {
            ArrayList<FlightListModel> flightListModels1 = getRefurend(flightListItem, refund, nonStopValue);
            if (flightListModels1 != null && flightListModels1.size() != 0 && depature.equalsIgnoreCase("All Time")) {
                flightListModels = flightListModels1;
            }
            if (refund.equalsIgnoreCase("null") && nonStopValue.equalsIgnoreCase("null") && depature.equalsIgnoreCase("All Time")) {
                flightListModels = flightListItem;
            }
            if (refund.equalsIgnoreCase("null") && nonStopValue.equalsIgnoreCase("null") && !depature.equalsIgnoreCase("All Time")) {
                flightListModels1 = flightListItem;
            }
            if (flightListModels1.size() != 0 && flightListModels1 != null && depature.contains("Before")) {
                SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
                SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
                try {
                    Date date = parseFormat.parse("11:00 AM");
                    String hoursList = displayFormat.format(date);
                    for (FlightListModel listModel : flightListModels1) {
                        if (date(listModel.getDepature()).before(date(hoursList))) {
                            flightListModels.add(listModel);
                        }
                    }

                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } else if (depature.contains("11:00-5:00 pm") && flightListModels1.size() != 0 && flightListModels1 != null) {
                SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
                SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
                try {
                    Date date = parseFormat.parse("11:00 AM");
                    Date secondDate = parseFormat.parse("5:00 PM");
                    String hoursList = displayFormat.format(date);
                    String hoursList1 = displayFormat.format(secondDate);
                    for (FlightListModel listModel : flightListModels1) {
                        if (date(listModel.getDepature()).after(date(hoursList)) && date(listModel.getDepature()).before(date(hoursList1))) {
                            flightListModels.add(listModel);
                        }
                    }

                } catch (ParseException e) {
                    e.printStackTrace();
                }

            } else if (depature.contains("5:00-9:00 pm") && flightListModels1.size() != 0 && flightListModels1 != null) {
                SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
                SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
                try {
                    Date date = parseFormat.parse("5:00 PM");
                    Date secondDate = parseFormat.parse("9:00 PM");
                    String hoursList = displayFormat.format(date);
                    String hoursList1 = displayFormat.format(secondDate);
                    Log.i("values", hoursList + "" + hoursList1);
                    for (FlightListModel listModel : flightListModels1) {
                        if (date(listModel.getDepature()).after(date(hoursList)) && date(listModel.getDepature()).before(date(hoursList1))) {
                            flightListModels.add(listModel);
                        }
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } else if (depature.contains("above 9:00 pm") && flightListModels1.size() != 0 && flightListModels1 != null) {
                SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
                SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
                try {
                    Date date = parseFormat.parse("9:00 PM");
                    String hoursList = displayFormat.format(date);
                    for (FlightListModel listModel : flightListModels1) {
                        if (date(listModel.getDepature()).after(date(hoursList))) {
                            flightListModels.add(listModel);
                        }
                    }

                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } else if (depature.contains("All Time") && flightListModels1.size() != 0 && flightListModels1 != null) {
                flightListModels = flightListModels1;
            }

        } else if (!refund.equalsIgnoreCase("null") && !nonStopValue.equalsIgnoreCase("null") && depature != null && !depature.isEmpty()) {
            ArrayList<FlightListModel> flightListModels1 = getRefurend(flightListItem, refund, nonStopValue);
            if (flightListModels1 != null && depature.equalsIgnoreCase("All Time")) {
                flightListModels = flightListModels1;
            } else if (flightListModels1 != null && !depature.equalsIgnoreCase("All Time")) {
                flightListModels = flightListModels1;
            }
            if (flightListModels1.size() != 0 && flightListModels1 != null && depature.contains("Before")) {
                SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
                SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
                try {
                    Date date = parseFormat.parse("11:00 AM");
                    String hoursList = displayFormat.format(date);
                    for (FlightListModel listModel : flightListModels1) {
                        if (date(listModel.getDepature()).before(date(hoursList))) {
                            flightListModels.add(listModel);
                        }
                    }

                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } else if (depature.contains("11:00-5:00 pm") && flightListModels1.size() != 0 && flightListModels1 != null) {
                SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
                SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
                try {
                    Date date = parseFormat.parse("11:00 AM");
                    Date secondDate = parseFormat.parse("5:00 PM");
                    String hoursList = displayFormat.format(date);
                    String hoursList1 = displayFormat.format(secondDate);
                    for (FlightListModel listModel : flightListModels1) {
                        if (date(listModel.getDepature()).after(date(hoursList)) && date(listModel.getDepature()).before(date(hoursList1))) {
                            flightListModels.add(listModel);
                        }
                    }

                } catch (ParseException e) {
                    e.printStackTrace();
                }

            } else if (depature.contains("5:00-9:00 pm") && flightListModels1.size() != 0 && flightListModels1 != null) {
                SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
                SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
                try {
                    Date date = parseFormat.parse("5:00 PM");
                    Date secondDate = parseFormat.parse("9:00 PM");
                    String hoursList = displayFormat.format(date);
                    String hoursList1 = displayFormat.format(secondDate);
                    for (FlightListModel listModel : flightListModels1) {
                        if (date(listModel.getDepature()).after(date(hoursList)) && date(listModel.getDepature()).before(date(hoursList1))) {
                            flightListModels.add(listModel);
                        }
                    }

                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } else if (depature.contains("above 9:00 pm") && flightListModels1.size() != 0 && flightListModels1 != null) {
                SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
                SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
                try {
                    Date date = parseFormat.parse("9:00 PM");
                    String hoursList = displayFormat.format(date);
                    for (FlightListModel listModel : flightListModels1) {
                        if (date(listModel.getDepature()).after(date(hoursList))) {
                            flightListModels.add(listModel);
                        }
                    }

                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } else if (depature.contains("All Time") && flightListModels1.size() != 0 && flightListModels1 != null) {
                flightListModels = flightListModels1;
            }
        }
        if (allAirLinesValue && flightListModels.size() != 0 && flightListModels != null) {
            if (getNonStop(flightListModels, "Spicejet").size() != 0 && getNonStop(flightListModels, "JetAirways").size() != 0 && getNonStop(flightListModels, "Indigo").size() != 0 && getNonStop(flightListModels, "Airindia").size() != 0 && getNonStop(flightListModels, "AirAsia").size() != 0 && getNonStop(flightListModels, "GoAir").size() != 0) {
                flightListModels.addAll(getNonStop(flightListModels, "Spicejet"));
                flightListModels.addAll(getNonStop(flightListModels, "Jetairways"));
                flightListModels.addAll(getNonStop(flightListModels, "Indigo"));
                flightListModels.addAll(getNonStop(flightListModels, "Airindia"));
                flightListModels.addAll(getNonStop(flightListModels, "AirAsia"));
                flightListModels.addAll(getNonStop(flightListModels, "GoAir"));
            } else {
                flightListModels = new ArrayList<>();
            }

        }
        Log.i("looo", String.valueOf(flightListModels.size()));
        if (!allAirLinesValue && flightListModels.size() != 0 && flightListModels != null) {
            ArrayList<FlightListModel> models = new ArrayList<>();
            models.addAll(getNonStop(flightListModels, spicejet));
            models.addAll(getNonStop(flightListModels, jetairways));
            models.addAll(getNonStop(flightListModels, indigo));
            models.addAll(getNonStop(flightListModels, airAsia));
            models.addAll(getNonStop(flightListModels, airIndia));
            models.addAll(getNonStop(flightListModels, goAir));
            if (spicejet.equalsIgnoreCase("") && jetairways.equalsIgnoreCase("") && indigo.equalsIgnoreCase("") && airIndia.equalsIgnoreCase("") && airAsia.equalsIgnoreCase("") && goAir.equalsIgnoreCase("")) {
                if (flightListModels != null && flightListModels.size() != 0) {
                    airAsia = "";
                    airIndia = "";
                    spicejet = "";
                    jetairways = "";
                    indigo = "";
                    goAir = "";
                    count = 0;
                    currentPostion = 0;
                    flightListAdapter = new FlightListAdapter(FlightListOnWayActivity.this, flightListModels, sourceCode, destinationCode, "\n" + dateOfDep, noOfAdult, noOfChild, noOfInfant, flightClass, jsonToSend, jsonFareToSend, citylist);
                    lvListBus.setAdapter(flightListAdapter);
                } else {
                    llNoFiltter.setVisibility(View.VISIBLE);
                    CustomFiltterDialog customFiltterDialog = new CustomFiltterDialog(FlightListOnWayActivity.this, "", "", new CitySelectedListener() {
                        @Override
                        public void citySelected(String type, long cityCode, String cityName) {
                            llNoFiltter.setVisibility(View.GONE);
                            airAsia = "";
                            airIndia = "";
                            spicejet = "";
                            jetairways = "";
                            indigo = "";
                            goAir = "";
                            count = 0;
                            currentPostion = 0;
                            flightListAdapter = new FlightListAdapter(FlightListOnWayActivity.this, flightListItem, sourceCode, destinationCode, "\n" + dateOfDep, noOfAdult, noOfChild, noOfInfant, flightClass, jsonToSend, jsonFareToSend, citylist);
                            lvListBus.setAdapter(flightListAdapter);
                        }

                        @Override
                        public void cityFilter(BusCityModel type) {

                        }
                    });

                    customFiltterDialog.show();
                }
            } else {
                Log.i("count", currentPostion + "::" + count);
                if (count == currentPostion) {
                    flightListModels = models;
                } else {
                    flightListModels = new ArrayList<>();
                }
            }
        }


        if (flightListModels != null && flightListModels.size() != 0) {
            airAsia = "";
            airIndia = "";
            spicejet = "";
            jetairways = "";
            indigo = "";
            goAir = "";
            count = 0;
            currentPostion = 0;
            flightListAdapter = new FlightListAdapter(FlightListOnWayActivity.this, flightListModels, sourceCode, destinationCode, "\n" + dateOfDep, noOfAdult, noOfChild, noOfInfant, flightClass, jsonToSend, jsonFareToSend, citylist);
            lvListBus.setAdapter(flightListAdapter);
        } else {
            llNoFiltter.setVisibility(View.VISIBLE);
            CustomFiltterDialog customFiltterDialog = new CustomFiltterDialog(FlightListOnWayActivity.this, "", "", new CitySelectedListener() {
                @Override
                public void citySelected(String type, long cityCode, String cityName) {
                    llNoFiltter.setVisibility(View.GONE);
                    airAsia = "";
                    airIndia = "";
                    spicejet = "";
                    jetairways = "";
                    indigo = "";
                    goAir = "";
                    count = 0;
                    currentPostion = 0;
                    flightListAdapter = new FlightListAdapter(FlightListOnWayActivity.this, flightListItem, sourceCode, destinationCode, "\n" + dateOfDep, noOfAdult, noOfChild, noOfInfant, flightClass, jsonToSend, jsonFareToSend, citylist);
                    lvListBus.setAdapter(flightListAdapter);
                }

                @Override
                public void cityFilter(BusCityModel type) {

                }
            });

            customFiltterDialog.show();

        }
    }

    ArrayList<FlightListModel> getRefurend(ArrayList<FlightListModel> flightListItem, String refund, String nonStop) {
        Log.i("list_refund", refund + nonStop);
        ArrayList<FlightListModel> flightListModels = new ArrayList<>();
        for (FlightListModel listModel : flightListItem) {
            Log.i("list_refund", String.valueOf(listModel.getFlightListArray().size()));
            if (!nonStop.equalsIgnoreCase("null") && !refund.equalsIgnoreCase("null")) {
                if (listModel.getFlightListArray().size() == Integer.parseInt(nonStop)) {
                    if (listModel.getRefund().equalsIgnoreCase(refund)) {
                        flightListModels.add(listModel);
                    }
                }
            } else if (refund.equalsIgnoreCase("null") && !nonStop.equalsIgnoreCase("null")) {
                if (listModel.getFlightListArray().size() == Integer.parseInt(nonStop)) {
                    flightListModels.add(listModel);
                }
            } else if (nonStop.equalsIgnoreCase("null") && !refund.equalsIgnoreCase("null")) {
                if (listModel.getRefund().equalsIgnoreCase(refund)) {
                    flightListModels.add(listModel);
                }
            }
        }
        return flightListModels;
    }

    ArrayList<FlightListModel> getNonStop(ArrayList<FlightListModel> flightListItem, String flag) {
        ArrayList<FlightListModel> flightListModels = new ArrayList<>();
        for (FlightListModel listModel : flightListItem) {
            if (listModel.getFlightJsonString().contains("\"engineID\":" + "\"" + flag + "\"")) {
                flightListModels.add(listModel);
            }

        }
        if (flightListModels != null && flightListModels.size() != 0) {
            currentPostion++;
            Log.i("postion", String.valueOf(currentPostion));
            Log.i("currentPostion", String.valueOf(count));
        }
        return flightListModels;
    }

    public String retrunAirLines(CheckBox checkBox) {
        if (checkBox.isChecked()) {
            return checkBox.getText().toString();
        }
        return null;
    }

    public static void longLog(String str) {
        if (str.length() > 4000) {
            Log.d("travel", str.substring(0, 4000));
            longLog(str.substring(4000));
        } else
            Log.d("travel", str);
    }

    private void getDepartureFilterHightToLow(FlightListAdapter flightListAdapterone, RecyclerView recyclerView) {
        List<FlightListModel> flightListModels = flightListAdapterone.getFlightArray();
        List<FlightListModel> listModels = new ArrayList<>();

        Collections.sort(flightListModels, new Comparator<FlightListModel>() {
            @Override
            public int compare(FlightListModel lhs, FlightListModel rhs) {
                return parseDate(lhs.getDepature()) == null ? Integer.MAX_VALUE : parseDate(rhs.getDepature()) == null ? Integer.MIN_VALUE : parseDate(lhs.getDepature()).compareTo(parseDate(rhs.getDepature()));

            }
        });

        if (flightListModels != null && flightListModels.size() != 0) {

            flightListAdapter = new FlightListAdapter(FlightListOnWayActivity.this, flightListModels, sourceCode, destinationCode, "\n" + dateOfDep, noOfAdult, noOfChild, noOfInfant, flightClass, jsonToSend, jsonFareToSend, citylist);

            recyclerView.setAdapter(flightListAdapter);
        }


    }


    private Long parseDate(String date) {
        Date arrTime = null;
        SimpleDateFormat arrinput = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
        SimpleDateFormat arroutput = new SimpleDateFormat("HH:mm a", Locale.ENGLISH);
        SimpleDateFormat depinput = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
        SimpleDateFormat depoutput = new SimpleDateFormat("HH:mm a", Locale.ENGLISH);
        try {
            arrTime = arrinput.parse(date);
//                depTime = depinput.parse(busArray.get(position).getBusDepTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return arrTime.getTime();
    }

    private Date date(String date) {
        Date arrTime = null;
        SimpleDateFormat arrinput = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
        SimpleDateFormat arroutput = new SimpleDateFormat("HH:mm a", Locale.ENGLISH);
        SimpleDateFormat depinput = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
        SimpleDateFormat depoutput = new SimpleDateFormat("HH:mm a", Locale.ENGLISH);
        try {
            arrTime = arrinput.parse(date);
//                depTime = depinput.parse(busArray.get(position).getBusDepTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return arrTime;
    }

    private Long parseDuration(String date) {
        Date arrTime = null;
        SimpleDateFormat arrinput = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
        try {
            String s = date.replaceAll(" h", ":").trim();
            String value = s.replaceAll("m", "").trim();
            arrTime = arrinput.parse(s.trim());
            Log.i("duration", String.valueOf(arrTime.getTime()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return arrTime.getTime();
    }

    private void getDUrationLowTOHigh(FlightListAdapter flightListAdapterone, RecyclerView recyclerView) {
        List<FlightListModel> flightListModels = flightListAdapterone.getFlightArray();
        List<FlightListModel> listModels = new ArrayList<>();

        Collections.sort(flightListModels, new Comparator<FlightListModel>() {
            @Override
            public int compare(FlightListModel lhs, FlightListModel rhs) {
                return parseDuration(lhs.getDuration()) == null ? Integer.MAX_VALUE : parseDuration(rhs.getDuration()) == null ? Integer.MIN_VALUE : parseDuration(rhs.getDuration()).compareTo(parseDuration(lhs.getDuration()));
            }
        });

        if (flightListModels != null && flightListModels.size() != 0) {

            flightListAdapter = new FlightListAdapter(FlightListOnWayActivity.this, flightListModels, sourceCode, destinationCode, "\n" + dateOfDep, noOfAdult, noOfChild, noOfInfant, flightClass, jsonToSend, jsonFareToSend, citylist);

            recyclerView.setAdapter(flightListAdapter);
        }
    }

    private void getDUrationHighTOLow(FlightListAdapter flightListAdapterone, RecyclerView recyclerView) {
        List<FlightListModel> flightListModels = flightListAdapterone.getFlightArray();

        Collections.sort(flightListModels, new Comparator<FlightListModel>() {
            @Override
            public int compare(FlightListModel lhs, FlightListModel rhs) {
                return parseDuration(rhs.getDuration()) == null ? Integer.MAX_VALUE : parseDuration(lhs.getDuration()) == null ? Integer.MIN_VALUE : parseDuration(lhs.getDuration()).compareTo(parseDuration(rhs.getDuration()));
            }
        });
        if (flightListModels != null && flightListModels.size() != 0) {
            flightListAdapter = new FlightListAdapter(FlightListOnWayActivity.this, flightListModels, sourceCode, destinationCode, "\n" + dateOfDep, noOfAdult, noOfChild, noOfInfant, flightClass, jsonToSend, jsonFareToSend, citylist);
            recyclerView.setAdapter(flightListAdapter);
        }
    }

    private void getpriceHighToLow(FlightListAdapter flightListAdapterone, RecyclerView recyclerView) {
        List<FlightListModel> flightListModels = flightListAdapterone.getFlightArray();

        Collections.sort(flightListModels, new Comparator<FlightListModel>() {
            @Override
            public int compare(FlightListModel lhs, FlightListModel rhs) {
//                    return Double.parseDouble(lhs.getBusFare()) > Double.parseDouble(rhs.getBusFare()) ? 1 : (Double.parseDouble(lhs.getBusFare()) < Double.parseDouble(rhs.getBusFare())) ? -1 : 0;
                return (lhs.getFlightNetFare()) > rhs.getFlightNetFare() ? -1 : lhs.getFlightNetFare() < rhs.getFlightNetFare() ? 1 : 0;
            }
        });

        if (flightListModels != null && flightListModels.size() != 0) {
            flightListAdapter = new FlightListAdapter(FlightListOnWayActivity.this, flightListModels, sourceCode, destinationCode, "\n" + dateOfDep, noOfAdult, noOfChild, noOfInfant, flightClass, jsonToSend, jsonFareToSend, citylist);
            recyclerView.setAdapter(flightListAdapter);
        }
    }

    private void getpriceLowToHight(FlightListAdapter flightListAdapterone, RecyclerView recyclerView) {
        List<FlightListModel> flightListModels = flightListAdapterone.getFlightArray();

        Collections.sort(flightListModels, new Comparator<FlightListModel>() {
            @Override
            public int compare(FlightListModel lhs, FlightListModel rhs) {
                // -1 - less than, 1 - greater than, 0 - equal, all inversed for descending
//                    return Double.parseDouble(lhs.getBusFare()) > Double.parseDouble(rhs.getBusFare()) ? 1 : (Double.parseDouble(lhs.getBusFare()) < Double.parseDouble(rhs.getBusFare())) ? -1 : 0;
                return lhs.getFlightNetFare() > rhs.getFlightNetFare() ? 1 : lhs.getFlightNetFare() < rhs.getFlightNetFare() ? -1 : 0;
            }
        });

        if (flightListModels != null && flightListModels.size() != 0) {

            flightListAdapter = new FlightListAdapter(FlightListOnWayActivity.this, flightListModels, sourceCode, destinationCode, "\n" + dateOfDep, noOfAdult, noOfChild, noOfInfant, flightClass, jsonToSend, jsonFareToSend, citylist);

            recyclerView.setAdapter(flightListAdapter);
        }
    }

    private void getDepartureFilter(FlightListAdapter flightListAdapterone, RecyclerView recyclerView) {
        List<FlightListModel> flightListModels = flightListAdapterone.getFlightArray();
        Collections.sort(flightListModels, new Comparator<FlightListModel>() {
            @Override
            public int compare(FlightListModel lhs, FlightListModel rhs) {
                return parseDate(lhs.getDepature()) == null ? Integer.MAX_VALUE : parseDate(rhs.getDepature()) == null ? Integer.MIN_VALUE : parseDate(rhs.getDepature()).compareTo(parseDate(lhs.getDepature()));

            }
        });

        if (flightListModels != null && flightListModels.size() != 0) {

            flightListAdapter = new FlightListAdapter(FlightListOnWayActivity.this, flightListModels, sourceCode, destinationCode, "\n" + dateOfDep, noOfAdult, noOfChild, noOfInfant, flightClass, jsonToSend, jsonFareToSend, citylist);

            recyclerView.setAdapter(flightListAdapter);
        }
    }

    public String compareDate(String depTime, String arrTime) {
        //HH converts hour in 24 hours format (0-23), day calculation
        @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("HH:mm");

        Date d1 = null;
        Date d2 = null;


        try {
            d1 = format.parse(depTime);
            d2 = format.parse(arrTime);

            long difference = d2.getTime() - d1.getTime();

            if (difference < 0) {
                Date dateMax = format.parse("24:00");
                Date dateMin = format.parse("00:00");
                difference = (dateMax.getTime() - d1.getTime()) + (d2.getTime() - dateMin.getTime());
            }
            int days = (int) (difference / (1000 * 60 * 60 * 24));
            int hours = (int) ((difference - (1000 * 60 * 60 * 24 * days)) / (1000 * 60 * 60));
            int min = (int) (difference - (1000 * 60 * 60 * 24 * days) - (1000 * 60 * 60 * hours)) / (1000 * 60);

            Calendar cal = Calendar.getInstance();
            cal.setTime(d1);
            cal.add(Calendar.HOUR, (int) hours);
            cal.add(Calendar.MINUTE, (int) min);
            String newTime = format.format(cal.getTime());
            if (days >= 1) {
                String date2 = String.valueOf(days + "d" + hours + "h" + min + "m");
                return date2.replace("-", "");
            } else {
                String date2 = String.valueOf(hours + " h" + min + " m");
                return date2.replace("-", "");
            }
//            }
            //in milliseconds


        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }


}
