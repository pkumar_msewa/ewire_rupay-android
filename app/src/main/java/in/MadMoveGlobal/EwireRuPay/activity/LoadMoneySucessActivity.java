package in.MadMoveGlobal.EwireRuPay.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
//import com.ebs.android.sdk.PaymentActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;

/**
 * Created by Ksf on 9/22/2016.
 */
public class LoadMoneySucessActivity extends AppCompatActivity {
    String payment_id;
    String PaymentId;
    String AccountId;
    String MerchantRefNo;
    String Amount;
    String DateCreated;
    String Description;
    String Mode;
    String IsFlagged;
    String BillingName;
    String BillingAddress;
    String BillingCity;
    String BillingState;
    String BillingPostalCode;
    String BillingCountry;
    String BillingPhone;
    String BillingEmail;
    String DeliveryName;
    String DeliveryAddress;
    String DeliveryCity;
    String DeliveryState;
    String DeliveryPostalCode;
    String DeliveryCountry;
    String DeliveryPhone;
    String PaymentStatus;
    String PaymentMode;
    String SecureHash;

    //Views and Dialog
    private LinearLayout llPaymentFailure;
    private Button btnPaymentSuccess, btnPaymentCancel, btnPaymentRetry;
    private TextView tvPaymentStatus;
    private LoadingDialog loadingDialog;
    private String type;
    SharedPreferences loadMoneyPref;

    private UserModel session = UserModel.getInstance();
    private String tag_json_obj = "load_money";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load_money_success);
        loadingDialog = new LoadingDialog(LoadMoneySucessActivity.this);

        llPaymentFailure = (LinearLayout) findViewById(R.id.llPaymentFailure);
        btnPaymentSuccess = (Button) findViewById(R.id.btnPaymentSuccess);
        btnPaymentCancel = (Button) findViewById(R.id.btnPaymentCancel);
        btnPaymentRetry = (Button) findViewById(R.id.btnPaymentRetry);
        tvPaymentStatus = (TextView) findViewById(R.id.tvPaymentStatus);

        loadMoneyPref = getSharedPreferences("type", Context.MODE_PRIVATE);
        type = loadMoneyPref.getString("type", "");
        Intent intent = getIntent();
        payment_id = intent.getStringExtra("payment_id");
        getJsonReport();

        btnPaymentSuccess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendFinish();
            }
        });

        btnPaymentRetry.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
//                Intent i = new Intent(getApplicationContext(), PaymentActivity.class);
//                LoadMoneySucessActivity.this.finish();
//                startActivity(i);
//
            }
        });
        btnPaymentCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                LoadMoneySucessActivity.this.finish();
                sendFinish();
            }
        });
    }

    private void getJsonReport() {
        String response = payment_id;
        JSONObject jObject;
        try {
            jObject = new JSONObject(response.toString());
            PaymentId = jObject.getString("PaymentId");
            AccountId = jObject.getString("AccountId");
            MerchantRefNo = jObject.getString("MerchantRefNo");
            Amount = jObject.getString("Amount");
            DateCreated = jObject.getString("DateCreated");
            Description = jObject.getString("Description");
            Mode = jObject.getString("Mode");
            IsFlagged = jObject.getString("IsFlagged");
            BillingName = jObject.getString("BillingName");
            BillingAddress = jObject.getString("BillingAddress");
            BillingCity = jObject.getString("BillingCity");
            BillingState = jObject.getString("BillingState");
            BillingPostalCode = jObject.getString("BillingPostalCode");
            BillingCountry = jObject.getString("BillingCountry");
            BillingPhone = jObject.getString("BillingPhone");
            BillingEmail = jObject.getString("BillingEmail");
            DeliveryName = jObject.getString("DeliveryName");
            DeliveryAddress = jObject.getString("DeliveryAddress");
            DeliveryCity = jObject.getString("DeliveryCity");
            DeliveryState = jObject.getString("DeliveryState");
            DeliveryPostalCode = jObject.getString("DeliveryPostalCode");
            DeliveryCountry = jObject.getString("DeliveryCountry");
            DeliveryPhone = jObject.getString("DeliveryPhone");
            PaymentStatus = jObject.getString("PaymentStatus");
            PaymentMode = jObject.getString("PaymentMode");
            SecureHash = jObject.getString("SecureHash");


            if (PaymentStatus.equalsIgnoreCase("failed")) {
                tvPaymentStatus.setText("");
                btnPaymentSuccess.setVisibility(View.GONE);
                llPaymentFailure.setVisibility(View.GONE);
//                addingResultToView(false);
                callVerifyPayment("0");


            } else {
                tvPaymentStatus.setText("");
                btnPaymentSuccess.setVisibility(View.GONE);
                llPaymentFailure.setVisibility(View.GONE);
                //VerifyTransactionIsSuccess
                callVerifyPayment("1");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void addingResultToView(boolean isSuccess) {
        TableLayout table_payment = (TableLayout) findViewById(R.id.tlResponse);
        ArrayList<String> arrlist = new ArrayList<>();
        arrlist.add("Payment Id");
        arrlist.add("MerchantRef No");
        arrlist.add("Amount");
        arrlist.add("Date Created");
        arrlist.add("Is Flagged");
        arrlist.add("Name");
        arrlist.add("Billing Country");
        arrlist.add("Phone");
        arrlist.add("Email");
        arrlist.add("Payment Status");
        arrlist.add("Payment Mode");

        ArrayList<String> arrlist1 = new ArrayList<>();
        arrlist1.add(PaymentId);
        arrlist1.add(MerchantRefNo);
        arrlist1.add(Amount);
        arrlist1.add(DateCreated);
        arrlist1.add(IsFlagged);
        arrlist1.add(BillingName);
        arrlist1.add(BillingCountry);
        arrlist1.add(BillingPhone);
        arrlist1.add(BillingEmail);
        arrlist1.add(PaymentStatus);
        arrlist1.add(PaymentMode);

        for (int i = 0; i < arrlist.size(); i++) {
            TableRow row = new TableRow(this);

            TextView textH = new TextView(this);
            TextView textC = new TextView(this);
            TextView textV = new TextView(this);

            textH.setText(arrlist.get(i));
            textC.setText(":  ");
            textV.setText(arrlist1.get(i));
            textV.setTypeface(null, Typeface.BOLD);

            row.addView(textH);
            row.addView(textC);
            row.addView(textV);

            table_payment.addView(row);
        }

        if (isSuccess) {

            tvPaymentStatus.setText("Load Money Successful.");
            btnPaymentSuccess.setVisibility(View.VISIBLE);
            llPaymentFailure.setVisibility(View.GONE);
        } else {
            tvPaymentStatus.setText("Load Money Failed.");
            btnPaymentSuccess.setVisibility(View.GONE);
            llPaymentFailure.setVisibility(View.VISIBLE);
        }
    }


    public void callVerifyPayment(final String respCode) {

        loadingDialog.show();
        StringRequest postReq = new StringRequest(Request.Method.POST, ApiUrl.URL_VERIFY_TRANSACTION_EBS_KIT, new Response.Listener<String>() {
            @Override
            public void onResponse(String stringResponse) {
                try {


                    JSONObject response = new JSONObject(stringResponse);

                    boolean isSuccess = response.getBoolean("success");
                    if (isSuccess) {

                        addingResultToView(isSuccess);
                        loadingDialog.dismiss();
                        sendRefresh();


                    } else {
                        addingResultToView(isSuccess);
                        loadingDialog.dismiss();
                        sendRefresh();
                    }
                } catch (JSONException e) {
                    loadingDialog.dismiss();
                    e.printStackTrace();
                    CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception2));

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loadingDialog.dismiss();
                CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception));
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("responseCode", respCode);
                params.put("responseMessage", "Ok");
                params.put("dateCreated", DateCreated);
                params.put("paymentId", PaymentId);
                params.put("merchantRefNo", MerchantRefNo);
                params.put("mode", Mode);
                params.put("billingName", BillingName);
                params.put("billingState", BillingState);
                params.put("billingPostalCode", BillingPostalCode);
                params.put("billingCountry", BillingCountry);
                params.put("billingPhone", BillingPhone);
                params.put("billingEmail", BillingEmail);
                params.put("deliveryName", DeliveryName);
                params.put("deliveryAddress", DeliveryAddress);

                params.put("deliveryCity", DeliveryCity);
                params.put("deliveryState", DeliveryState);
                params.put("deliveryPostalCode", DeliveryPostalCode);
                params.put("deliveryCountry", DeliveryCountry);
                params.put("deliveryPhone", DeliveryPhone);
                params.put("isFlagged", IsFlagged);
                params.put("secureHash", SecureHash);
                params.put("accountId", AccountId);

                return params;
            }

        };

        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);
        PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }

    private void sendRefresh() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "1");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }


    private void sendFinish() {
        Intent intent = new Intent("loadMoney-done");
        intent.putExtra("result", "1");
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
        LoadMoneySucessActivity.this.finish();
    }


}
