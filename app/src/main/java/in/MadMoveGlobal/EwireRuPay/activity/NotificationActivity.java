package in.MadMoveGlobal.EwireRuPay.activity;

import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.MadMoveGlobal.adapter.NotificationListAdapter;
import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.model.NotificationModel;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.util.NetworkErrorHandler;

/**
 * Created by kashifimam on 21/02/17.
 */

public class NotificationActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private ImageButton ivBackBtn;
    private LoadingDialog loadDlg;
    private RecyclerView rvNotificationsList;
    private LinearLayout llpbNotificationsList;
    private UserModel session = UserModel.getInstance();
    private String tag_json_obj = "json_events";
    private JsonObjectRequest postReq;
    private NotificationListAdapter itemAdp;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        LocalBroadcastManager.getInstance(this).registerReceiver(mCartReceiver, new IntentFilter("gift-cart-changed"));

        setContentView(R.layout.activity_notifications);
        toolbar = (Toolbar) findViewById(R.id.toolbars);
        loadDlg = new LoadingDialog(this);
        setSupportActionBar(toolbar);
        rvNotificationsList = (RecyclerView) findViewById(R.id.rvNotificationsList);
        llpbNotificationsList = (LinearLayout) findViewById(R.id.llpbNotificationsList);
        ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtns);
        ivBackBtn.setVisibility(View.VISIBLE);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        getCardCategory();
    }

    public void getCardCategory() {
        llpbNotificationsList.setVisibility(View.VISIBLE);
        rvNotificationsList.setVisibility(View.GONE);
        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (jsonRequest != null) {
            Log.i("NOTIFYURL", ApiUrl.URL_FETCH_NOTIFICATIONS);
            Log.i("NOTIFYREQ", jsonRequest.toString());
            postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_FETCH_NOTIFICATIONS, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObj) {
                    try {
                        Log.i("NOTIFYRES", jsonObj.toString());
                        String code = jsonObj.getString("code");

                        if (code != null && code.equals("S00")) {
                            List<NotificationModel> notifyListItemArray = new ArrayList<>();
//                            String jsonStringArray = jsonObj.getString("details");
//                            JSONObject jsonObjDetails = new JSONObject(jsonStringArray);
                            JSONArray notifyListArray = jsonObj.getJSONArray("details");

                            for (int i = 0; i < notifyListArray.length(); i++) {
                                JSONObject c = notifyListArray.getJSONObject(i);
                                String message = c.getString("message");
                                String date = c.getString("date");

                                NotificationModel notificationModel = new NotificationModel(message,date);
                                notifyListItemArray.add(notificationModel);

                            }
                            if (notifyListItemArray != null && notifyListItemArray.size() != 0) {
                                int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.recycle_space_double);
                                rvNotificationsList.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
                                GridLayoutManager manager = new GridLayoutManager(getApplicationContext(), 1);
                                rvNotificationsList.setLayoutManager(manager);
                                rvNotificationsList.setHasFixedSize(true);

                                itemAdp = new NotificationListAdapter(notifyListItemArray, NotificationActivity.this);
                                rvNotificationsList.setAdapter(itemAdp);
                                llpbNotificationsList.setVisibility(View.GONE);
                                rvNotificationsList.setVisibility(View.VISIBLE);

                            } else {
                                Toast.makeText(getApplicationContext(), "Empty Array", Toast.LENGTH_SHORT).show();
                                llpbNotificationsList.setVisibility(View.GONE);
                                rvNotificationsList.setVisibility(View.VISIBLE);
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        llpbNotificationsList.setVisibility(View.GONE);
                        rvNotificationsList.setVisibility(View.VISIBLE);
                        Toast.makeText(getApplicationContext(), "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    llpbNotificationsList.setVisibility(View.GONE);
                    rvNotificationsList.setVisibility(View.VISIBLE);
                    CustomToast.showMessage(NotificationActivity.this, NetworkErrorHandler.getMessage(error, NotificationActivity.this));
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        public SpacesItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view,
                                   RecyclerView parent, RecyclerView.State state) {
            outRect.left = space;
            outRect.right = space;
            outRect.bottom = space;
            outRect.top = space;
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.shopping_menu, menu);
//        MenuItem menuItem = menu.findItem(R.id.menuCart);
//
//        menuItem.setIcon(buildCounterDrawable(gIftCart.getProductsInCartArray().size(), R.drawable.shopping_cart));
//        return super.onCreateOptionsMenu(menu);
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        int id = item.getItemId();
//        if (id == R.id.menuCart) {
//            startActivity(new Intent(GiftCardListActivity.this, GiftCartListActivity.class));
//        }
//        return super.onOptionsItemSelected(item);
//    }
//
//    private Drawable buildCounterDrawable(int count, int backgroundImageId) {
//        LayoutInflater inflater = LayoutInflater.from(this);
//        View view = inflater.inflate(R.layout.layout_cart_icon, null);
//        view.setBackgroundResource(backgroundImageId);
//
//        if (count == 0) {
//            View counterTextPanel = view.findViewById(R.id.cartCount);
//            counterTextPanel.setVisibility(View.GONE);
//        } else {
//            TextView textView = (TextView) view.findViewById(R.id.cartCount);
//            textView.setText("" + count);
//        }
//
//        view.measure(
//                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
//                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
//        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
//
//        view.setDrawingCacheEnabled(true);
//        view.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
//        Bitmap bitmap = Bitmap.createBitmap(view.getDrawingCache());
//        view.setDrawingCacheEnabled(false);
//
//        return new BitmapDrawable(getResources(), bitmap);
//    }
//
//    private BroadcastReceiver mCartReceiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            invalidateOptionsMenu();
//
//        }
//    };
//
//
//    @Override
//    protected void onDestroy() {
//        LocalBroadcastManager.getInstance(this).unregisterReceiver(mCartReceiver);
//        super.onDestroy();
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        getCardCategory();
//    }
}
