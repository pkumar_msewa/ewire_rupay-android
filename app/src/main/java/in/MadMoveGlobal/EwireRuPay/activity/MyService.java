package in.MadMoveGlobal.EwireRuPay.activity;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.util.NetworkErrorHandler;


public class MyService extends IntentService {

    String stringPassedToThisService;

    public MyService() {
        super("Test the service");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        stringPassedToThisService = intent.getStringExtra("test");
        Log.i("values", stringPassedToThisService.toString());
        if (stringPassedToThisService != null) {
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(stringPassedToThisService);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_REFER_AND_EARN, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject responses) {

                }
            }, new Response.ErrorListener()

            {
                @Override
                public void onErrorResponse(VolleyError error) {
                    CustomToast.showMessage(getApplicationContext(), NetworkErrorHandler.getMessage(error, getApplicationContext()));
                }
            })

            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "12345");
                    return map;
                }

            };
            int socketTimeout = 600000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().

                    addToRequestQueue(postReq, "json");
        }
    }
}



