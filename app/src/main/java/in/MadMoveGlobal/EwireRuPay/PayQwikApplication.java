package in.MadMoveGlobal.EwireRuPay;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.support.multidex.MultiDex;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
//import com.expletus.mobiruck.MobiruckSdk;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.StandardExceptionParser;
import com.google.android.gms.analytics.Tracker;
//import com.msewa.pqgeolocation.activity.PQApplication;
import com.orm.SugarContext;
import com.zoho.commons.LauncherModes;
import com.zoho.commons.LauncherProperties;
import com.zoho.livechat.android.MbedableComponent;
import com.zoho.salesiqembed.ZohoSalesIQ;
//import com.zoho.livechat.android.MbedableComponent;
//import com.zoho.salesiqembed.ZohoSalesIQ;
//import com.zoho.livechat.android.MbedableComponent;
//import com.zoho.salesiqembed.ZohoSalesIQ;

import in.MadMoveGlobal.EwireRuPay.activity.MainActivity;
import io.fabric.sdk.android.Fabric;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;


import javax.net.ssl.SSLSocketFactory;

import in.MadMoveGlobal.util.AnalyticsTrackers;
import in.MadMoveGlobal.util.TLSSocketFactory;


/**
 * Created by DPS on 4/24/2018.
 */
public class PayQwikApplication extends Application {

    public static final String TAG = PayQwikApplication.class.getSimpleName();

    private static PayQwikApplication mInstance;
    private RequestQueue mRequestQueue;


    //Mark:- Mera Events

    String repoUrl = "http://s3-ap-southeast-1.amazonaws.com/repo.appsfly.io";
    String projectId = "58bc25a17639fc0010a6fa63";// Project under which MicroApp is created
    String microAppId = "591fe608a47fdb0011b09f41";// MicroApp Id
    String associationKey = "9b4874c6-4e05-4bf2-9e33-6e58f9928693"; // Association key created on the dashboard


    private static GoogleAnalytics sAnalytics;
    private static Tracker sTracker;

    @Override
    public void onCreate() {
        super.onCreate();
//        ZohoSalesIQ.init(this, "oK28J%2BCX8OhaqWXFf3UwX21OPnGi0KIpremUyHBt7ilHWyHvdo2vsf45rut%2FXM4j", "7SoPb1HVk5zfO4ZDnVYw3kBD8ZSkqOnMhJfBXsenq7rYHga5HOMyonQfsblnhd45snm%2FBrO1iNEIuKe9Ae%2FXraT0nLe5H24lGcmu2DI8yb%2BmJzCTEOfGppARWdMlF7oyT6w7y%2BMAyqVJlFIXY4yciBWX75UgJ%2FToGEU0%2F7NqXs4%3D");

        ZohoSalesIQ.init(this, "oK28J%2BCX8OgUuIwACAfa5w4mrIh4XtJXy3grcG8lQNo%3D", "7SoPb1HVk5zfO4ZDnVYw3k%2FGrmH9MJrXep9ov2ac8wgcfPE1oMqOh028XXv%2BNW9WgYggZ%2BB4r%2B%2B1738SXpCTgfxBJRrRSiCdUxA7WM%2FGc9vEFjvXkU1GnpfWFX8wfqwNjn6%2F8BezKYHCDZlodD65aqJkpUR3Srfc1Wl69H0PJb0%3D");

        Fabric.with(this, new Crashlytics());

        sAnalytics = GoogleAnalytics.getInstance(this);

//        Log.i("ANALITYCS", String.valueOf(sAnalytics));
//        MobiruckSdk.getInstance().init(getApplicationContext(), "e0dddc2339a406cd28bef8890a44ad90a2bc963dfacb2fb1b8fe8f9c30d631a6", true);

//        MobiruckSdk.getInstance().enableLog(true);  // this controlls log prints in sdk

//        MobiruckSdk.getInstance().startTracking();  // this starts the tracking system.



        //Mera Events
//        try {
//            ArrayList appsFlyClientConfigs = new ArrayList();
//            AppsFlyClientConfig appsflyConfig = new AppsFlyClientConfig(projectId, microAppId, associationKey, repoUrl);
//            appsFlyClientConfigs.add(appsflyConfig);
//            AppsFlyProvider.getInstance().initialize(appsFlyClientConfigs, this);
//        } catch (NoClassDefFoundError error) {
//            error.printStackTrace();
//        }


        SugarContext.init(this);
        mInstance = this;
        AnalyticsTrackers.initialize(this);
        AnalyticsTrackers.getInstance().get(AnalyticsTrackers.Target.APP);
//        PQApplication.mainUrl = "http://66.207.206.54:8041/GeoNotify/ws/api/v1/wallet/en/";
//        PQApplication.secretKey = "2DFF462E1DC9D398D456C8BC2B8AD518CF2F95FF";
//        PQApplication.tokenKey = "MTQ5OTgzODc3NDQzNg==";
//        PQApplication.bitmap = R.mipmap.ic_launcher;
//        PQApplication.appName = "Vpayqwik Test";
//        PQApplication.init(this);

         registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
                    try {
                        LauncherProperties launcherProperties = new LauncherProperties(LauncherModes.FLOATING);
                        ZohoSalesIQ.Chat.setVisibility(MbedableComponent.CHAT,activity instanceof MainActivity);
                        ZohoSalesIQ.setLauncherProperties(launcherProperties);
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
            }

            @Override
            public void onActivityStarted(Activity activity) {

            }

            @Override
            public void onActivityResumed(Activity activity) {

            }

            @Override
            public void onActivityPaused(Activity activity) {

            }

            @Override
            public void onActivityStopped(Activity activity) {

            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

            }

            @Override
            public void onActivityDestroyed(Activity activity) {

            }
        });
    }


    public static synchronized PayQwikApplication getInstance() {
        return mInstance;
    }

    synchronized public Tracker getDefaultTracker() {
        // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
        if (sTracker == null) {
            sTracker = sAnalytics.newTracker(R.xml.analytics);
        }

        return sTracker;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            try {
                final SSLSocketFactory sslSocketFactory = new TLSSocketFactory();
                mRequestQueue = Volley.newRequestQueue(getApplicationContext(), new HurlStack(null, sslSocketFactory));
            } catch (KeyManagementException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public synchronized Tracker getGoogleAnalyticsTracker() {
        AnalyticsTrackers analyticsTrackers = AnalyticsTrackers.getInstance();
        return analyticsTrackers.get(AnalyticsTrackers.Target.APP);
    }

    /***
     * Tracking screen view
     *
     * @param screenName screen name to be displayed on GA dashboard
     */
    public void trackScreenView(String screenName) {
        Tracker t = getGoogleAnalyticsTracker();

        // Set screen name.
        t.setScreenName(screenName);

        // Send a screen view.
        t.send(new HitBuilders.ScreenViewBuilder().build());

        GoogleAnalytics.getInstance(this).dispatchLocalHits();
    }

    /***
     * Tracking exception
     *
     * @param e exception to be tracked
     */
    public void trackException(Exception e) {
        if (e != null) {
            Tracker t = getGoogleAnalyticsTracker();

            t.send(new HitBuilders.ExceptionBuilder()
                    .setDescription(
                            new StandardExceptionParser(this, null)
                                    .getDescription(Thread.currentThread().getName(), e))
                    .setFatal(false)
                    .build()
            );
        }
    }

    /***
     * Tracking event
     *
     * @param category event category
     * @param action   action of the event
     * @param label    label
     */
    public void trackEvent(String category, String action, String label) {
        Tracker t = getGoogleAnalyticsTracker();

        // Build and send an Event.
        t.send(new HitBuilders.EventBuilder().setCategory(category).setAction(action).setLabel(label).build());
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        SugarContext.terminate();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

}
