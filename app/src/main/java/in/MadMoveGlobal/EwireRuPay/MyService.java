//package in.msspay.paulpay;
//
//import android.app.NotificationManager;
//import android.app.PendingIntent;
//import android.app.Service;
//import android.bluetooth.BluetoothAdapter;
//import android.bluetooth.BluetoothDevice;
//import android.bluetooth.BluetoothManager;
//import android.content.Context;
//import android.content.Intent;
//import android.location.Location;
//import android.location.LocationListener;
//import android.location.LocationManager;
//import android.media.RingtoneManager;
//import android.os.Bundle;
//import android.os.Handler;
//import android.os.IBinder;
//import android.os.RemoteException;
//import android.support.v4.app.NotificationCompat;
//import android.support.v7.widget.RecyclerView;
//import android.util.Log;
//import android.widget.Toast;
//
//import com.google.android.gms.common.ConnectionResult;
//import com.google.android.gms.common.api.GoogleApiClient;
//
//import org.altbeacon.beacon.Beacon;
//import org.altbeacon.beacon.BeaconConsumer;
//import org.altbeacon.beacon.BeaconManager;
//import org.altbeacon.beacon.BeaconParser;
//import org.altbeacon.beacon.MonitorNotifier;
//import org.altbeacon.beacon.RangeNotifier;
//import org.altbeacon.beacon.Region;
//
//import java.util.ArrayList;
//import java.util.Collection;
//import java.util.List;
//
//import in.msspay.beacon.BeaconActivity;
//import in.msspay.beacon.BeaconWebview;
//import in.msspay.model.MainMenuModel;
//import in.msspay.model.UserModel;
//import io.ap1.proximity.lib.AP1Manager;
//import io.ap1.proximity.lib.managers.scanner.AP1ScanObserver;
//import io.ap1.proximity.lib.managers.scanner.AP1ScannerManager;
//import io.ap1.proximity.lib.managers.scanner.config.AP1BeaconParser;
//import io.ap1.proximity.lib.managers.scanner.config.AP1Region;
//import io.ap1.proximity.lib.managers.scanner.config.Parser;
//import io.ap1.proximity.lib.managers.services.AP1Authentication;
//import io.ap1.proximity.lib.managers.services.AP1BeaconService;
//import io.ap1.proximity.lib.managers.services.callback.action.AP1Action;
//import io.ap1.proximity.lib.managers.services.callback.action.AP1Error;
//import io.ap1.proximity.lib.managers.services.model.AP1Beacon;
//import io.ap1.proximity.lib.managers.services.model.AP1Response;
//import timber.log.Timber;
//
//import static android.media.RingtoneManager.getDefaultUri;
//
//public class MyService extends Service implements BeaconConsumer, AP1ScanObserver, GoogleApiClient.ConnectionCallbacks,
//        GoogleApiClient.OnConnectionFailedListener, LocationListener {
//
//    private static final String LOG_TAG = "BeaconActivity";
//    private static final int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 21312;
//    private static final String TAG = "test";
//    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
//
//    private BluetoothManager btManager;
//    private BluetoothAdapter btAdapter;
//    private Handler scanHandler = new Handler();
//    private int scan_interval_ms = 5000;
//    private boolean isScanning = false;
//    private BluetoothManager bluetoothManager;
//    private AP1Manager.Builder builder;
//    private BeaconManager beaconManager;
//    private RecyclerView listView;
//    private ArrayList<Beacon> data;
//    //   private ArrayAdapter<String> adapter;
//    private Integer UserID;
//    private AP1BeaconService ap1BeaconService;
//    private List<BeaconParser> parsers;
//    private String hash;
//    BeaconActivity.RecyclerAdapter recyclerAdapter;
//
//    private static final int REQUEST_RESOLVE_ERROR = 100;
//    private static final int REQUEST_PERMISSION = 42;
//
//    private GoogleApiClient mGoogleApiClient;
//    private String provider;
//    private Location location1;
//    private Integer beaconId;
//    ArrayList<MainMenuModel> beaconItems;
//
//    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters
//    private static final int TWO_MINUTES = 100 * 10;
//
//    // The minimum time between updates in milliseconds
//    private static final long MIN_TIME_BW_UPDATES = 1000 * 10; // 30 seconds
//
//    private Context context;
//
//    double latitude;
//    double longitude;
//
//    Location location = null;
//    boolean isGPSEnabled = false;
//    boolean isNetworkEnabled = false;
//    protected LocationManager locationManager;
//    UserModel userModel = UserModel.getInstance();
//    Handler handler;
//
//    public MyService() {
//    }
//
//    @Override
//    public int onStartCommand(Intent intent, int flags, int startId) {
//
//        this.context = this;
//        get_current_location();
//        return START_STICKY;
//    }
//
//    @Override
//    public void onCreate() {
//        handler = new Handler();
//        super.onCreate();
//        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
//        if (!mBluetoothAdapter.isEnabled()) {
//            mBluetoothAdapter.enable();
//        }
//
//        builder = new AP1Manager.Builder(getApplication());
//        builder.apiKey("b0b3d6600adf859fd6454a6b050b71df");
//        builder.baseUrl("http://api.apengage.io/proximity/");
//        builder.addScanObserver(this);
//        builder.build();
//        ap1Manager = AP1Manager.ScannerManager;
//        beaconManager = AP1Manager.getBeaconManager();
//        AP1Authentication ap1Authentication = AP1Authentication.getInstance();
//        ap1Authentication.register(userModel.getUserEmail(), "123456", new AP1Action<String>() {
//            @Override
//            public void response(String s) {
////                Toast.makeText(BeaconActivity.this, s, Toast.LENGTH_LONG).show();
//
//            }
//        }, new AP1Action<AP1Error>() {
//            @Override
//            public void response(AP1Error ap1Error) {
////                Toast.makeText(BeaconActivity.this, ap1Error.getMessage(), Toast.LENGTH_LONG).show();
//            }
//        });
//
//
//        ap1Authentication.login(userModel.getUserEmail(), "123456", new AP1Action<Integer>() {
//            @Override
//            public void response(Integer integer) {
//                UserID = integer;
//
//            }
//        }, new AP1Action<AP1Error>() {
//            @Override
//            public void response(AP1Error ap1Error) {
//
//
//            }
//        });
//
////        beaconprogressbar = (GifImageView) findViewById(R.id.beaconProgress);
//
//
//        bluetoothManager = (BluetoothManager) getApplicationContext().getSystemService(Context.BLUETOOTH_SERVICE);
//        btAdapter = bluetoothManager.getAdapter();
//        btAdapter.enable();
//        if (btAdapter != null) {
//            btAdapter.startLeScan(leScanCallback);
//        }
//
//
//        beaconManager.getBeaconParsers().add(new BeaconParser().
//                setBeaconLayout(AP1BeaconParser.EDDYSTONE_UID_LAYOUT));
//        // Detect the telemetry Eddystone-TLM frame:
//        beaconManager.getBeaconParsers().add(new BeaconParser().
//                setBeaconLayout(AP1BeaconParser.EDDYSTONE_TLM_LAYOUT));
//        beaconManager.getBeaconParsers().add(new BeaconParser().
//                setBeaconLayout(AP1BeaconParser.URI_BEACON_LAYOUT));
//
//        ap1BeaconService = AP1BeaconService.getInstance();
//
//    }
//
//    //}
//    void getBeacon() {
//        builder = new AP1Manager.Builder(getApplication());
//        builder.apiKey("b0b3d6600adf859fd6454a6b050b71df");
//        builder.baseUrl("http://api.apengage.io/proximity/");
//        builder.addScanObserver(this);
//        builder.build();
//        ap1Manager = AP1Manager.ScannerManager;
//        beaconManager = AP1Manager.getBeaconManager();
//        AP1Authentication ap1Authentication = AP1Authentication.getInstance();
//        ap1Authentication.register("jakka.santosh@gmail.com", "123456", new AP1Action<String>() {
//            @Override
//            public void response(String s) {
////                Toast.makeText(BeaconActivity.this, s, Toast.LENGTH_LONG).show();
//
//            }
//        }, new AP1Action<AP1Error>() {
//            @Override
//            public void response(AP1Error ap1Error) {
////                Toast.makeText(BeaconActivity.this, ap1Error.getMessage(), Toast.LENGTH_LONG).show();
//            }
//        });
//
//
//        ap1Authentication.login("jakka.santosh@gmail.com", "123456", new AP1Action<Integer>() {
//            @Override
//            public void response(Integer integer) {
//                UserID = integer;
//
//            }
//        }, new AP1Action<AP1Error>() {
//            @Override
//            public void response(AP1Error ap1Error) {
//
//
//            }
//        });
//
////        beaconprogressbar = (GifImageView) findViewById(R.id.beaconProgress);
//
//
//        bluetoothManager = (BluetoothManager) getApplicationContext().getSystemService(Context.BLUETOOTH_SERVICE);
//        btAdapter = bluetoothManager.getAdapter();
//        btAdapter.enable();
//        if (btAdapter != null) {
//            btAdapter.startLeScan(leScanCallback);
//        }
//
//
//        beaconManager.getBeaconParsers().add(new BeaconParser().
//                setBeaconLayout(AP1BeaconParser.EDDYSTONE_UID_LAYOUT));
//        // Detect the telemetry Eddystone-TLM frame:
//        beaconManager.getBeaconParsers().add(new BeaconParser().
//                setBeaconLayout(AP1BeaconParser.EDDYSTONE_TLM_LAYOUT));
//        beaconManager.getBeaconParsers().add(new BeaconParser().
//                setBeaconLayout(AP1BeaconParser.URI_BEACON_LAYOUT));
//
//        ap1BeaconService = AP1BeaconService.getInstance();
//
//    }
//
//    @Override
//    public IBinder onBind(Intent intent) {
//        // TODO: Return the communication channel to the service.
//        throw new UnsupportedOperationException("Not yet implemented");
//    }
//
//    @Override
//    public void enterRange(AP1Beacon ap1Beacon) {
//
//    }
//
//    @Override
//    public void exitRange(AP1Beacon ap1Beacon, long l, long l1) {
//
//    }
//
//    @Override
//    public void onChange(AP1Beacon ap1Beacon) {
//
//    }
//
//    private String UUID;
//    private AP1ScannerManager ap1Manager;
//    private AP1Region ap1Region;
//    private BluetoothAdapter.LeScanCallback leScanCallback = new BluetoothAdapter.LeScanCallback() {
//        @Override
//        public void onLeScan(final BluetoothDevice device, final int rssi, final byte[] scanRecord) {
//            int startByte = 2;
//            boolean patternFound = false;
//            while (startByte <= 5) {
//                if (((int) scanRecord[startByte + 2] & 0xff) == 0x02 && //Identifies an iBeacon
//                        ((int) scanRecord[startByte + 3] & 0xff) == 0x15) { //Identifies correct data length
//                    patternFound = true;
//                    break;
//                }
//                startByte++;
//            }
//
//            if (patternFound) {
//                //Convert to hex String
//                byte[] uuidBytes = new byte[16];
//                System.arraycopy(scanRecord, startByte + 4, uuidBytes, 0, 16);
//                String hexString = bytesToHex(uuidBytes);
//
//                //UUID detection
//                final String uuid = hexString.substring(0, 8) + "-" +
//                        hexString.substring(8, 12) + "-" +
//                        hexString.substring(12, 16) + "-" +
//                        hexString.substring(16, 20) + "-" +
//                        hexString.substring(20, 32);
//
//                // major
//                final int major = (scanRecord[startByte + 20] & 0xff) * 0x100 + (scanRecord[startByte + 21] & 0xff);
//
//                // minor
//                final int minor = (scanRecord[startByte + 22] & 0xff) * 0x100 + (scanRecord[startByte + 23] & 0xff);
//
//                Log.i(LOG_TAG, "UUID: " + uuid + "\\nmajor: " + major + "\\nminor" + minor);
//
//                final int finalStartByte = startByte;
//
//                Thread thread = new Thread() {
//                    @Override
//                    public void run() {
//                        try {
//                            Thread.sleep(1);
//                        } catch (InterruptedException e) {
//                            e.printStackTrace();
//                        }
//                        handler.post(new Runnable() {
//                            @Override
//                            public void run() {
//                                Log.i("UUID", String.valueOf(major));
//                                Log.i("UUID", String.valueOf(uuid));
//                                Log.i("UUID", String.valueOf(minor));
//
//                                ap1Region = new AP1Region("beacon", uuid, major, minor);
//                                Log.i("service", String.valueOf(ap1Region.getId1()));
//
//
//                                builder.addRegion(new AP1Region("beacon", uuid, major, minor));
//                                builder.beaconType(Parser.EDDYSTONE);
//                                builder.addScanObserver(MyService.this);
//                                builder.build();
//                                btAdapter.stopLeScan(leScanCallback);
//
//                                beaconManager.bind(MyService.this);
//                                ap1Manager.startScanning(MyService.this);
//                                ap1BeaconService = AP1BeaconService.getInstance();
//                            }
//                        });
//
//
//                    }
//                };
//                thread.start();
//            }
//        }
//    };
//
//
//    /**
//     * bytesToHex method
//     */
//    static final char[] hexArray = "0123456789ABCDEF".toCharArray();
//
//    private static String bytesToHex(byte[] bytes) {
//        char[] hexChars = new char[bytes.length * 2];
//        for (int j = 0; j < bytes.length; j++) {
//            int v = bytes[j] & 0xFF;
//            hexChars[j * 2] = hexArray[v >>> 4];
//            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
//        }
//        return new String(hexChars);
//    }
//
//
//    @Override
//    public void onBeaconServiceConnect() {
//
//
//        beaconManager.setMonitorNotifier(new MonitorNotifier() {
//            @Override
//            public void didEnterRegion(Region region) {
//                try {
//                    Log.d(TAG, "didEnterRegion");
//
//                    beaconManager.startRangingBeaconsInRegion(region);
//                } catch (RemoteException e) {
//                    e.printStackTrace();
//                }
//            }
//
//            @Override
//            public void didExitRegion(Region region) {
//                try {
//                    Log.d(TAG, "didExitRegion");
//                    beaconManager.stopRangingBeaconsInRegion(region);
//                } catch (RemoteException e) {
//                    e.printStackTrace();
//                }
//            }
//
//            @Override
//            public void didDetermineStateForRegion(int i, Region region) {
//
//            }
//        });
//
//
//        beaconManager.setRangeNotifier(new RangeNotifier() {
//            @Override
//            public void didRangeBeaconsInRegion(Collection<Beacon> beacons, final Region region) {
//
//                ArrayList<Beacon> beacons1 = (ArrayList<Beacon>) beacons;
//
////                double latitude = Long.parseLong(beacons1.get.toString().substring(2), 16) / 10000.0 - 90.0;
////                double longitude = Long.parseLong(region.getId3().toString().substring(2), 16) / 10000.0 - 180.0;
////
//                for (final Beacon beacon : beacons) {
//
//                    if (beacon != null && beacon.getId1().toString().length() > 0) {
//                        ap1Manager.stopScanning(MyService.this);
//
//
//                        AP1Beacon.Builder ap1Beacon = new AP1Beacon.Builder();
//                        ap1Beacon.nickname("beacon");
//                        double latitude = Long.parseLong(beacon.getId2().toString().substring(2), 16) / 10000.0 - 90.0;
//                        double longitude = Long.parseLong(beacon.getId3().toString().substring(2), 16) / 10000.0 - 180.0;
////
//                        ap1Beacon.identifierId(String.valueOf(beacon.getId1()), String.valueOf(beacon.getId2()), String.valueOf(beacon.getId3()));
//                        ap1Beacon.location(String.valueOf(getLatitude()), String.valueOf(getLongitude()));
//                        ap1Beacon.locationId("301");
//
//                        AP1Beacon ap1Beacon1 = ap1Beacon.build();
//
//                        AP1BeaconService ap1BeaconService = AP1BeaconService.getInstance();
//                        ap1BeaconService.add(ap1Beacon1, new AP1Action<String>() {
//                            @Override
//                            public void response(String s) {
//                                Toast.makeText(MyService.this, "beacon" + s, Toast.LENGTH_LONG).show();
//                            }
//                        }, new AP1Action<AP1Error>() {
//                            @Override
//                            public void response(AP1Error ap1Error) {
//                                Toast.makeText(MyService.this, ap1Error.getMessage(), Toast.LENGTH_LONG).show();
//                            }
//                        });
//                    }
//                }
//
//                ap1BeaconService.fetch("", new AP1Action<AP1Response>() {
//                    @Override
//                    public void response(AP1Response ap1Response) {
//                        hash = ap1Response.getHash().toString();
//
//                    }
//                }, new AP1Action<AP1Error>() {
//                    @Override
//                    public void response(AP1Error ap1Error) {
//                        Log.i("reoore", ap1Error.getMessage());
//
//                    }
//                });
//
//                AP1Manager.BeaconService.fetch(hash, new AP1Action<AP1Response>() {
//                    @Override
//                    public void response(AP1Response ap1Response) {
//                        Timber.d("message:%s", ap1Response.toString());
//                        ArrayList arrayList = (ArrayList) ap1Response.getData();
//                        for (Object arrayList1 : arrayList) {
//
//                            AP1Beacon ap1Beacon = (AP1Beacon) arrayList1;
//                            beaconId = ap1Beacon.getId();
//                            Log.i("NotifyText", "" + ap1Beacon.getNotifytext());
//                            sendNotification(ap1Beacon);
//                        }
//
//                    }
//                }, new AP1Action<AP1Error>()
//
//                {
//                    @Override
//                    public void response(AP1Error ap1Error) {
//                        Log.i("reoore2", ap1Error.getMessage());
//
//                    }
//                });
//
////                updateList();
//            }
//        });
//
//
//    }
//
//
////    public void updateList() {
////        runOnUiThread(new Runnable() {
////            @Override
////            public void run() {
////                adapter.clear();
////                adapter.addAll(data);
////
////            }
////        });
////    }
//
//
//// Called when the publisher (beacon) is no longer nearby.
////        @Override
////        public void onLost(Message message) {
////            Log.i(TAG, "Lost message: " + message);
////            final OfferBeacon beacon = new OfferBeacon(message);
////            runOnUiThread(new Runnable() {
////                @Override
////                public void run() {
////                    mAdapter.remove(beacon);
////                }
////            });
////        }
////    };
//
//    /* API Client Callbacks */
//
//
////    public void getBeacon() {
////        builder = new AP1Manager.Builder(getApplication());
////        builder.apiKey("b0b3d6600adf859fd6454a6b050b71df");
////        builder.baseUrl("http://api.apengage.io/proximity/");
////        builder.addScanObserver(this);
////        builder.build();
////
////        /*mGoogleApiClient = new GoogleApiClient.Builder(this)
////                .addConnectionCallbacks(this)
////                .addOnConnectionFailedListener(this)
////                .addApi(Nearby.MESSAGES_API)
////                .build();*/
////
////        AP1Authentication ap1Authentication = AP1Authentication.getInstance();
////        ap1Authentication.register("jakka.santosh@gmail.com", "123456", new AP1Action<String>() {
////            @Override
////            public void response(String s) {
//////                Toast.makeText(BeaconActivity.this, s, Toast.LENGTH_LONG).show();
////
////            }
////        }, new AP1Action<AP1Error>() {
////            @Override
////            public void response(AP1Error ap1Error) {
//////                Toast.makeText(BeaconActivity.this, ap1Error.getMessage(), Toast.LENGTH_LONG).show();
////            }
////        });
////
////
////        ap1Authentication.login("jakka.santosh@gmail.com", "123456", new AP1Action<Integer>() {
////            @Override
////            public void response(Integer integer) {
////                UserID = integer;
////
////            }
////        }, new AP1Action<AP1Error>() {
////            @Override
////            public void response(AP1Error ap1Error) {
////
////
////            }
////        });
////
//////        beaconprogressbar = (GifImageView) findViewById(R.id.beaconProgress);
////
////        ap1Manager = AP1Manager.ScannerManager;
////        beaconManager = AP1Manager.getBeaconManager();
////        bluetoothManager = (BluetoothManager) getApplicationContext().getSystemService(Context.BLUETOOTH_SERVICE);
////        btAdapter = bluetoothManager.getAdapter();
////        if (btAdapter != null) {
////            btAdapter.startLeScan(leScanCallback);
////        }
////
////
////        beaconManager.getBeaconParsers().add(new BeaconParser().
////                setBeaconLayout(AP1BeaconParser.EDDYSTONE_UID_LAYOUT));
////        // Detect the telemetry Eddystone-TLM frame:
////        beaconManager.getBeaconParsers().add(new BeaconParser().
////                setBeaconLayout(AP1BeaconParser.EDDYSTONE_TLM_LAYOUT));
////        beaconManager.getBeaconParsers().add(new BeaconParser().
////                setBeaconLayout(AP1BeaconParser.URI_BEACON_LAYOUT));
////
////        ap1BeaconService = AP1BeaconService.getInstance();
////
//
////    }
//
//    @Override
//    public void onConnected(Bundle bundle) {
//        //Once connected, we have to check that the user has opted in
//        Runnable runOnSuccess = new Runnable() {
//            @Override
//            public void run() {
//                //Subscribe once user permission is verified
////                subscribe();
//            }
//        };
////        ResultCallback<Status> callback =
////                new ErrorCheckingCallback(runOnSuccess);
////        Nearby.Messages.getPermissionStatus(mGoogleApiClient)
////                .setResultCallback(callback);
//    }
//
//    @Override
//    public void onConnectionSuspended(int i) {
//        Log.d(TAG, "OnConnectionSuspended");
//    }
//
//    @Override
//    public void onConnectionFailed(ConnectionResult connectionResult) {
//        Log.w(TAG, "OnConnectionFailed");
//    }
//
//
//    @Override
//    public void onStatusChanged(String s, int i, Bundle bundle) {
//
//    }
//
//    @Override
//    public void onProviderEnabled(String s) {
//
//    }
//
//    @Override
//    public void onProviderDisabled(String s) {
//
//    }
//
//
////private class BackgroundRegisterCallback implements ResultCallback<Status> {
////    @Override
////    public void onResult(@NonNull Status status) {
////        //Validate if we were able to register for background scans
////        if (status.isSuccess()) {
////            Log.d(TAG, "Background Register Success!");
////        } else {
////            Log.w(TAG, "Background Register Error ("
////                    + status.getStatusCode() + "): "
////                    + status.getStatusMessage());
////        }
////    }
////
////}
//
////ResultCallback triggered when to handle Nearby permissions check
////private class ErrorCheckingCallback implements ResultCallback<Status> {
////    private final Runnable runOnSuccess;
////
////    private ErrorCheckingCallback(@Nullable Runnable runOnSuccess) {
////        this.runOnSuccess = runOnSuccess;
////    }
////
////    @Override
////    public void onResult(@NonNull Status status) {
////        if (status.isSuccess()) {
////            Log.i(TAG, "Permission status succeeded.");
////            if (runOnSuccess != null) {
////                runOnSuccess.run();
////            }
////        } else {
////            // Currently, the only resolvable error is that the device is not opted
////            // in to Nearby. Starting the resolution displays an opt-in dialog.
////            if (status.hasResolution()) {
////                try {
////                    status.startResolutionForResult(MyService.this,
////                            REQUEST_RESOLVE_ERROR);
////                } catch (IntentSender.SendIntentException e) {
////                    showToastAndLog(Log.ERROR, "Request failed with exception: " + e);
////                }
////            } else {
////                showToastAndLog(Log.ERROR, "Request failed with : " + status);
////            }
////        }
////    }
////
////}
//
//
//    private void showToast(String text) {
//        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
//    }
//
//    private void showToastAndLog(int logLevel, String message) {
//        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
//        Log.println(logLevel, TAG, message);
//    }
//
//
//    public void sendNotification(AP1Beacon ap1Beacon) {
//
//        Intent intent = new Intent(MyService.this, BeaconWebview.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        intent.putExtra("URL", ap1Beacon.getUrl());
//        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
//                PendingIntent.FLAG_ONE_SHOT);
//        NotificationCompat.Builder mBuilder =
//                new NotificationCompat.Builder(this)
//                        .setSmallIcon(R.mipmap.ic_launcher)
//                        .setContentTitle(ap1Beacon.getNotifytitle())
//                        .setContentText(ap1Beacon.getNotifytext())
//                        .setColor(getResources().getColor(R.color.text_dark))
//                        .setSound(getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
//                        .setContentIntent(pendingIntent)
//                        .setStyle(new NotificationCompat.BigTextStyle().bigText(ap1Beacon.getNotifytext()))
//                        .setAutoCancel(true);
//
//
//        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//
//
//        mNotificationManager.notify(001, mBuilder.build());
//    }
//
//    @Override
//    public void onLocationChanged(Location location) {
//        if ((location != null) && (location.getLatitude() != 0) && (location.getLongitude() != 0)) {
//
//            latitude = location.getLatitude();
//            longitude = location.getLongitude();
//
//        }
//
//    }
//
//
//    /*
//    *  Get Current Location
//    */
//    public Location get_current_location() {
//
//        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
//
//        isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
//
//        isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
//
//        if (!isGPSEnabled && !isNetworkEnabled) {
//
//
//        } else {
//            if (isGPSEnabled) {
//
//                if (location == null) {
//                    locationManager.requestLocationUpdates(
//                            LocationManager.GPS_PROVIDER,
//                            MIN_TIME_BW_UPDATES,
//                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
//
//                    if (locationManager != null) {
//                        location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
//                        if (location != null) {
//                            latitude = location.getLatitude();
//                            longitude = location.getLongitude();
//                            //                  Toast.makeText(context, "Latgps"+latitude+"long"+longitude,Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                }
//            }
//            if (isNetworkEnabled) {
//
//                locationManager.requestLocationUpdates(
//                        LocationManager.NETWORK_PROVIDER,
//                        MIN_TIME_BW_UPDATES,
//                        MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
//
//                if (locationManager != null) {
//
//                    if (location != null) {
//                        latitude = location.getLatitude();
//                        longitude = location.getLongitude();
//                        //              Toast.makeText(context, "Latgps1"+latitude+"long"+longitude,Toast.LENGTH_SHORT).show();
//                    }
//                }
//            }
//        }
//
//        return location;
//    }
//
//
//    public double getLatitude() {
//        if (location != null) {
//            latitude = location.getLatitude();
//        }
//        return latitude;
//    }
//
//    public double getLongitude() {
//        if (location != null) {
//            longitude = location.getLongitude();
//        }
//
//        return longitude;
//    }
//
//
//    @Override
//    public void onDestroy() {
//        if (locationManager != null) {
//            locationManager.removeUpdates(this);
//        }
//        super.onDestroy();
//    }
//
//}
