package in.MadMoveGlobal.EwireRuPay;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.androidquery.AQuery;
import com.orm.query.Select;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import in.MadMoveGlobal.adapter.OperatorSpinnerAdapter;
import in.MadMoveGlobal.custom.CustomAlertDialog;
import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.custom.LoadingDialog;
//import in.paulpay.fragment.LoadMoneyFragment;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.metadata.AppMetadata;
import in.MadMoveGlobal.metadata.MenuMetadata;
import in.MadMoveGlobal.model.OperatorsModel;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.EwireRuPay.activity.MainActivity;
import in.MadMoveGlobal.util.ImageLoadingUtils;
import in.MadMoveGlobal.util.MultipartRequest;
import in.MadMoveGlobal.util.NetworkErrorHandler;
import in.MadMoveGlobal.util.Utility;


public class EkycActivity extends AppCompatActivity {

    public static final String IMAGE_DIRECTORY_NAME = "PayQwik";
    private final int CameraResult = 1;
    private final int GalleryResult = 2;
    private EditText etAddress1, etAddress2, etCity, etState, etCountry, etPostalCode, etGovtIdNo;
    private RadioButton rbEditMale, rbEditFeMale;
    private Button btnEditSubmit;
    private ImageView ivEditProfilePic, ivEditProfilePic1;
    private String firstName, lastName, address, email, mobileNo, gender, dob;
    private LoadingDialog loadDlg;
    private TextInputLayout ilAddress1, ilAddress2, ilCity, ilState, ilCountry, ilPostalCode, ilId;
    private Button btnEditProfilePic;
    private UserModel session = UserModel.getInstance();
    private AlertDialog.Builder dialogPicture;
    private Uri fileUri;
    private String userImage, govtId;
    private boolean isimage1, isimage2;
    private boolean isImage = false;
    private Spinner spinner_govtid_provider;
    private OperatorSpinnerAdapter operatorSpinnerAdapter;
    private int postion;
    private TextView tvFront, tvBack1;
    private String blockCharacterSet = "~#^|$%&*`~@()_-+=|:;></.,'{}[]()!abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private ImageLoadingUtils utils;

    private Map<String, String> uploadHeaderParams;

    private String updatedImageUrl = null;

    private JSONObject jsonRequest;

    private Toolbar toolbar;
    private ImageButton ivBackBtn;

    private Bitmap btpUserImage;
    private String imageName = "";
    private String image1, image2;
    private static final String AUTHORITY = "com.example.alex.hopefulythisworks";

    private boolean isChanged = false;

    private String serviceProvider, serviceProviderName;

    //Volley Tag
    private String tag_json_obj = "json_user";
    private String userChoosenTask;
    private Runnable updateTask = new Runnable() {
        @Override
        public void run() {
            loadDlg.dismiss();
            finish();
        }
    };

    private String pathOfImg;
    //dushman
    private static final int CAMERA_PHOTO = 111;
    private Uri imageToUploadUri;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {

                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }


    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        utils = new ImageLoadingUtils(getApplicationContext());
        setContentView(R.layout.activity_ekyc_layout);
        loadDlg = new LoadingDialog(EkycActivity.this);


        requestDialog(genraterequestKyc());


        toolbar = (Toolbar) findViewById(R.id.toolbars);
        ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtns);
        setSupportActionBar(toolbar);
        ivBackBtn.setVisibility(View.VISIBLE);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mainIntent = new Intent(EkycActivity.this, MainActivity.class);
                startActivity(mainIntent);
                finish();
            }
        });


        etAddress1 = (EditText) findViewById(R.id.etAddress1);
        etPostalCode = (EditText) findViewById(R.id.etPostalCode);
        etCountry = (EditText) findViewById(R.id.etCountry);
        etCity = (EditText) findViewById(R.id.etCity);
        etAddress2 = (EditText) findViewById(R.id.etAddress2);
        btnEditProfilePic = (Button) findViewById(R.id.btnEditProfilePic);
        etState = (EditText) findViewById(R.id.etState);
        etGovtIdNo = (EditText) findViewById(R.id.etGovtIdNo);
        spinner_govtid_provider = (Spinner) findViewById(R.id.spinner_govtid_provider);

        rbEditMale = (RadioButton) findViewById(R.id.rbEditMale);
        rbEditFeMale = (RadioButton) findViewById(R.id.rbEditFeMale);


        btnEditSubmit = (Button) findViewById(R.id.btnEditSubmit);
        ilAddress1 = (TextInputLayout) findViewById(R.id.ilAddress1);
        ilAddress2 = (TextInputLayout) findViewById(R.id.ilAddress2);
        ilCity = (TextInputLayout) findViewById(R.id.ilCity);
        ilCountry = (TextInputLayout) findViewById(R.id.ilCountry);
        ilPostalCode = (TextInputLayout) findViewById(R.id.ilPostalCode);
        ivEditProfilePic = (ImageView) findViewById(R.id.ivEditProfilePic);
        ilState = (TextInputLayout) findViewById(R.id.ilState);
        ilId = (TextInputLayout) findViewById(R.id.ilId);
        tvFront = (TextView) findViewById(R.id.tvFront);
        ivEditProfilePic1 = (ImageView) findViewById(R.id.ivEditProfilePic1);
        tvBack1 = (TextView) findViewById(R.id.tvBack1);
        btnEditSubmit.setText("SAVE & PROCEED");

        Log.i("HASKYC", String.valueOf(session.isHaskycRequest()));

        etPostalCode.addTextChangedListener(new EkycActivity.MyTextWatcher(etPostalCode));


        if (session.getUserAcName() != null && session.isHaskycRequest()) {
            CustomToast.showMessage(EkycActivity.this, "Your KYC verification is underprocess");
            Intent mainIntent = new Intent(EkycActivity.this, MainActivity.class);
            startActivity(mainIntent);
            finish();
        }


        operatorSpinnerAdapter = new OperatorSpinnerAdapter(this, R.layout.spinners, MenuMetadata.getEkycId());
        spinner_govtid_provider.setAdapter(operatorSpinnerAdapter);

        loadDlg = new LoadingDialog(EkycActivity.this);

        spinner_govtid_provider.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                OperatorsModel operatorsModel = (OperatorsModel) adapterView.getSelectedItem();
                if (i != 0) {
                    postion = Integer.valueOf(operatorsModel.code);
                }

//                etGovtIdNo.setHint(operatorsModel.getName());
//                etGovtIdNo.setText("");
//                govtId = operatorsModel.getName();
                if (postion == 0) {
                    etGovtIdNo.setInputType(InputType.TYPE_CLASS_NUMBER);
                } else {
                    etGovtIdNo.setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
                }

//                aadhar.setVisibility(View.VISIBLE);
                etGovtIdNo.setHint(operatorsModel.getName());
                etGovtIdNo.setText("");
                govtId = operatorsModel.getName();

                if (postion == 1) {
                    showDLDialog(generateDLMessageDialog());
                } else if (postion == 2) {
                    showDLDialogs(generateVIMessageDialog());
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

//
        etGovtIdNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (govtId != null && govtId.equalsIgnoreCase("aadhaar")) {
                    if (charSequence.length() > 0) {
                        int length = etGovtIdNo.getText().toString().length();
                        if (i2 != 0) {
                            String str = "" + charSequence.charAt(i2 - 1);
                            if (blockCharacterSet.contains(str)) {
                                if (i2 > length - 1) {
                                    etGovtIdNo.setText(etGovtIdNo.getText().toString().substring(i, length - 1));
                                } else if (i2 - 1 == 0) {
                                    etGovtIdNo.setText(etGovtIdNo.getText().toString().substring(i + 1, length));
                                } else {
                                    String st = etGovtIdNo.getText().toString().substring(i, i2 - 1) + etGovtIdNo.getText().toString().substring(i2, length);
                                    etGovtIdNo.setText(st);
                                }
                            } else {
                                return;
                            }
                        }
                    }
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!validateValues(spinner_govtid_provider.getSelectedItemPosition())) {
                    return;
                }


            }
        });


        fillViews();


        btnEditSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitForm();
            }
        });


//        btnEditProfilePic.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View arg0) {
//                selectImage();
//
//            }
//        });


        ivEditProfilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isimage1 = true;
                isimage2 = false;
                selectImage(ivEditProfilePic);
            }
        });

        ivEditProfilePic1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isimage2 = true;
                isimage1 = false;
                selectImage(ivEditProfilePic1);
            }
        });


    }

    private void fillViews() {
        AQuery aq = new AQuery(EkycActivity.this);

    }

    private void submitForm() {

        if (!validatPinCode()) {
            return;
        } else if (!validateCity()) {
            return;
        } else if (!validateState()) {
            return;
        } else if (!validateAddressOne()) {
            return;
        } else if (!validateAddressTwo()) {
            return;
        }
        serviceProvider = ((OperatorsModel) spinner_govtid_provider.getSelectedItem()).code;
        serviceProviderName = ((OperatorsModel) spinner_govtid_provider.getSelectedItem()).defaultCode;
        if (spinner_govtid_provider.getSelectedItemPosition() == 0) {
            CustomToast.showMessage(this, "Please select any valid ID to continue");
            return;

        }
        serviceProvider = ((OperatorsModel) spinner_govtid_provider.getSelectedItem()).code;
        serviceProviderName = ((OperatorsModel) spinner_govtid_provider.getSelectedItem()).defaultCode;
        if (spinner_govtid_provider.getSelectedItemPosition() == 0) {
            CustomToast.showMessage(EkycActivity.this, "Please select any valid ID to continue");
            return;
        } else if (!validateValues(spinner_govtid_provider.getSelectedItemPosition())) {
            return;
        } else if (image1 == null) {
            CustomToast.showMessage(this, "Please select front image of id type");
            return;
        } else if (image2 == null) {
            CustomToast.showMessage(this, "Please select back image of id type");
            return;
        }


//        loadDlg.show();
        uploadImage(pathOfImg);


    }


    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void sendRefresh(String message) {
        if (!message.equals("Your request has been saved,Please wait to get it approved")) {
            Intent intent = new Intent("setting-change");
            intent.putExtra("updates", "1");
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
            CustomToast.showMessage(getApplicationContext(), message);

            Handler mHandler = new Handler();
            mHandler.postDelayed(updateTask, 2000);
        } else {
            CustomToast.showMessage(getApplicationContext(), message);
        }


    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(EkycActivity.this, R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    @Override
    public void onBackPressed() {
//        if (isChanged) {
//            showExitDialog();
//        } else {
//            super.onBackPressed();
//        }
        Intent shoppingIntent = new Intent(EkycActivity.this, MainActivity.class);
        startActivity(shoppingIntent);
        finish();
    }


    private void selectImage(final ImageView imageView) {


        final CharSequence[] items = {"Take Photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(EkycActivity.this);
        builder.setTitle("Upload Profile Image!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utility.checkPermission(EkycActivity.this);

                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    if (result)

                        captureCameraImage();

                } else if (items[item].equals("Choose from Gallery")) {
                    userChoosenTask = "Choose from Gallery";
                    if (result)
                        galleryIntent();


                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                    btnEditProfilePic.setVisibility(View.VISIBLE);
                    btnEditSubmit.setVisibility(View.GONE);
                    if (imageView.getId() == (R.id.ivEditProfilePic)) {
                        isimage1 = false;
                    } else if (imageView.getId() == (R.id.ivEditProfilePic1)) {
                        isimage1 = false;
                    }
                }
            }
        });
        builder.show();
    }

    private void galleryIntent() {

        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_PICK);//
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    private void cameraIntent() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == CAMERA_PHOTO)
                if (imageToUploadUri != null) {
                    Uri selectedImage = imageToUploadUri;
//                    getContentResolver().notifyChange(selectedImage, null);
                    Bitmap reducedSizeBitmap = getBitmap(imageToUploadUri.getPath());
                    pathOfImg = null;
                    pathOfImg = ((imageToUploadUri.getPath()));
                    if (reducedSizeBitmap != null) {
//                        ivEditProfilePic.setImageBitmap(reducedSizeBitmap);
                        if (isimage1) {
                            tvFront.setVisibility(View.GONE);
                            image1 = ((imageToUploadUri.getPath()));
                            ivEditProfilePic.setImageBitmap(reducedSizeBitmap);
                            isimage1 = false;
                            isimage2 = true;
                        } else if (isimage2) {
                            tvBack1.setVisibility(View.GONE);
                            image2 = ((imageToUploadUri.getPath()));
                            ivEditProfilePic1.setImageBitmap(reducedSizeBitmap);
                            isimage2 = false;
                            isimage1 = true;
                        }
                    } else {
                        Toast.makeText(this, "Error while capturing Image", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(this, "Error while capturing Image", Toast.LENGTH_LONG).show();
                }
        }
    }

    private Bitmap getBitmap(String path) {

//        Uri uri = FileProvider.getUriForFile(EkycActivity.this, BuildConfig.APPLICATION_ID + ".MadMoveGlobal.CashierCard.provider", new File(path));
//        Uri uri = FileProvider.getUriForFile(EkycActivity.this, this.getApplicationContext().getPackageName() + ".my.package.name.provider", new File(path));


        Uri uri = Uri.fromFile(new File(path));
        InputStream in = null;
        try {
            final int IMAGE_MAX_SIZE = 1200000; // 1.2MP
            in = getContentResolver().openInputStream(uri);

            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(in, null, o);
            in.close();


            int scale = 1;
            while ((o.outWidth * o.outHeight) * (1 / Math.pow(scale, 2)) >
                    IMAGE_MAX_SIZE) {
                scale++;
            }
            Log.d("", "scale = " + scale + ", orig-width: " + o.outWidth + ", orig-height: " + o.outHeight);

            Bitmap b = null;
            in = getContentResolver().openInputStream(uri);
            if (scale > 1) {
                scale--;
                // scale to max possible inSampleSize that still yields an image
                // larger than target
                o = new BitmapFactory.Options();
                o.inSampleSize = scale;
                b = BitmapFactory.decodeStream(in, null, o);

                // resize to desired dimensions
                int height = b.getHeight();
                int width = b.getWidth();
                Log.d("", "1th scale operation dimenions - width: " + width + ", height: " + height);

                double y = Math.sqrt(IMAGE_MAX_SIZE
                        / (((double) width) / height));
                double x = (y / height) * width;

                Bitmap scaledBitmap = Bitmap.createScaledBitmap(b, (int) x,
                        (int) y, true);
                b.recycle();
                b = scaledBitmap;

                System.gc();
            } else {
                b = BitmapFactory.decodeStream(in);
            }
            in.close();

            Log.d("", "bitmap size - width: " + b.getWidth() + ", height: " +
                    b.getHeight());
            return b;
        } catch (IOException e) {
            Log.e("", e.getMessage(), e);
            return null;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void onCaptureImageResult(Intent data) {
        btnEditProfilePic.setVisibility(View.GONE);
        tvFront.setVisibility(View.GONE);

        btnEditSubmit.setVisibility(View.VISIBLE);
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//        assert thumbnail != null;
//        thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        Uri tempUri = getImageUri(EkycActivity.this, thumbnail);
        String filePath = getRealPathFromURI(tempUri);

//        uploadImage(compressImage(filePath));
        pathOfImg = null;
        pathOfImg = (filePath);
        if (isimage1) {
            tvFront.setVisibility(View.GONE);
            image1 = (filePath);
            ivEditProfilePic.setImageBitmap(thumbnail);
        } else if (isimage2) {
            tvBack1.setVisibility(View.GONE);
            image2 = (filePath);
            ivEditProfilePic1.setImageBitmap(thumbnail);
        }


    }


    private void captureCameraImage() {
        Intent chooserIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (isimage1) {
            File f = new File(Environment.getExternalStorageDirectory(), "POST_IMAGE.jpg");
            chooserIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
            imageToUploadUri = Uri.fromFile(f);
        } else if (isimage2) {
            File f = new File(Environment.getExternalStorageDirectory(), "POST_IMAGE1.jpg");
            chooserIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
            imageToUploadUri = Uri.fromFile(f);

        }


        if (Build.VERSION.SDK_INT >= 24) {
            try {
                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
                startActivityForResult(chooserIntent, CAMERA_PHOTO);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            startActivityForResult(chooserIntent, CAMERA_PHOTO);
        }

        btnEditProfilePic.setVisibility(View.GONE);
        tvFront.setVisibility(View.GONE);

//        btnEditSubmit.setVisibility(View.VISIBLE);
//        Uri tempUri = getImageUri(EkycActivity.this, thumbnail);
////        uploadImage(compressImage(filePath));
//        pathOfImg = null;
//        pathOfImg = (imageToUploadUri.getPath());
//        if (isimage1) {
//            tvFront.setVisibility(View.GONE);
//            image1 = (filePath);
//            ivEditProfilePic.setImageBitmap(thumbnail);
//        } else if (isimage2) {
//            tvBack1.setVisibility(View.GONE);
//            image2 = (filePath);
//            ivEditProfilePic1.setImageBitmap(thumbnail);
//        }
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        btnEditProfilePic.setVisibility(View.GONE);
//        tvFront.setVisibility(View.GONE);
        btnEditSubmit.setVisibility(View.VISIBLE);
        Bitmap bm = null;
        if (data != null) {
            try {
                Uri selectedImageUri = data.getData();
//                uploadImage(compressImage(getPath(EkycActivity.this, selectedImageUri)));
                pathOfImg = (getPath(EkycActivity.this, selectedImageUri));
                if (isimage1) {
                    tvFront.setVisibility(View.GONE);
                    ivEditProfilePic.setImageBitmap(getCompressedBitmap(getPath(EkycActivity.this, selectedImageUri)));
                    image1 = pathOfImg;
                } else if (isimage2) {
                    tvBack1.setVisibility(View.GONE);
                    ivEditProfilePic1.setImageBitmap(getCompressedBitmap(getPath(EkycActivity.this, selectedImageUri)));
                    image2 = pathOfImg;
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public String getRealPathFromURI(Uri uri) {
        try (Cursor cursor = getContentResolver().query(uri, null, null, null, null)) {
            if (cursor.moveToFirst()) {
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                return cursor.getString(idx);
            } else return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void uploadImage(String imagePath) {
        loadDlg.show();
        Log.i("Image url", image1 + "        " + image2);
        File file = new File(image1);
        File seconimage = new File(image2);
        uploadHeaderParams = new HashMap<>();
        uploadHeaderParams.put("sessionId", session.getUserSessionId());
        uploadHeaderParams.put("address1", etAddress1.getText().toString());
        uploadHeaderParams.put("addres2", etAddress2.getText().toString());
        uploadHeaderParams.put("city", etCity.getText().toString());
        uploadHeaderParams.put("state", etState.getText().toString());
        uploadHeaderParams.put("pincode", etPostalCode.getText().toString());
        uploadHeaderParams.put("country", "India");
        uploadHeaderParams.put("idNumber", etGovtIdNo.getText().toString());
        uploadHeaderParams.put("idType", serviceProviderName);

        Log.i("Edit REQUEST", uploadHeaderParams.toString());
        Log.i("Edit URLSSSS", ApiUrl.URL_UPLOAD_IMAGE);
        MultipartRequest mr = new MultipartRequest(ApiUrl.URL_UPLOAD_IMAGE, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                loadDlg.dismiss();
//                Picasso.with(getApplicationContext()).load(ApiUrl.URL_IMAGE_MAIN + session.getUserImage()).placeholder(R.mipmap.ic_user_load).into(ivUser);
                CustomToast.showMessage(EkycActivity.this, NetworkErrorHandler.getMessage(volleyError, EkycActivity.this));
            }
        },
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("Edit REQUEST", uploadHeaderParams.toString());
                        JSONObject jsonObj;
                        try {
                            jsonObj = new JSONObject(response);
                            System.out.println("reee" + response);
                            String code = jsonObj.getString("code");
                            String message = jsonObj.getString("message");
                            if (code != null & code.equals("S00")) {
                                loadDlg.dismiss();

                                List<UserModel> currentUserList = Select.from(UserModel.class).list();
                                UserModel currentUser = currentUserList.get(0);
                                currentUser.setHaskycRequest(true);
                                session.setHaskycRequest(true);
                                currentUser.save();

                                try {
                                    InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                                } catch (Exception e) {
                                    loadDlg.dismiss();
                                }
                                successDialogs(message);

//                                CustomToast.showMessage(EkycActivity.this, message);
//                                Intent intent = new Intent(getBaseContext(), EkycbackActivity.class);
//
////                                startActivity(new Intent(getApplicationContext(), EkycbackActivity.class));
//                                intent.putExtra("idNumber", etGovtIdNo.getText().toString());
//                                intent.putExtra("idType", serviceProviderName);
//                                startActivity(intent);
                            } else if (code != null & code.equals("F03")) {
                                loadDlg.dismiss();
                                showInvalidSessionDialog();

                            } else {
                                loadDlg.dismiss();
//                                CustomToast.showMessage(EkycActivity.this, jsonObj.getString("message"));
//                                Toast.makeText(getApplicationContext(), "Error occurred please try again", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            loadDlg.dismiss();
//                            Picasso.with(getApplicationContext()).load(ApiUrl.URL_IMAGE_MAIN + session.getUserImage()).placeholder(R.mipmap.ic_user_load).into(ivUser);
                            CustomToast.showMessage(EkycActivity.this, "Exception Caused");
                            e.printStackTrace();
                        }


                    }
                }, file, uploadHeaderParams, "image", seconimage, "image2"
        );
        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        mr.setRetryPolicy(policy);
        PayQwikApplication.getInstance().addToRequestQueue(mr);
    }

    public String compressImage(String filePath) {


        Bitmap scaledBitmap = null;
        BitmapFactory.Options options = new BitmapFactory.Options();

        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = 800;
        int actualWidth = 800;


        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;


        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }


        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
        options.inJustDecodeBounds = false;
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {

            Log.i("WIDTH    HEIGHT", actualWidth + "      " + actualHeight);

            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);

            scaledBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return filename;

    }

    public String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "PayQwik/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
        return (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");

    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }

    //UPDATED!
    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        if (cursor != null) {
            //HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            //THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else return null;
    }

    public Bitmap getCompressedBitmap(String imagePath) {
        float maxHeight = 1920.0f;
        float maxWidth = 1080.0f;
        Bitmap scaledBitmap = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(imagePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;
        float imgRatio = (float) actualWidth / (float) actualHeight;
        float maxRatio = maxWidth / maxHeight;

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
        options.inJustDecodeBounds = false;
        options.inDither = false;
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
            bmp = BitmapFactory.decodeFile(imagePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

        ExifInterface exif = null;
        try {
            exif = new ExifInterface(imagePath);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
            } else if (orientation == 3) {
                matrix.postRotate(180);
            } else if (orientation == 8) {
                matrix.postRotate(270);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 85, out);

        byte[] byteArray = out.toByteArray();

        Bitmap updatedBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);

        return updatedBitmap;
    }


    private boolean validateValues(int postion) {
        if (postion == 1) {
            InputFilter[] FilterArray = new InputFilter[1];
            FilterArray[0] = new InputFilter.LengthFilter(12);
            etGovtIdNo.setFilters(FilterArray);
            return validateaadhar();
//        } else if (postion == 2) {
//            InputFilter[] FilterArray = new InputFilter[1];
//            FilterArray[0] = new InputFilter.LengthFilter(10);
//            etGovtIdNo.setFilters(FilterArray);
//            return validatepancard();
        } else if (postion == 4) {
            InputFilter[] FilterArray = new InputFilter[1];
            FilterArray[0] = new InputFilter.LengthFilter(16);
            etGovtIdNo.setFilters(FilterArray);
            return validatepassport();
        } else if (postion == 2) {
            InputFilter[] FilterArray = new InputFilter[1];
            FilterArray[0] = new InputFilter.LengthFilter(20);
            etGovtIdNo.setFilters(FilterArray);
            return validatevoterid();
        } else if (postion == 3) {
            InputFilter[] FilterArray = new InputFilter[1];
            FilterArray[0] = new InputFilter.LengthFilter(20);
            etGovtIdNo.setFilters(FilterArray);
            return validatedl();
        } else if (postion == 0) {
            InputFilter[] FilterArray = new InputFilter[1];
            FilterArray[0] = new InputFilter.LengthFilter(20);
            etGovtIdNo.setFilters(FilterArray);
            return validateSelectId();
        }
        return true;
    }


    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            int i = view.getId();
            if (i == R.id.etPostalCode) {
                if (editable.length() == EkycActivity.this.getResources().getInteger(R.integer.passwordLength)) {

                    loadPostalCode();
                }
            }
        }

    }


    public void loadPostalCode() {
        loadDlg.show();
//        jsonRequest = new JSONObject();
//        try {
//            jsonRequest.put("sessionId", session.getUserSessionId());
//        } catch (JSONException e) {
//            e.printStackTrace();
//            jsonRequest = null;
//        }

//        if (jsonRequest != null) {
//            Log.i("LOADCIRCLEOP Request", jsonRequest.toString());
//            Log.i("LOADCIRCLEOP URL", ApiUrl.URL_CIRCLE_OPERATOR);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, "http://postalpincode.in/api/pincode/" + etPostalCode.getText().toString(), (String) null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject JsonObj) {
                        loadDlg.dismiss();
                        Log.i("PostalCode", JsonObj.toString());
                        try {

                            //New Implementation

//                    String jsonString = JsonObj.getString("response");
//                    Log.i("Escape str", jsonEscape(jsonString));
//
//                    JSONObject response = new JSONObject(jsonString);

                            JSONArray operatorArray = JsonObj.getJSONArray("PostOffice");

//                        operatorList.add(new OperatorsModel("", "Select your operator", ""));

                            for (int i = 0; i < operatorArray.length(); i++) {
                                JSONObject c = operatorArray.getJSONObject(i);
                                String Circle = c.getString("Circle");
                                String State = c.getString("State");
//                                String op_default_code = c.getString("serviceLogo");

                                etCity.setText(Circle);
                                etState.setText(State);
//                                OperatorsModel oModel = new OperatorsModel(op_code, op_name, op_default_code);
//                                oModel.save();
//                                operatorList.add(oModel);
//                                operatorLists.add(oModel);
//                                Log.i("OperatorsModel", String.valueOf(operatorList.get(i).getCode()));
//                                operatorIdCode.put(op_code, i + 1);
                            }
//                            spService.setHint("Operator");
//
//                            JSONArray circleArray = JsonObj.getJSONArray("circles");
//
//                            circleList.add(new CircleModel("", "Select your circle"));
//                            for (int i = 0; i < circleArray.length(); i++) {
//                                JSONObject c = circleArray.getJSONObject(i);
//                                String op_code = c.getString("code");
//                                String op_name = c.getString("name");
//                                CircleModel cModel = new CircleModel(op_code, op_name);
//                                cModel.save();
//                                circleList.add(cModel);
//                                circleLists.add(cModel);
//                                circleIdCode.put(op_code, i + 1);
//                            }
//                            spArea.setHint("Circle");

//                            if (operatorList != null && operatorList.size() != 0) {
//                                try {
//                                    operatorList.add(0, new OperatorsModel("", "Select your operator", ""));
//                                    spinnerAdapterOperator = new OperatorSpinnerAdapter(getActivity(), android.R.layout.simple_spinner_item, operatorList);
//                                    spinnerPrepaidProvider.setAdapter(spinnerAdapterOperator);
//                                } catch (Exception e) {
//                                    e.printStackTrace();
//                                }
//                            }

//                    if (circleList != null && circleList.size() != 0) {
//                        try {
//                            circleList.add(0, new CircleModel("", "Select your circle"));
//                            spinnerAdapterCircle = new CircleSpinnerAdapter(getActivity(), android.R.layout.simple_spinner_item, circleList);
//                            spinnerPrepaidCircle.setAdapter(spinnerAdapterCircle);
//
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(EkycActivity.this, getResources().getString(R.string.server_exception2), Toast.LENGTH_SHORT).show();
//                            Snackbar.make(llLayouts, getResources().getString(R.string.server_exception2), Snackbar.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loadDlg.dismiss();
                error.printStackTrace();
                Toast.makeText(EkycActivity.this, getResources().getString(R.string.server_exception), Toast.LENGTH_SHORT).show();
//                Snackbar.make(llLayouts, getResources().getString(R.string.server_exception), Snackbar.LENGTH_LONG).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> map = new HashMap<>();
                map.put("hash", "1234");

                String basicAuth = "Basic " + new String(Base64.encode("user:pass".getBytes(), Base64.NO_WRAP));
                map.put("Authorization", basicAuth);


                return map;
            }

        };
        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonObjReq.setRetryPolicy(policy);
        PayQwikApplication.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);


    }


    private boolean validateaadhar() {
        if (etGovtIdNo.getText().toString().trim().isEmpty() || etGovtIdNo.getText().toString().trim().length() != 12) {
            etGovtIdNo.setError("Enter valid Aadhaar number");
            requestFocus(etGovtIdNo);
            return false;
        } else {
            etGovtIdNo.setError(null);
        }

        return true;
    }

    private boolean validatedl() {
        if ((etGovtIdNo.getText().toString().trim().isEmpty() || etGovtIdNo.getText().toString().trim().length() < 10 || etGovtIdNo.getText().toString().trim().length() > 15) && !isValidDl(etGovtIdNo.getText().toString())) {
            etGovtIdNo.setError("Enter valid DL number , do not use special characters and space");
            requestFocus(etGovtIdNo);
            return false;
        } else {
            etGovtIdNo.setError(null);
        }

        return true;
    }

        private boolean validateSelectId() {
            if (spinner_govtid_provider.getSelectedItemPosition() == 0) {
                etGovtIdNo.setError("Please select any valid ID to continue");


                return false;
            } else {

    //            etGovtIdNo.setError(null);
            }

            return true;
        }

    private boolean validatepancard() {
        if ((etGovtIdNo.getText().toString().trim().isEmpty() || etGovtIdNo.getText().toString().trim().length() != 10) && !isValidPAN(etGovtIdNo.getText().toString())) {
            etGovtIdNo.setError("Enter valid Pan Card number");
            requestFocus(etGovtIdNo);
            return false;
        } else {
            etGovtIdNo.setError(null);
        }

        return true;
    }


    private boolean validatepassport() {
        if ((etGovtIdNo.getText().toString().trim().isEmpty() || etGovtIdNo.getText().toString().trim().length() < 8) && !isValidPassport(etGovtIdNo.getText().toString())) {
            etGovtIdNo.setError("Enter valid passport number, do not use special characters and space");
            requestFocus(etGovtIdNo);
            return false;
        } else {
            etGovtIdNo.setError(null);
        }

        return true;
    }

    private boolean validatevoterid() {
        if (etGovtIdNo.getText().toString().trim().isEmpty() || etGovtIdNo.getText().toString().trim().length() < 10) {
            etGovtIdNo.setError("Enter valid voter id number, do not use special characters and space");
            requestFocus(etGovtIdNo);
            return false;
        } else {
            etGovtIdNo.setError(null);
        }

        return true;
    }

    private boolean validateAddressOne() {
        if (etAddress1.getText().toString().trim().isEmpty() || etAddress1.getText().toString().trim().length() < 1) {
            ilAddress1.setError("Enter your House No./ Flat No.");
            requestFocus(etAddress1);
            return false;
        } else if (etAddress1.getText().toString().trim().equals("0")) {
            ilAddress1.setError("Your Flat no./House no. should be greater than 0");
            requestFocus(etAddress1);
            return false;
        } else {
            ilAddress1.setError(null);
        }

        return true;
    }

    private boolean validateAddressTwo() {
        if (etAddress2.getText().toString().trim().isEmpty() || etAddress2.getText().toString().trim().length() < 1) {
            ilAddress2.setError("Enter Street Name/ Landmark");
            requestFocus(etAddress2);
            return false;
        } else {
            ilAddress2.setError(null);
        }

        return true;
    }

    private boolean validateCity() {
        if (etCity.getText().toString().trim().isEmpty() || etCity.getText().toString().trim().length() < 1) {
            ilCity.setError("Enter your city name");
            requestFocus(etCity);
            return false;
        } else {
            ilCity.setError(null);
        }

        return true;
    }

    private boolean validateState() {
        if (etState.getText().toString().trim().isEmpty() || etGovtIdNo.getText().toString().trim().length() < 1) {
            ilState.setError("Enter your state Name");
            requestFocus(etState);
            return false;
        } else {
            ilState.setError(null);
        }

        return true;
    }

    private boolean validatPinCode() {
        if (etPostalCode.getText().toString().trim().isEmpty() || etPostalCode.getText().toString().trim().length() != 6) {
            ilPostalCode.setError("Enter 6 digit valid pincode number");
            requestFocus(etPostalCode);
            return false;
        } else {
            ilPostalCode.setErrorEnabled(false);
        }

        return true;
    }

    public static boolean isValidPassport(final String passport) {

        Pattern pattern;
        Matcher matcher;
        final String PASSPORT_PATTERN = "^[A-Z]{1}-[0-9]{7}$";
        pattern = Pattern.compile(PASSPORT_PATTERN);
        matcher = pattern.matcher(passport);

        return matcher.matches();

    }

    public static boolean isValidDl(final String dL) {

        Pattern pattern;
        Matcher matcher;
        final String DL_PATTERN = "^[A-Z]{2}[0-9]{13}$";
        pattern = Pattern.compile(DL_PATTERN);
        matcher = pattern.matcher(dL);

        return matcher.matches();

    }

    public static boolean isValidPAN(final String pan) {

        Pattern pattern;
        Matcher matcher;
        final String PAN_PATTERN = "^[A-Z]{5}[0-9]{4}[A-Z]{1}$";
        pattern = Pattern.compile(PAN_PATTERN);
        matcher = pattern.matcher(pan);

        return matcher.matches();

    }

    public void successDialogs(String Messages) {
        CustomAlertDialog builder = new CustomAlertDialog(EkycActivity.this, R.string.dialog_title2, Html.fromHtml(Messages));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                sendRefresh();
                startActivity(new Intent(EkycActivity.this, MainActivity.class));
                finish();


            }
        });

        builder.show();
    }

    private void sendRefresh() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "1");
        LocalBroadcastManager.getInstance(EkycActivity.this).sendBroadcast(intent);
    }

    public String generateDLMessageDialog() {
        String source = "<b><font color=#000000> Please don't enter any special characters while entering driving license number.</font></b>";

        return source;
    }

    public String generateVIMessageDialog() {
        String source = "<b><font color=#000000> Please don't enter any special characters while entering voter id number.</font></b>";

        return source;
    }

    public void showDLDialog(String message) {
        CustomAlertDialog builder = new CustomAlertDialog(this, R.string.dialog_title2, Html.fromHtml(message));
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();


            }
        });
    }

    public void showDLDialogs(String message) {
        CustomAlertDialog builder = new CustomAlertDialog(this, R.string.dialog_title2, Html.fromHtml(message));
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();


            }
        });
    }


    public void requestDialog(String messagess) {
        CustomAlertDialog builder = new CustomAlertDialog(this, R.string.dialog_title2, Html.fromHtml(messagess));
        builder.setPositiveButton(getResources().getString(R.string.Accept), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();



            }
        });

        builder.show();
    }

    public String genraterequestKyc() {
        String source = "<b><font color=#000000>" + "Please upgrade your KYC with OSV to apply for physical card or contact customer care at" + "</font></b>" +
                "<br><br><b><font color=#3752a4>" + "info@ewiresofttech.com" + "</font></b><br></br>" +
                "<br><br><b><font color=#ff0000>" + getResources().getString(R.string.Accept_Cont) + "</font></b><br></br>";
        return source;
    }
}
