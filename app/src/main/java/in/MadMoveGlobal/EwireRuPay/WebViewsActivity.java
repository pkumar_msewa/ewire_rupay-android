package in.MadMoveGlobal.EwireRuPay;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.webkit.ConsoleMessage;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.RequestQueue;

import in.MadMoveGlobal.EwireRuPay.activity.MainActivity;
import in.MadMoveGlobal.custom.CustomAlertDialog;
import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.metadata.AppMetadata;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.util.ServiceUtility;


public class WebViewsActivity extends AppCompatActivity {
    private ProgressDialog dialog;
    Intent mainIntent;
    private LinearLayout llLoadMoneyWebViewLoading;
    String html, encVal, amountToLoad, transRefNo;
    private UserModel userModel = UserModel.getInstance();
    private LoadingDialog loadingDialog;
    private RequestQueue rq;
    private String type, URL, REQUESTOBJECT;
    private String SUBTYPE;
    private boolean loadmoney;

    @SuppressLint("RestrictedApi")
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
//        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(WebViewActivity.this, TargetActivity.class));
        setContentView(R.layout.activity_ebs_web);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbars);
        setSupportActionBar(toolbar);

        ImageView ivToolbarLogo = (ImageView) findViewById(R.id.ivpqlogo);
//        ivToolbarLogo.setImageBitmap(BitmapHelper.decodeSampledBitmapFromResource(getResources(), R.drawable.toolbart_niki, ivToolbarLogo.getMaxWidth(), ivToolbarLogo.getMaxHeight()));
        this.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        this.getSupportActionBar().setDisplayShowTitleEnabled(false);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        amountToLoad = getIntent().getStringExtra("amount");
        transRefNo = getIntent().getStringExtra("transactionRefNo");
        type = getIntent().getStringExtra("TYPE");
        if (!getIntent().getStringExtra("TYPE").equals("LoadMoney")) {
            URL = getIntent().getStringExtra("URL");
            REQUESTOBJECT = getIntent().getStringExtra("REQUESTOBJECT");
            try {
                SUBTYPE = getIntent().getStringExtra("SUBTYPE");
            } catch (NullPointerException e) {

            }
        }

        Integer randomNum = ServiceUtility.randInt(0, 9999999);
        loadingDialog = new LoadingDialog(WebViewsActivity.this);
        html = randomNum.toString();
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog();
            }
        });
        llLoadMoneyWebViewLoading = (LinearLayout) findViewById(R.id.llLoadMoneyWebViewLoading);
//        if (Integer.parseInt(amountToLoad) <= 5000) {
//            try {
        getTranscation();
        llLoadMoneyWebViewLoading.setVisibility(View.GONE);
//            } catch (NullPointerException e) {
//            }
//        } else {

//            CustomToast.showMessage(WebViewActivity.this, "Your Per Day Transaction limit is 5000");
//            finish();
//
//        }
    }

    public void showDialog() {
        android.support.v7.app.AlertDialog.Builder exitDialog =
                new android.support.v7.app.AlertDialog.Builder(WebViewsActivity.this, R.style.AppCompatAlertDialogStyle);
        exitDialog.setTitle(R.string.cancel_load);
        exitDialog.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                finish();
            }
        });
        exitDialog.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        exitDialog.show();
    }


    public void showToast(String msg) {
        Toast.makeText(this, "Toast: " + msg, Toast.LENGTH_LONG).show();
    }

    @SuppressLint({"AddJavascriptInterface", "SetJavaScriptEnabled"})
    void getTranscation() {


        @SuppressWarnings("unused")
        class MyJavaScriptInterface {
            @JavascriptInterface
            public void processHTML(String html) {
//                Log.i("logHtml", html);
                // process the html as needed by the app
                String status = null;
                if (html.contains("Failure")) {
                    status = "Failed";
                } else if (html.contains("Success")) {
                    status = "Success";
                } else if (html.contains("Aborted")) {
                    status = "Failed";

                } else {
                    status = "Status Not Known!";
                }

                Intent i = new Intent(WebViewsActivity.this, LoadMoneyStatusActivity.class);
                i.putExtra(LoadMoneyStatusActivity.EXTRA, status);
//                                                i.putExtra("TYPE", type);
//                                                if (SUBTYPE != null && !SUBTYPE.equalsIgnoreCase("")) {
//                                                    i.putExtra("SUBTYPE", SUBTYPE);
//                                                }

//                i.putExtra("amount", amountToLoad);
//                i.putExtra("transactionRefNo", transRefNo);
                                                i.putExtra("REQUESTOBJECT", REQUESTOBJECT);
                startActivity(i);
            }
        }
        final WebView webview = (WebView) findViewById(R.id.webview);
        webview.getSettings().setDomStorageEnabled(true);
        webview.addJavascriptInterface(new MyJavaScriptInterface(), "HTMLOUT");
        final String js = "javascript:document.getElementById('pay').click()";
//                                        String ua = "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.4) Gecko/20100101 Firefox/4.0";
//                                        webview.getSettings().setUserAgentString(ua);

        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setLoadWithOverviewMode(true);
        webview.getSettings().setUseWideViewPort(true);
        webview.getSettings().setDomStorageEnabled(true);

        webview.getSettings().setSupportZoom(true);
        webview.getSettings().setBuiltInZoomControls(true);
        webview.getSettings().setDisplayZoomControls(false);

        webview.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webview.setScrollbarFadingEnabled(false);
        llLoadMoneyWebViewLoading.setVisibility(View.GONE);
        webview.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(webview, url);
                llLoadMoneyWebViewLoading.setVisibility(View.GONE);
//                view.loadUrl("javascript:document.getElementById(\"ShippingInformation\").setAttribute(\"style\",\"display:none;\");");
//
//                Log.i("UELURL", url);

                if (url.contentEquals("https://liveewire.com/Api/v1/User/Android/en/LoadMoney/SuccessUPI")) {
                    sendRefresh();
                    Intent shoppingIntent = new Intent(WebViewsActivity.this, MainActivity.class);
                    startActivity(shoppingIntent);
                    finish();

                }else if(url.contentEquals("https://liveewire.com/ws/api/UpiRedirectFailureUPI")){
                    sendRefresh();
                    Intent shoppingIntent = new Intent(WebViewsActivity.this, MainActivity.class);
                    startActivity(shoppingIntent);
                    finish();

                }
//                                    view.stopLoading();
// if (true) {
//
//                                    webview.loadUrl("javascript:window.HTMLOUT.processHTML('<head>'+document.getElementsByTagName('html')[0].innerHTML+'</head>');");
//                                    webview.setVisibility(View.GONE);
//
//
//                                } else if (url.contentEquals("/Cancel")) {
//                                    view.stopLoading();
//
//                                    showInvalidCancleDialog();
//                                }
            }


            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
//                Log.d("erroecode", String.valueOf(errorCode));
                llLoadMoneyWebViewLoading.setVisibility(View.VISIBLE);
                showErrorDialog(errorCode);

//                                                Toast.makeText(getApplicationContext(), "Oh no! " + description, Toast.LENGTH_SHORT).show();

            }


            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                llLoadMoneyWebViewLoading.setVisibility(View.VISIBLE);

                if(url.contentEquals("https://liveewire.com/ws/api/UpiRedirectFailureUPI")){
                    sendRefresh();
                    Intent shoppingIntent = new Intent(WebViewsActivity.this, MainActivity.class);
                    startActivity(shoppingIntent);
                    finish();

                }
//                view.loadUrl("javascript: document.getElementById('pay').click()");
//                Log.i("UEL", url);
//                if(Build.VERSION.SDK_INT >= 19) {
//                    view.evaluateJavascript(js, new ValueCallback<String>() {
//                        @Override
//                        public void onReceiveValue(String s) {
//                        }
//                    });
//                }

                if (url.contains("LoadMoney/Cancel")) {
                    showInvalidCancleDialog();
                    view.stopLoading();
                }

            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                super.onReceivedSslError(view, handler, error);
                handler.proceed();

            }
        });
        webview.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onConsoleMessage(ConsoleMessage cm) {
                Log.d("TAG", cm.message() + " at " + cm.sourceId() + ":" + cm.lineNumber());
//                if (cm.sourceId() == "http://13.127.101.55/IplCards/Api/v1/User/Android/en/LoadMoney/SuccessUPI") {
//                    Intent intent = new Intent(WebViewActivity.this, MainActivity.class);
//                    startActivity(intent);
//                    finish();
//                }
                return true;
            }

        });
//                        webview.loadData(response,"","UTF-8");
//                webview.loadData(response, "text/html; charset=UTF-8", null);






        try {
            webview.loadData(getIntent().getStringExtra("response"), "text/html; charset=utf-8", "UTF-8");
        } catch (Exception e) {
            showToast("Exception occurred while opening Webview.");

        }



//        Intent intent = new Intent(WebViewActivity.this, MainActivity.class);
//        startActivity(intent);
//        finish();
//                    } else {
//                        CustomToast.showMessage(WebViewActivity.this, "Some thing went wrong,Please try after some time");
//                        finish();
//                    }
//                } else {
//                    loadingDialog.dismiss();
//                    CustomToast.showMessage(WebViewActivity.this, "!ERROR!Caller IP not registered/Merchant Not found.");
//                    finish();
    }
//
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                loadingDialog.dismiss();
//                finish();
//                try {
//                    CustomToast.showMessage(WebViewActivity.this, NetworkErrorHandler.getMessage(error, WebViewActivity.this));
//                } catch (NullPointerException e) {
//                }
//            }
//        }) {

//            @Override
//            public byte[] getBody() throws AuthFailureError {
//                try {
//                    return requestBody == null ? null : requestBody.getBytes("utf-8");
//                } catch (UnsupportedEncodingException uee) {
//                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
//                    return null;
//                }


//            @Override
//            protected JSONObject getParams() throws AuthFailureError {
//                JSONObject map = new JSONObject();
//                map.put("access_code", access_code);
//                map.put("transactionNo", value);
//                try {
//                    map.put("amount", amountToLoad);
//                    map.put("sessionId", userModel.getUserSessionId());
//                    map.put("transactionRefNo", transRefNo);
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//                Log.i("VALE", map.toString());
//                return map;
//            }

//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> map = new HashMap<>();
//                map.put("Content-Type", "application/json");
//                return map;
//            }
//        };
//        int socketTimeout = 60000;
//        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//        stringRequest.setRetryPolicy(policy);
//        rq.add(stringRequest);
//
//
//    }
//    else if(response.equals("!ERROR!Caller IP not registered/Merchant Not found."))
//
//    {
//        CustomToast.showMessage(WebViewActivity.this, response);
//        loadingDialog.dismiss();
//        finish();
//    } else if(jsonObject.getString("code").
//
//    equalsIgnoreCase("f00"))
//
//    {
//        CustomToast.showMessage(WebViewActivity.this, jsonObject.getString("message"));
//        loadingDialog.dismiss();
//        finish();
//    }

//} catch(JSONException e){
//        loadingDialog.dismiss();
//        finish();
//
//        }
//        }
//        },new Response.ErrorListener(){
//@Override
//public void onErrorResponse(VolleyError error){
//        loadingDialog.dismiss();
//        finish();
//        try{
//        CustomToast.showMessage(WebViewActivity.this,NetworkErrorHandler.getMessage(error,WebViewActivity.this));
//        }catch(NullPointerException e){
//        }
//        }
//        }){
//@Override
//protected Map<String, String> getParams()throws AuthFailureError{
//        HashMap<String, String> map=new HashMap<>();
//
//        map.put("sessionId",userModel.getUserSessionId());
//        map.put("amount",amountToLoad);
//        map.put("transactionRefNo",transRefNo);
//                map.put("phone", userModel.getUserMobileNo());
//                map.put("useVnet", "true");
//                map.put("lmService", "LMC");
//                map.put("email", userModel.getUserEmail());

//        return map;
//
//        }
//        };
//        int socketTimeout=60000;
//        RetryPolicy policy=new DefaultRetryPolicy(socketTimeout,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//        stringRequest.setRetryPolicy(policy);
//        rq.add(stringRequest);
//        }

    private void sendRefresh() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "1");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    public static void clearCookies(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            CookieManager.getInstance().removeAllCookies(null);
            CookieManager.getInstance().flush();
        } else {
            CookieSyncManager cookieSyncMngr = CookieSyncManager.createInstance(context);
            cookieSyncMngr.startSync();
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.removeAllCookie();
            cookieManager.removeSessionCookie();
            cookieSyncMngr.stopSync();
            cookieSyncMngr.sync();
        }
    }

    public void showErrorDialog(int errorcode) {
        CustomAlertDialog builder = new CustomAlertDialog(WebViewsActivity.this, R.string.dialog_title2, Html.fromHtml(AppMetadata.getErrorCode(errorcode)));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();

            }
        });

        builder.show();
    }

    @Override
    public void onBackPressed() {
        showDialog();
    }

    public void showInvalidCancleDialog() {

        CustomAlertDialog builder = new CustomAlertDialog(WebViewsActivity.this, R.string.dialog_title2, "Your transaction is cancelled");
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();
            }
        });

        builder.show();
    }


}
