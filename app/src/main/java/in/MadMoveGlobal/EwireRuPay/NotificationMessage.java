package in.MadMoveGlobal.EwireRuPay;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;


public class NotificationMessage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_message);
        ImageView ivView = (ImageView) findViewById(R.id.ivView);
        TextView tvDesc = (TextView) findViewById(R.id.tvDesc);
        TextView tittle = (TextView) findViewById(R.id.tittle);
        String Tittle = getIntent().getStringExtra("Tittle");
        String message = getIntent().getStringExtra("message");
        String image = getIntent().getStringExtra("image");
        tittle.setText(Tittle);
        tvDesc.setText(message);

        if (!image.equalsIgnoreCase("null")) {
            Picasso.with(NotificationMessage.this).load(image).placeholder(R.drawable.ic_loading_image).into(ivView);
        }
    }
}
