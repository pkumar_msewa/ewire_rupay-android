package in.MadMoveGlobal.EwireRuPay;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import in.MadMoveGlobal.model.contract;

public class ContactPickerMulti extends AppCompatActivity {

    // List variables
    public String[] Contacts = {};
    public int[] to = {};
    public ListView myListView;

    Button save_button;
    private TextView phone;
    private String phoneNumber;
    private Cursor cursor;
    private MyAdapter ma;
    private Button select;
    ArrayList<contract> contracts;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contacts_multi);

        contracts = new ArrayList<>();
        getAllContacts(this.getContentResolver());
        ListView lv = (ListView) findViewById(R.id.list);
        ma = new MyAdapter();
        lv.setAdapter(ma);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                ma.toggle(i);
                Log.i("value", ((contract) ma.getItem(i)).getPcId());
            }
        });
        lv.setItemsCanFocus(false);
        lv.setTextFilterEnabled(true);
        // adding
        select = (Button) findViewById(R.id.contact_done);
        select.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                StringBuilder checkedcontacts = new StringBuilder();

                for (int i = 0; i < contracts.size(); i++)

                {
                    if (ma.mCheckStates.get(i) == true) {
                        checkedcontacts.append(contracts.get(i).getPcId().toString());
                        checkedcontacts.append("\n");

                    } else {

                    }

                }

                Toast.makeText(ContactPickerMulti.this, checkedcontacts, Toast.LENGTH_LONG).show();
            }
        });

    }


    public void getAllContacts(ContentResolver cr) {

        Cursor phones = cr.query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null,
                null, null);
        while (phones.moveToNext()) {
            String name = phones
                    .getString(phones
                            .getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phoneNumber = phones
                    .getString(phones
                            .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            contracts.add(new contract(phoneNumber, name,false));
        }

        phones.close();
    }


    class MyAdapter extends BaseAdapter implements CompoundButton.OnCheckedChangeListener {
        private SparseBooleanArray mCheckStates;
        LayoutInflater mInflater;
        TextView tv1, tv;
        CheckBox cb;

        MyAdapter() {
            mCheckStates = new SparseBooleanArray(contracts.size());
            mInflater = (LayoutInflater) ContactPickerMulti.this
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return contracts.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub

            return 0;
        }

        @Override
        public View getView(final int position, View convertView,
                            ViewGroup parent) {
            // TODO Auto-generated method stub
            View vi = convertView;
            if (convertView == null)
                vi = mInflater.inflate(R.layout.row, null);
            cb = (CheckBox) vi.findViewById(R.id.checkbox1);
            cb.setText(contracts.get(position).getPcName());

            toggle(position);
            cb.setTag(position);
            cb.setChecked(mCheckStates.get(position, false));
            cb.setOnCheckedChangeListener(this);

            return vi;
        }

        public boolean isChecked(int position) {
            return mCheckStates.get(position, false);
        }

        public void setChecked(int position, boolean isChecked) {
            mCheckStates.put(position, isChecked);
        }

        public void toggle(int position) {
            setChecked(position, !isChecked(position));
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView,
                                     boolean isChecked) {
            // TODO Auto-generated method stub

            mCheckStates.put((Integer) buttonView.getTag(), isChecked);
        }
    }


    // Initializing the buttons according to their ID
//        save_button = (Button) findViewById(R.id.contact_done);
//
//        // Defines listeners for the buttons
//        save_button.setOnClickListener(this);
//
//        Cursor mCursor = getContacts();
//        startManagingCursor(mCursor);
//
//        ListAdapter adapter = new SimpleCursorAdapter(
//                this,
//                android.R.layout.simple_list_item_multiple_choice,
//                mCursor,
//                Contacts = new String[] { ContactsContract.Contacts.DISPLAY_NAME },
//                to = new int[] { android.R.id.text1 });
//
//        setListAdapter(adapter);
//        myListView = getListView();
//        myListView.setItemsCanFocus(false);
//        myListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
//
//    }
//
//    private Cursor getContacts() {
//        // Run query
//        Uri uri = ContactsContract.Contacts.CONTENT_URI;
//        String[] projection = new String[] { ContactsContract.Contacts._ID,
//                ContactsContract.Contacts.DISPLAY_NAME };
//        String selection = ContactsContract.Contacts.HAS_PHONE_NUMBER + " = '"
//                + ("1") + "'";
//        String[] selectionArgs = null;
//        String sortOrder = ContactsContract.Contacts.DISPLAY_NAME
//                + " COLLATE LOCALIZED ASC";
//
//        return managedQuery(uri, projection, selection, selectionArgs,
//                sortOrder);
//    }
//
//    public void onClick(View src) {
//        Intent i;
//        switch (src.getId()) {
//        case R.id.contact_done:
//
//            SparseBooleanArray selectedPositions = myListView
//                    .getCheckedItemPositions();
//            SparseBooleanArray checkedPositions = myListView
//                    .getCheckedItemPositions();
//            if (checkedPositions != null) {
//                for (int k = 0; k < checkedPositions.size(); k++) {
//                     if (checkedPositions.valueAt(k)) {
//                          String name =
//                                 ((Cursor)myListView.getAdapter().getItem(k)).getString(1);
//                            Log.i("XXXX",name + " was selected");
//                        }
//                }
//            }
//
//            break;
//        }
//
//    }
}