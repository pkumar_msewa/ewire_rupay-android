package in.MadMoveGlobal.EwireRuPay;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.os.Looper;
import android.os.StatFs;
import android.util.Log;

import com.android.volley.RequestQueue;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.Date;
import java.util.Locale;

public class ExceptionHandler implements UncaughtExceptionHandler {


    private static Context context;
    private static RequestQueue rq;

    public ExceptionHandler(Context ctx, Class<?> name) {
        context = ctx;
    }

    private StatFs getStatFs() {
        File path = Environment.getDataDirectory();
        return new StatFs(path.getPath());
    }

    private long getAvailableInternalMemorySize(StatFs stat) {
        long blockSize = stat.getBlockSize();
        long availableBlocks = stat.getAvailableBlocks();
        return availableBlocks * blockSize;
    }

    private long getTotalInternalMemorySize(StatFs stat) {
        long blockSize = stat.getBlockSize();
        long totalBlocks = stat.getBlockCount();
        return totalBlocks * blockSize;
    }

    private void addInformation(StringBuilder message) {
        message.append("Locale: ").append(Locale.getDefault()).append("<br />");
        try {
            PackageManager pm = context.getPackageManager();
            PackageInfo pi;
            pi = pm.getPackageInfo(context.getPackageName(), 0);
            message.append("Version: ").append(pi.versionName).append("<br />");
            message.append("Package: ").append(pi.packageName).append("<br />");
        } catch (Exception e) {
            Log.e("CustomExceptionHandler", "Error", e);
            message.append("Could not get Version information for ").append(
                    context.getPackageName());
        }
        message.append("Phone Model: ").append(android.os.Build.MODEL)
                .append("<br />");
        message.append("Android Version: ")
                .append(android.os.Build.VERSION.RELEASE).append("<br />");
        message.append("Board: ").append(android.os.Build.BOARD).append("<br />");
        message.append("Brand: ").append(android.os.Build.BRAND).append("<br />");
        message.append("Device: ").append(android.os.Build.DEVICE).append("<br />");
        message.append("Host: ").append(android.os.Build.HOST).append("<br />");
        message.append("ID: ").append(android.os.Build.ID).append("<br />");
        message.append("Model: ").append(android.os.Build.MODEL).append("<br />");
        message.append("Product: ").append(android.os.Build.PRODUCT)
                .append("<br />");
        message.append("Type: ").append(android.os.Build.TYPE).append("<br />");
        StatFs stat = getStatFs();
        message.append("Total Internal memory: ")
                .append(getTotalInternalMemorySize(stat)).append("<br />");
        message.append("Available Internal memory: ")
                .append(getAvailableInternalMemorySize(stat)).append("<br />");
    }

    public void uncaughtException(Thread t, Throwable e) {
        try {
            StringBuilder report = new StringBuilder();
            Date curDate = new Date();
            report.append("Error Report collected on : ")
                    .append(curDate.toString()).append("<br />").append("<br />");
            report.append("Informations :").append("<br />");
            addInformation(report);
            report.append("<br />").append("<br />");
            report.append("Stack:\n");
            final Writer result = new StringWriter();
            final PrintWriter printWriter = new PrintWriter(result);
            report.append(result.toString());
            printWriter.close();
            report.append("<br />");
            report.append("**** End of current Report ***");
            Log.e(ExceptionHandler.class.getName(),
                    "Error while sendErrorMail" + report);
            sendErrorMail(report);
        } catch (Throwable ignore) {
            Log.e(ExceptionHandler.class.getName(),
                    "Error while sending error e-mail", ignore);
        }
    }

    /**
     * This method for call alert dialog when application crashed!
     */
    private void sendErrorMail(final StringBuilder errorContent) {
        new Thread() {
            @Override
            public void run() {
                final AlertDialog.Builder builder = new AlertDialog.Builder((Activity) context);
                Looper.prepare();
                builder.setTitle("Sorry...!");
                builder.create();
                builder.setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                System.exit(0);
                                dialog.dismiss();
                            }
                        });
                builder.setPositiveButton("Report",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(final DialogInterface dialog,
                                                int which) {
                                StringBuilder body = new StringBuilder("Yoddle");
                                body.append("<br />").append("<br />");
                                body.append(errorContent).append("<br />")
                                        .append("<br />");
                                dialog.dismiss();
                                try {
                                    context.startActivity(new Intent(context, TargetActivity.class).putExtra("feedback", body.toString()));
                                    android.os.Process.killProcess(android.os.Process.myPid());
                                    System.exit(10);
                                } catch (InternalError error) {
                                    context.startActivity(new Intent(context, TargetActivity.class).putExtra("feedback", body.toString()));
                                    android.os.Process.killProcess(android.os.Process.myPid());
                                    System.exit(10);
                                }
                                dialog.dismiss();
                            }
                        });
                builder.setMessage("Oops,Your application has Stopped");

                AlertDialog alertDialog = builder.create();
                alertDialog.show();
                Looper.loop();
            }
        }.start();


    }
}