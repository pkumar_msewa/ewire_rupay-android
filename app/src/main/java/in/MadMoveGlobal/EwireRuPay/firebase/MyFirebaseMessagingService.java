package in.MadMoveGlobal.EwireRuPay.firebase;

//import android.app.Notification;
//import android.app.NotificationManager;
//import android.app.PendingIntent;
//import android.content.Context;
//import android.content.Intent;
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.media.RingtoneManager;
//import android.net.Uri;
//import android.support.v4.app.NotificationCompat;
//

//import com.expletus.mobiruck.fcm.MobiruckFcmData;
//import com.google.firebase.messaging.FirebaseMessagingService;
//import com.google.firebase.messaging.RemoteMessage;
//
//import java.io.IOException;
//import java.io.InputStream;
//import java.net.HttpURLConnection;
//import java.net.URL;
//
//import in.MadMoveGlobal.CashierCard.R;
//import in.MadMoveGlobal.CashierCard.activity.SplashActivity;
//
///**
// * Created by Ksf on 10/24/2016.
// */
//public class MyFirebaseMessagingService extends FirebaseMessagingService {
//    private static final String TAG = "FCM Service";
//    public static final int NOTIFICATION_ID = 1;
//    private NotificationManager mNotificationManager;
//    private Bitmap remote_picture;
//
//    Bitmap bitmap;
//
//    @Override
//    public void onMessageReceived(RemoteMessage remoteMessage) {
//
//        if (remoteMessage.getFrom().equals("171529593785")) {
//
//            String message = remoteMessage.getData().get("message");
//            String imageUri = remoteMessage.getData().get("image");
//            String title = remoteMessage.getData().get("title");
//            String source = "";
//            if (remoteMessage.getData().containsKey("notification_source")) {
//                source = remoteMessage.getData().get("notification_source");
//            }
//            if (source != null && source.equals("niki")) {
//            } else {
//                if (imageUri != null && !imageUri.isEmpty()) {
//                    bitmap = getBitmapFromUrl(imageUri);
//                    sendNotificationImage(message, bitmap, title);
//                }else
//                {
//                    sendNotification(message, title);
//                }
//            }
//
//
//        }
//
//    }
//
//    private void sendNotificationImage(String messageBody, Bitmap image, String title) {
//        Intent intent = new Intent(this, SplashActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
//                PendingIntent.FLAG_ONE_SHOT);
//
//        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
//                .setLargeIcon(image)
//                .setSmallIcon(getNotificationIcon())
//                .setContentTitle(title)
//                .setColor(getResources().getColor(R.color.white_text))
//                .setStyle(new NotificationCompat.BigPictureStyle()
//                        .bigPicture(image))
//                .setAutoCancel(true)
//                .setSound(defaultSoundUri)
//                .setContentText(messageBody)
//                .setContentIntent(pendingIntent);
//
//        NotificationManager notificationManager =
//                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//
//        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
//    }
//
//    public Bitmap getBitmapFromUrl(String imageUrl) {
//        try {
//            URL url = new URL(imageUrl);
//            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
//            connection.setDoInput(true);
//            connection.connect();
//            InputStream input = connection.getInputStream();
//            Bitmap bitmap = BitmapFactory.decodeStream(input);
//            return bitmap;
//
//        } catch (Exception e) {
//             TODO Auto-generated catch block
//            e.printStackTrace();
//            return null;
//
//        }
//    }
//
//    private int getNotificationIcon() {
//        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
//        return useWhiteIcon ? R.drawable.dushman : R.drawable.ic_notification;
//    }
//
//    private void sendNotification(String msg, String title) {
//        mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
//        Intent myIntent = new Intent(this, SplashActivity.class);
//        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, myIntent, PendingIntent.FLAG_UPDATE_CURRENT);
//
//        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
//                .setSmallIcon(R.drawable.dushman)
//                .setContentTitle(title)
//                .setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
//                .setAutoCancel(true)
//                .setContentText(msg);
//        mBuilder.setContentIntent(contentIntent);
//        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
//    }
//
//    private void showNotification(String mes, String url, String title) {
//        try {
//            NotificationCompat.BigPictureStyle notiStyle = new NotificationCompat.BigPictureStyle();
//            notiStyle.setSummaryText(mes);
//
//            try {
//                remote_picture = BitmapFactory.decodeStream((InputStream) new URL(url).getContent());
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//
//            notiStyle.bigPicture(remote_picture);
//
//            NotificationManager notificationManager = (NotificationManager) MyFirebaseMessagingService.this
//                    .getSystemService(Context.NOTIFICATION_SERVICE);
//            PendingIntent contentIntent = null;
//
//            Intent gotoIntent = new Intent(getApplicationContext(), SplashActivity.class);
//            contentIntent = PendingIntent.getActivity(getApplicationContext(),
//                    (int) (Math.random() * 100), gotoIntent,
//                    PendingIntent.FLAG_UPDATE_CURRENT);
//
//            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
//                    getApplicationContext());
//
//            Notification notification = mBuilder.setSmallIcon(R.drawable.ic_notification).setTicker(getResources().getString(R.string.app_name)).setWhen(0)
//                    .setAutoCancel(true)
//                    .setContentTitle(title)
//                    .setStyle(new NotificationCompat.BigTextStyle().bigText(mes))
//                    .setContentIntent(contentIntent)
//                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
//                    .setContentText(mes)
//                    .setStyle(notiStyle).build();
//
//            notification.flags = Notification.FLAG_AUTO_CANCEL;
//            notificationManager.notify(100, notification);//This will generate seperate notification each time server sends.
//
//        } catch (Throwable e) {
//            e.printStackTrace();
//        }
//    }
//}
//
//


//package in.msewa.vpayqwik.firebase;

//
//import android.annotation.SuppressLint;
//import android.annotation.TargetApi;
//import android.app.ActivityManager;
//import android.app.Notification;
//import android.app.NotificationChannel;
//import android.app.NotificationManager;
//import android.app.PendingIntent;
//import android.content.ComponentName;
//import android.content.Context;
//import android.content.Intent;
//import android.content.res.Resources;
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.graphics.Color;
//import android.media.RingtoneManager;
//import android.net.Uri;
//import android.os.Build;
//import android.support.v4.app.NotificationCompat;
//import android.text.TextUtils;
//import android.util.DisplayMetrics;
//import android.util.Log;
//import android.widget.RemoteViews;
//
//import com.google.firebase.messaging.FirebaseMessagingService;
//import com.google.firebase.messaging.RemoteMessage;

//import java.io.IOException;
//import java.io.InputStream;
//import java.net.HttpURLConnection;
//import java.net.URL;
//import java.util.List;
//import java.util.Random;

//import in.msewa.vpayqwik.NotificationMessage;
//import in.msewa.vpayqwik.R;
//import in.msewa.vpayqwik.activity.SplashActivity;
//import in.msewa.vpayqwik.broadcast.GcmBroadcastReceiver;
//
//import static android.media.RingtoneManager.getDefaultUri;


/**
 * Created by Ksf on 10/24/2016.
 */
//public class MyFirebaseMessagingService extends FirebaseMessagingService {
//  private static final String TAG = "FCM Service";
//  private NotificationManager mNotificationManager;
//  private Bitmap remote_picture;
//
//  private static final int NOTIFICATION_ID = 1;
//  private static final String NOTIFICATION_CHANNEL_ID = "my_notification_channel";
//
//
//  Bitmap bitmap;
//
//
//  @Override
//  public void onMessageReceived(RemoteMessage remoteMessage) {
//    super.onMessageReceived(remoteMessage);
//    if (remoteMessage!=null&&remoteMessage.getFrom().equalsIgnoreCase("369923058444")) {
//      String message = remoteMessage.getData().get("message");
//      String imageUri = remoteMessage.getData().get("image");
//      String title = remoteMessage.getData().get("title");
//      String source = "";
//      if (remoteMessage.getData().containsKey("notification_source")) {
//        source = remoteMessage.getData().get("notification_source");
//      }
//
//      if (source != null && source.equals("niki")) {
//
//      } else {
//        if (!TextUtils.isEmpty(imageUri)) {
//          try {
//            sendNotificationImage(message, getBitmapfromUrl(imageUri), title, imageUri);
//          } catch (IOException e) {
//          }
//        } else {
//          sendNotification(message, title);
//
//        }
//      }
//
//
//    }
//
//  }
//}
//

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;
import android.widget.RemoteViews;

import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import in.MadMoveGlobal.EwireRuPay.MyJobService;
import in.MadMoveGlobal.EwireRuPay.NotificationMessage;
import in.MadMoveGlobal.EwireRuPay.R;

//  public static float getImageFactor(Resources r) {
//    DisplayMetrics metrics = r.getDisplayMetrics();
//    float multiplier = metrics.density / 3f;
//    return multiplier;
//  }
//
//  /*
//   *To get a Bitmap image from the URL received
//   * */
//
//
//  private void sendNotification(String msg, String title) {
//    Intent myIntent = new Intent(MyFirebaseMessagingService.this, NotificationMessage.class);
//    myIntent.putExtra("Tittle", title);
//    myIntent.putExtra("message", msg);
//    myIntent.putExtra("image", "null");
//
//    PendingIntent pendingIntent = PendingIntent.getActivity(MyFirebaseMessagingService.this, 0, myIntent,
//      PendingIntent.FLAG_UPDATE_CURRENT);
//
//    NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
//    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//      @SuppressLint("WrongConstant") NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "My Notifications", NotificationManager.IMPORTANCE_DEFAULT);
//
//      // Configure the notification channel.
//      notificationChannel.setDescription("Channel description");
//      notificationChannel.enableLights(true);
//      notificationChannel.setLightColor(Color.RED);
//      notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
//      notificationChannel.enableVibration(true);
//      notificationManager.createNotificationChannel(notificationChannel);
//    }
//
//    NotificationCompat.Builder builder = new NotificationCompat.Builder(MyFirebaseMessagingService.this, NOTIFICATION_CHANNEL_ID)
//      .setVibrate(new long[]{0, 100, 100, 100, 100, 100})
//      .setSmallIcon(R.mipmap.ic_launcher)
//      .setContentTitle(title)
//      .setContentText(msg)
//      .setAutoCancel(true)
//      .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
//      .setContentIntent(pendingIntent);
//    notificationManager.notify(generateRandom(), builder.build());
////    GcmBroadcastReceiver.completeWakefulIntent(myIntent);
//  }
//
//  public int generateRandom() {
//    Random random = new Random();
//    return random.nextInt(9999 - 1000) + 1000;
//  }
//
//  private void sendNotificationLatest(String messageBody, Bitmap image, String TrueOrFalse) {
//    Intent intent = new Intent(this, NotificationMessage.class);
//    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//    intent.putExtra("AnotherActivity", TrueOrFalse);
//    PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
//      PendingIntent.FLAG_ONE_SHOT);
//
//    Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//    NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
//      .setLargeIcon(image)/*Notification icon image*/
//      .setSmallIcon(R.drawable.ic_vpayqwiklog)
//      .setContentTitle(messageBody)
//      .setStyle(new NotificationCompat.BigPictureStyle().bigPicture(image))/*Notification with Image*/
//      .setAutoCancel(true)
//      .setSound(defaultSoundUri)
//      .setContentIntent(pendingIntent);
//
//    NotificationManager notificationManager =
//      (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//
//    notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
//  }
//
//  /*
//   *To get a Bitmap image from the URL received
//   * */
////  public Bitmap getBitmapfromUrl(String imageUrl) {
////    try {
////      URL url = new URL(imageUrl);
////      HttpURLConnection connection = (HttpURLConnection) url.openConnection();
////      connection.setDoInput(true);
////      connection.connect();
////      InputStream input = connection.getInputStream();
////      Bitmap bitmap = BitmapFactory.decodeStream(input);
////      return bitmap;
////
////    } catch (Exception e) {
////      // TODO Auto-generated catch block
////      e.printStackTrace();
////      return null;
////
////    }
////  }
//
//
//}
////
//  public static float getImageFactor(Resources r) {
//    DisplayMetrics metrics = r.getDisplayMetrics();
//    float multiplier = metrics.density / 3f;
//    return multiplier;
//  }
//
//  /*
//   *To get a Bitmap image from the URL received
//   * */
//
//
//  private void sendNotification(String msg, String title) {
//    Intent myIntent = new Intent(MyFirebaseMessagingService.this, NotificationMessage.class);
//    myIntent.putExtra("Tittle", title);
//    myIntent.putExtra("message", msg);
//    myIntent.putExtra("image", "null");
//
//    PendingIntent pendingIntent = PendingIntent.getActivity(MyFirebaseMessagingService.this, 0, myIntent,
//      PendingIntent.FLAG_UPDATE_CURRENT);
//
//    NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
//    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//      @SuppressLint("WrongConstant") NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "My Notifications", NotificationManager.IMPORTANCE_DEFAULT);
//
//      // Configure the notification channel.
//      notificationChannel.setDescription("Channel description");
//      notificationChannel.enableLights(true);
//      notificationChannel.setLightColor(Color.RED);
//      notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
//      notificationChannel.enableVibration(true);
//      notificationManager.createNotificationChannel(notificationChannel);
//    }
//
//    NotificationCompat.Builder builder = new NotificationCompat.Builder(MyFirebaseMessagingService.this, NOTIFICATION_CHANNEL_ID)
//      .setVibrate(new long[]{0, 100, 100, 100, 100, 100})
//      .setSmallIcon(R.mipmap.ic_launcher)
//      .setContentTitle(title)
//      .setContentText(msg)
//      .setAutoCancel(true)
//      .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
//      .setContentIntent(pendingIntent);
//    notificationManager.notify(generateRandom(), builder.build());
////    GcmBroadcastReceiver.completeWakefulIntent(myIntent);
//  }
//
//  public int generateRandom() {
//    Random random = new Random();
//    return random.nextInt(9999 - 1000) + 1000;
//  }
//
//  private void sendNotificationLatest(String messageBody, Bitmap image, String TrueOrFalse) {
//    Intent intent = new Intent(this, NotificationMessage.class);
//    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//    intent.putExtra("AnotherActivity", TrueOrFalse);
//    PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
//      PendingIntent.FLAG_ONE_SHOT);
//
//    Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//    NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
//      .setLargeIcon(image)/*Notification icon image*/
//      .setSmallIcon(R.drawable.ic_vpayqwiklog)
//      .setContentTitle(messageBody)
//      .setStyle(new NotificationCompat.BigPictureStyle().bigPicture(image))/*Notification with Image*/
//      .setAutoCancel(true)
//      .setSound(defaultSoundUri)
//      .setContentIntent(pendingIntent);
//
//    NotificationManager notificationManager =
//      (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//
//    notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
//  }
//
//  /*
//   *To get a Bitmap image from the URL received
//   * */
////  public Bitmap getBitmapfromUrl(String imageUrl) {
////    try {
////      URL url = new URL(imageUrl);
////      HttpURLConnection connection = (HttpURLConnection) url.openConnection();
////      connection.setDoInput(true);
////      connection.connect();
////      InputStream input = connection.getInputStream();
////      Bitmap bitmap = BitmapFactory.decodeStream(input);
////      return bitmap;
////
////    } catch (Exception e) {
////      // TODO Auto-generated catch block
////      e.printStackTrace();
////      return null;
////
////    }
////  }
//
//
//}
////


public class MyFirebaseMessagingService extends FirebaseMessagingService {


    private static final String TAG = "MyFirebaseMsgService";
    private static final String NOTIFICATION_CHANNEL_ID = "1";

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from  Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(final RemoteMessage remoteMessage) {
        // Check if message contains a data payload.

        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            public void run() {

                if (remoteMessage.getData().size() > 0) {


                    if (/* Check if data needs to be processed by long running job */ true) {
                        // For long-running tasks (10 seconds or more) use Firebase Job Dispatcher.
                        scheduleJob(remoteMessage);
                    } else {
                        // Handle message within 10 seconds
                        handleNow();
                    }

                }

                // Check if message contains a notification payload.
                if (remoteMessage.getNotification() != null) {
                    Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
                }

            }
        });


        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
    // [END receive_message]

    /**
     * Schedule a job using FirebaseJobDispatcher.
     *
     * @param remoteMessage
     */
    private void scheduleJob(final RemoteMessage remoteMessage) {
        // [START dispatch_job]
        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(this));
        Job myJob = dispatcher.newJobBuilder()
                .setService(MyJobService.class)
                .setTag("my-job-tag")
                .build();
        dispatcher.schedule(myJob);

        final String message = remoteMessage.getData().get("message");
        final String imageUri = remoteMessage.getData().get("image");
        final String title = remoteMessage.getData().get("title");
        String source = "";
        if (remoteMessage.getData().containsKey("notification_source")) {
            source = remoteMessage.getData().get("notification_source");
        }

        if (source != null && source.equals("niki")) {

        } else {
            if (!TextUtils.isEmpty(imageUri)) {

                Handler handler = new Handler(Looper.getMainLooper());
                handler.post(new Runnable() {
                    public void run() {
                        try {
                            sendNotificationImage(message, getBitmapfromUrl(remoteMessage.getData().get("image")), title, imageUri);
                        } catch (IOException e) {
                        }
                    }
                });


            } else {
                Handler handler = new Handler(Looper.getMainLooper());
                handler.post(new Runnable() {
                    public void run() {
                        sendNotification(message, title);
                    }
                });
            }
        }

        // [END dispatch_job]
    }

    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private void handleNow() {
        Log.d(TAG, "Short lived task is done.");
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void sendNotification(String messageBody, String tittle) {
        Intent intent = new Intent(MyFirebaseMessagingService.this, NotificationMessage.class);
        intent.putExtra("Tittle", tittle);
        intent.putExtra("image", "null");
        intent.putExtra("message", messageBody);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(tittle)
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            @SuppressLint("WrongConstant") NotificationChannel channel = new NotificationChannel(channelId, "Channel human readable title", NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void sendNotificationImage(String messageBody, Bitmap image, String title, String imageUri) throws IOException {
        Intent intent = new Intent(MyFirebaseMessagingService.this, NotificationMessage.class);
        intent.putExtra("Tittle", title);
        intent.putExtra("image", imageUri);
        intent.putExtra("message", messageBody);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, (int) 0 /* Request code */, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            @SuppressLint("WrongConstant") NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "My Notifications", NotificationManager.IMPORTANCE_DEFAULT);
            // Configure the notification channel.
            notificationChannel.setDescription("Channel description");
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.enableVibration(true);
            notificationChannel.setDescription(messageBody);
            notificationChannel.setName(title);
            notificationManager.createNotificationChannel(notificationChannel);

        }
        RemoteViews contentView = new RemoteViews(getPackageName(), R.layout.fcmlayout);
        contentView.setImageViewResource(R.id.image, R.mipmap.ic_launcher);
        contentView.setTextViewText(R.id.title, title);
        contentView.setTextViewText(R.id.text, messageBody);
        contentView.setImageViewBitmap(R.id.ivLayout, image);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(MyFirebaseMessagingService.this, NOTIFICATION_CHANNEL_ID)
                .setVibrate(new long[]{0, 100, 100, 100, 100, 100})
                .setCustomBigContentView(contentView)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(true)
                .setContentText(messageBody)
                .setContentTitle(title)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setContentIntent(pendingIntent);
        Notification notification = mBuilder.build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notification.defaults |= Notification.DEFAULT_SOUND;
        notification.defaults |= Notification.DEFAULT_VIBRATE;
        notificationManager.notify(123, notification);
//    GcmBroadcastReceiver.completeWakefulIntent(intent);

    }

//    public Bitmap getBitmapfromUrl(String imageUrl) {
//        try {
//            URL url = new URL(imageUrl);
//            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
//            connection.setDoInput(true);
//            connection.connect();
//            InputStream input = connection.getInputStream();
//            Bitmap bitmap = BitmapFactory.decodeStream(input);
//            return bitmap;
//
//        } catch (Exception e) {
//             TODO Auto-generated catch block
//            e.printStackTrace();
//            return null;
//
//        }
//    }

    public Bitmap getBitmapfromUrl(String imageUrl) {
        Log.i("IMAGEURL", imageUrl);

        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.setConnectTimeout(50000);
            connection.setReadTimeout(50000);
            connection.setInstanceFollowRedirects(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(input);
            return bitmap;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;

        }
    }
}


//    class getBitmapTask extends AsyncTask<String, Void, Bitmap> {
//
//        private Exception exception;
//
////        protected Bitmap doInBackground(String... urls) {
////            try {
////                URL url = new URL(imageUrl);
////                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
////                connection.setDoInput(true);
////                connection.setConnectTimeout(50000);
////                connection.setReadTimeout(50000);
////                connection.setInstanceFollowRedirects(true);
////                connection.connect();
////                InputStream input = connection.getInputStream();
////                Bitmap bitmap = BitmapFactory.decodeStream(input);
////                return bitmap;
////
////            } catch (Exception e) {
////                 TODO Auto-generated catch block
////                e.printStackTrace();
////                return null;
////
////            }
////        }
////
////        protected void onPostExecute(Bitmap feed) {
////             TODO: check this.exception
////             TODO: do something with the feed
//
////        }
////    }
//}
