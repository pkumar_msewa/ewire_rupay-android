package in.MadMoveGlobal.EwireRuPay;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.webkit.ConsoleMessage;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;

import org.apache.http.util.EncodingUtils;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.SSLSocketFactory;

import in.MadMoveGlobal.EwireRuPay.activity.MainActivity;
import in.MadMoveGlobal.custom.CustomAlertDialog;
import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.metadata.AppMetadata;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.util.ServiceUtility;
import in.MadMoveGlobal.util.TLSSocketFactory;
import in.MadMoveGlobal.utility.Constant1;


public class WebViewPayfiActivity extends AppCompatActivity {
    private ProgressDialog dialog;
    Intent mainIntent;
    private LinearLayout llLoadMoneyWebViewLoading;
    String html, encVal, amountToLoad, transRefNo;
    private UserModel userModel = UserModel.getInstance();
    private LoadingDialog loadingDialog;
    private RequestQueue rq;
    private String type, URL, REQUESTOBJECT;
    private String SUBTYPE;
    private boolean loadmoney;

    @SuppressLint("RestrictedApi")
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
//        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(WebViewActivity.this, TargetActivity.class));
        setContentView(R.layout.activity_ebs_web);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbars);
        setSupportActionBar(toolbar);

        ImageView ivToolbarLogo = (ImageView) findViewById(R.id.ivpqlogo);
//        ivToolbarLogo.setImageBitmap(BitmapHelper.decodeSampledBitmapFromResource(getResources(), R.drawable.toolbart_niki, ivToolbarLogo.getMaxWidth(), ivToolbarLogo.getMaxHeight()));
        this.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        this.getSupportActionBar().setDisplayShowTitleEnabled(false);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        amountToLoad = getIntent().getStringExtra("amount");
        transRefNo = getIntent().getStringExtra("transactionRefNo");


        Integer randomNum = ServiceUtility.randInt(0, 9999999);
        loadingDialog = new LoadingDialog(WebViewPayfiActivity.this);
        html = randomNum.toString();
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog();
            }
        });
        llLoadMoneyWebViewLoading = (LinearLayout) findViewById(R.id.llLoadMoneyWebViewLoading);
//        if (Integer.parseInt(amountToLoad) <= 5000) {
//            try {
        getTranscation();
        llLoadMoneyWebViewLoading.setVisibility(View.GONE);
//            } catch (NullPointerException e) {
//            }
//        } else {

//            CustomToast.showMessage(WebViewActivity.this, "Your Per Day Transaction limit is 5000");
//            finish();
//
//        }
    }

    public void showDialog() {
        android.support.v7.app.AlertDialog.Builder exitDialog =
                new android.support.v7.app.AlertDialog.Builder(WebViewPayfiActivity.this, R.style.AppCompatAlertDialogStyle);
        exitDialog.setTitle(R.string.cancel_load);
        exitDialog.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                finish();
            }
        });
        exitDialog.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        exitDialog.show();
    }


    public void showToast(String msg) {
        Toast.makeText(this, "Toast: " + msg, Toast.LENGTH_LONG).show();
    }

    void getTranscation() {
        final String[] value = new String[1];
        try {
            final SSLSocketFactory sslSocketFactory = new TLSSocketFactory();
            rq = Volley.newRequestQueue(WebViewPayfiActivity.this, new HurlStack(null, sslSocketFactory));
        } catch (KeyManagementException | NoSuchAlgorithmException e) {
        }

//        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiUrl.URL_MAIN + "LoadMoney/RedirectUPI", new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.i("Rsponse", response);
//                try {
//                    JSONObject jsonObject = new JSONObject(response);
//                    loadingDialog.dismiss();
//                    if (jsonObject.getString("code").equals("S00")) {
//                        final String value = jsonObject.getString("orderId");
//                        final String access_code = jsonObject.getString("accessCode");
//
//                        Log.i("acccode", access_code);
//                        loadingDialog.show();

//        JSONObject jsonBody = new JSONObject();
//        try {
//
//            jsonBody.put("amount", amountToLoad);
//            jsonBody.put("sessionId", userModel.getUserSessionId());
//            jsonBody.put("transactionRefNo", transRefNo);
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        final String requestBody = jsonBody.toString();
//        Log.i("REDUPI", requestBody.toString());
//        Log.i("REDUPIURL", URL_MAIN+"LoadMoney/RedirectUPI");
//
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_MAIN + "LoadMoney/RedirectUPI", new Response.Listener<String>() {
//
//            @SuppressLint({"SetJavaScriptEnabled", "AddJavascriptInterface"})
//            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
//            @Override
//            public void onResponse(String response) {
//                Log.i("RSA RESPONSE", response);
//                loadingDialog.dismiss();
//                if (!ServiceUtility.chkNull(response).equals("")
//                        && !ServiceUtility.chkNull(response).toString().contains("!ERROR!Caller IP not registered/Merchant Not found.")) {
//                    StringBuffer vEncVal = new StringBuffer("");
//                    vEncVal.append(ServiceUtility.addToPostParams(AvenuesParams.CARD_NUMBER, ""));
//                    vEncVal.append(ServiceUtility.addToPostParams(AvenuesParams.EXPIRY_YEAR, ""));
//                    vEncVal.append(ServiceUtility.addToPostParams(AvenuesParams.EXPIRY_MONTH, ""));
//                    vEncVal.append(ServiceUtility.addToPostParams(AvenuesParams.CVV, ""));
////                    vEncVal.append(ServiceUtility.addToPostParams(AvenuesParams.ACCESS_CODE, access_code));
//                    vEncVal.append(ServiceUtility.addToPostParams(AvenuesParams.AMOUNT, amountToLoad));
//                    vEncVal.append(ServiceUtility.addToPostParams(AvenuesParams.CURRENCY, "INR"));
//                    vEncVal.append(ServiceUtility.addToPostParams(AvenuesParams.CUSTOMER_IDENTIFIER, "8553926329"));
//                    Log.i("AZAHAR", "vEncVal : "+String.valueOf(vEncVal)+"vEncVal length : "+vEncVal.length());
////                    encVal = RSAUtility.encrypt(vEncVal.substring(0, vEncVal.length() - 1), response);
//                    if (encVal != null || !encVal.equals("")) {
        @SuppressWarnings("unused")
        class MyJavaScriptInterface {
            @JavascriptInterface
            public void processHTML(String html) {
//                Log.i("logHtml", html);
                // process the html as needed by the app
                String status = null;
                if (html.contains("Failure")) {
                    status = "Failed";
                } else if (html.contains("Success")) {
                    status = "Success";
                } else if (html.contains("Aborted")) {
                    status = "Failed";

                } else {
                    status = "Status Not Known!";
                }

                Intent i = new Intent(WebViewPayfiActivity.this, LoadMoneyStatusActivity.class);
                i.putExtra(LoadMoneyStatusActivity.EXTRA, status);
//                                                i.putExtra("TYPE", type);
//                                                if (SUBTYPE != null && !SUBTYPE.equalsIgnoreCase("")) {
//                                                    i.putExtra("SUBTYPE", SUBTYPE);
//                                                }

                i.putExtra("amount", amountToLoad);
                i.putExtra("transactionRefNo", transRefNo);
//                                                i.putExtra("REQUESTOBJECT", REQUESTOBJECT);
                startActivity(i);
            }
        }
        final WebView webview = (WebView) findViewById(R.id.webview);
        webview.getSettings().setDomStorageEnabled(true);
        webview.addJavascriptInterface(new MyJavaScriptInterface(), "HTMLOUT");
//                                        String ua = "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.4) Gecko/20100101 Firefox/4.0";
//                                        webview.getSettings().setUserAgentString(ua);

        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setLoadWithOverviewMode(true);
        webview.getSettings().setUseWideViewPort(true);

        webview.getSettings().setSupportZoom(true);
        webview.getSettings().setBuiltInZoomControls(true);
        webview.getSettings().setDisplayZoomControls(false);

        webview.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webview.setScrollbarFadingEnabled(false);
        llLoadMoneyWebViewLoading.setVisibility(View.GONE);
        webview.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(webview, url);
                llLoadMoneyWebViewLoading.setVisibility(View.GONE);
//                view.loadUrl("javascript:document.getElementById(\"ShippingInformation\").setAttribute(\"style\",\"display:none;\");");
//
//                Log.i("UELURL", url);

                if (url.contentEquals("https://liveewire.com:8443/Api/v1/User/Android/en/LoadMoney/PGHandler")) {

//                    webview.setVisibility(View.GONE);
//                    llLoadMoneyWebViewLoading.setVisibility(View.VISIBLE);
                    Intent shoppingIntent = new Intent(WebViewPayfiActivity.this, MainActivity.class);
                    startActivity(shoppingIntent);
                    sendRefresh();
                    finish();

                }
                //https://liveewire.com:8443/Api/v1/User/Android/en/LoadMoney/PGHandler
//                else if(url.contentEquals("https://liveewire.com:8443/Api/v1/User/Android/en/LoadMoney/PGHandler")){
//                    sendRefresh();
//                    Intent shoppingIntent = new Intent(WebViewPayfiActivity.this, MainActivity.class);
//                    startActivity(shoppingIntent);
//                    finish();
//
//                }
//                                    view.stopLoading();
// if (true) {
//
//                                    webview.loadUrl("javascript:window.HTMLOUT.processHTML('<head>'+document.getElementsByTagName('html')[0].innerHTML+'</head>');");
//                                    webview.setVisibility(View.GONE);
//
//
//                                } else if (url.contentEquals("/Cancel")) {
//                                    view.stopLoading();
//
//                                    showInvalidCancleDialog();
//                                }
            }


            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Log.d("erroecode", String.valueOf(errorCode));
                llLoadMoneyWebViewLoading.setVisibility(View.VISIBLE);
                showErrorDialog(errorCode);

//                                                Toast.makeText(getApplicationContext(), "Oh no! " + description, Toast.LENGTH_SHORT).show();

            }


            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                llLoadMoneyWebViewLoading.setVisibility(View.VISIBLE);
//                Log.i("UEL", url);

                if (url.contains("LoadMoney/Cancel")) {
                    showInvalidCancleDialog();
                    view.stopLoading();
                }

            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                super.onReceivedSslError(view, handler, error);
                handler.proceed();

            }
        });
        webview.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onConsoleMessage(ConsoleMessage cm) {
                Log.d("TAG", cm.message() + " at " + cm.sourceId() + ":" + cm.lineNumber());
//                if (cm.sourceId() == "http://13.127.101.55/IplCards/Api/v1/User/Android/en/LoadMoney/SuccessUPI") {
//                    Intent intent = new Intent(WebViewActivity.this, MainActivity.class);
//                    startActivity(intent);
//                    finish();
//                }
                return true;
            }

        });
//                        webview.loadData(response,"","UTF-8");
//                webview.loadData(response, "text/html; charset=UTF-8", null);


        StringBuffer params = new StringBuffer();
        params.append(ServiceUtility.addToPostParams("sessionId", userModel.getUserSessionId()));
        params.append(ServiceUtility.addToPostParams("amount", amountToLoad));
        params.append(ServiceUtility.addToPostParams("transactionRefNo", transRefNo));
//                        params.append(ServiceUtility.addToPostParams(AvenuesParams.MERCHANT_ID, "150979"));
//                        params.append(ServiceUtility.addToPostParams(AvenuesParams.ORDER_ID, value));
//                        params.append(ServiceUtility.addToPostParams(AvenuesParams.REDIRECT_URL, ApiUrl.URL_VALIDATE_CARD_TRX_TIMEUPI));
//                        params.append(ServiceUtility.addToPostParams(AvenuesParams.CANCEL_URL, ApiUrl.CANCLE));
//                        params.append(ServiceUtility.addToPostParams(AvenuesParams.LANGUAGE, "EN"));
//                        params.append(ServiceUtility.addToPostParams(AvenuesParams.BILLING_NAME, userModel.getUserFirstName()));
//                        params.append(ServiceUtility.addToPostParams(AvenuesParams.BILLING_ADDRESS, "bangalore"));
//                        params.append(ServiceUtility.addToPostParams(AvenuesParams.BILLING_CITY, "bangalore"));
//                        params.append(ServiceUtility.addToPostParams(AvenuesParams.BILLING_STATE, "bangalore"));
//                        params.append(ServiceUtility.addToPostParams(AvenuesParams.BILLING_ZIP, "560040"));
//                        params.append(ServiceUtility.addToPostParams(AvenuesParams.BILLING_COUNTRY, "India"));
//                        params.append(ServiceUtility.addToPostParams(AvenuesParams.BILLING_TEL, userModel.getUserMobileNo()));
//                        params.append(ServiceUtility.addToPostParams(AvenuesParams.BILLING_EMAIL, userModel.getUserEmail()));
//                        params.append(ServiceUtility.addToPostParams(AvenuesParams.DELIVERY_NAME, userModel.getUserFirstName()));
//                        params.append(ServiceUtility.addToPostParams(AvenuesParams.DELIVERY_ADDRESS, "bangalore"));
//                        params.append(ServiceUtility.addToPostParams(AvenuesParams.DELIVERY_CITY, "bangalore"));
//                        params.append(ServiceUtility.addToPostParams(AvenuesParams.DELIVERY_STATE, "kannada"));
//                        params.append(ServiceUtility.addToPostParams(AvenuesParams.DELIVERY_ZIP, "560040"));
//                        params.append(ServiceUtility.addToPostParams(AvenuesParams.DELIVERY_COUNTRY, "India"));
//                        params.append(ServiceUtility.addToPostParams(AvenuesParams.DELIVERY_TEL, userModel.getUserMobileNo()));
//                        params.append(ServiceUtility.addToPostParams(AvenuesParams.MERCHANT_PARAM1, "additional Info."));
//                        params.append(ServiceUtility.addToPostParams(AvenuesParams.MERCHANT_PARAM2, "additional Info."));
//                        params.append(ServiceUtility.addToPostParams(AvenuesParams.MERCHANT_PARAM3, "additional Info."));
//                        params.append(ServiceUtility.addToPostParams(AvenuesParams.MERCHANT_PARAM4, "additional Info."));
//                        params.append(ServiceUtility.addToPostParams(AvenuesParams.MERCHANT_PARAM5, ""));
//                        params.append(ServiceUtility.addToPostParams(AvenuesParams.PAYMENT_OPTION, ""));
//                        params.append(ServiceUtility.addToPostParams(AvenuesParams.CARD_TYPE, ""));
//                        params.append(ServiceUtility.addToPostParams(AvenuesParams.CARD_NAME, ""));
//                        params.append(ServiceUtility.addToPostParams(AvenuesParams.DATA_ACCEPTED_AT, ""));
//                        params.append(ServiceUtility.addToPostParams(AvenuesParams.ISSUING_BANK, ""));
//                        params.append(ServiceUtility.addToPostParams(AvenuesParams.ENC_VAL, URLEncoder.encode(encVal)));
//                        params.append(ServiceUtility.addToPostParams(AvenuesParams.EMI_PLAN_ID, ""));
//                        params.append(ServiceUtility.addToPostParams(AvenuesParams.EMI_TENURE_ID, ""));

        String vPostParams = params.substring(0, params.length() - 1);
//        Log.i("Load Value", vPostParams);
        try {
            webview.postUrl(Constant1.TRANS_PAYFI_URL, EncodingUtils.getBytes(vPostParams, "UTF-8"));
//            Log.i("LOADURL", Constant1.TRANS_PAYFI_URL);
        } catch (Exception e) {
            showToast("Exception occurred while opening Webview.");

        }

//        Intent intent = new Intent(WebViewActivity.this, MainActivity.class);
//        startActivity(intent);
//        finish();
//                    } else {
//                        CustomToast.showMessage(WebViewActivity.this, "Some thing went wrong,Please try after some time");
//                        finish();
//                    }
//                } else {
//                    loadingDialog.dismiss();
//                    CustomToast.showMessage(WebViewActivity.this, "!ERROR!Caller IP not registered/Merchant Not found.");
//                    finish();
    }
//
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                loadingDialog.dismiss();
//                finish();
//                try {
//                    CustomToast.showMessage(WebViewActivity.this, NetworkErrorHandler.getMessage(error, WebViewActivity.this));
//                } catch (NullPointerException e) {
//                }
//            }
//        }) {

//            @Override
//            public byte[] getBody() throws AuthFailureError {
//                try {
//                    return requestBody == null ? null : requestBody.getBytes("utf-8");
//                } catch (UnsupportedEncodingException uee) {
//                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
//                    return null;
//                }


//            @Override
//            protected JSONObject getParams() throws AuthFailureError {
//                JSONObject map = new JSONObject();
//                map.put("access_code", access_code);
//                map.put("transactionNo", value);
//                try {
//                    map.put("amount", amountToLoad);
//                    map.put("sessionId", userModel.getUserSessionId());
//                    map.put("transactionRefNo", transRefNo);
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//                Log.i("VALE", map.toString());
//                return map;
//            }

//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> map = new HashMap<>();
//                map.put("Content-Type", "application/json");
//                return map;
//            }
//        };
//        int socketTimeout = 60000;
//        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//        stringRequest.setRetryPolicy(policy);
//        rq.add(stringRequest);
//
//
//    }
//    else if(response.equals("!ERROR!Caller IP not registered/Merchant Not found."))
//
//    {
//        CustomToast.showMessage(WebViewActivity.this, response);
//        loadingDialog.dismiss();
//        finish();
//    } else if(jsonObject.getString("code").
//
//    equalsIgnoreCase("f00"))
//
//    {
//        CustomToast.showMessage(WebViewActivity.this, jsonObject.getString("message"));
//        loadingDialog.dismiss();
//        finish();
//    }

//} catch(JSONException e){
//        loadingDialog.dismiss();
//        finish();
//
//        }
//        }
//        },new Response.ErrorListener(){
//@Override
//public void onErrorResponse(VolleyError error){
//        loadingDialog.dismiss();
//        finish();
//        try{
//        CustomToast.showMessage(WebViewActivity.this,NetworkErrorHandler.getMessage(error,WebViewActivity.this));
//        }catch(NullPointerException e){
//        }
//        }
//        }){
//@Override
//protected Map<String, String> getParams()throws AuthFailureError{
//        HashMap<String, String> map=new HashMap<>();
//
//        map.put("sessionId",userModel.getUserSessionId());
//        map.put("amount",amountToLoad);
//        map.put("transactionRefNo",transRefNo);
//                map.put("phone", userModel.getUserMobileNo());
//                map.put("useVnet", "true");
//                map.put("lmService", "LMC");
//                map.put("email", userModel.getUserEmail());

//        return map;
//
//        }
//        };
//        int socketTimeout=60000;
//        RetryPolicy policy=new DefaultRetryPolicy(socketTimeout,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//        stringRequest.setRetryPolicy(policy);
//        rq.add(stringRequest);
//        }

    private void sendRefresh() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "1");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    public static void clearCookies(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            CookieManager.getInstance().removeAllCookies(null);
            CookieManager.getInstance().flush();
        } else {
            CookieSyncManager cookieSyncMngr = CookieSyncManager.createInstance(context);
            cookieSyncMngr.startSync();
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.removeAllCookie();
            cookieManager.removeSessionCookie();
            cookieSyncMngr.stopSync();
            cookieSyncMngr.sync();
        }
    }

    public void showErrorDialog(int errorcode) {
        CustomAlertDialog builder = new CustomAlertDialog(WebViewPayfiActivity.this, R.string.dialog_title2, Html.fromHtml(AppMetadata.getErrorCode(errorcode)));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();

            }
        });

        builder.show();
    }

    @Override
    public void onBackPressed() {
        showDialog();
    }

    public void showInvalidCancleDialog() {

        CustomAlertDialog builder = new CustomAlertDialog(WebViewPayfiActivity.this, R.string.dialog_title2, "Your transaction is cancelled");
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();
            }
        });

        builder.show();
    }


}
