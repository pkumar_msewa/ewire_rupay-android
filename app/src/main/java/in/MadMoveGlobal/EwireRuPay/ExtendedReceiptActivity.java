package in.MadMoveGlobal.EwireRuPay;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import java.text.SimpleDateFormat;
import java.util.Date;

import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.EwireRuPay.activity.MainActivity;
import in.MadMoveGlobal.model.StatementModel;

public class ExtendedReceiptActivity extends AppCompatActivity {

    StatementModel statementModel;
    EditText status, date, indicator, amount, description;
    ImageView close;
    private Toolbar toolbar;
    private ImageButton ivBackBtn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_custom_redetails);

        statementModel = (StatementModel) getIntent().getParcelableExtra("statementModel");

        status = findViewById(R.id.status);
        date = findViewById(R.id.date);
        indicator = findViewById(R.id.indicator);
        amount = findViewById(R.id.amount);
        description = findViewById(R.id.description);


        toolbar = (Toolbar) findViewById(R.id.toolbars);
        ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtns);
        setSupportActionBar(toolbar);
        ivBackBtn.setVisibility(View.VISIBLE);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

//        close.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onBackPressed();
//            }
//        });

        printdata();

    }

    private void printdata() {


        String dt[] = statementModel.getDate().split("T");
        if (statementModel.getStatus().equalsIgnoreCase("success")) {
            status.setTextColor(getResources().getColor(R.color.green));
        } else
            status.setTextColor(getResources().getColor(R.color.mark_red));
        status.setText(statementModel.getStatus());
        date.setText(dt[0] + "\n" + "  " + dt[1].substring(0, 5));

        indicator.setText(statementModel.getIndicator());
        amount.setText(statementModel.getAmount());
        description.setText(statementModel.getDescription());
    }
}