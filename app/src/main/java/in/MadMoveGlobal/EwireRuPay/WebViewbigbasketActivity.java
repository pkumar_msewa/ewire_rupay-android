package in.MadMoveGlobal.EwireRuPay;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.webkit.ConsoleMessage;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;

import org.apache.http.util.EncodingUtils;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.SSLSocketFactory;

import in.MadMoveGlobal.EwireRuPay.activity.MainActivity;
import in.MadMoveGlobal.custom.CustomAlertDialog;
import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.metadata.AppMetadata;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.util.ServiceUtility;
import in.MadMoveGlobal.util.TLSSocketFactory;
import in.MadMoveGlobal.utility.Constant1;


public class WebViewbigbasketActivity extends AppCompatActivity {
    private ProgressDialog dialog;
    Intent mainIntent;
    private LinearLayout llLoadMoneyWebViewLoading;
    String html, encVal, amountToLoad, transRefNo;
    private UserModel userModel = UserModel.getInstance();
    private LoadingDialog loadingDialog;
    private RequestQueue rq;
    private String type, URL, REQUESTOBJECT;
    private String SUBTYPE;
    private boolean loadmoney;

    @SuppressLint("RestrictedApi")
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
//        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(WebViewActivity.this, TargetActivity.class));
        setContentView(R.layout.activity_ebs_web);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbars);
        setSupportActionBar(toolbar);

        ImageView ivToolbarLogo = (ImageView) findViewById(R.id.ivpqlogo);
//        ivToolbarLogo.setImageBitmap(BitmapHelper.decodeSampledBitmapFromResource(getResources(), R.drawable.toolbart_niki, ivToolbarLogo.getMaxWidth(), ivToolbarLogo.getMaxHeight()));
        this.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        this.getSupportActionBar().setDisplayShowTitleEnabled(false);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);


        Integer randomNum = ServiceUtility.randInt(0, 9999999);
        loadingDialog = new LoadingDialog(WebViewbigbasketActivity.this);
        html = randomNum.toString();
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                showDialog();
                Intent intent = new Intent(WebViewbigbasketActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
        llLoadMoneyWebViewLoading = (LinearLayout) findViewById(R.id.llLoadMoneyWebViewLoading);

        getTranscation();
        llLoadMoneyWebViewLoading.setVisibility(View.GONE);

    }


    public void showToast(String msg) {
        Toast.makeText(this, "Toast: " + msg, Toast.LENGTH_LONG).show();
    }

    void getTranscation() {
        final String[] value = new String[1];
        try {
            final SSLSocketFactory sslSocketFactory = new TLSSocketFactory();
            rq = Volley.newRequestQueue(WebViewbigbasketActivity.this, new HurlStack(null, sslSocketFactory));
        } catch (KeyManagementException | NoSuchAlgorithmException e) {
        }


        @SuppressWarnings("unused")
        class MyJavaScriptInterface {
            @JavascriptInterface
            public void processHTML(String html) {
//                Log.i("logHtml", html);
                // process the html as needed by the app
                String status = null;
                if (html.contains("Failure")) {
                    status = "Failed";
                } else if (html.contains("Success")) {
                    status = "Success";
                } else if (html.contains("Aborted")) {
                    status = "Failed";

                } else {
                    status = "Status Not Known!";
                }


            }
        }
        final WebView webview = (WebView) findViewById(R.id.webview);
        webview.getSettings().setDomStorageEnabled(true);
        webview.addJavascriptInterface(new MyJavaScriptInterface(), "HTMLOUT");

        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setLoadWithOverviewMode(true);
        webview.getSettings().setUseWideViewPort(true);

        webview.getSettings().setSupportZoom(true);
        webview.getSettings().setBuiltInZoomControls(true);
        webview.getSettings().setDisplayZoomControls(false);

        webview.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webview.setScrollbarFadingEnabled(false);
        llLoadMoneyWebViewLoading.setVisibility(View.GONE);
        webview.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(webview, url);
                llLoadMoneyWebViewLoading.setVisibility(View.GONE);

            }


            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Log.d("erroecode", String.valueOf(errorCode));
                llLoadMoneyWebViewLoading.setVisibility(View.VISIBLE);
                showErrorDialog(errorCode);


            }


            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                llLoadMoneyWebViewLoading.setVisibility(View.VISIBLE);


                if (url.contains("LoadMoney/Cancel")) {

                    view.stopLoading();
                    Intent intent = new Intent(WebViewbigbasketActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }

            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                super.onReceivedSslError(view, handler, error);
                handler.proceed();

            }
        });
        webview.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onConsoleMessage(ConsoleMessage cm) {
                Log.d("TAG", cm.message() + " at " + cm.sourceId() + ":" + cm.lineNumber());

                return true;
            }

        });

        try {
//            webview.postUrl("http://bigbasket.go2cloud.org/aff_c?offer_id=271&aff_id=3683", EncodingUtils.getBytes("", "UTF-8"));
            webview.postUrl("https://www.rupay.co.in/rupay-offers", EncodingUtils.getBytes("", "UTF-8"));

        } catch (Exception e) {
            showToast("Exception occurred while opening Webview.");

        }


    }


    private void sendRefresh() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "1");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    public static void clearCookies(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            CookieManager.getInstance().removeAllCookies(null);
            CookieManager.getInstance().flush();
        } else {
            CookieSyncManager cookieSyncMngr = CookieSyncManager.createInstance(context);
            cookieSyncMngr.startSync();
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.removeAllCookie();
            cookieManager.removeSessionCookie();
            cookieSyncMngr.stopSync();
            cookieSyncMngr.sync();
        }
    }

    public void showErrorDialog(int errorcode) {
        CustomAlertDialog builder = new CustomAlertDialog(WebViewbigbasketActivity.this, R.string.dialog_title2, Html.fromHtml(AppMetadata.getErrorCode(errorcode)));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();

            }
        });

        builder.show();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(WebViewbigbasketActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    public void showInvalidCancleDialog() {

        CustomAlertDialog builder = new CustomAlertDialog(WebViewbigbasketActivity.this, R.string.dialog_title2, "Your transaction is cancelled");
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();
            }
        });

        builder.show();
    }


}
