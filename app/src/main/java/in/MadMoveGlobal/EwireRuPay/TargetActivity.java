package in.MadMoveGlobal.EwireRuPay;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;


import org.json.JSONException;
import org.json.JSONObject;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.SSLSocketFactory;

import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.util.NetworkErrorHandler;
import in.MadMoveGlobal.util.TLSSocketFactory;
import in.MadMoveGlobal.EwireRuPay.activity.LoginRegActivity;

public class TargetActivity extends AppCompatActivity {

    private TextView textView;
    private RequestQueue rq;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_noticication);
        try {
            final SSLSocketFactory sslSocketFactory = new TLSSocketFactory();
            rq = Volley.newRequestQueue(TargetActivity.this, new HurlStack(null, sslSocketFactory));
        } catch (KeyManagementException e) {
        } catch (NoSuchAlgorithmException e) {
        }
        Button submit = (Button) findViewById(R.id.submit);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    final JSONObject jsonObject = new JSONObject();
                    jsonObject.put("description", getIntent().getStringExtra("feedback"));
//
                    final LoadingDialog loadDlg = new LoadingDialog(TargetActivity.this);
                    loadDlg.show();
                    JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_BANK_CRASH, jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            loadDlg.dismiss();
                            try {

                                String code = response.getString("code");

                                if (code.equals("S00")) {

                                    CustomToast.showMessage(TargetActivity.this, "Report is send to Developer");
                                    Intent intent = new Intent(TargetActivity.this, LoginRegActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
                                    finish();
                                    startActivity(intent);
                                } else {
                                    String message = response.getString("message");
                                    loadDlg.dismiss();
                                    CustomToast.showMessage(TargetActivity.this, message);
                                }

                            } catch (JSONException e) {
                                loadDlg.dismiss();
                                Log.i("REPORT", "weerror");
                                CustomToast.showMessage(TargetActivity.this, getResources().getString(R.string.server_exception2));
                            }
                        }
                    }, new Response.ErrorListener()

                    {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            loadDlg.dismiss();
                            Toast.makeText(TargetActivity.this, NetworkErrorHandler.getMessage(error, TargetActivity.this), Toast.LENGTH_SHORT).show();

                        }
                    })

                    {
                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            HashMap<String, String> map = new HashMap<>();
                            map.put("hash", "123");
                            return map;
                        }
                    };

                    int socketTimeout = 60000;
                    RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                    postReq.setRetryPolicy(policy);
                    rq.add(postReq);
                } catch (JSONException e) {
                    CustomToast.showMessage(TargetActivity.this, getResources().getString(R.string.server_exception2));
                }
            }
        });

    }
}