package in.MadMoveGlobal.EwireRuPay;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.orm.query.Select;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.MadMoveGlobal.EwireRuPay.activity.MainActivity;
import in.MadMoveGlobal.custom.CustomAlertDialog;
import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.loyaltyoffer.LoadingDialog;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.metadata.AppMetadata;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.util.MultipartInsuranceRequest;
import in.MadMoveGlobal.util.MultipartRequest;
import in.MadMoveGlobal.util.NetworkErrorHandler;
import in.MadMoveGlobal.util.Utility;

import static android.content.Context.INPUT_METHOD_SERVICE;


/**
 * A simple {@link Fragment} subclass.
 */
public class VehicleInsuranceFragment extends Fragment implements View.OnClickListener {
    public static final String TAG = VehicleInsuranceFragment.class.getSimpleName();
    private TextInputEditText mobileTIEditText, emailTIEditText;
    private Button submitButton;
    private ImageView frontImageView, backImageView;
    private boolean isFrontImage, isBackImage;
    private Uri imageToUploadUri;
    private int SELECT_FILE = 1;
    private int CAMERA_PHOTO = 2;
    private String pathOfImage;
    private String frontImage, backImage;
    private TextView frontTV, backTV;
    private Map<String, String> uploadDocumentMap;
    private LoadingDialog loadingDialog;
    private UserModel session = UserModel.getInstance();


    public VehicleInsuranceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_vehicle_insurance, container, false);
        mobileTIEditText = view.findViewById(R.id.mobile_number_TIE);
        emailTIEditText = view.findViewById(R.id.email_TIE);
        submitButton = view.findViewById(R.id.submit_BT);
        frontImageView = view.findViewById(R.id.front_IV);
        backImageView = view.findViewById(R.id.back_IV);
        frontTV = view.findViewById(R.id.front_TV);
        backTV = view.findViewById(R.id.back_TV);

        loadingDialog = new LoadingDialog(getActivity());

        frontImageView.setOnClickListener(this);
        backImageView.setOnClickListener(this);
        submitButton.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.submit_BT:

                if (!TextUtils.isEmpty(mobileTIEditText.getText()) && !TextUtils.isEmpty(emailTIEditText.getText())) {

                    if (isFrontImage && isBackImage) {

                        onUploadImage();


                    } else {
                        Toast.makeText(getActivity(), "Please Upload Image of Insurance Paper", Toast.LENGTH_SHORT).show();
                    }


                } else {
                    Toast.makeText(getActivity(), "Field Can't be Empty", Toast.LENGTH_SHORT).show();
                }

                break;

            case R.id.front_IV:

                isFrontImage = true;
                isBackImage = false;

                selectImage(frontImageView);

                break;

            case R.id.back_IV:

                isFrontImage = false;
                isBackImage = true;

                selectImage(backImageView);

                break;
        }


    }

    private void onUploadImage() {

        loadingDialog.show();

        File frontImageFile = new File(frontImage);
        File backImageFile = new File(backImage);

        uploadDocumentMap = new HashMap<>();
        uploadDocumentMap.put("sessionId", session.getUserSessionId());
        uploadDocumentMap.put("mobile",mobileTIEditText.getText().toString() );
        uploadDocumentMap.put("email", emailTIEditText.getText().toString());


        MultipartInsuranceRequest multipartInsuranceRequest = new MultipartInsuranceRequest(ApiUrl.URL_UPLOAD_IMAGE,
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loadingDialog.dismiss();
                        CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
                    }
                }, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                JSONObject jsonObject = new JSONObject();
                try {
                    if (jsonObject.getString("code").equals("S00")){
                        loadingDialog.dismiss();

                        List<UserModel> currentUserList = Select.from(UserModel.class).list();
                        UserModel currentUser = currentUserList.get(0);
                        currentUser.setHaskycRequest(true);
                        session.setHaskycRequest(true);
                        currentUser.save();

                        try {
                            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                        } catch (Exception e) {
                            loadingDialog.dismiss();
                        }
                        //For Response Message
                        successDialogs(jsonObject.getString("message"));


                    }else if (jsonObject.getString("code").equals("F03")){
                        loadingDialog.dismiss();
                        showInvalidSessionDialog();

                    }else {
                        loadingDialog.dismiss();
                    }
                } catch (JSONException e) {
                    loadingDialog.dismiss();
                    CustomToast.showMessage(getActivity(), e.toString());

                    e.printStackTrace();
                }


            }
        }, frontImageFile, uploadDocumentMap, "image", backImageFile, "image2");
        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        multipartInsuranceRequest.setRetryPolicy(policy);
        PayQwikApplication.getInstance().addToRequestQueue(multipartInsuranceRequest);

    }


    public void successDialogs(String Messages) {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(Messages));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                sendRefresh();
                startActivity(new Intent(getActivity(), MainActivity.class));
                getActivity().finish();


            }
        });

        builder.show();
    }

    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }


    private void sendRefresh() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "1");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    private void selectImage(ImageView imageView) {

        CharSequence[] items = {"Take Photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle("Upload Insurance Paper");
        alertDialog.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                boolean result = Utility.checkPermission(getActivity());

                if (items[which].equals("Take Photo")) {

                    if (result) {
                        captureCameraImage();
                    }


                } else if (items[which].equals("Choose from Gallery")) {
                    if (result) {

                        Intent intent = new Intent(Intent.ACTION_PICK);
                        intent.setType("image/*");
                        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);

                    }

                } else if (items[which].equals("Cancel")) {

                    dialog.dismiss();
                    if (imageView.getId() == (R.id.front_IV)) {
                        isFrontImage = false;
                    } else if (imageView.getId() == (R.id.back_IV)) {
                        isBackImage = false;
                    }

                }


            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE) {
                onSelectFromGalleryResult(data);
            } else if (requestCode == CAMERA_PHOTO) {
                if (imageToUploadUri != null) {
                    Bitmap reducedSizeBitmap = getBitmap(imageToUploadUri.getPath());
                    pathOfImage = null;
                    pathOfImage = ((imageToUploadUri.getPath()));

                    if (reducedSizeBitmap != null) {
                        if (isFrontImage) {
                            frontTV.setVisibility(View.GONE);
                            frontImage = imageToUploadUri.getPath();
                            frontImageView.setImageBitmap(reducedSizeBitmap);
                            isFrontImage = false;
                            isBackImage = true;

                        } else if (isBackImage) {
                            backTV.setVisibility(View.GONE);
                            backImage = imageToUploadUri.getPath();
                            backImageView.setImageBitmap(reducedSizeBitmap);
                            isFrontImage = true;
                            isBackImage = false;

                        }
                    } else {
                        Toast.makeText(getActivity(), "Error while capturing Image", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "Error while capturing Image", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    private void captureCameraImage() {

        Intent chooseIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (isFrontImage) {
            File file = new File(Environment.getExternalStorageDirectory(), "POST_IMAGE.jpg");
            chooseIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
            imageToUploadUri = Uri.fromFile(file);
        } else if (isBackImage) {
            File file = new File(Environment.getExternalStorageDirectory(), "POST_IMAGE1.jpg");
            chooseIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
            imageToUploadUri = Uri.fromFile(file);
        }
        if (Build.VERSION.SDK_INT >= 24) {
            try {
                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
                startActivityForResult(chooseIntent, CAMERA_PHOTO);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            startActivityForResult(chooseIntent, CAMERA_PHOTO);
        }
        frontTV.setVisibility(View.GONE);
    }

    private Bitmap getBitmap(String path) {

        Uri uri = Uri.fromFile(new File(path));
        InputStream in = null;
        try {
            final int IMAGE_MAX_SIZE = 1200000; // 1.2MP
            in = getActivity().getContentResolver().openInputStream(uri);

            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(in, null, o);
            in.close();


            int scale = 1;
            while ((o.outWidth * o.outHeight) * (1 / Math.pow(scale, 2)) >
                    IMAGE_MAX_SIZE) {
                scale++;
            }
            Log.d("", "scale = " + scale + ", orig-width: " + o.outWidth + ", orig-height: " + o.outHeight);

            Bitmap b = null;
            in = getActivity().getContentResolver().openInputStream(uri);
            if (scale > 1) {
                scale--;
                // scale to max possible inSampleSize that still yields an image
                // larger than target
                o = new BitmapFactory.Options();
                o.inSampleSize = scale;
                b = BitmapFactory.decodeStream(in, null, o);

                // resize to desired dimensions
                int height = b.getHeight();
                int width = b.getWidth();
                Log.d("", "1th scale operation dimenions - width: " + width + ", height: " + height);

                double y = Math.sqrt(IMAGE_MAX_SIZE
                        / (((double) width) / height));
                double x = (y / height) * width;

                Bitmap scaledBitmap = Bitmap.createScaledBitmap(b, (int) x,
                        (int) y, true);
                b.recycle();
                b = scaledBitmap;

                System.gc();
            } else {
                b = BitmapFactory.decodeStream(in);
            }
            in.close();

            Log.d("", "bitmap size - width: " + b.getWidth() + ", height: " +
                    b.getHeight());
            return b;
        } catch (IOException e) {
            Log.e("", e.getMessage(), e);
            return null;
        }
    }

    private void onSelectFromGalleryResult(Intent data) {
        if (data != null) {
            Uri selectedImageUri = data.getData();
            pathOfImage = getPath(getActivity(), selectedImageUri);

            if (isFrontImage) {
                frontImageView.setImageBitmap(getCompressedBitmap(getPath(getActivity(), selectedImageUri)));
                frontImage = pathOfImage;
                frontTV.setVisibility(View.GONE);

            } else if (isBackImage) {
                backImageView.setImageBitmap(getCompressedBitmap(getPath(getActivity(), selectedImageUri)));
                backImage = pathOfImage;
                backTV.setVisibility(View.GONE);

            }

        }

    }

    public Bitmap getCompressedBitmap(String imagePath) {
        float maxHeight = 1920.0f;
        float maxWidth = 1080.0f;
        Bitmap scaledBitmap = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(imagePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;
        float imgRatio = (float) actualWidth / (float) actualHeight;
        float maxRatio = maxWidth / maxHeight;

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
        options.inJustDecodeBounds = false;
        options.inDither = false;
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
            bmp = BitmapFactory.decodeFile(imagePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

        ExifInterface exif = null;
        try {
            exif = new ExifInterface(imagePath);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
            } else if (orientation == 3) {
                matrix.postRotate(180);
            } else if (orientation == 8) {
                matrix.postRotate(270);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 85, out);

        byte[] byteArray = out.toByteArray();

        Bitmap updatedBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);

        return updatedBitmap;
    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }


    public static String getPath(Context context, Uri uri) {
        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {

            String docId = DocumentsContract.getDocumentId(uri);

            if (isExternalStorageDocument(uri)) {

                String[] split = docId.split(":");
                String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

            } else if (isDownloadsDocument(uri)) {
                Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(docId));
                return getDataColumn(context, contentUri, null, null);


            } else if (isMediaDocument(uri)) {

                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        } else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }


    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;

    }
}
