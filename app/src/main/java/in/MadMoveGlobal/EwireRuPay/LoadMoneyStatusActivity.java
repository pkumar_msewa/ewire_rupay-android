package in.MadMoveGlobal.EwireRuPay;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.MadMoveGlobal.custom.CustomAlertDialog;
import in.MadMoveGlobal.custom.CustomBlockDialog;
import in.MadMoveGlobal.custom.CustomSuccessDialog;
import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.metadata.AppMetadata;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.util.EmailCouponsUtil;
import in.MadMoveGlobal.util.GenerateAPIHeader;
import in.MadMoveGlobal.util.NetworkErrorHandler;
import in.MadMoveGlobal.EwireRuPay.activity.LoginRegActivity;
import in.MadMoveGlobal.EwireRuPay.activity.MainActivity;


public class LoadMoneyStatusActivity extends AppCompatActivity {

    public static final String EXTRA = "EXTRA";
    private String type, URL, REQUESTOBJECT, amountToLoad;
    private LoadingDialog loadDlg;
    private String globalURL;
    private String globalObjectString = "";
    private JSONObject jsonObj;
    private UserModel session = UserModel.getInstance();
    private String tag_json_obj = "json_topup_pay";
    private String serviceProviderName = "";
    private String toMobileNumber = "";
    private String amount = "";
    private TextView MyBallance;
    private ImageButton ivLogo;
    private String tokenKeyNikki, secretKeyNikki;
    private JSONObject jsonRequest, extraJson;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_status);
        loadDlg = new LoadingDialog(LoadMoneyStatusActivity.this);

        Button btnTransactionOK = (Button) findViewById(R.id.btnTransactionOK);

        String status = getIntent().getStringExtra(LoadMoneyStatusActivity.EXTRA);
//        CustomToast.showMessage(LoadMoneyStatusActivity.this, status);
        if (status.equalsIgnoreCase("Success")) {

            Thread thread = new Thread() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    runOnUiThread(new Runnable() {
                                      @Override
                                      public void run() {
                                          if (getIntent().getStringExtra("TYPE").equals("LoadMoney")) {
                                              showSuccessDialog();
                                          } else {
                                              amountToLoad = getIntent().getStringExtra("splitAmount");
                                              type = getIntent().getStringExtra("TYPE");
                                              globalURL = getIntent().getStringExtra("URL");
                                              try {
                                                  jsonObj = new JSONObject(getIntent().getStringExtra("REQUESTOBJECT"));
                                              } catch (JSONException e) {
                                                  e.printStackTrace();
                                              }
                                              promoteRecharge();
                                          }
                                      }

                                  }
                    );
                }

            };
            thread.start();
        } else {
            CustomAlertDialog customAlertDialog = new CustomAlertDialog(LoadMoneyStatusActivity.this, R.string.dialog_title, status);
            customAlertDialog.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    sendRefresh();
                    Intent intent = new Intent(LoadMoneyStatusActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            });
            customAlertDialog.show();
        }

//        CustomAlertDialog customAlertDialog = new CustomAlertDialog(LoadMoneyStatusActivity.this, R.string.dialog_title, status);
//        customAlertDialog.setPositiveButton("ok", new DialogInterface.OnClickListener() {
//            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//                dialogInterface.dismiss();
//				sendRefresh();
//                Intent intent = new Intent(LoadMoneyStatusActivity.this, MainActivity.class);
//                startActivity(intent);
//                finish();
//            }
//        });
//        customAlertDialog.show();
    }

    private void sendRefresh() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "1");
        finishAffinity();
        startActivity(new Intent(LoadMoneyStatusActivity.this, MainActivity.class));
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    public void promoteRecharge() {
        Log.i("PROMOTEREC", "promote recharge is called");

        jsonRequest = new JSONObject();
        try {
            if (getIntent().getStringExtra("TYPE").equals("TOPUP")) {
                jsonRequest.put("topupType", jsonObj.getString("topupType"));
                jsonRequest.put("serviceProvider", jsonObj.getString("serviceProvider"));
                jsonRequest.put("mobileNo", jsonObj.getString("mobileNo"));
                jsonRequest.put("amount", jsonObj.getString("amount"));
                if (!jsonObj.getString("topupType").equals("Postpaid")) {
                    jsonRequest.put("area", jsonObj.getString("area"));
                }
                if (!jsonObj.getString("topupType").equals("Prepaid")) {
                    jsonRequest.put("area", jsonObj.getString("area"));
                }
                if (!jsonObj.getString("topupType").equals("DataCard")) {
                    jsonRequest.put("area", jsonObj.getString("area"));
                }
                jsonRequest.put("sessionId", jsonObj.getString("sessionId"));
            } else if (getIntent().getStringExtra("TYPE").equals("BILLPAY")) {
                jsonRequest.put("serviceProvider", jsonObj.getString("serviceProvider"));
                jsonRequest.put("amount", jsonObj.getString("amount"));
                jsonRequest.put("sessionId", jsonObj.getString("sessionId"));
                if (getIntent().getStringExtra("SUBTYPE").equals("DTH")) {
                    jsonRequest.put("dthNo", jsonObj.getString("dthNo"));
                } else if (getIntent().getStringExtra("SUBTYPE").equals("ELECTRIC")) {
                    jsonRequest.put("accountNumber", jsonObj.getString("accountNumber"));
                    jsonRequest.put("cycleNumber", jsonObj.getString("cycleNumber"));
                    jsonRequest.put("cityName", jsonObj.getString("cityName"));
                } else if (getIntent().getStringExtra("SUBTYPE").equals("ELECTRIC")) {
                    jsonRequest.put("accountNumber", jsonObj.getString("accountNumber"));
                } else if (getIntent().getStringExtra("SUBTYPE").equals("INSURANCE")) {
                    jsonRequest.put("policyNumber", jsonObj.getString("policyNumber"));
                    jsonRequest.put("policyDate", jsonObj.getString("policyDate"));
                } else if (getIntent().getStringExtra("SUBTYPE").equals("LANDLINE")) {
                    jsonRequest.put("stdCode", jsonObj.getString("stdCode"));
                    jsonRequest.put("accountNumber", jsonObj.getString("accountNumber"));
                    jsonRequest.put("landlineNumber", jsonObj.getString("landlineNumber"));
                }

            } else if (getIntent().getStringExtra("TYPE").equals("GIFTCARD")) {
                jsonRequest = jsonObj;
            } else if (getIntent().getStringExtra("TYPE").equals("BUS")) {
                jsonRequest = jsonObj;
            } else if (getIntent().getStringExtra("TYPE").equals("LoadMoney")) {

            }

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("globalURL", globalURL);
            Log.i("JsonRequestRecharge", jsonRequest.toString());
            loadDlg.show();
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, globalURL, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.i("JsonResponseRecharge", response.toString());
                        String code = response.getString("code");
                        loadDlg.dismiss();
                        if (code != null && code.equals("S00")) {

                            try {
                                if (getIntent().getStringExtra("TYPE").equals("QWICKPAY")) {
                                    loadDlg.dismiss();
                                    showSuccessDialog();
                                } else if (getIntent().getStringExtra("TYPE").equalsIgnoreCase("BUS")) {
                                    JSONObject details = response.getJSONObject("details");
                                    String transactionid = details.getString("transactionid");
                                    String vpQTxnId = details.getString("vpQTxnId");
                                    String mpQTxnId = details.getString("mpQTxnId");
                                    loadDlg.dismiss();
                                    showSuccessDialogBus(transactionid, vpQTxnId, mpQTxnId);
                                } else {
                                    String jsonString = response.getString("message");
//                                    JSONObject jsonObject = new JSONObject(jsonString);

//                                    sendRefresh();
                                    try {
                                        EmailCouponsUtil.emailForCoupons(tag_json_obj);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    loadDlg.dismiss();

                                    showSuccessDialog();
                                }
                            } catch (NullPointerException e) {

                                e.printStackTrace();
                            }
                        } else if (code != null && code.equals("F03")) {
                            loadDlg.dismiss();
                            showInvalidSessionDialog();
                        } else if (code != null && code.equals(ApiUrl.C_SESSION_BLOCKED)) {
                            loadDlg.dismiss();
                            showBlockDialog();
                        } else {
                            loadDlg.dismiss();
                            if (response.has("message") && response.getString("message") != null) {
                                String message = response.getString("message");
                                CustomAlertDialog customAlertDialog = new CustomAlertDialog(LoadMoneyStatusActivity.this, R.string.dialog_title, message);
                                customAlertDialog.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        sendRefresh();
                                        Intent intent = new Intent(LoadMoneyStatusActivity.this, MainActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                });
                                customAlertDialog.show();
                            } else {
                                CustomAlertDialog customAlertDialog = new CustomAlertDialog(LoadMoneyStatusActivity.this, R.string.dialog_title, "Oops!!! something is wrong. Please try again");
                                customAlertDialog.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        sendRefresh();
                                        Intent intent = new Intent(LoadMoneyStatusActivity.this, MainActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                });
                                customAlertDialog.show();
                            }
                        }

                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        e.printStackTrace();
                        CustomAlertDialog customAlertDialog = new CustomAlertDialog(LoadMoneyStatusActivity.this, R.string.dialog_title, "Oops!!! something is wrong. Please try again");
                        customAlertDialog.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                sendRefresh();
                                Intent intent = new Intent(LoadMoneyStatusActivity.this, MainActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        });
                        customAlertDialog.show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    CustomToast.showMessage(LoadMoneyStatusActivity.this, NetworkErrorHandler.getMessage(error, LoadMoneyStatusActivity.this));
                    error.printStackTrace();
                    CustomAlertDialog customAlertDialog = new CustomAlertDialog(LoadMoneyStatusActivity.this, R.string.dialog_title, "Oops!!! something is wrong. Please try again");
                    customAlertDialog.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            sendRefresh();
                            Intent intent = new Intent(LoadMoneyStatusActivity.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    });
                    customAlertDialog.show();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", GenerateAPIHeader.getHeader(ApiUrl.H_PREPAID + session.getUserSessionId()));
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }

    }

    public void showInvalidSessionDialog() {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(AppMetadata.getInvalidSession(), Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(AppMetadata.getInvalidSession());
        }
        CustomAlertDialog builder = new CustomAlertDialog(LoadMoneyStatusActivity.this, R.string.dialog_title2, result);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }


    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        finishAffinity();
        startActivity(new Intent(LoadMoneyStatusActivity.this, LoginRegActivity.class));
        LocalBroadcastManager.getInstance(LoadMoneyStatusActivity.this).sendBroadcast(intent);
    }

    public void showSuccessDialog() {
        Spanned result;
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
//            result = Html.fromHtml(getSuccessMessage(), Html.FROM_HTML_MODE_LEGACY);
//        } else {
//            result = Html.fromHtml(getSuccessMessage());
//        }
        CustomSuccessDialog builder = null;
        try {
            if (getIntent().getStringExtra("TYPE").equals("TOPUP")) {
                builder = new CustomSuccessDialog(LoadMoneyStatusActivity.this, "Recharge Successful", "");
            } else if(getIntent().getStringExtra("TYPE").equals("LoadMoney")) {
                builder = new CustomSuccessDialog(LoadMoneyStatusActivity.this, "Load Money Successful", "");
            }else {
                builder = new CustomSuccessDialog(LoadMoneyStatusActivity.this, "Payment Successful", "");
            }
            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                    sendRefresh();
                    finish();
                }
            });
            builder.show();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }


    }

//    public String getSuccessMessage() {
//        String source = "";
//        if (getIntent().getStringExtra("TYPE").equals("TOPUP")) {
//            source = "<b><font color=#000000> Service Provider: </font></b>" + "<font color=#000000>" + serviceProviderName + "</font><br>" +
//                    "<b><font color=#000000> Mobile No: </font></b>" + "<font>" + toMobileNumber + "</font><br>" +
//                    "<b><font color=#000000> Amount: </font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + amount + "</font><br><br>";
//        } else if (getIntent().getStringExtra("TYPE").equals("BILLPAY")) {
//            try {
//                if (getIntent().getStringExtra("SUBTYPE").equals("DTH")) {
//                    source =
//                            "<b><font color=#000000> Service Provider: </font></b>" + "<font color=#000000>" + jsonObj.getString("serviceProvider") + "</font><br>" +
//                                    "<b><font color=#000000> DTH No: </font></b>" + "<font>" + jsonObj.getString("dthNo") + "</font><br>" +
//                                    "<b><font color=#000000> Amount: </font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + jsonObj.getString("amount") + "</font><br><br>";
//                } else if (getIntent().getStringExtra("SUBTYPE").equals("ELECTRIC")) {
//                    String cycleNo = jsonObj.getString("cycleNumber");
//                    if (cycleNo != null && !cycleNo.isEmpty()) {
//                        source = "<b><font color=#000000> Service Provider: </font></b>" + "<font color=#000000>" + jsonObj.getString("serviceProvider") + "</font><br>" +
//                                "<b><font color=#000000> Account No: </font></b>" + "<font>" + jsonObj.getString("accountNumber") + "</font><br>" +
//                                "<b><font color=#000000> Cycle No: </font></b>" + "<font>" + cycleNo + "</font><br>" +
//                                "<b><font color=#000000> Amount: </font></b>" + "<font>" + jsonObj.getString("amount") + "</font><br>";
//                    } else {
//                        source = "<b><font color=#000000> Service Provider: </font></b>" + "<font color=#000000>" + jsonObj.getString("serviceProvider") + "</font><br>" +
//                                "<b><font color=#000000> Account No: </font></b>" + "<font>" + jsonObj.getString("accountNumber") + "</font><br>" +
//                                "<b><font color=#000000> Amount: </font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + jsonObj.getString("amount") + "</font><br>";
//                    }
//                } else if (getIntent().getStringExtra("SUBTYPE").equals("GAS")) {
//                    String payAmount = getIntent().getStringExtra("payAmount");
//                    String additionalCharges = getIntent().getStringExtra("AddCharge");
//                    source = "<b><font color=#000000> Service Provider: </font></b>" + "<font color=#000000>" + jsonObj.getString("serviceProvider") + "</font><br>" +
//                            "<b><font color=#000000> Account No: </font></b>" + "<font>" + jsonObj.getString("accountNumber") + "</font><br>" +
//                            "<b><font color=#000000> Amount: </font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + jsonObj.getString("amount") + "</font><br>" +
//                            "<b><font color=#000000> Service Charge: </font></b>" + "<font>" + additionalCharges + "</font><br>" +
//                            "<b><font color=#000000> Total Charge: </font></b>" + "<font>" + payAmount + "</font><br><br>" +
//                            "<b><font color=#ff0000> Are you sure you want to proceed?</font></b><br>";
//                } else if (getIntent().getStringExtra("SUBTYPE").equals("INSURANCE")) {
//                    source = "<b><font color=#000000> Service Provider: </font></b>" + "<font color=#000000>" + jsonObj.getString("serviceProvider") + "</font><br>" +
//                            "<b><font color=#000000> Policy No: </font></b>" + "<font>" + jsonObj.getString("policyNumber") + "</font><br>" +
//                            "<b><font color=#000000> Date: </font></b>" + "<font>" + jsonObj.getString("policyDate") + "</font><br>" +
//                            "<b><font color=#000000> Amount: </font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + jsonObj.getString("amount") + "</font><br><br>";
//                } else if (getIntent().getStringExtra("SUBTYPE").equals("LANDLINE")) {
//                    String acNo = jsonObj.getString("accountNumber");
//                    if (acNo != null && !acNo.isEmpty()) {
//                        source = "<b><font color=#000000> Service Provider: </font></b>" + "<font color=#000000>" + jsonObj.getString("serviceProvider") + "</font><br>" +
//                                "<b><font color=#000000> STD Code: </font></b>" + "<font>" + jsonObj.getString("stdCode") + "</font><br>" +
//                                "<b><font color=#000000> Phone No: </font></b>" + "<font>" + jsonObj.getString("landlineNumber") + "</font><br>" +
//                                "<b><font color=#000000> Amount: </font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + jsonObj.getString("amount") + "</font><br>" +
//                                "<b><font color=#000000> Ac No: </font></b>" + "<font>" + jsonObj.getString("accountNumber") + "</font><br><br>";
//                    } else {
//                        source = "<b><font color=#000000> Service Provider: </font></b>" + "<font color=#000000>" + jsonObj.getString("serviceProvider") + "</font><br>" +
//                                "<b><font color=#000000> STD Code: </font></b>" + "<font>" + jsonObj.getString("stdCode") + "</font><br>" +
//                                "<b><font color=#000000> Phone No: </font></b>" + "<font>" + jsonObj.getString("landlineNumber") + "</font><br>" +
//                                "<b><font color=#000000> Amount: </font></b>" + "<font>" + getResources().getString(R.string.rupease) + " " + jsonObj.getString("amount") + "</font><br>";
//                    }
//                }
//
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        } else if (getIntent().getStringExtra("TYPE").equals("QWICKPAY")) {
//            source = "<b><font color=#000000> Service Provider: </font></b>" + "<font color=#000000>Payment to merchant successful </font><br>";
//        }
//
//        return source;
////    }


    public void showBlockDialog() {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(AppMetadata.getBlockSession(), Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(AppMetadata.getBlockSession());
        }
        CustomBlockDialog builder = new CustomBlockDialog(LoadMoneyStatusActivity.this, "Please contact customer care.", result);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
                dialog.dismiss();
                finish();
            }
        });
        builder.show();
    }

    private void promoteBooking(String trxId, String pqTrxId, String mdexTxnId) {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("transactionId", trxId);
            jsonRequest.put("vpqTxnId", pqTrxId);
            jsonRequest.put("mdexTxnId", mdexTxnId);

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("BOOKINGREQ", jsonRequest.toString());
            Log.i("BOOKINGURL", ApiUrl.URL_BUS_SEAT_BOOKING);
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_BUS_SEAT_BOOKING, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.i("BOOKINGRES", response.toString());
                        if (response.has("code")) {
                            String code = response.getString("code");
                            String msg = response.getString("message");

                            if (code != null && code.equals("S00")) {
                                loadDlg.dismiss();
                                showSuccessDialog();
                            } else if (code != null && code.equals("F03")) {
                                loadDlg.dismiss();
                                showInvalidSessionDialog();
                            } else {
                                loadDlg.dismiss();
                                if (response.has("message") && response.getString("message") != null) {
                                    String message = response.getString("message");
                                    CustomToast.showMessage(getApplicationContext(), message);
                                } else {
                                    CustomToast.showMessage(getApplicationContext(), "Error message is null");
                                }

                            }
                        }
                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception2));
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    CustomToast.showMessage(getApplicationContext(), NetworkErrorHandler.getMessage(error, getApplicationContext()));
                    error.printStackTrace();
                }
            }) {
            };

            int socketTimeout = 600000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    public void showSuccessDialogBus(final String trnxID, final String PQTrnxID, final String mpQTxnId) {
//        Spanned result;
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
//            result = Html.fromHtml(getSuccessMessage(), Html.FROM_HTML_MODE_LEGACY);
//        } else {
//            result = Html.fromHtml(getSuccessMessage());
//        }
//        CustomSuccessDialogBus builder = new CustomSuccessDialogBus(BusBookActivity.this, "Please, Confirm Payment", result);
//        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
        promoteBooking(trnxID, PQTrnxID, mpQTxnId);
//                dialog.dismiss();
//            }
//        });
//        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                cancelTranx(PQTrnxID);
//            }
//        });
//        builder.show();
    }
}