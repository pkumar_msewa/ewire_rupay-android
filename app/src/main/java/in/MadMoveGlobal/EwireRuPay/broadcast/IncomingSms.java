package in.MadMoveGlobal.EwireRuPay.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

/**
 * Created by Ksf on 3/13/2016.
 */
public class IncomingSms extends BroadcastReceiver {

    public void onReceive(Context context, Intent intent) {
        final Bundle bundle = intent.getExtras();
        try {

            if (bundle != null) {
                final Object[] pdusObj = (Object[]) bundle.get("pdus");

                for (int i = 0; i < pdusObj.length; i++) {
                    SmsMessage currentMessage;

                    if (Build.VERSION.SDK_INT >= 23) {
                        String format = bundle.getString("format");
                        currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i], format);
                    } else {
                        Log.i("Not Lolipop", "Not Lolipop");
                        currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    }

                    String phoneNumber = currentMessage.getDisplayOriginatingAddress();
                    String senderNum = phoneNumber;
                    String message = currentMessage.getDisplayMessageBody();

                    String[] messageOTP = message.split(" ");


                    if (senderNum.contains("InfoSMS")) {
                        String otp = String.valueOf(message.replaceAll("[^0-9]", ""));
                        Intent smsIntent = new Intent("broadCastName");
                        smsIntent.putExtra("message", otp);
                        context.sendBroadcast(smsIntent);

                    }else if (senderNum.contains("51462")) {
                        String otp = String.valueOf(message.replaceAll("[^0-9]", ""));
                        Intent smsIntent = new Intent("broadCastName");
                        smsIntent.putExtra("message", otp);
                        context.sendBroadcast(smsIntent);

                    }else if (senderNum.contains("CASHER")) {
                        String otp = String.valueOf(message.replaceAll("[^0-9]", ""));
                        Intent smsIntent = new Intent("broadCastName");
                        smsIntent.putExtra("message", otp);
                        context.sendBroadcast(smsIntent);

                    }

                }
            }

        } catch (Exception e) {
            Log.e("SmsReceiver", "Exception smsReceiver" + e);

        }
    }


    private boolean isNumeric(String str) {
        return android.text.TextUtils.isDigitsOnly(str);  //match a number with optional '-' and decimal.
    }
}
