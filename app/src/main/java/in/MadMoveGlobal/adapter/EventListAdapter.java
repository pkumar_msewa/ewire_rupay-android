package in.MadMoveGlobal.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;

import java.util.List;

import in.MadMoveGlobal.model.EventsModel;
import in.MadMoveGlobal.EwireRuPay.R;

/**
 * Created by Dushant on 07/27/2017.
 */
public class EventListAdapter extends BaseAdapter {

    private Context context;
    private List<EventsModel> eventsArray;
    private ViewHolder viewHolder;

    public EventListAdapter(Context context, List<EventsModel> eventsArray) {
        this.context = context;
        this.eventsArray = eventsArray;
    }

    @Override
    public int getCount() {
        return eventsArray.size();
    }

    @Override
    public Object getItem(int index) {
        return eventsArray.get(index);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final int currentPosition = position;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.adapter_my_coupons, null);
            viewHolder = new ViewHolder();
            viewHolder.tvMyCouponsVendorAddress = (TextView) convertView.findViewById(R.id.tvMyCouponsVendorAddress);
            viewHolder.tvMyCouponsVendorName = (TextView) convertView.findViewById(R.id.tvMyCouponsVendorName);
            viewHolder.tvMyCouponsTitle = (TextView) convertView.findViewById(R.id.tvMyCouponsTitle);
            viewHolder.ivMyCoupons = (ImageView) convertView.findViewById(R.id.ivMyCoupons);
            viewHolder.llMyCouponsMain = (CardView) convertView.findViewById(R.id.llMyCouponsMain);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.tvMyCouponsVendorAddress.setText(eventsArray.get(position).getEventCity());
        viewHolder.tvMyCouponsVendorName.setText(eventsArray.get(position).getEventVenue());
        viewHolder.tvMyCouponsTitle.setText(eventsArray.get(position).getEventTitle());

        if (!eventsArray.get(position).getBannerImage().isEmpty()) {
            AQuery aq = new AQuery(context);
            aq.id(viewHolder.ivMyCoupons).background(R.drawable.loading_image).image(eventsArray.get(position).getBannerImage(), true, true);
            viewHolder.ivMyCoupons.setVisibility(View.VISIBLE);
        } else {
            viewHolder.ivMyCoupons.setVisibility(View.GONE);
        }
        return convertView;
    }


    static class ViewHolder {
        TextView tvMyCouponsVendorAddress;
        TextView tvMyCouponsVendorName;
        TextView tvMyCouponsTitle;
        ImageView ivMyCoupons;
        CardView llMyCouponsMain;

    }

}
