package in.MadMoveGlobal.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;

import in.MadMoveGlobal.model.BusBookedTicketModel;
import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.EwireRuPay.activity.flightinneracitivty.FlightTickDetailActivity;


/**
 * Created by Dushant on 07/27/2017.
 */
public class FlightBookListAdapter extends RecyclerView.Adapter<FlightBookListAdapter.RecyclerViewHolders> {

    private Context context;
    private ArrayList<BusBookedTicketModel> busArray;

    private long destinationCode, sourceCode;
    private String dateOfDep;
    Date arrTime, depTime = null;
    int sys;

    public FlightBookListAdapter(Context context, ArrayList<BusBookedTicketModel> busArray, int sys) {
        this.context = context;
        this.busArray = busArray;
        this.sys = sys;

    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_book_bus_list, null);
        return new RecyclerViewHolders(layoutView);
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolders viewHolder, final int position) {

        viewHolder.tvBusListName.setText(busArray.get(position).getBusOperator());
        viewHolder.tvArrDept.setText(busArray.get(position).getArrTime() + " - " + busArray.get(position).getDeptTime());
        viewHolder.tvTicketPnrNo.setText(busArray.get(position).getTicketPnr() + "");
        viewHolder.tvFromTo.setText(busArray.get(position).getSource() + " - " + busArray.get(position).getDestination());
        viewHolder.tvDate.setText(busArray.get(position).getJourneyDate());
        viewHolder.tvUserMobile.setText("Mob: " + busArray.get(position).getUserMobile() + "");
        viewHolder.tvBusFare.setText(context.getResources().getString(R.string.rupease) + " " + busArray.get(position).getTotalFare());
        viewHolder.tvBoardingAddress.setText(busArray.get(position).getBoardingAddress());


        viewHolder.llBusListMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent busDetailIntent = new Intent(context, FlightTickDetailActivity.class);
                BusBookedTicketModel busBookedTicketModel = busArray.get(position);
                busDetailIntent.putExtra("TicketValues", busBookedTicketModel);
                busDetailIntent.putExtra("Ticket", busBookedTicketModel);
                busDetailIntent.putExtra("citylist", sys);
                context.startActivity(busDetailIntent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return this.busArray.size();
    }

    public class RecyclerViewHolders extends RecyclerView.ViewHolder {
        TextView tvBusListName;
        TextView tvBusFare;
        TextView tvArrDept, tvJourneyDuration, tvDate, tvTicketPnrNo, tvFromTo, tvUserMobile, tvBoardingAddress;
        LinearLayout llBusListMain;


        public RecyclerViewHolders(View convertView) {
            super(convertView);
            tvBusListName = (TextView) convertView.findViewById(R.id.tvBusListName);
            tvArrDept = (TextView) convertView.findViewById(R.id.tvArrDept);
            tvJourneyDuration = (TextView) convertView.findViewById(R.id.tvJourneyDuration);
            tvDate = (TextView) convertView.findViewById(R.id.tvDate);
            tvBusFare = (TextView) convertView.findViewById(R.id.tvBusFare);
            tvTicketPnrNo = (TextView) convertView.findViewById(R.id.tvTicketPnrNo);
            tvFromTo = (TextView) convertView.findViewById(R.id.tvFromTo);
            tvUserMobile = (TextView) convertView.findViewById(R.id.tvUserMobile);
            tvBoardingAddress = (TextView) convertView.findViewById(R.id.tvBoardingAddress);
            llBusListMain = (LinearLayout) convertView.findViewById(R.id.llBusListMain);
        }


    }

}
