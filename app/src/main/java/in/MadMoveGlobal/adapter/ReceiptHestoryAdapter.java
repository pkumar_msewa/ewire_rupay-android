package in.MadMoveGlobal.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import in.MadMoveGlobal.model.StatementModel;
import in.MadMoveGlobal.EwireRuPay.R;

/**
 * Created by Dushant on 07/27/2017.
 */
public class ReceiptHestoryAdapter extends BaseAdapter {

    private ViewHolder viewHolder;
    private Context context;
    private ArrayList<StatementModel> statementList;

    public ReceiptHestoryAdapter(Context context, ArrayList<StatementModel> statementList) {
        this.context = context;
        this.statementList = statementList;
    }

    @Override
    public int getCount() {
        return statementList.size();
    }

    @Override
    public Object getItem(int position) {
        return statementList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parentView) {
        final int currentPosition = position;
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.adapter_receipt_history, null);
            viewHolder = new ViewHolder();
            viewHolder.tvReceiptDate = (TextView) convertView.findViewById(R.id.tvReceiptDate);
            viewHolder.tvReceiptCurrentBalance = (TextView) convertView.findViewById(R.id.tvReceiptCurrentBalance);
            viewHolder.tvReceiptAmount = (TextView) convertView.findViewById(R.id.tvReceiptAmount);
            viewHolder.tvReceiptDetails = (TextView) convertView.findViewById(R.id.tvReceiptDetails);
            viewHolder.tvReceiptStatus = (TextView) convertView.findViewById(R.id.tvReceiptStatus);
            viewHolder.tvReceiptTnxId = (TextView) convertView.findViewById(R.id.tvReceiptTnxId);

            viewHolder.tvReceiptAuthRef = (TextView) convertView.findViewById(R.id.tvReceiptAuthRef);
            viewHolder.tvReceiptReteriveRef = (TextView) convertView.findViewById(R.id.tvReceiptReteriveRef);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.tvReceiptDetails.setText(statementList.get(position).getRefNo());
//        viewHolder.tvReceiptTnxId.setText("Ref no. "+statementList.get(position).getRefNo());
//        viewHolder.tvReceiptCurrentBalance.setText("Wallet Balance: "+context.getResources().getString(R.string.rupease)+" "+statementList.get(position).getCurrentBalance());

        //MVISA

        String[] dateString = statementList.get(position).getDate().split("T");

        viewHolder.tvReceiptDate.setText(dateString[0]);

        viewHolder.tvReceiptStatus.setText(statementList.get(position).getStatus());
        if (statementList.get(position).getIndicator().equals("debit")) {

            viewHolder.tvReceiptAmount.setText("-" + context.getResources().getString(R.string.rupease) + statementList.get(position).getAmount());

        }
        if (statementList.get(position).getIndicator().equals("credit")) {

            viewHolder.tvReceiptAmount.setText("+" + context.getResources().getString(R.string.rupease) + statementList.get(position).getAmount());

        }


        if (statementList.get(position).getStatus().equals("Success")) {
            viewHolder.tvReceiptStatus.setTextColor(context.getResources().getColor(R.color.success));
        } else if (statementList.get(position).getStatus().equals("Booked")) {
            viewHolder.tvReceiptStatus.setTextColor(context.getResources().getColor(R.color.success));
        } else if (statementList.get(position).getStatus().equals("Failed")) {
            viewHolder.tvReceiptStatus.setTextColor(context.getResources().getColor(R.color.faliure));
        } else if (statementList.get(position).getStatus().equals("Reversed")) {
            viewHolder.tvReceiptStatus.setTextColor(context.getResources().getColor(R.color.reverse));
        } else {
            viewHolder.tvReceiptStatus.setTextColor(context.getResources().getColor(R.color.blue));
        }
        viewHolder.tvReceiptStatus.setText(statementList.get(position).getStatus());
        if (statementList.get(position).getIndicator().equals("credit")) {
            viewHolder.tvReceiptAmount.setText(context.getResources().getString(R.string.rupease) + statementList.get(position).getAmount());
        } else {
            viewHolder.tvReceiptAmount.setText(context.getResources().getString(R.string.rupease) + statementList.get(position).getAmount());
        }


//        if(statementList.get(position).getRetrievRefNo().isEmpty()||statementList.get(position).getRetrievRefNo().equals("null")){
//            viewHolder.tvReceiptReteriveRef.setVisibility(View.GONE);
//        }
//        else{
//            viewHolder.tvReceiptReteriveRef.setVisibility(View.VISIBLE);
//        }
//        if(statementList.get(position).getAuthRefNo().isEmpty()||statementList.get(position).getAuthRefNo().equals("null")){
//            viewHolder.tvReceiptAuthRef.setVisibility(View.GONE);
//        }
//        else{
//            viewHolder.tvReceiptAuthRef.setVisibility(View.VISIBLE);
//        }
        return convertView;
    }

    static class ViewHolder {
        TextView tvReceiptDate, tvReceiptCurrentBalance, tvReceiptAmount, tvReceiptStatus, tvReceiptDetails;
        TextView tvReceiptTnxId, tvReceiptReteriveRef, tvReceiptAuthRef;

    }
}
