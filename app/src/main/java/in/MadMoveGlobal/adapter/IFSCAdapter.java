package in.MadMoveGlobal.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.model.IFSCModel;


/**
 * Created by Dushant on 07/27/2017.
 */

public class IFSCAdapter extends BaseAdapter implements Filterable {

  private Context context;
  private ArrayList<IFSCModel> ifscArray;
  private List<IFSCModel> filteredData;
  private ViewHolder viewHolder;

  public IFSCAdapter(Context context, ArrayList<IFSCModel> serviceoperaterArray) {
    this.context = context;
    this.ifscArray = serviceoperaterArray;
    this.filteredData = serviceoperaterArray;
  }

  @Override
  public int getCount() {
    if (ifscArray == null) {
      return 0;
    }
    return ifscArray.size();
  }

  @Override
  public Object getItem(int index) {
    return ifscArray.get(index);
  }

  @Override
  public long getItemId(int position) {
    return position;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    if (convertView == null) {
      convertView = LayoutInflater.from(context).inflate(R.layout.adapter_ifsc_list, parent, false);
      viewHolder = new ViewHolder();
      viewHolder.tvTitle = (TextView) convertView.findViewById(R.id.tvIFSC);



      convertView.setTag(viewHolder);
    } else {
      viewHolder = (ViewHolder) convertView.getTag();
    }

    if(!ifscArray.get(position).getName().equals("null"))

    viewHolder.tvTitle.setText(ifscArray.get(position).getName());



    Log.i("VALUES",ifscArray.get(position).getName());


    return convertView;
  }

  @Override
  public Filter getFilter() {
    return new ItemFilter();
  }


  static class ViewHolder {
    TextView tvTitle;

  }


  private class ItemFilter extends Filter {
    @Override
    protected FilterResults performFiltering(CharSequence constraint) {

      String filterString = constraint.toString().toLowerCase();

      FilterResults results = new FilterResults();

      final List<IFSCModel> list = filteredData;

      int count = list.size();
      final ArrayList<Object> nlist = new ArrayList<Object>(count);

      String filterableString, filterableStringcode, filterableStringAreo,filterimage;

      for (int i = 0; i < count; i++) {
        filterableString = list.get(i).getName();

        if (filterableString.toLowerCase().contains(filterString)) {
          nlist.add(list.get(i));
        }
      }
      results.values = nlist;
      results.count = nlist.size();
      return results;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
      ifscArray = (ArrayList<IFSCModel>) results.values;
      notifyDataSetChanged();
    }

  }
}

