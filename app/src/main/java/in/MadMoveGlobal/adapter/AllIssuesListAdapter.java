package in.MadMoveGlobal.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;

import in.MadMoveGlobal.model.ListIssueModel;
import in.MadMoveGlobal.EwireRuPay.R;


/**
 * Created by Dushant on 07/27/2017.
 */
public class AllIssuesListAdapter extends BaseAdapter {

    private ViewHolder viewHolder;
    private Context context;
    private List<ListIssueModel> statementList;

    public AllIssuesListAdapter(Context context, List<ListIssueModel> statementList) {
        this.context = context;
        this.statementList = statementList;
    }

    @Override
    public int getCount() {
        return statementList.size();
    }

    @Override
    public Object getItem(int position) {
        return statementList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parentView) {
        final int currentPosition = position;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.adapter_issue_list, null);
            viewHolder = new ViewHolder();

            //viewHolder.tvId = (TextView) convertView.findViewById(R.id.tvId);
            viewHolder.tvTicketNo = (TextView) convertView.findViewById(R.id.tvTicketNo);
            //viewHolder.tvNew = (TextView) convertView.findViewById(R.id.tvNew);
            viewHolder.tvDetails = (TextView) convertView.findViewById(R.id.tvDetails);
            viewHolder.tvStatus = (TextView) convertView.findViewById(R.id.tvStatus);
            viewHolder.tvTicketNum = (TextView) convertView.findViewById(R.id.tvTicketNum);
            viewHolder.tvcomponent = (TextView) convertView.findViewById(R.id.tvcomponent);
            viewHolder.tvdate = (TextView) convertView.findViewById(R.id.tvdate);


            //viewHolder.tvNews = (TextView) convertView.findViewById(R.id.tvNews);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();

        }
        //viewHolder.tvId.setText(String.valueOf(statementList.get(position).getId()));
        viewHolder.tvTicketNo.setText("Ref" + "No.: " + statementList.get(position).getTicketNo());
        viewHolder.tvDetails.setText(statementList.get(position).getDescription());
        viewHolder.tvStatus.setText(statementList.get(position).getStatus());
        viewHolder.tvdate.setText(getDate(statementList.get(position).getDate()));
        viewHolder.tvcomponent.setText(statementList.get(position).getComponent());
        return convertView;
    }


    static class ViewHolder {
        TextView tvId, tvTicketNo, tvNew, tvDetails, tvStatus;
        TextView tvTicketNum, tvdate, tvcomponent, tvNews;

    }

    private CharSequence getDate(long timeStamp) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            Date netDate = (new Date(timeStamp));
            return sdf.format(netDate);
        } catch (Exception ex) {
            return "xx";
        }

    }
}
