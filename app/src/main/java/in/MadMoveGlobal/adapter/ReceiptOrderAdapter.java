package in.MadMoveGlobal.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;

import java.util.ArrayList;

import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.model.OrderModel;

/**
 * Created by Dushant on 07/27/2017.
 */
public class ReceiptOrderAdapter extends BaseAdapter {

    private ViewHolder viewHolder;
    private Context context;
    private ArrayList<OrderModel> statementList;

    public ReceiptOrderAdapter(Context context, ArrayList<OrderModel> statementList) {
        this.context = context;
        this.statementList = statementList;
    }

    @Override
    public int getCount() {
        return statementList.size();
    }

    @Override
    public Object getItem(int position) {
        return statementList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(int position, View convertView, ViewGroup parentView) {
        final int currentPosition = position;
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.adapter_order_new, null);
            viewHolder = new ViewHolder();
            viewHolder.Image = (ImageView) convertView.findViewById(R.id.ivImage);
            viewHolder.Order = (TextView) convertView.findViewById(R.id.tvOrder);
            viewHolder.Status = (TextView) convertView.findViewById(R.id.tvStatus);
            viewHolder.Date = (TextView) convertView.findViewById(R.id.tvDate);
            viewHolder.Service = (TextView) convertView.findViewById(R.id.tvService);
            viewHolder.Amount = (TextView) convertView.findViewById(R.id.tvAmount);
            viewHolder.Description = (TextView) convertView.findViewById(R.id.idDescription);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (!statementList.get(position).getImage().isEmpty()) {
            AQuery aq = new AQuery(context);
            aq.id(viewHolder.Image).background(R.color.white_text).image(statementList.get(position).getImage(), true, true);
            viewHolder.Image.setVisibility(View.VISIBLE);
        } else if (statementList.get(position).getDescription().equalsIgnoreCase("null") && statementList.get(position).getImage().equalsIgnoreCase("null")) {
            AQuery aqs = new AQuery(context);
            viewHolder.Image.setImageResource(R.drawable.menu_bus);
//            aqs.id(viewHolder.Image).background(R.color.white_text).image(R.drawable.menu_bus);
            viewHolder.Image.setVisibility(View.VISIBLE);
        } else {
            viewHolder.Image.setVisibility(View.GONE);
        }
        viewHolder.Order.setText("Order No." + statementList.get(position).getTransactionNumber());
        viewHolder.Status.setText(statementList.get(position).getServiceStatus());
        viewHolder.Service.setText(statementList.get(position).getServiceName());
        viewHolder.Amount.setText(context.getResources().getString(R.string.rupease) + statementList.get(position).getAmountPaid());

        if (statementList.get(position).getDescription().equalsIgnoreCase("null")) {
            viewHolder.Description.setText("");
        } else {
            viewHolder.Description.setText(statementList.get(position).getDescription());

        }
//        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy hh:mm" + " aaa");
//        String dateString = dateFormat.format(new Date(Long.parseLong(statementList.get(position).getDate())));
//
//        viewHolder.Date.setText(dateString);


        viewHolder.Date.setText(statementList.get(position).getDate());

        Log.i("TOTAL", statementList.get(position).getTransactionNumber());


        if (statementList.get(position).getServiceStatus().equals("Success")) {
            viewHolder.Status.setTextColor(context.getResources().getColor(R.color.success));
        } else if (statementList.get(position).getServiceStatus().equals("Booked")) {
            viewHolder.Status.setTextColor(context.getResources().getColor(R.color.success));
        } else if (statementList.get(position).getServiceStatus().equals("Failed")) {
            viewHolder.Status.setTextColor(context.getResources().getColor(R.color.faliure));
        } else if (statementList.get(position).getServiceStatus().equals("Reversed")) {
            viewHolder.Status.setTextColor(context.getResources().getColor(R.color.reverse));
        } else {
            viewHolder.Status.setTextColor(context.getResources().getColor(R.color.blue));
        }


//        String[] dateString = statementList.get(position).get().split("T");
//
//        viewHolder.tvReceiptDate.setText(dateString[0]);
//
//        viewHolder.tvReceiptStatus.setText(statementList.get(position).getStatus());
//        if (statementList.get(position).getIndicator().equals("debit")) {
//
//            viewHolder.tvReceiptAmount.setText("-" + context.getResources().getString(R.string.rupease) + statementList.get(position).getAmount());
//
//        }
//        if (statementList.get(position).getIndicator().equals("credit")) {
//
//            viewHolder.tvReceiptAmount.setText("+" + context.getResources().getString(R.string.rupease) + statementList.get(position).getAmount());
//
//        }

        return convertView;
    }

    static class ViewHolder {
        TextView Order, Status, Date, Service, Amount, Description;
        ImageView Image;

    }
}
