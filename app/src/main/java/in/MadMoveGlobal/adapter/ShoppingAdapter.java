//package in.payqwik.adapter;
//
//import android.content.Context;
//import android.content.Intent;
//import android.graphics.Paint;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.BaseAdapter;
//import android.widget.Button;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.android.volley.Request;
//import com.android.volley.RequestQueue;
//import com.android.volley.Response;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.JsonObjectRequest;
//import com.android.volley.toolbox.Volley;
////import com.squareup.picasso.Picasso;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.io.UnsupportedEncodingException;
//import java.net.URLEncoder;
//import java.util.List;
//
//import in.msewa.custom.LoadingDialog;
//import in.msewa.metadata.ApiUrl;
//import in.msewa.model.PQCart;
//import in.msewa.model.ShoppingModel;
//import in.msewa.model.UserModel;
//import in.msewa.test.R;
//import in.msewa.test.activity.ProductDetailActivity;
//import in.msewa.util.AddRemoveCartListner;
//
///**
// * Created by Ksf on 3/27/2016.
// */
//public class ShoppingAdapter extends BaseAdapter {
//
//    private List<ShoppingModel> shoppingArray;
//    private Context context;
//    private LayoutInflater layoutInflater;
//    ViewHolder viewHolder;
//    private PQCart pqCart = PQCart.getInstance();
//    private RequestQueue rq;
//    private UserModel session = UserModel.getInstance();
//    private AddRemoveCartListner adpListner;
//
//    private LoadingDialog loadingDialog;
//
//    public ShoppingAdapter(List<ShoppingModel> shoppingArray, Context context, AddRemoveCartListner adapterListner) {
//        this.shoppingArray = shoppingArray;
//        this.context = context;
//        this.adpListner = adapterListner;
//        try {
//            loadingDialog = new LoadingDialog(context);
//            rq = Volley.newRequestQueue(context);
//            layoutInflater = LayoutInflater.from(context);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    @Override
//    public int getCount() {
//        return shoppingArray.size();
//    }
//
//    @Override
//    public Object getItem(int i) {
//        return shoppingArray.get(i);
//    }
//
//    @Override
//    public long getItemId(int i) {
//        return i;
//    }
//
//    @Override
//    public View getView(int i, View convertView, ViewGroup viewGroup) {
//        final int currentPostion = i;
//        if (convertView == null) {
//            convertView = layoutInflater.inflate(R.layout.adapter_shopping, viewGroup, false);
//            viewHolder = new ViewHolder();
//            viewHolder.ivShoppingItems = (ImageView) convertView.findViewById(R.id.ivShoppingItems);
////            viewHolder.tvShoppingMRP = (TextView) convertView.findViewById(R.id.tvShoppingMRP);
//            viewHolder.tvShoppingPrice = (TextView) convertView.findViewById(R.id.tvShoppingPrice);
//            viewHolder.tvShoppingName = (TextView) convertView.findViewById(R.id.tvShoppingName);
////            viewHolder.tvShoppingDiscount = (TextView) convertView.findViewById(R.id.tvShoppingDiscount);
//            viewHolder.btnShoppingDetails = (Button) convertView.findViewById(R.id.btnShoppingDetails);
//            viewHolder.btnShoppingRemove = (Button) convertView.findViewById(R.id.btnShoppingRemove);
//            convertView.setTag(viewHolder);
//
//        } else {
//            viewHolder = (ViewHolder) convertView.getTag();
//        }
//
//        if (pqCart.isProductInCart(shoppingArray.get(currentPostion))) {
//            viewHolder.btnShoppingRemove.setText("Remove");
//            viewHolder.btnShoppingRemove.setBackgroundResource(R.drawable.bg_gray_btn);
//
//        }
//        else{
//            viewHolder.btnShoppingRemove.setText("Add");
//            viewHolder.btnShoppingRemove.setBackgroundResource(R.drawable.bg_red_btn);
//        }
//
//        viewHolder.btnShoppingRemove.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (pqCart.isProductInCart(shoppingArray.get(currentPostion))) {
//                    loadingDialog.show();
//                    removeProduct(shoppingArray.get(currentPostion));
//                } else {
//                    loadingDialog.show();
//                    addProduct(shoppingArray.get(currentPostion));
//
//                }
//            }
//        });
//
//        viewHolder.tvShoppingMRP.setText("INR " +shoppingArray.get(currentPostion).getpPrice());
//        viewHolder.tvShoppingPrice.setText("INR " + shoppingArray.get(currentPostion).getpPrice());
//        viewHolder.tvShoppingName.setText(shoppingArray.get(currentPostion).getpName());
//        viewHolder.tvShoppingDiscount.setText(""+shoppingArray.get(currentPostion).getpPrice());
////        Picasso.with(context).load(shoppingArray.get(currentPostion).getpImage()).placeholder(R.drawable.telebuy_logo).into(viewHolder.ivShoppingItems);
//
//        viewHolder.tvShoppingMRP.setPaintFlags(viewHolder.tvShoppingMRP
//                .getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//
//        viewHolder.btnShoppingDetails.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent detailIntent = new Intent(context, ProductDetailActivity.class);
//                detailIntent.putExtra("Shopping Data", shoppingArray.get(currentPostion));
//                context.startActivity(detailIntent);
//            }
//        });
//        return convertView;
//
//    }
//
//
//
//    public void removeProduct(final ShoppingModel shopingModel){
//        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, ApiUrl.URL_REMOVE_CART+session.getUserMobileNo()+"&PRODID="+shopingModel.getPid()+"&SIZEID=-1", (String)null,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        Log.i("aDD rESPONSE",response.toString());
//                        try {
//                            String responseCode = response.getString("response_code");
//                            if (responseCode.equals("0")) {
//                                pqCart.addProductsInCart(shopingModel);
//                                notifyDataSetChanged();
//                                adpListner.taskCompleted("Remove");
//                                loadingDialog.dismiss();
//
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                            loadingDialog.dismiss();
//                        }
//                    }
//                }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                loadingDialog.dismiss();
//            }
//
//        }) {
//
//        };
//        rq.add(jsonObjReq);
//    }
//
//    public void addProduct(final ShoppingModel shopingModel){
//        String pName = null;
//        try {
//            pName = URLEncoder.encode(shopingModel.getpName(), "UTF-8");
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
//        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, ApiUrl.URL_ADD_CART+session.getUserMobileNo()+"&PRODID="+shopingModel.getPid()+"&PRODDESC="+ pName+"&SIZEID=-1&SIZEDESC=NA&QNTY=1&EACH=10000", (String)null,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        Log.i("aDD rESPONSE",response.toString());
//                        try {
//                            String responseCode = response.getString("response_code");
//                            if (responseCode.equals("0")) {
//                                pqCart.addProductsInCart(shopingModel);
//                                notifyDataSetChanged();
//                                adpListner.taskCompleted("Add");
//                                loadingDialog.dismiss();
//
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                            loadingDialog.dismiss();
//                        }
//                    }
//                }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                loadingDialog.dismiss();
//            }
//
//        }) {
//
//        };
//        rq.add(jsonObjReq);
//    }
//
//
//    static class ViewHolder {
//        ImageView ivShoppingItems;
//        TextView tvShoppingMRP, tvShoppingPrice, tvShoppingName, tvShoppingDiscount;
//        Button btnShoppingDetails, btnShoppingRemove;
//
//    }
//}
