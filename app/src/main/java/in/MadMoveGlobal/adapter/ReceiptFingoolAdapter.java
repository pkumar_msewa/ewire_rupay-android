package in.MadMoveGlobal.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.model.FingoolHistoryModel;

/**
 * Created by Dushant on 07/27/2017.
 */
public class ReceiptFingoolAdapter extends BaseAdapter {

    private ViewHolder viewHolder;
    private Context context;
    private ArrayList<FingoolHistoryModel> statementList;

    public ReceiptFingoolAdapter(Context context, ArrayList<FingoolHistoryModel> statementList) {
        this.context = context;
        this.statementList = statementList;
    }

    @Override
    public int getCount() {
        return statementList.size();
    }

    @Override
    public Object getItem(int position) {
        return statementList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(int position, View convertView, ViewGroup parentView) {
        final int currentPosition = position;
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.adapter_fingool_history, null);
            viewHolder = new ViewHolder();
            viewHolder.Name = (TextView) convertView.findViewById(R.id.tvName);
            viewHolder.Mobile = (TextView) convertView.findViewById(R.id.tvMobile);
            viewHolder.Coi = (TextView) convertView.findViewById(R.id.tvCoi);
            viewHolder.Amount = (TextView) convertView.findViewById(R.id.tvAmounte);
            viewHolder.Date = (TextView) convertView.findViewById(R.id.tvDate);
            viewHolder.TxnNo = (TextView) convertView.findViewById(R.id.tvTxnNo);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }


        Date date = new Date(statementList.get(position).getDate());



        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
        String dateString = "";
        if (statementList.get(position).getDate() == "") {
            dateString = "";
        } else {
//            dateString = statementList.get(position).getDate();
            dateString = dateFormat.format((date));
        }
//


        viewHolder.Name.setText("Name          : " + statementList.get(position).getName());
        viewHolder.Mobile.setText("Mobile No.  : " + statementList.get(position).getMobile());
        viewHolder.Coi.setText("COI              : " + statementList.get(position).getCoi());
        viewHolder.Amount.setText("Amount       : " + context.getResources().getString(R.string.rupease) + statementList.get(position).getAmountPaid());
        viewHolder.Date.setText("Date            : " + dateString);
        viewHolder.TxnNo.setText("Txn No.       : " + statementList.get(position).getTransactionNo());


//        viewHolder.Date.setText(statementList.get(position).getDate());

        Log.i("TOTAL", statementList.get(position).getTransactionNo());


//        if (statementList.get(position).getServiceStatus().equals("Success")) {
//            viewHolder.Status.setTextColor(context.getResources().getColor(R.color.success));
//        } else if (statementList.get(position).getServiceStatus().equals("Booked")) {
//            viewHolder.Status.setTextColor(context.getResources().getColor(R.color.success));
//        } else if (statementList.get(position).getServiceStatus().equals("Failed")) {
//            viewHolder.Status.setTextColor(context.getResources().getColor(R.color.faliure));
//        } else if (statementList.get(position).getServiceStatus().equals("Reversed")) {
//            viewHolder.Status.setTextColor(context.getResources().getColor(R.color.reverse));
//        } else {
//            viewHolder.Status.setTextColor(context.getResources().getColor(R.color.blue));
//        }


//        String[] dateString = statementList.get(position).get().split("T");
//
//        viewHolder.tvReceiptDate.setText(dateString[0]);
//
//        viewHolder.tvReceiptStatus.setText(statementList.get(position).getStatus());
//        if (statementList.get(position).getIndicator().equals("debit")) {
//
//            viewHolder.tvReceiptAmount.setText("-" + context.getResources().getString(R.string.rupease) + statementList.get(position).getAmount());
//
//        }
//        if (statementList.get(position).getIndicator().equals("credit")) {
//
//            viewHolder.tvReceiptAmount.setText("+" + context.getResources().getString(R.string.rupease) + statementList.get(position).getAmount());
//
//        }

        return convertView;
    }

    static class ViewHolder {
        TextView Name, Mobile, Coi, Amount, Date, TxnNo;
//        ImageView Image;

    }
}
