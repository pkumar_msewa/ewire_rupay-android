package in.MadMoveGlobal.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

;import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.model.BusBookedTicketModel;


/**
 * Created by Ksf on 9/30/2016.
 */
public class BusBookListAdapterBackup extends BaseAdapter {

    Date arrTime, depTime = null;
    private Context context;
    private ArrayList<BusBookedTicketModel> busArray;
    private ViewHolder viewHolder;
    private long destinationCode, sourceCode;
    private String dateOfDep;



    public BusBookListAdapterBackup(Context context, ArrayList<BusBookedTicketModel> busArray ) {
        this.context = context;
        this.busArray = busArray;


    }

    @Override
    public int getCount() {
        return busArray.size();
    }

    @Override
    public Object getItem(int index) {
        return busArray.get(index);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final int currentPosition = position;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.adapter_bus_list, parent,false);
            viewHolder = new ViewHolder();
            viewHolder.tvBusListName = (TextView) convertView.findViewById(R.id.tvBusListName);
            viewHolder.tvArrDept = (TextView) convertView.findViewById(R.id.tvArrDept);
            viewHolder.tvJourneyDuration = (TextView) convertView.findViewById(R.id.tvJourneyDuration);
            viewHolder.tvDate = (TextView) convertView.findViewById(R.id.tvDate);
            viewHolder.tvBusFare = (TextView) convertView.findViewById(R.id.tvBusFare);
            viewHolder.tvTicketPnrNo = (TextView) convertView.findViewById(R.id.tvTicketPnrNo);
            viewHolder.tvFromTo = (TextView) convertView.findViewById(R.id.tvFromTo);
            viewHolder.tvUserMobile = (TextView) convertView.findViewById(R.id.tvUserMobile);
            viewHolder.tvBoardingAddress = (TextView) convertView.findViewById(R.id.tvBoardingAddress);
            viewHolder.llBusListMain = (LinearLayout) convertView.findViewById(R.id.llBusListMain);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        Log.i("DATEFORMAT", "Arrival: "+busArray.get(position).getArrTime()+"   Departure: "+busArray.get(position).getDeptTime());
        SimpleDateFormat arrinput = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
        SimpleDateFormat arroutput = new SimpleDateFormat("HH:mm a", Locale.ENGLISH);
        SimpleDateFormat depinput = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
        SimpleDateFormat depoutput = new SimpleDateFormat("HH:mm a", Locale.ENGLISH);
        try {
            arrTime = arrinput.parse(busArray.get(position).getArrTime());
            depTime = depinput.parse(busArray.get(position).getDeptTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        viewHolder.tvBusListName.setText(busArray.get(position).getBusOperator());

        Log.i("ARRDEPT", "ARRTIME: "+String.valueOf(arrTime)+"  DEPTIME: "+String.valueOf(depTime));
        viewHolder.tvArrDept.setText(String.valueOf(arroutput.format(arrTime))+" - "+String.valueOf(depoutput.format(depTime)));
        viewHolder.tvTicketPnrNo.setText("PNR: "+busArray.get(position).getTicketPnr());
        viewHolder.tvFromTo.setText(busArray.get(position).getSource()+" - "+busArray.get(position).getDestination());
        viewHolder.tvDate.setText(busArray.get(position).getJourneyDate());
        viewHolder.tvUserMobile.setText("Mob: "+busArray.get(position).getUserMobile());
        viewHolder.tvBusFare.setText(context.getResources().getString(R.string.rupease)+" "+busArray.get(position).getTotalFare());
        viewHolder.tvBoardingAddress.setText("Board: "+busArray.get(position).getBoardingAddress());


        viewHolder.llBusListMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent busDetailIntent = new Intent(context, BusDetailActivity.class);
//                busDetailIntent.putExtra("busValues",busArray.get(currentPosition));
//
//                busDetailIntent.putExtra("sourceId",sourceCode);
//                busDetailIntent.putExtra("destinationId",destinationCode);
//                busDetailIntent.putExtra("date",dateOfDep);
//
//                busDetailIntent.putExtra("busName",busArray.get(currentPosition).getBusName());
//                busDetailIntent.putExtra("busDetails",busArray.get(currentPosition).getBusType());
//
//                busDetailIntent.putExtra("busBoardingArray",busArray.get(currentPosition).getBusBoardingPointModels());
//                busDetailIntent.putExtra("busDroppingArray",busArray.get(currentPosition).getBusDroppingPointModels());
//                busDetailIntent.putExtra("engineId", busArray.get(currentPosition).getEngineId());
//                busDetailIntent.putExtra("seater", busArray.get(currentPosition).isSeater());
//                busDetailIntent.putExtra("sleeper", busArray.get(currentPosition).isSleeper());
//                context.startActivity(busDetailIntent);
                Toast.makeText(context, "Ticket "+position+" Selected",Toast.LENGTH_SHORT).show();
            }
        });

        return convertView;
    }


    static class ViewHolder {
        TextView tvBusListName;
        TextView tvBusFare;
        TextView tvArrDept, tvJourneyDuration, tvDate, tvTicketPnrNo, tvFromTo, tvUserMobile, tvBoardingAddress;
        LinearLayout llBusListMain;

    }

}
