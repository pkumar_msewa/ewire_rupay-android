package in.MadMoveGlobal.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import in.MadMoveGlobal.model.MobilePlansModel;
import in.MadMoveGlobal.EwireRuPay.R;


/**
 * Created by Dushant on 07/27/2017.
 */
public class MobilePlanAdapter extends BaseAdapter {
    private ViewHolder viewHolder;
    private Context context;
    private int post;
    private ArrayList<MobilePlansModel> mobilePlansList;
    private SharedPreferences sharedpreferences;
    private String prepaidNo, type;

    public MobilePlanAdapter(Context context, ArrayList<MobilePlansModel> mobilePlansList, int post, SharedPreferences sharedpreferences, String prepaidNo, String type) {
        this.context = context;
        this.mobilePlansList = mobilePlansList;
        this.post = post;
        this.prepaidNo = prepaidNo;
        this.sharedpreferences = sharedpreferences;
        this.type = type;
    }

    @Override
    public int getCount() {
        return mobilePlansList.size();
    }

    @Override
    public Object getItem(int position) {
        return mobilePlansList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parentView) {
        final int currentPosition = position;
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.adapter_mobile_plans, null);
            viewHolder = new ViewHolder();
            viewHolder.tvDescription = (TextView) convertView.findViewById(R.id.tvMobilePlansDescription);
            viewHolder.tvTalktime = (TextView) convertView.findViewById(R.id.tvMobilePlansTalkTime);
            viewHolder.tvValidity = (TextView) convertView.findViewById(R.id.tvMobilePlansValidity);
            viewHolder.btnRecharge = (Button) convertView.findViewById(R.id.btnMobilePlansSelect);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.tvTalktime.setText(mobilePlansList.get(position).getTalkTime());
        viewHolder.tvDescription.setText(mobilePlansList.get(position).getDescription());

        if (mobilePlansList.get(position).getValidity().equals("1") || mobilePlansList.get(position).getValidity().equals("0")) {
            viewHolder.tvValidity.setText("Validity " + mobilePlansList.get(position).getValidity() + " day");
        } else {
            viewHolder.tvValidity.setText("Validity " + mobilePlansList.get(position).getValidity() + " days");
        }

        viewHolder.btnRecharge.setText("Rs " + mobilePlansList.get(position).getAmount());
        viewHolder.btnRecharge.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                String selectedAmount = mobilePlansList.get(currentPosition).getAmount().toString();
                String operatorCode = mobilePlansList.get(currentPosition).getOperatorCode();
                Intent resultIntent = new Intent();
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString("amount", selectedAmount);
                editor.putBoolean(type, true);
                editor.putString("prepaidNo", prepaidNo);
                editor.commit();
                resultIntent.putExtra("amount",selectedAmount);

                ((Activity) context).setResult(Activity.RESULT_OK, resultIntent);
                ((Activity) context).finish();
            }

        });
        return convertView;
    }

    static class ViewHolder {
        Button btnRecharge;
        TextView tvTalktime, tvDescription, tvValidity/*, tvAmount*/;
    }
}

