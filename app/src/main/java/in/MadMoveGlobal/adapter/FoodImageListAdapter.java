package in.MadMoveGlobal.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.androidquery.AQuery;

import java.util.ArrayList;

import in.MadMoveGlobal.EwireRuPay.R;

/**
 * Created by Dushant on 07/27/2017.
 */
public class FoodImageListAdapter extends RecyclerView.Adapter<FoodImageListAdapter.RecyclerViewHolders> {

    private ArrayList<String> itemList;
    private Context context;

    public FoodImageListAdapter(Context context, ArrayList<String> itemList) {
        this.itemList = itemList;
        this.context = context;
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_horizonatal_food_list, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolders holder, final int position) {
        AQuery aq = new AQuery(context);
        aq.id(holder.ivFoodHorizontalImage).image(itemList.get(position), true, true);

        holder.llFoodHorizontalMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent("food-image-changed");
                intent.putExtra("position", position);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView ivFoodHorizontalImage;
        public LinearLayout llFoodHorizontalMain;

        public RecyclerViewHolders(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            ivFoodHorizontalImage = (ImageView) itemView.findViewById(R.id.ivFoodHorizontalImage);
            llFoodHorizontalMain = (LinearLayout) itemView.findViewById(R.id.llFoodHorizontalMain);

        }

        @Override
        public void onClick(View view) {
            int i = getAdapterPosition();

        }
    }
}

