package in.MadMoveGlobal.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;

import java.util.ArrayList;

import in.MadMoveGlobal.model.contract;
import in.MadMoveGlobal.EwireRuPay.R;

/**
 * Created by Dushant on 07/27/2017.
 */
public class CustomAdapter extends BaseAdapter {

    private Context activity;
    private ArrayList<contract> data;
    private static LayoutInflater inflater = null;
    private View vi;
    private ViewHolder viewHolder;

    public CustomAdapter(Context context, ArrayList<contract> items) {
        this.activity = context;
        this.data = items;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        vi = view;
        final int pos = position;
        contract items = data.get(pos);
        if (view == null) {
            vi = inflater.inflate(R.layout.contact_list, null);
            viewHolder = new ViewHolder();
            viewHolder.checkBox = (CheckBox) vi.findViewById(R.id.tvcontactname);
            vi.setTag(viewHolder);
        } else
            viewHolder = (ViewHolder) view.getTag();
        viewHolder.checkBox.setText(items.getPcName());
        if (items.isSelected()) {
            viewHolder.checkBox.setChecked(true);

        } else {
            viewHolder.checkBox.setChecked(false);
        }
        return vi;
    }

    public ArrayList<contract> getAllData() {
        return data;
    }

    public void setCheckBox(int position) {
        //Update status of checkbox
        contract items = data.get(position);
        items.setSelected(!items.isSelected());
        notifyDataSetChanged();
    }

    public class ViewHolder {
        CheckBox checkBox;
    }
}