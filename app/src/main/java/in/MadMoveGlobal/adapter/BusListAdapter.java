package in.MadMoveGlobal.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.EwireRuPay.activity.businneractivity.BusDetailActivity;
import in.MadMoveGlobal.model.BusListModel;


/**
 * Created by Ksf on 9/30/2016.
 */
public class BusListAdapter extends BaseAdapter {

    Date arrTime, depTime = null;
    private Context context;
    private List<BusListModel> busArray;
    private ViewHolder viewHolder;
    private long destinationCode, sourceCode;
    private String dateOfDep;



    public BusListAdapter(Context context, List<BusListModel> busArray, long destinationCode, long sourceCode, String dateOfDep ) {
        this.context = context;
        this.busArray = busArray;
        this.dateOfDep = dateOfDep;
        this.sourceCode = sourceCode;
        this.destinationCode = destinationCode;

    }

    @Override
    public int getCount() {
        return busArray.size();
    }

    @Override
    public Object getItem(int index) {
        return busArray.get(index);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final int currentPosition = position;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.adapter_bus_list, parent,false);
            viewHolder = new ViewHolder();
            viewHolder.tvBusListName = (TextView) convertView.findViewById(R.id.tvBusListName);
            viewHolder.tvArrDept = (TextView) convertView.findViewById(R.id.tvArrDept);
            viewHolder.tvJourneyDuration = (TextView) convertView.findViewById(R.id.tvJourneyDuration);
            viewHolder.tvSeatsLeft = (TextView) convertView.findViewById(R.id.tvSeatsLeft);
            viewHolder.tvBusType = (TextView) convertView.findViewById(R.id.tvBusType);
            viewHolder.tvBusFare = (TextView) convertView.findViewById(R.id.tvBusFare);
            viewHolder.llBusListMain = (LinearLayout) convertView.findViewById(R.id.llBusListMain);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        SimpleDateFormat inputFormat = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
        SimpleDateFormat outputFormat = new SimpleDateFormat("hh:mm aa", Locale.ENGLISH);

        try {
            arrTime = inputFormat.parse(busArray.get(position).getBusArrivalTime());
            depTime = inputFormat.parse(busArray.get(position).getBusDepTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if(busArray.get(position).getBusAvailableSeat() == null && Integer.parseInt(busArray.get(position).getBusAvailableSeat())==0){
            viewHolder.llBusListMain.setBackgroundColor(context.getResources().getColor(R.color.dark_grey));
            viewHolder.llBusListMain.setEnabled(false);
        }else {
            viewHolder.llBusListMain.setBackgroundColor(context.getResources().getColor(R.color.white_text));
            viewHolder.llBusListMain.setEnabled(true);
        }
        viewHolder.tvBusListName.setText(busArray.get(position).getBusName());
        viewHolder.tvBusType.setText(busArray.get(position).getBusType());
         if(!busArray.get(position).getBusAvailableSeat().equalsIgnoreCase("null")&& Integer.parseInt(busArray.get(position).getBusAvailableSeat())>1){
            viewHolder.tvSeatsLeft.setText(""+busArray.get(position).getBusAvailableSeat()+" Seats left");
        }else {
            viewHolder.tvSeatsLeft.setText(""+busArray.get(position).getBusAvailableSeat()+" Seat left");
        }
        viewHolder.tvArrDept.setText(String.valueOf(outputFormat.format(depTime))+" - "+String.valueOf(outputFormat.format(arrTime)));
        viewHolder.tvJourneyDuration.setText(busArray.get(position).getBusDuration());

        if(busArray.get(position).getBusFare().contains("/")){
            String[] fareArray = busArray.get(position).getBusFare().split("/");
            viewHolder.tvBusFare.setText(context.getResources().getString(R.string.rupease)+" "+fareArray[0]);

        }else{
            viewHolder.tvBusFare.setText(context.getResources().getString(R.string.rupease)+" "+busArray.get(position).getBusFare());
        }




        viewHolder.llBusListMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent busDetailIntent = new Intent(context, BusDetailActivity.class);
                busDetailIntent.putExtra("busValues",busArray.get(currentPosition));

                busDetailIntent.putExtra("sourceId",sourceCode);
                busDetailIntent.putExtra("destinationId",destinationCode);
                busDetailIntent.putExtra("date",dateOfDep);

                busDetailIntent.putExtra("busName",busArray.get(currentPosition).getBusName());
                busDetailIntent.putExtra("busDetails",busArray.get(currentPosition).getBusType());

                busDetailIntent.putExtra("busBoardingArray",busArray.get(currentPosition).getBusBoardingPointModels());
                busDetailIntent.putExtra("busDroppingArray",busArray.get(currentPosition).getBusDroppingPointModels());
                busDetailIntent.putExtra("engineId", busArray.get(currentPosition).getEngineId());
                busDetailIntent.putExtra("seater", busArray.get(currentPosition).isSeater());
                busDetailIntent.putExtra("sleeper", busArray.get(currentPosition).isSleeper());
                busDetailIntent.putExtra("bpId", busArray.get(currentPosition).getBpId());
                busDetailIntent.putExtra("dpId", busArray.get(currentPosition).getDpId());
                busDetailIntent.putExtra("bpDpLayout", busArray.get(currentPosition).isBpDpLayout());
                context.startActivity(busDetailIntent);
            }
        });

        return convertView;
    }


    static class ViewHolder {
        TextView tvBusListName;
        TextView tvBusType;
        TextView tvBusFare;
        TextView tvArrDept, tvJourneyDuration, tvSeatsLeft;
        LinearLayout llBusListMain;

    }

}
