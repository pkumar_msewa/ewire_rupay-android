package in.MadMoveGlobal.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;

import java.util.ArrayList;

import in.MadMoveGlobal.model.FoodModel;
import in.MadMoveGlobal.EwireRuPay.R;

/**
 * Created by Dushant on 07/27/2017.
 */
public class FoodListAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<FoodModel> foodArray;
    private ViewHolder viewHolder;

    public FoodListAdapter(Context context, ArrayList<FoodModel> foodArray) {
        this.context = context;
        this.foodArray = foodArray;

    }

    @Override
    public int getCount() {
        return foodArray.size();
    }

    @Override
    public Object getItem(int index) {
        return foodArray.get(index);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.adapter_food_list, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.llFoodMainList = (CardView) convertView.findViewById(R.id.llFoodMainList);
            viewHolder.tvFoodTitleList = (TextView) convertView.findViewById(R.id.tvFoodTitleList);
            viewHolder.tvFoodRetailPriceList = (TextView) convertView.findViewById(R.id.tvFoodRetailPriceList);
            viewHolder.tvFoodApplicablePriceList = (TextView) convertView.findViewById(R.id.tvFoodApplicablePriceList);
            viewHolder.ivFoodList = (ImageView) convertView.findViewById(R.id.ivFoodList);
            viewHolder.tvFoodDiscountList = (TextView) convertView.findViewById(R.id.tvFoodDiscountList);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.tvFoodApplicablePriceList.setText(context.getResources().getString(R.string.rupease) + " " + foodArray.get(position).getpApplicablePrice());
        viewHolder.tvFoodTitleList.setText(foodArray.get(position).getpName());

        if (foodArray.get(position).isDiscount()) {
            viewHolder.tvFoodRetailPriceList.setText(context.getResources().getString(R.string.rupease) + " " + foodArray.get(position).getpRetailPrice());
            viewHolder.tvFoodRetailPriceList.setPaintFlags(viewHolder.tvFoodRetailPriceList.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            viewHolder.tvFoodDiscountList.setText(foodArray.get(position).getpDiscountPrice()+"% Off");
            viewHolder.tvFoodRetailPriceList.setTextColor(ContextCompat.getColor(context,R.color.mark_red));
            viewHolder.tvFoodDiscountList.setVisibility(View.VISIBLE);
            viewHolder.tvFoodRetailPriceList.setVisibility(View.VISIBLE);
        } else {
            viewHolder.tvFoodRetailPriceList.setVisibility(View.GONE);
            viewHolder.tvFoodDiscountList.setVisibility(View.GONE);
        }

        if (!foodArray.get(position).getpThumb().isEmpty()) {
            AQuery aq = new AQuery(context);
            aq.id(viewHolder.ivFoodList).background(R.drawable.loading_image).image(foodArray.get(position).getpThumb(), true, true);
            viewHolder.ivFoodList.setVisibility(View.VISIBLE);
        } else {
            viewHolder.ivFoodList.setVisibility(View.GONE);
        }

        viewHolder.tvFoodApplicablePriceList.setText(context.getResources().getString(R.string.rupease) + " " + foodArray.get(position).getpApplicablePrice());
        return convertView;
    }

    static class ViewHolder {
        TextView tvFoodApplicablePriceList;
        TextView tvFoodRetailPriceList;
        TextView tvFoodTitleList;
        ImageView ivFoodList;
        CardView llFoodMainList;
        TextView tvFoodDiscountList;

    }
}