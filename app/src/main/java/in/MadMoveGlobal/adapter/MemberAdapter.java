package in.MadMoveGlobal.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.model.MemberModel;

public class MemberAdapter extends ArrayAdapter {

    private int adapterLayout;
    private Context mContext;
    private View view;

    public MemberAdapter(@NonNull Context context, int resource, ArrayList<MemberModel> memberModelList) {
        super(context, resource, memberModelList);
        this.adapterLayout = resource;
        this.mContext = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        view = convertView;

        if(view==null){
            LayoutInflater layoutInflater;
             layoutInflater = LayoutInflater.from(mContext);
             view = layoutInflater.inflate(adapterLayout,null);
        }

        MemberModel memberModel = (MemberModel) getItem(position);

        TextView tvName = view.findViewById(R.id.etName);
        TextView tvMobile = view.findViewById(R.id.etMobile);
        TextView tvNoOfDays = view.findViewById(R.id.noOfDays);

        tvName.setText(memberModel.getMemberName());
        tvMobile.setText(memberModel.getMobile());
        tvNoOfDays.setText(memberModel.getNoOfDays());

        return view;
    }



}
