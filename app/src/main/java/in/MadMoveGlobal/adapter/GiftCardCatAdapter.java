package in.MadMoveGlobal.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.squareup.picasso.Picasso;

import java.util.List;

import in.MadMoveGlobal.model.GiftCardCatModel;
import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.EwireRuPay.activity.giftcardactivity.GiftCardListActivity;


/**
 * Created by Dushant on 07/27/2017.
 */

public class GiftCardCatAdapter extends RecyclerView.Adapter<GiftCardCatAdapter.RecyclerViewHolders> {

  String categoryDS;
  private List<GiftCardCatModel> itemList;
  private Context context;


  public GiftCardCatAdapter(Context context, List<GiftCardCatModel> itemList) {
    super();
    this.itemList = itemList;
    this.context = context;
  }

  @Override
  public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

    View v = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.adapter_gift_card_cat, parent, false);
    RecyclerViewHolders viewHolder = new RecyclerViewHolders(v);
    return viewHolder;
  }

  @Override
  public void onBindViewHolder(final RecyclerViewHolders holder, int position) {
    AQuery aq = new AQuery(context);

    if (itemList.get(position).getImages().equalsIgnoreCase("")) {
      Picasso.with(context).load("ksehfkdjh").placeholder(R.drawable.ic_loading_image).error(R.drawable.circlegradient).into(holder.ivGiftCardCat);
    } else {
      Picasso.with(context).load(itemList.get(position).getImages()).placeholder(R.drawable.ic_loading_image).error(R.drawable.circlegradient).into(holder.ivGiftCardCat);
    }
    holder.tvDescription.setText(String.valueOf(itemList.get(position).

            getBrand_name()));


  }


  @Override
  public int getItemCount() {
    return this.itemList.size();
  }


  public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {
    public ImageView ivGiftCardCat;
    public Button btnTerms;
    private LinearLayout llGiftCard;
    public TextView tvDescription, tvTerms;


    public RecyclerViewHolders(View itemView) {
      super(itemView);
      ivGiftCardCat = (ImageView) itemView.findViewById(R.id.ivGiftCardCat);
      tvDescription = (TextView) itemView.findViewById(R.id.tvTittle);
      llGiftCard = (LinearLayout) itemView.findViewById(R.id.llGiftCard);
      llGiftCard.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
      int i = getAdapterPosition();
      if (view.getId() == R.id.llGiftCard) {
        Intent giftCardItemIntent = new Intent(context, GiftCardListActivity.class);
        giftCardItemIntent.putExtra("Model", itemList.get(i));
        context.startActivity(giftCardItemIntent);
      }
    }
  }


}

