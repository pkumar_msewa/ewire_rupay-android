package in.MadMoveGlobal.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import in.MadMoveGlobal.custom.TextDrawable;
import in.MadMoveGlobal.model.ImagicaTicketModel;
import in.MadMoveGlobal.model.TicketDetail;
import in.MadMoveGlobal.EwireRuPay.R;

/**
 * Created by Dushant on 07/27/2017.
 */


public class TicketAdapter extends RecyclerView.Adapter<TicketAdapter.ViewHolder> {

    ArrayList<ImagicaTicketModel> SubjectValue;
    static Context context;
    View view1;
    ViewHolder viewHolder1;
    TextView textView;
    private boolean valueEnableAmount = true;
    private HashMap<String, String> stringStringHashMap, totalAmount;
    private JSONObject jsonObject, amount;
    private String type;
    RecyclerView recyclerView;

    public TicketAdapter(Context context1, ArrayList<ImagicaTicketModel> SubjectValues1, String type, RecyclerView recyclerView) {

        SubjectValue = SubjectValues1;
        context = context1;
        this.stringStringHashMap = new HashMap<>();
        this.totalAmount = new HashMap<>();
        this.type = type;
        this.recyclerView = recyclerView;
        this.jsonObject = new JSONObject();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView tvAdultpercost, tvchildpercost, tvScrpercost;
        private final TextView tvAdultchildType, tvAdultchildpercost;
        private final ImageButton btnAdultChildup, AdultChildbtndown;
        private final TextView tvAdultChildquantityn;
        private final LinearLayout llCollege;
        public TextView Tdescription, Ttitle, tvCollegequantityn, tvquantityn, tvCollegepercost, tvAdultquantityn, tvScrquantityn, tvchildquantityn, tvtotalcost;
        private ImageView ivimage;
        CardView cardView1, cardView2;

        private LinearLayout llAdultChild, lladult;
        LinearLayout llSenior, llChild;
        private ImageButton ibbtndowns, btnCollegedown, btnup, btnAdultup, Adultbtndown, btnchildup, btnchilddown, btnScrup, btnScrdown, btnCollege;
        int i = 0;

        public ViewHolder(View v) {
            super(v);
            Ttitle = (TextView) v.findViewById(R.id.tvITitle);
            Tdescription = (TextView) v.findViewById(R.id.tvDescription);
            ivimage = (ImageView) v.findViewById(R.id.ivimage);
            cardView1 = (CardView) v.findViewById(R.id.cvamt);
            cardView2 = (CardView) v.findViewById(R.id.cvpacktick);
            llSenior = (LinearLayout) v.findViewById(R.id.llSenior);
            llChild = (LinearLayout) v.findViewById(R.id.llChild);
            tvAdultpercost = (TextView) v.findViewById(R.id.tvAdultpercost);
            tvchildpercost = (TextView) v.findViewById(R.id.tvchildpercost);
            tvScrpercost = (TextView) v.findViewById(R.id.tvScrpercost);
            llAdultChild = (LinearLayout) v.findViewById(R.id.llAdultChild);
            ibbtndowns = (ImageButton) v.findViewById(R.id.btndowns);
            btnup = (ImageButton) v.findViewById(R.id.btnup);
            btnAdultup = (ImageButton) v.findViewById(R.id.btnAdultup);
            Adultbtndown = (ImageButton) v.findViewById(R.id.Adultbtndown);
            tvAdultquantityn = (TextView) v.findViewById(R.id.tvAdultquantityn);
            tvAdultchildType = (TextView) v.findViewById(R.id.tvAdultchildType);
            tvAdultchildpercost = (TextView) v.findViewById(R.id.tvAdultchildpercost);
            btnAdultChildup = (ImageButton) v.findViewById(R.id.btnAdultChildup);
            tvAdultChildquantityn = (TextView) v.findViewById(R.id.tvAdultChildquantityn);
            AdultChildbtndown = (ImageButton) v.findViewById(R.id.AdultChildbtndown);
            btnchildup = (ImageButton) v.findViewById(R.id.btnchildup);
            btnchilddown = (ImageButton) v.findViewById(R.id.btnchilddown);
            tvchildquantityn = (TextView) v.findViewById(R.id.tvchildquantityn);
            lladult = (LinearLayout) v.findViewById(R.id.lladult);
            btnScrup = (ImageButton) v.findViewById(R.id.btnScrup);
            btnScrdown = (ImageButton) v.findViewById(R.id.btnScrdown);
            tvScrquantityn = (TextView) v.findViewById(R.id.tvScrquantityn);
            llCollege = (LinearLayout) v.findViewById(R.id.llCollege);
            tvCollegepercost = (TextView) v.findViewById(R.id.tvCollegepercost);
            tvtotalcost = (TextView) v.findViewById(R.id.tvtotalcost);
            btnCollege = (ImageButton) v.findViewById(R.id.btnCollege);
            tvCollegequantityn = (TextView) v.findViewById(R.id.tvCollegequantityn);
            btnCollegedown = (ImageButton) v.findViewById(R.id.btnCollegedown);
            TextDrawable plus = new TextDrawable(context, "+", ColorStateList.valueOf(context.getResources().getColor(R.color.white_text)), 64, TextDrawable.VerticalAlignment.BASELINE);
            TextDrawable mins = new TextDrawable(context, "-", ColorStateList.valueOf(context.getResources().getColor(R.color.white_text)), 64, TextDrawable.VerticalAlignment.BASELINE);
            btnchildup.setImageDrawable(mins);
            btnScrup.setImageDrawable(mins);
            btnchildup.setImageDrawable(mins);
            btnAdultup.setImageDrawable(mins);
            btnAdultChildup.setImageDrawable(mins);
            Adultbtndown.setImageDrawable(plus);
            btnchilddown.setImageDrawable(plus);
            btnScrdown.setImageDrawable(plus);
            btnAdultup.setImageDrawable(mins);
            btnAdultChildup.setImageDrawable(mins);
            AdultChildbtndown.setImageDrawable(plus);
            btnCollege.setImageDrawable(mins);
            btnCollegedown.setImageDrawable(plus);
            btnup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (cardView1.getVisibility() == View.GONE) {
                        cardView1.setVisibility(View.VISIBLE);
                        btnup.setVisibility(View.GONE);
                        ibbtndowns.setVisibility(View.VISIBLE);
                    } else {
                        cardView1.setVisibility(View.GONE);
                        btnup.setVisibility(View.VISIBLE);
                        btnup.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_down));
                        ibbtndowns.setVisibility(View.GONE);

                    }
                }
            });


            cardView2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (cardView1.getVisibility() == View.GONE) {
                        cardView1.setVisibility(View.VISIBLE);
                        btnup.setVisibility(View.GONE);
                        ibbtndowns.setVisibility(View.VISIBLE);
                    } else {
                        cardView1.setVisibility(View.GONE);
                        btnup.setVisibility(View.VISIBLE);
                        btnup.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_down));
                        ibbtndowns.setVisibility(View.GONE);

                    }
                }
            });


        }
    }

    @Override
    public TicketAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.ticket_adapter, parent, false);
        viewHolder1 = new ViewHolder(view1);

        return viewHolder1;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final ImagicaTicketModel imagicaTicketModel = (ImagicaTicketModel) SubjectValue.get(position);

        if (getPartyByName1(imagicaTicketModel, "Adult") != null) {
            holder.tvAdultpercost.setText(String.valueOf(getPartyByName(SubjectValue.get(position).getTicketDetail(), "Adult").getTdcostPerQty()));
            holder.tvAdultchildType.setText(getPartyByName(SubjectValue.get(position).getTicketDetail(), "Adult").getTdtype());
            if (position == 0) {
                holder.tvAdultquantityn.setText(String.valueOf(getPartyByName(imagicaTicketModel.getTicketDetail(), "Adult").getTdquantity()));
            }
            stringStringHashMap.put(position + "Adult", String.valueOf(getPartyByName(imagicaTicketModel.getTicketDetail(), "Adult").getTdproductID()));
            try {
                if (jsonObject.has(stringStringHashMap.get(position + "Adult"))) {
                    jsonObject.remove(stringStringHashMap.get(position + "Adult"));
                    jsonObject.put(stringStringHashMap.get(position + "Adult"), holder.tvAdultquantityn.getText().toString());
                } else {
                    jsonObject.put(stringStringHashMap.get(position + "Adult"), holder.tvAdultquantityn.getText().toString());
                }

                if (totalAmount.containsKey(String.valueOf(position + "Adult"))) {
                    totalAmount.remove(String.valueOf(position + "Adult"));
                    totalAmount.put(String.valueOf(position + "Adult"), holder.tvAdultpercost.getText().toString() + "," + holder.tvAdultquantityn.getText().toString());
                } else {
                    totalAmount.put(String.valueOf(position + "Adult"), holder.tvAdultpercost.getText().toString() + "," + holder.tvAdultquantityn.getText().toString());
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (getPartyByName1(imagicaTicketModel, "Adult") == null) {
            holder.lladult.setVisibility(View.GONE);
        }
        if (getPartyByName1(imagicaTicketModel, "Child") != null) {

            holder.tvchildpercost.setText(String.valueOf(getPartyByName(imagicaTicketModel.getTicketDetail(), "Child").getTdcostPerQty()));
            holder.tvchildquantityn.setText(String.valueOf(getPartyByName(imagicaTicketModel.getTicketDetail(), "Child").getTdquantity()));
            stringStringHashMap.put(position + "Child", String.valueOf(getPartyByName(imagicaTicketModel.getTicketDetail(), "Child").getTdproductID()));
            try {
                if (jsonObject.has(stringStringHashMap.get(position + "Child"))) {
                    jsonObject.remove(stringStringHashMap.get(position + "Child"));
                    jsonObject.put(stringStringHashMap.get(position + "Child"), holder.tvchildquantityn.getText().toString());
                } else {
                    jsonObject.put(stringStringHashMap.get(position + "Child"), holder.tvchildquantityn.getText().toString());
                }
                if (totalAmount.containsKey(String.valueOf(position + "Child"))) {
                    totalAmount.remove(String.valueOf(position + "Child"));
                    totalAmount.put(String.valueOf(position + "Child"), holder.tvAdultpercost.getText().toString() + "," + holder.tvAdultquantityn.getText().toString());
                } else {
                    totalAmount.put(String.valueOf(position + "Child"), holder.tvAdultpercost.getText().toString() + "," + holder.tvAdultquantityn.getText().toString());
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        if (getPartyByName1(imagicaTicketModel, "College") != null) {
            holder.llCollege.setVisibility(View.VISIBLE);
            holder.tvCollegepercost.setText(String.valueOf(getPartyByName(imagicaTicketModel.getTicketDetail(), "College").getTdcostPerQty()));
            holder.tvCollegequantityn.setText(String.valueOf(getPartyByName(imagicaTicketModel.getTicketDetail(), "College").getTdquantity()));
            stringStringHashMap.put(position + "College", String.valueOf(getPartyByName(imagicaTicketModel.getTicketDetail(), "College").getTdproductID()));

            try {
                if (jsonObject.has(stringStringHashMap.get(position + "College"))) {
                    jsonObject.remove(stringStringHashMap.get(position + "College"));
                    jsonObject.put(stringStringHashMap.get(position + "College"), holder.tvCollegequantityn.getText().toString());
                } else {
                    jsonObject.put(stringStringHashMap.get(position + "College"), holder.tvCollegequantityn.getText().toString());
                }
                if (totalAmount.containsKey(String.valueOf(position + "College"))) {
                    totalAmount.remove(String.valueOf(position + "College"));
                    totalAmount.put(String.valueOf(position + "College"), holder.tvAdultpercost.getText().toString() + "," + holder.tvAdultquantityn.getText().toString());
                } else {
                    totalAmount.put(String.valueOf(position + "College"), holder.tvAdultpercost.getText().toString() + "," + holder.tvAdultquantityn.getText().toString());
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
        if (getPartyByName1(imagicaTicketModel, "College") == null) {
            holder.llCollege.setVisibility(View.GONE);

        }
        if (getPartyByName1(imagicaTicketModel, "Child") == null) {
            holder.llChild.setVisibility(View.GONE);
        }
        if (getPartyByName1(imagicaTicketModel, "Senior Citizen") != null) {
            holder.tvScrpercost.setText(String.valueOf(getPartyByName(imagicaTicketModel.getTicketDetail(), "Senior Citizen").getTdcostPerQty()));
            holder.tvScrquantityn.setText(String.valueOf(getPartyByName(imagicaTicketModel.getTicketDetail(), "Senior Citizen").getTdquantity()));
            stringStringHashMap.put(position + "Senior Citizen", String.valueOf(getPartyByName(imagicaTicketModel.getTicketDetail(), "Senior Citizen").getTdproductID()));
            try {
                if (jsonObject.has(stringStringHashMap.get(position + "Senior Citizen"))) {
                    jsonObject.remove(stringStringHashMap.get(position + "Senior Citizen"));
                    jsonObject.put(stringStringHashMap.get(position + "Senior Citizen"), holder.tvScrquantityn.getText().toString());
                } else {
                    jsonObject.put(stringStringHashMap.get(position + "Senior Citizen"), holder.tvScrquantityn.getText().toString());
                }
                if (totalAmount.containsKey(String.valueOf(position + "Senior Citizen"))) {
                    totalAmount.remove(String.valueOf(position + "Senior Citizen"));
                    totalAmount.put(String.valueOf(position + "Senior Citizen"), holder.tvAdultpercost.getText().toString() + "," + holder.tvAdultquantityn.getText().toString());
                } else {
                    totalAmount.put(String.valueOf(position + "Senior Citizen"), holder.tvAdultpercost.getText().toString() + "," + holder.tvAdultquantityn.getText().toString());
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


        } else if (getPartyByName1(imagicaTicketModel, "Senior Citizen") == null) {
            holder.llSenior.setVisibility(View.GONE);
        }
        if (getPartyByName1(imagicaTicketModel, "Adult / Child / Sr. Citizen") != null) {
            holder.llAdultChild.setVisibility(View.VISIBLE);
            holder.tvAdultchildpercost.setText(String.valueOf(getPartyByName(SubjectValue.get(position).getTicketDetail(), "Adult / Child / Sr. Citizen").getTdcostPerQty()));
            holder.tvAdultchildType.setText("Adult / Child / Sr. Citizen");
            if (position == 0) {
                holder.tvAdultChildquantityn.setText(String.valueOf(getPartyByName(imagicaTicketModel.getTicketDetail(), "Adult / Child / Sr. Citizen").getTdquantity()));
            }
            stringStringHashMap.put(position + "Adult / Child / Sr. Citizen", String.valueOf(getPartyByName(imagicaTicketModel.getTicketDetail(), "Adult / Child / Sr. Citizen").getTdproductID()));
            try {
                if (jsonObject.has(stringStringHashMap.get(position + "Adult / Child / Sr. Citizen"))) {
                    jsonObject.remove(stringStringHashMap.get(position + "Adult / Child / Sr. Citizen"));
                    jsonObject.put(stringStringHashMap.get(position + "Adult / Child / Sr. Citizen"), holder.tvAdultChildquantityn.getText().toString());
                } else {
                    jsonObject.put(stringStringHashMap.get(position + "Adult / Child / Sr. Citizen"), holder.tvAdultChildquantityn.getText().toString());
                }
                if (totalAmount.containsKey(String.valueOf(position + "Adult / Child / Sr. Citizen"))) {
                    totalAmount.remove(String.valueOf(position + "Adult / Child / Sr. Citizen"));
                    totalAmount.put(String.valueOf(position + "Adult / Child / Sr. Citizen"), holder.tvAdultpercost.getText().toString() + "," + holder.tvAdultquantityn.getText().toString());
                } else {
                    totalAmount.put(String.valueOf(position + "Adult / Child / Sr. Citizen"), holder.tvAdultpercost.getText().toString() + "," + holder.tvAdultquantityn.getText().toString());
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (getPartyByName1(imagicaTicketModel, "Adult / Child / Sr. Citizen") == null) {
            holder.llAdultChild.setVisibility(View.GONE);
        }
        holder.Ttitle.setText(SubjectValue.get(position).getTtitle());
        holder.Tdescription.setText(Html.fromHtml(SubjectValue.get(position).getTdescription()));
        Picasso.with(context).load(SubjectValue.get(position).getTimageURL()).into(holder.ivimage);

        holder.Adultbtndown.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                String val = String.valueOf(Integer.parseInt(holder.tvAdultquantityn.getText().toString()) + 1);
                holder.tvAdultquantityn.setText(val);
                double college = Double.parseDouble(holder.tvCollegepercost.getText().toString()) * Double.parseDouble(holder.tvCollegequantityn.getText().toString());
                double adultchildAmount = Double.parseDouble(holder.tvAdultchildpercost.getText().toString()) * Double.parseDouble(holder.tvAdultChildquantityn.getText().toString());
                double adultAmount = Double.parseDouble(holder.tvAdultpercost.getText().toString()) * Double.parseDouble(holder.tvAdultquantityn.getText().toString());
                double childAmount = Double.parseDouble(holder.tvchildpercost.getText().toString()) * Double.parseDouble(holder.tvchildquantityn.getText().toString());
                double seniorAmount = Double.parseDouble(holder.tvScrpercost.getText().toString()) * Double.parseDouble(holder.tvScrquantityn.getText().toString());
                holder.tvtotalcost.setText(String.valueOf(adultchildAmount + adultAmount + childAmount + seniorAmount + college));
                try {
                    if (jsonObject.has(stringStringHashMap.get(position + "Adult"))) {
                        jsonObject.remove(stringStringHashMap.get(position + "Adult"));
                        jsonObject.put(stringStringHashMap.get(position + "Adult"), holder.tvAdultquantityn.getText().toString());
                    } else {
                        jsonObject.put(stringStringHashMap.get(position + "Adult"), holder.tvAdultquantityn.getText().toString());
                    }
                    if (totalAmount.containsKey(String.valueOf(position + "Adult"))) {
                        totalAmount.remove(String.valueOf(position + "Adult"));
                        totalAmount.put(String.valueOf(position + "Adult"), holder.tvAdultpercost.getText().toString() + "," + holder.tvAdultquantityn.getText().toString());
                    } else {
                        totalAmount.put(String.valueOf(position + "Adult"), holder.tvAdultpercost.getText().toString() + "," + holder.tvAdultquantityn.getText().toString());
                    }
                    sendRefresh(holder.tvtotalcost.getText().toString(), jsonObject, totalAmount);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        });

        holder.btnCollegedown.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                String val = String.valueOf(Integer.parseInt(holder.tvCollegequantityn.getText().toString()) + 1);
                holder.tvCollegequantityn.setText(val);
                double college = Double.parseDouble(holder.tvCollegepercost.getText().toString()) * Double.parseDouble(holder.tvCollegequantityn.getText().toString());
                double adultchildAmount = Double.parseDouble(holder.tvAdultchildpercost.getText().toString()) * Double.parseDouble(holder.tvAdultChildquantityn.getText().toString());
                double adultAmount = Double.parseDouble(holder.tvAdultpercost.getText().toString()) * Double.parseDouble(holder.tvAdultquantityn.getText().toString());
                double childAmount = Double.parseDouble(holder.tvchildpercost.getText().toString()) * Double.parseDouble(holder.tvchildquantityn.getText().toString());
                double seniorAmount = Double.parseDouble(holder.tvScrpercost.getText().toString()) * Double.parseDouble(holder.tvScrquantityn.getText().toString());
                holder.tvtotalcost.setText(String.valueOf(adultchildAmount + adultAmount + childAmount + seniorAmount + college));
                try {
                    if (jsonObject.has(stringStringHashMap.get(position + "College"))) {
                        jsonObject.remove(stringStringHashMap.get(position + "College"));
                        jsonObject.put(stringStringHashMap.get(position + "College"), holder.tvCollegequantityn.getText().toString());
                    } else {
                        jsonObject.put(stringStringHashMap.get(position + "College"), holder.tvCollegequantityn.getText().toString());
                    }
                    if (totalAmount.containsKey(String.valueOf(position + "College"))) {
                        totalAmount.remove(String.valueOf(position + "College"));
                        totalAmount.put(String.valueOf(position + "College"), holder.tvCollegepercost.getText().toString() + "," + holder.tvCollegequantityn.getText().toString());
                    } else {
                        totalAmount.put(String.valueOf(position + "College"), holder.tvCollegepercost.getText().toString() + "," + holder.tvCollegequantityn.getText().toString());
                    }
                    Log.i("jsonValue", jsonObject.toString());
                    sendRefresh(holder.tvtotalcost.getText().toString(), jsonObject, totalAmount);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        });


        holder.btnCollege.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (Integer.parseInt(holder.tvCollegequantityn.getText().toString()) > 0) {
                    String val = String.valueOf(Integer.parseInt(holder.tvCollegequantityn.getText().toString()) - 1);
                    holder.tvCollegequantityn.setText(val);
                    Log.i("Product ID", stringStringHashMap.toString());
                    double college = Double.parseDouble(holder.tvCollegepercost.getText().toString()) * Double.parseDouble(holder.tvCollegequantityn.getText().toString());
                    double adultchildAmount = Double.parseDouble(holder.tvAdultchildpercost.getText().toString()) * Double.parseDouble(holder.tvAdultChildquantityn.getText().toString());
                    double adultAmount = Double.parseDouble(holder.tvAdultpercost.getText().toString()) * Double.parseDouble(holder.tvAdultquantityn.getText().toString());
                    double childAmount = Double.parseDouble(holder.tvchildpercost.getText().toString()) * Double.parseDouble(holder.tvchildquantityn.getText().toString());
                    double seniorAmount = Double.parseDouble(holder.tvScrpercost.getText().toString()) * Double.parseDouble(holder.tvScrquantityn.getText().toString());
                    holder.tvtotalcost.setText(String.valueOf(adultchildAmount + college + adultAmount + childAmount + seniorAmount));
                    valueEnableAmount = false;
                    Log.i("Postion", String.valueOf(position));

                    try {
                        if (jsonObject.has(stringStringHashMap.get(position + "College"))) {
                            jsonObject.remove(stringStringHashMap.get(position + "College"));
                            jsonObject.put(stringStringHashMap.get(position + "College"), holder.tvCollegequantityn.getText().toString());
                        } else {
                            jsonObject.put(stringStringHashMap.get(position + "College"), holder.tvCollegequantityn.getText().toString());
                        }
                        if (totalAmount.containsKey(String.valueOf(position + "College"))) {
                            totalAmount.remove(String.valueOf(position + "College"));
                            totalAmount.put(String.valueOf(position + "College"), holder.tvCollegepercost.getText().toString() + "," + holder.tvCollegequantityn.getText().toString());
                        } else {
                            totalAmount.put(String.valueOf(position + "College"), holder.tvCollegepercost.getText().toString() + "," + holder.tvCollegequantityn.getText().toString());
                        }
                        if (holder.tvCollegequantityn.getText().equals("0")) {
                            jsonObject.remove(stringStringHashMap.get(position + "College"));
                            sendRefresh(holder.tvtotalcost.getText().toString(), jsonObject, totalAmount);
                        } else {
                            sendRefresh(holder.tvtotalcost.getText().toString(), jsonObject, totalAmount);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

            }
        });

        holder.AdultChildbtndown.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                String val = String.valueOf(Integer.parseInt(holder.tvAdultChildquantityn.getText().toString()) + 1);
                holder.tvAdultChildquantityn.setText(val);
                double college = Double.parseDouble(holder.tvCollegepercost.getText().toString()) * Double.parseDouble(holder.tvCollegequantityn.getText().toString());
                double adultchildAmount = Double.parseDouble(holder.tvAdultchildpercost.getText().toString()) * Double.parseDouble(holder.tvAdultChildquantityn.getText().toString());
                double adultAmount = Double.parseDouble(holder.tvAdultpercost.getText().toString()) * Double.parseDouble(holder.tvAdultquantityn.getText().toString());
                double childAmount = Double.parseDouble(holder.tvchildpercost.getText().toString()) * Double.parseDouble(holder.tvchildquantityn.getText().toString());
                double seniorAmount = Double.parseDouble(holder.tvScrpercost.getText().toString()) * Double.parseDouble(holder.tvScrquantityn.getText().toString());
                holder.tvtotalcost.setText(String.valueOf(adultchildAmount + adultAmount + college + childAmount + seniorAmount));
                try {
                    if (jsonObject.has(stringStringHashMap.get(position + "Adult / Child / Sr. Citizen"))) {
                        jsonObject.remove(stringStringHashMap.get(position + "Adult / Child / Sr. Citizen"));
                        jsonObject.put(stringStringHashMap.get(position + "Adult / Child / Sr. Citizen"), holder.tvAdultChildquantityn.getText().toString());
                    } else {
                        jsonObject.put(stringStringHashMap.get(position + "Adult / Child / Sr. Citizen"), holder.tvAdultChildquantityn.getText().toString());
                    }
                    if (totalAmount.containsKey(String.valueOf(position + "Adult / Child / Sr. Citizen"))) {
                        totalAmount.remove(String.valueOf(position + "Adult / Child / Sr. Citizen"));
                        totalAmount.put(String.valueOf(position + "Adult / Child / Sr. Citizen"), holder.tvAdultchildpercost.getText().toString() + "," + holder.tvAdultChildquantityn.getText().toString());
                    } else {
                        totalAmount.put(String.valueOf(position + "Adult / Child / Sr. Citizen"), holder.tvScrpercost.getText().toString() + "," + holder.tvScrquantityn.getText().toString());
                    }
                    Log.i("jsonValue", jsonObject.toString());
                    sendRefresh(holder.tvtotalcost.getText().toString(), jsonObject, totalAmount);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        });


        holder.btnAdultChildup.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (Integer.parseInt(holder.tvAdultChildquantityn.getText().toString()) > 0) {
                    String val = String.valueOf(Integer.parseInt(holder.tvAdultChildquantityn.getText().toString()) - 1);
                    holder.tvAdultChildquantityn.setText(val);
                    Log.i("Product ID", stringStringHashMap.toString());
                    double college = Double.parseDouble(holder.tvCollegepercost.getText().toString()) * Double.parseDouble(holder.tvCollegequantityn.getText().toString());
                    double adultchildAmount = Double.parseDouble(holder.tvAdultchildpercost.getText().toString()) * Double.parseDouble(holder.tvAdultChildquantityn.getText().toString());
                    double adultAmount = Double.parseDouble(holder.tvAdultpercost.getText().toString()) * Double.parseDouble(holder.tvAdultquantityn.getText().toString());
                    double childAmount = Double.parseDouble(holder.tvchildpercost.getText().toString()) * Double.parseDouble(holder.tvchildquantityn.getText().toString());
                    double seniorAmount = Double.parseDouble(holder.tvScrpercost.getText().toString()) * Double.parseDouble(holder.tvScrquantityn.getText().toString());
                    holder.tvtotalcost.setText(String.valueOf(adultchildAmount + adultAmount + childAmount + college + seniorAmount));
                    valueEnableAmount = false;
                    Log.i("Postion", String.valueOf(position));

                    try {
                        if (jsonObject.has(stringStringHashMap.get(position + "Adult / Child / Sr. Citizen"))) {
                            jsonObject.remove(stringStringHashMap.get(position + "Adult / Child / Sr. Citizen"));
                            jsonObject.put(stringStringHashMap.get(position + "Adult / Child / Sr. Citizen"), holder.tvAdultChildquantityn.getText().toString());
                        } else {
                            jsonObject.put(stringStringHashMap.get(position + "Adult / Child / Sr. Citizen"), holder.tvAdultChildquantityn.getText().toString());
                        }
                        if (totalAmount.containsKey(String.valueOf(position + "Adult / Child / Sr. Citizen"))) {
                            totalAmount.remove(String.valueOf(position + "Adult / Child / Sr. Citizen"));
                            totalAmount.put(String.valueOf(position + "Adult / Child / Sr. Citizen"), holder.tvAdultchildpercost.getText().toString() + "," + holder.tvAdultChildquantityn.getText().toString());
                        } else {
                            totalAmount.put(String.valueOf(position + "Adult / Child / Sr. Citizen"), holder.tvScrpercost.getText().toString() + "," + holder.tvScrquantityn.getText().toString());
                        }
                        if (holder.tvAdultChildquantityn.getText().equals("0")) {
                            jsonObject.remove(stringStringHashMap.get(position + "Adult / Child / Sr. Citizen"));
                            sendRefresh(holder.tvtotalcost.getText().toString(), jsonObject, totalAmount);
                        } else {
                            sendRefresh(holder.tvtotalcost.getText().toString(), jsonObject, totalAmount);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        });
        holder.btnAdultup.setOnClickListener(new View.OnClickListener()

        {

            @Override
            public void onClick(View v) {
                if (Integer.parseInt(holder.tvAdultquantityn.getText().toString()) > 0) {
                    String val = String.valueOf(Integer.parseInt(holder.tvAdultquantityn.getText().toString()) - 1);
                    holder.tvAdultquantityn.setText(val);
                    Log.i("Product ID", stringStringHashMap.toString());
                    double college = Double.parseDouble(holder.tvCollegepercost.getText().toString()) * Double.parseDouble(holder.tvCollegequantityn.getText().toString());
                    double adultchildAmount = Double.parseDouble(holder.tvAdultchildpercost.getText().toString()) * Double.parseDouble(holder.tvAdultChildquantityn.getText().toString());
                    double adultAmount = Double.parseDouble(holder.tvAdultpercost.getText().toString()) * Double.parseDouble(holder.tvAdultquantityn.getText().toString());
                    double childAmount = Double.parseDouble(holder.tvchildpercost.getText().toString()) * Double.parseDouble(holder.tvchildquantityn.getText().toString());
                    double seniorAmount = Double.parseDouble(holder.tvScrpercost.getText().toString()) * Double.parseDouble(holder.tvScrquantityn.getText().toString());
                    holder.tvtotalcost.setText(String.valueOf(adultchildAmount + adultAmount + childAmount + college + seniorAmount));
                    valueEnableAmount = false;
                    Log.i("Postion", String.valueOf(position));
                    try {
                        if (jsonObject.has(stringStringHashMap.get(position + "Adult"))) {
                            jsonObject.remove(stringStringHashMap.get(position + "Adult"));
                            jsonObject.put(stringStringHashMap.get(position + "Adult"), holder.tvAdultquantityn.getText().toString());
                        } else {
                            jsonObject.put(stringStringHashMap.get(position + "Adult"), holder.tvAdultquantityn.getText().toString());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (totalAmount.containsKey(String.valueOf(position + "Adult"))) {
                        totalAmount.remove(String.valueOf(position + "Adult"));
                        totalAmount.put(String.valueOf(position + "Adult"), holder.tvAdultpercost.getText().toString() + "," + holder.tvAdultquantityn.getText().toString());
                    } else {
                        totalAmount.put(String.valueOf(position + "Adult"), holder.tvScrpercost.getText().toString() + "," + holder.tvScrquantityn.getText().toString());
                    }

                    if (holder.tvAdultquantityn.getText().equals("0")) {
                        jsonObject.remove(stringStringHashMap.get(position + "Adult"));
                        sendRefresh(holder.tvtotalcost.getText().toString(), jsonObject, totalAmount);
                    } else {
                        sendRefresh(holder.tvtotalcost.getText().toString(), jsonObject, totalAmount);
                    }

                }
            }
        });

        holder.btnchilddown.setOnClickListener(new View.OnClickListener()

        {

            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                String val = String.valueOf(Integer.parseInt(holder.tvchildquantityn.getText().toString()) + 1);
                holder.tvchildquantityn.setText(val);
                valueEnableAmount = true;
                Log.i("Product ID", stringStringHashMap.toString());
                double college = Double.parseDouble(holder.tvCollegepercost.getText().toString()) * Double.parseDouble(holder.tvCollegequantityn.getText().toString());
                double adultchildAmount = Double.parseDouble(holder.tvAdultchildpercost.getText().toString()) * Double.parseDouble(holder.tvAdultChildquantityn.getText().toString());
                double adultAmount = Double.parseDouble(holder.tvAdultpercost.getText().toString()) * Double.parseDouble(holder.tvAdultquantityn.getText().toString());
                double childAmount = Double.parseDouble(holder.tvchildpercost.getText().toString()) * Double.parseDouble(holder.tvchildquantityn.getText().toString());
                double seniorAmount = Double.parseDouble(holder.tvScrpercost.getText().toString()) * Double.parseDouble(holder.tvScrquantityn.getText().toString());
                holder.tvtotalcost.setText(String.valueOf(adultchildAmount + adultAmount + childAmount + college + seniorAmount));
                stringStringHashMap.put(String.valueOf(position) + "Child", String.valueOf(getPartyByName(imagicaTicketModel.getTicketDetail(), "Child").getTdproductID()));

                try {
                    if (jsonObject.has(stringStringHashMap.get(position + "Child"))) {
                        jsonObject.remove(stringStringHashMap.get(position + "Child"));
                        jsonObject.put(stringStringHashMap.get(position + "Child"), holder.tvchildquantityn.getText().toString());
                    } else {
                        jsonObject.put(stringStringHashMap.get(position + "Child"), holder.tvchildquantityn.getText().toString());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (totalAmount.containsKey(String.valueOf(position + "Child"))) {
                    totalAmount.remove(String.valueOf(position + "Child"));
                    totalAmount.put(String.valueOf(position + "Child"), holder.tvchildpercost.getText().toString() + "," + holder.tvchildquantityn.getText().toString());
                } else {
                    totalAmount.put(String.valueOf(position + "Child"), holder.tvScrpercost.getText().toString() + "," + holder.tvScrquantityn.getText().toString());
                }
                sendRefresh(holder.tvtotalcost.getText().toString(), jsonObject, totalAmount);


            }
        });

        holder.btnchildup.setOnClickListener(new View.OnClickListener()

        {

            @Override
            public void onClick(View v) {

                if (!(holder.tvchildquantityn == null) && !(holder.tvchildquantityn.getText().equals("0"))) {

                    String val = String.valueOf(Integer.parseInt(holder.tvchildquantityn.getText().toString()) - 1);
                    holder.tvchildquantityn.setText(val);
                    valueEnableAmount = false;
                    Log.i("Product ID", stringStringHashMap.toString());
                    double college = Double.parseDouble(holder.tvCollegepercost.getText().toString()) * Double.parseDouble(holder.tvCollegequantityn.getText().toString());
                    double adultchildAmount = Double.parseDouble(holder.tvAdultchildpercost.getText().toString()) * Double.parseDouble(holder.tvAdultChildquantityn.getText().toString());
                    double adultAmount = Double.parseDouble(holder.tvAdultpercost.getText().toString()) * Double.parseDouble(holder.tvAdultquantityn.getText().toString());
                    double childAmount = Double.parseDouble(holder.tvchildpercost.getText().toString()) * Double.parseDouble(holder.tvchildquantityn.getText().toString());
                    double seniorAmount = Double.parseDouble(holder.tvScrpercost.getText().toString()) * Double.parseDouble(holder.tvScrquantityn.getText().toString());
                    holder.tvtotalcost.setText(String.valueOf(adultchildAmount + adultAmount + childAmount + college + seniorAmount));
                    try {
                        if (jsonObject.has(stringStringHashMap.get(position + "Child"))) {
                            jsonObject.remove(stringStringHashMap.get(position + "Child"));
                            jsonObject.put(stringStringHashMap.get(position + "Child"), holder.tvchildquantityn.getText().toString());
                        } else {
                            jsonObject.put(stringStringHashMap.get(position + "Child"), holder.tvchildquantityn.getText().toString());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (totalAmount.containsKey(String.valueOf(position + "Child"))) {
                        totalAmount.remove(String.valueOf(position + "Child"));
                        totalAmount.put(String.valueOf(position + "Child"), holder.tvchildpercost.getText().toString() + "," + holder.tvchildquantityn.getText().toString());
                    } else {
                        totalAmount.put(String.valueOf(position + "Child"), holder.tvScrpercost.getText().toString() + "," + holder.tvScrquantityn.getText().toString());
                    }
                    if (holder.tvchildquantityn.getText().equals("0")) {
                        jsonObject.remove(stringStringHashMap.get(position + "Child"));
                        sendRefresh(holder.tvtotalcost.getText().toString(), jsonObject, totalAmount);
                    } else {
                        sendRefresh(holder.tvtotalcost.getText().toString(), jsonObject, totalAmount);
                    }

                }
            }
        });

        holder.btnScrdown.setOnClickListener(new View.OnClickListener()

        {

            @Override
            public void onClick(View v) {

                String val = String.valueOf(Integer.parseInt(holder.tvScrquantityn.getText().toString()) + 1);
                holder.tvScrquantityn.setText(val);
                valueEnableAmount = true;
                double college = Double.parseDouble(holder.tvCollegepercost.getText().toString()) * Double.parseDouble(holder.tvCollegequantityn.getText().toString());
                double adultchildAmount = Double.parseDouble(holder.tvAdultchildpercost.getText().toString()) * Double.parseDouble(holder.tvAdultChildquantityn.getText().toString());
                double adultAmount = Double.parseDouble(holder.tvAdultpercost.getText().toString()) * Double.parseDouble(holder.tvAdultquantityn.getText().toString());
                double childAmount = Double.parseDouble(holder.tvchildpercost.getText().toString()) * Double.parseDouble(holder.tvchildquantityn.getText().toString());
                double seniorAmount = Double.parseDouble(holder.tvScrpercost.getText().toString()) * Double.parseDouble(holder.tvScrquantityn.getText().toString());
                holder.tvtotalcost.setText(String.valueOf(adultchildAmount + adultAmount + childAmount + college + seniorAmount));

                try {
                    if (jsonObject.has(stringStringHashMap.get(position + "Senior Citizen"))) {
                        jsonObject.remove(stringStringHashMap.get(position + "Senior Citizen"));
                        jsonObject.put(stringStringHashMap.get(position + "Senior Citizen"), holder.tvScrquantityn.getText().toString());
                    } else {
                        jsonObject.put(stringStringHashMap.get(position + "Senior Citizen"), holder.tvScrquantityn.getText().toString());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (totalAmount.containsKey(String.valueOf(position + "Senior Citizen"))) {
                    totalAmount.remove(String.valueOf(position + "Senior Citizen"));
                    totalAmount.put(String.valueOf(position + "Senior Citizen"), holder.tvScrpercost.getText().toString() + "," + holder.tvScrquantityn.getText().toString());
                } else {
                    totalAmount.put(String.valueOf(position + "Senior Citizen"), holder.tvScrpercost.getText().toString() + "," + holder.tvScrquantityn.getText().toString());
                }
                sendRefresh(holder.tvtotalcost.getText().toString(), jsonObject, totalAmount);

            }
        });

        holder.btnScrup.setOnClickListener(new View.OnClickListener()

        {

            @Override
            public void onClick(View v) {
                if (!(holder.tvScrquantityn == null) && !(holder.tvScrquantityn.getText().equals("0"))) {

                    String val = String.valueOf(Integer.parseInt(holder.tvScrquantityn.getText().toString()) - 1);
                    holder.tvScrquantityn.setText(val);
                    valueEnableAmount = false;
                    Log.i("Product ID", stringStringHashMap.toString());
                    double college = Double.parseDouble(holder.tvCollegepercost.getText().toString()) * Double.parseDouble(holder.tvCollegequantityn.getText().toString());
                    double adultchildAmount = Double.parseDouble(holder.tvAdultchildpercost.getText().toString()) * Double.parseDouble(holder.tvAdultChildquantityn.getText().toString());
                    double adultAmount = Double.parseDouble(holder.tvAdultpercost.getText().toString()) * Double.parseDouble(holder.tvAdultquantityn.getText().toString());
                    double childAmount = Double.parseDouble(holder.tvchildpercost.getText().toString()) * Double.parseDouble(holder.tvchildquantityn.getText().toString());
                    double seniorAmount = Double.parseDouble(holder.tvScrpercost.getText().toString()) * Double.parseDouble(holder.tvScrquantityn.getText().toString());
                    holder.tvtotalcost.setText(String.valueOf(adultchildAmount + adultAmount + college + childAmount + seniorAmount));
                    Log.i("Product ID", stringStringHashMap.get(position + "Adult") + holder.tvScrquantityn.getText().toString());
                    Log.i("Child ID", stringStringHashMap.get(position + "Child") + holder.tvAdultquantityn.getText().toString());
                    Log.i("seinor ID", stringStringHashMap.get(position + "seinor") + holder.tvAdultquantityn.getText().toString());
                    Log.i("Postion", String.valueOf(position));

                    try {
                        if (jsonObject.has(stringStringHashMap.get(position + "Senior Citizen"))) {
                            jsonObject.remove(stringStringHashMap.get(position + "Senior Citizen"));
                            jsonObject.put(stringStringHashMap.get(position + "Senior Citizen"), holder.tvScrquantityn.getText().toString());
                        } else {
                            jsonObject.put(stringStringHashMap.get(position + "Senior Citizen"), holder.tvScrquantityn.getText().toString());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (totalAmount.containsKey(String.valueOf(position + "Senior Citizen"))) {
                        totalAmount.remove(String.valueOf(position + "Senior Citizen"));
                        totalAmount.put(String.valueOf(position + "Senior Citizen"), holder.tvScrpercost.getText().toString() + "," + holder.tvScrquantityn.getText().toString());
                    } else {
                        totalAmount.put(String.valueOf(position + "Senior Citizen"), holder.tvScrpercost.getText().toString() + "," + holder.tvScrquantityn.getText().toString());
                    }

                    if (holder.tvScrquantityn.getText().equals("0")) {
                        jsonObject.remove(stringStringHashMap.get(position + "Senior Citizen"));
                        sendRefresh(holder.tvtotalcost.getText().toString(), jsonObject, totalAmount);
                    } else {
                        sendRefresh(holder.tvtotalcost.getText().toString(), jsonObject, totalAmount);
                    }
                }
            }
        });

        double adultchildAmount = Double.parseDouble(holder.tvAdultchildpercost.getText().toString()) * Double.parseDouble(holder.tvAdultChildquantityn.getText().toString());
        double adultAmount = Double.parseDouble(holder.tvAdultpercost.getText().toString()) * Double.parseDouble(holder.tvAdultquantityn.getText().toString());
        double childAmount = Double.parseDouble(holder.tvchildpercost.getText().toString()) * Double.parseDouble(holder.tvchildquantityn.getText().toString());
        double seniorAmount = Double.parseDouble(holder.tvScrpercost.getText().toString()) * Double.parseDouble(holder.tvScrquantityn.getText().toString());
        double college = Double.parseDouble(holder.tvCollegepercost.getText().toString()) * Double.parseDouble(holder.tvCollegequantityn.getText().toString());
        holder.tvtotalcost.setText(String.valueOf(adultchildAmount + college + adultAmount + college + childAmount + seniorAmount));


    }

    private String getPartyByName1(ImagicaTicketModel ticketDetail, String adult) {
        for (TicketDetail party : ticketDetail.getTicketDetail()) {
            if (adult.equalsIgnoreCase(party.getTdtype())) {
                return party.getTdtype();
            }
        }
        return null;
    }

    @Override
    public int getItemCount() {
        return SubjectValue.size();
    }


    public TicketDetail getPartyByName(List<TicketDetail> parties, String name) {

        for (TicketDetail party : parties) {
            if (name.equalsIgnoreCase(party.getTdtype())) {
                return party;
            }
        }
        return null;
    }

    private void sendRefresh(String amount, JSONObject jsonObject, HashMap<String, String> totalAmount) {
        Log.i("Amount", totalAmount.toString());
        ArrayList<String> strings = new ArrayList<>();
        for (Object o : totalAmount.keySet()) {
            String[] value = totalAmount.get(o.toString()).split(",");
            double value2 = Double.parseDouble(value[0]) * Double.parseDouble(value[1]);
            strings.add(String.valueOf(value2));
        }
        JSONObject products = new JSONObject();
        Iterator<String> iter = jsonObject.keys();
        while (iter.hasNext()) {
            String key = iter.next();
            try {
                Object value = jsonObject.get(key);
                if (!value.equals("0")) {
                    products.put(key, value);
                }
            } catch (JSONException e) {

            }
        }

        if (strings.size() != 0) {
            Intent intent = new Intent("Packages");
            intent.putExtra("updates", "1");
            intent.putExtra("type", "ticket");
            Log.i("product", products.toString());
            intent.putExtra("ticketProduct", products.toString());
            intent.putExtra("ticketAmount", String.valueOf(totalValue(strings)));
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
        }

    }

    private double totalValue(ArrayList<String> strings) {
        double sum = 0;
        for (int i = 0; i < strings.size(); i++) {
            sum += Double.parseDouble(strings.get(i));
        }
        return sum;
    }

}
