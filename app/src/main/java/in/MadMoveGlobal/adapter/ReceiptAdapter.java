package in.MadMoveGlobal.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import in.MadMoveGlobal.EwireRuPay.ExtendedReceiptActivity;
import in.MadMoveGlobal.custom.CustomAlertDialog;
import in.MadMoveGlobal.custom.CustomAlertDialogsReDetails;
import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.fragment.LoadMoneyFragment;
import in.MadMoveGlobal.model.StatementModel;
import in.MadMoveGlobal.EwireRuPay.R;

/**
 * Created by Dushant on 07/27/2017.
 */
public class ReceiptAdapter extends BaseAdapter {

    private ViewHolder viewHolder;
    private Context context;
    private ArrayList<StatementModel> statementList;
    String amount, status, indicator, dates, message;

    public ReceiptAdapter(Context context, ArrayList<StatementModel> statementList) {
        this.context = context;
        this.statementList = statementList;
    }

    @Override
    public int getCount() {
        return statementList.size();
    }

    @Override
    public Object getItem(int position) {
        return statementList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parentView) {
        final int currentPosition = position;
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.adapter_receipt_new, null);
            viewHolder = new ViewHolder();
            viewHolder.llAdptrItem = (LinearLayout) convertView.findViewById(R.id.llAdptrItem);
            viewHolder.tvReceiptDate = (TextView) convertView.findViewById(R.id.tvReceiptDate);
            viewHolder.tvReceiptCurrentBalance = (TextView) convertView.findViewById(R.id.tvReceiptCurrentBalance);
            viewHolder.tvReceiptAmount = (TextView) convertView.findViewById(R.id.tvReceiptAmount);
            viewHolder.tvReceiptDetails = (TextView) convertView.findViewById(R.id.tvReceiptDetails);
            viewHolder.tvReceiptStatus = (TextView) convertView.findViewById(R.id.tvReceiptStatus);
            viewHolder.tvReceiptTnxId = (TextView) convertView.findViewById(R.id.tvReceiptTnxId);

            viewHolder.tvReceiptAuthRef = (TextView) convertView.findViewById(R.id.tvReceiptAuthRef);
            viewHolder.tvReceiptReteriveRef = (TextView) convertView.findViewById(R.id.tvReceiptReteriveRef);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.tvReceiptDetails.setText(statementList.get(position).getDescription());
        Log.i("GHGHGH", statementList.get(position).getDescription());
        message = statementList.get(position).getDescription();
//        viewHolder.tvReceiptTnxId.setText("Ref no. "+statementList.get(position).getRefNo());
//        viewHolder.tvReceiptCurrentBalance.setText("Wallet Balance: "+context.getResources().getString(R.string.rupease)+" "+statementList.get(position).getCurrentBalance());

        //MVISA
//        viewHolder.tvReceiptAuthRef.setText("Auth Ref no. "+statementList.get(position).getAuthRefNo());
//        viewHolder.tvReceiptReteriveRef.setText("Retrieve Ref no. "+statementList.get(position).getRetrievRefNo());


        String[] dateString = statementList.get(position).getDate().split("T");

        viewHolder.tvReceiptDate.setText(dateString[0]);
        dates = dateString[0];

        viewHolder.tvReceiptStatus.setText(statementList.get(position).getStatus());
        if (statementList.get(position).getIndicator().equals("debit")) {

            viewHolder.tvReceiptAmount.setText("-" + context.getResources().getString(R.string.rupease) + statementList.get(position).getAmount());
            amount = statementList.get(position).getAmount();
        }
        if (statementList.get(position).getIndicator().equals("credit")) {

            viewHolder.tvReceiptAmount.setText("+" + context.getResources().getString(R.string.rupease) + statementList.get(position).getAmount());
            amount = statementList.get(position).getAmount();
        }
        status = statementList.get(position).getStatus();
        indicator = statementList.get(position).getIndicator();
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                showCustomDisclaimerDialog(position);

                Intent intent = new Intent(context,ExtendedReceiptActivity.class);

                intent.putExtra("statementModel",statementList.get(position));

                context.startActivity(intent);


            }
        });


        return convertView;
    }

    static class ViewHolder {
        TextView tvReceiptDate, tvReceiptCurrentBalance, tvReceiptAmount, tvReceiptStatus, tvReceiptDetails;
        TextView tvReceiptTnxId, tvReceiptReteriveRef, tvReceiptAuthRef;
        LinearLayout llAdptrItem;
    }

    public void showCustomDisclaimerDialog(int position) {

        StatementModel statementModel = statementList.get(position);


        CustomAlertDialogsReDetails builder = new CustomAlertDialogsReDetails(context, R.string.dialog_title2, Html.fromHtml(generateDetails(statementModel)));


        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });

        builder.show();
    }

    public String generateDetails(StatementModel statementModel) {

        String[] dateStrings = statementModel.getDate().split("T");

        return "<b><font color=#000000>Amount: </font></b>" + "<font color=#000000>" + "â‚¹ " + statementModel.getAmount() + "</font><br>" +
                "<b><font color=#000000>Status: </font></b>" + "<font>" + statementModel.getStatus() + "</font><br>" +
                "<b><font color=#000000>Indicator: </font></b>" + "<font>" + statementModel.getIndicator() + "</font><br>" +
                "<b><font color=#000000>Date: </font></b>" + "<font>" + dateStrings[0] + "</font><br>" +
                "<b><font color=#000000>Message: </font></b>" + "<font>" + statementModel.getDescription() + "</font><br>";
    }


}
