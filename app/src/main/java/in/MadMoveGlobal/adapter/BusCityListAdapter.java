package in.MadMoveGlobal.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.model.BusCityModel;


/**
 * Created by Ksf on 10/3/2016.
 */
public class BusCityListAdapter extends BaseAdapter {

    private Context context;
    private List<BusCityModel> busCityArray;
    private ViewHolder viewHolder;

    public BusCityListAdapter(Context context, List<BusCityModel> busCityArray) {
        this.context = context;
        this.busCityArray = busCityArray;

    }

    @Override
    public int getCount() {
        return busCityArray.size();
    }

    @Override
    public Object getItem(int index) {
        return busCityArray.get(index);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.adapter_bus_city_list, parent,false);
            viewHolder = new ViewHolder();
            viewHolder.tvBusCity = (TextView) convertView.findViewById(R.id.tvBusCity);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.tvBusCity.setText(busCityArray.get(position).getCityname());
        return convertView;
    }
    static class ViewHolder {
        TextView tvBusCity;
    }
}
