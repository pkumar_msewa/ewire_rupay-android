package in.MadMoveGlobal.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.metadata.AppMetadata;
import in.MadMoveGlobal.model.MainMenuModel;
import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.EwireRuPay.activity.MainMenuDetailActivity;

/**
 * Created by Dushant on 07/27/2017.
 */
public class HomeMenuAdapter extends RecyclerView.Adapter<HomeMenuAdapter.RecyclerViewHolders> {

    private ArrayList<MainMenuModel> itemList;
    private Context context;

    public HomeMenuAdapter(Context context, ArrayList<MainMenuModel> itemList) {
        this.itemList = itemList;
        this.context = context;
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_grid_menu, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolders holder, int position) {
        holder.tvMenuItems.setText(itemList.get(position).getMenuTitle());
        holder.ivMenuItems.setImageResource(itemList.get(position).getMenuImage());
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView tvMenuItems;
        public ImageView ivMenuItems;

        public RecyclerViewHolders(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            tvMenuItems = (TextView) itemView.findViewById(R.id.tvMenuItems);
            ivMenuItems = (ImageView) itemView.findViewById(R.id.ivMenuItems);
        }

        @Override
        public void onClick(View view) {
            int i = getAdapterPosition();
            Intent menuIntent = new Intent(context, MainMenuDetailActivity.class);
            if (i == 0) {
                menuIntent.putExtra(AppMetadata.FRAGMENT_TYPE, "MobileTopUp");
                context.startActivity(menuIntent);
            } else if (i == 5) {
                menuIntent.putExtra(AppMetadata.FRAGMENT_TYPE, "DTH");
                context.startActivity(menuIntent);
            } else if (i == 6) {
                menuIntent.putExtra(AppMetadata.FRAGMENT_TYPE, "FundTransferbyMobile");
                context.startActivity(menuIntent);
            } else if (i == 7) {
                menuIntent.putExtra(AppMetadata.FRAGMENT_TYPE, "Cards");
                context.startActivity(menuIntent);
            } else if (i == 8) {
                menuIntent.putExtra(AppMetadata.FRAGMENT_TYPE, "Donation");
                context.startActivity(menuIntent);
            } else if (i == 9) {
                menuIntent.putExtra(AppMetadata.FRAGMENT_TYPE, "Electricity");
                context.startActivity(menuIntent);
            } else if (i == 10) {
                menuIntent.putExtra(AppMetadata.FRAGMENT_TYPE, "Insurance");
                context.startActivity(menuIntent);
            } /*else if (i == 11) {

//                menuIntent.putExtra(AppMetadata.FRAGMENT_TYPE, "BankTransfer");
//                context.startActivity(menuIntent);
                CustomToast.showMessage(context, "Coming Soon...");
            } */else if (i == 11) {
                menuIntent.putExtra(AppMetadata.FRAGMENT_TYPE, "Fingool");
                context.startActivity(menuIntent);
            } else if (i == 12) {
                menuIntent.putExtra(AppMetadata.FRAGMENT_TYPE, "Bus");
                context.startActivity(menuIntent);
            } else if (i == 13) {
                menuIntent.putExtra(AppMetadata.FRAGMENT_TYPE, "Ofer");
                context.startActivity(menuIntent);
//                menuIntent.putExtra(AppMetadata.FRAGMENT_TYPE, "Flight");
//                context.startActivity(menuIntent);
            } else if (itemList.get(i).getMenuTitle().equalsIgnoreCase("data card")) {
                menuIntent.putExtra(AppMetadata.FRAGMENT_TYPE, "datacard");
                context.startActivity(menuIntent);
            } else if (itemList.get(i).getMenuTitle().equalsIgnoreCase("landline")) {
                menuIntent.putExtra(AppMetadata.FRAGMENT_TYPE, "landline");
                context.startActivity(menuIntent);
            } else if (itemList.get(i).getMenuTitle().equalsIgnoreCase("postpaid")) {
                menuIntent.putExtra(AppMetadata.FRAGMENT_TYPE, "postpaid");
                context.startActivity(menuIntent);
            } else if (itemList.get(i).getMenuTitle().equalsIgnoreCase("gas")) {
                menuIntent.putExtra(AppMetadata.FRAGMENT_TYPE, "gas");
                context.startActivity(menuIntent);
            } else if (itemList.get(i).getMenuTitle().equalsIgnoreCase("Vehicle Insurance")){
                menuIntent.putExtra(AppMetadata.FRAGMENT_TYPE,"Vehicle Insurance");
                context.startActivity(menuIntent);
            }else {
                CustomToast.showMessage(context, "Coming shortly");
            }
        }
    }
}
