package in.MadMoveGlobal.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;

import java.util.ArrayList;
import java.util.List;

import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.model.OperatorsModel;


/**
 * Created by Dushant on 07/27/2017.
 */

public class mobileAdapter extends BaseAdapter implements Filterable {

  private Context context;
  private ArrayList<OperatorsModel> serviceoperaterArray;
  private List<OperatorsModel> filteredData;
  private ViewHolder viewHolder;

  public mobileAdapter(Context context, ArrayList<OperatorsModel> serviceoperaterArray) {
    this.context = context;
    this.serviceoperaterArray = serviceoperaterArray;
    this.filteredData = serviceoperaterArray;
  }

  @Override
  public int getCount() {
    if (serviceoperaterArray == null) {
      return 0;
    }
    return serviceoperaterArray.size();
  }

  @Override
  public Object getItem(int index) {
    return serviceoperaterArray.get(index);
  }

  @Override
  public long getItemId(int position) {
    return position;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    if (convertView == null) {
      convertView = LayoutInflater.from(context).inflate(R.layout.adapter_flight_city_list, parent, false);
      viewHolder = new ViewHolder();
      viewHolder.tvTitle = (TextView) convertView.findViewById(R.id.tvFlightCity);
      viewHolder.tvCode = (TextView) convertView.findViewById(R.id.tvFlightCode);
      viewHolder.image = (ImageView) convertView.findViewById(R.id.ivimage);

      viewHolder.tvCode.setVisibility(View.GONE);
      convertView.setTag(viewHolder);
    } else {
      viewHolder = (ViewHolder) convertView.getTag();
    }

    viewHolder.tvTitle.setText(serviceoperaterArray.get(position).getName());

      if (!serviceoperaterArray.get(position).getDefaultCode().isEmpty()) {
          AQuery aq = new AQuery(context);
          aq.id(viewHolder.image).background(R.color.white_text).image(serviceoperaterArray.get(position).getDefaultCode(), true, true);
          viewHolder.image.setVisibility(View.VISIBLE);
      } else {
          viewHolder.image.setVisibility(View.GONE);
      }


    Log.i("VALUES",serviceoperaterArray.get(position).getName());


    return convertView;
  }

  @Override
  public Filter getFilter() {
    return new ItemFilter();
  }


  static class ViewHolder {
    TextView tvTitle;
    TextView tvCode;
    ImageView image;
  }


  private class ItemFilter extends Filter {
    @Override
    protected FilterResults performFiltering(CharSequence constraint) {

      String filterString = constraint.toString().toLowerCase();

      FilterResults results = new FilterResults();

      final List<OperatorsModel> list = filteredData;

      int count = list.size();
      final ArrayList<Object> nlist = new ArrayList<Object>(count);

      String filterableString, filterableStringcode, filterableStringAreo,filterimage;

      for (int i = 0; i < count; i++) {
        filterableString = list.get(i).getName();

        if (filterableString.toLowerCase().contains(filterString)) {
          nlist.add(list.get(i));
        }
      }
      results.values = nlist;
      results.count = nlist.size();
      return results;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
      serviceoperaterArray = (ArrayList<OperatorsModel>) results.values;
      notifyDataSetChanged();
    }

  }
}

