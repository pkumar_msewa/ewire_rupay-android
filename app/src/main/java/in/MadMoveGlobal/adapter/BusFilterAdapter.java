package in.MadMoveGlobal.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.model.BusCityModel;


/**
 * Created by Ksf on 10/6/2016.
 */
public class BusFilterAdapter extends BaseAdapter implements Filterable {

    private Context context;
    private List<BusCityModel> flightCityArray;
    private List<BusCityModel> filteredData;
    private ViewHolder viewHolder;

    public BusFilterAdapter(Context context, List<BusCityModel> flightCityArray) {
        this.context = context;
        this.flightCityArray = flightCityArray;
        this.filteredData = flightCityArray;
    }

    @Override
    public int getCount() {
        if (flightCityArray == null) {
            return 0;
        }
        return Math.min(10, flightCityArray.size());
    }

    @Override
    public Object getItem(int index) {
        return flightCityArray.get(index);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.adapter_flight_city_list, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.tvFlightCity = (TextView) convertView.findViewById(R.id.tvFlightCity);
            viewHolder.tvFlightCode = (TextView) convertView.findViewById(R.id.tvFlightCode);
            viewHolder.tvFlightCode.setVisibility(View.GONE);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.tvFlightCity.setText(flightCityArray.get(position).getCityname());


        return convertView;
    }

    @Override
    public Filter getFilter() {
        return new ItemFilter();
    }


    static class ViewHolder {
        TextView tvFlightCity;
        TextView tvFlightCode;
    }


    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final List<BusCityModel> list = filteredData;

            int count = list.size();
            final ArrayList<Object> nlist = new ArrayList<Object>(count);

            String filterableString, filterableStringcode, filterableStringAreo;

            for (int i = 0; i < count; i++) {
                    filterableString = list.get(i).getCityname();

                    if (filterableString.toLowerCase().contains(filterString)) {
                        nlist.add(list.get(i));
                    }
            }
            results.values = nlist;
            results.count = nlist.size();
            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            flightCityArray = (ArrayList<BusCityModel>) results.values;
            notifyDataSetChanged();
        }

    }
}

