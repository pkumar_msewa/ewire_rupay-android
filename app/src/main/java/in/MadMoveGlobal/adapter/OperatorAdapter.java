package in.MadMoveGlobal.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.BitmapAjaxCallback;

import java.util.ArrayList;
import java.util.List;

import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.model.OperatorsModel;

public class OperatorAdapter extends BaseAdapter implements Filterable{

    private ViewHolder viewHolder;
    private Context context;
    private List<OperatorsModel> operatorsModels;
    private List<OperatorsModel> duplicate;

    public OperatorAdapter(Context context, List<OperatorsModel> operatorsModels) {
        this.context = context;
        this.operatorsModels = operatorsModels;
        this.duplicate = operatorsModels;
    }

    @Override
    public int getCount() {
        return operatorsModels.size();
    }

    @Override
    public Object getItem(int position) {
        return operatorsModels.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final int currentPosition = position;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.adapter_operator_list, parent,false);
            viewHolder = new ViewHolder();
            viewHolder.tvOpName = (TextView) convertView.findViewById(R.id.tvOpName);
            viewHolder.ivOpImage = (ImageView) convertView.findViewById(R.id.ivOpImage);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.tvOpName.setText(operatorsModels.get(position).getName());
        AjaxCallback.setNetworkLimit(8);

//set the max number of icons (image width <= 50) to be cached in memory, default is 20
        BitmapAjaxCallback.setIconCacheLimit(50);

//set the max number of images (image width > 50) to be cached in memory, default is 20
        BitmapAjaxCallback.setCacheLimit(50);
        AQuery aQuery = new AQuery(context);
        if (operatorsModels.get(position).getDefaultCode() != null && operatorsModels.get(position).getDefaultCode().length() != 0) {
            aQuery.id(viewHolder.ivOpImage).image(operatorsModels.get(position).getDefaultCode(), true, true, 0, R.drawable.loading_image, null, AQuery.FADE_IN);
//            aQuery.id(viewHolder.ivOpImage).image(operatorsModels.get(position).getDefaultCode()).getContext();
        }else {
            aQuery.id(viewHolder.ivOpImage).background(R.drawable.loading_image).getContext();
        }
        return convertView;
    }

    @Override
    public Filter getFilter() {
        return new ItemFilter();
    }

    static class ViewHolder {
        TextView tvOpName;
        ImageView ivOpImage;

    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final List<OperatorsModel> list = duplicate;

            int count = list.size();
            final ArrayList<Object> nlist = new ArrayList<Object>(count);

            String filterableString;

            for (int i = 0; i < count; i++) {
                filterableString = list.get(i).getName();

                if (filterableString.toLowerCase().contains(filterString)) {
                    nlist.add(list.get(i));
                }
            }
            results.values = nlist;
            results.count = nlist.size();
            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            operatorsModels = (ArrayList<OperatorsModel>) results.values;
            notifyDataSetChanged();
        }

    }
}
