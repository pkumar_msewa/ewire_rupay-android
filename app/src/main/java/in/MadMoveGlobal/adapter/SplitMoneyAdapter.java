package in.MadMoveGlobal.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

//import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import in.MadMoveGlobal.model.SplitMoneyGroupModel;
import in.MadMoveGlobal.model.SplitMoneyModel;
import in.MadMoveGlobal.EwireRuPay.R;

/**
 * Created by Dushant on 07/27/2017.
 */

public class SplitMoneyAdapter extends BaseExpandableListAdapter {
    private Context context;
    private List<SplitMoneyModel> splitMoneyModels;
    private HashMap<Integer, List<SplitMoneyGroupModel>> values;


    public SplitMoneyAdapter(Context context, HashMap<Integer, List<SplitMoneyGroupModel>> values, List<SplitMoneyModel> splitMoneyModels) {
        this.context = context;
        this.values = values;
        this.splitMoneyModels = splitMoneyModels;

    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this.values.get(this.splitMoneyModels.get(groupPosition).getSplitId()).get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,boolean isLastChild, View convertView, ViewGroup parent) {

        final SplitMoneyGroupModel childText = (SplitMoneyGroupModel) getChild(groupPosition, childPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.adapter_split_money_child, parent,false);
        }

        TextView txtListChild = (TextView) convertView.findViewById(R.id.tvSplitMoneyChild);
        txtListChild.setText(" "+childText.getNamePerson()+" - "+context.getResources().getString(R.string.rupease)+" "+childText.getAmountPerson());




        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.values.get(this.splitMoneyModels.get(groupPosition).getSplitId()).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.splitMoneyModels.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.splitMoneyModels.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

//        String headerTitle = (String) getGroup(groupPosition);
        String headerTitle = splitMoneyModels.get(groupPosition).getSplitMoneyTitle();

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.adapter_split_money_parent, null);
        }

        TextView lblListHeader = (TextView) convertView.findViewById(R.id.tvSplitMoneyHeader);
        TextView tvSplitMoneyAmount = (TextView) convertView.findViewById(R.id.tvSplitMoneyAmount);

        lblListHeader.setText(headerTitle);
        tvSplitMoneyAmount.setText("Total: "+context.getResources().getString(R.string.rupease)+" "+splitMoneyModels.get(groupPosition).getSplitMoneyAmount());
        CircleImageView ivSplitMoneyHeader  = (CircleImageView) convertView.findViewById(R.id.ivSplitMoneyHeader);

        Log.i("Head Position",splitMoneyModels.get(groupPosition).getSplitGroupType());


        if(splitMoneyModels.get(groupPosition).getSplitGroupType().equals("1")){
//            Picasso.with(context).load(R.drawable.group_birthday).placeholder(R.drawable.group_birthday).into(ivSplitMoneyHeader);
        }
        else if (splitMoneyModels.get(groupPosition).getSplitGroupType().equals("2")){
//            Picasso.with(context).load(R.drawable.group_dinner).placeholder(R.drawable.group_dinner).into(ivSplitMoneyHeader);
        }
        else if (splitMoneyModels.get(groupPosition).getSplitGroupType().equals("3")){
//            Picasso.with(context).load(R.drawable.group_shopping).placeholder(R.drawable.group_shopping).into(ivSplitMoneyHeader);
        }
        else if (splitMoneyModels.get(groupPosition).getSplitGroupType().equals("4")){
//            Picasso.with(context).load(R.drawable.group_movie).placeholder(R.drawable.group_movie).into(ivSplitMoneyHeader);
        }
        else{
//            Picasso.with(context).load(R.drawable.group_custom).placeholder(R.drawable.group_custom).into(ivSplitMoneyHeader);
        }


        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}