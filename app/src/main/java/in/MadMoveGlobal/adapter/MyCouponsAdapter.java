package in.MadMoveGlobal.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;

import java.util.List;

import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.model.MyCouponsModel;
import in.MadMoveGlobal.EwireRuPay.R;

/**
 * Created by Dushant on 07/27/2017.
 */
public class MyCouponsAdapter extends BaseAdapter {

    private Context context;
    private List<MyCouponsModel> couponsArray;
    private ViewHolder viewHolder;

    public MyCouponsAdapter(Context context, List<MyCouponsModel> couponsArray) {
        this.context = context;
        this.couponsArray = couponsArray;
    }

    @Override
    public int getCount() {
        return couponsArray.size();
    }

    @Override
    public Object getItem(int index) {
        return couponsArray.get(index);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final int currentPosition = position;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.adapter_my_coupons, null);
            viewHolder = new ViewHolder();
            viewHolder.tvMyCouponsVendorAddress = (TextView) convertView.findViewById(R.id.tvMyCouponsVendorAddress);
            viewHolder.tvMyCouponsVendorName = (TextView) convertView.findViewById(R.id.tvMyCouponsVendorName);
            viewHolder.tvMyCouponsTitle = (TextView) convertView.findViewById(R.id.tvMyCouponsTitle);
            viewHolder.ivMyCoupons = (ImageView) convertView.findViewById(R.id.ivMyCoupons);
            viewHolder.llMyCouponsMain = (CardView) convertView.findViewById(R.id.llMyCouponsMain);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (couponsArray.get(position).getCouponAddress()!=null&&couponsArray.get(position).getCouponAddress().equals("National")) {
            viewHolder.tvMyCouponsVendorAddress.setText("All over India");
        } else {
            viewHolder.tvMyCouponsVendorAddress.setText(couponsArray.get(position).getCouponAddress());
        }
        viewHolder.tvMyCouponsVendorName.setText(couponsArray.get(position).getCouponVendorName());
        viewHolder.tvMyCouponsTitle.setText(couponsArray.get(position).getCouponTitle());

        if (!couponsArray.get(position).getCouponImage().isEmpty()) {
            Log.i("Image URL COupons",couponsArray.get(position).getCouponImage());
            AQuery aq  = new AQuery(context);
            aq.id(viewHolder.ivMyCoupons).background(R.drawable.loading_image).image(couponsArray.get(position).getCouponImage(), true, true);

//            Picasso.with(context).load(couponsArray.get(position).getCouponImage()).placeholder(R.drawable.loading_image).into(viewHolder.ivMyCoupons);
            viewHolder.ivMyCoupons.setVisibility(View.VISIBLE);
        } else {
            viewHolder.ivMyCoupons.setVisibility(View.GONE);
        }

        viewHolder.llMyCouponsMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (couponsArray.get(currentPosition)!=null&&!couponsArray.get(currentPosition).getCouponURL().isEmpty()) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(couponsArray.get(currentPosition).getCouponURL()));
                    browserIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(browserIntent);
                } else {
                    CustomToast.showMessage(context, "Sorry connection not available for this deal");
                }
            }
        });

        return convertView;
    }


    static class ViewHolder {
        TextView tvMyCouponsVendorAddress;
        TextView tvMyCouponsVendorName;
        TextView tvMyCouponsTitle;
        ImageView ivMyCoupons;
        CardView llMyCouponsMain;

    }

}
