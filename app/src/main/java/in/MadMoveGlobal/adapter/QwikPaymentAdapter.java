package in.MadMoveGlobal.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.MadMoveGlobal.custom.CustomAlertDialog;
import in.MadMoveGlobal.custom.EditQwikPayDialog;
import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.custom.LoadingDialog;
import in.MadMoveGlobal.custom.VerifyMPinDialog;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.metadata.AppMetadata;
import in.MadMoveGlobal.model.QwikPayModel;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.util.EditQwikPayListner;
import in.MadMoveGlobal.util.MPinVerifiedListner;
import in.MadMoveGlobal.util.NetworkErrorHandler;

/**
 * Created by Dushant on 07/27/2017.
 */

public class QwikPaymentAdapter extends RecyclerView.Adapter<QwikPaymentAdapter.RecyclerViewHolders> implements MPinVerifiedListner, EditQwikPayListner {

    private List<QwikPayModel> payItem;
    private Context context;
    private UserModel session = UserModel.getInstance();
    private LoadingDialog loadingDialog;
    //Volley Tag
    private String tag_json_obj = "json_quick_pay";
    private int currentPost;

    public QwikPaymentAdapter(Context context, List<QwikPayModel> payItem) {
        this.payItem = payItem;
        this.context = context;
        try {
            loadingDialog = new LoadingDialog(context);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_qwik_pay, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolders holder, int position) {
        final int currentPosition = position;
        holder.tvQwikAmount.setText(context.getResources().getString(R.string.rupease) + payItem.get(position).getPayAmount());
        holder.tvQwikPayDes.setText(payItem.get(position).getPayDes());

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy hh:mm" + " aaa");
        String dateString = dateFormat.format(new Date(Long.parseLong(payItem.get(position).getPayDate())));
        holder.tvQwikPayDate.setText(dateString);

        holder.tvQwikPayTitle.setText(payItem.get(position).getPayName());

        if (payItem.get(position).isfav()) {
            holder.btnFav.setBackgroundTintMode(PorterDuff.Mode.MULTIPLY);
        } else {
            holder.btnFav.setBackgroundTintMode(PorterDuff.Mode.SCREEN);
        }


        holder.btnFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadingDialog.show();
                updateFav(payItem.get(currentPosition).getPayRef(), !payItem.get(currentPosition).isfav(), currentPosition, view);
            }
        });

        holder.flRepeat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentPost = currentPosition;
                showEditDialog(currentPosition);

//                showCustomDialog(payItem.get(currentPosition).getPayAmount(), payItem.get(currentPosition).getPayDes(), currentPosition);
            }
        });


    }


    @Override
    public int getItemCount() {
        return this.payItem.size();
    }

    @Override
    public void onEditCompleted() {
        sendRefresh();
//        Intent intent = new Intent("Favtoogle");
////        intent.putExtra("done", "1");
//        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    @Override
    public void onEditError() {
    }

    @Override
    public void onSessionInvalid() {
        showInvalidSessionDialog();
    }

    public class RecyclerViewHolders extends RecyclerView.ViewHolder {
        public TextView tvQwikAmount;
        public TextView tvQwikPayDes;
        public TextView tvQwikPayDate;
        public TextView tvQwikPayTitle;
        public FloatingActionButton btnFav;
        public FrameLayout flRepeat;
        public LinearLayout llQwikPayMain;

        public RecyclerViewHolders(View itemView) {
            super(itemView);
            tvQwikAmount = (TextView) itemView.findViewById(R.id.tvQwikAmount);
            tvQwikPayDes = (TextView) itemView.findViewById(R.id.tvQwikPayDes);
            tvQwikPayDate = (TextView) itemView.findViewById(R.id.tvQwikPayDate);
            tvQwikPayTitle = (TextView) itemView.findViewById(R.id.tvQwikPayTitle);
            btnFav = (FloatingActionButton) itemView.findViewById(R.id.btnFav);
            flRepeat = (FrameLayout) itemView.findViewById(R.id.flRepeat);
            llQwikPayMain = (LinearLayout) itemView.findViewById(R.id.llQwikPayMain);
        }
    }

    public void showEditDialog(int post) {
        new EditQwikPayDialog(context, this, payItem.get(post).getPayRef(), payItem.get(post).getPayAmount(), payItem.get(post).getPayDes());
    }


    private void showCustomDialog(final String amount, final String title, final int currentPosition) {
        CustomAlertDialog builder = new CustomAlertDialog(context, R.string.dialog_title, Html.fromHtml(generateMessage(amount, title)));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (session.isMPin() && Double.valueOf(amount) >= Double.valueOf(context.getResources().getInteger(R.integer.transactionValue))) {
                    showMPinDialog();
                } else {
                    loadingDialog.show();
                    promotePayment(currentPosition);
                }
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public void showMPinDialog() {
        VerifyMPinDialog builder = new VerifyMPinDialog(context, this);
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
    }

    private String generateMessage(String amount, String title) {
        String source =
                "<b><font color=#000000>" + title + "</font></b><br>" +
                        "<b><font color=#000000> Amount: </font></b>" + "<font>" + context.getResources().getString(R.string.rupease) + " " + amount + "</font><br><br>" +
                        "<b><font color=#000000> Are you sure you want to proceed?</font></b><br>";
        return source;
    }

    private void sendToogleMessage() {
        Intent intent = new Intent("toogle");
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }


    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }


    private void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(context, R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    @Override
    public void verifiedCompleted() {
        promotePayment(currentPost);
    }

    @Override
    public void sessionInvalid() {
        showInvalidSessionDialog();
    }


    private void promotePayment(int currentPosition) {
        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("transactionRefNo", payItem.get(currentPosition).getPayRef());

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_QUICK_POST_PAY, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.i("Quick Pay RESPONSE", response.toString());
                        String code = response.getString("code");
                        String message = "";
                        if (response.has("message")) {
                            message = response.getString("message");
                        }

                        if (code != null && code.equals("S00")) {
                            loadingDialog.dismiss();
                            sendRefresh();

                        } else if (code != null && code.equals("F03")) {
                            loadingDialog.dismiss();
                            showInvalidSessionDialog();
                        } else {
                            loadingDialog.dismiss();
                        }
                        CustomToast.showMessage(context, message);

                    } catch (JSONException e) {
                        loadingDialog.dismiss();
                        CustomToast.showMessage(context, context.getResources().getString(R.string.server_exception2));
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadingDialog.dismiss();
                    error.printStackTrace();
                    CustomToast.showMessage(context, NetworkErrorHandler.getMessage(error, context));
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }


    private void updateFav(String refNo, boolean upFav, final int currentPosition, final View view) {
        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("transactionRefNo", refNo);
            jsonRequest.put("favourite", upFav);

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_UPDATE_FAV, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.i("Quick Pay Update Fav", response.toString());
                        String code = response.getString("code");
                        String message = "";
                        if (response.has("message")) {
                            message = response.getString("message");
                        }

                        if (code != null && code.equals("S00")) {
                            if (payItem.get(currentPosition).isfav()) {
                                ((FloatingActionButton) view).setBackgroundTintMode(PorterDuff.Mode.SCREEN);
                                payItem.get(currentPosition).setIsfav(false);

                                QwikPayModel qwikPayModel = payItem.get(currentPosition);
                                qwikPayModel.delete();
                                sendToogleMessage();

                            } else {
                                ((FloatingActionButton) view).setBackgroundTintMode(PorterDuff.Mode.MULTIPLY);
                                payItem.get(currentPosition).setIsfav(true);

                                QwikPayModel qwikPayModel = payItem.get(currentPosition);
                                qwikPayModel.save();
                                sendToogleMessage();
                            }
                            if (!message.isEmpty()) {
                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                            }

                            loadingDialog.dismiss();
                        } else if (code != null && code.equals("F03")) {
                            loadingDialog.dismiss();
                            showInvalidSessionDialog();
                        } else {
                            loadingDialog.dismiss();
                            if (!message.isEmpty()) {
                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (JSONException e) {
                        loadingDialog.dismiss();
                        CustomToast.showMessage(context, context.getResources().getString(R.string.server_exception2));
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadingDialog.dismiss();
                    error.printStackTrace();
                    CustomToast.showMessage(context, NetworkErrorHandler.getMessage(error, context));
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }

    }

    private void sendRefresh() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "1");
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }
}
