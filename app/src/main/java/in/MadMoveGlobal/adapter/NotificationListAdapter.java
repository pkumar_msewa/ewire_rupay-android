package in.MadMoveGlobal.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import in.MadMoveGlobal.model.NotificationModel;
import in.MadMoveGlobal.EwireRuPay.R;

/**
 * Created by Dushant on 07/27/2017.
 */
public class NotificationListAdapter extends RecyclerView.Adapter<NotificationListAdapter.RecyclerViewHolders> {

    private List<NotificationModel> itemList;
    private Context context;

    public NotificationListAdapter(List<NotificationModel> itemList, Context context) {
        this.itemList = itemList;
        this.context = context;
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_notification_item, parent, false);
        return new RecyclerViewHolders(layoutView);
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolders holder, final int position) {
       holder.tvNotifyMsg.setText(itemList.get(position).getMsg());
       holder.tvNotifyDate.setText(itemList.get(position).getDate());
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    public class RecyclerViewHolders extends RecyclerView.ViewHolder {
        public TextView tvNotifyMsg, tvNotifyDate;
        public RecyclerViewHolders(View itemView) {
            super(itemView);
            tvNotifyMsg = (TextView) itemView.findViewById(R.id.tvNotifyMsg);
            tvNotifyDate = (TextView) itemView.findViewById(R.id.tvNotifyDate);
        }
    }

}
