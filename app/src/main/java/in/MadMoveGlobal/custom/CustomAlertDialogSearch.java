package in.MadMoveGlobal.custom;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import in.MadMoveGlobal.adapter.BusFilterAdapter;
import in.MadMoveGlobal.model.BusCityModel;
import in.MadMoveGlobal.util.CitySelectedListener;
import in.MadMoveGlobal.EwireRuPay.R;


/**
 * Created by Dushant on 08/27/2017.
 */
public class CustomAlertDialogSearch extends Dialog {

    public CustomAlertDialogSearch(final Context context, ArrayList<BusCityModel> flightCityModelList, String type, final CitySelectedListener citySelectedListener) {
        super(context);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View viewDialog = inflater.inflate(R.layout.dialog_custom_search, null, false);
        android.widget.SearchView searchView = (android.widget.SearchView) viewDialog.findViewById(R.id.cadMessage);
        ListView lvSearch = (ListView) viewDialog.findViewById(R.id.lvSearchBus);
        final TextView tv_popularCity = (TextView) viewDialog.findViewById(R.id.tv_popularCity);
        ImageView back = (ImageView) viewDialog.findViewById(R.id.back);
//        flightCityModelList = new ArrayList<>();


        if (type.equalsIgnoreCase("To")) {
            searchView.setQueryHint(getContext().getResources().getString(R.string.Enter_Destination_City));
        } else {
            searchView.setQueryHint(getContext().getResources().getString(R.string.Enter_Source_City));
        }

        final BusFilterAdapter domesticCityListAdapter = new BusFilterAdapter(context, flightCityModelList);
        lvSearch.setAdapter(domesticCityListAdapter);
        searchView.setOnQueryTextListener(new android.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.length() != 0) {
                    tv_popularCity.setText(getContext().getResources().getString(R.string.Suggested_Cities));
                } else {
                    tv_popularCity.setText(getContext().getResources().getString(R.string.Popular_Cities));
                }
                domesticCityListAdapter.getFilter().filter(newText);

                return false;
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        lvSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                dismiss();
                BusCityModel domesticFlightModel = (BusCityModel) adapterView.getItemAtPosition(i);
                citySelectedListener.cityFilter(domesticFlightModel);

            }
        });
        this.setCancelable(true);

        this.setContentView(viewDialog);

        this.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        this.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

    }

}
