package in.MadMoveGlobal.custom;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by Dushant on 08/27/2017.
 */

public class MyButton extends Button {

	public MyButton(Context context) {
		super(context);
		init();
	}

	public MyButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
//		CustomFontHelper.setCustomFont(this, context, attrs);
	}

	public MyButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
//		CustomFontHelper.setCustomFont(this, context, attrs);
	}
	
	private void init() {
//        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "eurostile.ttf");
//        setTypeface(tf);
    }
}
