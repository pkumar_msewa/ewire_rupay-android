//package in.MadMoveGlobal.custom;
//
//
//import android.content.Context;
//import android.content.SharedPreferences;
//import android.content.pm.PackageManager;
//import android.hardware.fingerprint.FingerprintManager;
//import android.Manifest;
//import android.os.Build;
//import android.os.CancellationSignal;
//import android.support.annotation.RequiresApi;
//import android.support.v4.app.ActivityCompat;
//import android.widget.Toast;
//
//import javax.crypto.Cipher;
//
//import in.MadMoveGlobal.fragment.IAuthenticateListener;
//import in.MadMoveGlobal.fragment.fragmentuser.LoginFragment;
//import in.MadMoveGlobal.util.BiometricUtils;
//
///**
// * Created by Dushant on 08/27/2017.
// */
//
//@RequiresApi(api = Build.VERSION_CODES.M)
//class FingerprintHandler extends FingerprintManager.AuthenticationCallback {
//
//    private Context mContext;
//    private CancellationSignal mCancellationSignal;
//    private SharedPreferences mSharedPreferences;
//    private IAuthenticateListener mListener;
//
//    public FingerprintHandler(Context context, SharedPreferences sharedPreferences, IAuthenticateListener listener) {
//        mContext = context;
//        mSharedPreferences = sharedPreferences;
//        mListener = listener;
//        mCancellationSignal = new CancellationSignal();
//    }
//
//    public void startAuth(FingerprintManager fingerprintManager, FingerprintManager.CryptoObject cryptoObject) {
//        fingerprintManager.authenticate(cryptoObject, mCancellationSignal, 0, this, null);
//    }
//
//    @Override
//    public void onAuthenticationError(int errorCode, CharSequence errString) {
//        Toast.makeText(mContext, errString, Toast.LENGTH_SHORT).show();
//    }
//
//    @Override
//    public void onAuthenticationHelp(int helpCode, CharSequence helpString) {
//        Toast.makeText(mContext, helpString, Toast.LENGTH_SHORT).show();
//    }
//
//    @Override
//    public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
//
//        Cipher cipher = result.getCryptoObject().getCipher();
//        String encoded = mSharedPreferences.getString(LoginFragment.KEY_PASSWORD, null);
//        String decoded = BiometricUtils.decryptString(encoded, cipher);
//        mListener.onAuthenticate(decoded);
//    }
//
//    @Override
//    public void onAuthenticationFailed() {
//        Toast.makeText(mContext, "onAuthenticationFailed", Toast.LENGTH_SHORT).show();
//    }
//
//    void cancel() {
//        if (mCancellationSignal != null) mCancellationSignal.cancel();
//    }
//}
