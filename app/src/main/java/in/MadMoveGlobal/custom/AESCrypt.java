package in.MadMoveGlobal.custom;

import org.apache.commons.lang3.StringEscapeUtils;

import java.security.Key;
import java.util.regex.Pattern;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import Decoder.BASE64Decoder;
import Decoder.BASE64Encoder;

public class AESCrypt
{
  private static String algorithm = "AES";
  private static byte[] keyValue = new byte[]
    { 'E', 'W', 'C', 'A', 'w', 'i', 's', 'h', 'R', 'e', 'E', 'R', 'S', 'K', 'e', 'y' };

  // Performs Encryption
  public static String encrypt(String plainText) throws Exception
  {
    Key key = generateKey();
    Cipher chiper = Cipher.getInstance(algorithm);
    chiper.init(Cipher.ENCRYPT_MODE, key);
    byte[] encVal = chiper.doFinal(StringEscapeUtils.unescapeJson(plainText).replace(Pattern.quote("\""),"").replaceAll("\\n", "").getBytes());
    return new BASE64Encoder().encode(encVal).replace(Pattern.quote("\""),"").replaceAll("\\n", "").replaceAll("\\\\/", "");
  }

  // Performs decryption
  public static String decrypt(String encryptedText) throws Exception
  {
    // generate key
    Key key = generateKey();
    Cipher chiper = Cipher.getInstance(algorithm);
    chiper.init(Cipher.DECRYPT_MODE, key);
    byte[] decordedValue = new BASE64Decoder().decodeBuffer(encryptedText);
    byte[] decValue = chiper.doFinal(decordedValue);
    return new String(decValue);
  }

  //generateKey() is used to generate a secret key for AES algorithm
  private static Key generateKey() throws Exception
  {
    Key key = new SecretKeySpec(keyValue, algorithm);
    return key;
  }

  // performs encryption & decryption


//  public static void main(String[] args) throws Exception
//  {
//
//    String plainText = "Password";
//    String encryptedText = Decryption.encrypt(plainText);
//    String decryptedText = Decryption.decrypt(encryptedText);
//
//    System.out.println("Plain Text : " + plainText);
//    System.out.println("Encrypted Text : " + encryptedText);
//    System.out.println("Decrypted Text : " + decryptedText);
//  }
}
