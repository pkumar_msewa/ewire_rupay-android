package in.MadMoveGlobal.custom;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import in.MadMoveGlobal.EwireRuPay.R;

/**
 * Created by Dushant on 08/27/2017.
 */
public class CustomAlertDialogsReDetails extends AlertDialog.Builder {

    public CustomAlertDialogsReDetails(Context context, int title, CharSequence message) {
        super(context);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewDialog = inflater.inflate(R.layout.dialog_custom_redetails, null, false);

//        TextView messageTextView = (TextView) viewDialog.findViewById(R.id.cadMessage);
//        messageTextView.setText(message);

        this.setCancelable(false);

        this.setView(viewDialog);
        this.create();


    }
}
