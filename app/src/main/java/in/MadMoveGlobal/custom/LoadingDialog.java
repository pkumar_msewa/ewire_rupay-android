package in.MadMoveGlobal.custom;

import android.app.ProgressDialog;
import android.content.Context;

import in.MadMoveGlobal.EwireRuPay.R;

/**
 * Created by Dushant on 08/27/2017.
 */
public class LoadingDialog extends ProgressDialog{

    public LoadingDialog(Context context) {
        super(context);
        setMessage(getContext().getResources().getString(R.string.Please_wait));
        setCancelable(false);
    }
}
