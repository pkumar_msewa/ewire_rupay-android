package in.MadMoveGlobal.custom;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;

import in.MadMoveGlobal.EwireRuPay.R;
import pl.droidsonroids.gif.GifImageView;

/**
 * Created by Dushant on 08/27/2017.
 */
public class CustomFingerPrintSuccessDialog extends Dialog {

    public CustomFingerPrintSuccessDialog(Context context, String title, CharSequence message, Boolean succes) {
        super(context);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewDialog = inflater.inflate(R.layout.dialog_finger_print, null, false);

        AQuery aQuery = new AQuery(context);

//        GifImageView imageSuccess = (GifImageView) viewDialog.findViewById(R.id.imageSuccess);
//        GifImageView imageViewFail = (GifImageView) viewDialog.findViewById(R.id.imageViewFail);

//        aQuery.id(imageView).background(R.drawable.finger_print).getContext();

        TextView titleTextView = (TextView) viewDialog.findViewById(R.id.cadTiltle);
        titleTextView.setText(title);

        TextView messageTextView = (TextView) viewDialog.findViewById(R.id.cadMessage);
        messageTextView.setText(message);

        if (succes) {
            messageTextView.setTextColor(ContextCompat.getColor(context, R.color.greendark));
//            imageSuccess.setVisibility(View.VISIBLE);
//            imageViewFail.setVisibility(View.GONE);
        }
        else {
            messageTextView.setTextColor(ContextCompat.getColor(context, R.color.faliure));
//            imageSuccess.setVisibility(View.GONE);
//            imageViewFail.setVisibility(View.VISIBLE);
        }
        this.setCancelable(false);
        this.addContentView(viewDialog,null);
    }
}

