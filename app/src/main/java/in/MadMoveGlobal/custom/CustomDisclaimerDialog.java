package in.MadMoveGlobal.custom;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;

import in.MadMoveGlobal.EwireRuPay.R;

/**
 * Created by Dushant on 08/27/2017.
 */
public class CustomDisclaimerDialog extends AlertDialog.Builder {

    public CustomDisclaimerDialog(Context context) {
        super(context);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewDialog = inflater.inflate(R.layout.dialog_custom_webview, null, false);


        WebView wvDisclaimer = (WebView) viewDialog.findViewById(R.id.wvDisclaimer);
        wvDisclaimer.setVerticalScrollBarEnabled(false);
        wvDisclaimer.loadData(context.getString(R.string.dis), "text/html; charset=utf-8", "utf-8");

        this.setCancelable(false);

        this.setView(viewDialog);


    }
}

