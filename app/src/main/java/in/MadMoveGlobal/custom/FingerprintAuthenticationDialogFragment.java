package in.MadMoveGlobal.custom;

import android.app.DialogFragment;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import in.MadMoveGlobal.EwireRuPay.R;

import in.MadMoveGlobal.EwireRuPay.activity.LoginRegActivity;
import in.MadMoveGlobal.EwireRuPay.activity.MainActivity;
import in.MadMoveGlobal.EwireRuPay.activity.SplashActivity;
import in.MadMoveGlobal.finger.FingerprintUiHelper;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.model.UserModel;

public class FingerprintAuthenticationDialogFragment extends DialogFragment
        implements TextView.OnEditorActionListener, FingerprintUiHelper.Callback {

    private Button mCancelButton;
    private Button mSecondDialogButton;
    private View mFingerprintContent;
    private View mBackupContent;
    private EditText mPassword;
    private CheckBox mUseFingerprintFutureCheckBox;
    private TextView mPasswordDescriptionTextView;
    private TextView mNewFingerprintEnrolledTextView;

    private Stage mStage = Stage.FINGERPRINT;

    private FingerprintManager.CryptoObject mCryptoObject;
    private FingerprintUiHelper mFingerprintUiHelper;
    private SplashActivity mActivity;

    private InputMethodManager mInputMethodManager;
    private SharedPreferences mSharedPreferences;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Do not create a new Fragment when the Activity is re-created such as orientation changes.
        setRetainInstance(true);
        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Material_Light_Dialog);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().setCanceledOnTouchOutside(false);

//        getDialog().setTitle(getString(R.string.fingerprint_hint));
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        View v = inflater.inflate(R.layout.fingerprint_dialog_container, container, false);
        mCancelButton = (Button) v.findViewById(R.id.cancel_button);
        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                getDialog().dismiss();
                verifyPassword();
//                startActivity(new Intent(getContext(),LoginRegActivity.class));
            }
        });

        mSecondDialogButton = (Button) v.findViewById(R.id.second_dialog_button);
        mSecondDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mStage == Stage.FINGERPRINT) {
                    goToBackup();
                } else {
                    verifyPassword();
                }
            }
        });
        mFingerprintContent = v.findViewById(R.id.fingerprint_container);
        mBackupContent = v.findViewById(R.id.backup_container);
        mPassword =  v.findViewById(R.id.password);
        mPassword.setOnEditorActionListener(this);
        mPasswordDescriptionTextView = (TextView) v.findViewById(R.id.password_description);
        mUseFingerprintFutureCheckBox = (CheckBox)
                v.findViewById(R.id.use_fingerprint_in_future_check);
        mNewFingerprintEnrolledTextView = (TextView)
                v.findViewById(R.id.new_fingerprint_enrolled_description);
        mFingerprintUiHelper = new FingerprintUiHelper(
                mActivity.getSystemService(FingerprintManager.class),
                (ImageView) v.findViewById(R.id.fingerprint_icon),
                (TextView) v.findViewById(R.id.fingerprint_status), this);
        updateStage();

        // If fingerprint authentication is not available, switch immediately to the backup
        // (password) screen.
        if (!mFingerprintUiHelper.isFingerprintAuthAvailable()) {
            goToBackup();
        }
        return v;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onResume() {
        super.onResume();
        if (mStage == Stage.FINGERPRINT) {
            mFingerprintUiHelper.startListening(mCryptoObject);
        }
        int width = getResources().getDimensionPixelSize(R.dimen.popup_width);
        int height = getResources().getDimensionPixelSize(R.dimen.popup_height);
        getDialog().getWindow().setLayout(width, height);
    }

    public void setStage(Stage stage) {
        mStage = stage;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onPause() {
        super.onPause();
        mFingerprintUiHelper.stopListening();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (SplashActivity) getActivity();
        mInputMethodManager = context.getSystemService(InputMethodManager.class);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    /**
     * Sets the crypto object to be passed in when authenticating with fingerprint.
     */
    public void setCryptoObject(FingerprintManager.CryptoObject cryptoObject) {
        mCryptoObject = cryptoObject;
    }

    /**
     * Switches to backup (password) screen. This either can happen when fingerprint is not
     * available or the user chooses to use the password authentication method by pressing the
     * button. This can also happen when the user had too many fingerprint attempts.
     */
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void goToBackup() {
        mStage = Stage.PASSWORD;
        updateStage();
        mPassword.requestFocus();

        // Show the keyboard.
        mPassword.postDelayed(mShowKeyboardRunnable, 500);

        // Fingerprint is not used anymore. Stop listening for it.
        mFingerprintUiHelper.stopListening();
    }

    /**
     * Checks whether the current entered password is correct, and dismisses the the dialog and
     * let's the activity know about the result.
     */
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void verifyPassword() {
                                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                                                KeyguardManager km = (KeyguardManager) getActivity().getSystemService(Context.KEYGUARD_SERVICE);
//                                                 km1 =  getActivity().getSystemService(Context.FINGERPRINT_SERVICE);

                                                if (km.isKeyguardSecure()) {
                                                    Intent authIntent;
                                                    authIntent = km.createConfirmDeviceCredentialIntent("TestApp", "testing passcode app");
                                                    startActivityForResult(authIntent, 0);
                                                }
                                            }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0) {
            if (resultCode == -1) {
                getUserDetail();
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void getUserDetail() {

        LoadingDialog loadingDialog = new LoadingDialog(getContext());
        loadingDialog.show();

        UserModel session = UserModel.getInstance();
        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", UserModel.getInstance().getUserSessionId());

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("GET USER PARAMS", jsonRequest.toString());
            Log.i("UserUrl", ApiUrl.URL_GET_USER_DETAILS);
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_GET_USER_DETAILS, jsonRequest, new Response.Listener<JSONObject>() {
                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.i("GET USER RESPONSE", response.toString());
                        String code = response.getString("code");

                        if (code != null & code.equals("S00")) {
                            loadingDialog.dismiss();
//                            String jsonString = response.getString("response");
//                            JSONObject jsonObject = new JSONObject(jsonString);

                            //New Implementation
                            JSONObject jsonDetail = response.getJSONObject("details");
                            JSONObject accDetail = jsonDetail.getJSONObject("accountDetail");
                            boolean isImpsFixed = jsonDetail.getBoolean("isImpsFixed");
                            String impsCommissionAmt = jsonDetail.getString("impsCommissionAmt");
                            double userBalanceD = accDetail.getDouble("balance");
                            String userBalance = String.valueOf(new DecimalFormat("#.##").format(userBalanceD));

//                            MainActivity.group =

                            long userAcNoL = accDetail.getLong("accountNumber");
                            String userAcNo = String.valueOf(userAcNoL);
                            int userPointsI = accDetail.getInt("points");
                            String userPoints = String.valueOf(userPointsI);


                            JSONObject accType = accDetail.getJSONObject("accountType");

                            String accName = accType.getString("name");
                            String accCode = accType.getString("code");


                            double accBalanceLimitD = accType.getDouble("balanceLimit");
                            double accMonthlyLimitD = accType.getDouble("monthlyLimit");
                            double accDailyLimitD = accType.getDouble("dailyLimit");

                            String accBalanceLimit = String.valueOf(accBalanceLimitD);
                            String accMonthlyLimit = String.valueOf(accMonthlyLimitD);
                            String accDailyLimit = String.valueOf(accDailyLimitD);

                            JSONObject jsonUserDetail = jsonDetail.getJSONObject("userDetail");
                            MainActivity.group = jsonUserDetail.getString("groupName");
                            String userFName = jsonUserDetail.getString("firstName");
                            String userLName = jsonUserDetail.getString("lastName");
                            String userMobile = jsonUserDetail.getString("contactNo");
                            String userEmail = jsonUserDetail.getString("email");
                            String userEmailStatus = jsonUserDetail.getString("emailStatus");

                            String userAddress = jsonUserDetail.getString("locality");
                            String userImage = jsonUserDetail.getString("image");
//                            boolean isMPin = jsonUserDetail.getBoolean("mpinPresent");

                            String userDob = jsonUserDetail.getString("dateOfBirth");
//                            String userGender = jsonUserDetail.getString("gender");
                            String encodedImage = response.getString("encodedImage");
                            boolean hasRefer = response.getBoolean("hasRefer");
                            boolean hasvertualcard = response.getBoolean("hasVirtualCard");
                            boolean hasphysicalCard = response.getBoolean("hasPhysicalCard");
                            boolean hasPhysicalRequest = response.getBoolean("hasPhysicalRequest");
                            String physicalCardStatus = response.getString("physicalCardStatus");
                            String cardMinBalance = response.getString("minimumCardBalance");
                            String cardFees = response.getString("cardFees");
                            String cardBaseFare = response.getString("cardBaseFare");
                            boolean haskycRequest = response.getBoolean("kycRequest");
                            String loadMoneyComm = response.getString("loadMoneyComm");
                            boolean PCStatus = response.getBoolean("physicalIsBlock");
                            boolean VCStatus = response.getBoolean("virtualIsBlock");
                            String virtualBlock = response.getString("virtualCardBlock");
                            String physicalBlock = response.getString("physicalCardBlock");

                            String cardNo,cardHolder,cvv,expiryDate;

                            if (!response.isNull("cardDetails")) {
                                JSONObject cardDetails = response.getJSONObject("cardDetails");

                                cardNo = cardDetails.getString("walletNumber");
                                cvv = cardDetails.getString("cvv");
                                expiryDate = cardDetails.getString("expiryDate");
                                cardHolder = cardDetails.getString("holderName");
                            } else {
                                cardNo = "XXXXXXXXXXXXXXXX";
                                cvv = "XXX";
                                expiryDate = "XX/XX";
                                cardHolder = "XXXXXXXXXXXXXX";

                            }

                            UserModel.deleteAll(UserModel.class);

                            try {
                                //Encrypting sessionId
                                UserModel userModel = new UserModel(session.getUserSessionId(), userFName, userLName, userMobile, userEmail, userEmailStatus, 1, userAcNo, accName, accCode, userBalance, accBalanceLimit, accDailyLimit, accMonthlyLimit, userAddress, encodedImage, userDob, "", false, userPoints, hasRefer, hasvertualcard, hasphysicalCard, cardNo, cvv, expiryDate, cardHolder, hasPhysicalRequest, physicalCardStatus, cardMinBalance, cardFees, haskycRequest,loadMoneyComm,cardBaseFare,PCStatus, VCStatus, virtualBlock, physicalBlock,impsCommissionAmt,isImpsFixed);
                                userModel.save();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            session.setUserFirstName(userFName);
                            session.setUserLastName(userLName);
                            session.setUserMobileNo(userMobile);
                            session.setUserEmail(userEmail);
                            session.setUserAddress(userAddress);
                            session.setEmailIsActive(userEmailStatus);

                            session.setUserAcCode(accCode);
                            session.setUserAcNo(userAcNo);
                            session.setUserBalance(userBalance);
                            session.setUserAcName(accName);
                            session.setUserMonthlyLimit(accMonthlyLimit);
                            session.setUserDailyLimit(accDailyLimit);
                            session.setUserBalanceLimit(accBalanceLimit);
                            session.setUserImage(encodedImage);
                            session.setUserAddress(userAddress);
//                            session.setUserGender(userGender);
                            session.setUserDob(userDob);

                            session.setUsercardNo(cardNo);
                            session.setUsercvv(cvv);
                            session.setUserexpiryDate(expiryDate);
                            session.setUsercardHolder(cardHolder);
                            session.setUserminimumCardBalance(cardMinBalance);
                            session.setUsercardFees(cardFees);
                            session.setUsercardBaseFare(cardBaseFare);
                            session.setIsImpsFixed(isImpsFixed);
                            session.setImpsCommissionAmt(impsCommissionAmt);
//                            session.setMPin(isMPin);
                            session.setIsValid(1);
                            session.setUserPoints(userPoints);
                            session.setHasRefer(hasRefer);
                            session.setHasVcard(hasvertualcard);
                            session.setHasPcard(hasphysicalCard);
                            session.setHasPhysicalRequest(hasPhysicalRequest);
                            session.setPhysicalCardStatus(physicalCardStatus);
                            session.setHaskycRequest(haskycRequest);
                            session.setloadMoneyComm(loadMoneyComm);
                            session.setPCStatus(PCStatus);
                            session.setVCStatus(VCStatus);
                            session.setUserVirtualBlock(virtualBlock);
                            session.setUserPhysicalBlock(physicalBlock);
//                            mPinActive = isMPin;
                            Log.i("uSER dETAILS", " uSER dETAILS");


//                            if (mPinActive) {
//                                pbSplashStatus.setVisibility(View.GONE);
//                                tvSplashStatus.setVisibility(View.GONE);
//
//                                Intent mainIntent = new Intent(SplashActivity.this, MainActivity.class);
//                                startActivity(mainIntent);
////                                Intent mainIntent = new Intent(SplashActivity.this, VerifyMPinNewActivity.class);
////                                startActivity(mainIntent);
//                                finish();
//                            } else {
//                                pbSplashStatus.setVisibility(View.GONE);
//                                tvSplashStatus.setVisibility(View.GONE);

                            Intent mainIntent = new Intent(getActivity(), MainActivity.class);
                            startActivity(mainIntent);
                            getActivity().finish();
//                            }

                        } else if (code.equals("F03")) {
                            loadingDialog.dismiss();
                            CustomToast.showMessage(getActivity(), "Please login and try again");
                            startActivity(new Intent(getContext(), LoginRegActivity.class));
                        } else {
                            loadingDialog.dismiss();
//                            if (mPinActive) {
//                                pbSplashStatus.setVisibility(View.GONE);
//                                tvSplashStatus.setVisibility(View.GONE);
//
//                                Intent mainIntent = new Intent(SplashActivity.this, VerifyMPinNewActivity.class);
//                                startActivity(mainIntent);
//                                finish();
//                            } else {

                            Intent mainIntent = new Intent(getActivity(), MainActivity.class);
                            startActivity(mainIntent);
                            getActivity().finish();
//                            }
                        }
                    } catch (JSONException e) {
                        loadingDialog.dismiss();
                        e.printStackTrace();
//                        if (mPinActive) {
//                            pbSplashStatus.setVisibility(View.GONE);
//                            tvSplashStatus.setVisibility(View.GONE);
//
//                            Intent mainIntent = new Intent(SplashActivity.this, VerifyMPinNewActivity.class);
//                            startActivity(mainIntent);
//                            finish();
//                        } else {

                        Intent mainIntent = new Intent(getActivity(), LoginRegActivity.class);
                        startActivity(mainIntent);
                        getActivity().finish();
//                        }

                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadingDialog.dismiss();
                    error.printStackTrace();
//                    if (mPinActive) {
//                        pbSplashStatus.setVisibility(View.GONE);
//                        tvSplashStatus.setVisibility(View.GONE);
//
//                        Intent mainIntent = new Intent(SplashActivity.this, VerifyMPinNewActivity.class);
//                        startActivity(mainIntent);
//                        finish();
//                    } else {

                    Intent mainIntent = new Intent(getActivity(), MainActivity.class);
                    startActivity(mainIntent);
                    getActivity().finish();
//                    }


                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hash", "1234");
                    return map;
                }

            };

            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            Volley.newRequestQueue(getContext()).add(postReq);
        }
    }

    /**
     * @return true if {@code password} is correct, false otherwise
     */
    private boolean checkPassword(String password) {
        // Assume the password is always correct.
        // In the real world situation, the password needs to be verified in the server side.
        return password.length() > 0;
    }

    private final Runnable mShowKeyboardRunnable = new Runnable() {
        @Override
        public void run() {
            mInputMethodManager.showSoftInput(mPassword, 0);
        }
    };

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void updateStage() {
        switch (mStage) {
            case FINGERPRINT:
                mCancelButton.setText(R.string.cancel);
                mSecondDialogButton.setText(R.string.use_password);
                mFingerprintContent.setVisibility(View.VISIBLE);
                mBackupContent.setVisibility(View.GONE);
                break;
            case NEW_FINGERPRINT_ENROLLED:
                // Intentional fall through
            case PASSWORD:if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                KeyguardManager km = (KeyguardManager) getContext().getSystemService(Context.KEYGUARD_SERVICE);

                if (km.isKeyguardSecure()) {
                    Intent authIntent;
                    authIntent = km.createConfirmDeviceCredentialIntent("Please Enter Password/Pattern", "");
                    startActivityForResult(authIntent, 0);
                }
            }

                break;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_GO) {
            verifyPassword();
            return true;
        }
        return false;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onAuthenticated() {
        // Callback from FingerprintUiHelper. Let the activity know that authentication was
        // successful.
        mActivity.onPurchased(true /* withFingerprint */, mCryptoObject);
        dismiss();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onError() {
        goToBackup();
    }

    /**
     * Enumeration to indicate which authentication method the user is trying to authenticate with.
     */
    public enum Stage {
        FINGERPRINT,
        NEW_FINGERPRINT_ENROLLED,
        PASSWORD
    }
}