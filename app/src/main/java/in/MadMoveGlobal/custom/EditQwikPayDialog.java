package in.MadMoveGlobal.custom;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.metadata.AppMetadata;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.EwireRuPay.activity.MainMenuDetailActivity;
import in.MadMoveGlobal.util.CheckLog;
import in.MadMoveGlobal.util.EditQwikPayListner;
import in.MadMoveGlobal.util.NetworkErrorHandler;
import in.MadMoveGlobal.util.PayingDetailsValidation;

/**
 * Created by Dushant on 08/27/2017.
 */
public class EditQwikPayDialog extends AlertDialog.Builder {

    private TextView tvDialogEditQwikPay;
    private MaterialEditText etDialogEditQwikPay;
    private Button btnDialogEditPayQwikSave;
    private Button btnDialogEditPayQwikDismiss;
    private AlertDialog closeDialog;
    private EditQwikPayListner editQwikPayListner;
    private Context context;

    private View focusView = null;
    private boolean cancel;

    private LoadingDialog loadDlg;
    private JSONObject jsonRequest;
    //Volley Tag
    private String tag_json_obj = "json_fund_pay";
    private UserModel session = UserModel.getInstance();
    private String refId;
    private String description;

    public EditQwikPayDialog(Context context, final EditQwikPayListner editQwikPayListner, String refId, String amount, String description) {
        super(context);
        this.context = context;
        this.description = description;
        this.editQwikPayListner = editQwikPayListner;
        this.refId = refId;
        loadDlg = new LoadingDialog(context);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewDialog = inflater.inflate(R.layout.dialog_custom_edit_qwikpay, null, false);
        etDialogEditQwikPay = (MaterialEditText) viewDialog.findViewById(R.id.etDialogEditQwikPay);
        if(amount.contains(".")){
            String[] amountArray = amount.split("\\.");
            etDialogEditQwikPay.setText(amountArray[0]);
        }else{
            etDialogEditQwikPay.setText(amount);
        }
        tvDialogEditQwikPay = (TextView) viewDialog.findViewById(R.id.tvDialogEditQwikPay);
        btnDialogEditPayQwikDismiss = (Button) viewDialog.findViewById(R.id.btnDialogEditPayQwikDismiss);
        btnDialogEditPayQwikSave = (Button) viewDialog.findViewById(R.id.btnDialogEditPayQwikSave);
        tvDialogEditQwikPay.setText(description);

        btnDialogEditPayQwikSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptEdit();
            }
        });

        btnDialogEditPayQwikDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeDialog.dismiss();

            }
        });


        this.setView(viewDialog);
        closeDialog = this.create();
        closeDialog.show();
    }


    private void checkPayAmount(String amount) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
        if (!gasCheckLog.isValid) {
            etDialogEditQwikPay.setError(context.getString(gasCheckLog.msg));
            focusView = etDialogEditQwikPay;
            cancel = true;
        } else if (Double.valueOf(etDialogEditQwikPay.getText().toString()) < 1) {
            etDialogEditQwikPay.setError(context.getString(R.string.lessAmount));
            focusView = etDialogEditQwikPay;
            cancel = true;
        }
    }

    private void attemptEdit() {
        etDialogEditQwikPay.setError(null);
        cancel = false;
        checkPayAmount(etDialogEditQwikPay.getText().toString());
        if (cancel) {
            focusView.requestFocus();
        } else {
           checkUpBalance();
        }
    }

    public void checkUpBalance() {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("amount", etDialogEditQwikPay.getText().toString());
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("transactionRefNo", refId);
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("QWIKURL", ApiUrl.URL_CHECK_BALANCE_FOR_QWIKPAY);
            Log.i("JsonRequest", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_CHECK_BALANCE_FOR_QWIKPAY, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.i("Edit amount", response.toString());
                    try {
                        String code = response.getString("code");
                        if (code != null && code.equals("S00")) {
                            loadDlg.dismiss();
                           promoteEditAmount();
                        }else if (code != null && code.equals("T01")) {
                            loadDlg.dismiss();
                            closeDialog.dismiss();
                            JSONObject dtoObject = response.getJSONObject("dto");
                            String splitAmount = dtoObject.getString("splitAmount");
                            String message = response.getString("message");
                            CustomToast.showMessage(context,"You have insufficient balance. Please wait while you are redirected to load money.");
                            Intent mainMenuDetailActivity = new Intent(context, MainMenuDetailActivity.class);
                            mainMenuDetailActivity.putExtra(AppMetadata.FRAGMENT_TYPE, "LoadMoney");
                            mainMenuDetailActivity.putExtra("AutoFill", "yes");
                            if (!splitAmount.isEmpty()){
                                mainMenuDetailActivity.putExtra("TYPE","QWICKPAY");
                                mainMenuDetailActivity.putExtra("splitAmount", splitAmount);
                                mainMenuDetailActivity.putExtra("URL",ApiUrl.URL_QUICK_POST_PAY);
                                mainMenuDetailActivity.putExtra("REQUESTOBJECT",jsonRequest.toString());
                                context.startActivity(mainMenuDetailActivity);
//                                getActivity().finish();
                            }else{
                                CustomToast.showMessage(context,"split amount can't be empty");
                            }


                        } else if (code != null && code.equals("F03")) {
                            loadDlg.dismiss();
                            closeDialog.dismiss();
                            editQwikPayListner.onSessionInvalid();
                        } else {
                            loadDlg.dismiss();
                            closeDialog.dismiss();
                            if (response.has("message") && response.getString("message") != null) {
                                String message = response.getString("message");
                                CustomToast.showMessage(context, message);
                            }
                            editQwikPayListner.onEditError();
                        }
                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(context, context.getResources().getString(R.string.server_exception2));
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    CustomToast.showMessage(context, NetworkErrorHandler.getMessage(error, context));
                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    public void promoteEditAmount() {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("amount", etDialogEditQwikPay.getText().toString());
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("transactionRefNo", refId);
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("QWKURL", ApiUrl.URL_QUICK_POST_PAY);
            Log.i("QUICKREQ", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_QUICK_POST_PAY, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.i("QUICKRESP", response.toString());
                    try {
                        String code = response.getString("code");
                        if (code != null && code.equals("S00")) {
                            loadDlg.dismiss();
                            closeDialog.dismiss();
                            editQwikPayListner.onEditCompleted();

                        } else if (code != null && code.equals("F03")) {
                            loadDlg.dismiss();
                            closeDialog.dismiss();
                            editQwikPayListner.onSessionInvalid();
                        } else {
                            loadDlg.dismiss();
                            closeDialog.dismiss();
                            if (response.has("message") && response.getString("message") != null) {
                                String message = response.getString("message");
                                CustomToast.showMessage(context, message);
                            }
                            editQwikPayListner.onEditError();
                        }
                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(context, context.getResources().getString(R.string.server_exception2));
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    CustomToast.showMessage(context, NetworkErrorHandler.getMessage(error, context));
                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }
}