package in.MadMoveGlobal.custom;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Dushant on 08/27/2017.
 */

public class FontsForXMLLight extends TextView{
	
	public FontsForXMLLight(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public FontsForXMLLight(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public FontsForXMLLight(Context context) {
        super(context);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                                               "Roboto-Light.ttf");
        setTypeface(tf);
    }

}


