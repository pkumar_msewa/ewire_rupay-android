package in.MadMoveGlobal.custom;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.util.APIEncryptDecryptUtility;
import in.MadMoveGlobal.util.NetworkErrorHandler;
import in.MadMoveGlobal.util.SecurityUtil;
import in.MadMoveGlobal.util.VerifyLoginOTPListner;

/**
 * Created by Dushant on 08/27/2017.
 */
public class VerifyLoginOTPDialog extends AlertDialog.Builder {

    private TextView tvDialogOtpTitle;
    private MaterialEditText etDialogOTPVerify;
    private Button btnDialogVerifyDismiss;
    private Button btnDialogVerifyOk;

    private AlertDialog closeDialog;
    private VerifyLoginOTPListner verifyListner;
    private Context context;

    private View focusView = null;
    private boolean cancel;

    private LoadingDialog loadDlg;
    private JSONObject jsonRequest;
    //Volley Tag
    private String tag_json_obj = "json_fund_pay";
    private UserModel session = UserModel.getInstance();
    private String mobileNo;
    private String password;
    private String regId;
    private   String cardNo, cvv, cardHolder, expiryDate;
    //To Be Used Later
    private JSONObject jsonRequestED;

    public VerifyLoginOTPDialog(Context context, final VerifyLoginOTPListner verifyListner, String mobileNo, String password, String regId) {
        super(context);
        this.context = context;
        this.mobileNo = mobileNo;
        this.verifyListner = verifyListner;
        this.password = password;
        this.regId = regId;
        loadDlg = new LoadingDialog(context);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewDialog = inflater.inflate(R.layout.dialog_verify_login_otp, null, false);

        etDialogOTPVerify = (MaterialEditText) viewDialog.findViewById(R.id.etDialogOTPVerify);

        tvDialogOtpTitle = (TextView) viewDialog.findViewById(R.id.tvDialogOtpTitle);
        btnDialogVerifyDismiss = (Button) viewDialog.findViewById(R.id.btnDialogVerifyDismiss);
        btnDialogVerifyOk = (Button) viewDialog.findViewById(R.id.btnDialogVerifyOk);
        tvDialogOtpTitle.setText("Verify the OTP sent on "+ mobileNo);

        btnDialogVerifyOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLoginNew();
            }
        });

        btnDialogVerifyDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeDialog.dismiss();

            }
        });


        this.setView(viewDialog);
        closeDialog = this.create();
        closeDialog.show();
    }

    public void attemptLoginNew() {
        loadDlg.show();
        jsonRequest = new JSONObject();
        String encryptValue = "";
        try {
            jsonRequest.put("username", mobileNo);
            jsonRequest.put("password", password);
            jsonRequest.put("validate",true);
            jsonRequest.put("mobileToken",etDialogOTPVerify.getText().toString());


            if (!regId.isEmpty()) {
                jsonRequest.put("registrationId", regId);
            }
            if (SecurityUtil.getIMEI(context) != null) {
                jsonRequest.put("ipAddress", SecurityUtil.getAndroidId(context) + "-" + SecurityUtil.getIMEI(context));
            } else {
                jsonRequest.put("ipAddress", SecurityUtil.getAndroidId(context));
            }
            jsonRequestED = new JSONObject();
            try {
                encryptValue = APIEncryptDecryptUtility.encrypt(jsonRequest.toString());
                jsonRequestED.put("value", encryptValue);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            JSONObject jsonObjectRes = null;
            try {
                Log.i("REQUEST-DRY", APIEncryptDecryptUtility.decrypt(encryptValue));
                jsonObjectRes = new JSONObject(APIEncryptDecryptUtility.decrypt(encryptValue));
            } catch (Exception e) {
                e.printStackTrace();
            }

            Log.i("URL LOGIN", ApiUrl.URL_LOGIN);
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_LOGIN, jsonObjectRes, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.i("LOgin Resonse", response.toString());
                        String code = response.getString("code");

                        if (code != null && code.equals("S00")) {
//                            String jsonString = response.getString("response");
//                            JSONObject jsonObject = new JSONObject(jsonString);

                            //New Implementation
                            JSONObject jsonDetail = response.getJSONObject("details");
                            String userSessionId = jsonDetail.getString("sessionId");
                            boolean isImpsFixed = jsonDetail.getBoolean("isImpsFixed");
                            String impsCommissionAmt = jsonDetail.getString("impsCommissionAmt");
                            JSONObject accDetail = jsonDetail.getJSONObject("accountDetail");

                            double userBalanceD = accDetail.getDouble("balance");
                            String userBalance = String.valueOf(new DecimalFormat("#.##").format(userBalanceD));

                            long userAcNoL = accDetail.getLong("accountNumber");
                            String userAcNo = String.valueOf(userAcNoL);

                            int userPointsI = accDetail.getInt("points");
                            String userPoints = String.valueOf(userPointsI);

                            JSONObject accType = accDetail.getJSONObject("accountType");
                            String accName = accType.getString("name");
                            String accCode = accType.getString("code");

                            double accBalanceLimitD = accType.getDouble("balanceLimit");
                            double accMonthlyLimitD = accType.getDouble("monthlyLimit");
                            double accDailyLimitD = accType.getDouble("dailyLimit");

                            String accBalanceLimit = String.valueOf(accBalanceLimitD);
                            String accMonthlyLimit = String.valueOf(accMonthlyLimitD);
                            String accDailyLimit = String.valueOf(accDailyLimitD);

                            JSONObject jsonUserDetail = jsonDetail.getJSONObject("userDetail");

                            String userFName = jsonUserDetail.getString("firstName");
                            String userLName = jsonUserDetail.getString("lastName");
                            String userMobile = jsonUserDetail.getString("contactNo");
                            String userEmail = jsonUserDetail.getString("email");
                            String userEmailStatus = jsonUserDetail.getString("emailStatus");

                            String userAddress = jsonUserDetail.getString("locality");
                            String userImage = jsonUserDetail.getString("image");
                            String encodedImage = response.getString("encodedImage");
//                            String userGender = "";
//                            if (!jsonUserDetail.isNull("gender")) {
//                                userGender = jsonUserDetail.getString("gender");
//                            }

                            String userDob = jsonUserDetail.getString("dateOfBirth");
                            boolean hasRefer = response.getBoolean("hasRefer");
                            boolean hasPhysicalRequest = response.getBoolean("hasPhysicalRequest");
                            String physicalCardStatus = response.getString("physicalCardStatus");
                            boolean hasvertualcard = response.getBoolean("hasVirtualCard");
                            boolean hasphysicalCard = response.getBoolean("hasPhysicalCard");
                            String  cardMinBalance = response.getString("minimumCardBalance");
                            String cardFees = response.getString("cardFees");
                            String cardBaseFare = response.getString("cardBaseFare");
                            String loadMoneyComm = response.getString("loadMoneyComm");
                            boolean PCStatus = response.getBoolean("physicalIsBlock");
                            boolean VCStatus = response.getBoolean("virtualIsBlock");
                            String virtualBlock = response.getString("virtualCardBlock");
                            String physicalBlock = response.getString("physicalCardBlock");

                            if (!response.isNull("cardDetails")) {
                                JSONObject cardDetails = response.getJSONObject("cardDetails");

                                cardNo = cardDetails.getString("cardNo");
                                cvv = cardDetails.getString("cvv");
                                expiryDate = cardDetails.getString("expiryDate");
                                cardHolder = cardDetails.getString("cardHolder");
                            } else {
                                cardNo="XXXXXXXXXXXXXXXX";
                                cvv="XXX";
                                expiryDate="XXXX-XX";
                                cardHolder="XXXXXXXXXXXXXX";

                            }

                            UserModel.deleteAll(UserModel.class);
                            try {
                                //Encrypting sessionId
                                UserModel userModel = new UserModel(session.getUserSessionId(), userFName, userLName, userMobile, userEmail, userEmailStatus, 1, userAcNo, accName, accCode, session.getUserBalance(), accBalanceLimit, accDailyLimit, accMonthlyLimit, userAddress, encodedImage, userDob, "", false, userPoints, hasRefer, hasvertualcard, hasphysicalCard, cardNo, cvv, expiryDate, cardHolder, hasPhysicalRequest, physicalCardStatus,cardMinBalance,cardFees,false,loadMoneyComm,cardBaseFare, PCStatus, VCStatus, virtualBlock, physicalBlock,impsCommissionAmt,isImpsFixed);
                                userModel.save();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            session.setUserSessionId(userSessionId);
                            session.setUserFirstName(userFName);
                            session.setUserLastName(userLName);
                            session.setUserMobileNo(userMobile);
                            session.setUserEmail(userEmail);
                            session.setEmailIsActive(userEmailStatus);
                            session.setUserAcCode(accCode);
                            session.setUserAcNo(userAcNo);
                            session.setUserBalance(userBalance);
                            session.setUserAcName(accName);
                            session.setUserMonthlyLimit(accMonthlyLimit);
                            session.setUserDailyLimit(accDailyLimit);
                            session.setUserBalanceLimit(accBalanceLimit);
                            session.setUserImage(userImage);
                            session.setUserAddress(userAddress);
                            session.setUsercardNo(cardNo);
                            session.setUsercvv(cvv);
                            session.setUserexpiryDate(expiryDate);
                            session.setUsercardHolder(cardHolder);
                            session.setUserminimumCardBalance(cardMinBalance);
                            session.setUsercardFees(cardFees);
                            session.setUsercardFees(cardBaseFare);
//                            session.setUserGender(userGender);
                            session.setUserDob(userDob);
//                            session.setMPin(isMPin);
                            session.setHasRefer(hasRefer);
                            session.setHasVcard(hasvertualcard);
                            session.setHasPcard(hasphysicalCard);
                            session.setIsValid(1);
                            session.setUserPoints(userPoints);
                            session.setHasPhysicalRequest(hasPhysicalRequest);
                            session.setPhysicalCardStatus(physicalCardStatus);
                            session.setPCStatus(PCStatus);
                            session.setVCStatus(VCStatus);
                            session.setUserVirtualBlock(virtualBlock);
                            session.setUserPhysicalBlock(physicalBlock);
                            session.setIsImpsFixed(isImpsFixed);
                            session.setImpsCommissionAmt(impsCommissionAmt);

                            loadDlg.dismiss();
                            verifyListner.onVerifySuccess();
                        }
                        else {
                            String message = response.getString("message");
                            loadDlg.dismiss();
                            CustomToast.showMessage(context, message);
                            verifyListner.onVerifyError();
                        }
                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(context, context.getResources().getString(R.string.server_exception2));
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    CustomToast.showMessage(context, NetworkErrorHandler.getMessage(error, context));
                    error.printStackTrace();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };

            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }
}
