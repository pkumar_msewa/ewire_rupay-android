package in.MadMoveGlobal.custom;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import in.MadMoveGlobal.util.CitySelectedListener;
import in.MadMoveGlobal.EwireRuPay.R;

/**
 * Created by Dushant on 08/27/2017.
 */

public class CustomFiltterDialog extends Dialog {

    public CustomFiltterDialog(Context context, String title, CharSequence message, final CitySelectedListener citySelectedListener) {
        super(context);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewDialog = inflater.inflate(R.layout.dialog_custom_filtter_success, null, false);
        TextView messageTextView = (TextView) viewDialog.findViewById(R.id.cadMessage);
        Button btnOk = (Button) viewDialog.findViewById(R.id.btnOk);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                citySelectedListener.citySelected("", 100, "");
                dismiss();
            }
        });

        this.setCancelable(false);

        this.setContentView(viewDialog);


    }
}

