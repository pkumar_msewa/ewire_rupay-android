package in.MadMoveGlobal.custom;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;

import in.MadMoveGlobal.EwireRuPay.R;

/**
 * Created by Dushant on 08/27/2017.
 */
public class CustomAlertsDialog extends AlertDialog.Builder {

    public CustomAlertsDialog(Context context, int title, CharSequence message) {
        super(context);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewDialog = inflater.inflate(R.layout.dialog_customs, null, false);

//        TextView titleTextView = (TextView) viewDialog.findViewById(R.id.cadTiltle);
//        titleTextView.setText(context.getText(title));
//
//        TextView messageTextView = (TextView) viewDialog.findViewById(R.id.cadMessage);
//        messageTextView.setText(message);



        this.setCancelable(false);

        this.setView(viewDialog);
        this.create();


    }
}
