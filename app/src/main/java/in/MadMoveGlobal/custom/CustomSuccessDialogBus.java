package in.MadMoveGlobal.custom;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import in.MadMoveGlobal.EwireRuPay.R;

/**
 * Created by Dushant on 08/27/2017.
 */
public class CustomSuccessDialogBus extends AlertDialog.Builder {

    public CustomSuccessDialogBus(Context context, String title, CharSequence message) {
        super(context);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewDialog = inflater.inflate(R.layout.dialog_custom_success, null, false);

        TextView titleTextView = (TextView) viewDialog.findViewById(R.id.cadTiltle);
        titleTextView.setText(title);

        TextView messageTextView = (TextView) viewDialog.findViewById(R.id.cadMessage);
        messageTextView.setText(message);
        messageTextView.setGravity(Gravity.LEFT);

        this.setCancelable(false);

        this.setView(viewDialog);


    }
}

