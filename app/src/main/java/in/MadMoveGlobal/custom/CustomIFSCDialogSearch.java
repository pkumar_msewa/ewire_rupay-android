package in.MadMoveGlobal.custom;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.adapter.IFSCAdapter;
import in.MadMoveGlobal.model.IFSCModel;
import in.MadMoveGlobal.util.IFSCSelectedListener;



/**
 * Created by Dushant on 08/27/2017.
 */
public class CustomIFSCDialogSearch extends Dialog {

    public CustomIFSCDialogSearch(final Context context, ArrayList<IFSCModel> serviceList, String type, final IFSCSelectedListener ifscSelectedListener) {
        super(context);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View viewDialog = inflater.inflate(R.layout.dialog_custom_search, null, false);
        android.widget.SearchView searchView = (android.widget.SearchView) viewDialog.findViewById(R.id.cadMessage);
        ListView lvSearch = (ListView) viewDialog.findViewById(R.id.lvSearchBus);
        final TextView tv_popularCity = (TextView) viewDialog.findViewById(R.id.tv_popularCity);
//        tv_popularCity.setVisibility(View.GONE);
        ImageView back = (ImageView) viewDialog.findViewById(R.id.back);
//        flightCityModelList = new ArrayList<>();


        if (type.equalsIgnoreCase("To")) {
            searchView.setQueryHint("Enter Destination City");
        } else {
            searchView.setQueryHint("Select your beneficiary ");
        }

        final IFSCAdapter ifscAdapter = new IFSCAdapter(context, serviceList);
        lvSearch.setAdapter(ifscAdapter);
        searchView.setOnQueryTextListener(new android.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.length() != 0) {
                    tv_popularCity.setText("Suggested Cities");
                } else {
                    tv_popularCity.setText("Popular Cities");
                }
                ifscAdapter.getFilter().filter(newText);

                return false;
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        lvSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                dismiss();
                IFSCModel ifscModel = (IFSCModel) adapterView.getItemAtPosition(i);
                ifscSelectedListener.serviceFilter(ifscModel);

            }
        });
        this.setCancelable(true);

        this.setContentView(viewDialog);

        this.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        this.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

    }

}
