package in.MadMoveGlobal.custom;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import in.MadMoveGlobal.EwireRuPay.R;

/**
 * Created by Dushant on 08/27/2017.
 */
public class CustomTermAlertDialog extends AlertDialog.Builder {

    public CustomTermAlertDialog(Context context, String title, String message) {
        super(context);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewDialog = inflater.inflate(R.layout.dialog_terms, null, false);

        TextView titleTextView = (TextView) viewDialog.findViewById(R.id.cadTiltle);
        titleTextView.setText(title);
        TextView tvTermsMessage = (TextView) viewDialog.findViewById(R.id.tvTermsMessage);
        tvTermsMessage.setMovementMethod(ScrollingMovementMethod.getInstance());
        tvTermsMessage.setText(Html.fromHtml(message));
        this.setCancelable(false);

        this.setView(viewDialog);


    }
}
