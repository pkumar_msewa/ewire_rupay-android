package in.MadMoveGlobal.loyaltyoffer;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import in.MadMoveGlobal.EwireRuPay.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class CustomDialog extends DialogFragment {


    public CustomDialog() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_custom_dialog, container, false);
    }

}
