package in.MadMoveGlobal.loyaltyoffer;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class OfferModel implements Serializable {
    private String offersName,validTill,offerTerms,imageLink;

    public OfferModel() {
    }

    public OfferModel(String offersName, String validTill, String offerTerms, String imageLink) {
        this.offersName = offersName;
        this.validTill = validTill;
        this.offerTerms = offerTerms;
        this.imageLink = imageLink;
    }

    public String getOffersName() {
        return offersName;
    }

    public void setOffersName(String offersName) {
        this.offersName = offersName;
    }

    public String getValidTill() {
        return validTill;
    }

    public void setValidTill(String validTill) {
        this.validTill = validTill;
    }

    public String getOfferTerms() {
        return offerTerms;
    }

    public void setOfferTerms(String offerTerms) {
        this.offerTerms = offerTerms;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }
}
