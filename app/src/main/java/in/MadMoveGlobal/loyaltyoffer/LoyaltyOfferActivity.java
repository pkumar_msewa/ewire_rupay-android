package in.MadMoveGlobal.loyaltyoffer;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import in.MadMoveGlobal.EwireRuPay.PayQwikApplication;
import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.EwireRuPay.activity.MainActivity;
import in.MadMoveGlobal.EwireRuPay.activity.MainMenuDetailActivity;
import in.MadMoveGlobal.EwireRuPay.activity.OtpVerificationMidLayerActivity;
import in.MadMoveGlobal.custom.CustomToast;
import in.MadMoveGlobal.fragment.fragmentuser.LoginFragment;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.model.LoyaltyPointModel;
import in.MadMoveGlobal.model.UserModel;
import in.MadMoveGlobal.util.NetworkErrorHandler;

public class LoyaltyOfferActivity extends AppCompatActivity
        implements TabLayout.OnTabSelectedListener,
        OfferRecyclerAdapter.GetOfferDetailsListener {

    public static final String TAG = "Loyalty";

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ImageButton ivBackBtn;
    private LoadingDialog loadingDialog;
    private  LoyaltyPointModel loyaltyPointModel;

    private UserModel session = UserModel.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rewards_offer);
        loadingDialog = new LoadingDialog(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        loyaltyPointModel = new LoyaltyPointModel();

        ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtns);

        ivBackBtn.setVisibility(View.VISIBLE);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoyaltyOfferActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        tabLayout = findViewById(R.id.tab_layout);
        // tabLayout.setTabTextColors(getResources().getColor(R.color.black),getResources().getColor(R.color.rp_red));
        viewPager = findViewById(R.id.view_pager);

        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.loyalty_card)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.offers)));

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        //  ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());

        /*SharedPreferences  mPrefs = getSharedPreferences("LoyaltyPoint",Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = mPrefs.getString("Loyalty","");
        LoyaltyPointModel loyaltyPointModel = gson.fromJson(json,LoyaltyPointModel.class);*/

        SharedPreferences sharedPreferences = this.getSharedPreferences("EWIRE", Context.MODE_PRIVATE);

        Log.v(TAG,"SSS check "+sharedPreferences.getBoolean("hasClavax",false));

        if (sharedPreferences.getBoolean("hasClavax",false)){

            viewPager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager(),
                    tabLayout.getTabCount(),
                    sharedPreferences.getString("cardNo",""),
                    sharedPreferences.getString("pin",""),
                    sharedPreferences.getString("expiryDate","")));

            tabLayout.setupWithViewPager(viewPager);

        }else {
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.layout_custom_dialog, null);

        TextView textView1 = dialogView.findViewById(R.id.text1);
        TextView textView2 = dialogView.findViewById(R.id.text2);
        textView1.setText("I want to earn Ewire Loyalty Point Powered by Clavax. ");
        textView2.setText("Terms & Conditions");

        textView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(LoyaltyOfferActivity.this,ShowWebviewActivity.class));

            }
        });

        AlertDialog dialog = new AlertDialog.Builder(LoyaltyOfferActivity.this)
                .setTitle("Ewire Rewards")
                .setView(dialogView)
                .setPositiveButton("Accept", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {

                        loadingDialog.show();

                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("sessionId", session.getUserSessionId());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.v("SSSdd ", "SSSccc " + session.getUserSessionId());

                        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_LOYALTY_REQUEST, jsonObject, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {

                                Log.v("SSS cccc", "SSS vvvv " + response.toString());

                                String code = null;
                                try {
                                    code = response.getString("code");

                                    if (code != null && code.equals("S00")) {

                                        loadingDialog.dismiss();
                                        JSONObject detailsObject = response.getJSONObject("details");

                                        Log.v("SSS ", "SSS cc " + detailsObject);
                                        JSONObject loyaltyJSONObject = detailsObject.getJSONObject("clavexDetails");

                                        if (loyaltyJSONObject.has("cardNo")
                                                && loyaltyJSONObject.has("expiryDate")
                                                && loyaltyJSONObject.has("pin")) {

                                            loyaltyPointModel.setCardNo(loyaltyJSONObject.getString("cardNo"));
                                            loyaltyPointModel.setExpiryDate(loyaltyJSONObject.getString("expiryDate"));
                                            loyaltyPointModel.setPin(loyaltyJSONObject.getString("pin"));
                                            loyaltyPointModel.setHasClavax(true);

                                            SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("EWIRE",Context.MODE_PRIVATE);
                                            SharedPreferences.Editor editor = sharedPreferences.edit();
                                            editor.putString("cardNo", loyaltyJSONObject.getString("cardNo"));
                                            editor.putString("expiryDate", loyaltyJSONObject.getString("expiryDate"));
                                            editor.putString("pin", loyaltyJSONObject.getString("pin"));
                                            editor.putBoolean("hasClavax",true);
                                            editor.commit();

                                            String cardNo = loyaltyJSONObject.getString("cardNo");
                                            String pin = loyaltyJSONObject.getString("pin");
                                            String expDate = loyaltyJSONObject.getString("expiryDate");

                                            viewPager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager(),
                                                    tabLayout.getTabCount(), cardNo, pin, expDate));


                                        }else {

                                            loyaltyPointModel.setHasClavax(false);
                                            SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("EWIRE",Context.MODE_PRIVATE);
                                            SharedPreferences.Editor editor = sharedPreferences.edit();
                                            editor.putBoolean("hasClavax",false);
                                            editor.commit();
                                        }

                                        dialog.dismiss();

                                        tabLayout.setupWithViewPager(viewPager);
                                    }else {
                                        loadingDialog.dismiss();
                                        Toast.makeText(LoyaltyOfferActivity.this, response.getString("message").toString(), Toast.LENGTH_SHORT).show();
                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                error.printStackTrace();
                            }
                        });

                        int socketTimeout = 60000;
                        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                        postReq.setRetryPolicy(policy);
                        Volley.newRequestQueue(getApplicationContext()).add(postReq);


                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        onBackPressed();
                    }
                }).create();


        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        tabLayout.setOnTabSelectedListener(this);

    }

     }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {

        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public void getOfferDetails(OfferModel offerModel) {

        Log.v(TAG, "SSS offer " + offerModel.getOffersName());

        Intent intent = new Intent(this, OfferDetailsActivity.class);
        intent.putExtra("Offers", offerModel);
        startActivity(intent);

    }
}
