package in.MadMoveGlobal.loyaltyoffer;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import in.MadMoveGlobal.EwireRuPay.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoyaltyCardFragment extends Fragment {

    private static final String TAG = LoyaltyCardFragment.class.getSimpleName();
    private TextView cardNumberTV,cardValidationTV,cardPinTV,loyaltyPointTV;

    private String cardNumber,cardPIN,cardExpDate;


    public LoyaltyCardFragment() {
        // Required empty public constructor
    }

    public static LoyaltyCardFragment newInstance(String cardNumber,String pin,String expDate) {

        Bundle args = new Bundle();

        args.putString("CARD_NUMBER",cardNumber);
        args.putString("PIN",pin);
        args.putString("EXP_DATE",expDate);

        LoyaltyCardFragment fragment = new LoyaltyCardFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = this.getArguments();

       if (bundle != null && bundle.getString("CARD_NUMBER") != null){
           cardNumber = bundle.getString("CARD_NUMBER");
       }
        if (bundle != null && bundle.getString("PIN") != null){
            cardPIN = bundle.getString("PIN");
        }
        if (bundle != null && bundle.getString("EXP_DATE") != null){
            cardExpDate = bundle.getString("EXP_DATE");
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_loyalty_card, container, false);

        cardNumberTV = view.findViewById(R.id.card_number_TV);
        cardValidationTV = view.findViewById(R.id.card_validation_TV);
        cardPinTV = view.findViewById(R.id.card_pin_TV);
        loyaltyPointTV = view.findViewById(R.id.loyalty_point_TV);

        if (cardNumber != null){
            cardNumberTV.setText(cardNumber);
        }
        if (cardPIN != null){
            cardPinTV.setText("Pin :"+cardPIN);
        }
        if (cardExpDate != null){
            cardValidationTV.setText("Valid Till :"+cardExpDate);
        }


        return view;
    }

}
