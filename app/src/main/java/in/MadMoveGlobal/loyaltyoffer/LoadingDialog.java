package in.MadMoveGlobal.loyaltyoffer;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Window;

import in.MadMoveGlobal.EwireRuPay.R;

public class LoadingDialog extends Dialog {

    public LoadingDialog(Context context) {
        super(context,R.style.ProgressDialog);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_progress_bar);
        setCancelable(false);
    }
}
