package in.MadMoveGlobal.loyaltyoffer;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import in.MadMoveGlobal.EwireRuPay.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class OfferDetailsFragment extends Fragment {

    private TextView offerDetails;
    private ImageView offerPoster;

    private OfferModel offerModel;


    public OfferDetailsFragment() {
        // Required empty public constructor
    }

    public static OfferDetailsFragment newInstance(OfferModel offerModel) {

        Bundle args = new Bundle();
        args.putSerializable("OFFERS",offerModel);

        OfferDetailsFragment fragment = new OfferDetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

       offerModel = (OfferModel) getArguments().getSerializable("OFFERS");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_offer_details, container, false);

        offerDetails = view.findViewById(R.id.offers_termsTV);
        offerPoster = view.findViewById(R.id.offer_poster);

        offerDetails.setText(offerModel.getOffersName());

        return view;
    }

}
