package in.MadMoveGlobal.loyaltyoffer;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import in.MadMoveGlobal.EwireRuPay.R;

public class OfferRecyclerAdapter extends RecyclerView.Adapter<OfferRecyclerAdapter.OfferRecyclerViewHolder> {

    private ArrayList<OfferModel> offerList;

    GetOfferDetailsListener getOfferDetailsListener;

    Context context;

    public OfferRecyclerAdapter() {
    }

    public OfferRecyclerAdapter(Context context, ArrayList<OfferModel> offerList, GetOfferDetailsListener getOfferDetailsListener) {
        this.context = context;
        this.offerList = offerList;
        this.getOfferDetailsListener = getOfferDetailsListener;
    }

    @NonNull
    @Override
    public OfferRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_offer_list, viewGroup, false);

        return new OfferRecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OfferRecyclerViewHolder offerRecyclerViewHolder, int position) {

        OfferModel offerModel = offerList.get(position);


        Picasso.with(context).load(offerModel.getImageLink()).into(offerRecyclerViewHolder.offerImage);

        offerRecyclerViewHolder.offerNameTV.setText(offerModel.getOffersName());

        String str = offerModel.getValidTill();
        str = str.substring(0, str.lastIndexOf("T"));

        offerRecyclerViewHolder.offerValidTV.setText("Valid Till : " + str);

        offerRecyclerViewHolder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getOfferDetailsListener.getOfferDetails(offerModel);
            }
        });
    }

    @Override
    public int getItemCount() {
        return offerList.size();
    }

    public class OfferRecyclerViewHolder extends RecyclerView.ViewHolder {
        View view;
        TextView offerNameTV, offerValidTV;
        ImageView offerImage;

        public OfferRecyclerViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;

            offerImage = view.findViewById(R.id.offer_image);
            offerNameTV = view.findViewById(R.id.offer_nameTv);
            offerValidTV = view.findViewById(R.id.offer_validTv);
        }
    }

    public interface GetOfferDetailsListener {

        void getOfferDetails(OfferModel offerModel);
    }
}
