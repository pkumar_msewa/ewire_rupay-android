package in.MadMoveGlobal.loyaltyoffer;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

public class ViewPagerAdapter extends FragmentPagerAdapter {

    public static final String TAG = ViewPagerAdapter.class.getSimpleName();

    private int tabCount;
    private String cardNumber, cardPIN, cardValidation;

    public ViewPagerAdapter(FragmentManager fm, int tabCount) {
        super(fm);
        this.tabCount = tabCount;
    }

    public ViewPagerAdapter(FragmentManager fm, int tabCount, String cardNumber, String cardPIN, String cardValidation) {
        super(fm);
        this.tabCount = tabCount;
        this.cardNumber = cardNumber;
        this.cardPIN = cardPIN;
        this.cardValidation = cardValidation;
    }

    @Override
    public Fragment getItem(int position) {
        Log.v(TAG, "SSS position " + position);

        switch (position) {

            case 0:
                return LoyaltyCardFragment.newInstance(cardNumber, cardPIN, cardValidation);

            case 1:
                OfferFragment offerFragment = new OfferFragment();
                return offerFragment;

            default:
                return null;
        }

    }

    @Override
    public int getCount() {
        return tabCount;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Loyalty Card";
            case 1:
                return "Offers";

            default:
                return null;
        }
    }
}
