package in.MadMoveGlobal.loyaltyoffer;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.EwireRuPay.activity.MainActivity;

public class OfferDetailsActivity extends AppCompatActivity {

    private TextView offerDetails;
    private ImageView offerPoster;
    private OfferModel offerModel;
    private ImageButton ivBackBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer_details);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtns);

        ivBackBtn.setVisibility(View.VISIBLE);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(OfferDetailsActivity.this, LoyaltyOfferActivity.class);
                startActivity(intent);
                finish();
            }
        });

        offerDetails = findViewById(R.id.offers_termsTV);
        offerPoster = findViewById(R.id.offer_poster);

        offerModel = (OfferModel) getIntent().getSerializableExtra("Offers");

        offerDetails.setText(offerModel.getOfferTerms());
        Picasso.with(this).load(offerModel.getImageLink()).into(offerPoster);


    }

}
