package in.MadMoveGlobal.loyaltyoffer;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.MadMoveGlobal.EwireRuPay.R;
import in.MadMoveGlobal.metadata.ApiUrl;
import in.MadMoveGlobal.model.UserModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class OfferFragment extends Fragment {

    private static final String TAG = OfferFragment.class.getSimpleName();

    private RecyclerView offerRecyclerView;
    private UserModel session = UserModel.getInstance();
    private ArrayList<OfferModel> offersList;
    private OfferRecyclerAdapter offerRecyclerAdapter;
    private LoadingDialog loadingDialog;

    public OfferFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_offer, container, false);

        offerRecyclerView = view.findViewById(R.id.recycler_view);
        loadingDialog = new LoadingDialog(getActivity());

        setUpAdapter();

        getOffer();


        return view;
    }

    private void getOffer(){
loadingDialog.show();
       JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("sessionId", session.getUserSessionId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_LOYALTY_OFFER, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                String code = null;
                try {
                    code = response.getString("code");

                    if (code != null && code.equals("S00")) {
                        loadingDialog.dismiss();

                        String details = response.getString("details");

                        JSONObject detailsObj = new JSONObject(details);
                        JSONArray dataArray = detailsObj.getJSONArray("data");

                        for (int i =0;i<dataArray.length();i++){

                            JSONObject info = dataArray.getJSONObject(i);

                           OfferModel offerModel = new OfferModel();
                           offerModel.setOffersName(info.getString("title"));
                           offerModel.setValidTill(info.getString("endDateTime"));
                            offerModel.setImageLink(info.getString("mobileImageLink"));
                            offerModel.setOfferTerms(info.getString("termAndCondition"));

                           offersList.add(offerModel);

                           offerRecyclerAdapter.notifyDataSetChanged();
                        }


                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> map = new HashMap<>();
                map.put("hash", "1234");
                return map;
            }

        };

        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);
        Volley.newRequestQueue(getActivity()).add(postReq);

    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle("Offers");
    }

    private void setUpAdapter(){

        offersList = new ArrayList<>();

        offerRecyclerAdapter = new OfferRecyclerAdapter(getActivity(),offersList, (OfferRecyclerAdapter.GetOfferDetailsListener) getActivity());

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        offerRecyclerView.setLayoutManager(linearLayoutManager);

        offerRecyclerView.setAdapter(offerRecyclerAdapter);

    }
}
