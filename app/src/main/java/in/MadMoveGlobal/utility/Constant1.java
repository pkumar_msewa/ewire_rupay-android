package in.MadMoveGlobal.utility;

/**
 * Created by amit.pande on 28/7/15.
 */
public class Constant1 {
  public static final String PARAMETER_SEP = "&";
  public static final String PARAMETER_EQUALS = "=";
  public static final String JSON_URL = "http://106.51.8.246/ewire/Api/v1/User/Android/en/LoadMoney/RedirectUPI";
  public static final String TRANS_URL = "https://liveewire.com:8443/Api/v1/User/Android/en/LoadMoney/RedirectUPI";
  public static final String TRANS_PAYFI_URL = "https://liveewire.com:8443/Api/v1/User/Android/en/LoadMoney/RedirectPG";
}
